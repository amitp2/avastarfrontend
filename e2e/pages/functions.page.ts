import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';

export function navigateToAccounts() {
    return browser.get('/u/accounts');
}

export function getEventSearchField() {
    return element(by.css('.popup-events-list')).element(by.css('[formcontrolname="search"]'));
}

export function getFunctionSearchField() {
    return element(by.css('.popup-events-list-search')).element(by.css('[formcontrolname="search"]'));
}

export function getAddButton() {
    return element(by.css('app-function-equipments-search')).element(by.buttonText('Add'));
}

export function getEventsInfo() {
  return element(by.css('.popup-tabs-nav')).element(by.cssContainingText('a', 'Events'));
}

export function getEventCode() {
    return element(by.css('[formcontrolname="eventCode"]'));
}

export function getEventMasterNo() {
    return element(by.css('[formcontrolname="masterAccountNumber"]'));
}

export function getEventStatus() {
    return element(by.css('[formcontrolname="status"]'));
}

export function getEditEvent() {
    return element(by.css('.popup-events-list')).element(by.css('.fa-pencil'));
}

export function getAddFunction() {
    return element(by.cssContainingText('.red-plus-label', 'Add Function'));
}

export function getCopyFunction() {
    return element(by.css('app-event-functions')).element(by.css('.fa-copy'));
}

export function getEditFunction() {
    return element(by.css('app-event-functions')).element(by.css('.fa-pencil'));
}

export function getDeleteFunction() {
    return element(by.css('app-event-functions')).element(by.css('.fa-trash'));
}

export function getAccountDropdown() {
    return element(by.css('[formcontrolname="clientId"]'));
}

export function getEventDropdown() {
    return element(by.css('[formcontrolname="eventId"]'));
}

export function getFunctionDropdown() {
    return element(by.css('[formcontrolname="functionSpaceId"]'));
}

export function getLocationNote() {
    return element(by.css('[formcontrolname="locationNote"]'));
}

export function getFunctionName() {
    return element(by.css('[formcontrolname="functionName"]'));
}

export function getFunctionType() {
    return element(by.css('.heading-block+.content-block select[formcontrolname="type"]'));
}

export function getFunctionStyle() {
    return element(by.css('[formcontrolname="setupStyle"]'));
}

export function getFunctionParticipants() {
    return element(by.css('[formcontrolname="participantsCount"]'));
}

export function getPlanningContact() {
    return element(by.css('[formcontrolname="planningContact"] .ui-autocomplete-input'));
}

export function getSalesContact() {
    return element(by.css('[formcontrolname="salesContact"] .ui-autocomplete-input'));
}

export function getFunctionStaging() {
    return element(by.css('[formcontrolname="stagingRequirements"]'));
}

export function getFunctionStartDate() {
    return element(by.css('[formcontrolname="startDate"]')).element(by.css('.hasDatepicker'));
}

export function getFunctionEndDate() {
    return element(by.css('[formcontrolname="endDate"]')).element(by.css('.hasDatepicker'));
}

export function getStartDate() {
    return element(by.css('.subheader-tabs')).element(by.cssContainingText('a', '2018-01-01'));
}

export function getEndDate() {
    return element(by.css('.subheader-tabs')).element(by.cssContainingText('a', '2018-01-02'));
}

export function getSetTime() {
    return element(by.css('[formcontrolname="setTime"] input'));
}

export function getStartTime() {
    return element(by.css('[formcontrolname="startTime"] input'));
}

export function getEndTime() {
    return element(by.css('[formcontrolname="endTime"] input'));
}

export function getBEOField() {
    return element(by.css('[formcontrolname="beo"]'));
}

export function getWorkflowNoteField() {
    return element(by.css('[formcontrolname="workflowNote"]'));
}

export function getClientNoteField() {
    return element(by.css('[formcontrolname="clientNote"]'));
}

export function getHoursHold() {
    return element(by.css('[formcontrolname="hoursHold24"]'));
}

export function getHoursHoldClick() {
    return element(by.cssContainingText('label', '24 Hours Hold'));
}

export function getSetupDays() {
    return element(by.css('[formcontrolname="setupDay"]'));
}

export function getSetupDaysClick() {
    return element(by.cssContainingText('label', 'Setup Day'));
}

export function getFunctionItem() {
    return element(by.css('[formcontrolname="item"]'));
}

export function getEaselQty() {
    return element(by.id('Flipchart, Easel Client')).element(by.css('input'));
}

export function getFlipchartPackageQty() {
    return element(by.id('FunctionPackageFlipchart')).element(by.css('input'));
}

export function getFlipchartPackageType() {
    return element(by.id('FunctionPackageFlipchart')).element(by.css('app-select'));
}

export function getShowTotals() {
    return element(by.buttonText('Show Totals'));
}

export function getTotalCount() {
    return element(by.css('app-function-totals tbody tr:last-child'));
}

export function getStandardDiscount() {
    return element(by.css('[formcontrolname="standardDiscountRate"]'));
}

export function getSetupDiscount() {
    return element(by.css('[formcontrolname="discountRateForSetupDays"]'));
}

export function getAltChargePercentage() {
    return element(by.css('[formcontrolname="alternativeServiceChargePercentage"]'));
}

export function getServiceChargeCalculation() {
    return element(by.css('.add-function [formcontrolname="serviceChargeCalculation"]'));
}

export function getPreDiscount() {
    return element(by.cssContainingText('label', 'Pre Discounted Pricing'));
}

export function getPostDiscount() {
    return element(by.cssContainingText('label', 'Post Discounted Pricing'));
}

export function getAltCharge() {
    return element(by.css('[formcontrolname="alternativeServiceCharge"]'));
}

export function getTaxExempt() {
    return element(by.css('[formcontrolname="taxExempt"]'));
}

export function getItemSearch() {
    return element(by.css('app-function-equipments-search [formcontrolname="search"]'));
}

export function getFunctionTable() {
    return element(by.css('app-event-functions table tbody tr'));
}
