import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';

export function navigateToCost() {
    return browser.get('/u/manage-cost-category');
}

export function getCostName() {
      return element(by.css('[formcontrolname="name"]'));
}

export function getCostDescription() {
      return element(by.css('[formcontrolname="description"]'));
}
