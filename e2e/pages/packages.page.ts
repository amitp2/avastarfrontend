import { protractor, browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';

export function navigateToPackages() {
    return browser.get('/u/packages');
}

export function getMinPriceFilter() {
    return element(by.css('[formcontrolname="minPrice"]'));
}

export function getMaxPriceFilter() {
    return element(by.css('[formcontrolname="maxPrice"]'));
}

export function getPackageAddButton() {
    return element(by.css('app-package-items + .content-buttons button[type="submit"]'));
}

export function getPackageCancelButton() {
    return element.all(by.buttonText('Cancel')).last();
}

export function getItemAddButton() {
    return element(by.css('.tbt-center')).element(by.buttonText('Add'));
}

export function getItemRemoveButton() {
    return element(by.css('.tbt-center')).element(by.buttonText('Remove'));
}

export function getItemSearchButton() {
    return element(by.css('.content-buttons')).element(by.buttonText('Search'));
}

export function getSearchCancelButton() {
    return element.all(by.buttonText('Cancel')).first();
}

export function getCategoryDropdown() {
    return element(by.css('[formcontrolname="categoryId"]'));
}

export function getSubCategoryDropdown() {
    return element(by.css('[formcontrolname="subCategoryId"]'));
}

export function getPackageNameField() {
    return element(by.css('[formcontrolname="name"]'));
}

export function getPackageSalesField() {
    return element(by.css('[formcontrolname="salesDescription"]'));
}

export function getCalculatedPriceField() {
    return element(by.css('app-packages-form .content-right-col span'));
}

export function getActualPriceField() {
    return element(by.css('[formcontrolname="price"]'));
}

export function getInputQuantityFirst() {
    return element.all(by.css('input[type="number"]')).first();
}

export function getInputQuantityLast() {
    return element.all(by.css('input[type="number"]')).last();
}

export function getStartDate() {
    return element(by.css('[formcontrolname="priceFromDate"]')).element(by.css('.hasDatepicker'));
}

export function getEndDate() {
    return element(by.css('[formcontrolname="priceToDate"]')).element(by.css('.hasDatepicker'));
}

export function getOptionLeftEquipment1() {
    return element(by.css('.tbt-left')).element(by.cssContainingText('td', 'Flipchart, Easel'))
                                       .getWebElement()
                                       .getDriver()
                                       .findElement(by.css('.custom-checkbox'));
}

export function getOptionRightItem1Quantity() {
    return element(by.css('.tbt-right')).element(by.css('tbody tr:nth-child(1) input[type="number"]'));
}

export function getOptionLeftEquipment2() {
    return element(by.css('.tbt-left')).element(by.cssContainingText('td', 'Flipchart, Marker'))
                                       .getWebElement()
                                       .getDriver()
                                       .findElement(by.css('.custom-checkbox'));
}

export function getOptionRightItem2Quantity() {
    return element(by.css('.tbt-right')).element(by.css('tbody tr:nth-child(2) input[type="number"]'));
}

export function getOptionRightItem1() {
    return element(by.css('.tbt-right')).element(by.css('tbody tr:nth-child(1) .custom-checkbox'));
}

export function getOptionRightItem2() {
    return element(by.css('.tbt-right')).element(by.css('tbody tr:nth-child(2) .custom-checkbox'));
}

export function getOptionRightItem3() {
    return element(by.css('.tbt-right')).element(by.css('tbody tr:nth-child(3) .custom-checkbox'));
}

export function getOptionRightItem4() {
    return element(by.css('.tbt-right')).element(by.css('tbody tr:nth-child(4) .custom-checkbox'));
}

export function getHistoryItem() {
    return element(by.css('tbody tr:nth-child(2)'));
}
