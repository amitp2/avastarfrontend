import { protractor, browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';

export function navigateToSystems() {
    return browser.get('/u/systems');
}

export function getSystemAddButton() {
    return element(by.css('app-inventory-select + .content-buttons button[type="submit"]'));
}

export function getSystemCancelButton() {
    return element.all(by.buttonText('Cancel')).last();
}

export function getEquipmentAddButton() {
    return element(by.css('.tbt-center')).element(by.buttonText('Add'));
}

export function getEquipmentRemoveButton() {
    return element(by.css('.tbt-center')).element(by.buttonText('Remove'));
}

export function getSearchButton() {
    return element(by.css('.content-buttons')).element(by.buttonText('Search'));
}

export function getSearchCancelButton() {
    return element.all(by.buttonText('Cancel')).first();
}

export function getSystemNameField() {
    return element(by.css('[formcontrolname="name"]'));
}

export function getSystemDescriptionField() {
    return element(by.css('[formcontrolname="description"]'));
}

export function getCategoryDropdown() {
    return element(by.css('[formcontrolname="categoryId"]'));
}

export function getSubCategoryDropdown() {
    return element(by.css('[formcontrolname="subCategoryId"]'));
}

export function getBarcodeField() {
    return element(by.css('[formcontrolname="barcode"]'));
}

export function getTagField() {
    return element(by.css('[formcontrolname="tagNumber"]'));
}

export function getStatusDropdown() {
    return element(by.css('[formcontrolname="condition"]'));
}

export function getLocationDropdown() {
    return element(by.css('[formcontrolname="locationId"]'));
}

export function getOptionLeftInventory1() {
    return element(by.css('.tbt-left')).element(by.cssContainingText('td', 'Make Distribution'))
                                       .getWebElement()
                                       .getDriver()
                                       .findElement(by.css('.custom-checkbox'));
}

export function getOptionLeftInventory2() {
    return element(by.css('.tbt-left')).element(by.cssContainingText('td', 'Make Accessories'))
                                       .getWebElement()
                                       .getDriver()
                                       .findElement(by.css('.custom-checkbox'));
}

export function getOptionRightInventory1() {
    return element(by.css('.tbt-right')).element(by.css('tbody tr:nth-child(1) .custom-checkbox'));
}

export function getOptionRightInventory2() {
    return element(by.css('.tbt-right')).element(by.css('tbody tr:nth-child(2) .custom-checkbox'));
}

export function getCategoryFilter() {
    return element(by.css('[formcontrolname="categoryId"]'));
}

export function getSubCategoryFilter() {
    return element(by.css('[formcontrolname="subCategoryId"]'));
}

export function getHistoryItem() {
    return element(by.css('tbody tr'));
}
