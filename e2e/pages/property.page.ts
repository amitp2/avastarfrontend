import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';
export * from './common/contacts.page';

export function navigateToProps() {
    return browser.get('/u/venues');
}

export function getContactUpdate() {
    return element(by.css('.popup-footer-buttons')).element(by.cssContainingText('button', 'Update'));
}

export function getPropertyName() {
    return element(by.css('[formcontrolname="name"]'));
}

export function getAddressField() {
    return element(by.css('[formcontrolname="address"]'));
}

export function getAddressNdField() {
    return element(by.css('[formcontrolname="address2"]'));
}

export function getCityField() {
    return element(by.css('[formcontrolname="city"]'));
}

export function getPostCodeField() {
    return element(by.css('[formcontrolname="postCode"]'));
}

export function getCountrySelect() {
  return element(by.css('[formcontrolname="countryId"]'));
}

export function getPhoneMain() {
  return element(by.css('[formcontrolname="mainPhone"]')).element(by.css('input'));
}

export function getFaxMain() {
  return element(by.css('[formcontrolname="mainFax"]'));
}

export function getStateSelect() {
    return element(by.css('[formcontrolname="stateId"]'));
}

export function getWebsiteField() {
  return element(by.css('[formcontrolname="website"]'));
}

export function getOfficeField() {
  return element(by.css('[formcontrolname="branch"]'));
}

export function getSubscriberDropdown() {
  return element(by.css('[formcontrolname="subscriberId"]'));
}

export function getPropertyOwner() {
    return element(by.css('[formcontrolname="owner"]'));
}

export function getPropertyOperator() {
    return element(by.css('[formcontrolname="operator"]'));
}

export function getSettingsBilling() {
    return element(by.css('[formcontrolname="billingFormat"]'));
}

export function getSettingsCurrency() {
    return element(by.css('[formcontrolname="currency"]'));
}

export function getEventType() {
    return element(by.css('[formcontrolname="eventType"]'));
}

export function getTEONo() {
    return element(by.css('[formcontrolname="teo"]'));
}

export function getTEOStartNo() {
    return element(by.css('[formcontrolname="teoStart"]'));
}

export function getPONo() {
    return element(by.css('[formcontrolname="po"]'));
}

export function getPOStartNo() {
    return element(by.css('[formcontrolname="poStart"]'));
}

export function getSettingsReport() {
    return element(by.css('[formcontrolname="reportDisclaimer"]'));
}

export function getSettingsEmail() {
    return element(by.css('[formcontrolname="emailDisclaimer"]'));
}

export function getTableUser() {
    return element(by.css('tbody tr'));
}

export function getContactOwner() {
    return element(by.css('app-venue-contacts tbody tr')).element(by.cssContainingText('span', 'Property Owner'));
}

export function getContactOperator() {
    return element(by.css('app-venue-contacts tbody tr')).element(by.cssContainingText('span', 'Property Operator'));
}

export function getSubscriberDropdownAccess() {
    return element(by.css('app-select[formcontrolname="subscriberId"] div'));
}

export function getPropertyContent() {
    return element(by.css('.property-top-content'));
}
