import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';

export function navigateToPricing() {
    return browser.get('/u/pricing-extension');
}

export function navigateToEquipment() {
    return browser.get('/u/equipment');
}

export function navigateToPackages() {
    return browser.get('/u/packages');
}

export function getEndDate() {
    return element(by.css('[formcontrolname="priceToDate"]')).element(by.css('.hasDatepicker'));
}

export function getPackagesEndDate() {
    return element(by.css('[formcontrolname="priceToDate"]')).element(by.css('.hasDatepicker'));
}

export function getCurrentDate() {
    return element(by.css('[formcontrolname="currentDate"]')).element(by.css('.hasDatepicker'));
}

export function getExtendDate() {
    return element(by.css('[formcontrolname="extensionDate"]')).element(by.css('.hasDatepicker'));
}

export function getNextDate() {
    return element(by.cssContainingText('span', 'Next'));
}

export function getPrevDate() {
    return element(by.cssContainingText('span', 'Prev'));
}

export function getLastDate() {
    return element.all(by.css('.ui-state-default')).last();
}

export function getFirstDate() {
    return element.all(by.css('.ui-state-default')).first();
}

export function getApplyDropdown() {
    return element(by.css('[formcontrolname="applyTo"]'));
}

export function getTypeFilter() {
    return element(by.css('[formcontrolname="type"]'));
}

export function getDescriptionFilter() {
    return element(by.css('[formcontrolname="search"]'));
}
