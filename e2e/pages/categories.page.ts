import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';

export function navigateToCost() {
    return browser.get('/u/cost-category');
}

export function navigateToRevenue() {
    return browser.get('/u/revenue-category');
}

export function getCostCategoryField() {
    return element(by.css('[formcontrolname="costCategoryName"]'));
}

export function getCostCodeField() {
    return element(by.css('[formcontrolname="costCode"]'));
}

export function getRevenueCategoryField() {
    return element(by.css('[formcontrolname="revenueCategoryName"]'));
}

export function getRevenueCodeField() {
    return element(by.css('[formcontrolname="revenueCode"]'));
}

export function getDiscountbleToggle() {
    return element(by.cssContainingText('label', 'Discountable'));
}

export function getChargeToggle() {
    return element(by.cssContainingText('label', 'Service Charge'));
}
