import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';

export function navigateToStorageSpace() {
    return browser.get('/u/storage-spaces');
}

export function getStorageName() {
    return element(by.css('[formcontrolname="name"]'));
}

export function getStorageStatus() {
    return element(by.css('[formcontrolname="status"]'));
}

export function getStorageNotes() {
    return element(by.css('[formcontrolname="notes"]'));
}
