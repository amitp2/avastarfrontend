import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';

export function navigateToEquipment() {
    return browser.get('/u/equipment');
}

export function getCancelButton() {
    return element(by.buttonText('Cancel'));
}

export function getEquipmentCheckbox() {
    return element(by.cssContainingText('label', 'Equipment'));
}

export function getServiceCheckbox() {
    return element(by.cssContainingText('label', 'Service'));
}

export function getPortableCheckbox() {
    return element(by.cssContainingText('label', 'Portable Inventory'));
}

export function getInstalledCheckbox() {
    return element(by.cssContainingText('label', 'Installed Inventory'));
}

export function getYesCheckbox() {
    return element(by.cssContainingText('label', 'Yes'));
}

export function getNoCheckbox() {
    return element(by.cssContainingText('label', 'No'));
}

export function getPriceField() {
    return element(by.css('[formcontrolname="price"]'));
}

export function getStartDate() {
    return element(by.css('[formcontrolname="priceFromDate"]')).element(by.css('.hasDatepicker'));
}

export function getEndDate() {
    return element(by.css('[formcontrolname="priceToDate"]')).element(by.css('.hasDatepicker'));
}

export function getDescriptionField() {
    return element(by.css('[formcontrolname="name"]'));
}

export function getRentalTypeDropdown() {
    return element(by.css('[formcontrolname="crossRentalType"]'));
}

export function getSetupTimeField() {
    return element(by.css('[formcontrolname="setupTime"]'));
}

export function getCategoryDropdown() {
    return element(by.css('[formcontrolname="categoryId"]'));
}

export function getSubCategoryDropdown() {
    return element(by.css('[formcontrolname="subCategoryId"]'));
}

export function getEquipmentDropdown() {
    return element(by.css('.content-block')).element(by.css('[formcontrolname="subCategoryItemId"]'));
}

export function getRevenueDropdown() {
    return element(by.css('[formcontrolname="revenueCategoryId"]'));
}

export function getCostDropdown() {
    return element(by.css('[formcontrolname="costCategoryId"]'));
}

export function getClientField() {
    return element(by.css('[formcontrolname="clientDescription"]'));
}

export function getWorkflowField() {
    return element(by.css('[formcontrolname="workflowDescription"]'));
}

export function getTypeFilter() {
    return element(by.css('[formcontrolname="type"]'));
}

export function getDescriptionFilter() {
    return element(by.css('[formcontrolname="search"]'));
}

export function getHistoryItem() {
    return element(by.css('tbody tr'));
}
