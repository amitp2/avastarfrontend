import { browser, by, element } from 'protractor';
export * from './common/attributes.page';

export function getForgotButton() {
    return element(by.css('form a'));
}

export function getRememberMe() {
    return element(by.css('[formcontrolname="rememberMe"]'))
          .getWebElement()
          .getDriver()
          .findElement(by.css('label'));
}
