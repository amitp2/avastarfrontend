import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';
export * from './common/contacts.page';

export function navigateToVendors() {
    return browser.get('/u/vendors');
}

export function getVendorName() {
    return element(by.css('[formcontrolname="name"]'));
}

export function getVendorAddress() {
    return element(by.css('[formcontrolname="address"]'));
}

export function getVendorAddress2() {
    return element(by.css('[formcontrolname="address2"]'));
}

export function getVendorCity() {
    return element(by.css('[formcontrolname="city"]'));
}

export function getVendorState() {
    return element(by.css('[formcontrolname="stateId"]'));
}

export function getVendorCountry() {
    return element(by.css('[formcontrolname="countryId"]'));
}

export function getVendorPostCode() {
    return element(by.css('[formcontrolname="postCode"]'));
}

export function getVendorPhone() {
    return element(by.css('[formcontrolname="phone"]')).element(by.css('input'));
}

export function getVendorFax() {
    return element(by.css('[formcontrolname="fax"]'));
}

export function getVendorWebsite() {
    return element(by.css('[formcontrolname="website"]'));
}

export function getContactUpdate() {
  return element(by.css('.popup-footer-buttons')).element(by.buttonText('Update'));
}

export function getContactPhone() {
    return element(by.css('.popup-body [formcontrolname="phone"] input'));
}

export function getTableItem() {
    return element(by.css('.records-table-body tr'));
}

export function getUserTable() {
    return element(by.css('tbody tr'));
}
