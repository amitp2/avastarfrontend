import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';

export function navigateToRevenue() {
    return browser.get('/u/manage-revenue-category');
}

export function getRevenueName() {
    return element(by.css('[formcontrolname="name"]'));
}

export function getRevenueType() {
    return element(by.css('[formcontrolname="code"]'));
}

export function getRevenueDescription() {
    return element(by.css('[formcontrolname="description"]'));
}
