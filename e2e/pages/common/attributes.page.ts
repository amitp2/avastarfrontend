import { protractor, browser, by, element } from 'protractor';

const EC = protractor.ExpectedConditions;
const toastMessageIsVisible = EC.visibilityOf(getToastMessage());

export function navigateToLogin() {
    return browser.get('/');
}

export function getLoginButton() {
    return element(by.css('form [type="submit"]'));
}

export function getEmailField() {
    return element(by.css('[formcontrolname="username"]'));
}

export function getPasswordField() {
    return element(by.css('[formcontrolname="password"]'));
}

export function getToastMessage() {
    return element(by.css('.toast-message'));
}

export function getSpinner() {
    return element(by.css('.app-spinner'));
}

export function getUploadLogo() {
    return element(by.css('input[type="file"]'));
}

export function getDropdownSearch() {
    return element(by.css('.select2-search__field'));
}

export function getDropdownOption(str) {
    return element.all(by.cssContainingText('li', str)).last();
}

export function checkToastMessage(message) {
    browser.wait(function () {
        browser.ignoreSynchronization = true;
        browser.wait(toastMessageIsVisible, 5000);
        return expect(getToastMessage().getText()).toContain(message).then(function(toContain) {
          browser.ignoreSynchronization = false;
          return !toContain;
        });
    });
}

export function selectFromDropdown(str) {
    getDropdownSearch().sendKeys(str);
    getDropdownOption(str).click();
}

export function loginUserPass(username, password) {
    navigateToLogin();
    getEmailField().sendKeys(username);
    getPasswordField().sendKeys(password);
    getLoginButton().click();
    checkToastMessage('Login was successful');
}
