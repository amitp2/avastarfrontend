import { browser, by, element } from 'protractor';

export function getSearchField() {
    return element(by.css('[formcontrolname="search"]'));
}

export function getAddNew() {
    return element(by.css('.red-plus:not([style])'));
}

export function getAddNewFa() {
    return element(by.css('.fa-plus'));
}

export function getEditButton() {
    return element(by.css('.fa-pencil'));
}

export function getContactButton() {
    return element(by.css('.fa-address-card'));
}

export function getDeleteButton() {
  return element(by.css('.fa-trash'));
}

export function getAddButton() {
    return element(by.buttonText('Add'));
}

export function getUpdateButton() {
    return element(by.buttonText('Update'));
}

export function getCancelButton() {
    return element(by.cssContainingText('a', 'Cancel'));
}

export function getCloseButton() {
    return element(by.css('.popup-close'));
}

export function getBackButton() {
    return element(by.cssContainingText('a', 'BACK'));
}

export function getCloneButton() {
    return element(by.css('.fa-copy'));
}

export function getDownloadButton() {
    return element(by.css('.fa-download'));
}

export function getTableItem() {
    return element(by.css('.complex-table-body tr'));
}

export function getApplyButton() {
    return element(by.buttonText('Apply'));
}

export function getClearButton() {
    return element(by.buttonText('Clear'));
}

export function getSaveButton() {
    return element(by.buttonText('Save'));
}

export function getAddCopyButton() {
    return element(by.buttonText('Add & copy'));
}

export function getUpdateCopyButton() {
    return element(by.buttonText('Update & copy'));
}
