import { browser, by, element } from 'protractor';

export function getContactFname() {
return element(by.css('[formcontrolname="firstName"]'));
}

export function getContactLname() {
return element(by.css('[formcontrolname="lastName"]'));
}

export function getContactPhone() {
return element(by.css('[formcontrolname="phone"]')).element(by.css('input'));
}

export function getContactEmail() {
return element(by.css('[formcontrolname="email"]'));
}

export function getContactDepartment() {
    return element(by.css('[formcontrolname="department"]'));
}

export function getContactTitle() {
    return element(by.css('[formcontrolname="title"]'));
}

export function getContactFax() {
    return element(by.css('[formcontrolname="fax"]'));
}

export function getContactMobile() {
    return element(by.css('[formcontrolname="mobile"]')).element(by.css('input'));
}

export function getContactRole() {
    return element(by.css('[formcontrolname="role"]'));
}

export function getContactNote() {
    return element(by.css('[formcontrolname="notes"]'));
}
