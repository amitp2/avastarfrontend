import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';

export function navigateToDashboard() {
    return browser.get('/u');
}

export function navigateToEvents() {
    return browser.get('/u/event-management');
}

export function getEventBlockHeader() {
    return element(by.css('.-events .db-block-header .db-block-header-title'));
}

export function getEventBlockSearch() {
    return element(by.css('.-events .db-block-footer [type="search"]'));
}

export function getEventBlockBody() {
    return element(by.css('.-events .db-block-table-body tr'));
}

export function getTicketBlockHeader() {
    return element(by.css('.-events + div .db-block-header .db-block-header-title'));
}

export function getTicketBlockSearch() {
    return element(by.css('.-events  + div .db-block-footer [type="search"]'));
}

export function getTicketBlockBody() {
    return element(by.css('.-events + div .db-block-table-body tr'));
}

export function getFunctionStartDate() {
    return element(by.css('[formcontrolname="startDate"]')).element(by.css('.hasDatepicker'));
}

export function getFunctionEndDate() {
    return element(by.css('[formcontrolname="endDate"]')).element(by.css('.hasDatepicker'));
}

export function getEventCode() {
    return element(by.css('[formcontrolname="eventCode"]'));
}

export function getSetTime() {
    return element(by.css('[formcontrolname="setTime"] input'));
}

export function getStartTime() {
    return element(by.css('[formcontrolname="startTime"] input'));
}

export function getEndTime() {
    return element(by.css('[formcontrolname="endTime"] input'));
}
