import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';


export function navigateToTerms() {
    return browser.get('/u/terms');
}

export function getTermsTab() {
    return element(by.cssContainingText('a', 'Terms & Conditions'));
}

export function getBillingTab() {
    return element(by.cssContainingText('a', 'Billing Comment'));
}

export function getProposalTab() {
    return element(by.cssContainingText('a', 'Proposal Comment'));
}

export function getTextInput() {
    return element(by.id('tinymce'));
}
