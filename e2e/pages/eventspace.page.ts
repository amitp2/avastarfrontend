import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';

export function navigateToEventSpace() {
    return browser.get('/u/event-spaces');
}

export function getFunctionDelete() {
    return element.all(by.css('.fa-trash')).get(1);
}

export function getEventName() {
    return element(by.css('[formcontrolname="name"]'));
}

export function getEventStatus() {
    return element(by.css('[formcontrolname="status"]'));
}

export function getFunctionName() {
    return element(by.css('[formcontrolname="name"]'));
}

export function getFunctionShortName() {
    return element(by.css('[formcontrolname="roomName"]'));
}

export function getFunctionStatus() {
    return element(by.css('[formcontrolname="status"]'));
}

export function getTableFunction() {
    return element(by.css('.popup-users-list')).element(by.css('tbody tr'));
}

export function getFunctionSpace() {
    return element(by.cssContainingText('a', 'Function Spaces'));
}

export function getPlusFunction() {
  return element(by.css('.popup-users-list')).element(by.css('.red-plus'));
}

export function getEditFunction() {
  return element(by.css('.popup-users-list')).element(by.css('.fa-pencil'));
}

export function getDeleteFunction() {
  return element(by.css('.popup-users-list')).element(by.css('.fa-trash'));
}
