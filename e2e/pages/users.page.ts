import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';

  export function navigateToUsers() {
       return browser.get('/u/users');
  }

  export function getUserFname() {
    return element(by.css('[formcontrolname="firstName"]'));
  }

  export function getUserLname() {
    return element(by.css('[formcontrolname="lastName"]'));
  }

  export function getUserPhone() {
    return element(by.css('[formcontrolname="phone"]')).element(by.css('input'));
  }

  export function getUserEmail() {
    return element(by.css('[formcontrolname="username"]'));
  }

  export function getUserTitle() {
      return element(by.css('[formcontrolname="title"]'));
  }

  export function getUserFax() {
      return element(by.css('[formcontrolname="fax"]'));
  }

  export function getUserMobile() {
      return element(by.css('[formcontrolname="mobile"]')).element(by.css('input'));
  }

  export function getUserNotes() {
      return element(by.css('[formcontrolname="notes"]'));
  }

  export function getSubscriberDropdown() {
    return element(by.css('[formcontrolname="subscriberId"]'));
  }

  export function getSubscriberDropdownAccess() {
      return element(by.css('app-select[formcontrolname="subscriberId"] div'));
  }

  export function getUserRoleField() {
      return element(by.css('[formcontrolname="roles"] .select2-selection__rendered'));
  }

  export function getUserRolePick() {
      return element(by.css('.select2-results')).element(by.cssContainingText('span', 'Admin'));
  }
