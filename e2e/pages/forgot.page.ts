import { browser, by, element } from 'protractor';
export * from './common/attributes.page';

export function navigateToForgot() {
    return browser.get('/forgot');
}

export function getEmailField() {
    return element(by.css('[formcontrolname="email"]'));
}

export function getResetButton() {
    return element(by.css('form [type="submit"]'));
}
