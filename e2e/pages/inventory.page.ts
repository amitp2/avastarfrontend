import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';

export function navigateToInventory() {
    return browser.get('/u/inventory');
}

export function getAddButton() {
    return element(by.buttonText('Add & Go To Next Step'));
}

export function getUpdateButton() {
    return element(by.buttonText('Update & Go To Next Step'));
}

export function getBackButton() {
    return element(by.cssContainingText('.button', 'Back'));
}

export function getDoneButton() {
    return element(by.cssContainingText('.button', 'Done'));
}

export function getCancelButton() {
    return element(by.buttonText('Cancel'));
}

export function getCategoryFilter() {
    return element(by.css('[formcontrolname="categoryId"]'));
}

export function getMakeFilter() {
    return element(by.css('[formcontrolname="manufacturerId"]'));
}

export function getStatusFilter() {
    return element(by.css('[formcontrolname="condition"]'));
}

export function getTagFilter() {
    return element(by.css('[formcontrolname="tagNumber"]'));
}

export function getBarcodeFilter() {
    return element(by.css('[formcontrolname="barcode"]'));
}

export function getPurchaseFromFilter() {
    return element(by.css('[formcontrolname="purchaseDateFrom"]')).element(by.css('.hasDatepicker'));
}

export function getPurchaseToFilter() {
    return element(by.css('[formcontrolname="purchaseDateTo"]')).element(by.css('.hasDatepicker'));
}

export function getCategoryDropdown() {
    return element(by.css('[formcontrolname="categoryId"]'));
}

export function getSubCategoryDropdown() {
    return element(by.css('[formcontrolname="subCategoryId"]'));
}

export function getEquipmentDropdown() {
    return element(by.css('[formcontrolname="subCategoryItemId"]'));
}

export function getMakeField() {
    return element(by.css('[formcontrolname="manufacturerName"]')).element(by.css('.ui-autocomplete-input'));
}

export function getModelField() {
    return element(by.css('.content-block')).element(by.css('[formcontrolname="model"]'));
}

export function getSerialField() {
    return element(by.css('.content-block')).element(by.css('[formcontrolname="serialNumber"]'));
}

export function getTagField() {
    return element(by.css('.content-block')).element(by.css('[formcontrolname="tagNumber"]'));
}

export function getBarcodeField() {
    return element(by.css('.content-block')).element(by.css('[formcontrolname="barcode"]'));
}

export function getPurchaseDate() {
    return element(by.css('[formcontrolname="purchaseDate"]')).element(by.css('.hasDatepicker'));
}

export function getPurchasePrice() {
    return element(by.css('.content-block')).element(by.css('[formcontrolname="purchasePrice"]'));
}

export function getVendorDropdown() {
    return element(by.css('[formcontrolname="vendorId"]'));
}

export function getWarrantyVendor() {
    return element(by.css('[formcontrolname="warrantyVendorId"]'));
}

export function getWarrantyVendorSecond() {
    return element(by.css('.select2-results')).element(by.cssContainingText('li', 'Second Vendor'));
}

export function getServiceVendor() {
    return element(by.css('[formcontrolname="serviceVendorId"]'));
}

export function getServiceVendorSecond() {
    return element(by.css('.select2-results')).element(by.cssContainingText('li', 'Second Vendor'));
}

export function getSecondVendor() {
    return element(by.css('.select2-results')).element(by.cssContainingText('li', 'Second Vendor'));
}

export function getPortableCheckbox() {
    return element(by.cssContainingText('label', 'Portable'));
}

export function getInstalledCheckbox() {
    return element(by.cssContainingText('label', 'Installed'));
}

export function getOwnerCheckbox() {
    return element(by.cssContainingText('label', 'Owner'));
}

export function getOperatorCheckbox() {
    return element(by.cssContainingText('label', 'Operator'));
}

export function getVendorCheckbox() {
    return element(by.cssContainingText('label', 'Vendor'));
}

export function getExcellentCheckbox() {
    return element(by.cssContainingText('label', 'Excellent'));
}

export function getAcceptableCheckbox() {
    return element(by.cssContainingText('label', 'Acceptable'));
}

export function getSubStandardCheckbox() {
    return element(by.cssContainingText('label', 'Sub-Standard'));
}

export function getConditionStatus() {
    return element(by.css('[formcontrolname="condition"]'));
}

export function getCommentField() {
    return element(by.css('[formcontrolname="comment"]'));
}

export function getLocationDropdown() {
    return element(by.css('[formcontrolname="storageId"]'));
}

// export function getSecondStorage() {
//     return element(by.css('[formcontrolname="storageId"]'))
//           .element(by.css('.select2-results'))
//           .element(by.cssContainingText('li', 'Storage for inventory'));
// }

export function getOwnerDropdown() {
    return element(by.css('[formcontrolname="assetOwnerId"]'));
}

// export function getOwnerVendor() {
//     return element(by.css('[formcontrolname="assetOwnerId"]'))
//           .element(by.css('.select2-results'))
//           .element(by.cssContainingText('li', 'Second Vendor'));
// }

export function getWarranyDate() {
    return element(by.css('[formcontrolname="warrantyExpDate"]')).element(by.css('.hasDatepicker'));
}

export function getServiceDate() {
    return element(by.css('[formcontrolname="serviceExpDate"]')).element(by.css('.hasDatepicker'));
}

export function getMaintSchedule() {
    return element(by.css('[formcontrolname="schedule"]'));
}

export function getLastServiceDate() {
    return element(by.css('[formcontrolname="lastServiceDate"]')).element(by.css('.hasDatepicker'));
}

export function getNextServiceDate() {
    return element(by.css('[formcontrolname="nextServiceDate"]')).element(by.css('.hasDatepicker'));
}

export function getDocumentName() {
    return element(by.css('.popup-body')).element(by.css('[formcontrolname="name"]'));
}

export function getHeaderText() {
    return element(by.css('h2'));
}

export function getDocumentTable() {
    return element(by.css('tbody tr'));
}

export function getDocumentAdd() {
    return element(by.buttonText('Add'));
}
