import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';

export function navigateToService() {
    return browser.get('/u/service-charge');
}

export function navigateToTax() {
    return browser.get('/u/tax-charge');
}

export function getDescriptionField() {
    return element(by.css('[formcontrolname="name"]'));
}

export function getStartDate() {
    return element(by.css('[formcontrolname="startDate"]')).element(by.css('.hasDatepicker'));
}

export function getEndDate() {
    return element(by.css('[formcontrolname="endDate"]')).element(by.css('.hasDatepicker'));
}

export function getServiceCharge() {
    return element(by.css('[formcontrolname="charge"]'));
}

export function getStatusField() {
    return element(by.css('[formcontrolname="status"]'));
}

export function getOptionActive() {
    return element(by.css('[formcontrolname="status"]')).element(by.cssContainingText('option', 'Active'));
}

export function getOptionInactive() {
    return element(by.css('[formcontrolname="status"]')).element(by.cssContainingText('option', 'Inactive'));
}

export function getNextDate() {
    return element(by.cssContainingText('span', 'Next'));
}

export function getPrevDate() {
    return element(by.cssContainingText('span', 'Prev'));
}

export function getLastDate() {
    return element.all(by.css('.ui-state-default')).last();
}

export function getFirstDate() {
    return element.all(by.css('.ui-state-default')).first();
}
