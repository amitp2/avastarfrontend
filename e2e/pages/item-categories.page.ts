import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';

export function navigateToItem() {
    return browser.get('/u/category-management');
}

export function getShowSubCategory() {
      return element(by.css('.category-row .fa-angle-right'));
}

export function getShowItemCategory() {
      return element(by.css('.sub-category-row .fa-angle-right'));
}

export function getAddSubCategory() {
      return element(by.css('.category-row .fa-plus'));
}

export function getAddItemCategory() {
      return element(by.css('.sub-category-row .fa-plus'));
}

export function getEditCategory() {
      return element(by.css('.category-row .fa-pencil'));
}

export function getEditSubCategory() {
      return element(by.css('.sub-category-row .fa-pencil'));
}

export function getEditItemCategory() {
      return element(by.css('.sub-category-item-row .fa-pencil'));
}

export function getCategoryName() {
      return element(by.css('[formcontrolname="name"]'));
}
