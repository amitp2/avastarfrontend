import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';
export * from './common/contacts.page';

export function navigateToSubs() {
    return browser.get('/u/subscribers');
}

export function getSearchFieldUser() {
    return element(by.css('.popup-body [formcontrolname="search"]'));
}

export function getCompanyField() {
    return element(by.css('[formcontrolname="name"]'));
}

export function getAddressField() {
    return element(by.css('[formcontrolname="address"]'));
}

export function getAddressNdField() {
    return element(by.css('[formcontrolname="address2"]'));
}

export function getCityField() {
    return element(by.css('[formcontrolname="city"]'));
}

export function getPostCodeField() {
    return element(by.css('[formcontrolname="postCode"]'));
}

export function getCountrySelect() {
  return element(by.css('[formcontrolname="countryId"]'));
}

export function getStateSelect() {
    return element(by.css('[formcontrolname="stateId"]'));
}

export function getPhoneField() {
  return element(by.css('[formcontrolname="phone"]')).element(by.css('input'));
}

export function getFaxField() {
  return element(by.css('[formcontrolname="fax"]'));
}

export function getWebsiteField() {
  return element(by.css('[formcontrolname="website"]'));
}

export function getOfficeField() {
  return element(by.css('[formcontrolname="branch"]'));
}

export function getStatusToggle() {
  return element(by.css('.-checkbox'));
}

export function getBasicInfo() {
  return element(by.css('.nav-tabs')).all(by.tagName('li')).get(0);
}

export function getContactPerson() {
  return element(by.css('.popup-tabs-nav')).element(by.cssContainingText('a', 'Primary Contact'));
}

export function getUsersInfo() {
  return element(by.css('.popup-tabs-nav')).element(by.cssContainingText('a', 'Users'));
}

export function getAccessInfo() {
    return element(by.css('.popup-tabs-nav')).element(by.cssContainingText('a', 'Access'));
}

export function getUserEmail() {
  return element(by.css('[formcontrolname="username"]'));
}

export function getUserRoleField() {
    return element(by.css('.select2-selection__rendered'));
}

export function getUserRolePick() {
    return element(by.css('.select2-results')).element(by.cssContainingText('span', 'Admin'));
}

export function getTableUser() {
    return element(by.css('.popup-users-list')).element(by.css('tbody tr'));
}

export function getPlusUser() {
  return element(by.css('.popup-users-list')).element(by.css('.red-plus'));
}

export function getEditUser() {
  return element(by.css('.popup-users-list')).element(by.css('.fa-pencil'));
}

export function getDeleteUser() {
  return element(by.css('.popup-users-list')).element(by.css('.fa-trash'));
}

export function getHasAccessDropdown() {
    return element(by.cssContainingText('.popup-users-list-table td', 'Avastar'))
                     .getWebElement()
                     .getDriver()
                     .findElement(by.css('tbody tr td select'));
}

export function getNumberUsersField() {
    return element(by.cssContainingText('.popup-users-list-table td', 'Avastar'))
                     .getWebElement()
                     .getDriver()
                     .findElement(by.css('tbody tr td input'));
}

export function getFirstUserCheckbox() {
    return element(by.cssContainingText('.popup-users-list-table td', 'Jane Edited Doe Edited'))
          .getWebElement()
          .getDriver()
          .findElement(by.css('tbody tr td p input[type="checkbox"]'));
}

export function getSecondUserCheckbox() {
    return element(by.cssContainingText('.popup-users-list-table td', 'John Doe'))
          .getWebElement()
          .getDriver()
          .findElement(by.css('tbody tr td p input[type="checkbox"]'));
}
