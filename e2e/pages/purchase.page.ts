import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';

export function navigateToOrders() {
    return browser.get('/u/manage-purchase-orders');
}

export function getAddButton() {
    return element(by.css('popup.-visible')).element(by.buttonText('Add'));
}

export function getHeaderText() {
    return element(by.cssContainingText('.popup-body-header', 'Equipment Item'));
}

export function getVendorSelect() {
    return element(by.css('[formcontrolname="vendorId"]'));
}

export function getVendorContactSelect() {
    return element(by.css('[formcontrolname="contactId"]'));
}

export function getMonthSelect() {
    return element(by.css('[formcontrolname="month"]'));
}

export function getYearSelect() {
    return element(by.css('[formcontrolname="year"]'));
}

export function getStatusSelect() {
    return element(by.css('[formcontrolname="status"]'));
}

export function getTypeSelect() {
    return element(by.css('[formcontrolname="type"]'));
}

export function getAddItemButton() {
    return element(by.css('.complex-table-header .red-plus'));
}

export function getInventoryCategorySelect() {
    return element(by.css('[formcontrolname="categoryId"]'));
}

export function getInventorySubCategorySelect() {
    return element(by.css('[formcontrolname="subCategoryId"]'));
}

export function getInventorySelect() {
    return element(by.css('[formcontrolname="itemId"]'));
}

export function getArriveDate() {
    return element(by.css('[formcontrolname="arriveDate"] .hasDatepicker'));
}

export function getPickupDate() {
    return element(by.css('[formcontrolname="pickupDate"] .hasDatepicker'));
}

export function getRentalDays() {
    return element(by.css('[formcontrolname="rentalDays"]'));
}

export function getQuantityField() {
    return element(by.css('[formcontrolname="quantity"]'));
}

export function getUnitField() {
    return element(by.css('[formcontrolname="unitCost"]'));
}

export function getTotalCost() {
    return element(by.id('equipment-item-form')).element(by.css('.popup-body-right div:nth-child(3) input'));
}

export function getGuestCharge() {
    return element(by.css('[formcontrolname="guestCharge"]'));
}

export function getProfitCurrency() {
    return element(by.id('equipment-item-form')).element(by.css('.popup-body-right div:nth-child(5) input'));
}

export function getProfitPercent() {
    return element(by.id('equipment-item-form')).element(by.css('.popup-body-right div:nth-child(6) input'));
}
