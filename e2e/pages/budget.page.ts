import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';

export function navigateToBudget() {
    return browser.get('/u/budget-input');
}

export function getYearDropdown() {
    return element(by.css('[formcontrolname="year"]'));
}

export function getGroupsDropdown() {
    return element(by.css('[formcontrolname="category"]'));
}

export function getTableItem() {
    return element(by.css('.complex-table-body tr:first-child + tr'));
}

export function getAnnual() {
    return element(by.css('tr:first-child + tr td:nth-child(3)'));
}

export function getJanuary() {
    return element(by.css('tr:first-child + tr')).element(by.css('[formcontrolname="january"]'));
}

export function getFebruary() {
    return element(by.css('tr:first-child + tr')).element(by.css('[formcontrolname="february"]'));
}

export function getMarch() {
    return element(by.css('tr:first-child + tr')).element(by.css('[formcontrolname="march"]'));
}

export function getApril() {
    return element(by.css('tr:first-child + tr')).element(by.css('[formcontrolname="april"]'));
}

export function getMay() {
    return element(by.css('tr:first-child + tr')).element(by.css('[formcontrolname="may"]'));
}

export function getJune() {
    return element(by.css('tr:first-child + tr')).element(by.css('[formcontrolname="june"]'));
}

export function getJuly() {
    return element(by.css('tr:first-child + tr')).element(by.css('[formcontrolname="july"]'));
}

export function getAugust() {
    return element(by.css('tr:first-child + tr')).element(by.css('[formcontrolname="august"]'));
}

export function getSeptember() {
    return element(by.css('tr:first-child + tr')).element(by.css('[formcontrolname="september"]'));
}

export function getOctober() {
    return element(by.css('tr:first-child + tr')).element(by.css('[formcontrolname="october"]'));
}

export function getNovember() {
    return element(by.css('tr:first-child + tr')).element(by.css('[formcontrolname="november"]'));
}

export function getDecember() {
    return element(by.css('tr:first-child + tr')).element(by.css('[formcontrolname="december"]'));
}
