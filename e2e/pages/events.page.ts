import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';

export function navigateToAccounts() {
    return browser.get('/u/accounts');
}

export function getPopupSearchField() {
    return element(by.css('.popup-events-list')).element(by.css('[formcontrolname="search"]'));
}

export function getContactFirstName() {
    return element(by.id('popup-event-contact-form')).element(by.css('[formcontrolname="firstName"]'));
}

export function getContactLastName() {
    return element(by.id('popup-event-contact-form')).element(by.css('[formcontrolname="lastName"]'));
}

export function getContactPhone() {
    return element(by.id('popup-event-contact-form')).element(by.css('[formcontrolname="phone"]')).element(by.css('input'));
}

export function getContactEmail() {
    return element(by.id('popup-event-contact-form')).element(by.css('[formcontrolname="email"]'));
}

export function getContactDepartment() {
    return element(by.id('popup-event-contact-form')).element(by.css('[formcontrolname="department"]'));
}

export function getContactTitle() {
    return element(by.id('popup-event-contact-form')).element(by.css('[formcontrolname="title"]'));
}

export function getContactFax() {
    return element(by.id('popup-event-contact-form')).element(by.css('[formcontrolname="fax"]'));
}

export function getContactMobile() {
    return element(by.id('popup-event-contact-form')).element(by.css('[formcontrolname="mobile"]')).element(by.css('input'));
}

export function getContactNotes() {
    return element(by.id('popup-event-contact-form')).element(by.css('[formcontrolname="notes"]'));
}

export function getContactTable() {
    return element(by.css('app-event-contacts')).element(by.css('tbody tr'));
}

export function getContactPerson() {
  return element(by.css('.popup-tabs-nav')).element(by.cssContainingText('a', 'Primary Contact'));
}

export function getEventsInfo() {
  return element(by.css('.popup-tabs-nav')).element(by.cssContainingText('a', 'Events'));
}

export function getEventInfo() {
    return element(by.css('.popup-tabs-nav')).element(by.cssContainingText('a', 'Event'));
  }

export function getContactsInfo() {
    return element(by.css('.popup-tabs-nav')).element(by.cssContainingText('a', 'Contacts'));
}

export function getEventName() {
    return element(by.css('[formcontrolname="name"]'));
}

export function getEventCode() {
    return element(by.css('[formcontrolname="code"]'));
}

export function getEventMasterNo() {
    return element(by.css('[formcontrolname="masterAccountNumber"]'));
}

export function getEventDescription() {
    return element(by.css('[formcontrolname="eventDescription"]'));
}

export function getEventType() {
    return element(by.css('[formcontrolname="type"]'));
}

export function getEventStatus() {
    return element(by.css('[formcontrolname="status"]'));
}
export function getAddContact() {
    return element(by.css('.popup-body-header')).element(by.css('.red-plus'));
}

export function getEditContact() {
    return element(by.css('app-event-contacts')).element(by.css('.fa-pencil'));
}

export function getDeleteContact() {
    return element(by.css('app-event-contacts')).element(by.css('.fa-trash'));
}

export function getEventCancelButton() {
    return element(by.css('.popup-footer-buttons')).element(by.buttonText('Cancel'));
}

export function getEventTable() {
    return element(by.css('.popup-events-list-table')).element(by.css('tbody tr'));
}

export function getPlusEvent() {
  return element(by.css('.popup-events-list')).element(by.css('.red-plus'));
}

export function getEditEvent() {
  return element(by.css('.popup-events-list')).element(by.css('.fa-pencil'));
}

export function getDeleteEvent() {
  return element(by.css('.popup-events-list')).element(by.css('.fa-trash'));
}
