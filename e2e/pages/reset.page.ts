import { browser, by, element } from 'protractor';
export * from './common/attributes.page';

export function navigateToReset() {
    return browser.get('/reset/pw');
}

export function navigateToVerification() {
    return browser.get('/verification/pw');
}

export function getChangeButton() {
    return element(by.css('form [type="submit"]'));
}

export function getPasswordField() {
    return element(by.css('[formcontrolname="password"]'));
}

export function getPasswordConfirm() {
    return element(by.css('[formcontrolname="confirmPassword"]'));
}
