import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';
export * from './common/contacts.page';

export function navigateToAccounts() {
    return browser.get('/u/accounts');
}

export function getCompanyField() {
    return element(by.css('[formcontrolname="name"]'));
}

export function getAddressField() {
    return element(by.css('[formcontrolname="address"]'));
}

export function getAddressNdField() {
    return element(by.css('[formcontrolname="address2"]'));
}

export function getCityField() {
    return element(by.css('[formcontrolname="city"]'));
}

export function getPostCodeField() {
    return element(by.css('[formcontrolname="postCode"]'));
}

export function getCountrySelect() {
  return element(by.css('[formcontrolname="countryId"]'));
}

export function getStateSelect() {
    return element(by.css('[formcontrolname="stateId"]'));
}

export function getPhoneField() {
  return element(by.css('[formcontrolname="phone"]')).element(by.css('input'));
}

export function getFaxField() {
  return element(by.css('[formcontrolname="fax"]'));
}

export function getWebsiteField() {
  return element(by.css('[formcontrolname="website"]'));
}

export function getOfficeField() {
  return element(by.css('[formcontrolname="branch"]'));
}

export function getStatusToggle() {
  return element(by.css('.-checkbox'));
}

export function getMasterCheckbox() {
    return element(by.css('[formcontrolname="agreement"]'));
}

export function getStandardDiscount() {
    return element(by.css('[formcontrolname="standardDiscountRate"]'));
}

export function getSetupDiscount() {
    return element(by.css('[formcontrolname="discountRateForSetupDays"]'));
}

export function getAltChargeField() {
    return element(by.css('[formcontrolname="alternativeServiceChargePercentage"]'));
}

export function getPreDiscount() {
    return element(by.cssContainingText('label', 'Pre Discounted Pricing'));
}

export function getPostDiscount() {
    return element(by.cssContainingText('label', 'Post Discounted Pricing'));
}

export function getAltCharge() {
    return element(by.cssContainingText('label', 'Alternative Service Charge'));
}

export function getTaxExempt() {
    return element(by.cssContainingText('label', 'Tax Exempt'));
}

export function getBasicInfo() {
  return element(by.css('.nav-tabs')).all(by.tagName('li')).get(0);
}

export function getContactPerson() {
  return element(by.css('.popup-tabs-nav')).element(by.cssContainingText('a', 'Primary Contact'));
}

export function getEventsInfo() {
  return element(by.css('.popup-tabs-nav')).element(by.cssContainingText('a', 'Events'));
}
