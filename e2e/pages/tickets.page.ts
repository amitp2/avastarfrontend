import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';

export function navigateToTickets() {
    return browser.get('/u/tickets');
}

export function getAddCommentButton() {
    return element(by.css('app-ticket-comment-form .popup-footer-buttons button'));
}

export function getSystemNameFilter() {
    return element(by.css('[formcontrolname="systemName"]'));
}

export function getInventoryNameFilter() {
    return element(by.css('[formcontrolname="inventoryName"]'));
}

export function getTicketTitleFilter() {
    return element(by.css('[formcontrolname="search"]'));
}

export function getTicketNumberFilter() {
    return element(by.css('[formcontrolname="id"]'));
}

export function getTicketStatusFilter() {
    return element(by.css('[formcontrolname="ticketStatus"]'));
}

export function getInventoryTagFilter() {
    return element(by.css('[formcontrolname="tagNumber"]'));
}

export function getInventoryBarcodeFilter() {
    return element(by.css('[formcontrolname="barcode"]'));
}

export function getFromDateFilter() {
    return element(by.css('[formcontrolname="from"] .hasDatepicker'));
}

export function getNextDate() {
    return element(by.cssContainingText('span', 'Next'));
}

export function getPrevDate() {
    return element(by.cssContainingText('span', 'Prev'));
}

export function getLastDate() {
    return element.all(by.css('.ui-state-default')).last();
}

export function getFirstDate() {
    return element.all(by.css('.ui-state-default')).first();
}

export function getToDateFilter() {
    return element(by.css('[formcontrolname="to"] .hasDatepicker'));
}

export function getInventoryCheckbox() {
    return element(by.cssContainingText('span', 'Inventory'));
}

export function getSystemCheckbox() {
    return element(by.cssContainingText('label', 'System'));
}

export function getInventoryCategorySelect() {
    return element(by.css('[formcontrolname="inventoryCategoryId"]'));
}

export function getInventorySubCategorySelect() {
    return element(by.css('[formcontrolname="inventorySubCategoryId"]'));
}

export function getInventorySelect() {
    return element(by.css('[formcontrolname="inventoryId"]'));
}

export function getStorageLocation() {
    return element(by.css('app-storage-location div input'));
}

export function getSystemSearch() {
    return element(by.css('[formcontrolname="systemSearch"]'));
}

export function getSystemSelect() {
    return element(by.css('[formcontrolname="systemId"]'));
}

export function getTicketTitle() {
    return element(by.css('[formcontrolname="title"]'));
}

export function getTicketIssue() {
    return element(by.css('[formcontrolname="description"]'));
}

export function getTicketReminder() {
    return element(by.css('[formcontrolname="reminderTime"] .field-date'));
}

export function getInternalPersonnelCheckbox() {
    return element(by.cssContainingText('label', 'Internal Personnel'));
}

export function getVendorCheckbox() {
    return element(by.cssContainingText('span', 'Vendor'));
}

export function getVendorSelect() {
    return element(by.css('[formcontrolname="vendorId"]'));
}

export function getVendorContactSelect() {
    return element(by.css('[formcontrolname="vendorContactId"]'));
}

export function getVenueContactSelect() {
    return element(by.css('[formcontrolname="venueContactId"]'));
}

export function getTicketStatus() {
    return element(by.css('[formcontrolname="ticketStatus"]'));
}

export function getTicketComment() {
    return element(by.css('app-ticket-comment-form .form-field [formcontrolname="description"]'));
}
