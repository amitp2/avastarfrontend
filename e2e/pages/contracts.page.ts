import { browser, by, element } from 'protractor';
export * from './common/attributes.page';
export * from './common/buttons.page';

export function navigateToContracts() {
    return browser.get('/u/contracts');
}

export function getViewButton() {
    return element(by.cssContainingText('a', 'View Document'));
}

export function getVendorSelect() {
    return element(by.css('[formcontrolname="vendorId"]'));
}

export function getVendorSelectAccess() {
    return element(by.css('[formcontrolname="vendorId"] div'));
}

export function getContactSelect() {
    return element(by.css('[formcontrolname="vendorContactId"]'));
}

export function getWithoutDiscountField() {
    return element(by.css('tbody tr:nth-child(2) [formcontrolname="withoutDiscount"]'));
}

export function getDiscountTo10Field() {
    return element(by.css('tbody tr:nth-child(2) [formcontrolname="discountTo10"]'));
}

export function getDiscountTo20Field() {
    return element(by.css('tbody tr:nth-child(2) [formcontrolname="discountTo20"]'));
}

export function getDiscountTo30Field() {
    return element(by.css('tbody tr:nth-child(2) [formcontrolname="discountTo30"]'));
}

export function getDiscountTo40Field() {
    return element(by.css('tbody tr:nth-child(2) [formcontrolname="discountTo40"]'));
}

export function getDiscountTo50Field() {
    return element(by.css('tbody tr:nth-child(2) [formcontrolname="discountTo50"]'));
}

export function getDiscountFrom50Field() {
    return element(by.css('tbody tr:nth-child(2) [formcontrolname="discountFrom50"]'));
}
