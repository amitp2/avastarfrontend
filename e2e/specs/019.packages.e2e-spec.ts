import * as page from '../pages/packages.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Packages', () => {

    let originalTimeout;

    const EC = protractor.ExpectedConditions;
    const addButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getPackageAddButton()));
    const addButtonIsClickable = EC.elementToBeClickable(page.getPackageAddButton());
    const updateButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getUpdateButton()));
    const updateButtonIsClickable = EC.elementToBeClickable(page.getUpdateButton());
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));

    beforeEach(() => {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
    });

    afterEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    it('login with organization admin', () => {
        page.loginUserPass('protractoradmn@mailinator.com', '12345678');
    });

    it('Add Package', () => {

        const path = require('path');
        const fileToUpload = '../../src/assets/images/logo-hilton.png',
        absolutePath = path.resolve(__dirname, fileToUpload);

        page.navigateToPackages();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.edit-package'))), 5000);
        page.getUploadLogo().sendKeys(absolutePath);
        page.checkToastMessage('Package logo uploaded successfully');
        browser.wait(addButtonIsNotClickable, 5000);
        page.getStartDate().sendKeys('2018-02-01');
        page.getEndDate().sendKeys('2018-02-28');
        page.getPackageNameField().click();
        page.getPackageNameField().sendKeys('Package Name');
        page.getPackageSalesField().sendKeys('Package Sales Description');
        page.getActualPriceField().sendKeys('1000');
        browser.wait(addButtonIsClickable, 5000);
        page.getCategoryDropdown().click();
        page.selectFromDropdown('Presentation Support');
        browser.sleep(2000);
        page.getSubCategoryDropdown().click();
        page.selectFromDropdown('Markers & Easels');
        page.getSearchField().sendKeys('Flipchart, Easel');
        page.getItemSearchButton().click();
        browser.wait(spinnerIsNotVisible, 5000);
        page.getOptionLeftEquipment1().click();
        page.getItemAddButton().click();
        browser.sleep(1500);
        page.getOptionRightItem1Quantity().sendKeys('1');
        browser.sleep(2000);
        expect(page.getCalculatedPriceField().getText()).toEqual('100.00');
        page.getPackageAddButton().click();
        page.checkToastMessage('Package added successfully');
        expect(page.getHistoryItem().getText()).toContain('1,000.00 Feb 1, 2018 Feb 28, 2018');
        page.getPackageCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getSearchField().sendKeys('Package Name');
        page.getMinPriceFilter().sendKeys('50');
        page.getMaxPriceFilter().sendKeys('400');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getMaxPriceFilter().clear();
        page.getMaxPriceFilter().sendKeys('1200');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getTableItem().getText()).toEqual('Package Name Package Workflow Description Package Sales Description 1,000.00');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.edit-package'))), 5000);
        expect(page.getPackageNameField().getAttribute('value')).toEqual('Package Name');
        expect(page.getPackageSalesField().getAttribute('value')).toEqual('Package Sales Description');
        expect(page.getActualPriceField().getAttribute('value')).toEqual('1000');
        expect(page.getStartDate().getAttribute('value')).toEqual('2018-02-01');
        expect(page.getEndDate().getAttribute('value')).toEqual('2018-02-28');
     });

    it('Edit Package', () => {

        page.navigateToPackages();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getSearchField().sendKeys('Package Name');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.edit-package'))), 5000);
        browser.wait(updateButtonIsNotClickable, 5000);
        page.getStartDate().clear();
        page.getStartDate().sendKeys('2018-03-01');
        page.getEndDate().clear();
        page.getEndDate().sendKeys('2018-03-31');
        page.getPackageNameField().click();
        page.getPackageNameField().sendKeys(' Edited');
        page.getPackageSalesField().sendKeys(' Edited');
        page.getActualPriceField().clear();
        page.getActualPriceField().sendKeys('1500');
        browser.wait(addButtonIsClickable, 5000);
        page.getCategoryDropdown().click();
        page.selectFromDropdown('Presentation Support');
        browser.sleep(2000);
        page.getSubCategoryDropdown().click();
        page.selectFromDropdown('Markers & Easels');
        page.getSearchField().sendKeys('Flipchart, Marker');
        page.getItemSearchButton().click();
        browser.wait(spinnerIsNotVisible, 5000);
        page.getOptionLeftEquipment2().click();
        page.getItemAddButton().click();
        browser.sleep(1500);
        page.getOptionRightItem2Quantity().sendKeys('1');
        browser.sleep(2000);
        expect(page.getCalculatedPriceField().getAttribute('value')).toEqual('200.00');
        page.getPackageAddButton().click();
        page.checkToastMessage('Package updated successfully');
        expect(page.getHistoryItem().getText()).toContain('1500 2018-03-01 2018-03-31');
        page.getPackageCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getSearchField().sendKeys('Package Name Edited');
        page.getMinPriceFilter().sendKeys('200');
        page.getMaxPriceFilter().sendKeys('400');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getMaxPriceFilter().clear();
        page.getMaxPriceFilter().sendKeys('1600');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getTableItem()
                   .getText())
                   .toEqual('Package Name Edited Package Workflow Description Edited Package Sales Description Edited 1500');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.edit-package'))), 5000);
        expect(page.getPackageNameField().getAttribute('value')).toEqual('Package Name Edited');
        expect(page.getPackageSalesField().getAttribute('value')).toEqual('Package Sales Description Edited');
        expect(page.getActualPriceField().getAttribute('value')).toEqual('1500');
        expect(page.getStartDate().getAttribute('value')).toEqual('2018-03-01');
        expect(page.getEndDate().getAttribute('value')).toEqual('2018-03-31');
    });

    it('Copy Package', () => {

        page.navigateToPackages();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getSearchField().sendKeys('Package Name');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.edit-package'))), 5000);
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateCopyButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.edit-package'))), 5000);
        browser.wait(addButtonIsClickable, 5000);
        expect(page.getCalculatedPriceField().getAttribute('value')).toEqual('100.00');
        expect(page.getPackageNameField().getAttribute('value')).toEqual('Copy of Package Name Edited 1');
        expect(page.getPackageSalesField().getAttribute('value')).toEqual('Package Sales Description Edited');
        expect(page.getActualPriceField().getAttribute('value')).toEqual('1,500.00');
        expect(page.getStartDate().getAttribute('value')).toEqual('2018-03-01');
        expect(page.getEndDate().getAttribute('value')).toEqual('2018-03-31');
        page.getPackageNameField().sendKeys(' Copied');
        page.getPackageAddButton().click();
        page.checkToastMessage('Package added successfully');
        page.getPackageCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getSearchField().sendKeys('Copy of Package Name Edited 1');
        page.getMinPriceFilter().sendKeys('200');
        page.getMaxPriceFilter().sendKeys('400');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getMaxPriceFilter().clear();
        page.getMaxPriceFilter().sendKeys('1600');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getTableItem()
                   .getText())
                   .toEqual('Package Name Edited Copied Package Description Edited Package Sales Description Edited 1500');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.edit-package'))), 5000);
        expect(page.getCalculatedPriceField().getAttribute('value')).toEqual('600.00');
        expect(page.getPackageNameField().getAttribute('value')).toEqual('Copy of Package Name Edited 1');
        expect(page.getPackageSalesField().getAttribute('value')).toEqual('Package Sales Description Edited');
        expect(page.getActualPriceField().getAttribute('value')).toEqual('1,500.00');
        expect(page.getStartDate().getAttribute('value')).toEqual('2018-03-01');
        expect(page.getEndDate().getAttribute('value')).toEqual('2018-03-31');
    });

    it('Delete System', () => {

        page.navigateToPackages();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getSearchField().sendKeys('Package Name Edited');
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
        page.checkToastMessage('Package contains equipment or services');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.edit-package'))), 5000);
        page.getOptionRightItem1().click();
        // page.getOptionRightItem2().click();
        page.getItemRemoveButton().click();
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Package updated successfully');
        page.getPackageCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getSearchField().sendKeys('Package Name Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
        page.getSearchField().clear();
        page.getSearchField().sendKeys('Copy of Package Name Edited 1');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.edit-package'))), 5000);
        page.getOptionRightItem1().click();
        // page.getOptionRightItem2().click();
        // page.getOptionRightItem3().click();
        // page.getOptionRightItem4().click();
        page.getItemRemoveButton().click();
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Package updated successfully');
        page.getPackageCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getSearchField().sendKeys('Copy of Package Name Edited 1');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
    });

});
