import * as page from '../pages/property.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Properties', () => {

    const EC = protractor.ExpectedConditions;
    const addButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getAddButton()));
    const addButtonIsClickable = EC.elementToBeClickable(page.getAddButton());
    const updateButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getUpdateButton()));
    const updateButtonIsClickable = EC.elementToBeClickable(page.getUpdateButton());
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));
    const contactUpdateButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getContactUpdate()));
    const contactUpdateButtonIsClickable = EC.elementToBeClickable(page.getContactUpdate());

    const path = require('path');
    const fileToUpload = '../../src/assets/images/logo-hilton.png',
    absolutePath = path.resolve(__dirname, fileToUpload);

    let originalTimeout;

    beforeEach(() => {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
    });

    afterEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    it('login with super admin', () => {
        page.loginUserPass('pgagnidze@productsavvy.com', '12345678');
    });

    it('add new property', () => {

        page.navigateToProps();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        browser.wait(addButtonIsNotClickable, 5000);
        page.getUploadLogo().sendKeys(absolutePath);
        page.checkToastMessage('Property logo uploaded successfully');
        page.getPropertyName().sendKeys('01AOrgProp');
        page.getAddressField().sendKeys('2717 Clearview Drive');
        page.getCityField().sendKeys('Atlanta');
        page.getPostCodeField().sendKeys('30303');
        page.getCountrySelect().click();
        page.selectFromDropdown('United States');
        browser.sleep(1500);
        page.getStateSelect().click();
        page.selectFromDropdown('Georgia');
        page.getSubscriberDropdown().click();
        page.selectFromDropdown('Protractor Login Inactive Subscriber');
        page.getPropertyOwner().sendKeys('Owner');
        page.getPropertyOperator().sendKeys('Operator');
        expect(page.getPropertyContent().getText()).toEqual('01AOrgProp\n2717 Clearview Drive\nGA 30303');
        browser.wait(addButtonIsClickable, 5000);
        page.getAddButton().click();
        page.checkToastMessage('Property created successfully');
        browser.wait(updateButtonIsNotClickable, 5000);
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrgProp');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getTableItem().getText()).toEqual('01AOrgProp');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        expect(page.getPropertyName().getAttribute('value')).toEqual('01AOrgProp');
        expect(page.getAddressField().getAttribute('value')).toEqual('2717 Clearview Drive');
        expect(page.getCityField().getAttribute('value')).toEqual('Atlanta');
        expect(page.getPostCodeField().getAttribute('value')).toEqual('30303');
        expect(page.getCountrySelect().element(by.css('option:checked')).getText()).toEqual('United States');
        expect(page.getSubscriberDropdownAccess().getAttribute('class')).toBe('inaccessible');
        expect(page.getSubscriberDropdown().element(by.css('option:checked')).getText()).toEqual('Protractor Login Inactive Subscriber');
        expect(page.getStateSelect().element(by.css('option:checked')).getText()).toEqual('Georgia');
        expect(page.getPropertyOwner().getAttribute('value')).toEqual('Owner');
        expect(page.getPropertyOperator().getAttribute('value')).toEqual('Operator');
    });

    it('edit added property, add default settings', () => {

        page.navigateToProps();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrgProp');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        page.getPropertyName().sendKeys(' Edited');
        page.getAddressField().sendKeys(' Edited');
        page.getCityField().sendKeys(' Edited');
        page.getPostCodeField().sendKeys('03');
        page.getCountrySelect().click();
        page.selectFromDropdown('Germany');
        page.getPropertyOwner().sendKeys(' Edited');
        page.getPropertyOperator().sendKeys(' Edited');
        page.getAddressNdField().sendKeys('3788 Despard Street');
        page.getPhoneMain().sendKeys('133-713-3713');
        page.getFaxMain().sendKeys('MainFax');
        page.getWebsiteField().sendKeys('www.01aprop.org');
        expect(page.getPropertyContent()
                   .getText())
                   .toEqual('01AOrgProp Edited\n2717 Clearview Drive Edited\n3788 Despard Street, GA 3030303');
        browser.wait(updateButtonIsNotClickable, 5000);
        page.getSettingsCurrency().sendKeys('Euro');
        page.getSettingsBilling().sendKeys('Daily');
        page.getEventType().sendKeys('Group');
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Property updated successfully');
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrgProp Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getTableItem().getText()).toEqual('01AOrgProp Edited');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        expect(page.getPropertyName().getAttribute('value')).toEqual('01AOrgProp Edited');
        expect(page.getAddressField().getAttribute('value')).toEqual('2717 Clearview Drive Edited');
        expect(page.getAddressNdField().getAttribute('value')).toEqual('3788 Despard Street');
        expect(page.getCityField().getAttribute('value')).toEqual('Atlanta Edited');
        expect(page.getPostCodeField().getAttribute('value')).toEqual('3030303');
        expect(page.getCountrySelect().element(by.css('option:checked')).getText()).toEqual('Germany');
        expect(page.getPropertyOwner().getAttribute('value')).toEqual('Owner Edited');
        expect(page.getPropertyOperator().getAttribute('value')).toEqual('Operator Edited');
        expect(page.getPhoneMain().getAttribute('value')).toEqual('133-713-3713');
        expect(page.getFaxMain().getAttribute('value')).toEqual('MainFax');
        expect(page.getWebsiteField().getAttribute('value')).toEqual('www.01aprop.org');
        expect(page.getSettingsBilling().element(by.css('option:checked')).getText()).toEqual('Daily');
        expect(page.getSettingsCurrency().element(by.css('option:checked')).getText()).toEqual('Euro');
        expect(page.getEventType().element(by.css('option:checked')).getText()).toEqual('Group');
        expect(page.getSubscriberDropdownAccess().getAttribute('class')).toBe('inaccessible');
        expect(page.getSubscriberDropdown().element(by.css('option:checked')).getText()).toEqual('Protractor Login Inactive Subscriber');
    });

    it('edit default settings', () => {

        page.navigateToProps();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrgProp');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        browser.wait(updateButtonIsClickable, 5000);
        page.getSettingsBilling().sendKeys('Event');
        page.getSettingsCurrency().sendKeys('US Dollar');
        page.getEventType().sendKeys('Local');
        page.getTEONo().sendKeys('TEO');
        page.getTEOStartNo().sendKeys('10');
        page.getPONo().sendKeys('PO');
        page.getPOStartNo().sendKeys('10');
        page.getSettingsReport().sendKeys('reportDisclaimer');
        page.getSettingsEmail().sendKeys('emailDisclaimer');
        page.getUpdateButton().click();
        page.checkToastMessage('Property updated successfully');
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrgProp');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        expect(page.getSettingsBilling().element(by.css('option:checked')).getText()).toEqual('Event');
        expect(page.getSettingsCurrency().element(by.css('option:checked')).getText()).toEqual('US Dollar');
        expect(page.getEventType().element(by.css('option:checked')).getText()).toEqual('Local');
        expect(page.getTEONo().getAttribute('value')).toEqual('TEO');
        expect(page.getTEOStartNo().getAttribute('value')).toEqual('10');
        expect(page.getPONo().getAttribute('value')).toEqual('PO');
        expect(page.getPOStartNo().getAttribute('value')).toEqual('10');
        expect(page.getSettingsReport().getAttribute('value')).toEqual('reportDisclaimer');
        expect(page.getSettingsEmail().getAttribute('value')).toEqual('emailDisclaimer');
    });

    it('add primary contact', () => {

        page.navigateToProps();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrgProp');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getContactFname().sendKeys('John');
        page.getContactLname().sendKeys('Doe');
        page.getContactPhone().sendKeys('404-555-0176');
        page.getContactRole().sendKeys('None');
        page.getContactEmail().sendKeys('john@ doe');
        browser.wait(updateButtonIsNotClickable, 5000);
        page.getContactEmail().clear();
        page.getContactEmail().sendKeys('john@doe');
        browser.wait(updateButtonIsClickable, 5000);
        page.getContactUpdate().click();
        page.checkToastMessage('Venue contact created successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        expect(page.getTableUser().getText()).toContain('John 404-555-0176 john@doe');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getContactFname().getAttribute('value')).toEqual('John');
        expect(page.getContactLname().getAttribute('value')).toEqual('Doe');
        expect(page.getContactPhone().getAttribute('value')).toEqual('404-555-0176');
        expect(page.getContactEmail().getAttribute('value')).toEqual('john@doe');
        expect(page.getContactRole().element(by.css('option:checked')).getText()).toEqual('None');
    });

    it('edit primary contact', () => {

        page.navigateToProps();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrgProp');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getContactFname().sendKeys(' Edited');
        page.getContactLname().sendKeys(' Edited');
        page.getContactPhone().sendKeys(' Edited');
        page.getContactEmail().sendKeys(' Edited');
        page.getContactDepartment().sendKeys('Department');
        page.getContactTitle().sendKeys('Title');
        page.getContactFax().sendKeys('Fax');
        page.getContactMobile().sendKeys('133-713-3713');
        page.getContactNote().sendKeys('Note');
        // page.getContactRole().sendKeys('Admin');
        browser.wait(contactUpdateButtonIsNotClickable, 5000);
        page.getContactEmail().clear();
        page.getContactEmail().sendKeys('john@doeEdited');
        browser.wait(contactUpdateButtonIsClickable, 5000);
        page.getContactUpdate().click();
        page.checkToastMessage('Venue contact updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        expect(page.getTableUser().getText()).toContain('John Edited 404-555-0176 john@doeEdited Department');
        page.getContactOwner().click();
        browser.wait(spinnerIsNotVisible, 5000);
        page.getContactOperator().click();
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getContactFname().getAttribute('value')).toEqual('John Edited');
        expect(page.getContactLname().getAttribute('value')).toEqual('Doe Edited');
        expect(page.getContactPhone().getAttribute('value')).toEqual('404-555-0176');
        expect(page.getContactEmail().getAttribute('value')).toEqual('john@doeEdited');
        expect(page.getContactDepartment().getAttribute('value')).toEqual('Department');
        expect(page.getContactTitle().getAttribute('value')).toEqual('Title');
        expect(page.getContactFax().getAttribute('value')).toEqual('Fax');
        expect(page.getContactMobile().getAttribute('value')).toEqual('133-713-3713');
        expect(page.getContactNote().getAttribute('value')).toEqual('Note');
        // expect(page.getContactRole().element(by.css('option:checked')).getText()).toEqual('Admin');
    });

    it('delete property and property contact', () => {

        page.navigateToProps();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrgProp Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrgProp Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
    });

});
