import * as page from '../pages/vendor.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Vendors', () => {

    let originalTimeout;

    const EC = protractor.ExpectedConditions;
    const addButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getAddButton()));
    const addButtonIsClickable = EC.elementToBeClickable(page.getAddButton());
    const updateButtonIsClickable = EC.elementToBeClickable(page.getUpdateButton());
    const updateButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getUpdateButton()));
    const contactUpdateButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getContactUpdate()));
    const contactUpdateButtonIsClickable = EC.elementToBeClickable(page.getContactUpdate());
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));

    beforeEach(() => {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
    });

    afterEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    it('login with organization admin', () => {
        page.loginUserPass('protractoradmn@mailinator.com', '12345678');
    });

    it('add vendor', () => {

        page.navigateToVendors();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        browser.wait(addButtonIsNotClickable, 5000);
        page.getVendorName().sendKeys('Vendor Name');
        page.getVendorAddress().sendKeys('Address');
        page.getVendorCity().sendKeys('City');
        page.getVendorCountry().click();
        page.selectFromDropdown('United States');
        browser.sleep(1500);
        page.getVendorState().click();
        page.selectFromDropdown('Georgia');
        page.getVendorPostCode().sendKeys('postCode');
        browser.wait(addButtonIsClickable, 5000);
        page.getAddButton().click();
        page.checkToastMessage('Vendor created successfully');
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('Vendor Name');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getTableItem().getText()).toEqual('Vendor Name');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        expect(page.getVendorName().getAttribute('value')).toEqual('Vendor Name');
        expect(page.getVendorAddress().getAttribute('value')).toEqual('Address');
        expect(page.getVendorCity().getAttribute('value')).toEqual('City');
        expect(page.getVendorCountry().element(by.css('option:checked')).getText()).toEqual('United States');
        expect(page.getVendorState().element(by.css('option:checked')).getText()).toEqual('Georgia');
        expect(page.getVendorPostCode().getAttribute('value')).toEqual('postCode');
    });

    it('edit vendor', () => {

        page.navigateToVendors();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('Vendor Name');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        browser.wait(updateButtonIsClickable, 5000);
        page.getVendorName().sendKeys(' Edited');
        page.getVendorAddress().sendKeys(' Edited');
        page.getVendorAddress2().sendKeys('Address 2');
        page.getVendorCity().sendKeys(' Edited');
        page.getVendorCountry().click();
        page.selectFromDropdown('Germany');
        page.getVendorPostCode().sendKeys('up');
        page.getVendorPhone().sendKeys('133-713-3713');
        page.getVendorFax().sendKeys('Fax');
        page.getVendorWebsite().sendKeys('www.vendor.com');
        page.getUpdateButton().click();
        page.checkToastMessage('Vendor updated successfully');
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('Vendor Name Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getTableItem().getText()).toEqual('Vendor Name Edited 133-713-3713 Fax');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        expect(page.getVendorName().getAttribute('value')).toEqual('Vendor Name Edited');
        expect(page.getVendorAddress().getAttribute('value')).toEqual('Address Edited');
        expect(page.getVendorAddress2().getAttribute('value')).toEqual('Address 2');
        expect(page.getVendorCity().getAttribute('value')).toEqual('City Edited');
        expect(page.getVendorCountry().element(by.css('option:checked')).getText()).toEqual('Germany');
        expect(page.getVendorPostCode().getAttribute('value')).toEqual('postCodeup');
        expect(page.getVendorPhone().getAttribute('value')).toEqual('133-713-3713');
        expect(page.getVendorFax().getAttribute('value')).toEqual('Fax');
        expect(page.getVendorWebsite().getAttribute('value')).toEqual('www.vendor.com');
    });

    it('add primary contact', () => {

        page.navigateToVendors();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('Vendor Name Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getContactFname().sendKeys('John');
        page.getContactLname().sendKeys('Doe');
        page.getContactPhone().sendKeys('404-555-0176');
        page.getContactRole().sendKeys('None');
        page.getContactEmail().sendKeys('john@ doe');
        browser.wait(contactUpdateButtonIsNotClickable, 5000);
        page.getContactEmail().clear();
        page.getContactEmail().sendKeys('john@doe');
        browser.wait(contactUpdateButtonIsClickable, 5000);
        page.getContactUpdate().click();
        page.checkToastMessage('Vendor Contact created successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        expect(page.getUserTable().getText()).toEqual('John Doe 404-555-0176 john@doe');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getContactFname().getAttribute('value')).toEqual('John');
        expect(page.getContactLname().getAttribute('value')).toEqual('Doe');
        expect(page.getContactPhone().getAttribute('value')).toEqual('404-555-0176');
        expect(page.getContactEmail().getAttribute('value')).toEqual('john@doe');
        expect(page.getContactRole().element(by.css('option:checked')).getText()).toEqual('None');
    });

    it('edit primary contact', () => {

        page.navigateToVendors();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('Vendor Name Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getContactFname().sendKeys(' Edited');
        page.getContactLname().sendKeys(' Edited');
        page.getContactPhone().sendKeys(' Edited');
        page.getContactEmail().sendKeys(' Edited');
        page.getContactDepartment().sendKeys('Department');
        page.getContactTitle().sendKeys('Title');
        page.getContactFax().sendKeys('Fax');
        page.getContactMobile().sendKeys('133-713-3713');
        page.getContactNote().sendKeys('Note');
        // page.getContactRole().sendKeys('Admin');
        browser.wait(contactUpdateButtonIsNotClickable, 5000);
        page.getContactEmail().clear();
        page.getContactEmail().sendKeys('john@doeEdited');
        browser.wait(contactUpdateButtonIsClickable, 5000);
        page.getContactUpdate().click();
        page.checkToastMessage('Vendor Contact updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        expect(page.getUserTable().getText()).toEqual('John Edited Doe Edited 404-555-0176 john@doeEdited Department');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getContactFname().getAttribute('value')).toEqual('John Edited');
        expect(page.getContactLname().getAttribute('value')).toEqual('Doe Edited');
        expect(page.getContactPhone().getAttribute('value')).toEqual('404-555-0176');
        expect(page.getContactEmail().getAttribute('value')).toEqual('john@doeEdited');
        expect(page.getContactDepartment().getAttribute('value')).toEqual('Department');
        expect(page.getContactTitle().getAttribute('value')).toEqual('Title');
        expect(page.getContactFax().getAttribute('value')).toEqual('Fax');
        expect(page.getContactMobile().getAttribute('value')).toEqual('133-713-3713');
        expect(page.getContactNote().getAttribute('value')).toEqual('Note');
        // expect(page.getContactRole().element(by.css('option:checked')).getText()).toEqual('Admin');
    });

    it('delete primary contact and vendor', () => {

        page.navigateToVendors();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('Vendor Name Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property'))), 5000);
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('Vendor Name Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
    });

});
