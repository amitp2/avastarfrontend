import * as page from '../pages/events.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Subscribers', () => {

    let originalTimeout;

    const EC = protractor.ExpectedConditions;
    const addButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getAddButton()));
    const addButtonIsClickable = EC.elementToBeClickable(page.getAddButton());
    const updateButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getUpdateButton()));
    const updateButtonIsClickable = EC.elementToBeClickable(page.getUpdateButton());
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));

    beforeEach(() => {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
    });

    afterEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    it('login with organization admin', () => {
        page.loginUserPass('protractoradmn@mailinator.com', '12345678');
    });

    it('add new event', () => {

        page.navigateToAccounts();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('Account for Event');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getAddNewFa().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        browser.wait(addButtonIsNotClickable, 5000);
        page.getEventName().sendKeys('Event Name');
        browser.wait(addButtonIsClickable, 5000);
        page.getAddButton().click();
        page.checkToastMessage('Event added successfully');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEventCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-events-list'))), 5000);
        page.getPopupSearchField().sendKeys('Event Name');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getEventTable().getText()).toEqual('Event Name Definite');
        page.getEditEvent().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getEventName().getAttribute('value')).toEqual('Event Name');
        expect(page.getEventType().element(by.css('option:checked')).getText()).toEqual('Group');
        expect(page.getEventStatus().element(by.css('option:checked')).getText()).toEqual('Definite');
    });

    it('edit new event', () => {

        page.navigateToAccounts();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('Account for Event');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getEventsInfo().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-events-list'))), 5000);
        page.getPopupSearchField().sendKeys('Event Name');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditEvent().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        browser.wait(updateButtonIsClickable, 5000);
        page.getEventName().sendKeys(' Edited');
        page.getEventCode().sendKeys('1337');
        page.getEventMasterNo().sendKeys('1337331');
        page.getEventDescription().sendKeys('Event Description');
        page.getEventType().sendKeys('Internal');
        page.getEventStatus().sendKeys('Tentative');
        page.getUpdateButton().click();
        page.checkToastMessage('Event updated successfully');
        // page.getEventCancel().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-events-list'))), 5000);
        page.getPopupSearchField().sendKeys('Event Name Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getEventTable().getText()).toEqual('Event Name Edited 1337331 Tentative');
        page.getEditEvent().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getEventName().getAttribute('value')).toEqual('Event Name Edited');
        expect(page.getEventCode().getAttribute('value')).toEqual('1337');
        expect(page.getEventMasterNo().getAttribute('value')).toEqual('1337331');
        expect(page.getEventDescription().getAttribute('value')).toEqual('Event Description');
        expect(page.getEventType().element(by.css('option:checked')).getText()).toEqual('Internal');
        expect(page.getEventStatus().element(by.css('option:checked')).getText()).toEqual('Tentative');
    });

    it('add contact person', () => {

        page.navigateToAccounts();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('Account for Event');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getEventsInfo().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-events-list'))), 5000);
        page.getPopupSearchField().sendKeys('Event Name Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditEvent().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getContactsInfo().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getAddContact().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getContactFirstName().sendKeys('John');
        page.getContactLastName().sendKeys('Doe');
        page.getContactPhone().sendKeys('404-555-0176');
        page.getContactEmail().sendKeys('john@ doe');
        browser.wait(addButtonIsNotClickable, 5000);
        page.getContactEmail().clear();
        page.getContactEmail().sendKeys('john@doe');
        browser.wait(addButtonIsClickable, 5000);
        page.getAddButton().click();
        page.checkToastMessage('Event contact added successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getPopupSearchField().sendKeys('John Doe');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getContactTable().getText()).toEqual('John Doe 404-555-0176 john@doe');
        page.getEditContact().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getContactFirstName().getAttribute('value')).toEqual('John');
        expect(page.getContactLastName().getAttribute('value')).toEqual('Doe');
        expect(page.getContactPhone().getAttribute('value')).toEqual('404-555-0176');
        expect(page.getContactEmail().getAttribute('value')).toEqual('john@doe');
    });

    it('edit contact person', () => {

        page.navigateToAccounts();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('Account for Event');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getEventsInfo().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-events-list'))), 5000);
        page.getPopupSearchField().sendKeys('Event Name Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditEvent().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getContactsInfo().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getPopupSearchField().sendKeys('John Doe');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditContact().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getContactFirstName().sendKeys(' Edited');
        page.getContactLastName().sendKeys(' Edited');
        page.getContactPhone().sendKeys(' Edited');
        page.getContactEmail().sendKeys(' Edited');
        page.getContactDepartment().sendKeys('Department');
        page.getContactTitle().sendKeys('Title');
        page.getContactFax().sendKeys('Fax');
        page.getContactMobile().sendKeys('133-713-3713');
        page.getContactNotes().sendKeys('Note');
        browser.wait(updateButtonIsNotClickable, 5000);
        page.getContactEmail().clear();
        page.getContactEmail().sendKeys('john@doeEdited');
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Event contact updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getPopupSearchField().sendKeys('John Edited Doe Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getContactTable().getText()).toEqual('John Edited Doe Edited 404-555-0176 john@doeEdited Department');
        page.getEditContact().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getContactFirstName().getAttribute('value')).toEqual('John Edited');
        expect(page.getContactLastName().getAttribute('value')).toEqual('Doe Edited');
        expect(page.getContactPhone().getAttribute('value')).toEqual('404-555-0176');
        expect(page.getContactEmail().getAttribute('value')).toEqual('john@doeEdited');
        expect(page.getContactDepartment().getAttribute('value')).toEqual('Department');
        expect(page.getContactTitle().getAttribute('value')).toEqual('Title');
        expect(page.getContactFax().getAttribute('value')).toEqual('Fax');
        expect(page.getContactMobile().getAttribute('value')).toEqual('133-713-3713');
        expect(page.getContactNotes().getAttribute('value')).toEqual('Note');
    });

    it('delete added event and contact', () => {
        const EC = protractor.ExpectedConditions;

        page.navigateToAccounts();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('Account for Event');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getEventsInfo().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-events-list'))), 5000);
        page.getPopupSearchField().sendKeys('Event Name Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditEvent().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getContactsInfo().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getPopupSearchField().sendKeys('John Edited Doe Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getDeleteContact().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
        page.getEventInfo().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getEventCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-events-list'))), 5000);
        page.getPopupSearchField().sendKeys('Event Name Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getDeleteEvent().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
    });

});
