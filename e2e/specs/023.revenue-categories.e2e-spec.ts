import * as page from '../pages/revenue-categories.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Users', () => {

    let originalTimeout;

    const EC = protractor.ExpectedConditions;
    const addButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getAddButton()));
    const addButtonIsClickable = EC.elementToBeClickable(page.getAddButton());
    const updateButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getUpdateButton()));
    const updateButtonIsClickable = EC.elementToBeClickable(page.getUpdateButton());
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));

    beforeEach(() => {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
    });

    afterEach(() => {
      jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    it('login with super admin', () => {
      page.loginUserPass('pgagnidze@productsavvy.com', '12345678');
    });

    it('add new revenue category', () => {

      page.navigateToRevenue();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getAddNew().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      browser.wait(addButtonIsNotClickable, 5000);
      page.getRevenueName().sendKeys('RevenueName');
      page.getRevenueType().sendKeys('RT');
      browser.wait(addButtonIsClickable, 5000);
      page.getAddButton().click();
      page.checkToastMessage('Revenue category added successfully');
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().sendKeys('RevenueName');
      browser.wait(spinnerIsNotVisible, 5000);
      expect(page.getTableItem().getText()).toEqual('RevenueName RT');
      page.getEditButton().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      expect(page.getRevenueName().getAttribute('value')).toEqual('RevenueName');
      expect(page.getRevenueType().getAttribute('value')).toEqual('RT');
    });

    it('add existing revenue category', () => {

      page.navigateToRevenue();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getAddNew().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      browser.wait(addButtonIsNotClickable, 5000);
      page.getRevenueName().sendKeys('RevenueName');
      page.getRevenueType().sendKeys('RT');
      browser.wait(addButtonIsClickable, 5000);
      page.getAddButton().click();
      page.checkToastMessage('Revenue category already exists');
    });

    it('edit revenue category', () => {

      page.navigateToRevenue();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().sendKeys('RevenueName');
      browser.wait(spinnerIsNotVisible, 5000);
      page.getEditButton().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      browser.wait(updateButtonIsNotClickable, 5000);
      page.getRevenueName().sendKeys(' Edited');
      page.getRevenueType().sendKeys(' E');
      page.getRevenueDescription().sendKeys('RevenueDescription');
      browser.wait(updateButtonIsClickable, 5000);
      page.getUpdateButton().click();
      page.checkToastMessage('Revenue Category updated successfully');
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().clear();
      page.getSearchField().sendKeys('RevenueName Edited');
      browser.wait(spinnerIsNotVisible, 5000);
      expect(page.getTableItem().getText()).toEqual('RevenueName Edited RevenueDescription RT E');
      page.getEditButton().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      expect(page.getRevenueName().getAttribute('value')).toEqual('RevenueName Edited');
      expect(page.getRevenueType().getAttribute('value')).toEqual('RT E');
      expect(page.getRevenueDescription().getAttribute('value')).toEqual('RevenueDescription');
    });

    it('delete revenue category', () => {

      page.navigateToRevenue();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().sendKeys('RevenueName');
      browser.wait(spinnerIsNotVisible, 5000);
      page.getDeleteButton().click();
      browser.wait(EC.alertIsPresent(), 5000);
      browser.switchTo().alert().accept();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().clear();
      page.getSearchField().sendKeys('Sales Items');
      browser.wait(spinnerIsNotVisible, 5000);
      page.getDeleteButton().click();
      browser.wait(EC.alertIsPresent(), 5000);
      browser.switchTo().alert().accept();
      page.checkToastMessage('Revenue category used in system');
    });

});
