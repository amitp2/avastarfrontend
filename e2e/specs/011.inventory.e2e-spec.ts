import * as page from '../pages/inventory.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Inventories', () => {

    let originalTimeout;

    const EC = protractor.ExpectedConditions;
    const addButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getAddButton()));
    const addButtonIsClickable = EC.elementToBeClickable(page.getAddButton());
    const updateButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getUpdateButton()));
    const updateButtonIsClickable = EC.elementToBeClickable(page.getUpdateButton());
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));

    beforeEach(() => {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
    });

    afterEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    it('login with organization admin', () => {
        page.loginUserPass('protractoradmn@mailinator.com', '12345678');
    });

    it('add inventory item step 1', () => {

        const path = require('path');
        const fileToUpload = '../../src/assets/images/logo-hilton.png',
        absolutePath = path.resolve(__dirname, fileToUpload);

        page.navigateToInventory();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Asset Information');
        page.getUploadLogo().sendKeys(absolutePath);
        page.checkToastMessage('Inventory logo uploaded');
        browser.wait(addButtonIsNotClickable, 5000);
        browser.sleep(2000);
        page.getCategoryDropdown().click();
        page.selectFromDropdown('Accessories');
        browser.sleep(2000);
        page.getSubCategoryDropdown().click();
        page.selectFromDropdown('Cases & Bags');
        browser.sleep(2000);
        page.getEquipmentDropdown().click();
        page.selectFromDropdown('Case, Audio Recorder');
        page.getMakeField().sendKeys('Make');
        page.getModelField().sendKeys('Model');
        page.getTagFilter().sendKeys('Tag');
        browser.wait(addButtonIsClickable, 5000);
        page.getAddButton().click();
        page.checkToastMessage('Inventory asset created successfully');
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getTagFilter().sendKeys('Tag');
        page.getApplyButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        expect(page.getTableItem().getText()).toEqual('Make Model Case, Audio Recorder Tag');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getCategoryDropdown().element(by.css('option:checked')).getText()).toEqual('Accessories');
        expect(page.getSubCategoryDropdown().element(by.css('option:checked')).getText()).toEqual('Cases & Bags');
        expect(page.getEquipmentDropdown().element(by.css('option:checked')).getText()).toEqual('Case, Audio Recorder');
        expect(page.getMakeField().getAttribute('value')).toEqual('Make');
        expect(page.getModelField().getAttribute('value')).toEqual('Model');
        expect(page.getTagFilter().getAttribute('value')).toEqual('Tag');
    });

    it('edit inventory item step 1', () => {

        page.navigateToInventory();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getTagFilter().sendKeys('Tag');
        page.getApplyButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Asset Information');
        browser.sleep(2500);
        page.getCategoryDropdown().click();
        page.selectFromDropdown('Audio');
        browser.sleep(2500);
        page.getSubCategoryDropdown().click();
        page.selectFromDropdown('Speakers');
        browser.sleep(2500);
        page.getEquipmentDropdown().click();
        page.selectFromDropdown('Speaker, Bluetooth');
        page.getMakeField().sendKeys(' Edited');
        page.getModelField().sendKeys(' Edited');
        page.getSerialField().sendKeys('Serial Number');
        page.getTagFilter().sendKeys(' Edited');
        page.getBarcodeField().sendKeys('Inventory Barcode');
        page.getPurchaseDate().sendKeys('2018-01-01');
        page.getHeaderText().click();
        page.getPurchasePrice().sendKeys('1000');
        page.getVendorDropdown().click();
        page.getDropdownSearch().sendKeys('Second Vendor');
        page.getSecondVendor().click();
        page.getInstalledCheckbox().click();
        browser.sleep(2000);
        page.getLocationDropdown().click();
        page.selectFromDropdown('Function Room');
        page.getOperatorCheckbox().click();
        browser.sleep(2000);
        page.getOwnerDropdown().click();
        page.selectFromDropdown('Operator');
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset updated successfully');
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getTagFilter().sendKeys('Tag Edited');
        page.getApplyButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        expect(page.getTableItem().getText()).toEqual('Make Edited Model Edited Speaker, Bluetooth Tag Edited');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getCategoryDropdown().element(by.css('option:checked')).getText()).toEqual('Audio');
        expect(page.getSubCategoryDropdown().element(by.css('option:checked')).getText()).toEqual('Speakers');
        expect(page.getEquipmentDropdown().element(by.css('option:checked')).getText()).toEqual('Speaker, Bluetooth');
        expect(page.getMakeField().getAttribute('value')).toEqual('Make Edited');
        expect(page.getModelField().getAttribute('value')).toEqual('Model Edited');
        expect(page.getSerialField().getAttribute('value')).toEqual('Serial Number');
        expect(page.getTagField().getAttribute('value')).toEqual('Tag Edited');
        expect(page.getBarcodeField().getAttribute('value')).toEqual('Inventory Barcode');
        expect(page.getPurchaseDate().getAttribute('value')).toEqual('2018-01-01');
        expect(page.getPurchasePrice().getAttribute('value')).toEqual('1000');
        expect(page.getVendorDropdown().element(by.css('option:checked')).getText()).toEqual('Second Vendor');
        expect(page.getLocationDropdown().element(by.css('option:checked')).getText()).toEqual('Function Room');
        expect(page.getOwnerDropdown().element(by.css('option:checked')).getText()).toEqual('Operator');
    });

    it('add inventory item step 2', () => {

        page.navigateToInventory();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getBarcodeFilter().sendKeys('Inventory Barcode');
        page.getApplyButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Asset Information');
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Service & Maintenance Information');
        browser.wait(updateButtonIsNotClickable, 5000);
        page.getMaintSchedule().sendKeys('Monthly');
        page.getLastServiceDate().sendKeys('2018-01-01');
        page.getHeaderText().click();
        browser.sleep(1500);
        expect(page.getNextServiceDate().getAttribute('value')).toEqual('2018-02-01');
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset maintenance info updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Asset Condition Status');
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getBarcodeFilter().sendKeys('Inventory Barcode');
        page.getApplyButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        expect(page.getTableItem().getText()).toEqual('Make Edited Model Edited Speaker, Bluetooth Tag Edited');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        browser.wait(spinnerIsNotVisible, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Service & Maintenance Information');
        expect(page.getLastServiceDate().getAttribute('value')).toEqual('2018-01-01');
        expect(page.getNextServiceDate().getAttribute('value')).toEqual('2018-02-01');
    });

    it('edit inventory item step 2', () => {

        page.navigateToInventory();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getCategoryFilter().click();
        page.selectFromDropdown('Audio');
        page.getApplyButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Asset Information');
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Service & Maintenance Information');
        page.getWarranyDate().sendKeys('2018-01-01');
        page.getHeaderText().click();
        page.getWarrantyVendor().click();
        page.selectFromDropdown('Second Vendor');
        page.getServiceDate().sendKeys('2018-01-01');
        page.getHeaderText().click();
        page.getServiceVendor().click();
        page.selectFromDropdown('Second Vendor');
        page.getMaintSchedule().sendKeys('Annual');
        page.getLastServiceDate().clear();
        page.getLastServiceDate().sendKeys('2018-01-02');
        page.getHeaderText().click();
        expect(page.getNextServiceDate().getAttribute('value')).toEqual('2019-01-02');
        page.getMaintSchedule().click();
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset maintenance info updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Asset Condition Status');
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getCategoryFilter().click();
        page.selectFromDropdown('Audio');
        page.getApplyButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        expect(page.getTableItem().getText()).toEqual('Make Edited Model Edited Speaker, Bluetooth Tag Edited');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        browser.wait(spinnerIsNotVisible, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Service & Maintenance Information');
        expect(page.getWarranyDate().getAttribute('value')).toEqual('2018-01-01');
        expect(page.getWarrantyVendor().element(by.css('option:checked')).getText()).toEqual('Second Vendor');
        expect(page.getServiceDate().getAttribute('value')).toEqual('2018-01-01');
        expect(page.getServiceVendor().element(by.css('option:checked')).getText()).toEqual('Second Vendor');
        expect(page.getMaintSchedule().element(by.css('option:checked')).getText()).toEqual('Annual');
        expect(page.getLastServiceDate().getAttribute('value')).toEqual('2018-01-02');
        expect(page.getNextServiceDate().getAttribute('value')).toEqual('2019-01-02');
    });

    it('add inventory item step 3', () => {

        page.navigateToInventory();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getMakeFilter().click();
        page.selectFromDropdown('Make Edited');
        page.getApplyButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Asset Information');
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Service & Maintenance Information');
        page.getUpdateButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Asset Condition Status');
        browser.wait(spinnerIsNotVisible, 5000);
        browser.wait(updateButtonIsNotClickable, 5000);
        page.getAcceptableCheckbox().click();
        page.getConditionStatus().sendKeys('Removed');
        page.getCommentField().sendKeys('Status');
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset status updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Documents');
        page.getDoneButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getMakeFilter().click();
        page.selectFromDropdown('Make Edited');
        page.getApplyButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        expect(page.getTableItem().getText()).toEqual('Make Edited Model Edited Speaker, Bluetooth Tag Edited Removed');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        browser.wait(spinnerIsNotVisible, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Service & Maintenance Information');
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset maintenance info updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Asset Condition Status');
        expect(page.getConditionStatus().element(by.css('option:checked')).getText()).toEqual('Removed');
        expect(page.getCommentField().getAttribute('value')).toEqual('Status');
    });

    it('edit inventory item step 3', () => {

        page.navigateToInventory();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getStatusFilter().sendKeys('Removed');
        page.getApplyButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Asset Information');
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Service & Maintenance Information');
        page.getUpdateButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Asset Condition Status');
        browser.wait(spinnerIsNotVisible, 5000);
        browser.wait(updateButtonIsClickable, 5000);
        page.getSubStandardCheckbox().click();
        page.getConditionStatus().sendKeys('Active');
        page.getCommentField().sendKeys(' Updated');
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset status updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Documents');
        page.getDoneButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getStatusFilter().sendKeys('Active');
        page.getApplyButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        expect(page.getTableItem().getText()).toEqual('Make Edited Model Edited Speaker, Bluetooth Tag Edited Active');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        browser.wait(spinnerIsNotVisible, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Service & Maintenance Information');
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset maintenance info updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Asset Condition Status');
        expect(page.getConditionStatus().element(by.css('option:checked')).getText()).toEqual('Active');
        expect(page.getCommentField().getAttribute('value')).toEqual('Status Updated');
    });

    it('add inventory item step 4', () => {

        const path = require('path');
        const fileToUpload1 = '../../src/assets/images/logo-hilton.png',
        absolutePath1 = path.resolve(__dirname, fileToUpload1);
        const fileToUpload2 = '../../src/assets/images/logo-marriott.png',
        absolutePath2 = path.resolve(__dirname, fileToUpload2);
        const fileName1 = '../downloads/logo-hilton.png';
        const absoluteFile1 = path.resolve(__dirname, fileName1);
        const fileName2 = '../downloads/logo-marriott.png';
        const absoluteFile2 = path.resolve(__dirname, fileName2);
        const fs = require('fs');

        const addButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getDocumentAdd()));
        const addButtonIsClickable = EC.elementToBeClickable(page.getDocumentAdd());

        page.navigateToInventory();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getPurchaseFromFilter().sendKeys('2018-01-01');
        page.getPurchaseToFilter().sendKeys('2018-01-01');
        page.getApplyButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Asset Information');
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Service & Maintenance Information');
        page.getUpdateButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Asset Condition Status');
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset status updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Documents');
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        browser.wait(addButtonIsNotClickable, 5000);
        page.getDocumentName().sendKeys('Document1');
        page.getUploadLogo().sendKeys(absolutePath1);
        page.checkToastMessage('Inventory document uploaded');
        browser.wait(addButtonIsClickable, 5000);
        page.getDocumentAdd().click();
        page.checkToastMessage('Inventory document added');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getDocumentTable().getText()).toEqual('Document1 logo-hilton.png');
        page.getDownloadButton().click();
        browser.wait(function() {
            return fs.existsSync(absoluteFile1);
        }, 5000).then(function() {
            fs.unlinkSync(absoluteFile1);
        });
        page.getDoneButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getPurchaseFromFilter().sendKeys('2018-01-01');
        page.getPurchaseToFilter().sendKeys('2018-01-01');
        page.getApplyButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        expect(page.getTableItem().getText()).toEqual('Make Edited Model Edited Speaker, Bluetooth Tag Edited Active');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        browser.wait(spinnerIsNotVisible, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Service & Maintenance Information');
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset maintenance info updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Asset Condition Status');
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset status updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Documents');
        expect(page.getDocumentTable().getText()).toEqual('Document1 logo-hilton.png');
    });

    it('edit inventory item step 4', () => {

        const path = require('path');
        const fileToUpload1 = '../../src/assets/images/logo-hilton.png',
        absolutePath1 = path.resolve(__dirname, fileToUpload1);
        const fileToUpload2 = '../../src/assets/images/logo-marriott.png',
        absolutePath2 = path.resolve(__dirname, fileToUpload2);
        const fileName1 = '../downloads/logo-hilton.png';
        const absoluteFile1 = path.resolve(__dirname, fileName1);
        const fileName2 = '../downloads/logo-marriott.png';
        const absoluteFile2 = path.resolve(__dirname, fileName2);
        const fs = require('fs');

        const addButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getDocumentAdd()));
        const addButtonIsClickable = EC.elementToBeClickable(page.getDocumentAdd());

        page.navigateToInventory();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getPurchaseFromFilter().sendKeys('2018-01-01');
        page.getPurchaseToFilter().sendKeys('2018-01-01');
        page.getApplyButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Asset Information');
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Service & Maintenance Information');
        page.getUpdateButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Asset Condition Status');
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset status updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Documents');
        expect(page.getDocumentTable().getText()).toEqual('Document1 logo-hilton.png');
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        browser.wait(addButtonIsNotClickable, 5000);
        page.getDocumentName().sendKeys('Document2');
        page.getUploadLogo().sendKeys(absolutePath2);
        page.checkToastMessage('Inventory document uploaded');
        browser.wait(addButtonIsClickable, 5000);
        page.getDocumentAdd().click();
        page.checkToastMessage('Inventory document added');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getDocumentTable().getText()).toEqual('Document2 logo-marriott.png');
        page.getDownloadButton().click();
        browser.wait(function() {
            return fs.existsSync(absoluteFile2);
        }, 5000).then(function() {
            fs.unlinkSync(absoluteFile2);
        });
        fs.existsSync(absoluteFile2);
        page.getDoneButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getPurchaseFromFilter().sendKeys('2018-01-01');
        page.getPurchaseToFilter().sendKeys('2018-01-01');
        page.getApplyButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        expect(page.getTableItem().getText()).toEqual('Make Edited Model Edited Speaker, Bluetooth Tag Edited Active');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        browser.wait(spinnerIsNotVisible, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Service & Maintenance Information');
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset maintenance info updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Asset Condition Status');
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset status updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Documents');
        expect(page.getDocumentTable().getText()).toEqual('Document2 logo-marriott.png');
    });

    it('clone inventory item step 1', () => {

        page.navigateToInventory();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getTagFilter().sendKeys('Tag Edited');
        page.getApplyButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getCloneButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Asset Information');
        expect(page.getMakeField().getAttribute('value')).toEqual('Make Edited');
        expect(page.getModelField().getAttribute('value')).toEqual('Model Edited');
        page.getSerialField().sendKeys('Serial Number');
        page.getTagField().sendKeys('Tag Edited');
        page.getBarcodeField().sendKeys('Inventory Barcode');
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Duplicate product in the system');
        page.getCancelButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getTagFilter().sendKeys('Tag Edited');
        page.getApplyButton().click();
        page.getCloneButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        page.getTagField().sendKeys('Tag Cloned');
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset updated successfully');
        expect(page.getHeaderText().getText()).toEqual('Service & Maintenance Information');
        expect(page.getWarranyDate().getAttribute('value')).toEqual('2018-01-01');
        expect(page.getWarrantyVendor().element(by.css('option:checked')).getText()).toEqual('Second Vendor');
        expect(page.getServiceDate().getAttribute('value')).toEqual('2018-01-01');
        expect(page.getServiceVendor().element(by.css('option:checked')).getText()).toEqual('Second Vendor');
        expect(page.getMaintSchedule().element(by.css('option:checked')).getText()).toEqual('Annual');
        expect(page.getLastServiceDate().getAttribute('value')).toEqual('2018-01-02');
        expect(page.getNextServiceDate().getAttribute('value')).toEqual('2019-01-02');
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset maintenance info updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getHeaderText().getText()).toEqual('Asset Condition Status');
        expect(page.getHeaderText().getText()).toEqual('Asset Condition Status');
        expect(page.getConditionStatus().element(by.css('option:checked')).getText()).toEqual('Active');
        expect(page.getCommentField().getAttribute('value')).toEqual('Status Updated');
        page.getUpdateButton().click();
        page.checkToastMessage('Inventory asset status updated successfully');
        page.getDoneButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getTagFilter().sendKeys('Tag Cloned');
        page.getApplyButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        expect(page.getTableItem().getText()).toEqual('Make Edited Model Edited Speaker, Bluetooth Tag Cloned Active');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getCategoryDropdown().element(by.css('option:checked')).getText()).toEqual('Audio');
        expect(page.getSubCategoryDropdown().element(by.css('option:checked')).getText()).toEqual('Speaker');
        expect(page.getEquipmentDropdown().element(by.css('option:checked')).getText()).toEqual('Speaker, Bluetooth');
        expect(page.getMakeField().getAttribute('value')).toEqual('Make Edited');
        expect(page.getModelField().getAttribute('value')).toEqual('Model Edited');
        expect(page.getTagField().getAttribute('value')).toEqual('Tag Cloned');
    });

    it('delete inventory items', () => {

        page.navigateToInventory();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getTagFilter().sendKeys('Tag Edited');
        page.getApplyButton().click();
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getTagFilter().clear();
        page.getTagFilter().sendKeys('Tag Cloned');
        page.getApplyButton().click();
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
    });

});
