import * as page from '../pages/cost-categories.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Users', () => {

    let originalTimeout;

    const EC = protractor.ExpectedConditions;
    const addButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getAddButton()));
    const addButtonIsClickable = EC.elementToBeClickable(page.getAddButton());
    const updateButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getUpdateButton()));
    const updateButtonIsClickable = EC.elementToBeClickable(page.getUpdateButton());
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));

    beforeEach(() => {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
    });

    afterEach(() => {
      jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    it('login with super admin', () => {
      page.loginUserPass('pgagnidze@productsavvy.com', '12345678');
    });

    it('add new cost category', () => {

      page.navigateToCost();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getAddNew().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      browser.wait(addButtonIsNotClickable, 5000);
      page.getCostName().sendKeys('CostName');
      browser.wait(addButtonIsClickable, 5000);
      page.getAddButton().click();
      page.checkToastMessage('Cost category added successfully');
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().sendKeys('CostName');
      browser.wait(spinnerIsNotVisible, 5000);
      expect(page.getTableItem().getText()).toEqual('CostName');
      page.getEditButton().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      expect(page.getCostName().getAttribute('value')).toEqual('CostName');
    });

    it('add existing cost category', () => {

      page.navigateToCost();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getAddNew().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      browser.wait(addButtonIsNotClickable, 5000);
      page.getCostName().sendKeys('CostName');
      browser.wait(addButtonIsClickable, 5000);
      page.getAddButton().click();
      page.checkToastMessage('Cost category already exists');
    });

    it('edit revenue category', () => {

      page.navigateToCost();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().sendKeys('CostName');
      browser.wait(spinnerIsNotVisible, 5000);
      page.getEditButton().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      browser.wait(updateButtonIsNotClickable, 5000);
      page.getCostName().sendKeys(' Edited');
      page.getCostDescription().sendKeys('CostDescription');
      browser.wait(updateButtonIsClickable, 5000);
      page.getUpdateButton().click();
      page.checkToastMessage('Cost Category updated successfully');
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().clear();
      page.getSearchField().sendKeys('CostName Edited');
      browser.wait(spinnerIsNotVisible, 5000);
      expect(page.getTableItem().getText()).toEqual('CostName Edited CostDescription');
      page.getEditButton().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      expect(page.getCostName().getAttribute('value')).toEqual('CostName Edited');
      expect(page.getCostDescription().getAttribute('value')).toEqual('CostDescription');
    });

    it('delete revenue category', () => {

      const EC = protractor.ExpectedConditions;

      page.navigateToCost();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().sendKeys('CostName');
      browser.wait(spinnerIsNotVisible, 5000);
      page.getDeleteButton().click();
      browser.wait(EC.alertIsPresent(), 5000);
      browser.switchTo().alert().accept();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().clear();
      page.getSearchField().sendKeys('Sales');
      browser.wait(spinnerIsNotVisible, 5000);
      page.getDeleteButton().click();
      browser.wait(EC.alertIsPresent(), 5000);
      browser.switchTo().alert().accept();
      page.checkToastMessage('Cost category used in system');
    });

});
