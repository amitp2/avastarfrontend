import * as page from '../pages/categories.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Login', () => {

    const EC = protractor.ExpectedConditions;
    const updateButtonIsClickable = EC.elementToBeClickable(page.getUpdateButton());
    const updateButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getUpdateButton()));
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));

    it('login with organization admin', () => {
        page.loginUserPass('protractoradmn@mailinator.com', '12345678');
    });

    it('Update Cost Category', () => {

        page.navigateToCost();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getSearchField().sendKeys('Sales');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getTableItem().getText()).toEqual('Sales 1337');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getCostCategoryField().getAttribute('value')).toEqual('Sales');
        expect(page.getCostCodeField().getAttribute('value')).toEqual('1337');
        page.getCostCodeField().clear();
        page.getCostCodeField().sendKeys('7331');
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Cost Category updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        expect(page.getTableItem().getText()).toEqual('Sales 7331');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getCostCategoryField().getAttribute('value')).toEqual('Sales');
        expect(page.getCostCodeField().getAttribute('value')).toEqual('7331');
        page.getCostCodeField().clear();
        page.getCostCodeField().sendKeys('1337');
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Cost Category updated successfully');
    });

    it('Update Revenue Category', () => {

        page.navigateToRevenue();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getSearchField().sendKeys('Sales');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getTableItem().getText()).toEqual('Sales Items S No No 1337');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getRevenueCategoryField().getAttribute('value')).toEqual('Sales Items');
        expect(page.getRevenueCodeField().getAttribute('value')).toEqual('1337');
        page.getRevenueCodeField().clear();
        page.getRevenueCodeField().sendKeys('7331');
        page.getDiscountbleToggle().click();
        page.getChargeToggle().click();
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Revenue Category updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        expect(page.getTableItem().getText()).toEqual('Sales Items S Yes Yes 7331');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getRevenueCategoryField().getAttribute('value')).toEqual('Sales Items');
        expect(page.getRevenueCodeField().getAttribute('value')).toEqual('7331');
        page.getRevenueCodeField().clear();
        page.getRevenueCodeField().sendKeys('1337');
        page.getDiscountbleToggle().click();
        page.getChargeToggle().click();
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Revenue Category updated successfully');
    });

});
