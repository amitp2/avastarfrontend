import * as page from '../pages/terms.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Login', () => {

    const EC = protractor.ExpectedConditions;
    const saveButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getSaveButton()));
    const saveButtonIsClickable = EC.elementToBeClickable(page.getSaveButton());

    it('login with organization admin', () => {
        page.loginUserPass('protractoradmn@mailinator.com', '12345678');
    });

    it('add contact terms', () => {

        browser.ignoreSynchronization = true;
        page.navigateToTerms();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.id('terms'))), 5000);
        browser.wait(saveButtonIsClickable, 5000);
        browser.sleep(1500);
        browser.wait(EC.visibilityOf(element(by.tagName('iframe'))), 10000).then(function() {

            browser.driver.switchTo().frame(element(by.tagName('iframe')).getWebElement());

            expect(element(by.id('tinymce')).isDisplayed()).toBeTruthy();
        });
        page.getTextInput().click();
        page.getTextInput().clear();
        page.getTextInput().sendKeys('Terms and conditions');
        browser.driver.switchTo().defaultContent();
        page.getSaveButton().click();
        browser.sleep(2000);
        expect(page.getToastMessage().getText()).toEqual('Terms and Conditions saved successfully');
        browser.sleep(2000);
        page.getBillingTab().click();
        browser.sleep(1500);
        browser.wait(EC.visibilityOf(element(by.tagName('iframe'))), 10000).then(function() {

            browser.driver.switchTo().frame(element(by.tagName('iframe')).getWebElement());

            expect(element(by.id('tinymce')).isDisplayed()).toBeTruthy();
        });
        page.getTextInput().click();
        page.getTextInput().clear();
        page.getTextInput().sendKeys('Billing comment');
        browser.driver.switchTo().defaultContent();
        page.getSaveButton().click();
        browser.sleep(2000);
        expect(page.getToastMessage().getText()).toEqual('Billing Comment saved successfully');
        browser.sleep(2000);
        page.getProposalTab().click();
        browser.sleep(1500);
        browser.wait(EC.visibilityOf(element(by.tagName('iframe'))), 10000).then(function() {

            browser.driver.switchTo().frame(element(by.tagName('iframe')).getWebElement());

            expect(element(by.id('tinymce')).isDisplayed()).toBeTruthy();
        });
        page.getTextInput().click();
        page.getTextInput().clear();
        page.getTextInput().sendKeys('Proposal comment');
        browser.driver.switchTo().defaultContent();
        page.getSaveButton().click();
        browser.sleep(2000);
        expect(page.getToastMessage().getText()).toEqual('Proposal Comment saved successfully');
        browser.sleep(2000);
    });

    it('clear contact terms', () => {

        const EC = protractor.ExpectedConditions;
        const saveButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getSaveButton()));
        const saveButtonIsClickable = EC.elementToBeClickable(page.getSaveButton());

        page.navigateToTerms();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.id('terms'))), 5000);
        browser.wait(saveButtonIsClickable, 5000);
        page.getClearButton().click();
        page.getSaveButton().click();
        page.checkToastMessage('Terms and Conditions saved successfully');
        page.getBillingTab().click();
        browser.sleep(1500);
        page.getClearButton().click();
        page.getSaveButton().click();
        page.checkToastMessage('Billing Comment saved successfully');
        page.getProposalTab().click();
        browser.sleep(1500);
        page.getClearButton().click();
        page.getSaveButton().click();
        page.checkToastMessage('Proposal Comment saved successfully');
    });

});
