import * as page from '../pages/subscribers.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Subscribers', () => {
    let originalTimeout;

    const EC = protractor.ExpectedConditions;
    const addButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getAddButton()));
    const addButtonIsClickable = EC.elementToBeClickable(page.getAddButton());
    const updateButtonIsClickable = EC.elementToBeClickable(page.getUpdateButton());
    const updateButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getUpdateButton()));
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));

    beforeEach(() => {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
    });

    afterEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });


    it('login with super admin', () => {
        page.loginUserPass('pgagnidze@productsavvy.com', '12345678');
    });

    it('add new subscriber', () => {

        page.navigateToSubs();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        browser.wait(addButtonIsNotClickable, 5000);
        expect(page.getContactPerson().getAttribute('class')).toEqual('-disabled');
        expect(page.getUsersInfo().getAttribute('class')).toEqual('-disabled');
        expect(page.getAccessInfo().getAttribute('class')).toEqual('-disabled');
        page.getCompanyField().sendKeys('01AOrg');
        page.getAddressField().sendKeys('2717 Clearview Drive');
        page.getAddressNdField().sendKeys('3788 Despard Street');
        page.getCityField().sendKeys('Atlanta');
        page.getPostCodeField().sendKeys('30303');
        page.getCountrySelect().click();
        page.selectFromDropdown('United States');
        browser.sleep(1500);
        page.getStateSelect().click();
        page.selectFromDropdown('Georgia');
        page.getPhoneField().sendKeys('133-713-3713');
        browser.wait(addButtonIsClickable, 5000);
        page.getAddButton().click();
        page.checkToastMessage('Subscriber has been added');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrg');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getTableItem().getText()).toEqual('01AOrg 0 Inactive');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getCompanyField().getAttribute('value')).toEqual('01AOrg');
        expect(page.getAddressField().getAttribute('value')).toEqual('2717 Clearview Drive');
        expect(page.getAddressNdField().getAttribute('value')).toEqual('3788 Despard Street');
        expect(page.getCityField().getAttribute('value')).toEqual('Atlanta');
        expect(page.getPostCodeField().getAttribute('value')).toEqual('30303');
        expect(page.getCountrySelect().element(by.css('option:checked')).getText()).toEqual('United States');
        expect(page.getStateSelect().element(by.css('option:checked')).getText()).toEqual('Georgia');
        expect(page.getPhoneField().getAttribute('value')).toEqual('133-713-3713');
    });

    it('edit added subscriber', () => {

        page.navigateToSubs();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrg');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        browser.wait(updateButtonIsClickable, 5000);
        expect(page.getContactPerson().getAttribute('class')).not.toEqual('-disabled');
        expect(page.getUsersInfo().getAttribute('class')).not.toEqual('-disabled');
        expect(page.getAccessInfo().getAttribute('class')).not.toEqual('-disabled');
        page.getCompanyField().sendKeys(' Edited');
        page.getAddressField().sendKeys(' Edited');
        page.getAddressNdField().sendKeys(' Edited');
        page.getCityField().sendKeys(' Edited');
        page.getPostCodeField().sendKeys('03');
        page.getCountrySelect().click();
        page.selectFromDropdown('Georgia');
        page.getOfficeField().sendKeys('Atlanta');
        page.getFaxField().sendKeys('404-555-0135');
        page.getWebsiteField().sendKeys('www.01a.org');
        page.getPhoneField().sendKeys(' Edited');
        page.getStatusToggle().click();
        page.getUpdateButton().click();
        page.checkToastMessage('Subscriber information has been updated');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        expect(page.getTableItem().getText()).toEqual('01AOrg Edited Atlanta 0 Active');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getCompanyField().getAttribute('value')).toEqual('01AOrg Edited');
        expect(page.getAddressField().getAttribute('value')).toEqual('2717 Clearview Drive Edited');
        expect(page.getAddressNdField().getAttribute('value')).toEqual('3788 Despard Street Edited');
        expect(page.getCityField().getAttribute('value')).toEqual('Atlanta Edited');
        expect(page.getPostCodeField().getAttribute('value')).toEqual('3030303');
        expect(page.getCountrySelect().element(by.css('option:checked')).getText()).toEqual('Georgia');
        expect(page.getOfficeField().getAttribute('value')).toEqual('Atlanta');
        expect(page.getFaxField().getAttribute('value')).toEqual('404-555-0135');
        expect(page.getPhoneField().getAttribute('value')).toEqual('133-713-3713');
        expect(page.getWebsiteField().getAttribute('value')).toEqual('www.01a.org');
    });

    it('add contact person', () => {

        page.navigateToSubs();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrg Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getContactPerson().getAttribute('class')).not.toEqual('-disabled');
        expect(page.getUsersInfo().getAttribute('class')).not.toEqual('-disabled');
        expect(page.getAccessInfo().getAttribute('class')).not.toEqual('-disabled');
        page.getContactPerson().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getContactFname().sendKeys('John');
        page.getContactLname().sendKeys('Doe');
        page.getContactPhone().sendKeys('404-555-0176');
        page.getContactRole().sendKeys('None');
        page.getContactEmail().sendKeys('john@ doe');
        browser.wait(updateButtonIsNotClickable, 5000);
        page.getContactEmail().clear();
        page.getContactEmail().sendKeys('john@doe');
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Contact person information has been updated');
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getContactButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getContactFname().getAttribute('value')).toEqual('John');
        expect(page.getContactLname().getAttribute('value')).toEqual('Doe');
        expect(page.getContactPhone().getAttribute('value')).toEqual('404-555-0176');
        expect(page.getContactEmail().getAttribute('value')).toEqual('john@doe');
        expect(page.getContactRole().element(by.css('option:checked')).getText()).toEqual('None');
    });

    it('edit contact person', () => {

        page.navigateToSubs();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrg Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getContactButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getContactFname().sendKeys(' Edited');
        page.getContactLname().sendKeys(' Edited');
        page.getContactPhone().sendKeys(' Edited');
        page.getContactEmail().sendKeys(' Edited');
        page.getContactDepartment().sendKeys('Department');
        page.getContactTitle().sendKeys('Title');
        page.getContactFax().sendKeys('Fax');
        page.getContactMobile().sendKeys('133-713-3713');
        page.getContactNote().sendKeys('Note');
        page.getContactRole().sendKeys('Admin');
        browser.wait(updateButtonIsNotClickable, 5000);
        page.getContactEmail().clear();
        page.getContactEmail().sendKeys('john@doeEdited');
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Contact person information has been updated');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getContactButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getContactFname().getAttribute('value')).toEqual('John Edited');
        expect(page.getContactLname().getAttribute('value')).toEqual('Doe Edited');
        expect(page.getContactPhone().getAttribute('value')).toEqual('404-555-0176');
        expect(page.getContactEmail().getAttribute('value')).toEqual('john@doeEdited');
        expect(page.getContactDepartment().getAttribute('value')).toEqual('Department');
        expect(page.getContactTitle().getAttribute('value')).toEqual('Title');
        expect(page.getContactFax().getAttribute('value')).toEqual('Fax');
        expect(page.getContactMobile().getAttribute('value')).toEqual('133-713-3713');
        expect(page.getContactNote().getAttribute('value')).toEqual('Note');
        expect(page.getContactRole().element(by.css('option:checked')).getText()).toEqual('Admin');
    });

    it('add new user', () => {

        const randVal = Date.now();

        page.navigateToSubs();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrg Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getContactPerson().getAttribute('class')).not.toEqual('-disabled');
        expect(page.getUsersInfo().getAttribute('class')).not.toEqual('-disabled');
        expect(page.getAccessInfo().getAttribute('class')).not.toEqual('-disabled');
        page.getUsersInfo().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getPlusUser().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getContactFname().sendKeys('Jane');
        page.getContactLname().sendKeys('Doe');
        page.getContactPhone().sendKeys('404-555-0177');
        page.getUserEmail().sendKeys('pgagnidze@productsavvy.com');
        browser.wait(addButtonIsClickable, 5000);
        page.getAddButton().click();
        page.checkToastMessage('User with the provided username already exists');
        page.getUserEmail().clear();
        page.getUserEmail().sendKeys(randVal + '@mailinator. com');
        browser.wait(addButtonIsNotClickable, 5000);
        page.getUserEmail().clear();
        page.getUserEmail().sendKeys(randVal + '@mailinator.com');
        browser.wait(addButtonIsClickable, 5000);
        page.getAddButton().click();
        page.checkToastMessage('Subscriber user created successfully. An activation email has been sent to the user.');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getSearchFieldUser().sendKeys('Jane');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getTableUser().getText()).toEqual('Jane Doe ' + randVal + '@mailinator.com Admin');
        page.getEditUser().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getContactFname().getAttribute('value')).toEqual('Jane');
        expect(page.getContactLname().getAttribute('value')).toEqual('Doe');
        expect(page.getContactPhone().getAttribute('value')).toEqual('404-555-0177');
        expect(page.getUserEmail().getAttribute('value')).toEqual(randVal + '@mailinator.com');
        page.getCloseButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        expect(page.getTableItem().getText()).toEqual('01AOrg Edited Atlanta John Edited Doe Edited 0 Active');
    });

    it('add new user and edit old user', () => {

        const randVal = Date.now();

        page.navigateToSubs();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrg Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getContactPerson().getAttribute('class')).not.toEqual('-disabled');
        expect(page.getUsersInfo().getAttribute('class')).not.toEqual('-disabled');
        expect(page.getAccessInfo().getAttribute('class')).not.toEqual('-disabled');
        page.getUsersInfo().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getPlusUser().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getContactFname().sendKeys('John');
        page.getContactLname().sendKeys('Doe');
        page.getContactPhone().sendKeys('404-555-0177');
        page.getUserEmail().sendKeys(randVal + '@mailinator.com');
        browser.wait(addButtonIsClickable, 5000);
        page.getAddButton().click();
        page.checkToastMessage('Subscriber user created successfully. An activation email has been sent to the user.');
        page.getSearchFieldUser().sendKeys('Jane');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditUser().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getContactFname().sendKeys(' Edited');
        page.getContactLname().sendKeys(' Edited');
        page.getContactPhone().sendKeys(' Edited');
        page.getContactTitle().sendKeys('Title');
        page.getContactFax().sendKeys('Fax');
        page.getContactMobile().sendKeys('133-713-3713');
        page.getContactNote().sendKeys('Note');
        page.getUserRoleField().click();
        page.getUserRolePick().click();
        browser.wait(updateButtonIsNotClickable, 5000);
        page.getUserRoleField().click();
        page.getUserRolePick().click();
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Subscriber user updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getSearchFieldUser().sendKeys('Jane');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditUser().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getContactFname().getAttribute('value')).toEqual('Jane Edited');
        expect(page.getContactLname().getAttribute('value')).toEqual('Doe Edited');
        expect(page.getContactPhone().getAttribute('value')).toEqual('404-555-0177');
        expect(page.getContactTitle().getAttribute('value')).toEqual('Title');
        expect(page.getContactFax().getAttribute('value')).toEqual('Fax');
        expect(page.getUserEmail().getAttribute('value')).toContain('@mailinator.com');
        expect(page.getContactMobile().getAttribute('value')).toEqual('133-713-3713');
        expect(page.getContactNote().getAttribute('value')).toEqual('Note');
        page.getCloseButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().clear();
        page.getSearchField().sendKeys('01AOrg Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getTableItem().getText()).toEqual('01AOrg Edited Atlanta John Edited Doe Edited 2 Active');
    });

    it('access control', () => {

        page.navigateToSubs();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrg Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getContactPerson().getAttribute('class')).not.toEqual('-disabled');
        expect(page.getUsersInfo().getAttribute('class')).not.toEqual('-disabled');
        expect(page.getAccessInfo().getAttribute('class')).not.toEqual('-disabled');
        page.getAccessInfo().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getHasAccessDropdown().sendKeys('Yes');
        page.checkToastMessage('Access updated');
        page.getNumberUsersField().clear();
        page.getNumberUsersField().sendKeys('1');
        page.checkToastMessage('Access updated');
        page.getFirstUserCheckbox().click();
        page.checkToastMessage('Access updated');
        page.getNumberUsersField().clear();
        page.getNumberUsersField().sendKeys('0');
        page.checkToastMessage('Please Un-select users before updating number of licensed users information');
        page.getFirstUserCheckbox().click();
        page.checkToastMessage('Access updated');
        page.getNumberUsersField().clear();
        page.getNumberUsersField().sendKeys('0');
        page.checkToastMessage('Access updated');
        page.getHasAccessDropdown().sendKeys('No');
        page.checkToastMessage('Access updated');
    });

    it('delete added subscriber and user', () => {

        page.navigateToSubs();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrg Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getContactPerson().getAttribute('class')).not.toEqual('-disabled');
        expect(page.getUsersInfo().getAttribute('class')).not.toEqual('-disabled');
        page.getUsersInfo().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getDeleteUser().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
        page.getCloseButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
        browser.restart();
    });

});
