import * as page from '../pages/tickets.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Login', () => {

    let originalTimeout;

    const EC = protractor.ExpectedConditions;
    const addButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getAddButton()));
    const addButtonIsClickable = EC.elementToBeClickable(page.getAddButton());
    const updateButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getUpdateButton()));
    const updateButtonIsClickable = EC.elementToBeClickable(page.getUpdateButton());
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));

    beforeEach(() => {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
    });

    afterEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    it('login with organization admin', () => {
        page.loginUserPass('protractoradmn@mailinator.com', '12345678');
    });

    it('add service ticket', () => {

        const moment = require('moment');

        page.navigateToTickets();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property-block'))), 5000);
        browser.wait(addButtonIsNotClickable, 5000);
        page.getInventoryCategorySelect().click();
        page.selectFromDropdown('Presentation Support');
        browser.sleep(1500);
        page.getInventorySubCategorySelect().click();
        page.selectFromDropdown('Markers & Easels');
        browser.sleep(1500);
        page.getInventorySelect().click();
        page.selectFromDropdown('Flipchart, Easel - 0001');
        page.getTicketTitle().sendKeys('Ticket Name');
        page.getTicketIssue().sendKeys('Ticket Issue');
        page.getTicketReminder().sendKeys(moment().format('YYYY-MM-DD hh:mm'));
        page.getVendorSelect().click();
        page.selectFromDropdown('First Vendor');
        browser.sleep(1500);
        page.getVendorContactSelect().click();
        page.selectFromDropdown('First Contact');
        page.getTicketStatus().sendKeys('Open');
        browser.wait(addButtonIsClickable, 5000);
        page.getAddButton().click();
        page.checkToastMessage('Service Ticket added successfully');
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getInventoryNameFilter().sendKeys('Flipchart, Easel');
        page.getInventoryTagFilter().sendKeys('0001');
        page.getInventoryBarcodeFilter().sendKeys('Barcode Accessories');
        page.getTicketStatusFilter().sendKeys('Open');
        page.getTicketTitleFilter().sendKeys('Ticket Name');
        page.getFromDateFilter().click();
        page.getPrevDate().click();
        page.getFirstDate().click();
        page.getToDateFilter().click();
        page.getNextDate().click();
        page.getLastDate().click();
        page.getApplyButton().click();
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getTableItem()
                   .getText())
                   .toContain('Flipchart, Easel - 0001 Organization Admin First Contact Open');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property-block'))), 5000);
        expect(page.getInventoryCategorySelect().element(by.css('option:checked')).getText()).toEqual('Presentation Support');
        expect(page.getInventorySubCategorySelect().element(by.css('option:checked')).getText()).toEqual('Markers & Easels');
        expect(page.getInventorySelect().element(by.css('option:checked')).getText()).toEqual('Flipchart, Easel - 0001');
        expect(page.getStorageLocation().getAttribute('value')).toEqual('Storage for inventory');
        expect(page.getTicketTitle().getAttribute('value')).toEqual('Ticket Name');
        expect(page.getTicketIssue().getAttribute('value')).toEqual('Ticket Issue');
        expect(page.getTicketReminder().getAttribute('value')).toContain(moment().format('YYYY-MM-DD'));
        expect(page.getVendorSelect().element(by.css('option:checked')).getText()).toEqual('First Vendor');
        expect(page.getVendorContactSelect().element(by.css('option:checked')).getText()).toEqual('First Contact');
        expect(page.getTicketStatus().element(by.css('option:checked')).getText()).toEqual('Open');
    });

    it('edit service ticket', () => {

        const moment = require('moment');

        page.navigateToTickets();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getTicketTitleFilter().sendKeys('Ticket Name');
        page.getApplyButton().click();
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property-block'))), 5000);
        browser.wait(updateButtonIsClickable, 5000);
        page.getSystemCheckbox().click();
        browser.sleep(1500);
        page.getSystemSelect().click();
        page.checkToastMessage('System for ticket');
        page.getTicketTitle().sendKeys(' Edited');
        page.getTicketIssue().sendKeys(' Edited');
        page.getTicketReminder().sendKeys(moment().format('YYYY-MM-DD hh:mm'));
        page.getInternalPersonnelCheckbox().click();
        browser.sleep(1500);
        page.getVenueContactSelect().click();
        page.selectFromDropdown('Property Contact');
        page.getTicketStatus().sendKeys('Completed');
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getTicketComment().sendKeys('Comment');
        page.getAddCommentButton().click();
        browser.wait(spinnerIsNotVisible, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Service Ticket updated successfully');
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getSystemNameFilter().sendKeys('System for ticket');
        page.getTicketStatusFilter().sendKeys('Completed');
        page.getTicketTitleFilter().sendKeys('Ticket Name Edited');
        page.getFromDateFilter().click();
        page.getPrevDate().click();
        page.getFirstDate().click();
        page.getToDateFilter().click();
        page.getNextDate().click();
        page.getLastDate().click();
        page.getApplyButton().click();
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getTableItem()
                   .getText())
                   .toContain('System for ticket Organization Admin Property Contact Completed');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.property-block'))), 5000);
        expect(page.getSystemSelect().element(by.css('option:checked')).getText()).toEqual('System for ticket');
        expect(page.getTicketTitle().getAttribute('value')).toEqual('Ticket Name Edited');
        expect(page.getTicketIssue().getAttribute('value')).toEqual('Ticket Issue Edited');
        expect(page.getTicketReminder().getAttribute('value')).toContain(moment().format('YYYY-MM-DD'));
        expect(page.getVenueContactSelect().element(by.css('option:checked')).getText()).toEqual('Property Contact');
       expect(page.getTicketStatus().element(by.css('option:checked')).getText()).toEqual('Completed');
        expect(page.getTableItem()
                   .getText())
                   .toContain(moment().format('MMM D, YYYY') + ' Comment');

    });

    it('delete ticket', () => {

        page.navigateToTickets();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getTicketTitleFilter().sendKeys('Ticket Name Edited');
        page.getApplyButton().click();
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
    });

});
