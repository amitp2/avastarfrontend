import * as page from '../pages/storagespace.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Storage Spaces', () => {

    const EC = protractor.ExpectedConditions;
    const addButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getAddButton()));
    const addButtonIsClickable = EC.elementToBeClickable(page.getAddButton());
    const updateButtonIsClickable = EC.elementToBeClickable(page.getUpdateButton());
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));

    it('login with organization admin', () => {
        page.loginUserPass('protractoradmn@mailinator.com', '12345678');
    });

    it('Add Storage Space', () => {

        page.navigateToStorageSpace();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.storage-space'))), 5000);
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        browser.wait(addButtonIsNotClickable, 5000);
        page.getStorageName().sendKeys('Storage Name');
        page.getStorageStatus().sendKeys('Inactive');
        browser.wait(addButtonIsClickable, 5000);
        page.getAddButton().click();
        page.checkToastMessage('Storage Space created successfully');
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.storage-space'))), 5000);
        page.getSearchField().sendKeys('Storage Name');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getTableItem().getText()).toEqual('Storage Name Inactive');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getStorageName().getAttribute('value')).toEqual('Storage Name');
        expect(page.getStorageStatus().element(by.css('option:checked')).getText()).toEqual('Inactive');
    });

    it('Edit Storage Space', () => {

        page.navigateToStorageSpace();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.storage-space'))), 5000);
        page.getSearchField().sendKeys('Storage Name');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        browser.wait(updateButtonIsClickable, 5000);
        page.getStorageName().sendKeys(' Edited');
        page.getStorageStatus().sendKeys('Active');
        page.getStorageNotes().sendKeys('Notes');
        page.getUpdateButton().click();
        page.checkToastMessage('Storage Space updated successfully');
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.storage-space'))), 5000);
        browser.refresh();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.storage-space'))), 5000);
        page.getSearchField().sendKeys('Storage Name Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getTableItem().getText()).toEqual('Storage Name Edited Active');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getStorageName().getAttribute('value')).toEqual('Storage Name Edited');
        expect(page.getStorageStatus().element(by.css('option:checked')).getText()).toEqual('Active');
        expect(page.getStorageNotes().getAttribute('value')).toEqual('Notes');
    });

        it('delete storage space', () => {

        page.navigateToStorageSpace();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.storage-space'))), 5000);
        page.getSearchField().sendKeys('Storage Name Edited');
        browser.sleep(1500);
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
    });

});
