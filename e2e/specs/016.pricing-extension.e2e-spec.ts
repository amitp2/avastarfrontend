import * as page from '../pages/pricing.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Login', () => {

    let originalTimeout;

    const EC = protractor.ExpectedConditions;
    const saveButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getSaveButton()));
    const saveButtonIsClickable = EC.elementToBeClickable(page.getSaveButton());
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));

    const moment = require('moment');

    beforeEach(() => {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
    });

    afterEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    it('login with organization admin', () => {
        page.loginUserPass('protractoradmn@mailinator.com', '12345678');
    });

    it('extend price for equipment/services', () => {

        page.navigateToPricing();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.pricing'))), 5000);
        browser.wait(saveButtonIsNotClickable, 5000);
        page.getExtendDate().click();
        page.getNextDate().click();
        page.getLastDate().click();
        page.getCurrentDate().click();
        page.getApplyDropdown().sendKeys('Equipment / Services');
        browser.wait(saveButtonIsClickable, 5000);
        page.getSaveButton().click();
        page.checkToastMessage('Pricing extension updated successfully');
        page.navigateToEquipment();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getTypeFilter().sendKeys('Equipment');
        page.getDescriptionFilter().sendKeys('PricingExtensionEquipment');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getEndDate().getAttribute('value')).toEqual(moment().add(1, 'months').endOf('month').format('YYYY-MM-DD'));
        page.navigateToEquipment();
        page.getTypeFilter().sendKeys('Service');
        page.getDescriptionFilter().sendKeys('PricingExtensionService');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        page.navigateToPricing();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.pricing'))), 5000);
        expect(page.getCurrentDate().getAttribute('value')).toEqual(moment().add(1, 'months').endOf('month').format('YYYY-MM-DD'));
    });

    it('extend price for packages', () => {

        page.navigateToPricing();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.pricing'))), 5000);
        browser.wait(saveButtonIsNotClickable, 5000);
        page.getExtendDate().click();
        page.getNextDate().click();
        page.getLastDate().click();
        page.getCurrentDate().click();
        page.getApplyDropdown().sendKeys('Packages');
        browser.wait(saveButtonIsClickable, 5000);
        page.getSaveButton().click();
        page.checkToastMessage('Pricing extension updated successfully');
        page.navigateToPackages();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getDescriptionFilter().sendKeys('PricingExtensionPackage');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getPackagesEndDate().getAttribute('value')).toEqual(moment().add(1, 'months').endOf('month').format('YYYY-MM-DD'));
    });

    it('extend price for all', () => {

        page.navigateToPricing();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.pricing'))), 5000);
        browser.wait(saveButtonIsNotClickable, 5000);
        page.getExtendDate().click();
        page.getNextDate().click();
        page.getLastDate().click();
        page.getCurrentDate().click();
        page.getApplyDropdown().sendKeys('All');
        browser.wait(saveButtonIsClickable, 5000);
        page.getSaveButton().click();
        page.checkToastMessage('Pricing extension updated successfully');
        page.navigateToEquipment();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getTypeFilter().sendKeys('Equipment');
        page.getDescriptionFilter().sendKeys('PricingExtensionEquipment');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        page.navigateToEquipment();
        page.getTypeFilter().sendKeys('Service');
        page.getDescriptionFilter().sendKeys('PricingExtensionService');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        page.navigateToPricing();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.pricing'))), 5000);
        expect(page.getCurrentDate().getAttribute('value')).toEqual(moment().add(1, 'months').endOf('month').format('YYYY-MM-DD'));
        page.navigateToPackages();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getDescriptionFilter().sendKeys('PricingExtensionPackage');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getPackagesEndDate().getAttribute('value')).toEqual(moment().add(1, 'months').endOf('month').format('YYYY-MM-DD'));
    });

});
