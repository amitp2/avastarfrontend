import * as page from '../pages/functions.page';
import { protractor, browser, by, element } from 'protractor';
import { discardPeriodicTasks } from '@angular/core/testing';

describe('avastar-web Subscribers', () => {
    let originalTimeout;

    const EC = protractor.ExpectedConditions;
    const addButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getAddButton()));
    const addButtonIsClickable = EC.elementToBeClickable(page.getAddButton());
    const updateButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getUpdateButton()));
    const saveButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getSaveButton()));
    const saveButtonIsClickable = EC.elementToBeClickable(page.getSaveButton());
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));

    beforeEach(() => {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
    });

    afterEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    it('login with organization admin', () => {
        page.loginUserPass('protractoradmn@mailinator.com', '12345678');
    });

    it('add new function', () => {

        const itemPrice = 100;
        const serviceCharge = 0.5;
        const taxCharge = 0;
        const disCount = 0.5;

        const preDiscountResult = (itemPrice + itemPrice * serviceCharge) * (1 - disCount)
                                + (itemPrice + itemPrice * serviceCharge) * (1 - disCount) * taxCharge;

        page.navigateToAccounts();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('Account for Function');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getEventsInfo().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-events-list'))), 5000);
        page.getEventSearchField().sendKeys('Event for Function');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditEvent().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getAddFunction().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.add-function'))), 5000);
        expect(page.getAccountDropdown().element(by.css('option:checked')).getText()).toMatch('Account for Function');
        expect(page.getEventDropdown().element(by.css('option:checked')).getText()).toMatch('Event for Function');
        expect(page.getEventMasterNo().getAttribute('value')).toContain('1337');
        expect(page.getEventCode().getAttribute('value')).toEqual('7331');
        expect(page.getEventStatus().getAttribute('value')).toEqual('Definite');
        expect(page.getStandardDiscount().getAttribute('value')).toEqual('50');
        expect(page.getSetupDiscount().getAttribute('value')).toEqual('50');
        expect(page.getAltChargePercentage().getAttribute('value')).toEqual('50');
        expect(page.getAltCharge().isSelected()).toBe(true);
        expect(page.getTaxExempt().isSelected()).toBe(true);
        expect(page.getServiceChargeCalculation().isSelected()).toBe(true);
        browser.wait(saveButtonIsNotClickable, 5000);
        page.getFunctionDropdown().click();
        page.selectFromDropdown('Function Space for Function');
        page.getFunctionName().sendKeys('Function Name');
        page.getLocationNote().sendKeys('Location Note');
        page.getFunctionType().sendKeys('Exhibitor');
        page.getFunctionStyle().sendKeys('Function Style');
        page.getFunctionParticipants().sendKeys('10');
        page.getPlanningContact().sendKeys('Planning Contact');
        page.getSalesContact().sendKeys('Sales Contact');
        page.getFunctionStartDate().sendKeys('2018-01-01');
        page.getFunctionEndDate().sendKeys('2018-01-01');
        page.getEventCode().click();
        page.getSetTime().sendKeys('03:00');
        page.getStartTime().sendKeys('04:00');
        page.getEndTime().sendKeys('05:00');
        page.getBEOField().sendKeys('BEO');
        page.getWorkflowNoteField().sendKeys('Workflow Note');
        page.getClientNoteField().sendKeys('Client Note');
        // page.getHoursHoldClick().click();
        page.getSetupDaysClick().click();
        page.getFunctionItem().click();
        page.selectFromDropdown('Flipchart, Easel Client');
        browser.sleep(1500);
        page.getEaselQty().clear();
        page.getEaselQty().sendKeys('1');
        browser.wait(saveButtonIsClickable, 5000);
        page.getSaveButton().click();
        page.checkToastMessage('Function is added successfully');
        page.getShowTotals().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getTotalCount().getText()).toMatch('Total:\n' + preDiscountResult.toString() + '.00');
        page.getCloseButton().click();
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('Event for Function');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getFunctionSearchField().sendKeys('Function Name');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getFunctionTable().getText()).toEqual('Function Space for Function Function Name Exhibitor Jan 1, 2018 Jan 1, 2018');
        page.getEditFunction().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.add-function'))), 5000);
        expect(page.getAccountDropdown().element(by.css('option:checked')).getText()).toMatch('Account for Function');
        expect(page.getEventDropdown().element(by.css('option:checked')).getText()).toMatch('Event for Function');
        expect(page.getEventMasterNo().getAttribute('value')).toContain('1337');
        expect(page.getEventCode().getAttribute('value')).toEqual('7331');
        expect(page.getEventStatus().getAttribute('value')).toEqual('Definite');
        expect(page.getStandardDiscount().getAttribute('value')).toEqual('50');
        expect(page.getSetupDiscount().getAttribute('value')).toEqual('50');
        expect(page.getAltChargePercentage().getAttribute('value')).toEqual('50');
        expect(page.getAltCharge().isSelected()).toBe(true);
        expect(page.getTaxExempt().isSelected()).toBe(true);
        expect(page.getServiceChargeCalculation().isSelected()).toBe(true);
        expect(page.getFunctionDropdown().element(by.css('option:checked')).getText()).toMatch('Function Space for Function');
        expect(page.getLocationNote().getAttribute('value')).toEqual('Location Note');
        expect(page.getFunctionStyle().getAttribute('value')).toEqual('Function Style');
        expect(page.getPlanningContact().getAttribute('value')).toEqual('Planning Contact');
        expect(page.getSalesContact().getAttribute('value')).toEqual('Sales Contact');
        expect(page.getFunctionName().getAttribute('value')).toEqual('Function Name');
        expect(page.getFunctionType().element(by.css('option:checked')).getText()).toMatch('Exhibitor');
        expect(page.getFunctionParticipants().getAttribute('value')).toEqual('10');
        expect(page.getFunctionStartDate().getAttribute('value')).toEqual('2018-01-01');
        expect(page.getFunctionEndDate().getAttribute('value')).toEqual('2018-01-01');
        expect(page.getSetTime().getAttribute('value')).toEqual('03:00');
        expect(page.getStartTime().getAttribute('value')).toEqual('04:00');
        expect(page.getEndTime().getAttribute('value')).toEqual('05:00');
        expect(page.getBEOField().getAttribute('value')).toEqual('BEO');
        expect(page.getWorkflowNoteField().getAttribute('value')).toEqual('Workflow Note');
        expect(page.getClientNoteField().getAttribute('value')).toEqual('Client Note');
        expect(page.getHoursHold().isSelected()).toBe(false);
        expect(page.getSetupDays().isSelected()).toBe(true);
    });

    it('edit added function', () => {

        const itemPrice = 200;
        const serviceCharge = 0.5;
        const taxCharge = 0;
        const disCount = 0.5;

        const postDiscountResult = itemPrice * (1 - disCount) + itemPrice * serviceCharge
                                 + (itemPrice * (1 - disCount) + itemPrice * serviceCharge) * taxCharge;

        page.navigateToAccounts();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('Account for Function');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getEventsInfo().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-events-list'))), 5000);
        page.getEventSearchField().sendKeys('Event for Function');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditEvent().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getFunctionSearchField().sendKeys('Function Name');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditFunction().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.add-function'))), 5000);
        browser.wait(saveButtonIsNotClickable, 5000);
        page.getFunctionDropdown().click();
        page.selectFromDropdown('Function Room');
        page.getFunctionName().sendKeys(' Edited');
        page.getLocationNote().sendKeys(' Edited');
        page.getFunctionType().sendKeys('Affiliate');
        page.getFunctionStyle().sendKeys(' Edited');
        page.getFunctionParticipants().sendKeys('0');
        page.getPlanningContact().sendKeys(' Edited');
        page.getSalesContact().sendKeys(' Edited');
        page.getFunctionStartDate().clear();
        page.getFunctionStartDate().sendKeys('2018-01-02');
        page.getFunctionEndDate().clear();
        page.getFunctionEndDate().sendKeys('2018-01-02');
        page.getEventCode().click();
        page.getSetTime().clear();
        page.getSetTime().sendKeys('04:00');
        page.getStartTime().clear();
        page.getStartTime().sendKeys('05:00');
        page.getEndTime().clear();
        page.getEndTime().sendKeys('06:00');
        page.getBEOField().sendKeys(' Edited');
        page.getWorkflowNoteField().sendKeys(' Edited');
        page.getClientNoteField().sendKeys(' Edited');
        // page.getHoursHoldClick().click();
        page.getSetupDaysClick().click();
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
        page.getFunctionItem().click();
        page.selectFromDropdown('FunctionPackageFlipchart');
        browser.sleep(1500);
        page.getFlipchartPackageQty().clear();
        page.getFlipchartPackageQty().sendKeys('2');
        page.getFlipchartPackageType().click();
        page.selectFromDropdown('N');
        page.getPostDiscount().click();
        browser.wait(saveButtonIsClickable, 5000);
        page.getSaveButton().click();
        page.checkToastMessage('Function is updated successfully');
        page.getShowTotals().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getTotalCount().getText()).toMatch('Total:\n' + postDiscountResult.toString() + '.00');
        page.getCloseButton().click();
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('Event for Function');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getFunctionSearchField().sendKeys('Function Name');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getFunctionTable().getText()).toEqual('Function Room Function Name Edited Affiliate Jan 2, 2018 Jan 2, 2018');
        page.getEditFunction().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.add-function'))), 5000);
        expect(page.getAccountDropdown().element(by.css('option:checked')).getText()).toMatch('Account for Function');
        expect(page.getEventDropdown().element(by.css('option:checked')).getText()).toMatch('Event for Function');
        expect(page.getEventMasterNo().getAttribute('value')).toContain('1337');
        expect(page.getEventCode().getAttribute('value')).toEqual('7331');
        expect(page.getEventStatus().getAttribute('value')).toEqual('Definite');
        expect(page.getStandardDiscount().getAttribute('value')).toEqual('50');
        expect(page.getSetupDiscount().getAttribute('value')).toEqual('50');
        expect(page.getAltChargePercentage().getAttribute('value')).toEqual('50');
        expect(page.getAltCharge().isSelected()).toBe(true);
        expect(page.getTaxExempt().isSelected()).toBe(true);
        expect(page.getServiceChargeCalculation().isSelected()).toBe(false);
        expect(page.getFunctionDropdown().element(by.css('option:checked')).getText()).toMatch('Function Room');
        expect(page.getLocationNote().getAttribute('value')).toEqual('Location Note Edited');
        expect(page.getFunctionStyle().getAttribute('value')).toEqual('Function Style Edited');
        expect(page.getPlanningContact().getAttribute('value')).toEqual('Planning Contact Edited');
        expect(page.getSalesContact().getAttribute('value')).toEqual('Sales Contact Edited');
        expect(page.getFunctionName().getAttribute('value')).toEqual('Function Name Edited');
        expect(page.getFunctionType().element(by.css('option:checked')).getText()).toMatch('Affiliate');
        expect(page.getFunctionParticipants().getAttribute('value')).toEqual('100');
        expect(page.getFunctionStartDate().getAttribute('value')).toEqual('2018-01-02');
        expect(page.getFunctionEndDate().getAttribute('value')).toEqual('2018-01-02');
        expect(page.getSetTime().getAttribute('value')).toEqual('04:00');
        expect(page.getStartTime().getAttribute('value')).toEqual('05:00');
        expect(page.getEndTime().getAttribute('value')).toEqual('06:00');
        expect(page.getBEOField().getAttribute('value')).toEqual('BEO Edited');
        expect(page.getWorkflowNoteField().getAttribute('value')).toEqual('Workflow Note Edited');
        expect(page.getClientNoteField().getAttribute('value')).toEqual('Client Note Edited');
        expect(page.getHoursHold().isSelected()).toBe(false);
        expect(page.getSetupDays().isSelected()).toBe(false);
    });

    it('delete edited function', () => {

        page.navigateToAccounts();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('Account for Function');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getEventsInfo().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-events-list'))), 5000);
        page.getEventSearchField().sendKeys('Event for Function');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditEvent().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getFunctionSearchField().sendKeys('Function Name Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getDeleteFunction().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
    });

});
