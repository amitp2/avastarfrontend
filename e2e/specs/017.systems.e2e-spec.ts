import * as page from '../pages/systems.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Login', () => {

    const EC = protractor.ExpectedConditions;
    const addButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getSystemAddButton()));
    const addButtonIsClickable = EC.elementToBeClickable(page.getSystemAddButton());
    const updateButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getUpdateButton()));
    const updateButtonIsClickable = EC.elementToBeClickable(page.getUpdateButton());
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));

    it('login with organization admin', () => {
        page.loginUserPass('protractoradmn@mailinator.com', '12345678');
    });

    it('Add System', () => {

        page.navigateToSystems();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.edit-systems'))), 5000);
        browser.wait(addButtonIsNotClickable, 5000);
        page.getSystemNameField().sendKeys('System Name');
        page.getSystemDescriptionField().sendKeys('System Description');
        browser.wait(addButtonIsClickable, 5000);
        page.getCategoryDropdown().click();
        page.selectFromDropdown('Presentation Support');
        browser.sleep(2000);
        page.getSubCategoryDropdown().click();
        page.selectFromDropdown('Markers & Easels');
        page.getBarcodeField().sendKeys('Barcode Distribution');
        page.getSearchField().sendKeys('Model Conditioner');
        page.getTagField().sendKeys('0001');
        page.getStatusDropdown().sendKeys('At Repair');
        page.getSearchButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.tbt-left'))), 5000);
        page.getOptionLeftInventory1().click();
        page.getEquipmentAddButton().click();
        page.getSystemAddButton().click();
        page.checkToastMessage('Equipment System added successfully');
        page.getSystemCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getSearchField().sendKeys('Name');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.edit-systems'))), 5000);
        expect(page.getSystemNameField().getAttribute('value')).toEqual('System Name');
        expect(page.getSystemDescriptionField().getAttribute('value')).toEqual('System Description');
    });

    it('Edit System', () => {

        page.navigateToSystems();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getSearchField().sendKeys('System Name');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.edit-systems'))), 5000);
        page.getSystemNameField().sendKeys(' Edited');
        page.getSystemDescriptionField().sendKeys(' Edited');
        page.getCategoryDropdown().click();
        page.selectFromDropdown('Presentation Support');
        browser.sleep(2000);
        page.getSubCategoryDropdown().click();
        page.selectFromDropdown('Markers & Easels');
        page.getBarcodeField().sendKeys('Barcode Accessories');
        page.getSearchField().sendKeys('Model Accessories');
        page.getTagField().sendKeys('0001');
        page.getStatusDropdown().sendKeys('Missing');
        page.getSearchButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.tbt-left'))), 5000);
        page.getOptionLeftInventory2().click();
        page.getEquipmentAddButton().click();
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Equipment System updated successfully');
        page.getSystemCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getSearchField().sendKeys('Name Edited');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.edit-systems'))), 5000);
        expect(page.getSystemNameField().getAttribute('value')).toEqual('System Name Edited');
        expect(page.getSystemDescriptionField().getAttribute('value')).toEqual('System Description Edited');
    });

    it('Delete System', () => {

        page.navigateToSystems();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getSearchField().sendKeys('System Name Edited');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
        page.checkToastMessage('System containing inventory can not be deleted');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.edit-systems'))), 5000);
        page.getOptionRightInventory1().click();
        page.getOptionRightInventory2().click();
        page.getEquipmentRemoveButton().click();
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Equipment System updated successfully');
        page.getSystemCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getSearchField().sendKeys('System Name Edited');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
    });

});
