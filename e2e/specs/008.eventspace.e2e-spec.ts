import * as page from '../pages/eventspace.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Event Spaces', () => {

    const EC = protractor.ExpectedConditions;
    const addButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getAddButton()));
    const addButtonIsClickable = EC.elementToBeClickable(page.getAddButton());
    const updateButtonIsClickable = EC.elementToBeClickable(page.getUpdateButton());
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));

    it('login with organization admin', () => {
        page.loginUserPass('protractoradmn@mailinator.com', '12345678');
    });

    it('Add Event Space', () => {

        page.navigateToEventSpace();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.event-space'))), 5000);
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        browser.wait(addButtonIsNotClickable, 5000);
        expect(page.getFunctionSpace().getAttribute('class')).toEqual('-disabled');
        page.getEventName().sendKeys('Event Name');
        page.getEventStatus().sendKeys('Inactive');
        browser.wait(addButtonIsClickable, 5000);
        page.getAddButton().click();
        page.checkToastMessage('Event Space created successfully');
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.event-space'))), 5000);
        page.getSearchField().sendKeys('Event Name');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getTableItem().getText()).toEqual('Event Name Inactive');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getEventName().getAttribute('value')).toEqual('Event Name');
        expect(page.getEventStatus().element(by.css('option:checked')).getText()).toEqual('Inactive');
    });

    it('Edit Event Space', () => {

        page.navigateToEventSpace();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.event-space'))), 5000);
        page.getSearchField().sendKeys('Event Name');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        browser.wait(updateButtonIsClickable, 5000);
        expect(page.getFunctionSpace().getAttribute('class')).not.toEqual('-disabled');
        page.getEventName().sendKeys(' Edited');
        page.getEventStatus().sendKeys('Active');
        page.getUpdateButton().click();
        page.checkToastMessage('Event Space updated successfully');
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.event-space'))), 5000);
        browser.refresh();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.event-space'))), 5000);
        page.getSearchField().sendKeys('Event Name Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getTableItem().getText()).toEqual('Event Name Edited Active');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getEventName().getAttribute('value')).toEqual('Event Name Edited');
        expect(page.getEventStatus().element(by.css('option:checked')).getText()).toEqual('Active');
    });

    it('Add Function Space', () => {

        page.navigateToEventSpace();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.event-space'))), 5000);
        page.getSearchField().sendKeys('Event Name Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getFunctionSpace().getAttribute('class')).not.toEqual('-disabled');
        page.getFunctionSpace().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getPlusFunction().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        browser.wait(addButtonIsNotClickable, 5000);
        page.getFunctionName().sendKeys('Function Room Name');
        page.getFunctionShortName().sendKeys('Function Short Name');
        page.getFunctionStatus().sendKeys('Inactive');
        browser.wait(addButtonIsClickable, 5000);
        page.getAddButton().click();
        page.checkToastMessage('Function Space created successfully');
        page.getCloseButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.event-space'))), 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getFunctionSpace().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('tbody tr'))), 5000);
        expect(page.getTableFunction().getText()).toEqual('Function Room Name Function Short Name Inactive');
        page.getEditFunction().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getFunctionName().getAttribute('value')).toEqual('Function Room Name');
        expect(page.getFunctionShortName().getAttribute('value')).toEqual('Function Short Name');
        expect(page.getFunctionStatus().getAttribute('value')).toEqual('Inactive');
    });

    it('Edit Function Space', () => {

        page.navigateToEventSpace();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.event-space'))), 5000);
        page.getSearchField().sendKeys('Event Name Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getFunctionSpace().getAttribute('class')).not.toEqual('-disabled');
        page.getFunctionSpace().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getEditFunction().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        browser.wait(updateButtonIsClickable, 5000);
        page.getFunctionName().sendKeys(' Edited');
        page.getFunctionShortName().sendKeys(' Edited');
        page.getFunctionStatus().sendKeys('Active');
        page.getUpdateButton().click();
        page.checkToastMessage('Function Space updated successfully');
        page.getCloseButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.event-space'))), 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getFunctionSpace().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('tbody tr'))), 5000);
        expect(page.getTableFunction().getText()).toEqual('Function Room Name Edited Function Short Name Edited Active');
        page.getEditFunction().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getFunctionName().getAttribute('value')).toEqual('Function Room Name Edited');
        expect(page.getFunctionShortName().getAttribute('value')).toEqual('Function Short Name Edited');
        expect(page.getFunctionStatus().getAttribute('value')).toEqual('Active');
    });

    it('delete event and function space', () => {

        page.navigateToEventSpace();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.event-space'))), 5000);
        page.getSearchField().sendKeys('Event Name Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
        page.checkToastMessage('Event space containing function spaces can not be deleted');
        page.getFunctionDelete().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.event-space'))), 5000);
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
    });

});
