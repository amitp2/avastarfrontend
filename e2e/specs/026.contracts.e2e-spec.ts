import * as page from '../pages/contracts.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Login', () => {

    let originalTimeout;

    const EC = protractor.ExpectedConditions;
    const addButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getAddButton()));
    const addButtonIsClickable = EC.elementToBeClickable(page.getAddButton());
    const updateButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getUpdateButton()));
    const updateButtonIsClickable = EC.elementToBeClickable(page.getUpdateButton());
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));

    beforeEach(() => {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
    });

    afterEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    it('login with organization admin', () => {
        page.loginUserPass('protractoradmn@mailinator.com', '12345678');
    });

    it('add contact terms', () => {

        const moment = require('moment');

        page.navigateToContracts();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        browser.wait(addButtonIsNotClickable, 5000);
        page.getVendorSelect().click();
        page.selectFromDropdown('First Vendor');
        browser.sleep(1500);
        page.getContactSelect().click();
        page.selectFromDropdown('First Contact');
        page.getWithoutDiscountField().sendKeys('1');
        page.getDiscountTo10Field().sendKeys('2');
        page.getDiscountTo20Field().sendKeys('3');
        page.getDiscountTo30Field().sendKeys('4');
        page.getDiscountTo40Field().sendKeys('5');
        page.getDiscountTo50Field().sendKeys('6');
        page.getDiscountFrom50Field().sendKeys('7');
        browser.wait(addButtonIsClickable, 5000);
        page.getAddButton().click();
        page.checkToastMessage('Contract added successfully');
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getVendorSelect().click();
        page.selectFromDropdown('First Vendor');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getContactSelect().click();
        page.selectFromDropdown('First Contact');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getTableItem()
                   .getText())
                   .toEqual('First Vendor First Contact 133-713-3713 email@email ' + moment().format('MMM D, YYYY'));
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getVendorSelect().element(by.css('option:checked')).getText()).toEqual('First Vendor');
        expect(page.getContactSelect().element(by.css('option:checked')).getText()).toEqual('First Contact');
        expect(page.getWithoutDiscountField().getAttribute('value')).toEqual('1');
        expect(page.getDiscountTo10Field().getAttribute('value')).toEqual('2');
        expect(page.getDiscountTo20Field().getAttribute('value')).toEqual('3');
        expect(page.getDiscountTo30Field().getAttribute('value')).toEqual('4');
        expect(page.getDiscountTo40Field().getAttribute('value')).toEqual('5');
        expect(page.getDiscountTo50Field().getAttribute('value')).toEqual('6');
        expect(page.getDiscountFrom50Field().getAttribute('value')).toEqual('7');
    });

    it('update contact terms', () => {

        const path = require('path');
        const fileToUpload = '../src/assets/images/logo-hilton.png',
        absolutePath = path.resolve(__dirname, fileToUpload);

        const fileName1 = '../e2e/downloads/logo-hilton.png';
        const absoluteFile1 = path.resolve(__dirname, fileName1);

        const fs = require('fs');
        const moment = require('moment');

        page.navigateToContracts();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getVendorSelect().click();
        page.selectFromDropdown('First Vendor');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getContactSelect().click();
        page.selectFromDropdown('First Contact');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        browser.wait(updateButtonIsNotClickable, 5000);
        expect(page.getVendorSelectAccess().getAttribute('class')).toBe('inaccessible');
        page.getContactSelect().click();
        page.selectFromDropdown('Second Contact');
        page.getUploadLogo().sendKeys(absolutePath);
        page.checkToastMessage('Document uploaded successfully');
        page.getWithoutDiscountField().clear();
        page.getDiscountTo10Field().clear();
        page.getDiscountTo20Field().clear();
        page.getDiscountTo30Field().clear();
        page.getDiscountTo40Field().clear();
        page.getDiscountTo50Field().clear();
        page.getDiscountFrom50Field().clear();
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Contract updated successfully');
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getVendorSelect().click();
        page.selectFromDropdown('First Vendor');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getContactSelect().click();
        page.selectFromDropdown('Second Contact');
        browser.sleep(1500);
        expect(page.getTableItem()
                   .getText())
                   .toEqual('First Vendor Second Contact 133-713-3713 email@email ' + moment().format('MMM D, YYYY') + ' View');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        expect(page.getVendorSelect().element(by.css('option:checked')).getText()).toEqual('First Vendor');
        expect(page.getContactSelect().element(by.css('option:checked')).getText()).toEqual('Second Contact');
        page.getViewButton().click();
        browser.wait(function() {
            return fs.existsSync(absoluteFile1);
        }, 5000).then(function() {
            fs.unlinkSync(absoluteFile1);
        });
        expect(page.getWithoutDiscountField().getAttribute('value')).toEqual('');
        expect(page.getDiscountTo10Field().getAttribute('value')).toEqual('');
        expect(page.getDiscountTo20Field().getAttribute('value')).toEqual('');
        expect(page.getDiscountTo30Field().getAttribute('value')).toEqual('');
        expect(page.getDiscountTo40Field().getAttribute('value')).toEqual('');
        expect(page.getDiscountTo50Field().getAttribute('value')).toEqual('');
        expect(page.getDiscountFrom50Field().getAttribute('value')).toEqual('');
    });

    it('delete contract', () => {

        page.navigateToContracts();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getVendorSelect().click();
        page.selectFromDropdown('First Vendor');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getContactSelect().click();
        page.selectFromDropdown('Second Contact');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
    });

});
