import * as page from '../pages/charges.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Login', () => {

    const EC = protractor.ExpectedConditions;
    const saveButtonIsClickable = EC.elementToBeClickable(page.getSaveButton());
    const saveButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getSaveButton()));

    it('login with organization admin', () => {
        page.loginUserPass('protractoradmn@mailinator.com', '12345678');
    });

    it('Add Service Charge', () => {

        page.navigateToService();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        browser.wait(saveButtonIsNotClickable, 5000);
        page.getDescriptionField().sendKeys('Service Charge');
        page.getStartDate().click();
        page.getPrevDate().click();
        page.getFirstDate().click();
        page.getEndDate().click();
        page.getNextDate().click();
        page.getLastDate().click();
        page.getServiceCharge().sendKeys('10');
        page.getDescriptionField().click();
        browser.wait(saveButtonIsClickable, 5000);
        page.getSaveButton().click();
        page.checkToastMessage('Service Charge added successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        expect(page.getTableItem().getText()).toContain('10 Active');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getDescriptionField().getAttribute('value')).toEqual('Service Charge');
        expect(page.getServiceCharge().getAttribute('value')).toEqual('10');
    });

    it('Add existing service charge', () => {

        page.navigateToService();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        browser.wait(saveButtonIsNotClickable, 5000);
        page.getDescriptionField().sendKeys('Service Charge');
        page.getStartDate().click();
        page.getPrevDate().click();
        page.getFirstDate().click();
        page.getEndDate().click();
        page.getNextDate().click();
        page.getLastDate().click();
        page.getServiceCharge().sendKeys('10');
        page.getDescriptionField().click();
        browser.wait(saveButtonIsClickable, 5000);
        page.getSaveButton().click();
        page.checkToastMessage('Service charge in specified time period already exists');
    });

    it('Edit Service Charge', () => {

        page.navigateToService();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getDescriptionField().sendKeys(' Edited');
        page.getStartDate().clear();
        page.getStartDate().sendKeys('2000-01-01');
        page.getEndDate().clear();
        page.getEndDate().sendKeys('2000-01-02');
        page.getServiceCharge().clear();
        page.getServiceCharge().sendKeys('100');
        page.getDescriptionField().click();
        browser.wait(saveButtonIsClickable, 5000);
        page.getSaveButton().click();
        page.checkToastMessage('Service Charge updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        expect(page.getTableItem().getText()).toEqual('Service Charge Edited 2000-01-01 2000-01-02 100 Expired');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getDescriptionField().getAttribute('value')).toEqual('Service Charge Edited');
        expect(page.getServiceCharge().getAttribute('value')).toEqual('100');
    });

    it('Delete Service Charge', () => {

        page.navigateToService();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
    });

    it('Add Tax Charge', () => {

        page.navigateToTax();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        browser.wait(saveButtonIsNotClickable, 5000);
        page.getDescriptionField().sendKeys('Tax Charge');
        page.getStartDate().click();
        page.getPrevDate().click();
        page.getFirstDate().click();
        page.getEndDate().click();
        page.getNextDate().click();
        page.getLastDate().click();
        page.getServiceCharge().sendKeys('10');
        page.getDescriptionField().click();
        browser.wait(saveButtonIsClickable, 5000);
        page.getSaveButton().click();
        page.checkToastMessage('Tax Charge added successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        expect(page.getTableItem().getText()).toContain('10 Active');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getDescriptionField().getAttribute('value')).toEqual('Tax Charge');
        expect(page.getServiceCharge().getAttribute('value')).toEqual('10');
    });

    it('Add existing tax charge', () => {

        page.navigateToTax();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        browser.wait(saveButtonIsNotClickable, 5000);
        page.getDescriptionField().sendKeys('Tax Charge');
        page.getStartDate().click();
        page.getPrevDate().click();
        page.getFirstDate().click();
        page.getEndDate().click();
        page.getNextDate().click();
        page.getLastDate().click();
        page.getServiceCharge().sendKeys('10');
        page.getDescriptionField().click();
        browser.wait(saveButtonIsClickable, 5000);
        page.getSaveButton().click();
        page.checkToastMessage('Tax charge in specified time period already exists');
    });


    it('Edit Tax Charge', () => {

        page.navigateToTax();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getDescriptionField().sendKeys(' Edited');
        page.getStartDate().clear();
        page.getStartDate().sendKeys('2000-01-01');
        page.getEndDate().clear();
        page.getEndDate().sendKeys('2000-01-02');
        page.getServiceCharge().clear();
        page.getServiceCharge().sendKeys('100');
        page.getDescriptionField().click();
        browser.wait(saveButtonIsClickable, 5000);
        page.getSaveButton().click();
        page.checkToastMessage('Tax Charge updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        expect(page.getTableItem().getText()).toEqual('Tax Charge Edited 2000-01-01 2000-01-02 100 Expired');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getDescriptionField().getAttribute('value')).toEqual('Tax Charge Edited');
        expect(page.getServiceCharge().getAttribute('value')).toEqual('100');
    });

    it('Delete Tax Charge', () => {

        page.navigateToTax();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
    });

});
