import * as page from '../pages/accounts.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Subscribers', () => {

    let originalTimeout;

    const EC = protractor.ExpectedConditions;
    const addButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getAddButton()));
    const addButtonIsClickable = EC.elementToBeClickable(page.getAddButton());
    const updateButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getUpdateButton()));
    const updateButtonIsClickable = EC.elementToBeClickable(page.getUpdateButton());
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));

    beforeEach(() => {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
    });

    afterEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    it('login with organization admin', () => {
        page.loginUserPass('protractoradmn@mailinator.com', '12345678');
    });

    it('add new account', () => {

        const path = require('path');
        const fileToUpload = '../../src/assets/images/logo-hilton.png',
        absolutePath = path.resolve(__dirname, fileToUpload);

        page.navigateToAccounts();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        browser.wait(addButtonIsNotClickable, 5000);
        expect(page.getContactPerson().getAttribute('class')).toEqual('-disabled');
        expect(page.getEventsInfo().getAttribute('class')).toEqual('-disabled');
        page.getUploadLogo().sendKeys(absolutePath);
        page.checkToastMessage('Account logo uploaded successfully');
        page.getCompanyField().sendKeys('01AOrg');
        page.getAddressField().sendKeys('2717 Clearview Drive');
        page.getAddressNdField().sendKeys('3788 Despard Street');
        page.getCityField().sendKeys('Atlanta');
        page.getPostCodeField().sendKeys('30303');
        page.getCountrySelect().click();
        page.selectFromDropdown('United States');
        browser.sleep(1500);
        page.getStateSelect().click();
        page.selectFromDropdown('Georgia');
        page.getPhoneField().sendKeys('133-713-3713');
        browser.wait(addButtonIsClickable, 5000);
        page.getAddButton().click();
        page.checkToastMessage('Account added successfully');
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrg');
        browser.wait(spinnerIsNotVisible, 5000);
        expect(page.getTableItem().getText()).toEqual('01AOrg Client Active');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getCompanyField().getAttribute('value')).toEqual('01AOrg');
        expect(page.getAddressField().getAttribute('value')).toEqual('2717 Clearview Drive');
        expect(page.getAddressNdField().getAttribute('value')).toEqual('3788 Despard Street');
        expect(page.getCityField().getAttribute('value')).toEqual('Atlanta');
        expect(page.getPostCodeField().getAttribute('value')).toEqual('30303');
        expect(page.getCountrySelect().element(by.css('option:checked')).getText()).toEqual('United States');
        expect(page.getStateSelect().element(by.css('option:checked')).getText()).toEqual('Georgia');
        expect(page.getPhoneField().getAttribute('value')).toEqual('133-713-3713');
    });

    it('edit added account', () => {

        page.navigateToAccounts();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrg');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        browser.wait(updateButtonIsClickable, 5000);
        page.getCompanyField().sendKeys(' Edited');
        page.getAddressField().sendKeys(' Edited');
        page.getAddressNdField().sendKeys(' Edited');
        page.getCityField().sendKeys(' Edited');
        page.getPostCodeField().sendKeys('03');
        page.getCountrySelect().click();
        page.selectFromDropdown('Germany');
        page.getOfficeField().sendKeys('Atlanta');
        page.getFaxField().sendKeys('404-555-0135');
        page.getWebsiteField().sendKeys('www.01a.org');
        page.getPhoneField().sendKeys(' Edited');
        page.getStatusToggle().click();
        page.getMasterCheckbox().click();
        page.getStandardDiscount().sendKeys('10');
        page.getSetupDiscount().sendKeys('10');
        page.getAltCharge().click();
        page.getAltChargeField().sendKeys('10');
        page.getTaxExempt().click();
        page.getPreDiscount().click();
        page.getUpdateButton().click();
        page.checkToastMessage('Account updated successfully');
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        expect(page.getTableItem().getText()).toEqual('01AOrg Edited Client Inactive');
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getCompanyField().getAttribute('value')).toEqual('01AOrg Edited');
        expect(page.getAddressField().getAttribute('value')).toEqual('2717 Clearview Drive Edited');
        expect(page.getAddressNdField().getAttribute('value')).toEqual('3788 Despard Street Edited');
        expect(page.getCityField().getAttribute('value')).toEqual('Atlanta Edited');
        expect(page.getPostCodeField().getAttribute('value')).toEqual('3030303');
        expect(page.getCountrySelect().element(by.css('option:checked')).getText()).toEqual('Germany');
        expect(page.getOfficeField().getAttribute('value')).toEqual('Atlanta');
        expect(page.getFaxField().getAttribute('value')).toEqual('404-555-0135');
        expect(page.getPhoneField().getAttribute('value')).toEqual('133-713-3713');
        expect(page.getWebsiteField().getAttribute('value')).toEqual('www.01a.org');
        expect(page.getStandardDiscount().getAttribute('value')).toEqual('10');
        expect(page.getSetupDiscount().getAttribute('value')).toEqual('10');
        expect(page.getAltChargeField().getAttribute('value')).toEqual('10');
    });

    it('add contact person', () => {

        page.navigateToAccounts();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrg Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getContactPerson().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getContactFname().sendKeys('John');
        page.getContactLname().sendKeys('Doe');
        page.getContactPhone().sendKeys('404-555-0176');
        page.getContactEmail().sendKeys('john@ doe');
        browser.wait(updateButtonIsNotClickable, 5000);
        page.getContactEmail().clear();
        page.getContactEmail().sendKeys('john@doe');
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Contact person information has been updated');
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getContactButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getContactFname().getAttribute('value')).toEqual('John');
        expect(page.getContactLname().getAttribute('value')).toEqual('Doe');
        expect(page.getContactPhone().getAttribute('value')).toEqual('404-555-0176');
        expect(page.getContactEmail().getAttribute('value')).toEqual('john@doe');
    });

    it('edit contact person', () => {

        page.navigateToAccounts();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('01AOrg Edited');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getContactButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getContactFname().sendKeys(' Edited');
        page.getContactLname().sendKeys(' Edited');
        page.getContactPhone().sendKeys(' Edited');
        page.getContactEmail().sendKeys(' Edited');
        page.getContactDepartment().sendKeys('Department');
        page.getContactTitle().sendKeys('Title');
        page.getContactFax().sendKeys('Fax');
        page.getContactMobile().sendKeys('133-713-3713');
        page.getContactNote().sendKeys('Note');
        browser.wait(updateButtonIsNotClickable, 5000);
        page.getContactEmail().clear();
        page.getContactEmail().sendKeys('john@doeEdited');
        browser.wait(updateButtonIsClickable, 5000);
        page.getUpdateButton().click();
        page.checkToastMessage('Contact person information has been updated');
        page.getCancelButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getContactButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        expect(page.getContactFname().getAttribute('value')).toEqual('John Edited');
        expect(page.getContactLname().getAttribute('value')).toEqual('Doe Edited');
        expect(page.getContactPhone().getAttribute('value')).toEqual('404-555-0176');
        expect(page.getContactEmail().getAttribute('value')).toEqual('john@doeEdited');
        expect(page.getContactDepartment().getAttribute('value')).toEqual('Department');
        expect(page.getContactTitle().getAttribute('value')).toEqual('Title');
        expect(page.getContactFax().getAttribute('value')).toEqual('Fax');
        expect(page.getContactMobile().getAttribute('value')).toEqual('133-713-3713');
        expect(page.getContactNote().getAttribute('value')).toEqual('Note');
    });

    it('delete added subscriber and user', () => {

        page.navigateToAccounts();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getDeleteButton().click();
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
        browser.restart();
    });

});
