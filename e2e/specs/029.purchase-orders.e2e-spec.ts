import * as page from '../pages/purchase.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Login', () => {

    let originalTimeout;

    const EC = protractor.ExpectedConditions;
    const saveButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getSaveButton()));
    const saveButtonIsClickable = EC.elementToBeClickable(page.getSaveButton());
    const addButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getAddButton()));
    const addButtonIsClickable = EC.elementToBeClickable(page.getAddButton());
    const updateButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getUpdateButton()));
    const updateButtonIsClickable = EC.elementToBeClickable(page.getUpdateButton());
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));

    beforeEach(() => {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
    });

    afterEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    it('login with organization admin', () => {
        page.loginUserPass('protractoradmn@mailinator.com', '12345678');
    });

    it('add purchase order', () => {

        const moment = require('moment');

        page.navigateToOrders();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getAddNew().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.content-block'))), 5000);
        browser.wait(saveButtonIsNotClickable, 5000);
        page.getVendorSelect().click();
        page.selectFromDropdown('First Vendor');
        browser.sleep(1500);
        page.getVendorContactSelect().click();
        page.selectFromDropdown('First Contact');
        page.getMonthSelect().click();
        page.selectFromDropdown('January');
        page.getYearSelect().click();
        page.selectFromDropdown('2000');
        page.getStatusSelect().sendKeys('Paid');
        page.getTypeSelect().sendKeys('Purchase');
        browser.wait(saveButtonIsClickable, 5000);
        page.getAddItemButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.id('equipment-item-form'))), 5000);
        browser.wait(addButtonIsNotClickable, 5000);
        page.getInventoryCategorySelect().click();
        page.selectFromDropdown('Presentation Support');
        browser.sleep(1500);
        page.getInventorySubCategorySelect().click();
        page.selectFromDropdown('Markers & Easels');
        browser.sleep(1500);
        page.getInventorySelect().click();
        page.selectFromDropdown('Flipchart, Paper Pad');
        page.checkToastMessage('Selected equipment item is not defined in the system');
        page.getInventorySelect().click();
        page.selectFromDropdown('Flipchart, Easel');
        page.getArriveDate().sendKeys(moment().format('YYYY-MM-DD'));
        page.getPickupDate().sendKeys(moment().format('YYYY-MM-DD'));
        page.getHeaderText().click();
        page.getRentalDays().sendKeys('10');
        page.getQuantityField().sendKeys('10');
        page.getGuestCharge().sendKeys('2000');
        expect(page.getUnitField().getAttribute('value')).toEqual('100');
        expect(page.getTotalCost().getAttribute('value')).toEqual('1000.00');
        expect(page.getProfitCurrency().getAttribute('value')).toEqual('1000.00');
        expect(page.getProfitPercent().getAttribute('value')).toEqual('50.00');
        browser.wait(addButtonIsClickable, 5000);
        page.getAddButton().click();
        expect(page
            .getTableItem()
            .getText())
            .toEqual('Flipchart, Easel May 2, 2018	May 2, 2018 12 10 100.00 1,000.00 2,000.00 1,000.00 50');
        page.getSaveButton().click();
        page.checkToastMessage('Purchase order added successfully');
    });

});
