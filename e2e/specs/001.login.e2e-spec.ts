import * as page from '../pages/login.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Login', () => {

    beforeEach(() => {
        page.navigateToLogin();
    });

    it('go to login page', () => {
        expect(page.getEmailField().getAttribute('placeholder')).toEqual('E-mail address');
        expect(page.getPasswordField().getAttribute('placeholder')).toEqual('Password');
        expect(page.getLoginButton().getAttribute('value')).toEqual('Sign In');
        expect(page.getForgotButton().getText()).toEqual('Forgot Password');
        expect(page.getRememberMe().getText()).toEqual('Remember me');
    });

    it('login without entering either email or password', () => {
        page.getLoginButton().click();
        page.checkToastMessage('Please enter your email and password');
    });

    it('login without entering email', () => {
        page.getPasswordField().sendKeys('12345678');
        page.getLoginButton().click();
        page.checkToastMessage('Please enter your email and password');
    });

    it('login without entering password', () => {
        page.getEmailField().sendKeys('protractorusr@mailinator.com');
        page.getLoginButton().click();
        page.checkToastMessage('Please enter your email and password');
    });

    it('login with invalid email', () => {
        page.getEmailField().sendKeys('protractorusr@!mailinator.com');
        page.getPasswordField().sendKeys('12345678');
        page.getLoginButton().click();
        page.checkToastMessage('There was a problem with your email or password. Please try again');

    });

    it('login with non existing user', () => {
        page.getEmailField().sendKeys('nonexist@mailinator.com');
        page.getPasswordField().sendKeys('12345678');
        page.getLoginButton().click();
        page.checkToastMessage('There was a problem with your email or password. Please try again');
    });

    it('login with invalid password', () => {
        page.getEmailField().sendKeys('protractorusr@mailinator.com');
        page.getPasswordField().sendKeys('1234');
        page.getLoginButton().click();
        page.checkToastMessage('There was a problem with your email or password. Please try again');
    });

    it('login with with incorrect password 3 times', () => {
        expect(page.getEmailField().sendKeys('login3times@mailinator.com'));
        expect(page.getPasswordField().sendKeys('123456789'));
        page.getLoginButton().click();
        page.checkToastMessage('It looks like your password has been entered incorrectly too many times');
    });

    it('login with inactive user', () => {
        page.getEmailField().sendKeys('inactiveusr@mailinator.com');
        page.getPasswordField().sendKeys('12345678');
        page.getLoginButton().click();
        page.checkToastMessage('There was a problem with your email or password. Please try again');
    });

    it('login with user that does not have access to the product', () => {
        page.getEmailField().sendKeys('noaccessuser@mailinator.com');
        page.getPasswordField().sendKeys('12345678');
        page.getLoginButton().click();
        page.checkToastMessage('There was a problem with your email or password. Please try again');
    });

    it('login with user that subscriber does not have access to the product', () => {
        page.getEmailField().sendKeys('noaccesssubscriber@mailinator.com');
        page.getPasswordField().sendKeys('12345678');
        page.getLoginButton().click();
        page.checkToastMessage('There was a problem with your email or password. Please try again');
    });

    it('login with not verified user', () => {
        page.getEmailField().sendKeys('notverifieduser@mailinator.com');
        page.getPasswordField().sendKeys('12345678');
        page.getLoginButton().click();
        page.checkToastMessage('There was a problem with your email or password. Please try again');
    });

    it('login with user that subscriber is inactive', () => {
        page.getEmailField().sendKeys('inactivesubuser@mailinator.com');
        page.getPasswordField().sendKeys('12345678');
        page.getLoginButton().click();
        page.checkToastMessage('There was a problem with your email or password. Please try again');
    });

    it('login with valid credentials', () => {
        page.getEmailField().sendKeys('protractorusr@mailinator.com');
        page.getPasswordField().sendKeys('12345678');
        page.getLoginButton().click();
        page.checkToastMessage('Login was successful');
    });

});
