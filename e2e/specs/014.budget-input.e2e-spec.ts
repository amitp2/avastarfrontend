import * as page from '../pages/budget.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Login', () => {

    const EC = protractor.ExpectedConditions;
    const saveButtonIsClickable = EC.elementToBeClickable(page.getSaveButton());
    const saveButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getSaveButton()));
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));

    it('login with organization admin', () => {
        page.loginUserPass('protractoradmn@mailinator.com', '12345678');
    });

    it('Update Revenue Budget', () => {

        page.navigateToBudget();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getYearDropdown().click();
        page.selectFromDropdown('2000');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getGroupsDropdown().sendKeys('Revenue');
        browser.wait(spinnerIsNotVisible, 5000);
        browser.wait(saveButtonIsNotClickable, 5000);
        page.getJanuary().clear();
        page.getJanuary().sendKeys('1');
        page.getFebruary().clear();
        page.getFebruary().sendKeys('2');
        page.getMarch().clear();
        page.getMarch().sendKeys('3');
        page.getApril().clear();
        page.getApril().sendKeys('4');
        page.getMay().clear();
        page.getMay().sendKeys('5');
        page.getJune().clear();
        page.getJune().sendKeys('6');
        page.getJuly().clear();
        page.getJuly().sendKeys('7');
        page.getAugust().clear();
        page.getAugust().sendKeys('8');
        page.getSeptember().clear();
        page.getSeptember().sendKeys('9');
        page.getOctober().clear();
        page.getOctober().sendKeys('10');
        page.getNovember().clear();
        page.getNovember().sendKeys('11');
        page.getDecember().clear();
        page.getDecember().sendKeys('12');
        expect(page.getAnnual().getText()).toEqual('78');
        browser.wait(saveButtonIsClickable, 5000);
        page.getSaveButton().click();
        page.checkToastMessage('Input budget updated successfully');
    });

    it('Update Cost Budget', () => {

        page.navigateToBudget();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
        page.getYearDropdown().click();
        page.selectFromDropdown('2000');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getGroupsDropdown().sendKeys('Cost');
        browser.wait(spinnerIsNotVisible, 5000);
        browser.wait(saveButtonIsNotClickable, 5000);
        page.getJanuary().clear();
        page.getJanuary().sendKeys('1');
        page.getFebruary().clear();
        page.getFebruary().sendKeys('2');
        page.getMarch().clear();
        page.getMarch().sendKeys('3');
        page.getApril().clear();
        page.getApril().sendKeys('4');
        page.getMay().clear();
        page.getMay().sendKeys('5');
        page.getJune().clear();
        page.getJune().sendKeys('6');
        page.getJuly().clear();
        page.getJuly().sendKeys('7');
        page.getAugust().clear();
        page.getAugust().sendKeys('8');
        page.getSeptember().clear();
        page.getSeptember().sendKeys('9');
        page.getOctober().clear();
        page.getOctober().sendKeys('10');
        page.getNovember().clear();
        page.getNovember().sendKeys('11');
        page.getDecember().clear();
        page.getDecember().sendKeys('12');
        expect(page.getAnnual().getText()).toEqual('78');
        browser.wait(saveButtonIsClickable, 5000);
        page.getSaveButton().click();
        page.checkToastMessage('Input budget updated successfully');
    });

});
