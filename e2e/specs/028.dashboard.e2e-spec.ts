import * as page from '../pages/dashboard.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Login', () => {

    let originalTimeout;

    const EC = protractor.ExpectedConditions;
    const saveButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getSaveButton()));
    const saveButtonIsClickable = EC.elementToBeClickable(page.getSaveButton());
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));

    beforeEach(() => {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
    });

    afterEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    it('login with organization admin', () => {
        page.loginUserPass('protractoradmn@mailinator.com', '12345678');
    });

    it('check today\'s event', () => {

        const moment = require('moment');

        page.navigateToEvents();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.records-table'))), 5000);
        page.getSearchField().sendKeys('Event for Dashboard');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
        page.getSearchField().sendKeys('Function for Dashboard');
        browser.wait(spinnerIsNotVisible, 5000);
        page.getEditButton().click();
        browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.add-function'))), 5000);
        page.getFunctionStartDate().clear();
        page.getFunctionStartDate().sendKeys(moment().format('YYYY-MM-DD'));
        page.getFunctionEndDate().clear();
        page.getFunctionEndDate().sendKeys(moment().format('YYYY-MM-DD'));
        page.getEventCode().click();
        page.getSetTime().clear();
        page.getSetTime().sendKeys('00:00');
        page.getStartTime().clear();
        page.getStartTime().sendKeys('00:00');
        page.getEndTime().clear();
        page.getEndTime().sendKeys('00:00');
        browser.wait(saveButtonIsClickable, 5000);
        page.getSaveButton().click();
        page.checkToastMessage('Function is updated successfully');
        page.navigateToDashboard();
        browser.wait(protractor.ExpectedConditions.presenceOf(element(by.css('app-dashboard'))), 5000);
        browser.ignoreSynchronization = true;
        expect(page.getEventBlockHeader()
        .getText())
        .toEqual('Today\'s Events');
        page.getEventBlockSearch().sendKeys('Event for Dashboard');
        browser.sleep(1500);
        expect(page.getEventBlockBody().getText()).toEqual('Event for Dashboard Account for Function 1');
        browser.ignoreSynchronization = false;
    });

    it('check service tickets', () => {

        page.navigateToDashboard();
        browser.wait(protractor.ExpectedConditions.presenceOf(element(by.css('app-dashboard'))), 5000);
        browser.ignoreSynchronization = true;
        expect(page.getTicketBlockHeader()
        .getText())
        .toEqual('Service Tickets');
        page.getTicketBlockSearch().sendKeys('Ticket for Dashboard');
        browser.sleep(1500);
        expect(page.getTicketBlockBody().getText()).toEqual('Ticket for Dashboard Property Contact Open');
        browser.ignoreSynchronization = false;
    });

});
