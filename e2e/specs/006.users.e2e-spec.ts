import * as page from '../pages/users.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Users', () => {

    const EC = protractor.ExpectedConditions;
    const addButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getAddButton()));
    const addButtonIsClickable = EC.elementToBeClickable(page.getAddButton());
    const updateButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getUpdateButton()));
    const updateButtonIsClickable = EC.elementToBeClickable(page.getUpdateButton());
    const randVal = Date.now();

    let originalTimeout;

    beforeEach(() => {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
    });

    afterEach(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    it('login with super admin', () => {
      page.loginUserPass('pgagnidze@productsavvy.com', '12345678');
    });

    it('add new user', () => {

      page.navigateToUsers();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getAddNew().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      browser.wait(addButtonIsNotClickable, 5000);
      page.getUserFname().sendKeys('01AJane');
      page.getUserLname().sendKeys('Doe');
      page.getUserPhone().sendKeys('404-555-0177');
      page.getUserEmail().sendKeys(randVal + '@mailinator. com');
      page.getSubscriberDropdown().click();
      page.selectFromDropdown('Protractor Organization Admin');
      browser.wait(addButtonIsNotClickable, 5000);
      page.getUserEmail().clear();
      page.getUserEmail().sendKeys(randVal + '@mailinator.com');
      browser.wait(addButtonIsClickable, 5000);
      page.getAddButton().click();
      page.checkToastMessage('Subscriber user created successfully');
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().sendKeys('01AJane');
      browser.sleep(1500);
      expect(page.getTableItem()
                 .getText())
                 .toContain('01AJane Doe Protractor Organization Admin ' + randVal + '@mailinator.com Admin Not Verified');
      page.getEditButton().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      expect(page.getUserFname().getAttribute('value')).toEqual('01AJane');
      expect(page.getUserLname().getAttribute('value')).toEqual('Doe');
      expect(page.getUserPhone().getAttribute('value')).toEqual('404-555-0177');
      expect(page.getUserEmail().getAttribute('disabled')).toBe('true');
      expect(page.getSubscriberDropdownAccess().getAttribute('class')).toBe('inaccessible');
      expect(page.getUserEmail().getAttribute('value')).toEqual(randVal + '@mailinator.com');
      expect(page.getSubscriberDropdown().element(by.css('option:checked')).getText()).toEqual('Protractor Organization Admin');
    });

    it('edit added user', () => {

      page.navigateToUsers();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().sendKeys('01AJane');
      browser.sleep(1500);
      page.getEditButton().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      page.getUserFname().sendKeys(' Edited');
      page.getUserLname().sendKeys(' Edited');
      page.getUserPhone().sendKeys(' Edited');
      page.getUserTitle().sendKeys('Title');
      page.getUserFax().sendKeys('Fax');
      page.getUserMobile().sendKeys('133-7133-713');
      page.getUserNotes().sendKeys('Notes');
      page.getUserRoleField().click();
      page.getUserRolePick().click();
      browser.wait(updateButtonIsNotClickable, 5000);
      page.getUserRoleField().click();
      page.getUserRolePick().click();
      browser.wait(updateButtonIsClickable, 5000);
      page.getUpdateButton().click();
      page.checkToastMessage('Subscriber user updated successfully');
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().sendKeys('01AJane');
      browser.sleep(1500);
      page.getEditButton().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      expect(page.getUserFname().getAttribute('value')).toEqual('01AJane Edited');
      expect(page.getUserLname().getAttribute('value')).toEqual('Doe Edited');
      expect(page.getUserPhone().getAttribute('value')).toEqual('404-555-0177');
      expect(page.getUserFax().getAttribute('value')).toEqual('Fax');
      expect(page.getUserMobile().getAttribute('value')).toEqual('133-713-3713');
      expect(page.getUserEmail().getAttribute('disabled')).toBe('true');
      expect(page.getUserNotes().getAttribute('value')).toEqual('Notes');
      expect(page.getSubscriberDropdownAccess().getAttribute('class')).toBe('inaccessible');
      expect(page.getUserEmail().getAttribute('value')).toContain('@mailinator.com');
      expect(page.getSubscriberDropdown().element(by.css('option:checked')).getText()).toEqual('Protractor Organization Admin');
    });

    it('delete added user', () => {

      page.navigateToUsers();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().sendKeys('01AJane');
      browser.sleep(1500);
      page.getDeleteButton().click();
      browser.wait(EC.alertIsPresent(), 5000);
      browser.switchTo().alert().accept();
      browser.restart();
  });

});
