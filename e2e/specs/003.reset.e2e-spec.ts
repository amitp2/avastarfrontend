import * as page from '../pages/reset.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Reset', () => {

    beforeEach(() => {
        page.navigateToReset();
    });

    it('go to reset password page', () => {
        expect(page.getPasswordField().getAttribute('placeholder')).toEqual('Password');
        expect(page.getPasswordConfirm().getAttribute('placeholder')).toEqual('Confirm Password');
        expect(page.getChangeButton().getAttribute('value')).toEqual('Change Password');
    });

    it('Change password without entering password either in new password or confirm new password fields', () => {
        page.getChangeButton().click();
        page.checkToastMessage('Please enter your new password');
    });

    it('Change password without entering password in confirm new password field', () => {
        page.getPasswordField().sendKeys('12345678');
        page.getChangeButton().click();
        page.checkToastMessage('Passwords do not match');
    });

    it('Change password without entering password in new password field', () => {
        page.getPasswordConfirm().sendKeys('12345678');
        page.getChangeButton().click();
        page.checkToastMessage('Please enter your new password');
    });

    it('Change password with entering password that is less than 8 characters only in new password field', () => {
        page.getPasswordField().sendKeys('1234');
        page.getChangeButton().click();
        page.checkToastMessage('Please try again and make sure your password is at least 8 characters long.');
    });

    it('Change password with entering password that is less than 8 characters only in confirm new password field', () => {
        page.getPasswordConfirm().sendKeys('1234');
        page.getChangeButton().click();
        page.checkToastMessage('Please enter your new password');
    });

    it('enter new password that is less then 8 characters and do not match', () => {
        page.getPasswordField().sendKeys('12345');
        page.getPasswordConfirm().sendKeys('1234');
        page.getChangeButton().click();
        page.checkToastMessage('Please try again and make sure your password is at least 8 characters long.');
    });

    it('enter new password that is less then 8 characters', () => {
        page.getPasswordField().sendKeys('1234');
        page.getPasswordConfirm().sendKeys('1234');
        page.getChangeButton().click();
        page.checkToastMessage('Please try again and make sure your password is at least 8 characters long.');
    });

    it('enter different values in new password and confirm password fields', () => {
        page.getPasswordField().sendKeys('12345678');
        page.getPasswordConfirm().sendKeys('123456789');
        page.getChangeButton().click();
        page.checkToastMessage('Passwords do not match');
    });

    it('reset password with already used link', () => {
        page.getPasswordField().sendKeys('12345678');
        page.getPasswordConfirm().sendKeys('12345678');
        page.getChangeButton().click();
        page.checkToastMessage('Your password reset link is not valid, or already used');
    });

});
