import * as page from '../pages/forgot.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Forgot', () => {

    beforeEach(() => {
        page.navigateToForgot();
    });

    it('go to forgot password page', () => {
        expect(page.getEmailField().getAttribute('placeholder')).toEqual('E-mail address');
        expect(page.getResetButton().getAttribute('value')).toEqual('Reset Password');
    });

    it('reset password without entering email address', () => {
        page.getResetButton().click();
        page.checkToastMessage('Please enter your email');
    });

    it('Reset password with entering invalid email address', () => {
        page.getEmailField().sendKeys('protractor@!mailinator.com');
        page.getResetButton().click();
        page.checkToastMessage('There was a problem with your email address. Please try again');
    });

    it('enter non existing email address and press reset password button', () => {
        page.getEmailField().sendKeys('nonexist@mailinator.com');
        page.getResetButton().click();
        page.checkToastMessage('There was a problem with your email address. Please try again');
    });

    it('enter valid email address and press reset password button', () => {
        page.getEmailField().sendKeys('login3times@mailinator.com');
        page.getResetButton().click();
        page.checkToastMessage('Link for password reset has been sent to your email');
    });

});
