import * as page from '../pages/item-categories.page';
import { protractor, browser, by, element } from 'protractor';

describe('avastar-web Users', () => {

    let originalTimeout;

    const EC = protractor.ExpectedConditions;
    const addButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getAddButton()));
    const addButtonIsClickable = EC.elementToBeClickable(page.getAddButton());
    const updateButtonIsNotClickable = EC.not(EC.elementToBeClickable(page.getUpdateButton()));
    const updateButtonIsClickable = EC.elementToBeClickable(page.getUpdateButton());
    const spinnerIsNotVisible = EC.not(EC.visibilityOf(page.getSpinner()));

    beforeEach(() => {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 250000;
    });

    afterEach(() => {
      jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    it('login with super admin', () => {
      page.loginUserPass('pgagnidze@productsavvy.com', '12345678');
    });

    it('add new category', () => {

      page.navigateToItem();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getAddNew().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      browser.wait(addButtonIsNotClickable, 5000);
      page.getCategoryName().sendKeys('Category');
      browser.wait(addButtonIsClickable, 5000);
      page.getAddButton().click();
      page.checkToastMessage('Category added successfully');
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().sendKeys('Category');
      browser.wait(spinnerIsNotVisible, 5000);
      expect(page.getTableItem().getText()).toEqual('Category');
      page.getEditCategory().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      expect(page.getCategoryName().getAttribute('value')).toEqual('Category');
    });

    it('add existing category', () => {

      page.navigateToItem();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getAddNew().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      browser.wait(addButtonIsNotClickable, 5000);
      page.getCategoryName().sendKeys('Category');
      browser.wait(addButtonIsClickable, 5000);
      page.getAddButton().click();
      page.checkToastMessage('Category with same name already exists');
    });

    it('add new sub-category', () => {

      page.navigateToItem();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().sendKeys('Category');
      browser.wait(spinnerIsNotVisible, 5000);
      page.getAddSubCategory().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      browser.wait(addButtonIsNotClickable, 5000);
      page.getCategoryName().sendKeys('Sub Category');
      browser.wait(addButtonIsClickable, 5000);
      page.getAddButton().click();
      page.checkToastMessage('Sub category added successfully');
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().clear();
      page.getSearchField().sendKeys('Category');
      browser.wait(spinnerIsNotVisible, 5000);
      page.getShowSubCategory().click();
      browser.wait(spinnerIsNotVisible, 5000);
      page.getEditSubCategory().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      expect(page.getCategoryName().getAttribute('value')).toEqual('Sub Category');
    });

    it('add existing sub-category', () => {

      page.navigateToItem();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().sendKeys('Category');
      browser.wait(spinnerIsNotVisible, 5000);
      page.getAddSubCategory().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      browser.wait(addButtonIsNotClickable, 5000);
      page.getCategoryName().sendKeys('Sub Category');
      browser.wait(addButtonIsClickable, 5000);
      page.getAddButton().click();
      page.checkToastMessage('Sub category with same name already exists');
    });

    it('add new item category', () => {

      page.navigateToItem();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().sendKeys('Category');
      browser.wait(spinnerIsNotVisible, 5000);
      page.getShowSubCategory().click();
      browser.wait(spinnerIsNotVisible, 5000);
      page.getAddItemCategory().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      browser.wait(addButtonIsNotClickable, 5000);
      page.getCategoryName().sendKeys('Item Category');
      browser.wait(addButtonIsClickable, 5000);
      page.getAddButton().click();
      page.checkToastMessage('Equipment/Service item added successfully');
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().clear();
      page.getSearchField().sendKeys('Category');
      browser.wait(spinnerIsNotVisible, 5000);
      page.getShowSubCategory().click();
      browser.wait(spinnerIsNotVisible, 5000);
      page.getShowItemCategory().click();
      browser.wait(spinnerIsNotVisible, 5000);
      page.getEditItemCategory().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      expect(page.getCategoryName().getAttribute('value')).toEqual('Item Category');
    });

    it('add existing item category', () => {

      page.navigateToItem();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().sendKeys('Category');
      browser.wait(spinnerIsNotVisible, 5000);
      page.getShowSubCategory().click();
      browser.wait(spinnerIsNotVisible, 5000);
      page.getAddItemCategory().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      browser.wait(addButtonIsNotClickable, 5000);
      page.getCategoryName().sendKeys('Item Category');
      browser.wait(addButtonIsClickable, 5000);
      page.getAddButton().click();
      page.checkToastMessage('Equipment/Service with same name already exists');
    });

    it('update categories', () => {

      page.navigateToItem();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().sendKeys('Category');
      browser.wait(spinnerIsNotVisible, 5000);
      page.getEditCategory().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      browser.wait(updateButtonIsNotClickable, 5000);
      page.getCategoryName().sendKeys(' Edited');
      browser.wait(updateButtonIsClickable, 5000);
      page.getUpdateButton().click();
      page.checkToastMessage('Category updated successfully');
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      expect(page.getTableItem().getText()).toEqual('Category Edited');
      page.getEditCategory().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      expect(page.getCategoryName().getAttribute('value')).toEqual('Category Edited');
      page.getCancelButton().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getShowSubCategory().click();
      browser.wait(spinnerIsNotVisible, 5000);
      page.getEditSubCategory().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      browser.wait(updateButtonIsNotClickable, 5000);
      page.getCategoryName().sendKeys(' Edited');
      browser.wait(updateButtonIsClickable, 5000);
      page.getUpdateButton().click();
      page.checkToastMessage('Sub Category updated successfully');
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getShowSubCategory().click();
      browser.wait(spinnerIsNotVisible, 5000);
      page.getEditSubCategory().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      expect(page.getCategoryName().getAttribute('value')).toEqual('Sub Category Edited');
      page.getCancelButton().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getShowSubCategory().click();
      browser.wait(spinnerIsNotVisible, 5000);
      page.getShowItemCategory().click();
      browser.wait(spinnerIsNotVisible, 5000);
      page.getEditItemCategory().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      browser.wait(updateButtonIsNotClickable, 5000);
      page.getCategoryName().sendKeys(' Edited');
      browser.wait(updateButtonIsClickable, 5000);
      page.getUpdateButton().click();
      page.checkToastMessage('Equipment/Service item updated successfully');
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getShowSubCategory().click();
      browser.wait(spinnerIsNotVisible, 5000);
      page.getShowItemCategory().click();
      browser.wait(spinnerIsNotVisible, 5000);
      page.getEditItemCategory().click();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.popup-body'))), 5000);
      expect(page.getCategoryName().getAttribute('value')).toEqual('Item Category Edited');
    });

    it('delete categories', () => {

      page.navigateToItem();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().sendKeys('Category Edited');
      browser.wait(spinnerIsNotVisible, 5000);
      page.getDeleteButton().click();
      browser.wait(EC.alertIsPresent(), 5000);
      browser.switchTo().alert().accept();
      browser.wait(protractor.ExpectedConditions.visibilityOf(element(by.css('.complex-table'))), 5000);
      page.getSearchField().clear();
      page.getSearchField().sendKeys('Presentation Support');
      browser.wait(spinnerIsNotVisible, 5000);
      page.getDeleteButton().click();
      browser.wait(EC.alertIsPresent(), 5000);
      browser.switchTo().alert().accept();
      page.checkToastMessage('Category used in system');
    });

});
