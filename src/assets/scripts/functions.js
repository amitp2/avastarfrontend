(function($){
$(document).ready(function(){

//Dashboard custom scrollbars
$('.db-block-table-body').mCustomScrollbar({
	axis: 'y',
	scrollButtons: { enable: false },
	theme: 'dark-2',
});

//Mobile menu
$('#nav input').click(function(){
	$(this).parent('#nav').toggleClass('-visible');
});


//popup tabs
var tabContainers = $('.popup-tabs-container > div');
	tabContainers.hide().filter(':first').show();

$('.popup-tabs-nav a').click(function(){
	tabContainers.hide();
	tabContainers.filter(this.hash).show();
	$('.popup-tabs-nav a').removeClass('-current');
	$(this).addClass('-current');
	return false;
}).filter(':first').click();

//Show popup
$('.account-management .records-table .icon.-dots').click(function(e){
	e.preventDefault();
	$('.popup-shader').show();
	$('.popup').addClass('-visible');
});
//hide popup
$('.popup-close, .popup-shader').click(function(e){
	e.preventDefault();
	$('.popup-shader').hide();
	$('.popup').removeClass('-visible');
});

//popup users tab
$('.popup-users-list .icon.-dots').click(function(e){
	e.preventDefault();
	$('.popup-users-list').hide();
	$('.popup-users-update').show();
});
$('.popup-users-list .red-plus').click(function(e){
	e.preventDefault();
	$('.popup-users-list').hide();
	$('.popup-users-add-new').show();
});
$('.popup-body-header-back').click(function(e){
	e.preventDefault();
	$('.popup-users-list').show();
	$('.popup-users-update, .popup-users-add-new').hide();
});




});
})(jQuery);