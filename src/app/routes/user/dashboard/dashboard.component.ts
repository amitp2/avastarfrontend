import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { UtilsService } from '../../../services/utils.service';
import { CurrentUser } from '../../../interfaces/current-user';
import { UserRole } from '../../../enums/user-role.enum';
import { TicketResourceService } from '../../../services/resources/ticket-resource.service';
import { CurrentPropertyService } from '../../../services/current-property.service';
import { TicketModel, TicketStatus } from '../../../models/ticket-model';
import * as moment from 'moment';
import { FormControl } from '@angular/forms';
import { VenueEventModel } from '../../../models/venue-event-model';
import { VenueEventResourceService } from '../../../services/resources/venue-event-resource.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.styl']
})
export class DashboardComponent implements OnInit {
    tickets: TicketModel[] = [];
    events: VenueEventModel[] = [];
    userRole: typeof UserRole = UserRole;
    ticketSearchControl: FormControl = new FormControl();
    eventSearchControl: FormControl = new FormControl();

    get completedTicketCount() {
        return this.tickets.filter(x => x.ticketStatus === TicketStatus.Completed).length;
    }

    get unCompletedTickets() {
        return this.tickets.filter(x => x.ticketStatus !== TicketStatus.Completed);
    }


    constructor(
        private authService: AuthService,
        private router: Router,
        private utilsService: UtilsService,
        private tickerService: TicketResourceService,
        private eventService: VenueEventResourceService,
        private currentVenue: CurrentPropertyService
    ) {
        this.authService.currentUser().then((user: CurrentUser) => {
            if (this.utilsService.userHasRole(user, UserRole.SUPER_ADMIN)) {
                this.router.navigateByUrl('/u/subscribers');
            }

            if (this.utilsService.userHasAnyRole(user, [UserRole.ADMIN, UserRole.TECH_TEAM_LEAD, UserRole.TECH_TEAM, UserRole.SERVICE_TEAM])) {
                this.loadTickets();
                this.ticketSearchControl.valueChanges.subscribe(
                    this.loadTickets.bind(this)
                );
            }

            if (!this.utilsService.userHasRole(user, UserRole.SERVICE_TEAM)) {
                this.loadEvents();
                this.eventSearchControl.valueChanges.subscribe(
                    this.loadEvents.bind(this)
                );
            }
        });
    }

    ngOnInit() {

    }

    private loadTickets() {
        
        const query: any = {
            ...this.tickerService.getAllQueryObject(),
            otherParams: {}
            // otherParams: {
            //     from: moment().format('YYYY/MM/DD'),
            //     to: moment().add(1, 'd').format('YYYY/MM/DD')
            // }
        };
        if (this.ticketSearchControl.value) {
            query.otherParams.search = this.ticketSearchControl.value;
        }
        this.currentVenue.currentVenueAsync()
            .then(venue => this.tickerService.queryByVenueId(query, venue.id))
            .then(tickets => {
               // 
                this.tickets = tickets.items;
                // for (let i = 0; i <= tickets.items.length; i++) {
                //     if (tickets.items[i].ticketStatus != TicketStatus.Completed) {
                //         this.unCompletedTicket.push(tickets.items[i])
                //     }
                // }

            });
    }

    private loadEvents() {
        this.currentVenue.currentVenueAsync()
            .then(venue => this.eventService.queryAllOfTodayEventsByVenueId(venue.id, this.eventSearchControl.value || null))
            .then(events => {
                this.events = events;
            });
    }

}
