import { Component } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AsyncTasksService } from '../../../services/async-tasks.service';
import { AppError } from '../../../enums/app-error.enum';
import { AppSuccess } from '../../../enums/app-success.enum';

@Component({
    selector: 'app-reset',
    templateUrl: './reset.component.html',
    styleUrls: ['./reset.component.styl']
})
export class ResetComponent {

    resetForm: FormGroup;
    resetPasswordTask: string;
    token: string;

    constructor(private authService: AuthService,
                private activatedRoute: ActivatedRoute,
                private asyncTasksService: AsyncTasksService,
                private router: Router) {
        this.resetForm = new FormGroup({
            password: new FormControl(null, [
                Validators.required,
                Validators.minLength(8)
            ]),
            confirmPassword: new FormControl(null, [Validators.required])
        }, this.passwordConfirming);
        this.resetPasswordTask = 'RESET_PASSWORD';
        this.token = this.activatedRoute.snapshot.params.token;
    }

    passwordConfirming(c: AbstractControl): ValidationErrors | any {
        if (c.get('password').value !== c.get('confirmPassword').value) {
            return {confirm: true};
        }
        return null;
    }

    onSubmit() {
        if (this.resetForm.invalid) {
            if (this.resetForm.get('password').hasError('required')) {
                this.asyncTasksService.taskError(this.resetPasswordTask, AppError.PASSWORD_IS_REQUIRED);
            } else if (this.resetForm.get('password').hasError('minlength')) {
                this.asyncTasksService.taskError(this.resetPasswordTask, AppError.PASSWORD_MING_LENGTH);
            } else if (this.resetForm.hasError('confirm')) {
                this.asyncTasksService.taskError(this.resetPasswordTask, AppError.PASSWORDS_DO_NOT_MATCH);
            }
            return;
        }

        this.asyncTasksService.taskStart(this.resetPasswordTask);
        this.authService
            .resetPassword(this.token, this.resetForm.controls.password.value)
            .then(() => {

                setTimeout(() => {
                    this.router.navigateByUrl('/');
                }, 5000);

                this.asyncTasksService.taskSuccess(this.resetPasswordTask, AppSuccess.RESET_PASSWORD);
            })
            .catch(error => {
                this.asyncTasksService.taskError(this.resetPasswordTask, error);
            });
    }
}
