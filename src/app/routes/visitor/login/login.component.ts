import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AsyncTasksService } from '../../../services/async-tasks.service';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { AppSuccess } from '../../../enums/app-success.enum';
import { AppError } from '../../../enums/app-error.enum';
import { AsyncMethod } from '../../../decorators/async-method.decorator';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.styl']
})
export class LoginComponent {

    loginForm: FormGroup;
    loginFormSubmitted: boolean;

    constructor(private asyncTasksService: AsyncTasksService,
                private authService: AuthService,
                private router: Router) {
        this.loginForm = new FormGroup({
            username: new FormControl(null, [Validators.required]),
            password: new FormControl(null, [Validators.required]),
            rememberMe: new FormControl(false)
        });
        this.loginFormSubmitted = false;
    }

    submit() {
        const userLoginTask = 'USER_LOGIN';
        this.loginFormSubmitted = true;

        if (this.loginForm.invalid) {
            this.asyncTasksService.taskError(userLoginTask, AppError.LOGIN_EMAIL_AND_PASSWORD_REQUIRED);
            return;
        }

        this.login(this.loginForm.value.username, this.loginForm.value.password, this.loginForm.value.rememberMe)
            .then(() => {
                this.loginFormSubmitted = false;
                this.loginForm.reset();
                this.router.navigateByUrl('/u');
            });
    }

    @AsyncMethod({
        taskName: 'USER_LOGIN',
        success: AppSuccess.USER_LOGIN
    })
    login(username: string, password: string, rememberMe: any) {
        return this.authService.login(
            username,
            password,
            rememberMe
        );
    }

}
