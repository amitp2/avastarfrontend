import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { AsyncTasksService } from '../../../services/async-tasks.service';
import { AppSuccess } from '../../../enums/app-success.enum';
import { AppError } from '../../../enums/app-error.enum';

@Component({
    selector: 'app-forgot',
    templateUrl: './forgot.component.html',
    styleUrls: ['./forgot.component.styl']
})
export class ForgotComponent {

    forgotForm: FormGroup;

    constructor(private authService: AuthService,
                private asyncTasksService: AsyncTasksService) {
        this.forgotForm = new FormGroup({
            email: new FormControl(null, [Validators.required])
        });
    }

    onSubmit() {
        const forgotTask = 'FORGOT_PASSWORD';

        if (this.forgotForm.invalid) {
            this.forgotForm.get('email').markAsTouched();
            this.asyncTasksService.taskError(forgotTask, AppError.FORGOT_PASSWORD_EMAIL_REQUIRED);
            return;
        }

        this.asyncTasksService.taskStart(forgotTask);

        this.authService.forgotPassword(this.forgotForm.controls.email.value)
            .then(() => {
                this.forgotForm.get('email').setValue('');
                this.asyncTasksService.taskSuccess(forgotTask, AppSuccess.FORGOT_PASSWORD);
            })
            .catch((error) => {
                this.asyncTasksService.taskError(forgotTask, error);
            });
    }

}
