export interface PagingTableLoadResult {
    total: number;
    items: any[];
}
