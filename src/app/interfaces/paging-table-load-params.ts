export interface PagingTableLoadParams {
    page: number;
    itemsPerPage: number;
    search: string;
    sortColumn: string;
    sortAsc: boolean;
}
