export interface PagingTableHeaderColumn {
    isSortable: boolean;
    title: string;
    key: string;
}
