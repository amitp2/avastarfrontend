export interface HttpResourceQueryParams {
    page: number;
    size: number;
    sortFields: string[];
    sortAsc: boolean;
    search?: string;
    otherParams?: any;
}
