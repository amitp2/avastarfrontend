export type AutocompleteLoader = (term: string) => Promise<any[]>;
