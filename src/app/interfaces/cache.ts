export abstract class AppCache {

    abstract set(key: string, value: any): Promise<any>;

    abstract get(key: string): Promise<any>;

    abstract remove(key: string): Promise<any>;

    abstract hasKey(key: string): Promise<boolean>;

}
