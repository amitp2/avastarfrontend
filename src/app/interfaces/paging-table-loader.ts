import { PagingTableLoadResult } from './paging-table-load-result';
import { PagingTableLoadParams } from './paging-table-load-params';

export interface PagingTableLoader {
    (loadParams: PagingTableLoadParams): Promise<PagingTableLoadResult>;
}
