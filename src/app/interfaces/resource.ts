import { ResourceQueryResponse } from './resource-query-response';

export interface Resource<T> {
    query(params: any): Promise<ResourceQueryResponse<T>>;
    edit(id: number, model: T): Promise<T>;
    add(model: T): Promise<T>;
    delete(id: number): Promise<void>;
    get(id: number): Promise<T>;
}
