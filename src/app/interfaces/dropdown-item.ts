export interface DropdownItem {
    value: any;
    display: any;
    disabled?: boolean;
}
