export type MapperFunction<F, T> = (from: F) => T;
