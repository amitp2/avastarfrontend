import { DropdownItem } from './dropdown-item';

export type DropdownLoader = () => Promise<DropdownItem[]>;
