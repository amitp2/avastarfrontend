export interface ResourceQueryResponse<T> {
    total: number;
    items: T[];
}
