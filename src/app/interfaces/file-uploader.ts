import { Observable } from 'rxjs/Observable';
import { FileUploaderProgress } from './file-uploader-progress';
import { AppSuccess } from '../enums/app-success.enum';
import { AllowedFileType } from '../enums/allowed-file-type.enum';
import { AppError } from '../enums/app-error.enum';

export interface FileUploader {

    upload(file: any): Observable<FileUploaderProgress>;

    asyncTaskName(): string;

    uploadAppSuccess(): AppSuccess;

    uploadAppError?(): AppError;

    allowedFileType(): AllowedFileType;

}
