import { ObjectTransformerType } from '../enums/object-transformer-type.enum';

export interface ObjectTransformer {
    [propName: string]: {
        type?: ObjectTransformerType;
        to: string;
        [propName: string]: any
    };
}
