export type Cloner<T> = (f: T) => T;
