import { UserRole } from '../enums/user-role.enum';
export interface CurrentUser {
    id: number;
    firstName: string;
    username: string;
    roles: UserRole[];
    subscriberId?: number;
}
