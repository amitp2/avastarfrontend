export interface FileUploaderProgress {
    completed: number;
    fileUrl: string;
}
