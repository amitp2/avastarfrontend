import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { UtilsService } from '../../../../services/utils.service';
import { StorageSpaceResourceService } from '../../../../services/resources/storage-space-resource.service';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { CurrentPropertyService } from '../../../../services/current-property.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { StorageSpaceModel } from '../../../../models/storage-space-model';
import { AppError } from '../../../../enums/app-error.enum';
import { VenueModel } from '../../../../models/venue-model';
import { ActivatedRoute } from '@angular/router';
import { TableHandler } from '../../../../classes/table-handler';

@Component({
    selector: 'app-storage-spaces',
    templateUrl: './storage-spaces.component.html',
    styleUrls: ['./storage-spaces.component.styl']
})
export class StorageSpacesComponent extends TableHandler {

    @ViewChild('table') table: PagingTableComponent;

    columns: PagingTableHeaderColumn[] = [
        {isSortable: true, title: 'Storage Space', key: 'name'},
        {isSortable: true, title: 'Status', key: 'status'},
        {isSortable: false, title: 'Actions', key: 'actions'}
    ];

    constructor(private utilsService: UtilsService,
                private storageSpace: StorageSpaceResourceService,
                private currentProperty: CurrentPropertyService,
                activatedRoute: ActivatedRoute) {
        super(activatedRoute);
    }

    getTable(): PagingTableComponent {
        return this.table;
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return this.currentProperty.currentVenueAsync()
                .then((venue: VenueModel) => {
                    return this.storageSpace.queryByVenue(
                        this.utilsService.tableToHttpParams(params),
                        venue.id
                    );
                });
        }).bind(this);
    }

    @AsyncMethod({
        taskName: 'event-space-delete',
        error: AppError.STORAGE_SPACE_DELETE
    })
    delete(model: StorageSpaceModel): Promise<void> {
        return this.storageSpace.delete(model.id).then(() => {
            this.table.refresh();
        });
    }


}
