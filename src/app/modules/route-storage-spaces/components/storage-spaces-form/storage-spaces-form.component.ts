import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageSpaceStatus } from '../../../../enums/storage-space-status.enum';
import { FormHandler } from '../../../../classes/form-handler';
import { StorageSpaceModel } from '../../../../models/storage-space-model';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { StorageSpaceResourceService } from '../../../../services/resources/storage-space-resource.service';
import { UtilsService } from '../../../../services/utils.service';
import { ObjectTransformerType } from '../../../../enums/object-transformer-type.enum';
import { CurrentPropertyService } from '../../../../services/current-property.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';

@Component({
    selector: 'app-storage-spaces-form',
    templateUrl: './storage-spaces-form.component.html',
    styleUrls: ['./storage-spaces-form.component.styl']
})
export class StorageSpacesFormComponent extends FormHandler<StorageSpaceModel> {


    storageSpaceStatus = StorageSpaceStatus;

    constructor(private activatedRoute: ActivatedRoute,
                private storageSpace: StorageSpaceResourceService,
                private utilsService: UtilsService,
                private currentProperty: CurrentPropertyService,
                private router: Router) {
        super();
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    initializeForm(): FormGroup {
        return new FormGroup({
            name: new FormControl(null, [Validators.required]),
            status: new FormControl(StorageSpaceStatus.Active),
            notes: new FormControl()
        });
    }

    get(id: number): Promise<StorageSpaceModel> {
        return this.storageSpace.get(id);
    }

    @AsyncMethod({
        taskName: 'storage_space_add',
        success: AppSuccess.STORAGE_SPACE_ADD,
        error: AppError.STORAGE_SPACE_ADD
    })
    add(model: StorageSpaceModel): Promise<any> {
        return this
            .storageSpace
            .addForVenue(model, this.currentProperty.snapshot.id)
            .then((added: StorageSpaceModel) => {
                this.router.navigateByUrl(`/u/storage-spaces/${added.id}?fresh=${Date.now()}`);
                // this.router.navigateByUrl(`/u/storage-spaces?fresh=${Date.now()}`);
            });
    }

    @AsyncMethod({
        taskName: 'storage_space_edit',
        success: AppSuccess.STORAGE_SPACE_EDIT,
        error: AppError.STORAGE_SPACE_EDIT
    })
    edit(id: number, model: StorageSpaceModel): Promise<any> {
        return this.storageSpace
            .edit(id, model)
            .then(() => {
                // this.router.navigateByUrl(`/u/storage-spaces?fresh=${Date.now()}`);
            });
    }

    serialize(model: StorageSpaceModel): any {
        return this.utilsService.transformInto(model, {
            name: {
                type: ObjectTransformerType.Default,
                to: 'name'
            },
            status: {
                type: ObjectTransformerType.Default,
                to: 'status'
            },
            notes: {
                to: 'notes'
            }
        });
    }

    deserialize(formValue: any): StorageSpaceModel {
        return new StorageSpaceModel(this.utilsService.transformInto(formValue, {
            name: {
                type: ObjectTransformerType.Default,
                to: 'name'
            },
            status: {
                type: ObjectTransformerType.Default,
                to: 'status'
            },
            notes: {
                to: 'notes'
            }
        }));
    }

}
