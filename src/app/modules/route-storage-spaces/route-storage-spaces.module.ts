import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { RouterModule, Routes } from '@angular/router';
import { StorageSpacesComponent } from './components/storage-spaces/storage-spaces.component';
import { StorageSpacesFormComponent } from './components/storage-spaces-form/storage-spaces-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AllowOnlyService } from '../../services/guards/allow-only.service';
import { UserRole } from '../../enums/user-role.enum';

const routes: Routes = [
    {
        path: '',
        component: StorageSpacesComponent,
        canActivate: [AllowOnlyService],
        data: {
            roles: [UserRole.ADMIN, UserRole.TECH_TEAM_LEAD]
        },
        children: [
            {path: ':id', component: StorageSpacesFormComponent}
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        PagingTableModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule
    ],
    declarations: [
        StorageSpacesComponent,
        StorageSpacesFormComponent
    ]
})
export class RouteStorageSpacesModule {
}
