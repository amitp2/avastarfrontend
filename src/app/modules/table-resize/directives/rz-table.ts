import { AfterContentInit, ContentChildren, Directive, ElementRef, Input, QueryList } from '@angular/core';
import { GlobalsService } from '../../../services/globals.service';

import '../col-resizable';

@Directive({
    selector: '[appRzColGroup]'
})
export class RzColGroupDirective {
    constructor(public elementRef: ElementRef) {}
}

@Directive({
    selector: '[appRzTable]'
})
export class RzTableDirective implements AfterContentInit {
    @ContentChildren(RzColGroupDirective) colGroups: QueryList<RzColGroupDirective>;
    private $: any;

    @Input() rzDisabledColumns: number[] = [];
    @Input('appRzTable') tableName: string;

    get disabledBodyColumns() {
        return this.rzDisabledColumns.map(value => value === 0 ? value : value + 1);
    }

    constructor(private globals: GlobalsService, private elementRef: ElementRef) {
        this.$ = globals.globals().$;
    }

    get columns() {
        return this.$(this.elementRef.nativeElement).find('th');
    }

    get table() {
        return this.$(this.elementRef.nativeElement).find('table').first();
    }

    ngAfterContentInit() {
        setTimeout(() => {
            this.table.colResizable({
                fixed: true,
                liveDrag: true,
                gripInnerHtml: '<div class=\'grip2\'></div>',
                draggingClass: 'dragging',
                minWidth: 100,
                headerOnly: true,
                disabledColumns: this.rzDisabledColumns,
                onDrag: () => {
                    this.onEndDrag();
                }
            });

            this.$(this.elementRef.nativeElement).find('table').addClass('JColResizer');

            if (this.tableName) {
                this.loadSaved();
            }
        }, 2000);
    }

    onEndDrag() {
        // Calculates the percent width of each column
        const totWidth = this.table.outerWidth();
        const callbacks = [];

        this.columns.each((index, column) => {
            if (this.disabledBodyColumns.indexOf(index) > -1) {
                return;
            }
            const colWidth = this.$(column).outerWidth();
            const percentWidth = (colWidth / totWidth * 100).toFixed(1) + '%';
            console.log(colWidth, percentWidth, totWidth)
            callbacks.push(() => {
                this.setWidthForColgroup(index, percentWidth);
            });
        });

        // Apply the calculated width of every column
        callbacks.map(cb => cb());
    }

    setWidthForColgroup(index, percentWidth) {
        const groups = this.colGroups.toArray();
        groups[0].elementRef.nativeElement.children[index].setAttribute('width', percentWidth);
        if (groups.length === 2) {
            groups[1].elementRef.nativeElement.children[index].setAttribute('width', percentWidth);
            groups[1].elementRef.nativeElement.children[index].setAttribute('style',
                groups[0].elementRef.nativeElement.children[index].getAttribute('style')
            );
        }
    }

    loadSaved() {

    }
}

