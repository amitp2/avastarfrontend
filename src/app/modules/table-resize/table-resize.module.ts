import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RzColGroupDirective, RzTableDirective } from './directives/rz-table';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        RzTableDirective,
        RzColGroupDirective
    ],
    exports: [
        RzTableDirective,
        RzColGroupDirective
    ]
})
export class TableResizeModule {
}
