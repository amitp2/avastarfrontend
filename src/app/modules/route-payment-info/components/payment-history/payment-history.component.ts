import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { SubscriptionStatus, SubscriptionStatusNames, subscriptionStatusToName } from '../../../../enums/subscription-type';
import { ActivatedRoute } from '@angular/router';
import { SubscriberPaymentHistoryService } from '../../../../services/resources/subscriber-payment-history.service';
import { UtilsService } from '../../../../services/utils.service';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { PagingTableService } from '../../../../services/paging-table.service';

@Component({
    selector: 'app-payment-history',
    templateUrl: './payment-history.component.html',
    styleUrls: ['./payment-history.component.styl'],
    providers: [
        PagingTableService
    ]
})
export class PaymentHistoryComponent {
    private SubscriptionStatus: typeof SubscriptionStatus = SubscriptionStatus;
    private SubscriptionStatusNames: typeof SubscriptionStatusNames = SubscriptionStatusNames;
    private _subscriberId: number;
    private prevParams: any;
    @ViewChild('table')
    private table: PagingTableComponent;

    @Input()
    set subscriberId(id: number) {
        this._subscriberId = id;
        if (this.table) {
            this.table.refresh();
        }
    }

    get subscriberId() {
        return this._subscriberId;
    }

    constructor(
        private utils: UtilsService,
        private historyApi: SubscriberPaymentHistoryService
    ) {
    }

    load = (params) => {
        if (!params && this.prevParams) {
            params = this.prevParams;
        }
        if (params) {
            this.prevParams = params;
        }
        if (!this.subscriberId || !params) {
            return Promise.resolve([]);
        }
        params.sortAsc = false;
        const httpParams = this.utils.tableToHttpParams(params);
        return this.historyApi.queryForSubscriber(httpParams, this.subscriberId);
    }

    getStatusName(status: SubscriptionStatus) {
        return subscriptionStatusToName(status);
    }
}
