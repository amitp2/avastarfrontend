import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import {
    PaymentMethod,
    PaymentMethodNames,
    RenewalPeriod,
    RenewalPeriodNames,
    SubscriptionStatus,
    SubscriptionStatusNames,
    SubscriptionType,
    SubscriptionTypeNames
} from '../../../../enums/subscription-type';
import { SubscriberResourceService } from '../../../../services/resources/subscriber-resource.service';
import { Router } from '@angular/router';
import { InitialInvoiceUploaderService } from '../../../route-subscribers/services/initial-invoice-uploader.service';
import { ContractUploaderService } from '../../../route-subscribers/services/contract-uploader.service';
import { Location } from '@angular/common';
import { AuthService } from '../../../../services/auth.service';

@Component({
    selector: 'app-payment-info',
    templateUrl: './payment-info.component.html',
    styleUrls: ['./payment-info.component.styl']
})
export class PaymentInfoComponent implements OnInit {
    private form: FormGroup;
    private subscriberId: number;
    private PaymentMethod: typeof PaymentMethod = PaymentMethod;
    private RenewalPeriod: typeof RenewalPeriod = RenewalPeriod;
    private SubscriptionType: typeof SubscriptionType = SubscriptionType;
    private SubscriptionStatus: typeof SubscriptionStatus = SubscriptionStatus;

    private PaymentMethodNames: typeof PaymentMethodNames = PaymentMethodNames;
    private RenewalPeriodNames: typeof RenewalPeriodNames = RenewalPeriodNames;
    private SubscriptionTypeNames: typeof SubscriptionTypeNames = SubscriptionTypeNames;
    private SubscriptionStatusNames: typeof SubscriptionStatusNames = SubscriptionStatusNames;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private authApi: AuthService,
        private subscriberApi: SubscriberResourceService,
        private contractUploader: ContractUploaderService,
        private invoiceUploader: InitialInvoiceUploaderService
    ) {
        this.buildForm();
    }

    ngOnInit() {
        this.loadInfo();
    }

    buildForm() {
        this.form = this.fb.group({
            accountType: [null, [Validators.required]],
            renewalPeriod: [null, [Validators.required]],
            creationDate: [null],
            subscriptionDate: [null, [Validators.required]],
            contractUrl: [null],
            amountDue: [null, [Validators.required]],
            implementationFee: [null, [Validators.required]],
            initialInvoiceUrl: [null],
            paymentMethod: [null, [Validators.required]],
            status: [null, [Validators.required]],
            customerPo: [null, [Validators.required]],
            contractNumber: [null, [Validators.required]],
            nextPaymentDate: [null]
        });
    }

    @AsyncMethod({
        taskName: 'get_payment_info'
    })
    async loadInfo() {
        const subscriberId = (await this.authApi.currentUser()).subscriberId;
        this.subscriberId = subscriberId;
        const paymentInfo = await this.subscriberApi.getPaymentInfo(subscriberId);

        this.form.patchValue({ ...paymentInfo });
        this.form.disable();
        this.form.markAsPristine();
    }
}
