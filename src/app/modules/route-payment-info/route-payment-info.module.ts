import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentHistoryComponent } from './components/payment-history/payment-history.component';
import { PaymentInfoComponent } from './components/payment-info/payment-info.component';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ContractUploaderService } from '../route-subscribers/services/contract-uploader.service';
import { InitialInvoiceUploaderService } from '../route-subscribers/services/initial-invoice-uploader.service';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { MaskedInputModule } from '../masked-input/masked-input.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        ReactiveFormsModule,
        PagingTableModule,
        MaskedInputModule,
        RouterModule.forChild([
            {
                path: '',
                component: PaymentInfoComponent
            }
        ])
    ],
    declarations: [
        PaymentInfoComponent,
        PaymentHistoryComponent
    ],
    providers: [
        ContractUploaderService,
        InitialInvoiceUploaderService
    ]
})
export class RoutePaymentInfoModule {
}
