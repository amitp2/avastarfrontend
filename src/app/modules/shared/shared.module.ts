import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileUploaderComponent } from './components/form-controls/file-uploader/file-uploader.component';
import { ConfirmClickDirective } from '../../directives/confirm-click.directive';
import { FileUploaderResultDirective } from '../../directives/form-controls/file-uploader/file-uploader-result.directive';
import { FileUploaderProgressDirective } from '../../directives/form-controls/file-uploader/file-uploader-progress.directive';
import { CountryListDirective } from './directives/country-list.directive';
import { StateListDirective } from './directives/state-list.directive';
import { FormModeToTextPipe } from './pipes/form-mode-to-text.pipe';
import { UserRoleToTextPipe } from './pipes/user-role-to-text.pipe';
import { TrackClickDirective } from '../../directives/track-click.directive';
import { AsyncTaskErrorMessageComponent } from '../../components/async-tasks/async-task-error-message/async-task-error-message.component';
import { UserRolesToTextPipe } from './pipes/user-roles-to-text.pipe';
import { AllowDisableDirective } from './directives/allow-disable.directive';
import { TopBarAndHeaderComponent } from './components/layout/top-bar-and-header/top-bar-and-header.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import { CurrentUserDirective } from './directives/current-user.directive';
import { LogoutClickDirective } from './directives/logout-click.directive';
import { CustomScrollbarDirective } from './directives/custom-scrollbar.directive';
import { RouterModule } from '@angular/router';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { AllowShowDirective } from './directives/allow-show.directive';
import { ColgroupForWidthsComponent } from './components/layout/colgroup-for-widths/colgroup-for-widths.component';
import { PropertyLogoComponent } from './components/property-logo/property-logo.component';
import { VenueLogoComponent } from './components/layout/venue-logo/venue-logo.component';
import { UserStatusToTextPipe } from './pipes/user-status-to-text.pipe';
import { AddHttpToUrlPipe } from './pipes/add-http-to-url.pipe';
import { AddClassAfterTimeDirective } from './directives/add-class-after-time.directive';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Select2Directive } from './directives/select2.directive';
import { MultiSelectComponent } from './components/form-controls/multi-select/multi-select.component';
import { DynamicDropdownComponent } from './components/form-controls/dynamic-dropdown/dynamic-dropdown.component';
import { DynamicAutocompleteComponent } from './components/form-controls/dynamic-autocomplete/dynamic-autocomplete.component';
import { CalendarComponent } from './components/form-controls/calendar/calendar.component';
import { VendorCreationButtonComponent } from './components/vendor-creation-button/vendor-creation-button.component';
import { NumberInputDirective } from '../../directives/number-input.directive';
import { InventoryCategoryDropdownComponent } from './components/form-controls/inventory-category-dropdown/inventory-category-dropdown.component';
import { InventorySubCategoryDropdownComponent } from './components/form-controls/inventory-sub-category-dropdown/inventory-sub-category-dropdown.component';
import { InventoryStorageLocationDropdownComponent } from './components/form-controls/inventory-storage-location-dropdown/inventory-storage-location-dropdown.component';
import { TinymceComponent } from './components/form-controls/tinymce/tinymce.component';
import { AccountDropdownComponent } from './components/form-controls/account-dropdown/account-dropdown.component';
import { EventDropdownComponent } from './components/form-controls/event-dropdown/event-dropdown.component';
import { AccountCreationButtonComponent } from './components/account-creation-button/account-creation-button.component';
import { EventCreationButtonComponent } from './components/event-creation-button/event-creation-button.component';
import { TimePickerComponent } from './components/form-controls/time-picker/time-picker.component';
import { DpDatePickerModule } from 'ng2-date-picker';
import { VendorDropdownComponent } from './components/form-controls/vendor-dropdown/vendor-dropdown.component';
import { VendorContactDropdownComponent } from './components/form-controls/vendor-contact-dropdown/vendor-contact-dropdown.component';
import { InventoryDropdownComponent } from './components/form-controls/inventory-dropdown/inventory-dropdown.component';
import { SystemDropdownComponent } from './components/form-controls/system-dropdown/system-dropdown.component';
import { InventorySubCategoryItemDropdownComponent } from './components/form-controls/inventory-sub-category-item-dropdown/inventory-sub-category-item-dropdown.component';
import { FunctionSpaceDropdownComponent } from './components/form-controls/function-space-dropdown/function-space-dropdown.component';
import { MonthDropdownComponent } from './components/form-controls/month-dropdown/month-dropdown.component';
import { YearDropdownComponent } from './components/form-controls/year-dropdown/year-dropdown.component';
import { VendorContactCreateButtonComponent } from './components/vendor-contact-create-button/vendor-contact-create-button.component';
import { DisableControlDirective } from './directives/disable-control.directive';
import { EquipmentDropdownComponent } from './components/form-controls/equipment-dropdown/equipment-dropdown.component';
import { InventorySubCategoryItemsDropdownComponent } from './components/form-controls/inventory-sub-category-items-dropdown/inventory-sub-category-items-dropdown.component';
import { KeysPipe } from './pipes/keys.pipe';
import { ValueChangesDirective } from './directives/value-changes.directive';
import { DateTimePickerComponent } from './components/form-controls/date-time-picker/date-time-picker.component';
import { PurchaseOrderDropdownComponent } from './components/form-controls/purchase-order-dropdown/purchase-order-dropdown.component';
import { CostCategoryDropdownComponent } from './components/form-controls/cost-category-dropdown/cost-category-dropdown.component';
import { VenueContactDropdownComponent } from './components/form-controls/venue-contact-dropdown/venue-contact-dropdown.component';
import { KeepHtmlPipe } from './pipes/keep-html.pipe';
import { UserAccessControlComponent } from './components/user-access-control/user-access-control.component';
import { DisallowShowDirective } from './directives/disallow-show.directive';
import { ModalComponent } from './components/modal/modal.component';
import { SelectModule } from '../select/select.module';
import { MaskedInputModule } from '../masked-input/masked-input.module';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        DpDatePickerModule,
        SelectModule,
        MaskedInputModule
    ],
    declarations: [
        FileUploaderComponent,
        ConfirmClickDirective,
        FileUploaderProgressDirective,
        FileUploaderResultDirective,
        CountryListDirective,
        StateListDirective,
        FormModeToTextPipe,
        UserRoleToTextPipe,
        TrackClickDirective,
        AsyncTaskErrorMessageComponent,
        UserRolesToTextPipe,
        AllowDisableDirective,
        TopBarAndHeaderComponent,
        FooterComponent,
        CurrentUserDirective,
        LogoutClickDirective,
        CustomScrollbarDirective,
        SpinnerComponent,
        AllowShowDirective,
        ColgroupForWidthsComponent,
        PropertyLogoComponent,
        VenueLogoComponent,
        UserStatusToTextPipe,
        AddHttpToUrlPipe,
        AddClassAfterTimeDirective,
        Select2Directive,
        MultiSelectComponent,
        DynamicDropdownComponent,
        DynamicAutocompleteComponent,
        CalendarComponent,
        VendorCreationButtonComponent,
        NumberInputDirective,
        InventoryCategoryDropdownComponent,
        InventorySubCategoryDropdownComponent,
        InventoryStorageLocationDropdownComponent,
        TinymceComponent,
        AccountDropdownComponent,
        EventDropdownComponent,
        AccountCreationButtonComponent,
        EventCreationButtonComponent,
        TimePickerComponent,
        VendorDropdownComponent,
        VendorContactDropdownComponent,
        InventoryDropdownComponent,
        SystemDropdownComponent,
        InventorySubCategoryItemDropdownComponent,
        FunctionSpaceDropdownComponent,
        MonthDropdownComponent,
        YearDropdownComponent,
        VendorContactCreateButtonComponent,
        DisableControlDirective,
        EquipmentDropdownComponent,
        InventorySubCategoryItemsDropdownComponent,
        KeysPipe,
        ValueChangesDirective,
        DateTimePickerComponent,
        PurchaseOrderDropdownComponent,
        CostCategoryDropdownComponent,
        VenueContactDropdownComponent,
        KeepHtmlPipe,
        UserAccessControlComponent,
        DisallowShowDirective,
        ModalComponent
    ],
    exports: [
        FileUploaderComponent,
        ConfirmClickDirective,
        FileUploaderProgressDirective,
        FileUploaderResultDirective,
        CountryListDirective,
        StateListDirective,
        FormModeToTextPipe,
        UserRoleToTextPipe,
        TrackClickDirective,
        AsyncTaskErrorMessageComponent,
        UserRolesToTextPipe,
        AllowDisableDirective,
        TopBarAndHeaderComponent,
        FooterComponent,
        CurrentUserDirective,
        LogoutClickDirective,
        CustomScrollbarDirective,
        SpinnerComponent,
        AllowShowDirective,
        DisallowShowDirective,
        ColgroupForWidthsComponent,
        PropertyLogoComponent,
        VenueLogoComponent,
        UserStatusToTextPipe,
        AddHttpToUrlPipe,
        AddClassAfterTimeDirective,
        Select2Directive,
        MultiSelectComponent,
        DynamicDropdownComponent,
        DynamicAutocompleteComponent,
        CalendarComponent,
        VendorCreationButtonComponent,
        NumberInputDirective,
        InventoryCategoryDropdownComponent,
        InventorySubCategoryDropdownComponent,
        InventoryStorageLocationDropdownComponent,
        TinymceComponent,
        AccountDropdownComponent,
        EventDropdownComponent,
        AccountCreationButtonComponent,
        EventCreationButtonComponent,
        TimePickerComponent,
        VendorDropdownComponent,
        VendorContactDropdownComponent,
        InventoryDropdownComponent,
        SystemDropdownComponent,
        InventorySubCategoryItemDropdownComponent,
        FunctionSpaceDropdownComponent,
        MonthDropdownComponent,
        YearDropdownComponent,
        DisableControlDirective,
        VendorContactCreateButtonComponent,
        InventorySubCategoryItemsDropdownComponent,
        KeysPipe,
        ValueChangesDirective,
        DateTimePickerComponent,
        EquipmentDropdownComponent,
        PurchaseOrderDropdownComponent,
        CostCategoryDropdownComponent,
        VenueContactDropdownComponent,
        KeepHtmlPipe,
        UserAccessControlComponent,
        ModalComponent
    ]
})
export class SharedModule {
}
