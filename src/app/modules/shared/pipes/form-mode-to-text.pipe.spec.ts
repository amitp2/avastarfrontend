import { FormModeToTextPipe } from './form-mode-to-text.pipe';
import { FormMode } from '../../../enums/form-mode.enum';

describe('FormModeToTextPipe', () => {

    it('should translate form mode Add to "Add"', () => {
        const pipe = new FormModeToTextPipe();
        expect(pipe.transform(FormMode.Add)).toEqual('Add');
    });

    it('should translate form mode Edit to "Update"', () => {
        const pipe = new FormModeToTextPipe();
        expect(pipe.transform(FormMode.Edit)).toEqual('Update');
    });

});
