import { Pipe, PipeTransform } from '@angular/core';
import { UserRole } from '../../../enums/user-role.enum';
import { UtilsService } from '../../../services/utils.service';

@Pipe({
    name: 'userRoleToText'
})
export class UserRoleToTextPipe implements PipeTransform {

    constructor(private utilsService: UtilsService) {

    }

    transform(value: UserRole, args?: any): any {
        return this.utilsService.userRoleToDisplayString(value);
    }

}
