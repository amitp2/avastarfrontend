import { Pipe, PipeTransform } from '@angular/core';
import { UserStatus } from '../../../enums/user-status.enum';
import { UtilsService } from '../../../services/utils.service';

@Pipe({
    name: 'userStatusToText'
})
export class UserStatusToTextPipe implements PipeTransform {

    constructor(private utilsService: UtilsService) {

    }

    transform(value: UserStatus, args?: any): any {
        return this.utilsService.userStatusToString(value);
    }

}
