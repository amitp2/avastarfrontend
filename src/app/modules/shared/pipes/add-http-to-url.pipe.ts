import { Pipe, PipeTransform } from '@angular/core';
import { UtilsService } from '../../../services/utils.service';

@Pipe({
    name: 'addHttpToUrl'
})
export class AddHttpToUrlPipe implements PipeTransform {

    constructor(private utilsService: UtilsService) {

    }

    transform(value: any, args?: any): any {
        if (!value) {
            return null;
        }
        return this.utilsService.addHttpToUrl(value);
    }

}
