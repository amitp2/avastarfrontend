import { Pipe, PipeTransform } from '@angular/core';
import { UserRole } from '../../../enums/user-role.enum';
import { UtilsService } from '../../../services/utils.service';

@Pipe({
    name: 'userRolesToText'
})
export class UserRolesToTextPipe implements PipeTransform {

    constructor(private utilsService: UtilsService) {

    }

    transform(value: UserRole[], args?: any): any {
        if (!value) {
            return '';
        }
        return value.map((role: UserRole) => this.utilsService.userRoleToDisplayString(role)).join(', ');
    }

}
