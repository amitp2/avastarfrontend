import { Pipe, PipeTransform } from '@angular/core';
import { FormMode } from '../../../enums/form-mode.enum';

@Pipe({
    name: 'formModeToText'
})
export class FormModeToTextPipe implements PipeTransform {

    transform(value: FormMode, args?: any): any {
        switch (value) {
            case FormMode.Add:
                return 'Add';
            case FormMode.Edit:
                return 'Update';
        }
        return '';
    }

}
