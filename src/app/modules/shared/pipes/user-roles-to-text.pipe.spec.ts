import { UserRolesToTextPipe } from './user-roles-to-text.pipe';
import { UserRole } from '../../../enums/user-role.enum';
import { UtilsService } from '../../../services/utils.service';

describe('UserRolesToTextPipe', () => {

    it('it should transform the array of roles into proper string', () => {
        const pipe = new UserRolesToTextPipe(new UtilsService());
        expect(pipe.transform([UserRole.ADMIN, UserRole.SUPER_ADMIN])).toEqual('Admin, Super Admin');
    });

});
