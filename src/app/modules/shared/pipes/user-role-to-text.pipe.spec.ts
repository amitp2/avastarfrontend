import { UserRoleToTextPipe } from './user-role-to-text.pipe';
import { UserRole } from '../../../enums/user-role.enum';
import { UtilsService } from '../../../services/utils.service';

describe('UserRoleToTextPipe', () => {

    it('should translate user role to proper text', () => {
        const pipe = new UserRoleToTextPipe(new UtilsService());

        expect(pipe.transform(UserRole.ADMIN)).toEqual('Admin');
        expect(pipe.transform(UserRole.SUPER_ADMIN)).toEqual('Super Admin');
    });

});
