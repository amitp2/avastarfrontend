import { Component, Input, OnInit } from '@angular/core';
import { ProductModel } from '../../../../models/product-model';
import { ProductResourceService } from '../../../../services/resources/product-resource.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { UserModel } from '../../../../models/user-model';
import {
    SubscriberProductUsageDecreased,
    SubscriberProductUsageIncreased
} from '../../../../store/subscriber-product-usage/subscriber-product-usage.actions';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { UserProductModel } from '../../../../models/user-product-model';
import { AppError } from '../../../../enums/app-error.enum';
import { UserProductResourceService } from '../../../../services/resources/user-product-resource.service';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';
import { Observable } from 'rxjs/Observable';
import { subscriberHasAvailableSlotsForProduct } from '../../../../store/selectors';
import { ProductType } from '../../../../enums/product-type.enum';

@Component({
    selector: 'app-user-access-control',
    templateUrl: './user-access-control.component.html',
    styleUrls: ['./user-access-control.component.styl']
})
export class UserAccessControlComponent implements OnInit {

    @Input()
    subscriberId: any;

    @Input()
    user: UserModel;

    products: ProductModel[];

    isAvailable: {
        [key: string]: Observable<boolean>
    };

    constructor(private productResource: ProductResourceService,
                private userProductResource: UserProductResourceService,
                private store: Store<AppState>) {
        this.isAvailable = {};
    }

    ngOnInit() {
        this.fetchAllProducts();
        this.isAvailable[ProductType.Aavastar] = this.store.select(subscriberHasAvailableSlotsForProduct(this.subscriberId, ProductType.Aavastar));
        this.isAvailable[ProductType.Syzygy] = this.store.select(subscriberHasAvailableSlotsForProduct(this.subscriberId, ProductType.Syzygy));

        // this.isAvailable[ProductType.Aavastar].subscribe((val) => {
        //     console.log('avastar', val);
        // });
        //
        // this.isAvailable[ProductType.Syzygy].subscribe((val) => {
        //     console.log('syzygy', val);
        // });
    }

    async fetchAllProducts() {
        const products = await this.productResource.findAllBySubscriberId(this.subscriberId);
        this.products = products;
    }

    @AsyncMethod({
        taskName: 'user_access_for_product',
        success: AppSuccess.ACCESS_UPDATED,
        error: AppError.ACCESS_UPDATED
    })
    changeAccess(event: any, product: ProductModel) {
        const user = this.user;
        const type = product.product;
        const checked = event.target.checked;

        if (checked) {
            const userProduct = new UserProductModel();
            userProduct.product = type;
            return this.userProductResource.addForUser(user.id, userProduct)
                .then((added: UserProductModel) => {
                    user.addProduct(added);
                    this.store.dispatch(new SubscriberProductUsageIncreased({
                        subscriberId: +user.subscriberId,
                        product: product.product
                    }));
                });
        } else {
            return this.userProductResource.delete(user.productId(product.product))
                .then(() => {
                    user.removeProduct(product.product);
                    this.store.dispatch(new SubscriberProductUsageDecreased({
                        subscriberId: +user.subscriberId,
                        product: product.product
                    }));
                });
        }
    }

}
