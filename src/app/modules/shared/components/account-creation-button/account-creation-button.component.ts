import { Component, OnDestroy, OnInit } from '@angular/core';
import { PromisedStoreService } from '../../../../services/promised-store.service';
import { SubscriberLogoUploaderService } from '../../../route-subscribers/services/subscriber-logo-uploader.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormMode } from '../../../../enums/form-mode.enum';
import { VenueClientType } from '../../../../enums/venue-client-type.enum';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { UploadType } from '../../../../enums/upload-type.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { VenueClientStatus } from '../../../../enums/venue-client-status.enum';
import { CountryModel } from '../../../../models/country-model';
import { Subscription } from 'rxjs/Subscription';
import { SubscriberStatus } from '../../../../enums/subscriber-status.enum';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { VenueClientModel } from '../../../../models/venue-client-model';
import { VenueClientResourceService } from '../../../../services/resources/venue-client-resource.service';
import { StateModel } from '../../../../models/state-model';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';
import { AccountAdded } from '../../../../store/all-accounts/all-accounts.actions';

@Component({
    selector: 'app-account-creation-button',
    templateUrl: './account-creation-button.component.html',
    styleUrls: ['./account-creation-button.component.styl'],
    providers: [SubscriberLogoUploaderService]
})
export class AccountCreationButtonComponent implements OnDestroy {

    id: any;

    form: FormGroup;
    accountType = VenueClientType;
    status = VenueClientStatus;

    uploader: any;
    mode: FormMode;

    modeObs: BehaviorSubject<FormMode>;
    modeObsSubscription: Subscription;
    initialisedObs: BehaviorSubject<boolean>;
    initialisedItems: number;
    countries: CountryModel[];
    states: StateModel[];

    submitting = false;
    popupVisible = false;

    constructor(private venueClientResource: VenueClientResourceService,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private logoUploader: SubscriberLogoUploaderService,
                private promisedStore: PromisedStoreService,
                private store: Store<AppState>) {

        // this.uploader = logoUploader;
        this.uploader = logoUploader;
        this.initialisedItems = 0;
        this.modeObs = new BehaviorSubject<FormMode>(FormMode.Add);
        this.initialisedObs = new BehaviorSubject<boolean>(false);

        this.form = new FormGroup({
            name: new FormControl(null, [Validators.required]),
            accountType: new FormControl(VenueClientType.Client),
            status: new FormControl(false),
            address: new FormControl(null, [Validators.required]),
            address2: new FormControl(null),
            city: new FormControl(null, [Validators.required]),
            stateId: new FormControl(),
            postCode: new FormControl(null, [Validators.required, Validators.maxLength(10)]),
            countryId: new FormControl(null, [Validators.required]),
            branch: new FormControl(),
            phone: new FormControl(null, [Validators.required]),
            fax: new FormControl(null),
            website: new FormControl(),
            agreement: new FormControl(true),
            logoUrl: new FormControl()
        });

        this.mode = FormMode.Add;
        this.uploader.uploadType = UploadType.Add;
        this.modeObs.next(this.mode);

        this.modeObsSubscription = this.modeObs
            .combineLatest(this.initialisedObs.filter(inited => inited))
            .map(combined => combined[0])
            .subscribe((mode: FormMode) => {
                if (mode === FormMode.Edit) {
                    this.load();
                    return;
                }
                this.form.get('stateId').setValue(this.states[0].id);
                this.form.get('countryId').setValue(233);
            });

        this.form.get('stateId').disable();

        this.form.get('countryId').valueChanges.subscribe((val: number) => {
            if (+val === 233) {
                this.form.get('stateId').enable();
            } else {
                this.form.get('stateId').disable();
            }
        });

    }

    countriesAreLoaded(countries: CountryModel[]) {
        this.initialisedItems++;
        this.countries = countries;
        if (this.initialisedItems === 2) {
            this.initialisedObs.next(true);
        }
    }

    statesAreLoaded(states: StateModel[]) {
        this.initialisedItems++;
        this.states = states;
        if (this.initialisedItems === 2) {
            this.initialisedObs.next(true);
        }
    }

    submit() {
        if (this.submitting) {
            return;
        }

        this.submitting = true;

        const newModel = this.deserialize(this.form.value);
        if (this.mode === FormMode.Add) {
            this.add(newModel)
                .then(() => this.submitting = false);
            return;
        }
        this.update(+this.id, newModel)
            .then(() => this.submitting = false)
            .then(() => this.router.navigateByUrl('/u/accounts?fresh=' + Date.now()));
    }

    deserialize(formValue: any): VenueClientModel {
        const modelVal: any = {
            ...formValue
        };
        modelVal.status = formValue.status ? SubscriberStatus.Active : SubscriberStatus.Locked;
        modelVal.masterAgreementAgreed = formValue.agreement;
        return new VenueClientModel(modelVal);
    }

    serialize(model: VenueClientModel): any {
        return {
            name: model.name,
            accountType: model.accountType,
            status: model.status === VenueClientStatus.Active ? true : false,
            address: model.address,
            address2: model.address2,
            city: model.city,
            stateId: model.stateId,
            postCode: model.postCode,
            countryId: model.countryId,
            branch: model.branch,
            phone: model.phone,
            fax: model.fax,
            website: model.website,
            agreement: model.masterAgreementAgreed,
            logoUrl: model.logoUrl
        };
    }

    @AsyncMethod({
        taskName: 'account_loaded'
    })
    load() {
        return this.venueClientResource
            .get(+this.id)
            .then((subscriber: VenueClientModel) => {
                this.uploader.uploadType = UploadType.Update;
                this.uploader.oldUrl = subscriber.logoUrl;
                this.form.setValue(this.serialize(subscriber));
            });
    }

    @AsyncMethod({
        taskName: 'account_add',
        success: AppSuccess.ACCOUNT_ADD,
        error: AppError.ACCOUNT_ADD
    })
    add(model: VenueClientModel) {
        return this.promisedStore.currentVenueId()
            .then((venueId: number) => {
                model.venueId = venueId;
                return this.venueClientResource.add(model);
            })
            .then((added: VenueClientModel) => this.store.dispatch(new AccountAdded(added)))
            .then(() => this.popupVisible = false);
    }

    @AsyncMethod({
        taskName: 'account_update',
        success: AppSuccess.ACCOUNT_UPDATE,
        error: AppError.ACCOUNT_UPDATE
    })
    update(id: number, model: VenueClientModel) {
        return this.promisedStore.currentVenueId()
            .then((venueId: number) => {
                model.venueId = venueId;
                return this.venueClientResource.edit(id, model);
            });
    }

    ngOnDestroy() {
        this.modeObsSubscription.unsubscribe();
    }

}
