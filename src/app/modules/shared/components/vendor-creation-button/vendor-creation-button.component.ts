import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { UserRole } from '../../../../enums/user-role.enum';
import { FormMode } from '../../../../enums/form-mode.enum';
import { FormHandler } from '../../../../classes/form-handler';
import { VendorModel } from '../../../../models/vendor-model';
import { VendorResourceService } from '../../../../services/resources/vendor-resource.service';
import { UtilsService } from '../../../../services/utils.service';
import { AuthService } from '../../../../services/auth.service';
import { Observable } from 'rxjs/Observable';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { VendorStatus } from '../../../../enums/vendor-status.enum';
import { ObjectTransformerType } from '../../../../enums/object-transformer-type.enum';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';
import { VendorAdded } from '../../../../store/vendors/vendors.actions';
import { SelectFetchType } from '../../../select/types/select-fetch-type';

const transformer = {
    address: {
        type: ObjectTransformerType.Default,
        to: 'address'
    },
    name: {
        type: ObjectTransformerType.Default,
        to: 'name'
    },
    phone: {
        type: ObjectTransformerType.Default,
        to: 'phone'
    },
    fax: {
        type: ObjectTransformerType.Default,
        to: 'fax'
    },
    website: {
        type: ObjectTransformerType.Default,
        to: 'website'
    }
};

@Component({
    selector: 'app-vendor-creation-button',
    templateUrl: './vendor-creation-button.component.html',
    styleUrls: ['./vendor-creation-button.component.styl']
})
export class VendorCreationButtonComponent extends FormHandler<VendorModel> implements OnInit {

    @Output() vendorCreated: EventEmitter<any>;

    popupVisible: boolean;
    userRole = UserRole;
    mode = FormMode;

    SelectFetchType = SelectFetchType;

    constructor(
        private vendors: VendorResourceService,
        private utilsService: UtilsService,
        private authService: AuthService,
        private store: Store<AppState>
    ) {
        super();
        this.popupVisible = false;
        this.vendorCreated = new EventEmitter<any>();
    }

    closePopup() {
        this.popupVisible = false;
        this.form.reset();
    }

    ngOnInit() {
        super.ngOnInit();
        this.form.get('address.countryId')
            .valueChanges
            .subscribe((id: number) => +id !== 233 ? this.form.get('address.stateId').disable() : this.form.get('address.stateId').enable());
    }

    entityId(): Observable<number> {
        return new BehaviorSubject<number>(null);
    }

    formMode(): Observable<FormMode> {
        return new BehaviorSubject<FormMode>(FormMode.Add);
    }

    initializeForm(): FormGroup {
        return new FormGroup({
            name: new FormControl(null, [Validators.required]),
            address: new FormGroup(({
                countryId: new FormControl(null, [Validators.required]),
                stateId: new FormControl(),
                city: new FormControl(null, [Validators.required]),
                address: new FormControl(null, [Validators.required]),
                address2: new FormControl(),
                postCode: new FormControl(null, [Validators.required, Validators.maxLength(10)])
            })),
            phone: new FormControl(),
            fax: new FormControl(),
            website: new FormControl()
        });
    }

    @AsyncMethod({
        taskName: 'get_vendor'
    })
    get(id: number): Promise<VendorModel> {
        return this.vendors.get(id);
    }

    @AsyncMethod({
        taskName: 'vendor_add',
        success: AppSuccess.VENDOR_ADD,
        error: AppError.VENDOR_ADD
    })
    add(model: VendorModel): Promise<any> {
        model.status = VendorStatus.Active;
        return this.vendors
            .addForSubscriber(model, this.authService.currentUserSnapshot.subscriberId)
            .then((added: VendorModel) => {
                this.vendorCreated.emit();
                this.store.dispatch(new VendorAdded(added));
                this.closePopup();
            });
    }

    @AsyncMethod({
        taskName: 'vendor_add',
        success: AppSuccess.VENDOR_EDIT,
        error: AppError.VENDOR_EDIT
    })
    edit(id: number, model: VendorModel): Promise<any> {
        model.status = VendorStatus.Active;
        return this.vendors
            .edit(id, model)
            .then(() => {
                this.closePopup();
            });
    }

    serialize(model: VendorModel): any {
        return this.utilsService.transformInto(model, transformer);
    }

    deserialize(formValue: any): VendorModel {
        return new VendorModel(this.utilsService.transformInto(formValue, {
            ...transformer
        }));
    }

}
