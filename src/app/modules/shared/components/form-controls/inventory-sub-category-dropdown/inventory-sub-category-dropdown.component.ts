import { Component, forwardRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { InventoryCategoryResourceService } from '../../../../../services/resources/inventory-category-resource.service';
import { InventoryCategoryModel } from '../../../../../models/inventory-category-model';
import { DropdownItem } from '../../../../../interfaces/dropdown-item';
import { DynamicDropdownComponent } from '../dynamic-dropdown/dynamic-dropdown.component';

@Component({
    selector: 'app-inventory-sub-category-dropdown',
    templateUrl: './inventory-sub-category-dropdown.component.html',
    styleUrls: ['./inventory-sub-category-dropdown.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => InventorySubCategoryDropdownComponent),
            multi: true
        }
    ]
})
export class InventorySubCategoryDropdownComponent implements ControlValueAccessor, OnInit, OnDestroy {

    currentCategoryId: number;

    @Input() set categoryId(val: any) {
        this.currentCategoryId = val;
        this.refresh();
    }

    @Input() addAllOption: boolean;
    @Input() pleaseSelect = false;

    @ViewChild('subCategory') subCategoryDropdown: DynamicDropdownComponent;

    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;
    form: FormGroup;
    subs: Subscription;

    constructor(private inventoryCategory: InventoryCategoryResourceService) {
        this.form = new FormGroup({
            categoryId: new FormControl()
        });
    }

    private mapCategoriesToDropdown(resp: InventoryCategoryModel[]): DropdownItem[] {
        return resp.map((item) => ({
            value: item.id, display: item.name
        }));
    }

    loadCategories = (): Promise<DropdownItem[]> => {
        if (!this.currentCategoryId) {
            return Promise.resolve([]);
        }
        return this.inventoryCategory
            .getAllSubcategories(this.currentCategoryId)
            .then((resp: InventoryCategoryModel[]) => this.mapCategoriesToDropdown(resp));
    }

    refresh() {
        if (this.subCategoryDropdown) {
            this.subCategoryDropdown.refresh();
        }
    }

    ngOnInit() {
        this.subs = this.form.get('categoryId').valueChanges.subscribe(val => this.onChange && this.onChange(val));
    }

    ngOnDestroy() {
        this.subs.unsubscribe();
    }

    writeValue(obj: any): void {
        if (!obj) {
            return;
        }
        this.currentValue = obj;
        this.form.setValue({categoryId: obj});
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

}
