import { Component, ElementRef, forwardRef, Input, NgZone, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { GlobalsService } from '../../../../../services/globals.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Component({
    selector: 'app-calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => CalendarComponent),
            multi: true
        }
    ]
})
export class CalendarComponent implements ControlValueAccessor, OnInit, OnDestroy {

    @Input()
    format: string;

    @Input()
    setRaw = false;

    @ViewChild('calendar')
    calendar: ElementRef;

    @Input()
    set minDate(val: string) {
        this.minDateInner = val;
        this.setMinDate();
    }

    @Input()
    set maxDate(val: string) {
        this.maxDateInner = val;
        this.setMaxDate();
    }

    minDateInner: string;
    maxDateInner: string;

    get $() {
        return this.globals.globals().$;
    }


    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;

    constructor(private globals: GlobalsService,
                private zone: NgZone) {
    }

    ngOnInit() {
        const $ = this.globals.globals().$;
        const format = this.format ? this.format : 'yy-mm-dd';
        const el = this.calendar.nativeElement;

        $(el).datepicker({
            onSelect: (dateText) => {
                this.zone.run(() => this.issueChange(dateText));
            }
        });
        $(el).datepicker('option', 'dateFormat', format);
        // $(el).on('change', () => {
        //     console.log(el.value);
        //     this.zone.run(() => this.issueChange(el.value));
        // });

        this.setMinDate();
    }

    private setMinDate() {
        if (!this.minDateInner) {
            return;
        }
        const el = this.calendar.nativeElement;
        this.$(el).datepicker('option', 'minDate', this.minDateInner);
        this.issueChange(el.value);
    }

    private setMaxDate() {
        if (!this.maxDateInner) {
            return;
        }
        const el = this.calendar.nativeElement;
        this.$(el).datepicker('option', 'maxDate', this.maxDateInner);
        this.issueChange(el.value);
    }

    ngOnDestroy() {
        const $ = this.globals.globals().$;
        const el = this.calendar.nativeElement;
        $(el).off('change');
        $(el).datepicker('destroy');
    }

    private issueChange(val: any) {
        try {
            this.onChange(val);
        } catch (exc) {
            console.log('change function for calendar hasnt registered yet');
        }
    }

    writeValue(obj: any): void {
        const $ = this.globals.globals().$;
        this.currentValue = obj;

        if (obj) {
            $(this.calendar.nativeElement).datepicker('setDate', this.setRaw ? obj : new Date(obj));
        } else {
            $(this.calendar.nativeElement).datepicker('setDate', null);
        }
        // this.renderer.setValue(this.calendar.nativeElement, obj);
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }
}
