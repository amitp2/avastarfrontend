import { Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { InventoryCategoryModel } from '../../../../../models/inventory-category-model';
import { DropdownItem } from '../../../../../interfaces/dropdown-item';
import {
    InventorySpace, InventorySpaceInfo,
    InventorySpacesService
} from '../../../../route-inventory/services/inventory-spaces.service';


@Component({
    selector: 'app-inventory-storage-location-dropdown',
    templateUrl: './inventory-storage-location-dropdown.component.html',
    styleUrls: ['./inventory-storage-location-dropdown.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => InventoryStorageLocationDropdownComponent),
            multi: true
        }
    ]
})
export class InventoryStorageLocationDropdownComponent implements ControlValueAccessor, OnInit, OnDestroy {

    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;
    form: FormGroup;
    subs: Subscription;
    @Input() addAllOption: boolean;

    constructor(private inventorySpaces: InventorySpacesService) {
        this.form = new FormGroup({
            locationId: new FormControl()
        });
    }

    private mapCategoriesToDropdown(resp: InventoryCategoryModel[]): DropdownItem[] {
        return resp.map((item) => ({
            value: item.id, display: item.name
        }));
    }

    loadLocations = (): Promise<DropdownItem[]> => {
        return this.inventorySpaces
            .getSpaces()
            .then((res) => res.map((space: InventorySpace) => ({
                value: space.id,
                display: space.name,
                disabled: +space.id === -1
            })))
            .then((resp: any) => {

                if (this.addAllOption) {
                    return [{value: '', display: 'All'}].concat(resp);
                }

                return resp;
            });
    }

    ngOnInit() {
        this.subs = this.form.get('locationId').valueChanges.subscribe(val => this.onChange && this.onChange({
            id: this.inventorySpaces.getRealId(val),
            type: this.inventorySpaces.getSpaceType(val)
        }));
    }

    ngOnDestroy() {
        this.subs.unsubscribe();
    }

    writeValue(obj: InventorySpaceInfo): void {
        this.currentValue = obj;
        if (obj) {
            this.form.setValue({
                locationId: this.inventorySpaces.getId(obj.id, obj.type)
            });
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

}
