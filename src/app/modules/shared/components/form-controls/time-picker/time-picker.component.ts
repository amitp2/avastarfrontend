import { Component, forwardRef, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'app-time-picker',
    templateUrl: './time-picker.component.html',
    styleUrls: ['./time-picker.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => TimePickerComponent),
            multi: true
        }
    ]
})
export class TimePickerComponent implements ControlValueAccessor, OnInit, OnDestroy {


    change: any;
    currentValue: any;

    get config(): any {
        return {
            format: 'HH:mm'
        };
    }

    constructor() {
    }

    ngOnInit() {
    }

    writeValue(obj: any): void {
        if (obj) {
            console.log(obj);
            this.currentValue = obj;
        }
    }

    registerOnChange(fn: any): void {
        this.change = fn;
    }

    registerOnTouched(fn: any): void {
    }

    ngOnDestroy(): void {
    }

    changed(event: any) {
        if (this.change) {
            this.change(event.format('HH:mm'));
        }
    }

}
