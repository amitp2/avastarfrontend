import { Component, forwardRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { DropdownItem } from '../../../../../interfaces/dropdown-item';
import { InventoryCategoryResourceService } from '../../../../../services/resources/inventory-category-resource.service';
import { EquipmentSystemModel } from '../../../../../models/equipment-system-model';
import { EquipmentSystemResourceService } from '../../../../../services/resources/equipment-system-resource.service';
import { PromisedStoreService } from '../../../../../services/promised-store.service';
import { DynamicDropdownComponent } from '../dynamic-dropdown/dynamic-dropdown.component';

@Component({
    selector: 'app-system-dropdown',
    templateUrl: './system-dropdown.component.html',
    styleUrls: ['./system-dropdown.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => SystemDropdownComponent),
            multi: true
        }
    ]
})
export class SystemDropdownComponent implements ControlValueAccessor, OnInit, OnDestroy {

    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;
    form: FormGroup;
    subs: Subscription;
    currentSearchTerm: string;

    @Input()
    addAllOption: boolean;

    @ViewChild('dropdown')
    dropdown: DynamicDropdownComponent;

    @Input()
    pleaseSelect = true;

    @Input()
    set searchTerm(val: any) {
        this.currentSearchTerm = val;
        if (this.dropdown) {
            this.dropdown.refresh();
        }
    }


    constructor(private inventoryCategory: InventoryCategoryResourceService,
                private system: EquipmentSystemResourceService,
                private promisedStore: PromisedStoreService) {
        this.form = new FormGroup({
            id: new FormControl()
        });
    }

    private map(resp: EquipmentSystemModel[]): DropdownItem[] {
        return resp.map((item) => ({
            value: item.id, display: item.name
        }));
    }

    load = (): Promise<DropdownItem[]> => {
        return this.promisedStore.currentVenueId()
            .then((venueId: number) => this.system.getAllByVenueId(venueId, {search: this.currentSearchTerm}))
            .then((resp: EquipmentSystemModel[]) => this.map(resp))
            .then((resp) => {

                if (this.pleaseSelect) {
                    return [{value: null, display: 'Please select...'}].concat(resp);
                }

                if (this.addAllOption) {
                    return [{value: '', display: 'All'}].concat(resp);
                }
                return resp;
            });
    };

    ngOnInit() {
        this.subs = this.form.get('id').valueChanges.subscribe(val => this.onChange && this.onChange(val));
    }

    ngOnDestroy() {
        this.subs.unsubscribe();
    }

    writeValue(obj: any): void {
        if (!obj) {
            return;
        }
        this.currentValue = obj;
        this.form.setValue({id: obj});
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

}
