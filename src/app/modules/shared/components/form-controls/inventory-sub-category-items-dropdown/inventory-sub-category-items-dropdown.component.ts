import { Component, OnInit, Input, ViewChild, OnDestroy, forwardRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../../../../../services/config.service';
import { Subscription } from 'rxjs/Subscription';
import { FormGroup, FormBuilder, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DynamicDropdownComponent } from '../dynamic-dropdown/dynamic-dropdown.component';

@Component({
    selector: 'app-inventory-sub-category-items-dropdown',
    templateUrl: './inventory-sub-category-items-dropdown.component.html',
    styleUrls: ['./inventory-sub-category-items-dropdown.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => InventorySubCategoryItemsDropdownComponent),
            multi: true
        }
    ]
})
export class InventorySubCategoryItemsDropdownComponent implements OnInit, OnDestroy {
    _subCategoryId: number;
    apiUrl: string;
    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;
    form: FormGroup;
    subs: Subscription;

    @Input() addAllOption: boolean;
    @ViewChild('dropdown') dropdown: DynamicDropdownComponent;

    get subCategoryId() {
        return this._subCategoryId;
    }

    @Input()
    set subCategoryId(value: number) {
        this._subCategoryId = value;
        if (this.dropdown) {
            this.dropdown.refresh();
        }
    }

    constructor(private http: HttpClient, config: ConfigService, fb: FormBuilder) {
        this.apiUrl = config.config().API_URL;
        this.form = fb.group({
            subCategoryItemId: ''
        });
    }

    load = () => {
        if (!this.subCategoryId) {
            return Promise.resolve([]);
        }
        return new Promise((resolve, reject) => {
            this.http.get<any[]>(`${this.apiUrl}venues/inventories/categories/subcategories/${this.subCategoryId}/subcategoryItems`, {
                params: {
                    page: '1',
                    size: '99999'
                }
            }).subscribe(response => {
                resolve(this.mapToOptions(response));
            }, error => {
                reject(error);
            });
        });
    }

    getName() {
        if (!this.dropdown) {
            return null;
        }
        const item = this.dropdown.getSelected();
        return item ? item.display : null;
    }

    private mapToOptions(records: any[]) {
        return records.map(x => ({
            value: x.id,
            display: x.name
        }));
    }

    ngOnInit() {
        this.subs = this.form.get('subCategoryItemId')
            .valueChanges
            .subscribe(val => this.onChange && this.onChange(val));
    }

    ngOnDestroy() {
        this.subs.unsubscribe();
    }

    writeValue(obj: number): void {
        this.currentValue = obj;
        if (obj) {
            this.form.setValue({
                subCategoryItemId: obj
            });
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }
}
