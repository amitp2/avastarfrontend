import { Component, OnInit, ViewChild, Input, forwardRef } from '@angular/core';
import { FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { DynamicDropdownComponent } from '../dynamic-dropdown/dynamic-dropdown.component';
import { CostCategoryResourceService } from '../../../../../services/resources/cost-category-resource.service';
import { AuthService } from '../../../../../services/auth.service';
import { CostCategoryModel } from '../../../../../models/cost-category-model';
import { DropdownItem } from '../../../../../interfaces/dropdown-item';

@Component({
    selector: 'app-cost-category-dropdown',
    templateUrl: './cost-category-dropdown.component.html',
    styleUrls: ['./cost-category-dropdown.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => CostCategoryDropdownComponent),
            multi: true
        }
    ]
})
export class CostCategoryDropdownComponent implements OnInit {
    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;
    formControl: FormControl = new FormControl();
    subs: Subscription;

    @ViewChild('dropdown') dropdown: DynamicDropdownComponent;
    @Input() addAllOption: boolean;
    @Input() pleaseSelect: boolean;

    constructor(
        private costCategoryApi: CostCategoryResourceService,
        private authApi: AuthService
    ) {
    }

    load = (): Promise<DropdownItem[]> => {
        return this.costCategoryApi.queryBySubscriberId(
            this.costCategoryApi.getAllQueryObject(),
            this.authApi.currentUserSnapshot.subscriberId
        )
        .then(response => response.items.filter(item => item.costCode != null))
        .then(items => items.map(item => ({
            value: item.costCategoryId,
            display: `${item.costCategoryName} ${item.costCode}`
        })))
        .then((items: any) => {
            if (this.pleaseSelect) {
                return [{value: null, display: 'Please select...'}].concat(items);
            }
            if (this.addAllOption) {
                return [{value: '', display: 'All'}].concat(items);
            }
            return items;
        });
    };

    ngOnInit() {
        this.subs = this.formControl.valueChanges
            .subscribe(val => this.onChange && this.onChange(val));
    }

    ngOnDestroy() {
        this.subs.unsubscribe();
    }

    writeValue(obj: number): void {
        this.currentValue = obj;
        if (obj) {
            this.formControl.setValue(obj);
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }
}
