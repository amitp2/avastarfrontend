import { Component, OnInit, ViewChild, Input, forwardRef, OnDestroy } from '@angular/core';
import { DynamicDropdownComponent } from '../dynamic-dropdown/dynamic-dropdown.component';
import { FormGroup, FormControl, NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { PurchaseOrderService } from '../../../../../services/resources/purchase-order.service';
import { CurrentPropertyService } from '../../../../../services/current-property.service';
import { DropdownItem } from '../../../../../interfaces/dropdown-item';

@Component({
    selector: 'app-purchase-order-dropdown',
    templateUrl: './purchase-order-dropdown.component.html',
    styleUrls: ['./purchase-order-dropdown.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => PurchaseOrderDropdownComponent),
            multi: true
        }
    ]
})
export class PurchaseOrderDropdownComponent implements OnInit, ControlValueAccessor, OnDestroy {
    _filterParams: any;
    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;
    form: FormGroup;

    subs: Subscription;
    allAccountsSubs: Subscription;

    @ViewChild('dropdown') dropdown: DynamicDropdownComponent;
    @Input() addAllOption: boolean;

    get filterParams() {
        return this._filterParams;
    }

    @Input() set filterParams(value) {
        this._filterParams = value;
        if (this.dropdown) {
            this.dropdown.refresh();
        }
    }

    constructor(private purchaseOrderApi: PurchaseOrderService,
                private currentVenue: CurrentPropertyService) {
        this.form = new FormGroup({
            purchaseOrderId: new FormControl()
        });
    }

    load = (): Promise<DropdownItem[]> =>
        this.currentVenue.currentVenueAsync()
            .then((venue) => (
                this.purchaseOrderApi.queryByVenueId({
                    page: 0,
                    size: 999999,
                    sortFields: [],
                    sortAsc: true,
                    otherParams: this.filterParams
                }, venue.id)
            ))
            .then((response) => response.items.map(x => ({
                display: x.poNumber,
                value: x.id
            })))
            .then((items: any) => (
                this.addAllOption ?
                [{value: '', display: 'All'}].concat(items) :
                items
            ))

    ngOnInit() {
        this.subs = this.form.get('purchaseOrderId')
            .valueChanges
            .subscribe(val => this.onChange && this.onChange(val));
    }

    ngOnDestroy() {
        this.subs.unsubscribe();
    }

    writeValue(obj: number): void {
        this.currentValue = obj;
        if (obj) {
            this.form.setValue({
                purchaseOrderId: obj
            });
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

}
