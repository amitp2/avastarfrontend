import { Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import { DropdownItem } from '../../../../../interfaces/dropdown-item';
import { ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { PromisedStoreService } from '../../../../../services/promised-store.service';
import { EquipmentSystemResourceService } from '../../../../../services/resources/equipment-system-resource.service';
import { InventoryCategoryResourceService } from '../../../../../services/resources/inventory-category-resource.service';
import { FunctionSpaceResourceService } from '../../../../../services/resources/function-space-resource.service';
import { FunctionSpaceModel } from '../../../../../models/function-space-model';

@Component({
  selector: 'app-function-space-dropdown',
  templateUrl: './function-space-dropdown.component.html',
  styleUrls: ['./function-space-dropdown.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => FunctionSpaceDropdownComponent),
            multi: true
        }
    ]
})
export class FunctionSpaceDropdownComponent implements ControlValueAccessor, OnInit, OnDestroy {

    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;
    form: FormGroup;
    subs: Subscription;

    @Input() addAllOption: boolean;

    constructor(private inventoryCategory: InventoryCategoryResourceService,
                private system: EquipmentSystemResourceService,
                private promisedStore: PromisedStoreService,
                private functionSpace: FunctionSpaceResourceService) {
        this.form = new FormGroup({
            id: new FormControl()
        });
    }

    private map(resp: FunctionSpaceModel[]): DropdownItem[] {
        return resp.map((item) => ({
            value: item.id, display: item.name
        }));
    }

    load = (): Promise<DropdownItem[]> => {
        return this.promisedStore.currentVenueId()
            .then((venueId: number) => this.functionSpace.getAllFunctionSpacesByVenueId(venueId))
            .then((resp: FunctionSpaceModel[]) => this.map(resp))
            .then((resp) => {
                if (this.addAllOption) {
                    return [{value: '', display: 'All'}].concat(resp);
                }
                return resp;
            });
    }

    ngOnInit() {
        this.subs = this.form.get('id').valueChanges.subscribe(val => this.onChange && this.onChange(val));
    }

    ngOnDestroy() {
        this.subs.unsubscribe();
    }

    writeValue(obj: any): void {
        if (!obj) {
            return;
        }
        this.currentValue = obj;
        this.form.setValue({id: obj});
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

}
