import { Component, forwardRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DynamicDropdownComponent } from '../dynamic-dropdown/dynamic-dropdown.component';
import { InventoryCategoryResourceService } from '../../../../../services/resources/inventory-category-resource.service';
import { ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { DropdownItem } from '../../../../../interfaces/dropdown-item';
import { InventoryModel } from '../../../../../models/inventory-model';
import { InventoryResourceService } from '../../../../../services/resources/inventory-resource.service';
import { PromisedStoreService } from '../../../../../services/promised-store.service';

@Component({
    selector: 'app-inventory-dropdown',
    templateUrl: './inventory-dropdown.component.html',
    styleUrls: ['./inventory-dropdown.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => InventoryDropdownComponent),
            multi: true
        }
    ]
})
export class InventoryDropdownComponent implements ControlValueAccessor, OnInit, OnDestroy {

    currentCategoryId: number;
    currentMainCategoryId: number;

    @Input() set subCategoryId(val: any) {
        this.currentCategoryId = val;
        this.refresh();
    }

    @Input() set categoryId(val: any) {
        this.currentMainCategoryId = val;
        this.refresh();
    }

    @Input() addAllOption: boolean;
    @Input() pleaseSelect = true;

    @ViewChild('dropdown') dropdown: DynamicDropdownComponent;

    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;
    form: FormGroup;
    subs: Subscription;

    constructor(private inventoryCategory: InventoryCategoryResourceService,
                private inventory: InventoryResourceService,
                private promisedStore: PromisedStoreService) {
        this.form = new FormGroup({
            id: new FormControl()
        });
    }

    private map(resp: InventoryModel[]): DropdownItem[] {
        return resp.map((item) => ({
            // value: item.id, display: item.model + (item.tagNumber ? `${item.model ? ' - ' : ''}${item.tagNumber}` : '')
            value: item.id, display: item.subCategoryItemName + (item.tagNumber ? `${item.subCategoryItemName ? ' - ' : ''}${item.tagNumber}` : '')
        }));
    }

    load = (): Promise<DropdownItem[]> => {
        // if (!this.currentCategoryId && !this.currentMainCategoryId) {
        //     return Promise.resolve([]);
        // }

        return this.promisedStore.currentVenueId()
            .then((venueId: number) => {

                if (!this.currentMainCategoryId) {
                    return this.inventory.getAllByVenueId(venueId);
                }

                if (this.currentCategoryId) {
                    return this.inventory.getAllBySubCategoryId(this.currentCategoryId, venueId);
                }

                return this.inventory.getAllByCategoryId(this.currentMainCategoryId, venueId);
            })
            .then((resp: any) => this.map(resp))
            .then((resp) => {

                if (this.pleaseSelect) {
                    return [{value: null, display: 'Please select...'}].concat(resp);
                }

                if (this.addAllOption) {
                    return [{value: '', display: 'All'}].concat(resp);
                }
                return resp;
            });
    };

    refresh() {
        if (this.dropdown) {
            this.dropdown.refresh();
        }
    }

    ngOnInit() {
        this.subs = this.form.get('id').valueChanges.subscribe(val => this.onChange && this.onChange(val));
    }

    ngOnDestroy() {
        this.subs.unsubscribe();
    }

    writeValue(obj: any): void {
        if (!obj) {
            return;
        }
        this.currentValue = obj;
        this.form.setValue({id: obj});
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

}
