import { Component, forwardRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { DropdownItem } from '../../../../../interfaces/dropdown-item';
import { InventoryCategoryResourceService } from '../../../../../services/resources/inventory-category-resource.service';
import { DynamicDropdownComponent } from '../dynamic-dropdown/dynamic-dropdown.component';
import { VendorContactResourceService } from '../../../../../services/resources/vendor-contact-resource.service';
import { VendorContactModel } from '../../../../../models/vendor-contact-model';

@Component({
    selector: 'app-vendor-contact-dropdown',
    templateUrl: './vendor-contact-dropdown.component.html',
    styleUrls: ['./vendor-contact-dropdown.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => VendorContactDropdownComponent),
            multi: true
        }
    ]
})
export class VendorContactDropdownComponent implements ControlValueAccessor, OnInit, OnDestroy {

    currentVendorId: number;

    @Input() set vendorId(val: any) {
        this.currentVendorId = val;
        this.refresh();
    }

    @Input() addAllOption: boolean;
    @Input() pleaseSelect = true;

    @ViewChild('dropdown') dropdown: DynamicDropdownComponent;

    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;
    form: FormGroup;
    subs: Subscription;

    constructor(private inventoryCategory: InventoryCategoryResourceService,
                private vendorContact: VendorContactResourceService) {
        this.form = new FormGroup({
            id: new FormControl()
        });
    }

    private map(resp: VendorContactModel[]): DropdownItem[] {
        return resp.map((item) => ({
            value: item.id, display: item.firstName
        }));
    }

    load = (): Promise<DropdownItem[]> => {
        let promise;
        if (!this.currentVendorId) {
            promise = Promise.resolve([]);
        } else {
            promise = this.vendorContact
                .getAllByVendorId(this.currentVendorId)
                .then((resp: VendorContactModel[]) => this.map(resp));
        }
        return promise
            .then((resp) => {
                if (this.addAllOption) {
                    return [{value: '', display: 'All'}].concat(resp);
                }
                return resp;
            })
            .then((resp: any) => {
                if (this.pleaseSelect) {
                    resp.unshift({value: '', display: 'Please select...'});
                }
                return resp;
            });
    }

    refresh() {
        if (this.dropdown) {
            this.dropdown.refresh();
        }
    }

    ngOnInit() {
        this.subs = this.form.get('id').valueChanges.subscribe(val => this.onChange && this.onChange(val));
    }

    ngOnDestroy() {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    writeValue(obj: any): void {
        if (!obj) {
            return;
        }
        this.currentValue = obj;
        this.form.setValue({id: obj});
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

}
