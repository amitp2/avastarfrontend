import { Component, forwardRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { PromisedStoreService } from '../../../../../services/promised-store.service';
import { DropdownItem } from '../../../../../interfaces/dropdown-item';
import { Subscription } from 'rxjs/Subscription';
import { AppState } from '../../../../../store/store';
import { Store } from '@ngrx/store';
import { DynamicDropdownComponent } from '../dynamic-dropdown/dynamic-dropdown.component';
import { VenueEventModel } from '../../../../../models/venue-event-model';
import { VenueEventResourceService } from '../../../../../services/resources/venue-event-resource.service';
import { SetAllEvents } from '../../../../../store/all-events/all-events.actions';
import { UnsubscribePoint } from '../../../../../decorators/unsubscribe-point.decorator';
import { ObservableSubscription } from '../../../../../decorators/observable-subscription.decorator';

@Component({
    selector: 'app-event-dropdown',
    templateUrl: './event-dropdown.component.html',
    styleUrls: ['./event-dropdown.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => EventDropdownComponent),
            multi: true
        }
    ]
})
export class EventDropdownComponent implements ControlValueAccessor, OnInit, OnDestroy {

    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;
    form: FormGroup;

    @ObservableSubscription
    subs: Subscription;

    @ObservableSubscription
    allEventsSubs: Subscription;

    @ViewChild('dropdown')
    dropdown: DynamicDropdownComponent;

    @Input()
    addAllOption: boolean;

    @Input()
    optional: boolean;

    accountIdInner: any;

    @Input()
    set accountId(val: number) {
        this.accountIdInner = val;
        this.triggerLoad();
    }

    get accountId(): number {
        return this.accountIdInner;
    }

    constructor(
        private events: VenueEventResourceService,
        private promisedStore: PromisedStoreService,
        private store: Store<AppState>
    ) {
        this.form = new FormGroup({
            id: new FormControl()
        });
    }

    private triggerLoad() {
        if (this.accountId) {
            this.promisedStore
                .currentVenueId()
                .then((venueId: number) => this.events.getAllByAccountId(this.accountId, null))
                .then((resp: any) => this.store.dispatch(new SetAllEvents(resp)));
            return;
        }
        // this.promisedStore
        //     .currentVenueId()
        //     .then((venueId: number) => this.events.getAllByVenueId(venueId))
        //     .then((resp: any) => this.store.dispatch(new SetAllEvents(resp)));
    }

    load = (): Promise<DropdownItem[]> => {
        return this.promisedStore.allEvents()
            .then((events: VenueEventModel[]) => events.map((event: VenueEventModel) => ({
                value: event.id,
                display: event.name
            })))
            .then((resp: any) => {
                if (this.optional) {
                    return [{value: '', display: 'Please select...'}].concat(resp);
                }
                if (this.addAllOption) {
                    return [{value: '', display: 'All'}].concat(resp);
                }
                return resp;
            });
    };

    ngOnInit() {
        this.subs = this.form.get('id')
            .valueChanges
            .subscribe(val => this.onChange && this.onChange(val));
        this.triggerLoad();
        this.allEventsSubs = this.store.select('allEvents').subscribe(() => this.dropdown.refresh());
    }

    @UnsubscribePoint
    ngOnDestroy() {
    }

    writeValue(obj: number): void {
        this.currentValue = obj;
        if (obj) {
            this.form.setValue({
                id: obj
            });
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

}
