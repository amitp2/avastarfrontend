import { Component, forwardRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { DropdownItem } from '../../../../../interfaces/dropdown-item';
import { InventoryCategoryModel } from '../../../../../models/inventory-category-model';
import { InventoryCategoryResourceService } from '../../../../../services/resources/inventory-category-resource.service';
import { VendorResourceService } from '../../../../../services/resources/vendor-resource.service';
import { AuthService } from '../../../../../services/auth.service';
import { CurrentUser } from '../../../../../interfaces/current-user';
import { DynamicDropdownComponent } from '../dynamic-dropdown/dynamic-dropdown.component';
import { VendorModel } from '../../../../../models/vendor-model';

@Component({
    selector: 'app-vendor-dropdown',
    templateUrl: './vendor-dropdown.component.html',
    styleUrls: ['./vendor-dropdown.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => VendorDropdownComponent),
            multi: true
        }
    ]
})
export class VendorDropdownComponent implements ControlValueAccessor, OnInit, OnDestroy {

    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;
    form: FormGroup;
    subs: Subscription;

    @Input() addAllOption: boolean;
    @Input() pleaseSelect = true;

    @ViewChild(DynamicDropdownComponent) dropdown: DynamicDropdownComponent;

    constructor(private inventoryCategory: InventoryCategoryResourceService,
                private vendors: VendorResourceService,
                private auth: AuthService) {
        this.form = new FormGroup({
            id: new FormControl()
        });
    }

    private map(resp: VendorModel[]): DropdownItem[] {
        return resp.map((item) => ({
            value: item.id, display: item.name
        }));
    }

    load = (): Promise<DropdownItem[]> => {
        return this.auth.currentUser()
            .then((c: CurrentUser) => c.subscriberId)
            .then((subscriberId: number) => this.vendors.getAllBySubscriber(subscriberId))
            .then((resp: VendorModel[]) => this.map(resp))
            .then((resp) => {
                if (this.addAllOption) {
                    return [{value: '', display: 'All'}].concat(resp);
                }
                return resp;
            })
            .then((resp: any) => {
                if (this.pleaseSelect) {
                    resp.unshift({value: '', display: 'Please select...'});
                }
                return resp;
            });
    }

    refresh() {
        this.dropdown.refresh();
    }

    getVendorName(id: number) {
        if (this.dropdown && Array.isArray(this.dropdown.items)) {
            const item = this.dropdown.items.find(x => +x.value === +id);
            if (item) {
                return item.display;
            }
        }
        return '';
    }

    ngOnInit() {
        this.subs = this.form.get('id').valueChanges.subscribe(val => this.onChange && this.onChange(val));
    }

    ngOnDestroy() {
        this.subs.unsubscribe();
    }

    writeValue(obj: any): void {
        this.currentValue = obj;
        this.form.setValue({id: obj});
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

}
