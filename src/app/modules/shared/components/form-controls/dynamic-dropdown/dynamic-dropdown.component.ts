import { Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import { AbstractDropdown } from '../../../../../classes/abstract-dropdown';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DropdownItem } from '../../../../../interfaces/dropdown-item';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-dynamic-dropdown',
    templateUrl: './dynamic-dropdown.component.html',
    styleUrls: ['./dynamic-dropdown.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DynamicDropdownComponent),
            multi: true
        }
    ]
})
export class DynamicDropdownComponent extends AbstractDropdown implements OnInit, OnDestroy, ControlValueAccessor {

    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;
    currentValueFromForm: any;
    loadingsSubs: Subscription;

    @Input() autoLoad = true;
    @Input() addAllOption = false;
    @Input() pleaseSelect = false;

    constructor() {
        super();

        this.loadingsSubs = this.loadings().subscribe((list: DropdownItem[]) => {
            const listContainsItems = list.length > 0;
            if (!listContainsItems) {
                this.issueChange(undefined);
                return;
            }
            if (!list.some(x => x.value === this.currentValue)) {
                this.issueChange(list[0].value);
            }
        });
    }

    isSelected(item): boolean {
        return `${this.currentValue}` === `${item}`;
    }

    getSelected() {
        return this.items.find(x => `${this.currentValue}` === `${x.value}`);
    }

    ngOnInit() {
        if (this.autoLoad) {
            super.ngOnInit();
        }
    }

    ngOnDestroy() {
        this.loadingsSubs.unsubscribe();
    }

    writeValue(obj: any): void {
        this.currentValue = obj;
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

    valueChanged(event: any) {
        this.issueChange(event.target.value);
    }

    issueChange(val: any) {
        try {
            this.currentValue = val;
            this.onChange(val);
        } catch (exc) {
            console.log(exc);
        }
    }

}
