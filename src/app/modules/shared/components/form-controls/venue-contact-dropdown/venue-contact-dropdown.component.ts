import { Component, OnInit, forwardRef, Input, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { DynamicDropdownComponent } from '../dynamic-dropdown/dynamic-dropdown.component';
import { CurrentPropertyService } from '../../../../../services/current-property.service';
import { VenueContactModel } from '../../../../../models/venue-contact-model';
import { DropdownItem } from '../../../../../interfaces/dropdown-item';
import { VenueContactResourceService } from '../../../../../services/resources/venue-contact-resource.service';

@Component({
    selector: 'app-venue-contact-dropdown',
    templateUrl: './venue-contact-dropdown.component.html',
    styleUrls: ['./venue-contact-dropdown.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => VenueContactDropdownComponent),
            multi: true
        }
    ]
})
export class VenueContactDropdownComponent implements OnInit {
    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;
    subs: Subscription;
    formControl = new FormControl();
    @Input() addAllOption: boolean;
    @Input() pleaseSelect = true;

    @ViewChild(DynamicDropdownComponent) dropdown: DynamicDropdownComponent;

    constructor(private venueApi: CurrentPropertyService,
                private venueContactApi: VenueContactResourceService) {}

    private map(resp: VenueContactModel[]): DropdownItem[] {
        return resp.map((item) => ({
            value: item.id,
            display: item.firstName + ' ' + item.lastName
        }));
    }

    load = (): Promise<DropdownItem[]> => {
        return this.venueApi.currentVenueAsync()
            .then(venue => (
                this.venueContactApi.queryByVenueId(
                    this.venueContactApi.getAllQueryObject(),
                    venue.id
                )
            ))
            .then(response => this.map(response.items))
            .then(contacts => {
                if (this.addAllOption) {
                    return [{value: '', display: 'All'}].concat(contacts);
                }
                return contacts;
            })
            .then((contacts: any) => {
                if (this.pleaseSelect) {
                    contacts.unshift({value: '', display: 'Please select...'});
                }
                return contacts;
            });
    }

    refresh() {
        this.dropdown.refresh();
    }

    getContactName(id: number) {
        if (this.dropdown && Array.isArray(this.dropdown.items)) {
            const item = this.dropdown.items.find(x => +x.value === +id);
            if (item) {
                return item.display;
            }
        }
        return '';
    }

    ngOnInit() {
        this.subs = this.formControl.valueChanges.subscribe(val => this.onChange && this.onChange(val));
    }

    ngOnDestroy() {
        this.subs.unsubscribe();
    }

    writeValue(obj: any): void {
        this.currentValue = obj;
        this.formControl.setValue(obj);
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }
}
