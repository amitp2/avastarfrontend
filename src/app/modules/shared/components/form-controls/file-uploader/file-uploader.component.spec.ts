import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { FileUploaderComponent } from './file-uploader.component';
import { FileUploaderProgressDirective } from '../../../../../directives/form-controls/file-uploader/file-uploader-progress.directive';
import { FileUploaderResultDirective } from '../../../../../directives/form-controls/file-uploader/file-uploader-result.directive';
import { Component, DebugElement } from '@angular/core';
import { FileUploader } from '../../../../../interfaces/file-uploader';
import { Observable } from 'rxjs/Observable';
import { FileUploaderProgress } from '../../../../../interfaces/file-uploader-progress';
import { By } from '@angular/platform-browser';
import { Subject } from 'rxjs/Subject';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AsyncTasksService, AsyncTaskState, AsyncTaskStatus } from '../../../../../services/async-tasks.service';
import { AppSuccess } from '../../../../../enums/app-success.enum';

import 'rxjs/add/operator/first';
import { AllowedFileType } from '../../../../../enums/allowed-file-type.enum';

@Component({
    template: `
        <form [formGroup]="fileForm">

            <app-file-uploader formControlName="file" [fileUploader]="uploader">
            </app-file-uploader>

        </form>
    `
})
class TestHostComponent {
    uploader: FileUploader;
    fileForm: FormGroup;

    constructor() {
        this.fileForm = new FormGroup({
            file: new FormControl()
        });
    }
}


describe('FileUploaderComponent', () => {
    let component: TestHostComponent;
    let fixture: ComponentFixture<TestHostComponent>;
    let fileChooser: DebugElement;
    let progressSubject: Subject<FileUploaderProgress>;
    let asyncTasksService: AsyncTasksService;

    function fireChangeEvent(element: DebugElement, value: any) {
        // element.nativeElement.files = new FileList();

        spyOn(element.nativeElement.files, 'item').and.returnValue(value);

        const evt = document.createEvent('HTMLEvents');
        evt.initEvent('change', false, true);
        element.nativeElement.dispatchEvent(evt);
        fixture.detectChanges();
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                FileUploaderComponent,
                FileUploaderProgressDirective,
                FileUploaderResultDirective,
                TestHostComponent
            ],
            imports: [
                FormsModule,
                ReactiveFormsModule
            ],
            providers: [
                AsyncTasksService
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TestHostComponent);
        fileChooser = fixture.debugElement.query(By.css('[type=file]'));
        progressSubject = new Subject<FileUploaderProgress>();
        component = fixture.componentInstance;
        component.uploader = {
            upload(file: any): Observable<FileUploaderProgress> {
                return progressSubject;
            },
            asyncTaskName(): string {
                return 'some-upload-task';
            },
            uploadAppSuccess(): AppSuccess {
                return 2;
            },
            allowedFileType(): AllowedFileType {
                return AllowedFileType.All;
            }
        };
        fixture.detectChanges();
        asyncTasksService = TestBed.get(AsyncTasksService);
    });

    it('should call file uploader\'s upload method once file chosen, with chosen file', () => {
        const spy = spyOn(component.uploader, 'upload').and.callThrough();
        fireChangeEvent(fileChooser, 'some/path');
        expect(spy.calls.all().length).toEqual(1);
        expect(spy.calls.first().args[0]).toEqual('some/path');
    });

    it('should start async task with uploader\'s name once file chosen', fakeAsync(() => {
        let res;
        asyncTasksService.track('some-upload-task').subscribe(evt => res = evt);
        fireChangeEvent(fileChooser, {name: 'some/path'});
        tick();
        fixture.detectChanges();
        expect(res).toEqual(new AsyncTaskStatus(
            'some-upload-task',
            AsyncTaskState.IN_PROGRESS,
            {file: 'some/path'}
        ));
    }));

    it('it should report progress using async tasks service, with NOTIFY event', fakeAsync(() => {
        let res;
        asyncTasksService.track('some-upload-task')
            .filter(evt => evt.state === AsyncTaskState.NOTIFY)
            .subscribe(evt => res = evt);

        fireChangeEvent(fileChooser, 'some/path');

        progressSubject.next({
            completed: 50,
            fileUrl: 'some/path'
        });
        tick();
        fixture.detectChanges();
        expect(res).toEqual(new AsyncTaskStatus('some-upload-task', AsyncTaskState.NOTIFY, {uploaded: 50}));

        progressSubject.next({
            completed: 70,
            fileUrl: 'some/path'
        });
        tick();
        fixture.detectChanges();
        expect(res).toEqual(new AsyncTaskStatus('some-upload-task', AsyncTaskState.NOTIFY, {uploaded: 70}));

        progressSubject.next({
            completed: 56,
            fileUrl: 'some/path'
        });
        tick();
        fixture.detectChanges();
        expect(res).toEqual(new AsyncTaskStatus('some-upload-task', AsyncTaskState.NOTIFY, {uploaded: 56}));
    }));

    it('disabling/enabling form control, should disable/enable the chooser element', () => {
        component.fileForm.get('file').disable();
        fixture.detectChanges();
        expect(fileChooser.nativeElement.getAttribute('disabled')).toEqual('');


        component.fileForm.get('file').enable();
        fixture.detectChanges();
        expect(fileChooser.nativeElement.getAttribute('disabled')).not.toEqual('');
    });

    it('finishing upload should update the form value with uploaded file URL', fakeAsync(() => {
        expect(component.fileForm.get('file').value).toBeFalsy();

        fireChangeEvent(fileChooser, 'some/path');
        progressSubject.next({
            completed: 100,
            fileUrl: 'uploaded/some/file'
        });
        tick();
        fixture.detectChanges();

        expect(component.fileForm.get('file').value).toEqual('uploaded/some/file');
    }));

    it('finishing upload should fire SUCCESS status for given async task', fakeAsync(() => {
        let res;
        asyncTasksService.track('some-upload-task')
            .filter(evt => evt.state === AsyncTaskState.SUCCESS)
            .filter(evt => +evt.payload !== 2)
            .subscribe(evt => res = evt);

        fireChangeEvent(fileChooser, {name: 'some/path'});
        progressSubject.next({
            completed: 100,
            fileUrl: 'uploaded/some/file'
        });
        tick();
        fixture.detectChanges();
        expect(res).toEqual(new AsyncTaskStatus('some-upload-task', AsyncTaskState.SUCCESS, {file: 'uploaded/some/file'}));
    }));

    it('file chooser should be disabled while upload is in progress', fakeAsync(() => {
        fireChangeEvent(fileChooser, 'some/path');
        progressSubject.next({
            completed: 50,
            fileUrl: 'some/path'
        });
        tick();
        fixture.detectChanges();
        expect(fileChooser.nativeElement.getAttribute('disabled')).toEqual('');

        progressSubject.next({
            completed: 70,
            fileUrl: 'some/path'
        });
        tick();
        fixture.detectChanges();
        expect(fileChooser.nativeElement.getAttribute('disabled')).toEqual('');

        progressSubject.next({
            completed: 0,
            fileUrl: 'some/path'
        });
        tick();
        fixture.detectChanges();
        expect(fileChooser.nativeElement.getAttribute('disabled')).not.toEqual('');

        progressSubject.next({
            completed: 100,
            fileUrl: 'some/path'
        });
        tick();
        fixture.detectChanges();
        expect(fileChooser.nativeElement.getAttribute('disabled')).not.toEqual('');
    }));

});
