import { Component, ContentChild, forwardRef, Input, OnInit, TemplateRef } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { FileUploader } from '../../../../../interfaces/file-uploader';
import { FileUploaderProgressDirective } from '../../../../../directives/form-controls/file-uploader/file-uploader-progress.directive';
import { FileUploaderResultDirective } from '../../../../../directives/form-controls/file-uploader/file-uploader-result.directive';
import { FileUploaderProgress } from '../../../../../interfaces/file-uploader-progress';
import { AsyncTasksService } from '../../../../../services/async-tasks.service';
import { AllowedFileType } from '../../../../../enums/allowed-file-type.enum';
import { AppError } from '../../../../../enums/app-error.enum';

@Component({
    selector: 'app-file-uploader',
    templateUrl: './file-uploader.component.html',
    styleUrls: ['./file-uploader.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => FileUploaderComponent),
            multi: true
        }
    ]
})
export class FileUploaderComponent implements OnInit, ControlValueAccessor {


    @Input() fileUploader: FileUploader;

    @ContentChild(FileUploaderProgressDirective, {read: TemplateRef}) progressTemplate;
    @ContentChild(FileUploaderResultDirective, {read: TemplateRef}) resultTemplate;

    uploaded: FileUploaderProgress;
    isDisabled: boolean;
    uploadProgress: Subscription;
    accept: any;

    onChange = (fileUrl: string) => {
    };
    onTouched = () => {
    };

    constructor(private asyncTasksService: AsyncTasksService) {
        this.isDisabled = false;
        this.uploaded = null;
    }

    fileChosen(files: File[]) {
        if (!files.length) {
            return;
        }

        if (this.fileUploader.allowedFileType() === AllowedFileType.Images) {
            if (!files[0].name.match(/.(jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF)$/)) {
                this.asyncTasksService.taskError('incorrect_file_type', AppError.INVALID_FILE_FORMAT);
                return;
            }
        }

        this.asyncTasksService.taskStart(this.fileUploader.asyncTaskName(), {
            file: files[0].name
        });
        this.uploadProgress = this.fileUploader
            .upload(files[0])
            .subscribe((progress: FileUploaderProgress) => {
                this.uploaded = progress;
                this.asyncTasksService.taskNotify(this.fileUploader.asyncTaskName(), {
                    uploaded: this.uploaded.completed
                });
                if (progress.completed === 100) {
                    this.asyncTasksService.taskSuccess(this.fileUploader.asyncTaskName(), {
                        file: this.uploaded.fileUrl
                    });
                    this.asyncTasksService.taskSuccess(
                        this.fileUploader.asyncTaskName(),
                        this.fileUploader.uploadAppSuccess()
                    );
                    this.onChange(progress.fileUrl);
                    this.uploadProgress.unsubscribe();
                }
            }, (error) => {
                if (this.fileUploader.uploadAppError) {
                    this.asyncTasksService.taskError(this.fileUploader.asyncTaskName(), this.fileUploader.uploadAppError());
                }
            });
    }


    ngOnInit(): void {
        console.log(this.fileUploader)
        if (this.fileUploader.allowedFileType() === AllowedFileType.Images) {
            this.accept = 'image/*';
        } else {
            this.accept = '';
        }
    }

    uploadIsInProgress(): boolean {
        return this.uploaded && this.uploaded.completed > 0 && this.uploaded.completed < 100;
    }

    writeValue(fileUrl: string): void {
        if (!fileUrl) {
            return;
        }
        this.uploaded = {
            completed: 100,
            fileUrl
        };
        if (this.uploadProgress) {
            this.uploadProgress.unsubscribe();
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

}
