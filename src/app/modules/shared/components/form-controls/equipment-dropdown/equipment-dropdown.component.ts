import { Component, OnInit, Input, Output, EventEmitter, ViewChild, OnDestroy, forwardRef } from '@angular/core';
import { EquipmentResourceService } from '../../../../../services/resources/equipment-resource.service';
import { CurrentPropertyService } from '../../../../../services/current-property.service';
import { FormGroup, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { DynamicDropdownComponent } from '../dynamic-dropdown/dynamic-dropdown.component';
import { EquipmentServiceType } from '../../../../../enums/equipment';

@Component({
    selector: 'app-equipment-dropdown',
    templateUrl: './equipment-dropdown.component.html',
    styleUrls: ['./equipment-dropdown.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => EquipmentDropdownComponent),
            multi: true
        }
    ]
})
export class EquipmentDropdownComponent implements OnInit, OnDestroy {
    _subCategoryId: number;
    apiUrl: string;
    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;
    formControl: FormControl = new FormControl('');
    subs: Subscription;

    @Output() changed: EventEmitter<number> = new EventEmitter();
    @Input() addAllOption: boolean;
    @Input() pleaseSelect = false;
    @ViewChild('dropdown') dropdown: DynamicDropdownComponent;

    get subCategoryId() {
        return this._subCategoryId;
    }

    @Input()
    set subCategoryId(value: number) {
        this._subCategoryId = value;
        if (this.dropdown) {
            this.dropdown.refresh();
        }
    }
    constructor(
        private equipmentApi: EquipmentResourceService,
        private currentVenue: CurrentPropertyService
    ) {
    }

    load = () => {
        if (!this.subCategoryId) {
            return Promise.resolve([]);
        }
        return this.currentVenue.currentVenueAsync()
            .then(venue => (
                this.equipmentApi.queryByVenueId({
                    page: 0,
                    size: 999999,
                    sortFields: [],
                    sortAsc: true,
                    otherParams: {
                        subCategoryId: this.subCategoryId,
                        type: EquipmentServiceType.Equipment
                    }
                }, venue.id)
            ))
            .then(response => this.mapToOptions(response.items));
    }

    getPrice() {
        if (!this.dropdown) {
            return null;
        }
        const item = this.dropdown.getSelected() as any;
        return item ? item.price : null;
    }

    getName() {
        if (!this.dropdown) {
            return null;
        }
        const item = this.dropdown.getSelected();
        return item ? item.display : null;
    }

    private mapToOptions(records: any[]) {
        return records.map(x => ({
            value: x.id,
            display: x.subCategoryItemName,
            price: x.price
        }));
    }

    ngOnInit() {
        this.subs = this.formControl.valueChanges
            .subscribe(val => {
                if (this.onChange) {
                    this.onChange(val);
                }
                this.changed.next(val);
            });
    }

    ngOnDestroy() {
        this.subs.unsubscribe();
    }

    writeValue(obj: number): void {
        this.currentValue = obj;
        if (obj) {
            this.formControl.setValue(obj);
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
        if (isDisabled) {
            this.formControl.disable();
        } else {
            this.formControl.enable();
        }
    }
}
