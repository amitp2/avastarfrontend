import { Component, ElementRef, forwardRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { GlobalsService } from '../../../../../services/globals.service';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import * as moment from 'moment';

@Component({
    selector: 'app-date-time-picker',
    templateUrl: './date-time-picker.component.html',
    styleUrls: ['./date-time-picker.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DateTimePickerComponent),
            multi: true
        }
    ]
})
export class DateTimePickerComponent implements ControlValueAccessor, OnInit, OnDestroy {

    format = 'Y-m-d H:i';
    momentFormat = 'YYYY-MM-DD HH:mm';

    @ViewChild('calendar')
    calendar: ElementRef;

    @Input() set minDate(val: string) {
        this.minDateInner = val;
        this.setMinDate();
    }

    minDateInner: string;

    get $() {
        return this.globals.globals().$;
    }


    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;

    constructor(private globals: GlobalsService) {
    }

    ngOnInit() {
        const $ = this.globals.globals().$;
        const el = this.calendar.nativeElement;

        $(el).datetimepicker({
            format: this.format,
            step: 15
        });
        $(el).on('change', () => this.issueChange(el.value));

        this.setMinDate();
    }

    private setMinDate() {
        if (!this.minDateInner) {
            return;
        }
        const el = this.calendar.nativeElement;
        this.$(el).datetimepicker('setOptions', {
            minDate: this.minDateInner
        });
    }

    ngOnDestroy() {
        const $ = this.globals.globals().$;
        const el = this.calendar.nativeElement;
        $(el).off('change');
        $(el).datetimepicker('destroy');
    }

    private issueChange(val: any) {
        try {
            this.onChange(moment(val).valueOf());
        } catch (exc) {
            console.log('change function for calendar hasnt registered yet');
        }
    }

    writeValue(obj: any): void {
        const $ = this.globals.globals().$;
        this.currentValue = obj;
        if (obj) {
            $(this.calendar.nativeElement).val(moment(+obj).format(this.momentFormat).toString());
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }
}
