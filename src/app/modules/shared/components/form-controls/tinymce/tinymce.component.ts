import { AfterViewInit, Component, forwardRef, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';
import { GlobalsService } from '../../../../../services/globals.service';

declare var tinymce: any;

@Component({
    selector: 'app-tinymce',
    templateUrl: './tinymce.component.html',
    styleUrls: ['./tinymce.component.styl'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => TinymceComponent),
        multi: true
    }]
})
export class TinymceComponent implements ControlValueAccessor, AfterViewInit, OnDestroy, OnInit {

    onChange: any;
    editor: any;
    elementId: string;
    editorInitialized: BehaviorSubject<any>;
    valueChangedFromOutside: BehaviorSubject<string>;
    valueChangedFromInside: BehaviorSubject<string>;
    subs: Subscription[];

    constructor(private globals: GlobalsService) {
        this.elementId = 'tiny-' + Math.floor(Math.random() * 1000);
        this.editorInitialized = new BehaviorSubject<any>(null);
        this.valueChangedFromOutside = new BehaviorSubject<string>(null);
        this.valueChangedFromInside = new BehaviorSubject<string>(null);
        this.subs = [];
    }

    ngOnInit() {
        this.subs.push(
            this.editorInitialized
                .filter(initialized => initialized !== null)
                .combineLatest(this.valueChangedFromOutside.filter((value: string) => value !== null))
                .map(([initialized, value]) => value)
                .subscribe((value: string) => {
                    this.editor.setContent(value);
                })
        );

        this.subs.push(
            this.valueChangedFromInside
                .filter(value => value !== null)
                .subscribe((value: string) => {
                    if (this.onChange) {
                        this.onChange(value);
                    }
                })
        );
    }

    ngAfterViewInit() {
        tinymce.init({
            selector: `#${this.elementId}`,
            plugins: ['link', 'paste', 'table'],
            skin_url: '/assets/skins/lightgray',
            menubar: 'edit view insert format table',
            setup: editor => {
                this.editor = editor;

                editor.on('init', () => {
                    this.editorInitialized.next(true);
                    this.globals.globals().$('.mce-branding').remove();
                });
                editor.on('keyup NodeChange Change KeyDown', () => {
                    this.valueChangedFromInside.next(editor.getContent());
                });
            }
        });
    }

    ngOnDestroy() {
        tinymce.remove(this.editor);
        this.subs.forEach((s: Subscription) => s.unsubscribe());
    }

    writeValue(obj: any): void {
        this.valueChangedFromOutside.next(obj);
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
    }
}
