import { Component, forwardRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { DynamicDropdownComponent } from '../dynamic-dropdown/dynamic-dropdown.component';
import { InventoryCategoryResourceService } from '../../../../../services/resources/inventory-category-resource.service';
import { DropdownItem } from '../../../../../interfaces/dropdown-item';
import { InventorySubcategoryItemModel } from '../../../../../models/inventory-subcategory-item-model';

@Component({
    selector: 'app-inventory-sub-category-item-dropdown',
    templateUrl: './inventory-sub-category-item-dropdown.component.html',
    styleUrls: ['./inventory-sub-category-item-dropdown.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => InventorySubCategoryItemDropdownComponent),
            multi: true
        }
    ]
})
export class InventorySubCategoryItemDropdownComponent implements ControlValueAccessor, OnInit, OnDestroy {

    currentCategoryId: number;

    @Input() set categoryId(val: any) {
        this.currentCategoryId = val;
        this.refresh();
    }

    @Input() addAllOption: boolean;
    @Input() pleaseSelect = true;

    @ViewChild('dropdown') dropdown: DynamicDropdownComponent;

    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;
    form: FormGroup;
    subs: Subscription;

    constructor(private inventoryCategory: InventoryCategoryResourceService) {
        this.form = new FormGroup({
            id: new FormControl()
        });
    }

    private map(resp: InventorySubcategoryItemModel[]): DropdownItem[] {
        return resp.map((item) => ({
            value: item.id, display: item.name
        }));
    }

    load = (): Promise<DropdownItem[]> => {
        if (!this.currentCategoryId) {
            return Promise.resolve([]);
        }
        return this.inventoryCategory
            .getAllSubcategoryItems(this.currentCategoryId)
            .then((resp: InventorySubcategoryItemModel[]) => this.map(resp))
            .then((resp) => {

                if (this.addAllOption) {
                    return [{value: '', display: 'All'}].concat(resp);
                }

                return resp;
            })
            .then((resp: any[]) => {
                if (this.pleaseSelect) {
                    resp.unshift(<any>{value: '', display: 'Please select...'});
                }
                return resp;
            });
    }

    refresh() {
        if (this.dropdown) {
            this.dropdown.refresh();
        }
    }

    ngOnInit() {
        this.subs = this.form.get('id').valueChanges.subscribe(val => this.onChange && this.onChange(val));
    }

    ngOnDestroy() {
        this.subs.unsubscribe();
    }

    writeValue(obj: any): void {
        if (!obj) {
            return;
        }
        this.currentValue = obj;
        this.form.setValue({id: obj});
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

}
