import { Component, OnInit, Input, EventEmitter, Output, OnDestroy, forwardRef } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'app-year-dropdown',
    templateUrl: './year-dropdown.component.html',
    styleUrls: ['./year-dropdown.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => YearDropdownComponent),
            multi: true
        }
    ]
})
export class YearDropdownComponent implements OnInit, OnDestroy {
    @Input() from = 2000;
    @Input() to = 2050;
    @Input() optional = false;
    @Input() pleaseSelect = false;
    @Output() changed: EventEmitter<number> = new EventEmitter();

    private onChange: any;
    private onTouched: any;
    private isDisabled: boolean;
    private currentValue: any;
    private valueChangesSub: Subscription;
    public formControl: FormControl = new FormControl();

    get options() {
        const options = [];
        if (!this.from || !this.to) {
            return options;
        }
        for (let i = this.from; i < this.to + 1; i++) {
            options.push({
                value: i,
                display: i.toString()
            });
        }
        if (this.optional) {
            options.unshift({
                value: '',
                display: 'All'
            });
        }
        return options;
    }

    constructor() { }

    ngOnInit() {
        this.valueChangesSub = this.formControl.valueChanges
            .subscribe(val => {
                if (this.onChange) {
                    this.onChange(val);
                }
                this.changed.emit(val);
            });
    }

    ngOnDestroy() {
        this.valueChangesSub.unsubscribe();
    }

    writeValue(obj: any): void {
        if (!obj) {
            return;
        }
        this.currentValue = obj;
        this.formControl.setValue(obj);
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }
}
