import { Component, OnInit, EventEmitter, Output, Input, OnDestroy, forwardRef } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'app-month-dropdown',
    templateUrl: './month-dropdown.component.html',
    styleUrls: ['./month-dropdown.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => MonthDropdownComponent),
            multi: true
        }
    ]
})
export class MonthDropdownComponent implements OnInit, OnDestroy {
    @Input() optional = false;
    @Input() pleaseSelect = false;
    @Output() changed: EventEmitter<number> = new EventEmitter();

    private onChange: any;
    private onTouched: any;
    private isDisabled: boolean;
    private currentValue: any;
    private valueChangesSub: Subscription;
    public formControl: FormControl = new FormControl();

    constructor() { }

    ngOnInit() {
        this.valueChangesSub = this.formControl.valueChanges
            .subscribe(val => {
                if (this.onChange) {
                    this.onChange(val);
                }
                this.changed.emit(val);
            });
    }

    ngOnDestroy() {
        this.valueChangesSub.unsubscribe();
    }

    writeValue(obj: any): void {
        if (!obj) {
            return;
        }
        this.currentValue = obj;
        this.formControl.setValue(obj);
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }
}
