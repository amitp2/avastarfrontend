import { Component, forwardRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { InventoryCategoryModel } from '../../../../../models/inventory-category-model';
import { DropdownItem } from '../../../../../interfaces/dropdown-item';
import { InventoryCategoryResourceService } from '../../../../../services/resources/inventory-category-resource.service';
import { Subscription } from 'rxjs/Subscription';
import { DynamicDropdownComponent } from '../dynamic-dropdown/dynamic-dropdown.component';

@Component({
    selector: 'app-inventory-category-dropdown',
    templateUrl: './inventory-category-dropdown.component.html',
    styleUrls: ['./inventory-category-dropdown.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => InventoryCategoryDropdownComponent),
            multi: true
        }
    ]
})
export class InventoryCategoryDropdownComponent implements ControlValueAccessor, OnInit, OnDestroy {

    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;
    form: FormGroup;
    subs: Subscription;

    @Input() addAllOption: boolean;
    @Input() pleaseSelect = false;
    @ViewChild(DynamicDropdownComponent) dropdown: DynamicDropdownComponent;

    constructor(private inventoryCategory: InventoryCategoryResourceService) {
        this.form = new FormGroup({
            categoryId: new FormControl()
        });
    }

    private mapCategoriesToDropdown(resp: InventoryCategoryModel[]): DropdownItem[] {
        return resp.map((item) => ({
            value: item.id, display: item.name
        }));
    }

    loadCategories = (): Promise<DropdownItem[]> => {
        return this.inventoryCategory
            .getAll()
            .then((resp: InventoryCategoryModel[]) => this.mapCategoriesToDropdown(resp))
            .then((resp) => {
                if (this.pleaseSelect) {
                    return [{value: '', display: 'Please select...'}].concat(resp);
                }
                if (this.addAllOption) {
                    return [{value: '', display: 'All'}].concat(resp);
                }

                return resp;
            });
    }

    ngOnInit() {
        this.subs = this.form.get('categoryId').valueChanges.subscribe(val => this.onChange && this.onChange(val));
    }

    refresh() {
        if (this.dropdown) {
            this.dropdown.refresh();
        }
    }

    ngOnDestroy() {
        this.subs.unsubscribe();
    }

    writeValue(obj: any): void {
        this.currentValue = obj;
        this.form.setValue({categoryId: obj});
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

}
