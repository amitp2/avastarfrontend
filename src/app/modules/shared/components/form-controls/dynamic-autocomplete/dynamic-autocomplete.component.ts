import { Component, ElementRef, forwardRef, Input, NgZone, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { GlobalsService } from '../../../../../services/globals.service';
import { AutocompleteLoader } from '../../../../../interfaces/autocomplete-loader';

@Component({
    selector: 'app-dynamic-autocomplete',
    templateUrl: './dynamic-autocomplete.component.html',
    styleUrls: ['./dynamic-autocomplete.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DynamicAutocompleteComponent),
            multi: true
        }
    ]
})
export class DynamicAutocompleteComponent implements ControlValueAccessor, OnInit, OnDestroy {

    @ViewChild('text') textField: ElementRef;
    @Input() loadFunction: AutocompleteLoader;

    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;

    constructor(private globalsService: GlobalsService,
                private zone: NgZone) {
    }

    ngOnInit() {
        const $ = this.globalsService.globals().$;
        $(this.textField.nativeElement).autocomplete({
            source: (request, response) => {
                this.loadFunction(request.term)
                    .then((resp: any) => response(resp));
            },
            select: (event, ui) => {
                this.zone.run(() => this.issueChange(ui.item.value));
            },
            change: () => {
                this.zone.run(() => this.issueChange(this.textField.nativeElement.value));
            }
        });

        $(this.textField.nativeElement).on('input', () => {
            this.zone.run(() => {
                this.issueChange(this.textField.nativeElement.value);
            });
        });
    }

    ngOnDestroy() {
        const $ = this.globalsService.globals().$;
        $(this.textField.nativeElement).off('input');
    }

    private issueChange(val: any) {
        try {
            this.onChange(val);
        } catch (exc) {
            console.log('change function isnt registered yet');
        }
    }

    writeValue(obj: any): void {
        const $ = this.globalsService.globals().$;
        this.currentValue = obj;
        $(this.textField.nativeElement).val(obj);
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

}
