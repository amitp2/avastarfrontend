import { Component, ElementRef, forwardRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { GlobalsService } from '../../../../../services/globals.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';

export interface MultiSelectItem {
    id: any;
    text: any;
    disabled?: boolean;
}

@Component({
    selector: 'app-multi-select',
    templateUrl: './multi-select.component.html',
    styleUrls: ['./multi-select.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => MultiSelectComponent),
            multi: true
        }
    ]
})
export class MultiSelectComponent implements ControlValueAccessor, OnInit, OnDestroy {

    onChange: any;
    onTouched: any;
    isDisabled: boolean;

    incoming: any[];
    incomingValues: BehaviorSubject<any>;
    incomingValuesSubs: Subscription;

    @ViewChild('select') select: ElementRef;
    @Input() selection: MultiSelectComponent[];

    constructor(private globals: GlobalsService) {
        this.incomingValues = new BehaviorSubject<any>(null);
    }

    ngOnInit() {
        // console.log(this.select);
        this.incoming = [];
        const $ = this.globals.globals().$;
        const el = this.select.nativeElement;
        $(el).select2({
            data: this.selection,
            multiple: true,
            templateResult: (state: any) => {
                if (state.disabled) {
                    return state.text;
                }
                // const selected = state.selected || this.incoming ? this.incoming.indexOf(state.id) !== -1 : '';
                let selected = '';
                if(this.incoming){
                    selected = state.selected || this.incoming.indexOf(state.id) !== -1;
                }
                else {
                   selected = state.selected;
                }

                const alteredState = `<input type="checkbox" ${selected ? 'checked' : ''}>&nbsp;<span>${state.text}</span>`;
                return $(alteredState);
            }
        });

        this.incomingValuesSubs = this.incomingValues
            .filter(val => val ? true : false)
            .subscribe((val: any) => {
                $(el).val(val);
                // $(el).trigger('select2:select');
                $(el).trigger('change');
            });

        $(el).on('select2:select', () => {
            this.onChange($(el).select2('data').map((sel: any) => sel.id));
            this.incoming = $(el).select2('data').map((sel: any) => sel.id);
        });

        $(el).on('select2:unselect', () => {
            this.onChange($(el).select2('data').map((sel: any) => sel.id));
            this.incoming = $(el).select2('data').map((sel: any) => sel.id);
        });
    }

    ngOnDestroy() {
        this.incomingValuesSubs.unsubscribe();
        const $ = this.globals.globals().$;
        const el = this.select.nativeElement;
        $(el).select2('destroy');
        $(el).off('select2:select');
        $(el).off('select2:unselect');
        console.log('off is this');
    }

    writeValue(val: any): void {
        this.incoming = val;
        this.incomingValues.next(val);
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

}
