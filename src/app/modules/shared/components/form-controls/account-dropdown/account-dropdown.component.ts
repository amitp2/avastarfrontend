import { Component, Input, OnDestroy, OnInit, ViewChild, forwardRef } from '@angular/core';
import { ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { DropdownItem } from '../../../../../interfaces/dropdown-item';
import { VenueClientResourceService } from '../../../../../services/resources/venue-client-resource.service';
import { PromisedStoreService } from '../../../../../services/promised-store.service';
import { VenueClientModel } from '../../../../../models/venue-client-model';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../../store/store';
import { UtilsService } from '../../../../../services/utils.service';
import { SetAllAccounts } from '../../../../../store/all-accounts/all-accounts.actions';
import { DynamicDropdownComponent } from '../dynamic-dropdown/dynamic-dropdown.component';

@Component({
    selector: 'app-account-dropdown',
    templateUrl: './account-dropdown.component.html',
    styleUrls: ['./account-dropdown.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => AccountDropdownComponent),
            multi: true
        }
    ]
})
export class AccountDropdownComponent implements ControlValueAccessor, OnInit, OnDestroy {

    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;
    form: FormGroup;

    subs: Subscription;
    allAccountsSubs: Subscription;

    @ViewChild('dropdown') dropdown: DynamicDropdownComponent;
    @Input() addAllOption: boolean;
    @Input() optional: boolean;

    constructor(private accounts: VenueClientResourceService,
                private promisedStore: PromisedStoreService,
                private store: Store<AppState>,
                private utils: UtilsService) {
        this.form = new FormGroup({
            accountId: new FormControl()
        });
    }

    load = (): Promise<DropdownItem[]> => {

        return this.promisedStore.allAccounts()
            .then((clients: VenueClientModel[]) => clients.map((client: VenueClientModel) => ({
                value: client.id,
                display: client.name
            })))
            .then((resp: any) => {
                if (this.optional) {
                    return [{value: null, display: 'Please select...'}].concat(resp);
                }
                if (this.addAllOption) {
                    return [{value: '', display: 'All'}].concat(resp);
                }
                return resp;
            });
    };

    ngOnInit() {
        this.subs = this.form.get('accountId')
            .valueChanges
            .subscribe(val => this.onChange && this.onChange(val));

        this.promisedStore
            .currentVenueId()
            .then((venueId: number) => this.accounts.getAllByVenueID(venueId))
            .then((resp: any) => this.store.dispatch(new SetAllAccounts(resp)));


        this.allAccountsSubs = this.store.select('allAccounts').subscribe(() => this.dropdown.refresh());
    }

    ngOnDestroy() {
        this.subs.unsubscribe();
        this.allAccountsSubs.unsubscribe();
    }

    writeValue(obj: number): void {
        this.currentValue = obj;
        if (obj) {
            this.form.setValue({
                accountId: obj
            });
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

}
