import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { VendorContactResourceService } from '../../../../services/resources/vendor-contact-resource.service';
import { VendorContactModel } from '../../../../models/vendor-contact-model';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { UserRole } from '../../../../enums/user-role.enum';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppError } from '../../../../enums/app-error.enum';

@Component({
    selector: 'app-vendor-contact-create-button',
    templateUrl: './vendor-contact-create-button.component.html',
    styleUrls: ['./vendor-contact-create-button.component.styl']
})
export class VendorContactCreateButtonComponent implements OnInit {
    @Input() vendorId: number;
    @Output() contactCreated: EventEmitter<VendorContactModel> = new EventEmitter();

    form: FormGroup;
    popupVisible = false;
    userRole = UserRole;

    constructor(
        private fb: FormBuilder,
        private vendorContacts: VendorContactResourceService,
    ) {
        this.buildForm();
    }

    buildForm() {
        this.form = this.fb.group({
            firstName: ['', [Validators.required, Validators.maxLength(50)]],
            lastName: ['', [Validators.required, Validators.maxLength(50)]],
            title: ['', [Validators.maxLength(100)]],
            email: ['', [Validators.required, Validators.email, Validators.maxLength(100)]],
            mobile: ['', [Validators.maxLength(30)]],
            phone: ['', [Validators.required, Validators.maxLength(30)]],
            role: [UserRole.NONE, [Validators.required]],
            notes: ['', [Validators.maxLength(400)]],
            fax: ['', [Validators.maxLength(30)]],
            type: ['', [Validators.maxLength(100)]],
            department: [null, [Validators.maxLength(250)]]
        });
    }

    close() {
        this.popupVisible = false;
        this.form.reset({
            role: UserRole.NONE
        });
    }

    submit() {
        if (this.form.invalid || !this.vendorId) {
            return;
        }
        this.add(new VendorContactModel({ ...this.form.value }));
    }

    @AsyncMethod({
        taskName: 'add_vendor_contact',
        success: AppSuccess.VENDOR_CONTACT_ADD,
        error: AppError.VENDOR_CONTACT_ADD
    })
    private add(contact: VendorContactModel) {
        return this.vendorContacts.addForVendor(contact, this.vendorId)
            .then((entity) => {
                this.contactCreated.emit(entity);
                this.close();
            });
    }

    ngOnInit() {
    }

}
