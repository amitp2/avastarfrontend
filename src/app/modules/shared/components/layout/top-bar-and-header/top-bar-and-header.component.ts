import { Component } from '@angular/core';
import { UserRole } from '../../../../../enums/user-role.enum';
import { CurrentPropertyService } from '../../../../../services/current-property.service';
import { Observable } from 'rxjs/Observable';
import { VenueModel } from '../../../../../models/venue-model';
import { Router } from '@angular/router';

@Component({
    selector: 'app-top-bar-and-header',
    templateUrl: './top-bar-and-header.component.html',
    styleUrls: ['./top-bar-and-header.component.styl']
})
export class TopBarAndHeaderComponent {
    currentSubscriberHasVenue: Observable<boolean>;
    userRole = UserRole;
    currentVenuesList: Observable<VenueModel[]>;
    currentVenue: Observable<VenueModel>;
    menuBarVisible: boolean;
    adminShowSub: boolean;
    accShowSub: boolean;
    inventoryShowSub: boolean;
    reportShowSub: boolean;
    expensesShowSub: boolean;
    eventsShowSub = false;

    constructor(
        private currentPropertyService: CurrentPropertyService,
        private router: Router
    ) {
        this.currentVenuesList = this.currentPropertyService.currentVenuesList;
        this.currentVenue = this.currentPropertyService.currentVenue;
        this.currentSubscriberHasVenue = this.currentPropertyService.currentSubscriberHasVenue;
        this.currentPropertyService.load();
        this.menuBarVisible = false;
        this.adminShowSub = false;
        this.accShowSub = false;
        this.reportShowSub = false;
        this.expensesShowSub = false;

        this.router.events.subscribe(() => {
            this.menuBarVisible = false;
            this.adminShowSub = false;
            this.accShowSub = false;
            this.inventoryShowSub = false;
            this.reportShowSub = false;
            this.expensesShowSub = false;
        });
    }

    select(venue: VenueModel) {
        this.currentPropertyService.select(venue);
    }

}
