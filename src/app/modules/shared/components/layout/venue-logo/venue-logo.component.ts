import { Component, OnDestroy } from '@angular/core';
import { CurrentPropertyService } from '../../../../../services/current-property.service';
import { Observable } from 'rxjs/Observable';
import { VenueModel } from '../../../../../models/venue-model';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../../store/store';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-venue-logo',
    templateUrl: './venue-logo.component.html',
    styleUrls: ['./venue-logo.component.styl']
})
export class VenueLogoComponent implements OnDestroy {

    image: string;
    subs: Subscription;

    constructor(private store: Store<AppState>) {

        this.subs = this.store
            .select('currentVenue')
            .filter((venue: VenueModel) => venue.logoUrl ? true : false)
            .subscribe((venue: VenueModel) => {
                this.image = venue.logoUrl;
            });
    }

    ngOnDestroy() {
        this.subs.unsubscribe();
    }


}
