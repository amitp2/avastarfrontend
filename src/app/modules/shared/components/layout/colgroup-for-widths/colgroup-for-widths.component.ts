import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: '[app-colgroup-for-widths]',
    templateUrl: './colgroup-for-widths.component.html',
    styleUrls: ['./colgroup-for-widths.component.styl']
})
export class ColgroupForWidthsComponent {

    @Input() set colsCount(val: number) {
        const cols = [];
        for (let i = 1; i <= val; i++) {
            cols.push(100 / val);
        }
        this.cols = cols;
    }

    cols: number[];

    constructor() {
    }


}
