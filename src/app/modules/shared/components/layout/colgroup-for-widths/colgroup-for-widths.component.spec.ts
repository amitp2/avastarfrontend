import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColgroupForWidthsComponent } from './colgroup-for-widths.component';
import { By } from '@angular/platform-browser';

describe('ColgroupForWidthsComponent', () => {
    let component: ColgroupForWidthsComponent;
    let fixture: ComponentFixture<ColgroupForWidthsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ColgroupForWidthsComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ColgroupForWidthsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('it should generate cols of given count', () => {
        component.colsCount = 3;
        fixture.detectChanges();
        expect(fixture.debugElement.queryAll(By.css('col')).length).toEqual(3);

        component.colsCount = 5;
        fixture.detectChanges();
        expect(fixture.debugElement.queryAll(By.css('col')).length).toEqual(5);
    });

    it('generated cols should have proper width percentage', () => {
        component.colsCount = 3;
        fixture.detectChanges();

        const count = 100 / 3;

        fixture.debugElement.queryAll(By.css('col')).forEach(col => {
            expect(col.nativeElement.getAttribute('width')).toEqual(`${count}%`);
        });
    });
});
