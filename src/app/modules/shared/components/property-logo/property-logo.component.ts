import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../services/auth.service';
import { UtilsService } from '../../../../services/utils.service';
import { VenueResourceService } from '../../../../services/resources/venue-resource.service';
import { CurrentUser } from '../../../../interfaces/current-user';

@Component({
    selector: 'app-property-logo',
    templateUrl: './property-logo.component.html',
    styleUrls: ['./property-logo.component.styl']
})
export class PropertyLogoComponent implements OnInit {

    image: string;

    constructor(private authService: AuthService,
                private venueResourceService: VenueResourceService) {

        this.authService.currentUser().then((user: CurrentUser) => {
            // if (user.subscriberId) {
            //     this.venueResourceService.
            // }
        });
    }

    ngOnInit() {
    }

}
