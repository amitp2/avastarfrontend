import { AfterViewInit, Component, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

const openModalSet = new Set();

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.styl']
})
export class ModalComponent implements OnInit, OnDestroy {
    private _visible = true;
    private viewReady = false;
    @Input() redirectToRoute: string;
    @Input() closeDisabled = false;
    @Output() close = new EventEmitter();

    @Input() set visible(visible: boolean) {
        this._visible = visible;
        if (visible) {
            openModalSet.add(this);
            setTimeout(() => this.viewReady = true, 200);
        } else {
            openModalSet.delete(this);
            this.viewReady = false;
        }
        checkOpenModalStatus();
    }

    get visible(): boolean {
        return this._visible;
    }

    constructor(private router: Router) {
    }

    ngOnInit() {
        this.visible = this.visible;
        setTimeout(() => this.viewReady = true, 200);
    }

    ngOnDestroy(): void {
        this.visible = false;
    }

    onClose() {
        if (this.closeDisabled) {
            return;
        }
        this.visible = false;
        this.close.emit();
        if (this.redirectToRoute) {
            this.router.navigateByUrl(this.redirectToRoute);
        }
    }

    @HostListener('window:keyup', ['$event'])
    private onKeyUp(ev: KeyboardEvent) {
        if (ev.which === 27) {
            this.onClose();
        }
    }
}

function checkOpenModalStatus() {
    if (openModalSet.size === 0) {
        document.body.classList.remove('modal-open');
    } else {
        document.body.classList.add('modal-open');
    }
}
