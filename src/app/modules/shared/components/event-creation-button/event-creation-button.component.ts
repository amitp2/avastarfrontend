import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppError } from '../../../../enums/app-error.enum';
import { FormMode } from '../../../../enums/form-mode.enum';
import { VenueEventModel, VenueEventStatus, VenueEventType } from '../../../../models/venue-event-model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { UtilsService } from '../../../../services/utils.service';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { PromisedStoreService } from '../../../../services/promised-store.service';
import { VenueEventResourceService } from '../../../../services/resources/venue-event-resource.service';
import { FormHandler } from '../../../../classes/form-handler';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';
import { EventAdded } from '../../../../store/all-events/all-events.actions';

@Component({
    selector: 'app-event-creation-button',
    templateUrl: './event-creation-button.component.html',
    styleUrls: ['./event-creation-button.component.styl']
})
export class EventCreationButtonComponent extends FormHandler<VenueEventModel> implements OnInit {

    popupVisible = false;

    type = VenueEventType;
    status = VenueEventStatus;

    @Input()
    accountId: number;

    get backUrl(): string {
        return `/u/accounts/${this.accountId}/account-events`;
    }


    constructor(private activatedRoute: ActivatedRoute,
                private events: VenueEventResourceService,
                private utils: UtilsService,
                private promisedStore: PromisedStoreService,
                private router: Router,
                private store: Store<AppState>) {
        super();
        // this.accountId = +this.activatedRoute.parent.snapshot.params['id'];
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute, 'eventId');
    }

    formMode(): Observable<FormMode> {
        return new BehaviorSubject<FormMode>(FormMode.Add);
    }

    initializeForm(): FormGroup {
        return new FormGroup({
            name: new FormControl(null, [Validators.required]),
            code: new FormControl(),
            type: new FormControl(VenueEventType.Local),
            status: new FormControl(VenueEventStatus.Active),
            masterAccountNumber: new FormControl(),
            eventDescription: new FormControl()
        });
    }

    @AsyncMethod({
        taskName: 'get_account_event'
    })
    get(id: number): Promise<VenueEventModel> {
        return this.events.get(id);
    }

    @AsyncMethod({
        taskName: 'add_event',
        success: AppSuccess.ADD_EVENT,
        error: AppError.ADD_EVENT
    })
    add(model: VenueEventModel): Promise<any> {
        return this.promisedStore.currentVenueId()
            .then((venueId: number) => {
                model.venueId = venueId;
                model.accountId = this.accountId;
                return this.events.add(model);
            })
            .then((added: VenueEventModel) => this.store.dispatch(new EventAdded(added)))
            .then(() => this.popupVisible = false);
    }

    @AsyncMethod({
        taskName: 'edit_event',
        success: AppSuccess.EDIT_EVENT,
        error: AppError.EDIT_EVENT
    })
    edit(id: number, model: VenueEventModel): Promise<any> {
        return this.promisedStore.currentVenueId()
            .then((venueId: number) => {
                model.venueId = venueId;
                return this.events.edit(id, model);
            })
            .then(() => this.router.navigateByUrl(this.backUrl));
    }

    serialize(model: VenueEventModel): any {
        return this.utils.transformInto(model, {
            venueId: {to: 'venueId'},
            name: {to: 'name'},
            code: {to: 'code'},
            masterAccountNumber: {to: 'masterAccountNumber'},
            eventDescription: {to: 'eventDescription'},
            type: {to: 'type'},
            status: {to: 'status'}
        });
    }

    deserialize(formValue: any): VenueEventModel {
        return new VenueEventModel(this.utils.transformInto(formValue, {
            venueId: {to: 'venueId'},
            name: {to: 'name'},
            code: {to: 'code'},
            masterAccountNumber: {to: 'masterAccountNumber'},
            eventDescription: {to: 'eventDescription'},
            type: {to: 'type'},
            status: {to: 'status'}
        }));
    }

    cancel() {
        this.router.navigateByUrl(this.backUrl);
    }

}
