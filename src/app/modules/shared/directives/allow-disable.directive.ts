import { Directive, HostBinding, Input, OnInit } from '@angular/core';
import { UserRole } from '../../../enums/user-role.enum';
import { AuthService } from '../../../services/auth.service';
import { CurrentUser } from '../../../interfaces/current-user';

@Directive({
    selector: '[appAllowDisable]'
})
export class AllowDisableDirective implements OnInit {

    @HostBinding('attr.disabled') disabled: string;
    @Input() appAllowDisable: UserRole[];

    constructor(private authService: AuthService) {
    }

    ngOnInit() {
        this.authService.currentUser().then((user: CurrentUser) => {
            let has = false;
            user.roles.forEach((role: UserRole) => {
                if (this.appAllowDisable.filter(r => r === role).length > 0) {
                    has = true;
                }
            });
            if (has) {
                this.disabled = null;
            } else {
                this.disabled = 'disabled';
            }
        });
    }

}
