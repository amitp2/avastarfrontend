import { CustomScrollbarDirective } from './custom-scrollbar.directive';
import { Component } from '@angular/core';
import { async, TestBed } from '@angular/core/testing';
import { GlobalsService } from '../../../services/globals.service';


@Component({
    template: `
        <div appCustomScrollbar></div>
    `
})
class TestHostComponent {

}


describe('CustomScrollbarDirective', () => {

    let globalsService: GlobalsService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                TestHostComponent,
                CustomScrollbarDirective
            ],
            providers: [
                GlobalsService
            ]
        }).compileComponents();

        globalsService = TestBed.get(GlobalsService);
    }));

    it('should call mCustomScrollbar with proper params on created element', () => {
        let mCustomScrollbarCalled = false;
        let mCustomScrollbarParams = null;
        spyOn(globalsService, 'globals').and.returnValue({
            $: () => ({
                mCustomScrollbar: (params: any) => {
                    mCustomScrollbarCalled = true;
                    mCustomScrollbarParams = params;
                }
            })
        });

        const fixture = TestBed.createComponent(TestHostComponent);
        fixture.detectChanges();
        expect(mCustomScrollbarCalled).toEqual(true);
        expect(mCustomScrollbarParams).toEqual({
            axis: 'y',
            scrollButtons: {enable: false},
            theme: 'dark-2',
        });
    });
});
