import { LogoutClickDirective } from './logout-click.directive';
import { Component } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';


@Component({
    template: `
        <button type="button" appLogoutClick></button>
    `
})
class TestHostComponent {
}

class AuthServiceStub {
    logout(): Promise<any> {
        return Promise.resolve();
    }
}

class RouterStub {
    navigateByUrl(url: any) {

    }
}

describe('LogoutClickDirective', () => {

    let fixture: ComponentFixture<TestHostComponent>;
    let component: TestHostComponent;
    let authService: AuthService;
    let routerService: Router;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                TestHostComponent,
                LogoutClickDirective
            ],
            providers: [
                {
                    provide: AuthService,
                    useClass: AuthServiceStub
                },
                {
                    provide: Router,
                    useClass: RouterStub
                }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(TestHostComponent);
        component = fixture.componentInstance;
        authService = TestBed.get(AuthService);
        routerService = TestBed.get(Router);
        fixture.detectChanges();
    }));

    it('by clicking attached element it should logout user and redirect to root', fakeAsync(() => {
        const spy = spyOn(authService, 'logout').and.callThrough();
        const navigateByUrlSpy = spyOn(routerService, 'navigateByUrl').and.callThrough();
        const button = fixture.debugElement.query(By.css('button'));

        button.triggerEventHandler('click', null);
        tick();
        fixture.detectChanges();

        expect(spy.calls.all().length).toEqual(1);
        expect(navigateByUrlSpy.calls.all().length).toEqual(1);
        expect(navigateByUrlSpy.calls.argsFor(0)[0]).toEqual('/');
    }));
});
