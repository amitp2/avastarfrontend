import { AddClassAfterTimeDirective } from './add-class-after-time.directive';
import { Component } from '@angular/core';
import { async, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

@Component({
    template: '<div appAddClassAfterTime="-visible"></div>'
})
class TestHostComponent {

}

describe('AddClassAfterTimeDirective', () => {

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                TestHostComponent,
                AddClassAfterTimeDirective
            ]
        }).compileComponents();
    }));

    it('should add given class after 200ms', fakeAsync(() => {
        let div;
        const fixture = TestBed.createComponent(TestHostComponent);
        fixture.detectChanges();

        tick(100);
        fixture.detectChanges();
        div = fixture.debugElement.query(By.css('.-visible'));
        expect(div && div.nativeElement).toBeFalsy();

        tick(100);
        fixture.detectChanges();
        div = fixture.debugElement.query(By.css('.-visible'));
        expect(div && div.nativeElement).toBeTruthy();
    }));


    // it('should create an instance', () => {
    //     const directive = new AddClassAfterTimeDirective();
    //     expect(directive).toBeTruthy();
    // });
});
