import { Directive, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { UserRole } from '../../../enums/user-role.enum';
import { CurrentUser } from '../../../interfaces/current-user';
import { AuthService } from '../../../services/auth.service';

@Directive({
    selector: '[appDisallowShow]'
})
export class DisallowShowDirective implements OnInit{

    @Input() appDisallowShow: UserRole[];

    constructor(private templateRef: TemplateRef<any>,
                private viewContainerRef: ViewContainerRef,
                private authService: AuthService) {
    }

    ngOnInit() {
        this.viewContainerRef.clear();
        this.authService.currentUser().then((user: CurrentUser) => {
            let has = true;
            user.roles.forEach((role: UserRole) => {
                if (this.appDisallowShow.some(r => r === role)) {
                    has = false;
                }
            });
            if (has) {
                this.viewContainerRef.createEmbeddedView(this.templateRef);
            } else {
                this.viewContainerRef.clear();
            }
        });
    }

}
