import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
    selector: '[appAddClassAfterTime]'
})
export class AddClassAfterTimeDirective implements OnInit {

    @Input() appAddClassAfterTime: string;

    constructor(private elementRef: ElementRef,
                private renderer: Renderer2) {
    }

    ngOnInit(): void {
        setTimeout(() => this.renderer.addClass(this.elementRef.nativeElement, this.appAddClassAfterTime), 200);
    }

}
