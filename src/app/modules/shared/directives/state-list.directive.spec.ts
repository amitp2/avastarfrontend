import { StateListDirective } from './state-list.directive';
import { Component, DebugElement } from '@angular/core';
import { HttpResourceQueryParams } from '../../../interfaces/http-resource-query-params';
import { ResourceQueryResponse } from '../../../interfaces/resource-query-response';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { StateResourceService } from '../../../services/resources/state-resource.service';
import { By } from '@angular/platform-browser';

@Component({
    template: `<select appStateList></select>`
})
class TestHostComponent {
}

class StateResourceServiceStub {
    query(params: HttpResourceQueryParams): Promise<ResourceQueryResponse<any>> {
        return Promise.resolve({
            total: 50, items: [
                {
                    id: 1,
                    name: 'country 1'
                },
                {
                    id: 2,
                    name: 'country 2'
                }
            ]
        });
    }
}

describe('StateListDirective', () => {
    let fixture: ComponentFixture<TestHostComponent>;
    let component: TestHostComponent;
    let stateResourceService: StateResourceService;
    let spy;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                TestHostComponent,
                StateListDirective
            ],
            providers: [
                {provide: StateResourceService, useClass: StateResourceServiceStub}
            ]
        }).compileComponents();
    }));

    beforeEach(fakeAsync(() => {
        stateResourceService = TestBed.get(StateResourceService);
        spy = spyOn(stateResourceService, 'query').and.callThrough();
        fixture = TestBed.createComponent(TestHostComponent);
        component = fixture.componentInstance;
        tick();
        fixture.detectChanges();
    }));


    it('should fetches all the countries and put it inside select as options', fakeAsync(() => {
        const elements = fixture.debugElement.queryAll(By.css('select option'));
        const options = elements.map((e: DebugElement) => ({
            id: e.nativeElement.getAttribute('value'),
            text: e.nativeElement.textContent.trim()
        }));
        expect(options).toEqual([
            {
                id: '1',
                text: 'country 1'
            },
            {
                id: '2',
                text: 'country 2'
            }
        ]);
    }));
});
