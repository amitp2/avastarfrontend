import { Directive, ElementRef, OnInit, Renderer2 } from '@angular/core';
import { GlobalsService } from '../../../services/globals.service';

@Directive({
    selector: '[appSelect2]'
})
export class Select2Directive implements OnInit {

    constructor(private elementRef: ElementRef,
                private globals: GlobalsService,
                private renderer: Renderer2) {
    }

    ngOnInit() {
        const $ = this.globals.globals().$;
        const el = this.elementRef.nativeElement;

        setTimeout(() => {
            $(el).select2();
            this.renderer.setValue(this.elementRef.nativeElement, '3');
            $(el).on('select2:select', () => {
                console.log($(el).select2('data'));

            });
        }, 500);
    }

}
