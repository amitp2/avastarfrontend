import { Directive, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { CountryResourceService } from '../../../services/resources/country-resource.service';
import { ResourceQueryResponse } from '../../../interfaces/resource-query-response';
import { CountryModel } from '../../../models/country-model';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import 'rxjs/add/operator/skip';
import { StateModel } from '../../../models/state-model';

@Directive({
    selector: '[appCountryList]',
    exportAs: 'countryList'
})
export class CountryListDirective implements OnInit {

    @Output() loaded: EventEmitter<any>;
    @Input() defaultCountry: number;
    @Input() pleaseSelect = true;

    private countryId: BehaviorSubject<number>;

    constructor(private countryResourceService: CountryResourceService,
                private renderer: Renderer2,
                private elementRef: ElementRef) {
        this.countryId = new BehaviorSubject<number>(null);
        this.loaded = new EventEmitter<any>();
    }

    ngOnInit() {
        this.countryResourceService.query({
            page: 0,
            size: 1000,
            sortFields: ['name'],
            sortAsc: true
        }).then((resp: ResourceQueryResponse<CountryModel>) => {
            if (this.pleaseSelect) {
                resp.items.unshift(new CountryModel({
                    id: '',
                    name: 'Please Select...',
                    code: ''
                }));
            }
            resp.items.forEach((c: CountryModel) => {
                const option = this.renderer.createElement('option');
                this.renderer.setAttribute(option, 'value', '' + c.id);

                if (this.defaultCountry === c.id) {
                    this.renderer.setAttribute(option, 'selected', '');
                }

                this.renderer.appendChild(option, this.renderer.createText(c.name));
                this.renderer.appendChild(this.elementRef.nativeElement, option);
            });
            this.loaded.emit(resp.items);
        });

    }

}
