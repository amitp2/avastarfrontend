import { AllowShowDirective } from './allow-show.directive';
import { Component } from '@angular/core';
import { async, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { AuthService } from '../../../services/auth.service';
import { CurrentUser } from '../../../interfaces/current-user';
import { UserRole } from '../../../enums/user-role.enum';
import { By } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';


@Component({
    template: `
        some template
        <div *appAllowShow="roles"></div>
    `
})
class TestHostComponent {
    roles = [UserRole.ADMIN];
}

class AuthServiceStub {

    currentUser(): Promise<CurrentUser> {
        return Promise.resolve(<CurrentUser> {
            id: 5,
            username: 'user',
            firstName: 'firstname',
            roles: [UserRole.SUPER_ADMIN]
        });
    }

}

describe('AllowShowDirective', () => {
    let authService: AuthService;

    beforeEach(async(() => {

        TestBed.configureTestingModule({
            declarations: [
                TestHostComponent,
                AllowShowDirective
            ],
            imports: [
                CommonModule
            ],
            providers: [
                {provide: AuthService, useClass: AuthServiceStub}
            ]
        }).compileComponents();

        authService = TestBed.get(AuthService);
    }));

    it('should remove element if current user has not necessary role', fakeAsync(() => {
        const fixture = TestBed.createComponent(TestHostComponent);
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        const div = fixture.debugElement.query(By.css('div'));
        expect(div && div.nativeElement).toBeFalsy();
    }));

    it('should add element into dom if current user has necessary role', fakeAsync(() => {
        spyOn(authService, 'currentUser').and.returnValue(Promise.resolve({
            id: 5,
            username: 'user',
            firstName: 'firstname',
            roles: [UserRole.ADMIN]
        }));
        const fixture = TestBed.createComponent(TestHostComponent);
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        const div = fixture.debugElement.query(By.css('div'));
        expect(div && div.nativeElement).toBeTruthy();
    }));
});
