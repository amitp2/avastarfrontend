import { CurrentUserDirective } from './current-user.directive';
import { Component, DebugElement } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { AuthService } from '../../../services/auth.service';
import { CurrentUser } from '../../../interfaces/current-user';
import { UserRole } from '../../../enums/user-role.enum';
import { By } from '@angular/platform-browser';

@Component({
    template: `
        <div appCurrentUser="username"></div>
    `
})
class TestHostComponent {
}

class AuthServiceStub {
    currentUser(): Promise<CurrentUser> {
        return Promise.resolve({
            id: 1,
            firstName: 'firstName',
            username: 'user name',
            roles: [UserRole.SUPER_ADMIN]
        });
    }
}


describe('CurrentUserDirective', () => {

    let authService: AuthService;
    let fixture: ComponentFixture<TestHostComponent>;
    let component: TestHostComponent;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                TestHostComponent,
                CurrentUserDirective
            ],
            providers: [
                {provide: AuthService, useClass: AuthServiceStub}
            ]
        }).compileComponents();
    }));

    beforeEach(fakeAsync(() => {
        fixture = TestBed.createComponent(TestHostComponent);
        component = fixture.componentInstance;
        authService = fixture.debugElement.injector.get(AuthService);
        tick();
        fixture.detectChanges();
    }));


    it('should display binded property of current user into attached element', () => {
        const div: DebugElement = fixture.debugElement.query(By.css('div'));
        expect(div.nativeElement.textContent.trim()).toEqual('user name');
    });
});
