import { Directive, Output, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { NgControl } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/distinctUntilChanged';

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[valueChanges]'
})
export class ValueChangesDirective implements OnDestroy, OnInit {
    @Output('valueChanges') valueChanges: EventEmitter<any> = new EventEmitter();
    private listener: Subscription;

    constructor(private ngControl: NgControl) {
    }

    ngOnInit() {
        this.listener = this.ngControl.valueChanges
            .distinctUntilChanged()
            .subscribe(value => this.valueChanges.emit(value));
    }

    ngOnDestroy() {
        this.listener.unsubscribe();
    }
}
