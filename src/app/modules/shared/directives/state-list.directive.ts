import { Directive, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { StateResourceService } from '../../../services/resources/state-resource.service';
import { ResourceQueryResponse } from '../../../interfaces/resource-query-response';
import { StateModel } from '../../../models/state-model';

@Directive({
    selector: '[appStateList]'
})
export class StateListDirective implements OnInit {

    private stateId: BehaviorSubject<number>;

    @Output() loaded: EventEmitter<any>;
    @Input() defaultState: number;
    @Input() pleaseSelect = true;

    constructor(private stateResourceService: StateResourceService,
                private renderer: Renderer2,
                private elementRef: ElementRef) {
        this.stateId = new BehaviorSubject<number>(null);
        this.loaded = new EventEmitter<any>();
    }

    ngOnInit() {
        this.stateResourceService.query({
            page: 0,
            size: 1000,
            sortFields: ['name'],
            sortAsc: true
        }).then((resp: ResourceQueryResponse<StateModel>) => {
            if (this.pleaseSelect) {
                resp.items.unshift(new StateModel({
                    id: '',
                    name: 'Please Select...',
                    code: ''
                }));
            }

            resp.items.forEach((c: StateModel) => {
                const option = this.renderer.createElement('option');
                this.renderer.setAttribute(option, 'value', '' + c.id);
                if (this.defaultState === c.id) {
                    this.renderer.setAttribute(option, 'selected', '');
                }
                this.renderer.appendChild(option, this.renderer.createText(c.name));
                this.renderer.appendChild(this.elementRef.nativeElement, option);
            });
            this.loaded.emit(resp.items);
        });

    }

}
