import { Directive, ElementRef, OnInit } from '@angular/core';
import { GlobalsService } from '../../../services/globals.service';

@Directive({
    selector: '[appCustomScrollbar]'
})
export class CustomScrollbarDirective implements OnInit {

    constructor(private elementRef: ElementRef, private globalsService: GlobalsService) {
    }

    ngOnInit() {
        this.globalsService.globals().$(this.elementRef.nativeElement).mCustomScrollbar({
            axis: 'y',
            scrollButtons: {enable: false},
            theme: 'dark-2',
        });
    }

}
