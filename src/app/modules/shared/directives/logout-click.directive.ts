import { Directive, HostListener } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';

@Directive({
    selector: '[appLogoutClick]'
})
export class LogoutClickDirective {

    @HostListener('click') clicked() {
        this.authService.logout().then(() => {
            this.router.navigateByUrl('/');
        });
    }

    constructor(private authService: AuthService,
                private router: Router) {
    }

}
