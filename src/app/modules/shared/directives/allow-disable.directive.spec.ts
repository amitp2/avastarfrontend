import { AllowDisableDirective } from './allow-disable.directive';
import { Component } from '@angular/core';
import { UserRole } from '../../../enums/user-role.enum';
import { async, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { AuthService } from '../../../services/auth.service';
import { CurrentUser } from '../../../interfaces/current-user';
import { By } from '@angular/platform-browser';

@Component({
    template: `
        <button [appAllowDisable]="[userRole.SUPER_ADMIN]"></button>
    `
})
class TestHostComponent {
    userRole = UserRole;
}

class AuthServiceStub {

    currentUser(): Promise<CurrentUser> {
        return Promise.resolve({
            id: 5,
            username: 'user',
            firstName: 'firstname',
            roles: [UserRole.ADMIN]
        });
    }

}

describe('AllowDisableDirective', () => {

    let authService: AuthService;

    beforeEach(async(() => {

        TestBed.configureTestingModule({
            declarations: [
                TestHostComponent,
                AllowDisableDirective
            ],
            providers: [
                {provide: AuthService, useClass: AuthServiceStub}
            ]
        }).compileComponents();

        authService = TestBed.get(AuthService);

    }));

    it('should add disabled attribute if current user has not necessary role', fakeAsync(() => {
        const fixture = TestBed.createComponent(TestHostComponent);
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        const div = fixture.debugElement.query(By.css('button'));
        expect(div.nativeElement.getAttribute('disabled')).toBeTruthy();
    }));

    it('should remove disabled attribute if current user has necessary role', fakeAsync(() => {
        spyOn(authService, 'currentUser').and.returnValue(Promise.resolve({
            id: 5,
            username: 'user',
            roles: [UserRole.SUPER_ADMIN]
        }));
        const fixture = TestBed.createComponent(TestHostComponent);
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        const div = fixture.debugElement.query(By.css('button'));
        expect(div.nativeElement.getAttribute('disabled')).toBeFalsy();
    }));
});
