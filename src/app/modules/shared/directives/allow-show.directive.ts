import { Directive, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { UserRole } from '../../../enums/user-role.enum';
import { CurrentUser } from '../../../interfaces/current-user';
import { AuthService } from '../../../services/auth.service';

@Directive({
    selector: '[appAllowShow]'
})
export class AllowShowDirective implements OnInit {

    @Input() appAllowShow: UserRole[];

    constructor(private templateRef: TemplateRef<any>,
                private viewContainerRef: ViewContainerRef,
                private authService: AuthService) {
    }

    ngOnInit() {
        this.viewContainerRef.clear();
        this.authService.currentUser().then((user: CurrentUser) => {
            let has = false;
            user.roles.forEach((role: UserRole) => {
                if (this.appAllowShow.filter(r => r === role).length > 0) {
                    has = true;
                }
            });
            if (has) {
                this.viewContainerRef.createEmbeddedView(this.templateRef);
            } else {
                this.viewContainerRef.clear();
            }
        });
    }

}
