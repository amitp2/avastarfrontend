import { CountryListDirective } from './country-list.directive';
import { Component, DebugElement } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { CountryResourceService } from '../../../services/resources/country-resource.service';
import { HttpResourceQueryParams } from '../../../interfaces/http-resource-query-params';
import { ResourceQueryResponse } from '../../../interfaces/resource-query-response';
import { By } from '@angular/platform-browser';

@Component({
    template: `<select appCountryList></select>`
})
class TestHostComponent {

}

class CountryResourceServiceStub {
    query(params: HttpResourceQueryParams): Promise<ResourceQueryResponse<any>> {
        return Promise.resolve({
            total: 50, items: [
                {
                    id: 1,
                    name: 'country 1'
                },
                {
                    id: 2,
                    name: 'country 2'
                }
            ]
        });
    }
}

describe('CountryListDirective', () => {

    let fixture: ComponentFixture<TestHostComponent>;
    let component: TestHostComponent;
    let countryResourceService: CountryResourceService;
    let spy;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                TestHostComponent,
                CountryListDirective
            ],
            providers: [
                {provide: CountryResourceService, useClass: CountryResourceServiceStub}
            ]
        }).compileComponents();
    }));

    beforeEach(fakeAsync(() => {
        countryResourceService = TestBed.get(CountryResourceService);
        spy = spyOn(countryResourceService, 'query').and.callThrough();
        fixture = TestBed.createComponent(TestHostComponent);
        component = fixture.componentInstance;
        tick();
        fixture.detectChanges();
    }));


    it('should fetches all the countries and put it inside select as options', fakeAsync(() => {
        const elements = fixture.debugElement.queryAll(By.css('select option'));
        const options = elements.map((e: DebugElement) => ({
            id: e.nativeElement.getAttribute('value'),
            text: e.nativeElement.textContent.trim()
        }));
        expect(options).toEqual([
            {
                id: '1',
                text: 'country 1'
            },
            {
                id: '2',
                text: 'country 2'
            }
        ]);
    }));

});
