import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { CurrentUser } from '../../../interfaces/current-user';

@Directive({
    selector: '[appCurrentUser]'
})
export class CurrentUserDirective implements OnInit {

    @Input('appCurrentUser') userProperty;

    constructor(private authService: AuthService,
                private elementRef: ElementRef) {
    }

    ngOnInit() {
        this.authService.currentUser().then((user: CurrentUser) => {
            this.elementRef.nativeElement.innerHTML = user[this.userProperty];
        });
    }

}
