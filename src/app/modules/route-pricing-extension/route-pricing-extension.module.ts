import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PricingExtensionComponent } from './components/pricing-extension/pricing-extension.component';
import { SharedModule } from '../shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { AllowOnlyService } from '../../services/guards/allow-only.service';
import { UserRole } from '../../enums/user-role.enum';
import { ReactiveFormsModule } from '@angular/forms';
import { PricingExtensionService } from '../../services/pricing-extension.service';

const routes: Routes = [
    {
        path: '',
        component: PricingExtensionComponent,
        canActivate: [AllowOnlyService],
        data: {
            roles: [UserRole.ADMIN]
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    declarations: [PricingExtensionComponent],
    providers: [
        PricingExtensionService
    ]
})
export class RoutePricingExtensionModule { }
