import { Component, OnInit } from '@angular/core';
import { CurrentPropertyService } from '../../../../services/current-property.service';
import { PricingExtensionService } from '../../../../services/pricing-extension.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { VenuePricingExtension } from '../../../../enums/venue-pricing-extension.enum';

@Component({
    selector: 'app-pricing-extension',
    templateUrl: './pricing-extension.component.html',
    styleUrls: ['./pricing-extension.component.styl']
})
export class PricingExtensionComponent implements OnInit {
    public form: FormGroup;
    public isLoading = true;
    public applyTo = VenuePricingExtension;

    constructor(private currentVenue: CurrentPropertyService,
                private pricingExtension: PricingExtensionService,
                private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            extensionDate: ['', [Validators.required]],
            currentDate: [''],
            applyTo: [VenuePricingExtension.Both, [Validators.required]]
        });

        this.load();
    }

    @AsyncMethod({
        taskName: 'load_pricing_extension'
    })
    load() {
        const disableCurrent = () => this.form.get('currentDate').disable();

        return this.currentVenue.currentVenueAsync()
            .then((venue) => {
                return this.pricingExtension.getByVenueId(venue.id).then((model: any) => {
                    this.form.get('extensionDate').setValue('');
                    if (model) {
                        this.form.patchValue({
                            currentDate: model.extensionDate
                        });
                    }
                    this.form.markAsPristine();
                    this.isLoading = false;
                }).catch(() => {
                    this.isLoading = false;
                });
            })
            //.then(disableCurrent, disableCurrent)
            .catch((e) => {
                this.isLoading = false;
            });
    }

    @AsyncMethod({
        taskName: 'update_pricing_extension',
        success: AppSuccess.EDIT_PRICING_EXTENSION,
        error: AppError.EDIT_PRICING_EXTENSION
    })
    update() {
        this.isLoading = false;

        return this.currentVenue.currentVenueAsync()
            .then((venue) => {
                const model = {
                    extensionDate: this.form.value.extensionDate,
                    applyTo: this.form.value.applyTo
                };
                return this.pricingExtension.updateByVenueId(model, venue.id).then(() => {
                    return this.load();
                });
            })
            .catch(() => {
                this.isLoading = false;
            });
    }

    cancel() {
        this.form.reset({
            currentDate: this.form.get('currentDate').value
        });
    }

}
