import { Component, OnInit, ViewChild } from '@angular/core';
import { VenueEventResourceService } from '../../../../services/resources/venue-event-resource.service';
import { PromisedStoreService } from '../../../../services/promised-store.service';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { PagingTableService } from '../../../../services/paging-table.service';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { UtilsService } from '../../../../services/utils.service';
import { UserRole } from '../../../../enums/user-role.enum';
import { SubscriberModel } from '../../../../models/subscriber-model';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';

import { FormGroup, FormBuilder } from '@angular/forms';
import { CurrentPropertyService } from '../../../../services/current-property.service';
import { VenueModel } from '../../../../models/venue-model';

@Component({
    selector: 'app-event-list',
    templateUrl: './event-list.component.html',
    styleUrls: ['./event-list.component.styl'],
    providers: [PagingTableService]
})
export class EventListComponent implements OnInit {

    filterForm: FormGroup;
    UserRole = UserRole;


    @ViewChild('table')
    table: PagingTableComponent;

    columns: PagingTableHeaderColumn[] = [
        { isSortable: true, title: 'Event Name', key: 'name' },
        { isSortable: false, title: 'Company', key: '' },
        // {isSortable: false, title: 'Primary Contact', key: ''},
        // {isSortable: true, title: 'Master Account #', key: 'masterAccountNumber'},
        { isSortable: true, title: 'Start Date', key: 'eventStartDate' },
        { isSortable: true, title: 'End Date', key: 'eventEndDate' },
        { isSortable: false, title: 'Status', key: 'status' },
        { isSortable: false, title: 'Actions', key: '' }
    ];

    constructor(private venueEventResource: VenueEventResourceService,
        private promisedStore: PromisedStoreService,
        private utils: UtilsService,
        private fb: FormBuilder,
        private currentVenue: CurrentPropertyService,
    ) {
        this.buildFilterForm();
    }

    private buildFilterForm() {
        this.filterForm = this.fb.group({
            start: '',
        });
    }



    // loadFunction(): any {
    //     return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
    //         console.log("params");
    //         console.log(params);
    //         return this.promisedStore.currentVenueId().then((venueId: number) => {
    //             const httpParams = this.utils.tableToHttpParams(params);
    //             // httpParams.otherParams = { ...this.filterForm.value };
    //             // if (!httpParams.otherParams.start) {
    //             //     delete httpParams.otherParams.start;
    //             // } else {
    //             //     httpParams.otherParams.start = httpParams.otherParams.start.split('-').join('/');
    //             // }
    //             // debugger
    //             return this.venueEventResource.queryByVenueId(httpParams, venueId);
    //         });
    //     }).bind(this);
    // }




    loadFunction() {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            console.log("Params ", params);
            return this.currentVenue.currentVenueAsync()
                .then((venue: VenueModel) => {
                    const queryParams = this.utils.tableToHttpParams(params);

                    queryParams.otherParams = { ...this.filterForm.value };
                    
                    if (!queryParams.otherParams.start) {
                        delete queryParams.otherParams.start;
                    } else {
                        queryParams.otherParams.start = queryParams.otherParams.start.split('-').join('/');
                    }
                   
                    return this.venueEventResource.queryByVenueId(queryParams, venue.id);
                });
        }).bind(this);
    }

    ngOnInit() {
    }

    delete(item: SubscriberModel) {
        this.venueEventResource
            .delete(item.id)
            .then(() => this.table.refresh());
    }


    applyFilters() {
        this.table.refresh();
    }

    clearFilters() {
        this.filterForm.reset({
            // eventId: '',
            // vendorId: '',
            // status: ''
        });
        this.table.refresh();
    }
}
