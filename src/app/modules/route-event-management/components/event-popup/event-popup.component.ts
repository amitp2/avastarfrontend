import { Component, OnInit } from '@angular/core';
import { FormMode } from '../../../../enums/form-mode.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { backToUrlExists, getBackToUrl } from '../../../../helpers/helper-functions';

@Component({
  selector: 'app-event-popup',
  templateUrl: './event-popup.component.html',
  styleUrls: ['./event-popup.component.styl']
})
export class EventPopupComponent implements OnInit {
    id: string;
    mode: FormMode;
    formMode = FormMode;
    backTo: any;
    accountId: number;

    constructor(private activatedRoute: ActivatedRoute, public router: Router) {
        this.accountId = this.activatedRoute.snapshot.queryParams.accountId;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: any) => {
            this.id = params.id;
            if (this.id === 'add') {
                this.mode = FormMode.Add;
                return;
            }
            this.mode = FormMode.Edit;
        });
    }

    goBack() {

        if (backToUrlExists(this.activatedRoute)) {
            this.router.navigateByUrl(getBackToUrl(this.activatedRoute));
            return;
        }

        if (this.accountId) {
            return this.router.navigateByUrl(`/u/accounts/${this.accountId}/account-events`);
        }
        return this.router.navigateByUrl('/u/event-management');
    }

    linkPrefix(): string {
        return '/u/event-management/' + this.id + '/';
    }
}
