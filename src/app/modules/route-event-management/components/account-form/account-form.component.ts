import { Component, OnInit } from '@angular/core';
import { UploadType } from '../../../../enums/upload-type.enum';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { StateModel } from '../../../../models/state-model';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { Store } from '@ngrx/store';
import { UtilsService } from '../../../../services/utils.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AppError } from '../../../../enums/app-error.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountLogoUploaderService } from '../../../route-accounts/services/account-logo-uploader.service';
import { VenueClientResourceService } from '../../../../services/resources/venue-client-resource.service';
import { VenueClientModel } from '../../../../models/venue-client-model';
import { FunctionServiceChargeCalculationType } from '../../../../models/function-model';
import { GeolocationDataModel } from '../../../../models/geolocation-data.model';
import { SubscriberStatus } from '../../../../enums/subscriber-status.enum';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { countryByCode, userLocation } from '../../../../store/selectors';
import { VenueClientType } from '../../../../enums/venue-client-type.enum';
import { VenueClientStatus } from '../../../../enums/venue-client-status.enum';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { FormMode } from '../../../../enums/form-mode.enum';
import { Subscription } from 'rxjs/Subscription';
import { PromisedStoreService } from '../../../../services/promised-store.service';
import { AppState } from '../../../../store/store';
import { CountryModel } from '../../../../models/country-model';
import { EventPopupComponent } from '../event-popup/event-popup.component';
import { VenueEventResourceService } from '../../../../services/resources/venue-event-resource.service';
import { VenueClientContactModel } from '../../../../models/venue-client-contact-model';
import { VenueClientContactResourceService } from '../../../../services/resources/venue-client-contact-resource.service';
import { percentFormControl } from '../../../../helpers/helper-functions';

@Component({
  selector: 'app-account-form',
  templateUrl: './account-form.component.html',
  styleUrls: ['./account-form.component.styl']
})
export class AccountFormComponent implements OnInit {
    id: any;
    form: FormGroup;
    accountType = VenueClientType;
    status = VenueClientStatus;
    uploader: any;
    modeObs: BehaviorSubject<FormMode>;
    initialisedObs: BehaviorSubject<boolean>;
    initialisedItems: number;
    countries: CountryModel[];
    states: StateModel[];
    functionServiceChargeCalculationType = FunctionServiceChargeCalculationType;
    submitting = false;

    SelectFetchType = SelectFetchType;
    private primaryContact: VenueClientContactModel;

    get backUrl() {
        if (this.eventPopup.accountId) {
            return `/u/accounts/${this.eventPopup.accountId}/account-events`;
        }
        return '/u/event-management';
    }

    constructor(
        private venueClientResource: VenueClientResourceService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private logoUploader: AccountLogoUploaderService,
        private utilsService: UtilsService,
        private store: Store<AppState>,
        private promisedStore: PromisedStoreService,
        private eventPopup: EventPopupComponent,
        private eventService: VenueEventResourceService,
        private clientContactService: VenueClientContactResourceService
    ) {
        this.uploader = logoUploader;
        this.initialisedItems = 0;
        this.initialisedObs = new BehaviorSubject<boolean>(false);

        this.form = new FormGroup({
            name: new FormControl(null, [Validators.required]),
            accountType: new FormControl(VenueClientType.Client),
            status: new FormControl(true),
            address: new FormControl(null, [Validators.required]),
            address2: new FormControl(null),
            city: new FormControl(null, [Validators.required]),
            stateId: new FormControl(),
            postCode: new FormControl(null, [Validators.required, Validators.maxLength(10)]),
            countryId: new FormControl(null, [Validators.required]),
            branch: new FormControl(),
            agreement: new FormControl(false),
            logoUrl: new FormControl(),
            standardDiscountRate: percentFormControl(),
            discountRateForSetupDays: percentFormControl(),
            alternativeServiceCharge: new FormControl(),
            alternativeServiceChargePercentage: percentFormControl(),
            taxExempt: new FormControl(),
            serviceChargeCalculation: new FormControl(),
            primaryContact: new FormGroup({
                firstName: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
                lastName: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
                title: new FormControl(null, [Validators.maxLength(100)]),
                email: new FormControl(null, [Validators.required, Validators.email, Validators.maxLength(100)]),
                mobile: new FormControl(null, [Validators.maxLength(30)]),
                phone: new FormControl(null, [Validators.required]),
                notes: new FormControl(null, [Validators.maxLength(50)]),
                fax: new FormControl(null, [Validators.maxLength(30)])
            })
        });

        this.form.get('stateId').disable();

        this.form.get('countryId').valueChanges.subscribe((val: number) => {
            if (+val === 233) {
                this.form.get('stateId').enable();
            } else {
                this.form.get('stateId').disable();
            }
        });

        this.form.get('alternativeServiceCharge')
            .valueChanges
            .subscribe(val => {
                if (!val) {
                    this.form
                        .get('alternativeServiceChargePercentage')
                        .setValue('');
                }
            });
    }

    ngOnInit() {
        this.load();
    }

    countriesAreLoaded(countries: CountryModel[]) {
        this.initialisedItems++;
        this.countries = countries;
        if (this.initialisedItems === 2) {
            this.initialisedObs.next(true);
        }
    }

    statesAreLoaded(states: StateModel[]) {
        this.initialisedItems++;
        this.states = states;
        if (this.initialisedItems === 2) {
            this.initialisedObs.next(true);
        }
    }

    async submit() {
        if (this.submitting) {
            return;
        }
        this.submitting = true;

        const newModel = this.deserialize(this.form.value);

        await this.update(+this.id, newModel);

        this.submitting = false;
        this.form.markAsPristine();
    }

    deserialize(formValue: any): VenueClientModel {
        const modelVal: any = {
            ...formValue
        };
        modelVal.status = formValue.status ? SubscriberStatus.Active : SubscriberStatus.Locked;
        modelVal.masterAgreementAgreed = formValue.agreement;

        const model = new VenueClientModel(modelVal);

        model['primaryContact'] = formValue.primaryContact;

        return model;
    }

    serialize(model: VenueClientModel): any {
        const serialized = {
            name: model.name,
            accountType: model.accountType,
            status: model.status === VenueClientStatus.Active,
            address: model.address,
            address2: model.address2,
            city: model.city,
            stateId: model.stateId,
            postCode: model.postCode,
            countryId: model.countryId,
            alternativeServiceChargePercentage: model.alternativeServiceChargePercentage,
            branch: model.branch,
            phone: model.phone,
            fax: model.fax,
            website: model.website,
            agreement: model.masterAgreementAgreed,
            logoUrl: model.logoUrl,
            serviceChargeCalculation: model.serviceChargeCalculation
        };
        serialized['standardDiscountRate'] = model.standardDiscountRate;
        serialized['discountRateForSetupDays'] = model.discountRateForSetupDays;
        serialized['alternativeServiceCharge'] = model.alternativeServiceCharge;
        serialized['taxExempt'] = model.taxExempt;
        return serialized;
    }

    @AsyncMethod({
        taskName: 'account_loaded'
    })
    async load() {
        const event = await this.eventService.get(+this.eventPopup.id);
        const account = await this.venueClientResource.get(event.accountId);
        const modelValue = this.serialize(account);

        try {
            const primaryContact = await this.clientContactService.queryByClientId({
                page: 0,
                size: 100,
                sortFields: ['id'],
                sortAsc: true
            }, account.id);
            modelValue.primaryContact = {
                ...primaryContact.items[0]
            };
            this.primaryContact = primaryContact.items[0];
        } catch (ex)  {

        }

        this.id = account.id;
        this.uploader.uploadType = UploadType.Update;
        this.uploader.oldUrl = account.logoUrl;
        this.form.patchValue(modelValue);

        await this.utilsService.resolveAfter(300);

        this.form.markAsPristine();
    }

    @AsyncMethod({
        taskName: 'account_update',
        success: AppSuccess.ACCOUNT_UPDATE,
        error: AppError.ACCOUNT_UPDATE
    })
    update(id: number, model: VenueClientModel) {
        const primaryContact = model['primaryContact'];

        delete model['primaryContact'];

        return this.promisedStore.currentVenueId()
            .then((venueId: number) => {
                model.venueId = venueId;
                return this.venueClientResource.edit(id, model);
            }).then(account => {
                const contactModel = new VenueClientContactModel({
                    ...primaryContact
                });
                return this.clientContactService.edit(this.primaryContact.id, contactModel).then(x => account);
            });
    }
}
