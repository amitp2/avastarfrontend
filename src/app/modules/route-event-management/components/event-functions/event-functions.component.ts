import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { UtilsService } from '../../../../services/utils.service';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { FunctionResourceService } from '../../../../services/resources/function-resource.service';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableService } from '../../../../services/paging-table.service';
import { EquipmentPagingCurrent } from '../../../../store/equipment-paging/equipment-paging.actions';
import { EquipmentModel } from '../../../../models/equipment-model';
import { FunctionModel } from '../../../../models/function-model';
import { FunctionPagingCurrent } from '../../../../store/function-paging/function-paging.actions';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';

@Component({
    selector: 'app-event-functions',
    templateUrl: './event-functions.component.html',
    styleUrls: ['./event-functions.component.styl'],
    providers: [
        PagingTableService
    ]
})
export class EventFunctionsComponent implements OnInit {
    @Input()
    eventId: number;

    @ViewChild('table')
    table: PagingTableComponent;

    private latestParams: any;

    columns: PagingTableHeaderColumn[] = [
        { isSortable: true, title: 'Location', key: 'functionSpace.name' },
        // { isSortable: true, title: 'TEO#', key: 'functionName' },
        { isSortable: true, title: 'TEO#', key: 'teoNumber' },
        { isSortable: true, title: 'type', key: 'type' },
        { isSortable: true, title: 'Start Date', key: 'startDate' },
        { isSortable: true, title: 'End Date', key: 'endDate' },
        { isSortable: false, title: 'Actions', key: 'actions' }
    ];

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            this.latestParams = this.utils.tableToHttpParams(params);
            return this.functions.queryByEventId(
                this.latestParams,
                this.eventId
            ).then((resp) => {
                return resp;
            });
        }).bind(this);
    }

    constructor(
        private functions: FunctionResourceService,
        private utils: UtilsService,
        private store: Store<AppState>
    ) {
    }

    ngOnInit() {
        // console.log(this.eventId, 'eventt');
    }

    delete(item: any) {
        this.functions.delete(item.id)
            .then(() => this.table.refresh());
    }

    edit($event, item: FunctionModel, index: number) {
        const current = {
            page: (this.latestParams.page * this.latestParams.size + index) as number,
            id: item.id
        };

        this.store.dispatch(new FunctionPagingCurrent(current));
    }
}
