import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { UtilsService } from '../../../../services/utils.service';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { AppError } from '../../../../enums/app-error.enum';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { EventContactResourceService } from '../../../../services/resources/event-contact-resource.service';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { EventPopupComponent } from '../event-popup/event-popup.component';
import { PagingTableService } from '../../../../services/paging-table.service';

@Component({
    selector: 'app-event-contacts',
    templateUrl: './event-contacts.component.html',
    styleUrls: ['./event-contacts.component.styl'],
    providers: [
        PagingTableService
    ]
})
export class EventContactsComponent {
    @ViewChild('table') table: PagingTableComponent;

    columns: PagingTableHeaderColumn[] = [
        { isSortable: true, title: 'Name', key: 'firstName' },
        { isSortable: true, title: 'Direct Phone', key: 'phone' },
        { isSortable: true, title: 'Email', key: 'email' },
        { isSortable: true, title: 'Department', key: 'department' },
        { isSortable: false, title: 'Actions', key: 'actions' }
    ];

    get addUrl() {
        return `/u/event-management/${this.eventPopup.id}/contacts/add`;
    }

    constructor(
        private contactApi: EventContactResourceService,
        private utils: UtilsService,
        private eventPopup: EventPopupComponent
    ) {
    }

    load = (params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
        return this.contactApi.queryByEventId(this.utils.tableToHttpParams(params), +this.eventPopup.id);
    };

    @AsyncMethod({
        taskName: 'delete_event_contact',
        error: AppError.EVENT_CONTACT_DELETE,
        success: AppSuccess.EVENT_CONTACT_DELETE
    })
    delete(item: any) {
        return this.contactApi.delete(item.id)
            .then(() => this.table.refresh());
    }

    editUrl(item) {
        return `/u/event-management/${this.eventPopup.id}/contacts/${item.id}`;
    }
}
