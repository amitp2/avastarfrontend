import { Component, OnDestroy, OnInit } from '@angular/core';
import { EventType } from '../../../../enums/event-type.enum';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { VenueSettingsResourceService } from '../../../../services/resources/venue-settings-resource.service';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { ObservableSubscription } from '../../../../decorators/observable-subscription.decorator';
import { UtilsService } from '../../../../services/utils.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AppError } from '../../../../enums/app-error.enum';
import { VenueEventModel, VenueEventStatus, VenueEventType } from '../../../../models/venue-event-model';
import { ActivatedRoute, Router } from '@angular/router';
import { VenueClientResourceService } from '../../../../services/resources/venue-client-resource.service';
import { UnsubscribePoint } from '../../../../decorators/unsubscribe-point.decorator';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { Subscription } from 'rxjs/Subscription';
import { PromisedStoreService } from '../../../../services/promised-store.service';
import { FormHandler } from '../../../../classes/form-handler';
import { VenueEventResourceService } from '../../../../services/resources/venue-event-resource.service';
import { EventPopupComponent } from '../event-popup/event-popup.component';
import { backToUrlExists, getBackToUrl } from '../../../../helpers/helper-functions';
import { addQueryParamToUrl } from '../../../../helpers/functions/add-query-param-to-url';

@Component({
    selector: 'app-event-form',
    templateUrl: './event-form.component.html',
    styleUrls: ['./event-form.component.styl']
})
export class EventFormComponent extends FormHandler<VenueEventModel> implements OnInit, OnDestroy {
    type = VenueEventType;
    status = VenueEventStatus;
    formModeType = FormMode;
    accountId: number;
    ref: string;
    @ObservableSubscription
    modeSubs: Subscription;

    get backUrl(): string {

        if (backToUrlExists(this.activatedRoute)) {
            return getBackToUrl(this.activatedRoute);
        }

        if (this.ref === 'event' || !this.accountId) {
            return '/u/event-management';
        }
        return `/u/accounts/${this.accountId}/account-events`;
    }

    get eventId(): string {
        return this.activatedRoute.snapshot.params.id;
    }

    constructor(
        private activatedRoute: ActivatedRoute,
        private events: VenueEventResourceService,
        private utils: UtilsService,
        private promisedStore: PromisedStoreService,
        private router: Router,
        private account: VenueClientResourceService,
        private venueSettings: VenueSettingsResourceService,
        private eventPopup: EventPopupComponent
    ) {
        super();
        this.ref = this.activatedRoute.snapshot.queryParamMap.get('ref');
        this.accountId = +this.activatedRoute.snapshot.queryParamMap.get('accountId');
    }

    ngOnInit() {
        super.ngOnInit();

        this.modeSubs = this.formMode().subscribe((mode: FormMode) => {
            if (mode === FormMode.Add) {
                this.account.get(this.accountId)
                    .then((account) => account.venueId)
                    .then((venueId) => this.venueSettings.getForVenue(venueId))
                    .then((venue) => {
                        let type;
                        switch (venue.eventType) {
                            case 1:
                                type = EventType.Group;
                                break;
                            case 2:
                                type = EventType.Local;
                                break;
                            case 3:
                                type = EventType.Internal;
                                break;
                        }
                        this.form.patchValue({
                            type
                        });
                    });
            }
        });
    }

    @UnsubscribePoint
    ngOnDestroy() {
        super.ngOnDestroy();
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute.parent, 'id');
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute.parent, 'id');
    }

    initializeForm(): FormGroup {
        return new FormGroup({
            name: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            code: new FormControl(),
            type: new FormControl(VenueEventType.Local),
            status: new FormControl(VenueEventStatus.Definite),
            masterAccountNumber: new FormControl(),
            eventDescription: new FormControl()
        });
    }

    @AsyncMethod({
        taskName: 'get_account_event'
    })
    get(id: number): Promise<VenueEventModel> {
        return this.events.get(id).then(model => {
            this.accountId = model.accountId;
            return model;
        });
    }

    @AsyncMethod({
        taskName: 'add_event',
        success: AppSuccess.ADD_EVENT,
        error: AppError.ADD_EVENT
    })
    add(model: VenueEventModel): Promise<any> {
        return this.promisedStore.currentVenueId()
            .then((venueId: number) => {
                model.venueId = venueId;
                model.accountId = this.accountId;
                return this.events.add(model);
            })
            .then((model) => {

                if (backToUrlExists(this.activatedRoute)) {
                    const url = addQueryParamToUrl(getBackToUrl(this.activatedRoute), 'addedEvent', `${model.id}`);
                    this.router.navigateByUrl(url);
                    return;
                }

                this.router.navigateByUrl(
                    `/u/event-management/${model.id}/info`,
                    { queryParams: { accountId: model.accountId } }
                );
            });
    }

    @AsyncMethod({
        taskName: 'edit_event',
        success: AppSuccess.EDIT_EVENT,
        error: AppError.EDIT_EVENT
    })
    edit(id: number, model: VenueEventModel): Promise<any> {
        return this.promisedStore.currentVenueId()
            .then((venueId: number) => {
                model.venueId = venueId;
                model.accountId = this.accountId;
                return this.events.edit(id, model);
            })
            .then(() => this.router.navigateByUrl(this.backUrl));
    }

    serialize(model: VenueEventModel): any {
        return this.utils.transformInto(model, {
            venueId: { to: 'venueId' },
            name: { to: 'name' },
            code: { to: 'code' },
            masterAccountNumber: { to: 'masterAccountNumber' },
            eventDescription: { to: 'eventDescription' },
            type: { to: 'type' },
            status: { to: 'status' }
        });
    }

    deserialize(formValue: any): VenueEventModel {
        return new VenueEventModel(this.utils.transformInto(formValue, {
            venueId: { to: 'venueId' },
            name: { to: 'name' },
            code: { to: 'code' },
            masterAccountNumber: { to: 'masterAccountNumber' },
            eventDescription: { to: 'eventDescription' },
            type: { to: 'type' },
            status: { to: 'status' }
        }));
    }

    cancel() {
        this.router.navigateByUrl(this.backUrl);
    }
}
