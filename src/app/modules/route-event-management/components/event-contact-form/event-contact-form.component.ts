import { Component } from '@angular/core';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { EventPopupComponent } from '../event-popup/event-popup.component';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppError } from '../../../../enums/app-error.enum';
import { VenueEventContact } from '../../../../models/venue-event-model';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { FormHandler } from '../../../../classes/form-handler';
import { EventContactResourceService } from '../../../../services/resources/event-contact-resource.service';

@Component({
    selector: 'app-event-contact-form',
    templateUrl: './event-contact-form.component.html',
    styleUrls: ['./event-contact-form.component.styl']
})
export class EventContactFormComponent extends FormHandler<VenueEventContact> {
    accountId: number;

    get backUrl(): string {
        return `/u/event-management/${this.eventPopup.id}/contacts`;
    }

    get eventId(): string {
        return this.activatedRoute.snapshot.params.id;
    }

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private contactApi: EventContactResourceService,
        private eventPopup: EventPopupComponent
    ) {
        super(fb);
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute, 'contactId');
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute, 'contactId');
    }

    initializeForm(): FormGroup {
        return this.formBuilder.group({
            firstName: [null, [Validators.required, Validators.maxLength(50)]],
            lastName: [null, [Validators.required, Validators.maxLength(50)]],
            title: [null, [Validators.maxLength(100)]],
            email: [null, [Validators.required, Validators.email, Validators.maxLength(100)]],
            mobile: [null, [Validators.maxLength(100)]],
            phone: [null, [Validators.required, Validators.maxLength(30)]],
            notes: [null, [Validators.maxLength(400)]],
            fax: [null, [Validators.maxLength(30)]],
            department: [null, [Validators.maxLength(250)]]
        });
    }

    @AsyncMethod({
        taskName: 'get_event_contact'
    })
    get(id: number): Promise<VenueEventContact> {
        return this.contactApi.get(id);
    }

    @AsyncMethod({
        taskName: 'add_event',
        success: AppSuccess.EVENT_CONTACT_ADD,
        error: AppError.EVENT_CONTACT_ADD
    })
    add(model: VenueEventContact): Promise<any> {
        return this.contactApi.addByEventId(model, +this.eventPopup.id)
            .then(() => this.router.navigateByUrl(this.backUrl));
    }

    @AsyncMethod({
        taskName: 'edit_event',
        success: AppSuccess.EVENT_CONTACT_EDIT,
        error: AppError.EVENT_CONTACT_EDIT
    })
    edit(id: number, model: VenueEventContact): Promise<any> {
        return this.contactApi.edit(id, model)
            .then(() => this.router.navigateByUrl(this.backUrl));
    }

    serialize(model: VenueEventContact): any {
        return model.toJson();
    }

    deserialize(formValue: any): VenueEventContact {
        return VenueEventContact.fromJson(formValue);
    }

    cancel() {
        if (this.form.dirty && !confirm('Your changes will be lost. Do yo want to continue?')) {
            return;
        }
        this.router.navigateByUrl(this.backUrl);
    }
}
