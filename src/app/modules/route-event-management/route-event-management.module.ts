import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventListComponent } from './components/event-list/event-list.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { EventFormComponent } from './components/event-form/event-form.component';
import { EventPopupComponent } from './components/event-popup/event-popup.component';
import { EventContactsComponent } from './components/event-contacts/event-contacts.component';
import { EventContactFormComponent } from './components/event-contact-form/event-contact-form.component';
import { EventFunctionsComponent } from './components/event-functions/event-functions.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaskedInputModule } from '../masked-input/masked-input.module';
import { AccountFormComponent } from './components/account-form/account-form.component';
import { SelectModule } from '../select/select.module';
import { AccountLogoUploaderService } from '../route-accounts/services/account-logo-uploader.service';
import { TableResizeModule } from '../table-resize/table-resize.module';

const routes: Routes = [
    {
        path: '',
        component: EventListComponent,
        children: [
            {
                path: ':id',
                component: EventPopupComponent,
                children: [
                    { path: 'info', component: EventFormComponent },
                    { path: 'account-info', component: AccountFormComponent },
                    { path: 'contacts', component: EventContactsComponent, pathMatch: 'full' },
                    { path: 'contacts/:contactId', component: EventContactFormComponent }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        PagingTableModule,
        ReactiveFormsModule,
        MaskedInputModule,
        SelectModule,
        TableResizeModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        EventListComponent,
        EventFormComponent,
        EventPopupComponent,
        EventContactsComponent,
        EventContactFormComponent,
        EventFunctionsComponent,
        AccountFormComponent
    ],
    providers: [
        AccountLogoUploaderService
    ]
})
export class RouteEventManagementModule {
}
