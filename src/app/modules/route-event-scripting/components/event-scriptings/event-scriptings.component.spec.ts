import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventScriptingsComponent } from './event-scriptings.component';

describe('EventScriptingsComponent', () => {
  let component: EventScriptingsComponent;
  let fixture: ComponentFixture<EventScriptingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventScriptingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventScriptingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
