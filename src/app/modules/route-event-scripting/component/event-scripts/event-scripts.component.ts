import { Component, OnInit, ViewChild } from '@angular/core';
import { ZingtreeService } from '../../services/zingtree.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { DecisionTreeResourceService } from '../../../../services/resources/decision-tree-resource.service';
import { UtilsService } from '../../../../services/utils.service';
import { AuthService } from '../../../../services/auth.service';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';

@Component({
    selector: 'app-event-scripts',
    templateUrl: './event-scripts.component.html',
    styleUrls: ['./event-scripts.component.styl']
})
export class EventScriptsComponent implements OnInit {
    @ViewChild('table') table: PagingTableComponent;

    constructor(
        private zingTreeApi: ZingtreeService,
        private treeApi: DecisionTreeResourceService,
        private utils: UtilsService,
        private authApi: AuthService
    ) {
    }

    ngOnInit() {
    }

    loadFunction() {
        return async (params) => {
            const httpParams = this.utils.tableToHttpParams(params);
            const user = await this.authApi.currentUser();
            return await this.treeApi.queryBySubscriberId(httpParams, user.subscriberId);
        };
    }
}
