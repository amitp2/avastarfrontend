import { TestBed, inject } from '@angular/core/testing';

import { ZingtreeService } from './zingtree.service';

describe('ZingtreeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ZingtreeService]
    });
  });

  it('should be created', inject([ZingtreeService], (service: ZingtreeService) => {
    expect(service).toBeTruthy();
  }));
});
