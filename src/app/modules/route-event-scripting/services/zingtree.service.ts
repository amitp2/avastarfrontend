import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilsService } from '../../../services/utils.service';
import { AuthService } from '../../../services/auth.service';

declare var show_popup: any;

@Injectable()
export class ZingtreeService {
    private apiKey = '67b8f48746b372ca86e231d2e9dabce4';

    constructor(private http: HttpClient, private utils: UtilsService, private authApi: AuthService) {
    }

    async getTrees() {
        const trees$ = this.http.get(`https://zingtree.com/api/tree/${this.apiKey}/get_trees`);
        const data = await this.utils.toPromise(trees$);
        console.log(await this.getAgentSessions());
        return data.trees;
    }

    async getAgentSessions(agent = '*') {
        const sessions$ = this.http.get(`https://zingtree.com/api/agent_sessions/${this.apiKey}/${agent}`);
        const sessions = await this.utils.toPromise(sessions$);
        console.log(sessions);
        return sessions;
    }

    async getSessionData(session: string) {
        const sessionData$ = this.http.get(`https://zingtree.com/api/session/${session}/get_session_data`);
        const formData$ = this.http.get(`https://zingtree.com/api/session/${session}/get_form_data`);
        const sessionData = await this.utils.toPromise(sessionData$);
        const formData = await this.utils.toPromise(formData$);

        return {
            formData,
            sessionData
        };
    }

    async open(treeId) {
        const user = await this.authApi.currentUser();
        const token = this.authApi.getToken();
        show_popup(treeId, `&source=${user.username}&agent_mode=1&auth_token=${token}`);
    }
}
