import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventScriptsComponent } from './component/event-scripts/event-scripts.component';
import { ZingtreeService } from './services/zingtree.service';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { TableResizeModule } from '../table-resize/table-resize.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        PagingTableModule,
        TableResizeModule,
        RouterModule.forChild([
            { path: '', component: EventScriptsComponent }
        ])
    ],
    declarations: [EventScriptsComponent],
    providers: [
        ZingtreeService
    ]
})
export class RouteEventScriptingModule {
}
