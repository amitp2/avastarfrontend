import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './components/users/users.component';
import { SubscriberUsersModule } from '../subscriber-users/subscriber-users.module';
import { UsersFormComponent } from './components/users-form/users-form.component';
import { SharedModule } from '../shared/shared.module';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { SelectModule } from '../select/select.module';
import { TableResizeModule } from '../table-resize/table-resize.module';

const routes: Routes = [
    {path: '', pathMatch: 'full', component: UsersComponent},
    {path: ':id', component: UsersFormComponent}
];

@NgModule({
    imports: [
        CommonModule,
        SubscriberUsersModule,
        SharedModule,
        PagingTableModule,
        SelectModule,
        TableResizeModule,
        RouterModule.forChild(routes)
    ],
    declarations: [UsersComponent, UsersFormComponent]
})
export class RouteUsersModule {
}
