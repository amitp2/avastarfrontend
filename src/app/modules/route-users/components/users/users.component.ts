import { Component, OnInit, ViewChild } from '@angular/core';
import { UserModel } from '../../../../models/user-model';
import { UserRole } from '../../../../enums/user-role.enum';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { UserResourceService } from '../../../../services/resources/user-resource.service';
import { UtilsService } from '../../../../services/utils.service';
import { AuthService } from '../../../../services/auth.service';
import { CurrentUser } from '../../../../interfaces/current-user';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { PagingTableService } from '../../../../services/paging-table.service';
import { SetSubscriberProductUsage } from '../../../../store/subscriber-product-usage/subscriber-product-usage.actions';
import { ProductResourceService } from '../../../../services/resources/product-resource.service';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';
import { SetSubscriberProductAccess } from '../../../../store/subscriber-product-access/subscriber-product-access.actions';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.styl'],
    providers: [PagingTableService]
})
export class UsersComponent implements OnInit {

    @ViewChild('table') table: PagingTableComponent;

    userRole = UserRole;
    currentUser: CurrentUser;

    columns: PagingTableHeaderColumn[] = [
        {isSortable: true, title: 'Contact', key: 'firstName'},
        {isSortable: true, title: 'Subscriber', key: 'subscriber.name'},
        {isSortable: true, title: 'Email', key: 'username'},
        {isSortable: false, title: 'Role', key: 'role'},
        {isSortable: true, title: 'Status', key: 'status'},
        {isSortable: false, title: 'Access', key: 'status'},
        {isSortable: false, title: 'Actions', key: 'actions'}
    ];

    constructor(private userResourceService: UserResourceService,
                private utilsService: UtilsService,
                private authService: AuthService,
                private productResource: ProductResourceService,
                private store: Store<AppState>) {

        this.authService.currentUser().then((user: CurrentUser) => {
            this.currentUser = user;
            if (this.utilsService.userHasRole(user, UserRole.ADMIN)) {
                this.columns = [
                    {isSortable: true, title: 'Contact', key: 'firstName'},
                    {isSortable: true, title: 'Email', key: 'username'},
                    {isSortable: false, title: 'Role', key: 'role'},
                    {isSortable: true, title: 'Status', key: 'status'},
                    {isSortable: false, title: 'Access', key: 'status'},
                    {isSortable: false, title: 'Actions', key: 'actions'}
                ];
            }
        });
    }


    ngOnInit(): void {
        this.fetchSubscriberProductUsage();
        this.fetchSubscriberProductAccess();
    }

    async fetchSubscriberProductAccess() {
        const currentUser = await this.authService.currentUser();
        const subscriberId = currentUser.subscriberId;
        const access = await this.productResource.accessBySubscriber(subscriberId);
        this.store.dispatch(new SetSubscriberProductAccess({
            subscriberId,
            access
        }));
    }

    async fetchSubscriberProductUsage() {
        const currentUser = await this.authService.currentUser();
        const subscriberId = currentUser.subscriberId;
        const usage = await this.productResource.usageBySubscriber(subscriberId);

        this.store.dispatch(new SetSubscriberProductUsage({
            subscriberId,
            usage
        }));
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return this.userResourceService.query(this.utilsService.tableToHttpParams(params));
        }).bind(this);
    }

    async delete(user: UserModel) {
        await this.userResourceService.delete(user.id);
        await this.userResourceService.releaseProducts(user);
        this.table.delete((u) => +u.id === +user.id);
    }

}
