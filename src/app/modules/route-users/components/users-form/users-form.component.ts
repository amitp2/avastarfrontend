import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute, Router } from '@angular/router';
import { UserModel } from '../../../../models/user-model';
import { UserRole } from '../../../../enums/user-role.enum';

@Component({
    selector: 'app-users-form',
    templateUrl: './users-form.component.html',
    styleUrls: ['./users-form.component.styl']
})
export class UsersFormComponent implements OnInit {
    userIdObs: Observable<any>;
    userRole = UserRole;

    constructor(private activatedRoute: ActivatedRoute, private router: Router) {
        this.userIdObs = this.activatedRoute.params.map(params => params.id);
    }

    ngOnInit() {
    }

    // added(user: UserModel) {
    //     this.router.navigateByUrl(`/u/users/${user.id}`);
    // }

    goBack() {
        this.router.navigateByUrl(`/u/users`);
    }

}
