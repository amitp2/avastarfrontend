import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ReportService, ReportAction } from '../../../../services/report.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { CurrentPropertyService } from '../../../../services/current-property.service';

@Component({
    selector: 'app-overdue-maintenance-report',
    templateUrl: './overdue-maintenance-report.component.html',
    styleUrls: ['./overdue-maintenance-report.component.styl']
})
export class OverdueMaintenanceReportComponent {
    isDisabled: boolean;
    ReportAction = ReportAction;

    constructor(
        private reportApi: ReportService,
        private currentVenue: CurrentPropertyService
    ) {
    }

    @AsyncMethod({
        taskName: 'generate_report'
    })
    generate(action: ReportAction) {
        this.isDisabled = true;
        return this.currentVenue.currentVenueAsync()
            .then(venue => this.reportApi.overdueMaintenanceReport(venue.id, action))
            .then(() => this.isDisabled = false);
    }
}
