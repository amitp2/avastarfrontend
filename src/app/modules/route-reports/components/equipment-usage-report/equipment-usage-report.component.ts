import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReportAction, ReportService } from '../../../../services/report.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { CurrentPropertyService } from '../../../../services/current-property.service';

@Component({
    selector: 'app-equipment-usage-report',
    templateUrl: './equipment-usage-report.component.html',
    styleUrls: ['./equipment-usage-report.component.styl']
})
export class EquipmentUsageReportComponent implements OnInit {
    form: FormGroup;
    isDisabled: boolean;
    ReportAction = ReportAction;

    constructor(
        private fb: FormBuilder,
        private reportApi: ReportService,
        private currentVenue: CurrentPropertyService
    ) {
    }

    ngOnInit() {
        this.form = this.fb.group({
            fromDate: ['', Validators.required],
            toDate: ['', Validators.required]
        });
    }

    @AsyncMethod({
        taskName: 'generate_report'
    })
    generate(action: ReportAction) {
        this.isDisabled = true;
        const params = {
            ...this.form.value,
            fromDate: this.form.value.fromDate.split('-').join('/'),
            toDate: this.form.value.toDate.split('-').join('/')
        };
        return this.currentVenue.currentVenueAsync()
            .then(venue => this.reportApi.usageReport(action, params, venue.id))
            .then(() => this.isDisabled = false)
            .catch(() => this.isDisabled = false);
    }
}
