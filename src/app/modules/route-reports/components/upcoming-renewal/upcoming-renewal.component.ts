import { Component, OnInit } from '@angular/core';
import { ReportAction, ReportService } from '../../../../services/report.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-upcoming-renewal',
    templateUrl: './upcoming-renewal.component.html',
    styleUrls: ['./upcoming-renewal.component.styl']
})
export class UpcomingRenewalComponent implements OnInit {

    ReportAction = ReportAction;
    inProgress = false;
    form: FormGroup;

    constructor(private reportApi: ReportService) {
        this.form = new FormGroup({
            from: new FormControl(null, [Validators.required]),
            to: new FormControl(null, [Validators.required])
        });
    }

    ngOnInit() {
    }

    async generate(action: ReportAction) {
        this.inProgress = true;
        const { from, to } = this.form.value;
        try {
            await this.reportApi.upcomingRenewal(action, from, to);
        } catch (exc) {
        }
        this.inProgress = false;
    }

}
