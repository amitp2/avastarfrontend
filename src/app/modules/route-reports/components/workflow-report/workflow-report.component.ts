import { Component, OnInit } from '@angular/core';
import { ReportAction, ReportService } from '../../../../services/report.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { CurrentPropertyService } from '../../../../services/current-property.service';

@Component({
    selector: 'app-workflow-report',
    templateUrl: './workflow-report.component.html',
    styleUrls: ['./workflow-report.component.styl']
})
export class WorkflowReportComponent implements OnInit {
    form: FormGroup;
    isDisabled: boolean;
    ReportAction = ReportAction;

    constructor(
        private fb: FormBuilder,
        private reportApi: ReportService,
        private currentVenue: CurrentPropertyService
    ) {
    }

    ngOnInit() {
        this.form = this.fb.group({
            fromDate: ['', Validators.required],
            toDate: ['', Validators.required]
        });
    }

    @AsyncMethod({
        taskName: 'generate_report'
    })
    generate(action: ReportAction) {
        this.isDisabled = true;
        const params = {
            ...this.form.value,
            fromDate: this.form.value.fromDate.split('-').join('/'),
            toDate: this.form.value.toDate.split('-').join('/')
        };
        return this.currentVenue.currentVenueAsync()
            .then(venue => this.reportApi.workflowReport(action, params, venue.id))
            .then(() => this.isDisabled = false);
    }
}
