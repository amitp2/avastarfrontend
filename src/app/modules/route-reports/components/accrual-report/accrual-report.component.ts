import { Component, OnInit } from '@angular/core';
import { ReportAction, ReportService } from '../../../../services/report.service';
import { CurrentPropertyService } from '../../../../services/current-property.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { SelectFetchType } from '../../../select/types/select-fetch-type';

@Component({
    selector: 'app-accrual-report',
    templateUrl: './accrual-report.component.html',
    styleUrls: ['./accrual-report.component.styl']
})
export class AccrualReportComponent implements OnInit {
    form: FormGroup;
    isDisabled: boolean;
    ReportAction = ReportAction;

    SelectFetchType = SelectFetchType;
    constructor(
        private fb: FormBuilder,
        private reportApi: ReportService,
        private currentVenue: CurrentPropertyService
    ) {
    }

    ngOnInit() {
        this.form = this.fb.group({
            fromDate: ['', Validators.required],
            toDate: ['', Validators.required]
        });

        // this.form = this.fb.group({
        //         year: [null, Validators.required],
        //         month: [null, Validators.required]
        //     });

            // this.form.valueChanges.subscribe(dat =>{
            //     console.log("Month ", dat.month)
            // })
    }

    @AsyncMethod({
        taskName: 'generate_report'
    })
    generate(action: ReportAction) {
        this.isDisabled = true;
        
        const params = {
           ...this.form.value,
            fromDate: this.form.value.fromDate.split('-').join('/'),
            toDate: this.form.value.toDate.split('-').join('/')
            // year:this.form.value.year,
            // //month: this.form.value.month
            // month:parseInt(this.form.value.month) + 1
            // // year: this.form.value.year.split('-').join('/'),
            // // month: this.form.value.month.split('-').join('/')
        };
        return this.currentVenue.currentVenueAsync()
            .then(venue => this.reportApi.accrualReport(action, params, venue.id))
            .then(() => this.isDisabled = false);
    }

    // getChange(event){
    //     let evn =  parseInt(event) + 1;
    //     console.log("1 month",evn);
    //     this.form.value.month = event
    //     console.log("2 montn",evn);
    // }
}


