import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ReportAction, ReportService} from '../../../../services/report.service';
import {VenueModel} from '../../../../models/venue-model';
import {Observable} from 'rxjs/Observable';
import {CurrentPropertyService} from '../../../../services/current-property.service';
import {AsyncMethod} from '../../../../decorators/async-method.decorator';

const toDropdownList = venues => venues.map(venue => ({
    value: venue.id,
    display: venue.name
}));

@Component({
    selector: 'app-event-spaces-report',
    templateUrl: './event-spaces-report.component.html',
    styleUrls: ['./event-spaces-report.component.styl']
})
export class EventSpacesReportComponent implements OnInit {
    form: FormGroup;
    isDisabled: boolean;
    currentVenueList: Observable<VenueModel[]>;
    ReportAction = ReportAction;

    constructor(
        private fb: FormBuilder,
        private currentVenue: CurrentPropertyService,
        private reportApi: ReportService
    ) {
        this.currentVenueList = currentVenue.currentVenuesList;
    }

    loadFunction = () => this.currentVenue.currentVenueListAsync().then(toDropdownList);

    ngOnInit() {
        this.form = this.fb.group({
            venueId: ['', Validators.required]
        });

        this.form.get('venueId').disable();
    }

    @AsyncMethod({
        taskName: 'generate_report'
    })
    generate(action: ReportAction) {
        this.isDisabled = true;
        return this.currentVenue.currentVenueAsync()
            .then(venue => this.reportApi.eventSpacesReport(action, venue.id))
            .then(() => this.isDisabled = false)
            .catch(error => this.isDisabled = false);
    }
}
