import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReportService, ReportAction } from '../../../../services/report.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { CurrentPropertyService } from '../../../../services/current-property.service';

@Component({
    selector: 'app-scheduled-maintenance-report',
    templateUrl: './scheduled-maintenance-report.component.html',
    styleUrls: ['./scheduled-maintenance-report.component.styl']
})
export class ScheduledMaintenanceReportComponent {
    isDisabled: boolean;
    ReportAction = ReportAction;

    form:FormGroup

    constructor(private reportApi: ReportService, 
        private currentVenue: CurrentPropertyService,
        private fb: FormBuilder,
     ) {
    }

    ngOnInit() {
        this.form = this.fb.group({
            fromDate: ['', Validators.required],
            toDate: ['', Validators.required]
        });
    }

    @AsyncMethod({
        taskName: 'generate_report'
    })
    
    // generate(action: ReportAction) {
    //     this.isDisabled = true;
    //     return this.currentVenue.currentVenueAsync()
    //         .then(venue => this.reportApi.scheduledMaintenanceReport(venue.id, action))
    //         .then(() => this.isDisabled = false);
    // }

    generate(action: ReportAction) {
        this.isDisabled = true;
        const params = {
            ...this.form.value,
            fromDate: this.form.value.fromDate.split('-').join('/'),
            toDate: this.form.value.toDate.split('-').join('/')
        };
        return this.currentVenue.currentVenueAsync()
            .then(venue => this.reportApi.scheduledMaintenanceReport(action, params, venue.id))
            .then(() => this.isDisabled = false);
    }
}
