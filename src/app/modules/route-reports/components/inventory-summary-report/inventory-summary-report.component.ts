import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { VenueModel } from '../../../../models/venue-model';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CurrentPropertyService } from '../../../../services/current-property.service';
import { ReportService, ReportAction } from '../../../../services/report.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { MultiSelectItem } from '../../../shared/components/form-controls/multi-select/multi-select.component';
import { InventoryCategoryResourceService } from '../../../../services/resources/inventory-category-resource.service';
import { SubscriberResourceService } from '../../../../services/resources/subscriber-resource.service';
import { InventorySpacesService, InventorySpace } from '../../../route-inventory/services/inventory-spaces.service';
import { StorageLocationFetchFactory } from '../../../select/fetches/factories/storage-location.fetch.factory';
import { DropdownItem } from '../../../../interfaces/dropdown-item';

const toDropdownList = venues => venues.map(venue => ({
    value: venue.id,
    display: venue.name
}));

@Component({
    selector: 'app-inventory-summary-report',
    templateUrl: './inventory-summary-report.component.html',
    styleUrls: ['./inventory-summary-report.component.styl']
})
export class InventorySummaryReportComponent implements OnInit {
    form: FormGroup;
    location: FormGroup;
    statusForm: FormGroup;
    installPortable: FormGroup;

    isDisabled: boolean;

    currentVenueList: Observable<VenueModel[]>;
    ReportAction = ReportAction;

    private category: MultiSelectItem[];
    private status: MultiSelectItem[];
    private locat: MultiSelectItem[];
    SelectFetchType = SelectFetchType;

    selectlocation: boolean;

    installPort = [
        { id: 1, name: "Installed" },
        { id: 2, name: "Portable" },
    ]

    locationName = [
        { id: 1, name: "Portable" },
        { id: 2, name: "Install" },
    ]

    constructor(
        private fb: FormBuilder,
        private currentVenue: CurrentPropertyService,
        private reportApi: ReportService,

        private categoryApi: InventoryCategoryResourceService,
        private inventorySpaces: InventorySpacesService,

        public storageLocationFetchFactory: StorageLocationFetchFactory,


    ) {
        this.currentVenueList = currentVenue.currentVenuesList;
        this.loadSubscribers();
        this.stat();
        this.loadLocation();

    }


    loadFunction = () => this.currentVenue.currentVenueListAsync().then(toDropdownList);

    ngOnInit() {
        this.form = this.fb.group({
            venueId: this.fb.control({ disabled: true }, [Validators.required])
        });
        //uncomment the 41 when
        this.form.get('venueId').disable();

        // console.log("Space", this.storageLocationFetchFactory);

        // //Category DropDown
        // this.form = this.fb.group({
        //     categoryIds: new FormControl(null),
        //     statusID: new FormControl(null),
        //     locationIds: new FormControl(null),
        //     type: new FormControl(null)
        // })

    }

    // this.form.get('venueId').disable();
    async stat() {
        const stats = await [
            { id: 1, name: "Active" },
            { id: 2, name: "AtRepair" },
            { id: 3, name: "Broken" },
            { id: 4, name: "Missing" },
            { id: 5, name: "Removed" },
            { id: 6, name: "ServiceRequired" }
        ]
        this.status = stats.map(sub => ({ id: sub.id, text: sub.name }))
    }

    // onChangeLocation(loc){
    //    console.log("Location ", loc);
    //    this.selectlocation = loc;
    // }

    @AsyncMethod({
        taskName: 'generate_report'
    })

    generate(action: ReportAction) {
        this.isDisabled = true;

        return this.currentVenue.currentVenueAsync()
            .then(venue => (
                this.reportApi.inventoryReport(venue.id, action)
            ))
            .then(() => {
                this.isDisabled = false;
            });
    }
    // @AsyncMethod({
    //     taskName: 'generate__report'
    // })

    // generate(action: ReportAction) {
    //     this.isDisabled = true;
    //     const params = {
    //        ...this.form.value,
    //        categoryIds:this.form.value.categoryIds,
    //        statusID:this.form.value.statusID,
    //        locationIds:this.form.value.locationIds,
    //        type:this.form.value.type,
    //      };
    //      console.log("Generatecategory " , params);
    //      return this.currentVenue.currentVenueAsync()
    //         .then(venue => this.reportApi.inventoryReport(action, params, venue.id))
    //         .then(() => this.isDisabled = false);
    // }


    //  loadLocation = (): Promise<DropdownItem[]> =>{
    //  return this.inventorySpaces.getSpaces()
    //   .then((res) => res.map((space: InventorySpace) => ({
    //     value: space.id,
    //     display: space.name,
    //     disabled: +space.id === -1
    // })))
    //console.log("hellodd", this.inventorySpaces);
    // .then((resp: any) => {

    //     if (this.addAllOption) {
    //         return [{value: '', display: 'All'}].concat(resp);
    //     }

    //     return resp;
    // });
    //}

    async loadLocation() {
        const models = await this.inventorySpaces.getSpaces();
        this.locat = models.map(sub => ({ id: sub.id, text: sub.name, disabled: +sub.id === -1 }));
    }


    async loadSubscribers() {
        const models = await this.categoryApi.getAll();
        this.category = models.map(sub => ({ id: sub.id, text: sub.name }));
    }
}
