import { Component, OnInit } from '@angular/core';
import { ReportAction, ReportService } from '../../../../services/report.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { CurrentPropertyService } from '../../../../services/current-property.service';

@Component({
    selector: 'app-service-maintenance-report',
    templateUrl: './service-maintenance-report.component.html',
    styleUrls: ['./service-maintenance-report.component.styl']
})
export class ServiceMaintenanceReportComponent implements OnInit {
    form: FormGroup;
    isDisabled: boolean;
    ReportAction = ReportAction;

    constructor(private fb: FormBuilder, private reportApi: ReportService, private currentVenue: CurrentPropertyService) {
    }

    ngOnInit() {
        this.form = this.fb.group({
            fromDate: [null, Validators.required],
            toDate: [null, Validators.required],
            // ticketCount: [null, [Validators.required, Validators.min(0)]]
            //as per change the jira ticket -AB-47
            //ticketId: [null, [Validators.required, Validators.min(0)]]
        });
    }

    @AsyncMethod({
        taskName: 'generate_report'
    })
    generate(action: ReportAction) {
        
        this.isDisabled = true;
        const params = {
            ...this.form.value,
            fromDate: this.form.value.fromDate.split('-').join('/'),
            toDate: this.form.value.toDate.split('-').join('/')
        };
        return this.currentVenue.currentVenueAsync()
            .then(venue => this.reportApi.serviceMaintenanceReport(action, venue.id, params))
            .then(() => this.isDisabled = false)
            .catch((e) => this.isDisabled = false);
    }
}
