import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { ReportService, ReportAction } from '../../../../services/report.service';
import { EventFetchFactory } from '../../../select/fetches/factories/event.fetch.factory';
import { SelectFetchType } from '../../../select/types/select-fetch-type';

@Component({
    selector: 'app-invoice-report',
    templateUrl: './invoice-report.component.html',
    styleUrls: ['./invoice-report.component.styl']
})
export class InvoiceReportComponent implements OnInit {
    form: FormGroup;
    isDisabled: boolean;
    ReportAction = ReportAction;

    SelectFetchType = SelectFetchType;

    constructor(private fb: FormBuilder,
                private reportApi: ReportService,
                public eventFetchFactory: EventFetchFactory) {
    }

    ngOnInit() {
        this.form = this.fb.group({
            eventId: [null, Validators.required],
            accountId: [null, Validators.required]
        });
    }

    @AsyncMethod({
        taskName: 'generate_report'
    })
    generate(action: ReportAction) {
        this.isDisabled = true;
        return this.reportApi.invoiceReport(action, this.form.value.eventId)
            .then(() => this.isDisabled = false);
    }
}
