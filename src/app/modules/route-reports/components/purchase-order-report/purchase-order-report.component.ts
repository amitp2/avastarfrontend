import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { ReportService, ReportAction } from '../../../../services/report.service';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { PurchaseOrderFetchFactory } from '../../../select/fetches/factories/purchase-order.fetch.factory';

@Component({
    selector: 'app-purchase-order-report',
    templateUrl: './purchase-order-report.component.html',
    styleUrls: ['./purchase-order-report.component.styl']
})
export class PurchaseOrderReportComponent implements OnInit {
    form: FormGroup;
    isDisabled: boolean;
    ReportAction = ReportAction;
    SelectFetchType = SelectFetchType;

    constructor(private fb: FormBuilder,
                private reportApi: ReportService,
                public purchaseOrderFetchFactory: PurchaseOrderFetchFactory) {
    }

    ngOnInit() {
        this.form = this.fb.group({
            //vendorId: [null],
            purchaseOrderId: [null, Validators.required]
        });
    }

    @AsyncMethod({
        taskName: 'generate_report'
    })
    generate(action: ReportAction) {
        this.isDisabled = true;
        return this.reportApi.purchaseOrderReport(action, this.form.value.purchaseOrderId)
            .then(() => this.isDisabled = false);
    }
}
