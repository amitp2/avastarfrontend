import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CurrentPropertyService } from '../../../../services/current-property.service';
import { VenueModel } from '../../../../models/venue-model';
import { Observable } from 'rxjs/Observable';
import { ReportAction, ReportService } from '../../../../services/report.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';

@Component({
    selector: 'app-daily-billing-summary-report',
    templateUrl: './daily-billing-summary-report.component.html',
    styleUrls: ['./daily-billing-summary-report.component.styl']
})
export class DailyBillingSummaryReportComponent implements OnInit {
    form: FormGroup;
    isDisabled: boolean;
    ReportAction = ReportAction;
    currentVenueList: Observable<VenueModel[]>;

    constructor(private fb: FormBuilder, private currentVenue: CurrentPropertyService, private reportApi: ReportService) {
        this.currentVenueList = currentVenue.currentVenuesList;
    }

    loadFunction = () => this.currentVenue.currentVenueListAsync()
        .then(venues => venues.map(v => ({
            value: v.id,
            display: v.name
        })))

    ngOnInit() {
        this.form = this.fb.group({
            date: ['', Validators.required],
            venueId: ['', Validators.required]
        });

        this.form.get('venueId').disable();
    }

    @AsyncMethod({
        taskName: 'generate_report'
    })
    generate(action: ReportAction) {
        this.isDisabled = true;
        const params = {
            ...this.form.value,
            date: this.form.value.date.split('-').join('/')
        };
        return this.currentVenue.currentVenueAsync()
            .then(venue => this.reportApi.dailyReport(action, params, venue.id))
            .then(() => this.isDisabled = false)
            .catch(() => this.isDisabled = false);
    }
}
