import { Component, OnInit } from '@angular/core';
import { ReportAction, ReportService } from '../../../../services/report.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-unpaid-accounts',
    templateUrl: './unpaid-accounts.component.html',
    styleUrls: ['./unpaid-accounts.component.styl']
})
export class UnpaidAccountsComponent implements OnInit {

    ReportAction = ReportAction;
    inProgress = false;
    form: FormGroup;

    constructor(private reportApi: ReportService) {
        this.form = new FormGroup({
            from: new FormControl(null, [Validators.required]),
            to: new FormControl(null, [Validators.required])
        });
    }

    ngOnInit() {

    }

    async generate(action: ReportAction) {
        this.inProgress = true;
        const { from, to } = this.form.value;
        try {
            await this.reportApi.unpaidAccounts(action, from, to);
        } catch (exc) {
        }
        this.inProgress = false;

    }
}
