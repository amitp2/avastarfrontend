import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScheduledMaintenanceReportComponent } from './components/scheduled-maintenance-report/scheduled-maintenance-report.component';
import { PurchaseOrderReportComponent } from './components/purchase-order-report/purchase-order-report.component';
import { OverdueMaintenanceReportComponent } from './components/overdue-maintenance-report/overdue-maintenance-report.component';
import { LaborSummaryReportComponent } from './components/labor-summary-report/labor-summary-report.component';
import { InvoiceReportComponent } from './components/invoice-report/invoice-report.component';
import { EquipmentUsageReportComponent } from './components/equipment-usage-report/equipment-usage-report.component';
import { InventorySummaryReportComponent } from './components/inventory-summary-report/inventory-summary-report.component';
import { DailyBillingSummaryReportComponent } from './components/daily-billing-summary-report/daily-billing-summary-report.component';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Route } from '@angular/router';
import { AllowOnlyService } from '../../services/guards/allow-only.service';
import { UserRole } from '../../enums/user-role.enum';
import { ReportService } from '../../services/report.service';
import { EventSpacesReportComponent } from './components/event-spaces-report/event-spaces-report.component';
import { SelectModule } from '../select/select.module';
import { UpcomingRenewalComponent } from './components/upcoming-renewal/upcoming-renewal.component';
import { UnpaidAccountsComponent } from './components/unpaid-accounts/unpaid-accounts.component';
import { ServiceMaintenanceReportComponent } from './components/service-maintenance-report/service-maintenance-report.component';
import { WorkflowReportComponent } from './components/workflow-report/workflow-report.component';
import { AccrualReportComponent } from './components/accrual-report/accrual-report.component';

const techTeamLeadRouteConfig = {
    canActivate: [AllowOnlyService],
    data: {
        roles: [UserRole.ADMIN, UserRole.TECH_TEAM_LEAD]
    }
};

const eventPlannerRouteConfig = {
    canActivate: [AllowOnlyService],
    data: {
        roles: [UserRole.ADMIN, UserRole.EVENT_PLANNER]
    }
};

const reportRoutes: Route[] = [
    {
        canActivate: [AllowOnlyService],
        data: {
            roles: [
                UserRole.SUPER_ADMIN
            ]
        },
        path: 'upcoming-renewal',
        component: UpcomingRenewalComponent
    },
    {
        canActivate: [AllowOnlyService],
        data: {
            roles: [
                UserRole.SUPER_ADMIN
            ]
        },
        path: 'unpaid-accounts',
        component: UnpaidAccountsComponent
    },
    {
        canActivate: [AllowOnlyService],
        data: {
            roles: [
                UserRole.ADMIN,
                UserRole.TECH_TEAM_LEAD,
                UserRole.TECH_TEAM
            ]
        },
        path: 'inventory-summary',
        component: InventorySummaryReportComponent
    },
    {
        canActivate: [AllowOnlyService],
        data: {
            roles: [
                UserRole.ADMIN,
                UserRole.TECH_TEAM_LEAD,
                UserRole.TECH_TEAM
            ]
        },
        path: 'scheduled-maintenance',
        component: ScheduledMaintenanceReportComponent
    },
    {
        canActivate: [AllowOnlyService],
        data: {
            roles: [
                UserRole.ADMIN,
                UserRole.TECH_TEAM_LEAD,
                UserRole.TECH_TEAM
            ]
        },
        path: 'overdue-maintenance',
        component: OverdueMaintenanceReportComponent
    },
    {
        canActivate: [AllowOnlyService],
        data: {
            roles: [
                UserRole.ADMIN,
                UserRole.TECH_TEAM_LEAD,
                UserRole.TECH_TEAM,
                UserRole.SERVICE_TEAM
            ]
        },
        path: 'service-maintenance',
        component: ServiceMaintenanceReportComponent
    },
    {
        ...eventPlannerRouteConfig,
        path: 'invoice',
        component: InvoiceReportComponent
    },
    {
        canActivate: [AllowOnlyService],
        data: {
            roles: [
                UserRole.ADMIN,
                UserRole.TECH_TEAM_LEAD,
                UserRole.TECH_TEAM
            ]
        },
        path: 'purchase-order',
        component: PurchaseOrderReportComponent
    },
    {
        ...techTeamLeadRouteConfig,
        path: 'daily-billing',
        component: DailyBillingSummaryReportComponent
    },
    {
        canActivate: [AllowOnlyService],
        data: {
            roles: [
                UserRole.ADMIN,
                UserRole.EVENT_PLANNER,
                UserRole.TECH_TEAM_LEAD,
                UserRole.TECH_TEAM
            ]
        },
        path: 'labor-summary',
        component: LaborSummaryReportComponent
    },
    {
        canActivate: [AllowOnlyService],
        data: {
            roles: [
                UserRole.ADMIN,
                UserRole.EVENT_PLANNER,
                UserRole.TECH_TEAM_LEAD,
                UserRole.TECH_TEAM
            ]
        },
        path: 'workflow-summary',
        component: WorkflowReportComponent
    },
    {
        ...techTeamLeadRouteConfig,
        path: 'accrual',
        component: AccrualReportComponent
    },
    {
        canActivate: [AllowOnlyService],
        data: {
            roles: [
                UserRole.ADMIN,
                UserRole.TECH_TEAM_LEAD,
                UserRole.TECH_TEAM
            ]
        },
        path: 'equipment-usage',
        component: EquipmentUsageReportComponent
    },
    {
        canActivate: [AllowOnlyService],
        data: {
            roles: [
                UserRole.ADMIN,
                UserRole.EVENT_PLANNER,
                UserRole.TECH_TEAM,
                UserRole.TECH_TEAM_LEAD
            ]
        },
        path: 'event-spaces',
        component: EventSpacesReportComponent
    }
];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        ReactiveFormsModule,
        RouterModule.forChild(reportRoutes),
        SelectModule
    ],
    declarations: [
        ScheduledMaintenanceReportComponent,
        PurchaseOrderReportComponent,
        OverdueMaintenanceReportComponent,
        LaborSummaryReportComponent,
        InvoiceReportComponent,
        EquipmentUsageReportComponent,
        InventorySummaryReportComponent,
        DailyBillingSummaryReportComponent,
        EventSpacesReportComponent,
        UpcomingRenewalComponent,
        UnpaidAccountsComponent,
        ServiceMaintenanceReportComponent,
        WorkflowReportComponent,
        AccrualReportComponent
    ],
    providers: [
        ReportService
    ]
})
export class RouteReportsModule {
}
