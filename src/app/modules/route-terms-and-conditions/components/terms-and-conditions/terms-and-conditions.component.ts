import { Component, OnInit } from '@angular/core';
import {FormHandler} from '../../../../classes/form-handler';
import {VenueTermsModel} from '../../../../models/venue-terms';
import {Observable} from 'rxjs/Observable';
import {FormControl, FormGroup} from '@angular/forms';
import {FormMode} from '../../../../enums/form-mode.enum';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {PromisedStoreService} from '../../../../services/promised-store.service';
import {VenueTermsResourceService} from '../../../../services/resources/venue-terms-resource.service';
import {AsyncMethod} from '../../../../decorators/async-method.decorator';
import {UtilsService} from '../../../../services/utils.service';
import {ActivatedRoute} from '@angular/router';
import {AsyncTasksService} from '../../../../services/async-tasks.service';
import {AppSuccess} from '../../../../enums/app-success.enum';

@Component({
    selector: 'app-terms-and-conditions',
    templateUrl: './terms-and-conditions.component.html',
    styleUrls: ['./terms-and-conditions.component.styl']
})
export class TermsAndConditionsComponent extends FormHandler<VenueTermsModel> {

    activeField: string;
    taskSuccess: AppSuccess;

    constructor(private promisedStore: PromisedStoreService,
                private terms: VenueTermsResourceService,
                private utils: UtilsService,
                private activatedRoute: ActivatedRoute,
                private asyncTasks: AsyncTasksService) {
        super();
        this.activeField = this.activatedRoute.snapshot.data.activeField;
        this.taskSuccess = this.activatedRoute.snapshot.data.taskSuccess;
    }

    entityId(): Observable<number> {
        return new BehaviorSubject<any>('add');
    }

    formMode(): Observable<FormMode> {
        return new BehaviorSubject<FormMode>(FormMode.Edit);
    }

    initializeForm(): FormGroup {
        return new FormGroup({
            terms: new FormControl(),
            billingComment: new FormControl(),
            proposalComment: new FormControl()
        });
    }

    get(id: number): Promise<VenueTermsModel> {
        return this.promisedStore.currentVenueId()
            .then((venueId: number) => this.terms.getByVenueId(venueId));
    }

    add(model: VenueTermsModel): Promise<any> {
        return undefined;
    }

    @AsyncMethod({
        taskName: 'update_terms'
    })
    edit(id: number, model: VenueTermsModel): Promise<any> {
        return this.promisedStore.currentVenueId()
            .then((venueId: number) => this.terms.editByVenueId(model, venueId))
            .then(() => {
                this.asyncTasks.taskSuccess('update_terms', this.taskSuccess);
            });
    }

    serialize(model: VenueTermsModel): any {
        return this.utils.transformInto(model, {
            terms: { to: 'terms' },
            billingComment: { to: 'billingComment' },
            proposalComment: { to: 'proposalComment' },
        });
    }

    deserialize(formValue: any): VenueTermsModel {
        return new VenueTermsModel(this.utils.transformInto(formValue, {
            terms: { to: 'terms' },
            billingComment: { to: 'billingComment' },
            proposalComment: { to: 'proposalComment' },
        }));
    }

    clear() {
        this.form.patchValue({
            [this.activeField]: ''
        });
    }

}
