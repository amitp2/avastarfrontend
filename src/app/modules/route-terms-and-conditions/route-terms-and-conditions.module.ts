import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TermsComponent } from './components/terms/terms.component';
import { SharedModule } from '../shared/shared.module';
import { TermsAndConditionsComponent } from './components/terms-and-conditions/terms-and-conditions.component';
import { BillingCommentComponent } from './components/billing-comment/billing-comment.component';
import { ProposalCommentComponent } from './components/proposal-comment/proposal-comment.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AppSuccess} from '../../enums/app-success.enum';

const routes: Routes = [
    {
        path: '', component: TermsComponent,
        children: [
            { path: '', data: { activeField: 'terms', taskSuccess: AppSuccess.TERM_UPDATE }, component: TermsAndConditionsComponent},
            { path: 'billing', data: { activeField: 'billingComment', taskSuccess: AppSuccess.BILLING_UPDATE }, component: TermsAndConditionsComponent },
            { path: 'proposal', data: { activeField: 'proposalComment', taskSuccess: AppSuccess.PROPOSAL_UPDATE }, component: TermsAndConditionsComponent }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule
    ],
    declarations: [TermsComponent, TermsAndConditionsComponent, BillingCommentComponent, ProposalCommentComponent]
})
export class RouteTermsAndConditionsModule {
}
