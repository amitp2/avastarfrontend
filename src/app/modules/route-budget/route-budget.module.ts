import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BudgetInputComponent } from './components/budget-input/budget-input.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule, Route } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { AllowOnlyService } from '../../services/guards/allow-only.service';
import { UserRole } from '../../enums/user-role.enum';
import { SelectModule } from '../select/select.module';
import { MaskedInputModule } from '../masked-input/masked-input.module';

const routes: Route[] = [
    {
        path: '',
        pathMatch: 'full',
        component: BudgetInputComponent,
        canActivate: [AllowOnlyService],
        data: {
            roles: [UserRole.ADMIN]
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        MaskedInputModule,
        SelectModule
    ],
    declarations: [
        BudgetInputComponent
    ]
})
export class RouteBudgetModule { }
