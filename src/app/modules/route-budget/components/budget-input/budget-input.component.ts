import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, FormArray, Validators} from '@angular/forms';
import {AuthService} from '../../../../services/auth.service';
import {BudgetService} from '../../../../services/budget.service';
import {AsyncMethod} from '../../../../decorators/async-method.decorator';
import {AppError} from '../../../../enums/app-error.enum';
import {AppSuccess} from '../../../../enums/app-success.enum';
import {SelectFetchType} from '../../../select/types/select-fetch-type';

@Component({
    selector: 'app-budget-input',
    templateUrl: './budget-input.component.html',
    styleUrls: ['./budget-input.component.styl']
})
export class BudgetInputComponent implements OnInit {
    form: FormGroup;
    filterForm: FormGroup;
    revenueBudgetForm: FormGroup;
    costBudgetForm: FormGroup;
    yearList: number[] = [];
    isLoading: boolean;

    SelectFetchType = SelectFetchType;

    get categoryControl() {
        return this.filterForm.get('category');
    }

    get yearCtrl() {
        return this.filterForm.get('year');
    }

    get revenueSelected() {
        const budgetType = this.categoryControl.value;
        return budgetType === 'revenue' || budgetType === 'both';
    }

    get costSelected() {
        const budgetType = this.categoryControl.value;
        return budgetType === 'cost' || budgetType === 'both';
    }

    get revenueBudgetCtrl() {
        return this.revenueBudgetForm.get('budgets') as FormArray;
    }

    get costBudgetCtrl() {
        return this.costBudgetForm.get('budgets') as FormArray;
    }

    constructor(private fb: FormBuilder,
                private budgetService: BudgetService,
                private auth: AuthService) {
        this.buildFilterForm();
        this.buildRevenueBudgetForm();
        this.buildCostBudgetForm();
        this.buildYearList();
        this.bindSubscribers();
        this.reloadRecords();
    }

    ngOnInit() {
    }

    private buildCostBudgetForm(): any {
        this.costBudgetForm = this.fb.group({
            budgets: this.fb.array([])
        });
    }

    private buildRevenueBudgetForm(): any {
        this.revenueBudgetForm = this.fb.group({
            budgets: this.fb.array([])
        });
    }

    private buildFilterForm() {
        this.filterForm = this.fb.group({
            year: [new Date().getFullYear()],
            category: ['both']
        });
    }

    private buildYearList() {
        for (let i = 2000; i < 2050; i++) {
            this.yearList.push(i);
        }
    }

    @AsyncMethod({
        taskName: 'load_budget_input'
    })
    private reloadRecords() {
        const tasks = [];
        const category = this.filterForm.get('category').value;

        this.isLoading = true;
        if (category === 'both' || category === 'cost') {
            tasks.push(this.reloadCostBudget());
        } else {
            this.removeBudgetRows(this.costBudgetCtrl);
        }

        if (category === 'both' || category === 'revenue') {
            tasks.push(this.reloadRevenueBudget());
        } else {
            this.removeBudgetRows(this.revenueBudgetCtrl);
        }

        return Promise.all(tasks)
            .then(() => this.isLoading = false);
    }

    private reloadCostBudget() {
        const year = this.filterForm.get('year').value;
        const subscribeId = this.auth.currentUserSnapshot.subscriberId;

        return this.budgetService.getCostBudgetBySubscriberIdAndYear(subscribeId, year)
            .then((budgets) => {
                const budgetsCtrl = this.costBudgetCtrl;
                this.removeBudgetRows(budgetsCtrl);
                budgets.forEach(budget => {
                    const budgetRow = this.getCostBudgetRow();
                    budgetRow.setValue(budget);
                    budgetsCtrl.push(budgetRow);
                });
            });
    }

    private reloadRevenueBudget() {
        const year = this.filterForm.get('year').value;
        const subscribeId = this.auth.currentUserSnapshot.subscriberId;

        return this.budgetService.getRevenueBudgetBySubscriberIdAndYear(subscribeId, year)
            .then((budgets) => {
                const budgetsCtrl = this.revenueBudgetCtrl;
                this.removeBudgetRows(budgetsCtrl);
                budgets.forEach(budget => {
                    const budgetRow = this.getRevenueBudgetRow();
                    budgetRow.setValue(budget);
                    budgetsCtrl.push(budgetRow);
                });
            });
    }

    private removeBudgetRows(budgetsCtrl: FormArray) {
        while (budgetsCtrl.controls.length) {
            budgetsCtrl.removeAt(budgetsCtrl.controls.length - 1);
        }
    }

    private getCostBudgetRow() {
        const numberRegEx = Validators.pattern(/\d+/);

        return this.fb.group({
            id: [null],
            status: [null],
            year: [null],
            annual: [null, [numberRegEx]],
            january: [null, [numberRegEx]],
            february: [null, [numberRegEx]],
            march: [null, [numberRegEx]],
            april: [null, [numberRegEx]],
            may: [null, [numberRegEx]],
            june: [null, [numberRegEx]],
            july: [null, [numberRegEx]],
            august: [null, [numberRegEx]],
            september: [null, [numberRegEx]],
            october: [null, [numberRegEx]],
            november: [null, [numberRegEx]],
            december: [null, [numberRegEx]],
            costCode: [null],
            costCategoryName: [null],
            categoryId: [null]
        });
    }

    private getRevenueBudgetRow() {
        const numberRegEx = Validators.pattern(/\d+/);

        return this.fb.group({
            id: [null],
            status: [null],
            year: [null],
            annual: [null, [numberRegEx]],
            january: [null, [numberRegEx]],
            february: [null, [numberRegEx]],
            march: [null, [numberRegEx]],
            april: [null, [numberRegEx]],
            may: [null, [numberRegEx]],
            june: [null, [numberRegEx]],
            july: [null, [numberRegEx]],
            august: [null, [numberRegEx]],
            september: [null, [numberRegEx]],
            october: [null, [numberRegEx]],
            november: [null, [numberRegEx]],
            december: [null, [numberRegEx]],
            revenueCode: [null],
            revenueCategoryName: [null],
            categoryId: [null]
        });
    }

    private bindSubscribers() {
        this.filterForm.valueChanges
            .subscribe(() => this.reloadRecords());
    }

    @AsyncMethod({
        taskName: 'update_budget',
        error: AppError.INPUT_BUDGET_EDIT,
        success: AppSuccess.INPUT_BUDGET_EDIT
    })
    update() {
        const tasks = [];
        this.isLoading = true;
        if (this.costBudgetForm.dirty) {
            tasks.push(this.updateCostBudget());
        }
        if (this.revenueBudgetForm.dirty) {
            tasks.push(this.updateRevenueBudget());
        }
        return Promise.all(tasks)
            .then(() => this.isLoading = false);
    }

    private updateCostBudget() {
        const year = this.yearCtrl.value;
        const subscribeId = this.auth.currentUserSnapshot.subscriberId;

        return this.budgetService.updateCostBudgetBySubscriberIdAndYear(
            subscribeId,
            year,
            this.costBudgetCtrl.value
        );
    }

    private updateRevenueBudget() {
        const year = this.yearCtrl.value;
        const subscribeId = this.auth.currentUserSnapshot.subscriberId;

        return this.budgetService.updateRevenueBudgetBySubscriberIdAndYear(
            subscribeId,
            year,
            this.revenueBudgetCtrl.value
        );
    }

    calculateAnnualBudget(budget: FormGroup) {
        return [
            budget.get('january'), budget.get('february'),
            budget.get('march'), budget.get('april'),
            budget.get('may'), budget.get('june'),
            budget.get('july'), budget.get('august'),
            budget.get('september'), budget.get('october'),
            budget.get('november'), budget.get('december'),
        ].filter(x => !isNaN(+x.value))
            .reduce((current, next) => current + (+next.value), 0);
    }
}
