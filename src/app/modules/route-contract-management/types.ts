export class ContractModel {
    id: number;
    vendorId: number;
    vendorName: string;
    vendorContactId: number;
    vendorContactName: string;
    discounts: ContractDiscount[] = [];
    documentUrl: string;
    vendorContact: any;
    creationTime: Date;

    toJson() {
        return {
            id: this.id,
            vendorId: this.vendorId,
            vendorContactId: this.vendorContactId,
            documentUrl: this.documentUrl,
            discounts: this.discounts.map(x => x.toJson())
        };
    }

    toFormValue() {
        return {
            vendorId: this.vendorId,
            vendorContactId: this.vendorContactId,
            documentUrl: this.documentUrl
        };
    }

    // tslint:disable-next-line:member-ordering
    static fromJson(data: any) {
        const model = new ContractModel;

        model.id = data.id;
        model.documentUrl = data.documentUrl;
        model.creationTime = data.creationTime ? new Date(data.creationTime) : null;
        model.discounts = (data.discounts || []).filter(
            x => x.subscriberRevenueCategory ? x.subscriberRevenueCategory.revenueCode != null : true
        ).map(ContractDiscount.fromJson);

        if (data.vendorContact) {
            model.vendorId = data.vendorContact.vendorId;
            model.vendorName = data.vendorContact.vendorName;
            model.vendorContactId = data.vendorContact.id;
            model.vendorContactName = `${data.vendorContact.firstName} ${data.vendorContact.lastName}`;
            model.vendorContact = data.vendorContact;
        }

        return model;
    }
}

export class ContractDiscount {
    id: number;
    discountTo10: number;
    discountTo20: number;
    discountTo30: number;
    discountTo40: number;
    discountTo50: number;
    discountFrom50: number;
    withoutDiscount: number;
    subscriberRevenueCategoryId: number;
    subscriberRevenueCategoryName: string;

    toJson() {
        return {
            discountTo10: this.discountTo10,
            discountTo20: this.discountTo20,
            discountTo30: this.discountTo30,
            discountTo40: this.discountTo40,
            discountTo50: this.discountTo50,
            discountFrom50: this.discountFrom50,
            withoutDiscount: this.withoutDiscount,
            subscriberRevenueCategoryId: this.subscriberRevenueCategoryId,
        };
    }

    // tslint:disable-next-line:member-ordering
    static fromJson(data: any) {
        const model = new ContractDiscount;

        model.discountTo10 = data.discountTo10;
        model.discountTo20 = data.discountTo20;
        model.discountTo30 = data.discountTo30;
        model.discountTo40 = data.discountTo40;
        model.discountTo50 = data.discountTo50;
        model.discountFrom50 = data.discountFrom50;
        model.withoutDiscount = data.withoutDiscount;

        if (data.subscriberRevenueCategory) {
            model.subscriberRevenueCategoryId = data.subscriberRevenueCategory.id;
            model.subscriberRevenueCategoryName = data.subscriberRevenueCategory.revenueCategoryName;
        }

        if (data.subscriberRevenueCategoryId) {
            model.subscriberRevenueCategoryId = data.subscriberRevenueCategoryId;
        }

        return model;
    }
}
