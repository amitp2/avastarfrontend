import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { VendorContractsComponent } from './component/vendor-contracts/vendor-contracts.component';
import { VendorContractFormComponent } from './component/vendor-contract-form/vendor-contract-form.component';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { DocumentUploader } from './document-uploader';
import { SelectModule } from '../select/select.module';
import { TableResizeModule } from '../table-resize/table-resize.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        ReactiveFormsModule,
        PagingTableModule,
        SelectModule,
        TableResizeModule,
        RouterModule.forChild([{
            path: '',
            component: VendorContractsComponent
        }, {
            path: ':id',
            component: VendorContractFormComponent
        }])
    ],
    declarations: [
        VendorContractsComponent,
        VendorContractFormComponent
    ],
    providers: [
        DocumentUploader
    ]
})
export class RouteContractManagementModule {}
