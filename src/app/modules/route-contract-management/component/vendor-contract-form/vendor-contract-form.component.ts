import { Component, OnInit } from '@angular/core';
import { ContractModel } from '../../types';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { FormMode } from '../../../../enums/form-mode.enum';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute, Router } from '@angular/router';
import { ContractService } from '../../../../services/resources/contract.service';
import { FormHandler } from '../../../../classes/form-handler';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppError } from '../../../../enums/app-error.enum';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { RevenueCategoryResourceService } from '../../../../services/resources/revenue-category-resource.service';
import { AuthService } from '../../../../services/auth.service';
import { DocumentUploader } from '../../document-uploader';
import { FormControlValue } from '../../../../decorators/form-control-value.decorator';
import { VendorContactFetchFactory } from '../../../select/fetches/factories/vendor-contact.fetch.factory';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { UtilsService } from '../../../../services/utils.service';

export interface HiddenColumns {
    discountTo10: boolean;
    discountTo20: boolean;
    discountTo30: boolean;
    discountTo40: boolean;
    discountTo50: boolean;
    discountFrom50: boolean;
    withoutDiscount: boolean;
}

@
    Component({
    selector: 'app-vendor-contract-form',
    templateUrl: './vendor-contract-form.component.html',
    styleUrls: ['./vendor-contract-form.component.styl']
})
export class VendorContractFormComponent extends FormHandler<ContractModel> implements OnInit {
    apiUrl: string;
    hidden: HiddenColumns;
    FormMode: typeof FormMode = FormMode;
    showResetHiddenColumns = true;

    get discounts() {
        return (this.form.get('discounts') as FormArray).controls;
    }

    @FormControlValue('documentUrl')
    documentUrl: string;

    SelectFetchType = SelectFetchType;

    constructor(
        formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private contractApi: ContractService,
        private router: Router,
        private auth: AuthService,
        public documentUploader: DocumentUploader,
        private revenueCategoryApi: RevenueCategoryResourceService,
        public vendorContactFetchFactory: VendorContactFetchFactory,
        private utils: UtilsService
    ) {
        super(formBuilder);

        this.resetHidden();
    }

    ngOnInit() {
        super.ngOnInit();
        if (this.activatedRoute.snapshot.paramMap.get('id') !== 'add') {
            return;
        }
        const subscriberId = this.auth.currentUserSnapshot.subscriberId;
        const httpParams = {
            page: 0,
            size: 999999,
            sortFields: [],
            sortAsc: true
        };
        this.revenueCategoryApi.queryBySubscriberId(httpParams, subscriberId).then((data) => {
            this.setFormValueForAdd(data.items);
        });
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    initializeForm(): FormGroup {
        return this.formBuilder.group({
            vendorId: [null, Validators.required],
            documentUrl: '',
            vendorContactId: [null, Validators.required],
            discounts: this.formBuilder.array([])
        });
    }

    resetHidden() {
        if (!this.showResetHiddenColumns) {
            this.showResetHiddenColumns = true;
            return;
        }
        this.hidden = {
            discountTo10: false,
            discountTo20: false,
            discountTo30: false,
            discountTo40: false,
            discountTo50: false,
            discountFrom50: false,
            withoutDiscount: false
        };
    }

    hideColumn(discount: string) {
        this.hidden[discount] = true;
        this.discounts.forEach(x => x.get(discount).setValue(null));
    }

    getColumnWidth(discount: string) {
        if (this.hidden[discount]) {
            return '0%';
        }
        const visibleColumnCount = Object.keys(this.hidden).filter(x => this.hidden[x] === false).length;
        return (100 - 16) / visibleColumnCount + '%';
    }

    setFormValue = async (model: ContractModel) => {
        const discountRows = this.form.get('discounts') as FormArray;
        while (discountRows.controls.length) {
            discountRows.removeAt(discountRows.controls.length - 1);
        }
        for (const discount of model.discounts) {
            const row = this.formBuilder.group({
                discountTo10: [discount.discountTo10, [
                    Validators.min(0),
                    Validators.max(100)
                ]],
                discountTo20: [discount.discountTo20, [
                    Validators.min(0),
                    Validators.max(100)
                ]],
                discountTo30: [discount.discountTo30, [
                    Validators.min(0),
                    Validators.max(100)
                ]],
                discountTo40: [discount.discountTo40, [
                    Validators.min(0),
                    Validators.max(100)
                ]],
                discountTo50: [discount.discountTo50, [
                    Validators.min(0),
                    Validators.max(100)
                ]],
                discountFrom50: [discount.discountFrom50, [
                    Validators.min(0),
                    Validators.max(100)
                ]],
                withoutDiscount: [discount.withoutDiscount, [
                    Validators.min(0),
                    Validators.max(100)
                ]],
                subscriberRevenueCategoryId: [discount.subscriberRevenueCategoryId],
                subscriberRevenueCategoryName: [discount.subscriberRevenueCategoryName]
            });
            discountRows.push(row);
        }

        this.form.get('vendorId').setValue(model.vendorId);
        await this.utils.resolveAfter(350);
        this.form.get('vendorContactId').setValue(model.vendorContactId);
        this.form.get('vendorId').disable();
        this.resetHidden();

        setTimeout(() => this.form.markAsPristine(), 350);

        return model;
    }

    setFormValueForAdd = (data) => {
        const discountRows = this.form.get('discounts') as FormArray;
        for (const revenue of data.filter(x => x.revenueCode != null)) {
            const row = this.formBuilder.group({
                discountTo10: [null, [
                    Validators.min(0),
                    Validators.max(100)
                ]],
                discountTo20: [null, [
                    Validators.min(0),
                    Validators.max(100)
                ]],
                discountTo30: [null, [
                    Validators.min(0),
                    Validators.max(100)
                ]],
                discountTo40: [null, [
                    Validators.min(0),
                    Validators.max(100)
                ]],
                discountTo50: [null, [
                    Validators.min(0),
                    Validators.max(100)
                ]],
                discountFrom50: [null, [
                    Validators.min(0),
                    Validators.max(100)
                ]],
                withoutDiscount: [null, [
                    Validators.min(0),
                    Validators.max(100)
                ]],
                subscriberRevenueCategoryId: revenue.id,
                subscriberRevenueCategoryName: revenue.revenueCategoryName
            });
            discountRows.push(row);
        }
        this.form.markAsPristine();
        return data;
    }

    @AsyncMethod({
        taskName: 'get_contract'
    })
    get(id: number): Promise<ContractModel> {
        return this.contractApi.get(id).then(this.setFormValue);
    }

    @AsyncMethod({
        taskName: 'add_contract',
        error: AppError.ADD_CONTRACT,
        success: AppSuccess.ADD_CONTRACT
    })
    add(model: ContractModel): Promise<any> {
        return this.contractApi.addByVendorId(model.vendorId, model)
            .then(contact => { this.showResetHiddenColumns = false; return contact; })
            .then(contact => this.router.navigateByUrl(`/u/contracts/${contact.id}`));
    }

    @AsyncMethod({
        taskName: 'edit_contract',
        error: AppError.EDIT_CONTRACT,
        success: AppSuccess.EDIT_CONTRACT
    })
    edit(id: number, model: ContractModel): Promise<ContractModel> {
        return this.contractApi.edit(id, model).then((data) => {
            this.form.markAsPristine();
            return data;
        });
    }

    serialize(model: ContractModel) {
        return model.toFormValue();
    }

    deserialize(formValue: any): ContractModel {
        return ContractModel.fromJson({
            ...formValue,
            vendorContact: {
                id: formValue.vendorContactId,
                vendorId: formValue.vendorId,
            }
        });
    }

    validateRange(event: any) {
        const value = event.target.value;
        if (!value) {
            return;
        }
        const floatValue = parseFloat(value);
        if (floatValue > 100 || floatValue < 0) {
            event.preventDefault();
        }
    }

    cancel() {
        if (this.form.dirty && !confirm('Your changes will be lost. Do you want to continue?')) {
            return;
        }
        this.router.navigateByUrl('/u/contracts');
    }
}
