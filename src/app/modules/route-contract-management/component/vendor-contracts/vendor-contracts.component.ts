import { Component, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ContractModel } from '../../types';
import { TableHandler } from '../../../../classes/table-handler';
import { ContractService } from '../../../../services/resources/contract.service';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { UtilsService } from '../../../../services/utils.service';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { AuthService } from '../../../../services/auth.service';
import { AppError } from '../../../../enums/app-error.enum';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { VendorContactFetchFactory } from '../../../select/fetches/factories/vendor-contact.fetch.factory';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-vendor-contracts',
    templateUrl: './vendor-contracts.component.html',
    styleUrls: ['./vendor-contracts.component.styl']
})
export class VendorContractsComponent extends TableHandler implements OnDestroy {
    filterForm: FormGroup;
    columns: PagingTableHeaderColumn[] = [
        { title: 'Vendor Name', key: 'vendorContact.vendor.name', isSortable: true },
        { title: 'Contact Name', key: 'vendorContact.firstName', isSortable: true },
        { title: 'Contact Phone', key: 'vendorContact.phone', isSortable: true },
        { title: 'Contact Email', key: 'vendorContact.email', isSortable: true },
        { title: 'Contract Date', key: 'createdAt', isSortable: true },
        { title: 'Document', key: 'documentUrl', isSortable: false },
    ];

    SelectFetchType = SelectFetchType;

    @ViewChild(PagingTableComponent) table: PagingTableComponent;
    private filter$: Subscription;

    constructor(
        activatedRoute: ActivatedRoute,
        private fb: FormBuilder,
        private utils: UtilsService,
        private auth: AuthService,
        private contractApi: ContractService,
        public vendorContactFetchFactory: VendorContactFetchFactory
    ) {
        super(activatedRoute);

        this.buildFilterForm();
    }

    private buildFilterForm() {
        this.filterForm = this.fb.group({
            vendorId: '',
            vendorContactId: ''
        });

        this.filter$ = this.filterForm.valueChanges.subscribe(
            () => this.table.refresh()
        );
    }

    getTable(): PagingTableComponent {
        return this.table;
    }

    load = (params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
        const queryParams = this.utils.tableToHttpParams(params);

        queryParams.otherParams = {...this.filterForm.value};

        if (!queryParams.otherParams.vendorId) {
            delete queryParams.otherParams.vendorId;
            delete queryParams.otherParams.vendorContactId;
        }
        if (!queryParams.otherParams.vendorContactId) {
            delete queryParams.otherParams.vendorContactId;
        }
        return this.contractApi.queryBySubscriberId(queryParams, this.auth.currentUserSnapshot.subscriberId);
    }

    @AsyncMethod({
        taskName: 'delete_contract',
        error: AppError.DELETE_EQUIPMENT
    })
    delete(contract: ContractModel) {
        return this.contractApi.delete(contract.id).then(() => {
            this.table.delete((x) => +x.id === +contract.id);
        });
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        if (this.filter$) {
            this.filter$.unsubscribe();
        }
    }
}
