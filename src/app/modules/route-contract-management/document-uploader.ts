import { StandardUploaderV2 } from '../../classes/standard-uploader-v2';
import { AllowedFileType } from '../../enums/allowed-file-type.enum';
import { AppSuccess } from '../../enums/app-success.enum';
import { ConfigService } from '../../services/config.service';
import { AuthService } from '../../services/auth.service';
import { Injectable } from '@angular/core';
import { AppError } from '../../enums/app-error.enum';

@Injectable()
export class DocumentUploader extends StandardUploaderV2 {
    constructor(private config: ConfigService, auth: AuthService) {
        super(auth);
    }

    uploadUrl(): string {
        return `${this.config.config().API_URL}files/vendors/contracts/documents`;
    }

    asyncTaskName(): string {
        return 'upload_contract_document';
    }

    uploadAppSuccess(): AppSuccess {
        return AppSuccess.UPLOAD_CONTRACT_DOCUMENT;
    }

    uploadAppError(): AppError {
        return AppError.UPLOAD_CONTRACT_DOCUMENT;
    }

    allowedFileType(): AllowedFileType {
        return AllowedFileType.All;
    }
}
