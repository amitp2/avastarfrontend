import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FunctionSpaceModel } from '../../models/function-space-model';
import { AsyncMethod } from '../../decorators/async-method.decorator';
import { AbstractInlineForm } from '../../classes/abstract-inline-form';
import { FunctionSpaceStatus } from '../../enums/function-space-status.enum';
import { UtilsService } from '../../services/utils.service';
import { FunctionSpaceResourceService } from '../../services/resources/function-space-resource.service';
import { AppError } from '../../enums/app-error.enum';
import { AppSuccess } from '../../enums/app-success.enum';
import { Injectable } from '@angular/core';

@Injectable()
export class FunctionSpaceInlineForm extends AbstractInlineForm<FunctionSpaceModel> {

    constructor(private utils: UtilsService, private functionSpace: FunctionSpaceResourceService) {
        super();
    }

    buildForm(): FormGroup {
        return new FormGroup({
            name: new FormControl(null, [Validators.required]),
            roomName: new FormControl(null, [Validators.required]),
            status: new FormControl(FunctionSpaceStatus.Active)
        });
    }

    serialize(model: FunctionSpaceModel): any {
        return this.utils.transformInto(model, {
            name: { to: 'name' },
            roomName: { to: 'roomName' },
            status: { to: 'status' }
        });
    }

    deserialize(formValue: any): FunctionSpaceModel {
        return new FunctionSpaceModel(this.utils.transformInto(formValue, {
            name: { to: 'name' },
            roomName: { to: 'roomName' },
            status: { to: 'status' }
        }));
    }

    @AsyncMethod({
        taskName: 'edit_function_space',
        success: AppSuccess.FUNCTION_SPACE_EDIT,
        error: AppError.FUNCTION_SPACE_EDIT
    })
    save(id: number, model: FunctionSpaceModel): Promise<any> {
        return this.functionSpace.edit(id, model);
    }
}

