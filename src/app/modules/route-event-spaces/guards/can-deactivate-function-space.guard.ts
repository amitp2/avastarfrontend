import { FunctionSpacesComponent } from '../components/function-spaces/function-spaces.component';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class CanDeactivateFunctionSpaceGuard implements CanDeactivate<FunctionSpacesComponent> {
    canDeactivate(
        component: FunctionSpacesComponent,
        currentRoute: ActivatedRouteSnapshot,
        currentState: RouterStateSnapshot,
        nextState: RouterStateSnapshot
    ) {
        if (component.form.hasChanges()) {
            return confirm('There are pending changes that have not been saved. Discard changes?');
        }
        return true;
    }
}
