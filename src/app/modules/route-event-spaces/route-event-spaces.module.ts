import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventSpacesComponent } from './components/event-spaces/event-spaces.component';
import { RouterModule, Routes } from '@angular/router';
import { EventSpacesFormComponent } from './components/event-spaces-form/event-spaces-form.component';
import { SharedModule } from '../shared/shared.module';
import { EventSpaceInfoComponent } from './components/event-space-info/event-space-info.component';
import { FunctionSpacesComponent } from './components/function-spaces/function-spaces.component';
import { FunctionSpacesFormComponent } from './components/function-spaces-form/function-spaces-form.component';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AllowOnlyService } from '../../services/guards/allow-only.service';
import { UserRole } from '../../enums/user-role.enum';
import { CanDeactivateFunctionSpaceGuard } from './guards/can-deactivate-function-space.guard';

const routes: Routes = [
    {
        path: '',
        component: EventSpacesComponent,
        canActivate: [AllowOnlyService],
        data: {
            roles: [UserRole.ADMIN, UserRole.TECH_TEAM_LEAD]
        },
        children: [
            {
                path: ':id',
                component: EventSpacesFormComponent,
                children: [
                    {
                        path: 'event-space-info',
                        component: EventSpaceInfoComponent
                    },
                    {
                        path: 'function-spaces',
                        component: FunctionSpacesComponent,
                        pathMatch: 'full',
                        canDeactivate: [CanDeactivateFunctionSpaceGuard]
                    },
                    {
                        path: 'function-spaces/:id',
                        component: FunctionSpacesFormComponent
                    }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        PagingTableModule,
        ReactiveFormsModule
    ],
    declarations: [
        EventSpacesComponent,
        EventSpacesFormComponent,
        EventSpaceInfoComponent,
        FunctionSpacesComponent,
        FunctionSpacesFormComponent
    ],
    providers: [
        CanDeactivateFunctionSpaceGuard
    ]
})
export class RouteEventSpacesModule {
}
