import { Component } from '@angular/core';
import { FunctionSpaceStatus } from '../../../../enums/function-space-status.enum';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute, Router } from '@angular/router';
import { FormHandler } from '../../../../classes/form-handler';
import { FunctionSpaceModel } from '../../../../models/function-space-model';
import { FormMode } from '../../../../enums/form-mode.enum';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FunctionSpaceResourceService } from '../../../../services/resources/function-space-resource.service';
import { UtilsService } from '../../../../services/utils.service';
import { ObjectTransformerType } from '../../../../enums/object-transformer-type.enum';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { Location } from '@angular/common';

@Component({
    selector: 'app-function-spaces-form',
    templateUrl: './function-spaces-form.component.html',
    styleUrls: ['./function-spaces-form.component.styl']
})
export class FunctionSpacesFormComponent extends FormHandler<FunctionSpaceModel> {


    functionSpaceStatus = FunctionSpaceStatus;
    eventSpaceIdObs: Observable<number>;

    constructor(
        private activatedRoute: ActivatedRoute,
        private functionSpace: FunctionSpaceResourceService,
        private utilsService: UtilsService,
        private router: Router,
        private location: Location
    ) {
        super();
        this.eventSpaceIdObs = this.activatedRoute.parent.params.map((params: any) => params.id);
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    initializeForm(): FormGroup {
        return new FormGroup({
            name: new FormControl(null, [Validators.required]),
            roomName: new FormControl(null, [Validators.required]),
            status: new FormControl(FunctionSpaceStatus.Active)
        });
    }

    get(id: number): Promise<FunctionSpaceModel> {
        return this.functionSpace.get(id);
    }

    @AsyncMethod({
        taskName: 'add_function_space',
        success: AppSuccess.FUNCTION_SPACE_ADD,
        error: AppError.FUNCTION_SPACE_ADD
    })
    add(model: FunctionSpaceModel): Promise<any> {
        const eventSpaceId = this.activatedRoute.parent.snapshot.params['id'];
        return this.functionSpace
            .addForEventSpace(model, eventSpaceId)
            .then(() => this.router.navigateByUrl(`/u/event-spaces/${eventSpaceId}/function-spaces`));
    }

    @AsyncMethod({
        taskName: 'edit_function_space',
        success: AppSuccess.FUNCTION_SPACE_EDIT,
        error: AppError.FUNCTION_SPACE_EDIT
    })
    edit(id: number, model: FunctionSpaceModel): Promise<any> {
        const eventSpaceId = this.activatedRoute.parent.snapshot.params['id'];
        return this
            .functionSpace
            .edit(id, model)
            .then(() => this.router.navigateByUrl(`/u/event-spaces/${eventSpaceId}/function-spaces`));
        // .then(() => this.router.navigateByUrl(`/u/event-spaces?fresh=${Date.now()}`));
    }

    serialize(model: FunctionSpaceModel): any {
        return this.utilsService.transformInto(model, {
            name: {
                type: ObjectTransformerType.Default,
                to: 'name'
            },
            roomName: {
                type: ObjectTransformerType.Default,
                to: 'roomName'
            },
            status: {
                type: ObjectTransformerType.Default,
                to: 'status'
            }
        });
    }

    deserialize(formValue: any): FunctionSpaceModel {
        return new FunctionSpaceModel(this.utilsService.transformInto(formValue, {
            name: {
                type: ObjectTransformerType.Default,
                to: 'name'
            },
            roomName: {
                type: ObjectTransformerType.Default,
                to: 'roomName'
            },
            status: {
                type: ObjectTransformerType.Default,
                to: 'status'
            }
        }));
    }

    goBack() {
        this.location.back();
    }

}
