import { Component, OnInit } from '@angular/core';
import { FormMode } from '../../../../enums/form-mode.enum';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-event-spaces-form',
    templateUrl: './event-spaces-form.component.html',
    styleUrls: ['./event-spaces-form.component.styl']
})
export class EventSpacesFormComponent implements OnInit {
    id: string;
    mode: FormMode;
    formMode = FormMode;

    constructor(private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: any) => {
            this.id = params.id;
            if (this.id === 'add') {
                this.mode = FormMode.Add;
                return;
            }
            this.mode = FormMode.Edit;
        });
    }

    linkPrefix(): string {
        return '/u/event-spaces/' + this.id + '/';
    }

}
