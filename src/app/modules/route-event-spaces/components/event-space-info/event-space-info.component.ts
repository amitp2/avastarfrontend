import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormMode } from '../../../../enums/form-mode.enum';
import { FormHandler } from '../../../../classes/form-handler';
import { Observable } from 'rxjs/Observable';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EventSpaceModel } from '../../../../models/event-space-model';
import { EventSpaceType } from '../../../../enums/event-space-type.enum';
import { EventSpaceResourceService } from '../../../../services/resources/event-space-resource.service';
import { UtilsService } from '../../../../services/utils.service';
import { ObjectTransformerType } from '../../../../enums/object-transformer-type.enum';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { CurrentPropertyService } from '../../../../services/current-property.service';

@Component({
    selector: 'app-event-space-info',
    templateUrl: './event-space-info.component.html',
    styleUrls: ['./event-space-info.component.styl']
})
export class EventSpaceInfoComponent extends FormHandler<EventSpaceModel> {

    eventSpaceType = EventSpaceType;

    constructor(private activatedRoute: ActivatedRoute,
                private eventSpace: EventSpaceResourceService,
                private utilsService: UtilsService,
                private currentProperty: CurrentPropertyService,
                private router: Router) {
        super();
    }

    entityId(): Observable<number> {
        return this.activatedRoute
            .parent
            .params
            .map((params: any) => params.id);
    }

    formMode(): Observable<FormMode> {
        return this
            .activatedRoute
            .parent
            .params
            .map((params: any) => params.id === 'add' ? FormMode.Add : FormMode.Edit);
    }

    initializeForm(): FormGroup {
        return new FormGroup({
            name: new FormControl(null, [Validators.required]),
            status: new FormControl(EventSpaceType.Active)
        });
    }

    get(id: number): Promise<EventSpaceModel> {
        return this.eventSpace.get(id);
    }

    @AsyncMethod({
        taskName: 'event_space_add',
        success: AppSuccess.EVENT_SPACE_ADD,
        error: AppError.EVENT_SPACE_ADD
    })
    add(model: EventSpaceModel): Promise<any> {
        return this.eventSpace
            .addForVenue(model, this.currentProperty.snapshot.id)
            .then((added: EventSpaceModel) => {
                // this.router.navigateByUrl(`/u/event-spaces?fresh=${Date.now()}`);
                this.router.navigateByUrl(`/u/event-spaces/${added.id}/event-space-info`);
            });
    }

    @AsyncMethod({
        taskName: 'event_space_edit',
        success: AppSuccess.EVENT_SPACE_EDIT,
        error: AppError.EVENT_SPACE_EDIT
    })
    edit(id: number, model: EventSpaceModel): Promise<any> {
        return this.eventSpace
            .edit(id, model);
            // .then(() => {
            //     this.router.navigateByUrl(`/u/event-spaces?fresh=${Date.now()}`);
            // });
    }

    serialize(model: EventSpaceModel): any {
        return this.utilsService.transformInto(model, {
            name: {
                type: ObjectTransformerType.Default,
                to: 'name'
            },
            status: {
                type: ObjectTransformerType.Default,
                to: 'status'
            }
        });
    }

    deserialize(formValue: any): EventSpaceModel {
        return new EventSpaceModel(this.utilsService.transformInto(formValue, {
            name: {
                type: ObjectTransformerType.Default,
                to: 'name'
            },
            status: {
                type: ObjectTransformerType.Default,
                to: 'status'
            }
        }));
    }

}
