import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { FunctionSpaceResourceService } from '../../../../services/resources/function-space-resource.service';
import { UtilsService } from '../../../../services/utils.service';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { PagingTableService } from '../../../../services/paging-table.service';
import { FunctionSpaceInlineForm } from '../../function-space.inline';
import { FunctionSpaceStatus } from '../../../../enums/function-space-status.enum';
import { Page } from '../../../paging-table/components/paging-table-paging/paging-table-paging.component';

@Component({
    selector: 'app-function-spaces',
    templateUrl: './function-spaces.component.html',
    styleUrls: ['./function-spaces.component.styl'],
    providers: [
        PagingTableService,
        FunctionSpaceInlineForm
    ]
})
export class FunctionSpacesComponent {

    functionSpaceStatus = FunctionSpaceStatus;
    eventSpaceIdObs: Observable<number>;
    @ViewChild('table') table: PagingTableComponent;

    columns: PagingTableHeaderColumn[] = [
        { isSortable: true, title: 'Name', key: 'name' },
        { isSortable: true, title: 'Status', key: 'status' },
        { isSortable: false, title: 'Actions', key: 'actions' }
    ];

    constructor(
        private activatedRoute: ActivatedRoute,
        private functionSpace: FunctionSpaceResourceService,
        private utilsService: UtilsService,
        public form: FunctionSpaceInlineForm
    ) {
        this.eventSpaceIdObs = this.activatedRoute.parent.params.map((params: any) => params.id);
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return this.functionSpace.queryByEventSpace(
                this.utilsService.tableToHttpParams(params),
                this.activatedRoute.parent.snapshot.params['id']
            );
        }).bind(this);
    }

    @AsyncMethod({
        taskName: 'function_space_delete',
        success: AppSuccess.FUNCTION_SPACE_DELETE,
        error: AppError.FUNCTION_SPACE_DELETE
    })
    delete(model: any): Promise<void> {
        return this.functionSpace.delete(model.id).then(() => {
            this.table.refresh();
        });
    }

    async submit(id: number) {
        await this.form.submit(id);
        this.table.refresh();
    }

    canNavigate = (page: Page) => {
        if (this.form.hasChanges()) {
            if (confirm('There are pending changes that have not been saved. Discard changes?')) {
                this.form.clear();
                return true;
            }
        }
        this.form.clear();
        return true;
    }
}
