import { Component, ViewChild } from '@angular/core';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { EventSpaceResourceService } from '../../../../services/resources/event-space-resource.service';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { UtilsService } from '../../../../services/utils.service';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { PagingTableService } from '../../../../services/paging-table.service';
import { CurrentPropertyService } from '../../../../services/current-property.service';
import { VenueModel } from '../../../../models/venue-model';

import 'rxjs/add/operator/take';
import { EventSpaceModel } from '../../../../models/event-space-model';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { FunctionSpaceResourceService } from '../../../../services/resources/function-space-resource.service';
import { TableHandler } from '../../../../classes/table-handler';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-event-spaces',
    templateUrl: './event-spaces.component.html',
    styleUrls: ['./event-spaces.component.styl'],
    providers: [PagingTableService]
})
export class EventSpacesComponent extends TableHandler {

    @ViewChild('table') table: PagingTableComponent;

    columns: PagingTableHeaderColumn[] = [
        {isSortable: true, title: 'Event Space | Function Room', key: 'name'},
        {isSortable: true, title: 'Status', key: 'status'},
        {isSortable: false, title: 'Actions', key: 'actions'}
    ];

    constructor(private eventSpace: EventSpaceResourceService,
                private utilsService: UtilsService,
                private currentProperty: CurrentPropertyService,
                private functionSpace: FunctionSpaceResourceService,
                activatedRoute: ActivatedRoute) {
        super(activatedRoute);
        // TODO: refresh table on venue change
    }

    getTable(): PagingTableComponent {
        return this.table;
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return new Promise((resolve, reject) => {
                let subs;
                subs = this.currentProperty
                    .currentVenue
                    .filter(venue => venue.id ? true : false)
                    .subscribe((venue: VenueModel) => {

                        this.eventSpace
                            .queryByVenueId(this.utilsService.tableToHttpParams(params), venue.id)
                            .then(res => {
                                let transformed;
                                transformed = res.items.reduce((acc: any[], cur: any) => {
                                    acc.push(cur);
                                    if (cur.functionSpaces) {
                                        cur.functionSpaces.forEach((functionSpace: any, i: number) => {
                                            acc.push({
                                                ...functionSpace,
                                                isFunctionSpace: true,
                                                isLast: cur.functionSpaces.length - 1 === i,
                                                parentId: cur.id
                                            });
                                        });
                                    }
                                    return acc;
                                }, []);
                                res.items = transformed;
                                resolve(res);
                                subs.unsubscribe();
                            })
                            .catch(() => {
                                reject();
                                subs.unsubscribe();
                            });

                    }, () => reject());
            });
        }).bind(this);
    }

    @AsyncMethod({
        taskName: 'event-space-delete',
        success: AppSuccess.EVENT_SPACE_DELETE
    })
    delete(model: EventSpaceModel): Promise<void> {

        if (model.functionSpaces && model.functionSpaces.length) {
            return Promise.reject(AppError.EVENT_SPACE_DELETE_WITH_FUNCTION_SPACES);
        }

        return this.eventSpace.delete(model.id).then(() => {
            this.table.refresh();
        }).catch(() => Promise.reject(AppError.EVENT_SPACE_DELETE));
    }

    @AsyncMethod({
        taskName: 'function_space_delete',
        success: AppSuccess.FUNCTION_SPACE_DELETE,
        error: AppError.FUNCTION_SPACE_DELETE
    })
    deleteFunctionSpace(model: any): Promise<void> {
        return this.functionSpace.delete(model.id).then(() => {
            this.table.refresh();
        });
    }

}
