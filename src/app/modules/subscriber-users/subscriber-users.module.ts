import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { SubscriberUsersListComponent } from './components/subscriber-users-list/subscriber-users-list.component';
import { SubscriberUsersEntityComponent } from './components/subscriber-users-entity/subscriber-users-entity.component';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { SelectModule } from '../select/select.module';
import { MaskedInputModule } from '../masked-input/masked-input.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        PagingTableModule,
        RouterModule,
        ReactiveFormsModule,
        SelectModule,
        MaskedInputModule
    ],
    declarations: [
        SubscriberUsersListComponent,
        SubscriberUsersEntityComponent
    ],
    exports: [
        SubscriberUsersListComponent,
        SubscriberUsersEntityComponent
    ]
})
export class SubscriberUsersModule {
}
