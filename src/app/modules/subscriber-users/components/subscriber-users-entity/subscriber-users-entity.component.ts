import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { UserRole } from '../../../../enums/user-role.enum';
import { UserStatus } from '../../../../enums/user-status.enum';
import { FormMode } from '../../../../enums/form-mode.enum';
import { UserResourceService } from '../../../../services/resources/user-resource.service';
import { UserModel } from '../../../../models/user-model';
import { SubscriberResourceService } from '../../../../services/resources/subscriber-resource.service';
import { ResourceQueryResponse } from '../../../../interfaces/resource-query-response';
import { SubscriberModel } from '../../../../models/subscriber-model';
import { AsyncTasksService } from '../../../../services/async-tasks.service';
import { AppError } from '../../../../enums/app-error.enum';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AuthService } from '../../../../services/auth.service';
import { CurrentUser } from '../../../../interfaces/current-user';
import { UtilsService } from '../../../../services/utils.service';
import { MultiSelectItem } from '../../../shared/components/form-controls/multi-select/multi-select.component';
import { SelectFetchType } from '../../../select/types/select-fetch-type';

@Component({
    selector: 'app-subscriber-users-entity',
    templateUrl: './subscriber-users-entity.component.html',
    styleUrls: ['./subscriber-users-entity.component.styl']
})
export class SubscriberUsersEntityComponent implements OnInit {

    @Input() cancelLink: string;
    @Input() userIdObs: Observable<number>;
    @Input() subscriberId: number;
    @Output() added: EventEmitter<UserModel>;
    @Output() updated: EventEmitter<UserModel>;

    subscribers: SubscriberModel[];
    form: FormGroup;
    formMode: FormMode;
    userRole = UserRole;
    status = UserStatus;
    mode = FormMode;
    id: number;
    rolesSelection: MultiSelectItem[];
    SelectFetchType = SelectFetchType;
    private submitting: boolean;


    get f() { return this.form.controls; }
    
    constructor(
        private userResourceService: UserResourceService,
        private subscriberResourceService: SubscriberResourceService,
        public asyncTasksService: AsyncTasksService,
        private authService: AuthService,
        private utilsService: UtilsService
    ) {
        this.formMode = FormMode.Add;
        this.added = new EventEmitter<UserModel>();

        this.updated = new EventEmitter<UserModel>();

        this.form = new FormGroup({
            firstName: new FormControl(null, [Validators.required]),
            lastName: new FormControl(null, [Validators.required]),
            status: new FormControl(UserStatus.Active),
            title: new FormControl(),
            username: new FormControl(null, [Validators.required, Validators.email]),
            mobile: new FormControl(),
            phone: new FormControl(null, [Validators.required]),
            roles: new FormControl([UserRole.ADMIN], [Validators.required]),
            notes: new FormControl(),
            fax: new FormControl(),
            subscriberId: new FormControl()
        });

       

        this.authService.currentUser().then((user: CurrentUser) => {
            if (this.utilsService.userHasRole(user, UserRole.SUPER_ADMIN)) {
                this.subscriberResourceService.query({
                    page: 0,
                    size: 1000,
                    sortFields: ['name'],
                    sortAsc: true
                }).then((resp: ResourceQueryResponse<SubscriberModel>) => {
                    this.subscribers = resp.items;
                    if (this.formMode === FormMode.Add && resp.items.length > 0) {
                        this.form.get('subscriberId').setValue(resp.items[0].id);
                    }
                });
            } else {
                this.form.get('subscriberId').setValue(user.subscriberId);
            }

        });

        this.rolesSelection = [
            { id: UserRole.SUPER_ADMIN, text: this.utilsService.userRoleToDisplayString(UserRole.SUPER_ADMIN), disabled: true },
            { id: UserRole.ADMIN, text: this.utilsService.userRoleToDisplayString(UserRole.ADMIN) },
            { id: UserRole.TECH_TEAM_LEAD, text: this.utilsService.userRoleToDisplayString(UserRole.TECH_TEAM_LEAD) },
            { id: UserRole.TECH_TEAM, text: this.utilsService.userRoleToDisplayString(UserRole.TECH_TEAM) },
            { id: UserRole.EVENT_PLANNER, text: this.utilsService.userRoleToDisplayString(UserRole.EVENT_PLANNER) },
            { id: UserRole.SERVICE_TEAM, text: this.utilsService.userRoleToDisplayString(UserRole.SERVICE_TEAM) },
        ];

    }

    serialize(model: UserModel): any {
        return undefined;
    }

    deserialize(formValue: any): UserModel {
        const newVal = {
            ...formValue,
            status: formValue.status,
            subscriberId: this.subscriberId ? this.subscriberId : this.form.get('subscriberId').value
        };
        return new UserModel(newVal);
    }

    submit() {
        if (this.submitting) {
            return;
        }
        this.submitting = true;

        const model = this.deserialize(this.form.value);

        this.submitting = true;

        if (this.formMode === FormMode.Add) {
            this.add(model).then(() => this.submitting = false);
            return;
        }
        this.update(model).then(() => this.submitting = false);
    }

    @AsyncMethod({
        taskName: 'subscriber_users_add',
        success: AppSuccess.SUBSCRIBER_USER_ADD,
        // error: AppError.SUBSCRIBER_USER_ADD
    })
    add(model: UserModel) {
        return this.userResourceService.add(model)
            .then((user: UserModel) => this.added.emit(user));
    }

    @AsyncMethod({
        taskName: 'subscriber_users_update',
        success: AppSuccess.SUBSCRIBER_USER_UPDATE,
        // error: AppError.SUBSCRIBER_USER_UPDATE
    })
    update(model: UserModel) {
        return this.userResourceService.edit(this.id, model)
            .then((user: UserModel) => this.updated.emit(user));
    }

    ngOnInit() {
        this.userIdObs.subscribe((id: any) => {
            if (id === 'add') {
                this.formMode = FormMode.Add;
                this.id = id;
                return;
            }
            this.formMode = FormMode.Edit;
            this.form.get('username').disable();
            this.form.get('subscriberId').disable();
            this.id = +id;
            this.userResourceService.get(this.id).then((user: UserModel) => {
                const formValue: any = {
                    ...user
                    // status: user.status === UserStatus.Active ? true : false
                };
                delete formValue.subscriberName;
                delete formValue.id;
                this.form.patchValue(formValue);
            });
        });
    }

}
