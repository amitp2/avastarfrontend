import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscriberUsersEntityComponent } from './subscriber-users-entity.component';

describe('SubscriberUsersEntityComponent', () => {
    let component: SubscriberUsersEntityComponent;
    let fixture: ComponentFixture<SubscriberUsersEntityComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SubscriberUsersEntityComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SubscriberUsersEntityComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    // it('should create', () => {
    //     expect(component).toBeTruthy();
    // });
});
