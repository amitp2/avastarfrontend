import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { UserResourceService } from '../../../../services/resources/user-resource.service';
import { UtilsService } from '../../../../services/utils.service';
import { UserModel } from '../../../../models/user-model';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { AuthService } from '../../../../services/auth.service';
import { CurrentUser } from '../../../../interfaces/current-user';
import { PagingTableService } from '../../../../services/paging-table.service';
import { ProductType } from '../../../../enums/product-type.enum';

@Component({
    selector: 'app-subscriber-users-list',
    templateUrl: './subscriber-users-list.component.html',
    styleUrls: ['./subscriber-users-list.component.styl'],
    providers: [PagingTableService]
})
export class SubscriberUsersListComponent implements OnInit {

    @Input() formLink: string;
    @Input() bySubscriberId: number;
    @Input() showCompany: boolean;
    @Output() editRequired: EventEmitter<UserModel>;
    @ViewChild('table') table: PagingTableComponent;
    currentUser: CurrentUser;

    columns: PagingTableHeaderColumn[] = [
        {isSortable: false, title: 'Actions', key: 'actions'},
        {isSortable: true, title: 'Contact', key: 'firstName'},
        {isSortable: true, title: 'Direct Phone', key: 'phone'},
        {isSortable: true, title: 'Mobile Phone', key: 'mobile'},
        {isSortable: true, title: 'Email', key: 'username'},
        {isSortable: true, title: 'Role', key: 'role'}
    ];

    subscriberIdObs: Observable<any>;

    constructor(private activatedRoute: ActivatedRoute,
                private userResourceService: UserResourceService,
                private utilsService: UtilsService,
                private authService: AuthService) {
        this.editRequired = new EventEmitter<UserModel>();
        this.subscriberIdObs = this.activatedRoute.parent.params.map(params => params.id);

        this.authService.currentUser().then(user => this.currentUser = user);
    }

    ngOnInit() {
        if (this.showCompany) {
            this.columns = [
                {isSortable: false, title: 'Actions', key: 'actions'},
                {isSortable: true, title: 'Contact', key: 'firstName'},
                {isSortable: true, title: 'Subscriber', key: 'subscriberName'},
                {isSortable: true, title: 'Direct Phone', key: 'phone'},
                {isSortable: true, title: 'Mobile Phone', key: 'mobile'},
                {isSortable: true, title: 'Email', key: 'username'},
                {isSortable: true, title: 'Role', key: 'role'}
            ];
        }
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            if (this.bySubscriberId) {
                return this.userResourceService.bySubscriber(
                    this.bySubscriberId,
                    this.utilsService.tableToHttpParams(params)
                );
            }
            return this.userResourceService.query(this.utilsService.tableToHttpParams(params));
        }).bind(this);
    }

    async delete(user: UserModel) {
        await this.userResourceService.delete(user.id);
        await this.userResourceService.releaseProducts(user);
        this.table.delete((u) => +u.id === +user.id);
    }
}
