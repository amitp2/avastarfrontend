import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagingTableComponent } from './components/paging-table/paging-table.component';
import { PagingTableHeaderColumnComponent } from './components/paging-table-header-column/paging-table-header-column.component';
import { PagingTableItemsPerPageComponent } from './components/paging-table-items-per-page/paging-table-items-per-page.component';
import { PagingTablePagingComponent } from './components/paging-table-paging/paging-table-paging.component';
import { PagingTableSearchComponent } from './components/paging-table-search/paging-table-search.component';
import { PagingTableRowDirective } from './directives/paging-table-row.directive';
import { PagingTableService } from '../../services/paging-table.service';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        SharedModule
    ],
    declarations: [
        PagingTableComponent,
        PagingTableHeaderColumnComponent,
        PagingTableItemsPerPageComponent,
        PagingTablePagingComponent,
        PagingTableSearchComponent,
        PagingTableRowDirective
    ],
    providers: [PagingTableService],
    exports: [
        PagingTableComponent,
        PagingTableSearchComponent,
        PagingTableRowDirective,
        PagingTableItemsPerPageComponent,
        PagingTablePagingComponent,
        PagingTableHeaderColumnComponent
    ]
})
export class PagingTableModule {
}
