import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { PagingTableSearchComponent } from './paging-table-search.component';
import {
    PagingTableEventType, PagingTableSearchChangedPayload,
    PagingTableService
} from '../../../../services/paging-table.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('PagingTableSearchComponent', () => {
    let component: PagingTableSearchComponent;
    let fixture: ComponentFixture<PagingTableSearchComponent>;
    let pagingTableService: PagingTableService;


    function setInputValue(select: DebugElement, value: any) {
        select.nativeElement.value = value;
        const evt = document.createEvent('HTMLEvents');
        evt.initEvent('input', false, true);
        select.nativeElement.dispatchEvent(evt);
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                PagingTableSearchComponent
            ],
            imports: [
                FormsModule,
                ReactiveFormsModule
            ],
            providers: [PagingTableService]
        }).compileComponents();

        pagingTableService = TestBed.get(PagingTableService);
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PagingTableSearchComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should fire SEARCH_CHANGED when user types in some input', fakeAsync(() => {
        let res;
        pagingTableService.on(PagingTableEventType.SEARCH_CHANGED).subscribe((event) => res = event.payload);
        const search: DebugElement = fixture.debugElement.query(By.css('.search'));
        setInputValue(search, '123');
        fixture.detectChanges();
        tick();
        expect(res).toEqual(new PagingTableSearchChangedPayload('123'));
    }));

    it('should fire empty string once initialized', fakeAsync(() => {
        let res;
        pagingTableService.on(PagingTableEventType.SEARCH_CHANGED).subscribe((event) => res = event.payload);
        fixture.detectChanges();
        tick();
        expect(res).toEqual(new PagingTableSearchChangedPayload(''));
    }));

    it('should put the given placeholder text as search input placeholder', () => {
        component.placeholder = 'Some Search Text';
        fixture.detectChanges();
        const search = fixture.debugElement.query(By.css('.search'));
        expect(search.nativeElement.getAttribute('placeholder')).toEqual('Some Search Text');
    });
});
