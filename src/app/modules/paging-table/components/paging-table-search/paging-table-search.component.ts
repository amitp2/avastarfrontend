import { Component, Input, OnDestroy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { PagingTableService } from '../../../../services/paging-table.service';

@Component({
    selector: 'app-paging-table-search',
    templateUrl: './paging-table-search.component.html',
    styleUrls: ['./paging-table-search.component.styl']
})
export class PagingTableSearchComponent implements OnDestroy {

    @Input() placeholder: string;
    searchForm: FormGroup;
    searchFormSubscription: Subscription;

    constructor(private pagingTableService: PagingTableService) {
        this.pagingTableService.fireSearchChanged('');
        this.searchForm = new FormGroup({
            search: new FormControl()
        });
        this.searchFormSubscription = this.searchForm.valueChanges.subscribe(val => {
            this.pagingTableService.fireSearchChanged(val.search);
        });
    }

    ngOnDestroy() {
        this.searchFormSubscription.unsubscribe();
    }

}
