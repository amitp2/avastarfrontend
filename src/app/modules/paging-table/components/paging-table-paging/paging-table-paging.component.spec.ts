import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { PagingTablePagingComponent } from './paging-table-paging.component';
import {
    PagingTableEventType, PagingTablePageChangedPayload,
    PagingTableService
} from '../../../../services/paging-table.service';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import set = Reflect.set;

describe('PagingTablePagingComponent', () => {
    let component: PagingTablePagingComponent;
    let fixture: ComponentFixture<PagingTablePagingComponent>;
    let pagingTableService: PagingTableService;
    const prev = '«';
    const next = '»';

    function clickOnPage(index: number): void {
        const page: DebugElement = fixture.debugElement.query(By.css(`.page:nth-child(${index})`));

        page.triggerEventHandler('click', null);
        fixture.detectChanges();
    }

    function textOfPage(index: number): string {
        const page: DebugElement = fixture.debugElement.query(By.css(`.page:nth-child(${index})`));
        return page.nativeElement.textContent.trim();
    }

    function textOfAllPages(): string[] {
        const pages: DebugElement[] = fixture.debugElement.queryAll(By.css('.page'));
        const texts = pages.map((page: DebugElement) => page.nativeElement.textContent.trim());
        return texts;
    }

    function activePage(): DebugElement {
        return fixture.debugElement.query(By.css('.page.-current'));
    }

    function previousPage(): DebugElement {
        return fixture.debugElement.query(By.css('.page.-back'));
    }

    function nextPage(): DebugElement {
        return fixture.debugElement.query(By.css('.page.-forward'));
    }

    function activePageText(): string {
        return activePage().nativeElement.textContent.trim();
    }

    function activePageIndex(): number {
        return +activePage().nativeElement.textContent.trim() + 1;
    }

    function set20Pages() {
        pagingTableService.fireItemsPerPageChanged(1);
        pagingTableService.firePageLoaded(20, []);
        fixture.detectChanges();
    }

    function setPagesCount(count: number) {
        pagingTableService.fireItemsPerPageChanged(1);
        pagingTableService.firePageLoaded(count, []);
        fixture.detectChanges();
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PagingTablePagingComponent],
            providers: [PagingTableService]
        }).compileComponents();

        pagingTableService = TestBed.get(PagingTableService);
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PagingTablePagingComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should determine correctly how many pages exist, on PAGE_LOADED event, considering items-per-page. by setting last page number correctly', () => {

        let itemsPerPage;
        let total;

        let lastPage: DebugElement;

        itemsPerPage = 5;
        total = 100;
        pagingTableService.fireItemsPerPageChanged(itemsPerPage);
        pagingTableService.firePageLoaded(total, []);
        fixture.detectChanges();
        lastPage = fixture.debugElement.query(By.css('.last-page'));
        expect(lastPage.nativeElement.textContent.trim()).toEqual(`${total / itemsPerPage}`);


        itemsPerPage = 5;
        total = 102;
        pagingTableService.fireItemsPerPageChanged(itemsPerPage);
        pagingTableService.firePageLoaded(total, []);
        fixture.detectChanges();
        lastPage = fixture.debugElement.query(By.css('.last-page'));
        expect(lastPage.nativeElement.textContent.trim()).toEqual(`${Math.ceil(total / itemsPerPage)}`);
    });

    it('should send PAGE_CHANGED event with page number 1 once created', fakeAsync(() => {
        let res;
        pagingTableService.on(PagingTableEventType.PAGE_CHANGED).subscribe(event => res = event.payload);
        tick();
        expect(res).toEqual(new PagingTablePageChangedPayload(1));
    }));

    it('should send PAGE_CHANGED event with necessary page number when user clicks on it', fakeAsync(() => {

        pagingTableService.fireItemsPerPageChanged(1);
        pagingTableService.firePageLoaded(4, []);
        fixture.detectChanges();

        let res;
        clickOnPage(3);
        fixture.detectChanges();
        const pageNumber = textOfPage(3);

        tick();
        pagingTableService.on(PagingTableEventType.PAGE_CHANGED).subscribe(event => res = event.payload);
        tick();
        expect(res).toEqual(new PagingTablePageChangedPayload(+pageNumber));
    }));

    it('should mark clicked page with active class', () => {
        pagingTableService.fireItemsPerPageChanged(1);
        pagingTableService.firePageLoaded(2, []);
        fixture.detectChanges();

        clickOnPage(2);
        fixture.detectChanges();
        expect(textOfPage(activePageIndex())).toEqual('1');

        clickOnPage(3);
        fixture.detectChanges();
        expect(textOfPage(activePageIndex())).toEqual('2');
    });

    it('should draw (< 1,2,3,4 >) without ... (dots) if total pages is less than 4', () => {
        setPagesCount(4);
        expect(textOfAllPages()).toEqual([prev, '1', '2', '3', '4', next]);
    });

    it('should draw nothing if total pages is 1', () => {
        setPagesCount(1);
        // expect(textOfAllPages()).toEqual([prev, '1', next]);
        expect(textOfAllPages()).toEqual([]);
    });

    it('should draw (<1,2 ... N>) with dots if total pages is more than 4 and current page is 1', () => {
        set20Pages();
        clickOnPage(2);
        expect(textOfAllPages()).toEqual([prev, '1', '2', '...', '20', next]);
    });

    it('should draw (<1,2,3 ... N>) with dots if total pages is more than 4 and current page is 2', () => {
        set20Pages();
        clickOnPage(3);
        expect(textOfAllPages()).toEqual([prev, '1', '2', '3', '...', '20', next]);
    });

    it('should draw (<1,2,3,4 ... N>) with dots if total pages is more than 4 and current page is 3', () => {
        set20Pages();
        clickOnPage(3);
        clickOnPage(4);
        expect(textOfAllPages()).toEqual([prev, '1', '2', '3', '4', '...', '20', next]);
    });

    it('should draw (<1, ... N-1, N>) with dots if total pages is more than 4 and current page is N', () => {
        set20Pages();
        clickOnPage(5);
        expect(textOfAllPages()).toEqual([prev, '1', '...', '19', '20', next]);
    });

    it('should draw (<1, ... N-2, N-1, N>) with dots if total pages is more than 4 and current page is N - 1', () => {
        set20Pages();
        clickOnPage(5);
        clickOnPage(4);
        expect(textOfAllPages()).toEqual([prev, '1', '...', '18', '19', '20', next]);
    });

    it('should draw (<1, ... N-3, N-2, N-1, N>) with dots if total pages is more than 4 and current page is N - 2', () => {
        set20Pages();
        clickOnPage(5);
        clickOnPage(4);
        clickOnPage(4);
        expect(textOfAllPages()).toEqual([prev, '1', '...', '17', '18', '19', '20', next]);
    });

    it('should draw (<1, ... , K-1, K, K+1, ... N>) with dots if total pages is more than 4 and current page is somewhere in between', () => {
        set20Pages();
        clickOnPage(2);
        clickOnPage(3);
        clickOnPage(4);
        clickOnPage(5);
        expect(textOfAllPages()).toEqual([prev, '1', '...', '3', '4', '5', '...', '20', next]);
    });

    it('only if active page is 1 then previous button should has disabled class', () => {
        set20Pages();
        clickOnPage(2);
        expect(previousPage().nativeElement.className.indexOf('disabled')).not.toEqual(-1);
        clickOnPage(3);
        expect(previousPage().nativeElement.className.indexOf('disabled')).toEqual(-1);
    });

    it('only if active page is N then next button should have class disabled', () => {
        set20Pages();
        clickOnPage(5);
        expect(nextPage().nativeElement.className.indexOf('disabled')).not.toEqual(-1);
        clickOnPage(4);
        expect(nextPage().nativeElement.className.indexOf('disabled')).toEqual(-1);
    });

    it('next button should move to the next page if active page is not N', fakeAsync(() => {
        fixture.detectChanges();
        set20Pages();
        tick();
        fixture.detectChanges();

        clickOnPage(5);
        tick();
        fixture.detectChanges();

        clickOnPage(4);
        tick();
        fixture.detectChanges();
        expect(activePageText()).toEqual('19');

        clickOnPage(7);
        tick();
        fixture.detectChanges();
        expect(activePageText()).toEqual('20');

        clickOnPage(6);
        tick();
        fixture.detectChanges();
        expect(activePageText()).toEqual('20');
    }));

    it('prev button should move to the previous page if active page is not 1', () => {
        set20Pages();
        clickOnPage(2);
        clickOnPage(3);
        expect(activePageText()).toEqual('2');
        clickOnPage(1);
        expect(activePageText()).toEqual('1');
        clickOnPage(1);
        expect(activePageText()).toEqual('1');
    });


    it('if ITEM_DELETED event happens, it should remove the last page if are no remaining elements', fakeAsync(() => {
        pagingTableService.fireItemsPerPageChanged(10);
        pagingTableService.firePageLoaded(22, []);

        fixture.detectChanges();
        expect(textOfAllPages()).toEqual(['«', '1', '2', '3', '»']);

        pagingTableService.fireItemDeleted({item: 'some item'});
        tick();
        fixture.detectChanges();
        expect(textOfAllPages()).toEqual(['«', '1', '2', '3', '»']);

        pagingTableService.fireItemDeleted({item: 'some item'});
        tick();
        fixture.detectChanges();
        expect(textOfAllPages()).toEqual(['«', '1', '2', '»']);
    }));

    it('if ITEM_DELETED event happens and last page is deleted, then active page should become 1 and fire page changed event', fakeAsync(() => {
        let res;
        pagingTableService.on(PagingTableEventType.PAGE_CHANGED).subscribe(resp => res = resp.payload);
        pagingTableService.fireItemsPerPageChanged(10);
        pagingTableService.firePageLoaded(21, []);
        fixture.detectChanges();
        clickOnPage(4);
        expect(activePageText()).toEqual('3');
        pagingTableService.fireItemDeleted({item: 'some item'});
        tick();
        fixture.detectChanges();
        expect(activePageText()).toEqual('1');
        expect(res).toEqual(new PagingTablePageChangedPayload(1));
    }));
});
