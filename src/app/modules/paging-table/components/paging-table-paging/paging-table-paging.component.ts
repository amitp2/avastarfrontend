import { Component, Input, OnDestroy } from '@angular/core';
import {
    PagingTableEventType,
    PagingTableItemsPerPageChangedPayload,
    PagingTablePageLoadedPayload,
    PagingTableService
} from '../../../../services/paging-table.service';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

interface CombinedEvents {
    itemsPerPageChanged: PagingTableItemsPerPageChangedPayload;
    pageLoaded: PagingTablePageLoadedPayload;
}

export interface Page {
    disabled: boolean;
    text: string;
    isLast: boolean;
    isPrevious: boolean;
    isNext: boolean;
}

@Component({
    selector: 'app-paging-table-paging',
    templateUrl: './paging-table-paging.component.html',
    styleUrls: ['./paging-table-paging.component.styl']
})
export class PagingTablePagingComponent implements OnDestroy {

    private combined: Observable<any>;

    private totalPagesSubscription: Subscription;
    private itemDeletedSubscription: Subscription;
    private searchSubscription: Subscription;
    private pageChangedSubscription: Subscription;

    totalPages: number;
    activePage: number;
    itemsPerPage: number;
    totalItems: number;
    pagesList: Array<Page>;

    @Input() canNavigate: Function;

    constructor(private pagingTableService: PagingTableService) {

        this.activePage = 1;
        this.totalPages = 1;
        this.pagingTableService.firePageChanged(this.activePage);
        this.pagesList = this.pages();

        this.combined = pagingTableService
            .on(PagingTableEventType.ITEMS_PER_PAGE_CHANGED)
            .combineLatest(pagingTableService.on(PagingTableEventType.PAGE_LOADED))
            .map((arr: Array<any>) => ({
                itemsPerPageChanged: arr[0].payload,
                pageLoaded: arr[1].payload
            }));

        this.totalPagesSubscription = this.combined
            .subscribe((events: CombinedEvents) => {
                this.itemsPerPage = events.itemsPerPageChanged.itemsPerPage;
                this.totalItems = events.pageLoaded.total;
                this.totalPages = Math.ceil(events.pageLoaded.total / events.itemsPerPageChanged.itemsPerPage);
                this.pagesList = this.pages();
            });

        this.itemDeletedSubscription = this.pagingTableService
            .on(PagingTableEventType.ITEM_DELETED)
            .subscribe(() => {
                this.totalItems--;
                if (this.totalItems % this.itemsPerPage === 0) {
                    this.totalPages--;
                    this.activePage = 1;
                    this.pagingTableService.firePageChanged(1);
                }
                this.pagesList = this.pages();
            });

        this.searchSubscription = this.pagingTableService
            .on(PagingTableEventType.SEARCH_CHANGED)
            .subscribe(() => {
                this.activePage = 1;
                this.pagingTableService.firePageChanged(1);
            });

        this.pageChangedSubscription = this.pagingTableService
            .on(PagingTableEventType.PAGE_CHANGED)
            .subscribe(val => this.activePage = val.payload.page);
    }


    ngOnDestroy() {
        this.itemDeletedSubscription.unsubscribe();
        this.totalPagesSubscription.unsubscribe();
        this.searchSubscription.unsubscribe();
    }

    private prevPage(disabled: boolean = false): Page {
        return {
            disabled,
            text: '&laquo;',
            isLast: false,
            isPrevious: true,
            isNext: false
        };
    }

    private nextPage(disabled: boolean = false): Page {
        return {
            disabled,
            text: '&raquo',
            isLast: false,
            isPrevious: false,
            isNext: true
        };
    }

    private page(text: string = '...', isLast: boolean = false, disabled: boolean = false) {
        return {
            disabled,
            text,
            isLast,
            isPrevious: false,
            isNext: false
        };
    }

    private lastPage(): Page {
        return this.page(`${this.totalPages}`, true);
    }

    private dotsPage(): Page {
        return this.page('...');
    }

    pages(): Array<Page> {
        const pagesList: Page[] = [];


        pagesList.push(this.prevPage());

        if (this.totalPages > 1) {
            pagesList.push(this.page('1'));
        }

        if (this.totalPages <= 4) {
            for (let i = 2; i <= this.totalPages - 1; i++) {
                pagesList.push(this.page(`${i}`, i === this.totalPages));
            }
        } else if (this.activePage <= 3) {

            pagesList.push(this.page('2'));
            if (this.activePage === 2 || this.activePage === 3) {
                pagesList.push(this.page('3'));
            }
            if (this.activePage === 3) {
                pagesList.push(this.page('4'));
            }
            pagesList.push(this.dotsPage());

        } else if (this.activePage >= this.totalPages - 2) {

            pagesList.push(this.dotsPage());
            if (this.activePage === this.totalPages - 2) {
                pagesList.push(this.page(`${this.totalPages - 3}`));
            }
            if (this.activePage === this.totalPages - 1 || this.activePage === this.totalPages - 2) {
                pagesList.push(this.page(`${this.totalPages - 2}`));
            }
            pagesList.push(this.page(`${this.totalPages - 1}`));

        } else {
            pagesList.push(this.dotsPage());
            pagesList.push(this.page(`${this.activePage - 1}`));
            pagesList.push(this.page(`${this.activePage}`));
            pagesList.push(this.page(`${this.activePage + 1}`));
            pagesList.push(this.dotsPage());
        }

        pagesList.push(this.lastPage());
        pagesList.push(this.nextPage());
        return pagesList;
    }

    pageClicked(page: Page) {
        if (typeof this.canNavigate === 'function') {
            const result = this.canNavigate(page);
            if (result) {
                this._pageClicked(page);
            }
        } else {
            this._pageClicked(page);
        }
    }

    private _pageClicked(page: Page) {
        if (!page.isNext && !page.isPrevious && page.text !== '...') {
            this.activePage = +page.text;
        }

        if (page.isNext && this.activePage < this.totalPages) {
            this.activePage++;
        }

        if (page.isPrevious && this.activePage > 1) {
            this.activePage--;
        }

        this.pagesList = this.pages();
        this.pagingTableService.firePageChanged(+this.activePage);
    }

    pageShouldBeDisabled(page: Page): boolean {
        const firstPageIsActive = this.activePage === 1;
        const lastPageIsActive = this.activePage === this.totalPages;
        const isDots = page.text === '...';
        return (page.isPrevious && firstPageIsActive) ||
            (page.isNext && lastPageIsActive) ||
            isDots;
    }

}
