import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { PagingTableComponent } from './paging-table.component';
import { PagingTableHeaderColumnComponent } from '../paging-table-header-column/paging-table-header-column.component';
import { PagingTableItemsPerPageComponent } from '../paging-table-items-per-page/paging-table-items-per-page.component';
import { PagingTablePagingComponent } from '../paging-table-paging/paging-table-paging.component';
import { PagingTableSearchComponent } from '../paging-table-search/paging-table-search.component';
import {
    PagingTableEventType, PagingTableItemDeletedPayload,
    PagingTableService, PagingTableSortColumnChangedPayload
} from '../../../../services/paging-table.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import Spy = jasmine.Spy;
import { AsyncTasksService } from '../../../../services/async-tasks.service';

describe('PagingTableComponent', () => {
    let component: PagingTableComponent;
    let fixture: ComponentFixture<PagingTableComponent>;
    let pagingTableService: PagingTableService;
    let loadSpy: Spy;

    function load(params: PagingTableLoadParams): Promise<PagingTableLoadResult> {
        return Promise.resolve({
            items: [{name: 'name1'}, {name: 'name2'}],
            total: 100
        });
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                PagingTableComponent,
                PagingTableHeaderColumnComponent,
                PagingTableItemsPerPageComponent,
                PagingTablePagingComponent,
                PagingTableSearchComponent
            ],
            imports: [
                FormsModule,
                ReactiveFormsModule
            ],
            providers: [
                PagingTableService,
                AsyncTasksService
            ]
        }).compileComponents();
    }));

    beforeEach(fakeAsync(() => {
        fixture = TestBed.createComponent(PagingTableComponent);
        component = fixture.componentInstance;
        // component.columns = [];
        component.loadFunction = load;
        component.defaultSortColumnKey = 'col1';
        // component.tableTitle = 'table title';
        loadSpy = spyOn(component, 'loadFunction');
        loadSpy.and.callThrough();
        pagingTableService = fixture.debugElement.injector.get(PagingTableService);
        tick();
        fixture.detectChanges();
    }));

    // it('should put title to given title by tableTitle prop', () => {
    //     expect(fixture.debugElement.query(By.css('.table-title')).nativeElement.textContent).toEqual('table title');
    // });

    it('should call load function initially once loaded, with default params and update items array', () => {
        expect(loadSpy.calls.first().args[0]).toEqual({
            page: 1,
            itemsPerPage: 10,
            search: '',
            sortColumn: 'col1',
            sortAsc: true
        });
        expect(component.items).toEqual([{name: 'name1'}, {name: 'name2'}]);
    });


    it('should call load function when parameters change', fakeAsync(() => {
        pagingTableService.fireSortColumnChanged('col2', false);
        pagingTableService.fireSearchChanged('123');
        pagingTableService.fireItemsPerPageChanged(20);
        pagingTableService.firePageChanged(3);

        fixture.detectChanges();
        tick(100000);

        expect(loadSpy.calls.mostRecent().args[0]).toEqual({
            page: 3,
            itemsPerPage: 20,
            search: '123',
            sortColumn: 'col2',
            sortAsc: false
        });
    }));

    it('should fire SORT_COLUMN_CHANGED with default column key and asc true initially', () => {
        let res;
        pagingTableService.on(PagingTableEventType.SORT_COLUMN_CHANGED).subscribe(event => res = event.payload);
        expect(res).toEqual(new PagingTableSortColumnChangedPayload('col1', true));
    });


    it('delete method should delete matching element from items array', () => {
        component.items = [{name: 'name 1'}, {name: 'name 2'}, {name: 'name 3'}];
        component.delete((item) => item.name === 'name 2');
        expect(component.items).toEqual([{name: 'name 1'}, {name: 'name 3'}]);
    });

    it('delete method should fire ITEM_DELETED event as many times as deleted items count', fakeAsync(() => {
        const res = [];
        pagingTableService.on(PagingTableEventType.ITEM_DELETED).subscribe(event => res.push(event.payload));
        component.items = [{name: 'name 1'}, {name: 'name 2'}, {name: 'name 3'}];
        component.delete((item) => item.name === 'name 2' || item.name === 'name 1');
        tick();
        expect(res).toEqual([
            new PagingTableItemDeletedPayload({name: 'name 1'}),
            new PagingTableItemDeletedPayload({name: 'name 2'})
        ]);
    }));

});
