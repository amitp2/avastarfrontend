import { Component, ContentChild, Input, OnInit, TemplateRef } from '@angular/core';
import { PagingTableRowDirective } from '../../directives/paging-table-row.directive';
import {
    PagingTableEvent,
    PagingTableEventType,
    PagingTableService
} from '../../../../services/paging-table.service';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { PagingTableLoader } from '../../../../interfaces/paging-table-loader';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { AsyncTasksService } from '../../../../services/async-tasks.service';
import { Debounce } from '../../../../decorators/debounce.decorator';
import { UtilsService } from '../../../../services/utils.service';

@Component({
    selector: '[app-paging-table]',
    templateUrl: './paging-table.component.html',
    styleUrls: ['./paging-table.component.styl'],
})
export class PagingTableComponent implements OnInit {

    items: Array<any> = [];
    @Input() loadFunction: PagingTableLoader;
    @Input() defaultSortColumnKey: string;

    @ContentChild(PagingTableRowDirective, {read: TemplateRef}) rowTemplate;

    private latestParams: PagingTableLoadParams;

    constructor(private pagingTableService: PagingTableService,
                private asyncTasksService: AsyncTasksService,
                private utils: UtilsService) {
    }

    ngOnInit() {

        this.pagingTableService.fireSortColumnChanged(this.defaultSortColumnKey, true);

        this.load({
            page: 1,
            itemsPerPage: 10,
            search: '',
            sortColumn: this.defaultSortColumnKey,
            sortAsc: true
        });

        this.pagingTableService
            .on(PagingTableEventType.SEARCH_CHANGED)
            .map((event: PagingTableEvent) => ({search: event.payload.search}))
            .combineLatest(this.pagingTableService.on(PagingTableEventType.SORT_COLUMN_CHANGED))
            .map((arr: any[]) => ({...arr[0], sortColumn: arr[1].payload.key, sortAsc: arr[1].payload.asc}))
            .combineLatest(this.pagingTableService.on(PagingTableEventType.ITEMS_PER_PAGE_CHANGED))
            .map((arr: any[]) => ({...arr[0], itemsPerPage: arr[1].payload.itemsPerPage}))
            .combineLatest(this.pagingTableService.on(PagingTableEventType.PAGE_CHANGED))
            .map((arr: any[]) => ({...arr[0], page: arr[1].payload.page}))
            .debounceTime(100)
            .subscribe((data: PagingTableLoadParams) => {
                this.load(data);
                this.latestParams = data;
            });

    }

    async resetPage() {
        await this.utils.resolveAfter(1000);
        this.pagingTableService.firePageChanged(1);
        await this.utils.resolveAfter(1000);
    }

    load(params: PagingTableLoadParams) {
        const tableLoadingTask = 'TABLE_LOADING';
        this.asyncTasksService.taskStart(tableLoadingTask);
        try {
            this.loadFunction(params)
                .then((res: PagingTableLoadResult) => {
                    
                    this.items = res.items;
                    console.log("Orber BY ", this.items)
                    this.asyncTasksService.taskSuccess(tableLoadingTask);
                    this.pagingTableService.firePageLoaded(res.total, res.items);
                })
                .catch(() => {
                    this.asyncTasksService.taskError(tableLoadingTask);
                });
        } catch (exc) {
            this.asyncTasksService.taskError(tableLoadingTask);
            console.log('handled table exc, load function is not defined yet', exc);
        }
    }

    refresh() {
        this.load(this.latestParams);
    }

    delete(matchFn: (item: any) => boolean) {
        this.items
            .filter(item => matchFn(item))
            .forEach(item => this.pagingTableService.fireItemDeleted(item));
        this.items = this.items.filter(item => !matchFn(item));
    }

}
