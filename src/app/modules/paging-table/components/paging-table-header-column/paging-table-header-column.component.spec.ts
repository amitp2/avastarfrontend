import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { PagingTableHeaderColumnComponent } from './paging-table-header-column.component';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import {
    PagingTableEventType, PagingTableService,
    PagingTableSortColumnChangedPayload
} from '../../../../services/paging-table.service';


describe('PagingTableHeaderColumnComponent', () => {
    let component: PagingTableHeaderColumnComponent;
    let fixture: ComponentFixture<PagingTableHeaderColumnComponent>;
    let pagingTableService: PagingTableService;

    function getIcon(className: string = '.fa'): DebugElement {
        return fixture.debugElement.query(By.css(className));
    }

    function clickSortIcon() {
        fixture.debugElement.query(By.css('.sortable')).triggerEventHandler('click', null);
        fixture.detectChanges();
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                PagingTableHeaderColumnComponent
            ],
            providers: [
                PagingTableService
            ]
        }).compileComponents();

        pagingTableService = TestBed.get(PagingTableService);
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PagingTableHeaderColumnComponent);
        component = fixture.componentInstance;
        fixture.componentInstance.column = {
            title: 'My Title',
            isSortable: false,
            key: '123'
        };
        fixture.detectChanges();
    });

    it('should show given title', () => {
        const title: DebugElement = fixture.debugElement.query(By.css('.title'));
        expect(title.nativeElement.textContent.trim()).toEqual('My Title');
    });

    it('should show sorting icon if column is sortable', () => {
        let icon: DebugElement;

        icon = getIcon();
        expect(icon && icon.nativeElement).toBeFalsy();

        fixture.componentInstance.column = {
            title: 'My Title',
            isSortable: true,
            key: '123'
        };

        fixture.detectChanges();

        icon = getIcon();
        expect(icon && icon.nativeElement).toBeTruthy();
    });

    it('should fire SORT_COLUMN_CHANGED event with given key and asc true when first time clicked and then false, it should toggle', () => {
        let res;
        pagingTableService.on(PagingTableEventType.SORT_COLUMN_CHANGED).subscribe(event => res = event.payload);
        fixture.componentInstance.column = {
            title: 'My Title',
            isSortable: true,
            key: '123'
        };
        fixture.detectChanges();

        clickSortIcon();
        expect(res).toEqual(new PagingTableSortColumnChangedPayload('123', false));

        clickSortIcon();
        expect(res).toEqual(new PagingTableSortColumnChangedPayload('123', true));
    });

    it('should show fa-sort icon by default if column is sortable', () => {
        fixture.componentInstance.column = {
            title: 'My Title',
            isSortable: true,
            key: '123'
        };
        fixture.detectChanges();
        expect(getIcon().nativeElement.className.indexOf('fa-sort')).not.toEqual(-1);
    });

    it('should set icon to fa-sort-asc/fa-sort-desc/fa-sort if on SORT_COLUMN_CHANGED event points to it and is sortable', fakeAsync(() => {
        fixture.componentInstance.column = {
            title: 'My Title',
            isSortable: true,
            key: '123'
        };

        pagingTableService.fireSortColumnChanged('123', true);
        fixture.detectChanges();
        expect(getIcon('.fa.fa-sort-asc') && getIcon('.fa.fa-sort-asc').nativeElement).toBeTruthy();
        expect(getIcon('.fa.fa-sort') && getIcon('.fa.fa-sort').nativeElement).toBeFalsy();

        pagingTableService.fireSortColumnChanged('123', false);
        fixture.detectChanges();
        expect(getIcon('.fa.fa-sort-desc') && getIcon('.fa.fa-sort-desc').nativeElement).toBeTruthy();
        expect(getIcon('.fa.fa-sort') && getIcon('.fa.fa-sort').nativeElement).toBeFalsy();
    }));

    it('should reset icon to fa-sort if on SORT_COLUMN_CHANGED received the other key', () => {
        fixture.componentInstance.column = {
            title: 'My Title',
            isSortable: true,
            key: '123'
        };
        fixture.detectChanges();

        pagingTableService.fireSortColumnChanged('123', true);
        fixture.detectChanges();
        expect(getIcon('.fa.fa-sort-asc') && getIcon('.fa.fa-sort-asc').nativeElement).toBeTruthy();
        expect(getIcon('.fa.fa-sort') && getIcon('.fa.fa-sort').nativeElement).toBeFalsy();

        pagingTableService.fireSortColumnChanged('1234', true);
        fixture.detectChanges();
        expect(getIcon('.fa.fa-sort') && getIcon('.fa.fa-sort').nativeElement).toBeTruthy();
    });
});
