import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import {
    PagingTableEventType, PagingTableService,
    PagingTableSortColumnChangedPayload
} from '../../../../services/paging-table.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-paging-table-header-column',
    templateUrl: './paging-table-header-column.component.html',
    styleUrls: ['./paging-table-header-column.component.styl']
})
export class PagingTableHeaderColumnComponent implements OnInit, OnDestroy {

    @Input() column: PagingTableHeaderColumn;

    sortedAsc: boolean;
    sortColumnChangedSubscription: Subscription;

    isSorted: boolean;
    sortedReceivedAsc: boolean;

    constructor(private pagingTableService: PagingTableService) {
        this.sortedAsc = false;
        this.isSorted = false;
    }

    ngOnInit() {
        this.sortColumnChangedSubscription = this.pagingTableService
            .on(PagingTableEventType.SORT_COLUMN_CHANGED)
            .map(event => event.payload)
            .subscribe((payload: PagingTableSortColumnChangedPayload) => {
                if (payload.key === this.column.key) {
                    this.isSorted = true;
                    this.sortedReceivedAsc = payload.asc;
                } else {
                    this.isSorted = false;
                }
            });
    }

    ngOnDestroy() {
        this.sortColumnChangedSubscription.unsubscribe();
    }

    sortClass() {
        const classes = {};
        if (this.isSorted) {
            if (this.sortedReceivedAsc) {
                classes['fa-sort-asc'] = true;
            } else {
                classes['fa-sort-desc'] = true;
            }
        } else {
            classes['fa-sort'] = true;
        }
        return classes;
    }

    sort() {
        this.pagingTableService.fireSortColumnChanged(this.column.key, this.sortedAsc);
        this.sortedAsc = !this.sortedAsc;
    }

}
