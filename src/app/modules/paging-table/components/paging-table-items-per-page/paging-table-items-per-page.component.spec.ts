import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { PagingTableItemsPerPageComponent } from './paging-table-items-per-page.component';
import {
    PagingTableEventType, PagingTableItemsPerPageChangedPayload,
    PagingTableService
} from '../../../../services/paging-table.service';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('PagingTableItemsPerPageComponent', () => {
    let component: PagingTableItemsPerPageComponent;
    let fixture: ComponentFixture<PagingTableItemsPerPageComponent>;

    let pagingTableService: PagingTableService;

    function setSelectValue(select: DebugElement, value: any) {
        select.nativeElement.value = value;
        const evt = document.createEvent('HTMLEvents');
        evt.initEvent('change', false, true);
        select.nativeElement.dispatchEvent(evt);
    }

    beforeEach(async(() => {

        TestBed.configureTestingModule({
            declarations: [
                PagingTableItemsPerPageComponent
            ],
            imports: [
                FormsModule,
                ReactiveFormsModule
            ],
            providers: [PagingTableService]
        })
            .compileComponents();


        pagingTableService = TestBed.get(PagingTableService);
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PagingTableItemsPerPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should fire ITEMS_PER_PAGE_CHANGED event once initialized with value of 10', () => {
        let res;
        pagingTableService.on(PagingTableEventType.ITEMS_PER_PAGE_CHANGED).subscribe((event) => res = event.payload);
        expect(res).toEqual(new PagingTableItemsPerPageChangedPayload(10));
    });

    it('on changing page size it should ITEMS_PER_PAGE_CHANGED  with given value', fakeAsync(() => {
        let res;
        pagingTableService.on(PagingTableEventType.ITEMS_PER_PAGE_CHANGED).subscribe((event) => res = event.payload);

        const select: DebugElement = fixture.debugElement.query(By.css('.page-size'));
        setSelectValue(select, '50');
        fixture.detectChanges();
        tick();
        expect(res).toEqual(new PagingTableItemsPerPageChangedPayload(50));
    }));
});
