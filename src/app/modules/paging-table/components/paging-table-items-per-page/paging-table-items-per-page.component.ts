import { Component, OnDestroy } from '@angular/core';
import { PagingTableService } from '../../../../services/paging-table.service';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-paging-table-items-per-page',
    templateUrl: './paging-table-items-per-page.component.html',
    styleUrls: ['./paging-table-items-per-page.component.styl']
})
export class PagingTableItemsPerPageComponent implements OnDestroy {

    pageSizeForm: FormGroup;
    pageSizeFormSubscription: Subscription;

    constructor(private pagingTableService: PagingTableService) {
        this.pagingTableService.fireItemsPerPageChanged(10);
        this.pageSizeForm = new FormGroup({
            pageSize: new FormControl(10)
        });
        this.pageSizeFormSubscription = this.pageSizeForm.valueChanges.subscribe((val) => {
            this.pagingTableService.fireItemsPerPageChanged(+val.pageSize);
            this.pagingTableService.firePageChanged(1);
        });
    }

    ngOnDestroy() {
        this.pageSizeFormSubscription.unsubscribe();
    }

}
