import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCostCategoryFormComponent } from './manage-cost-category-form.component';

describe('ManageCostCategoryFormComponent', () => {
  let component: ManageCostCategoryFormComponent;
  let fixture: ComponentFixture<ManageCostCategoryFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageCostCategoryFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCostCategoryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
