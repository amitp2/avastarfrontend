import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { Observable } from 'rxjs/Observable';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { UtilsService } from '../../../../services/utils.service';
import { FormMode } from '../../../../enums/form-mode.enum';
import { FormHandler } from '../../../../classes/form-handler';
import { AppError } from '../../../../enums/app-error.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { CostCategoryModel } from '../../../../models/cost-category-model';
import { CostCategoryResourceService } from '../../../../services/resources/cost-category-resource.service';

@Component({
    selector: 'app-manage-cost-category-form',
    templateUrl: './manage-cost-category-form.component.html',
    styleUrls: ['./manage-cost-category-form.component.styl']
})
export class ManageCostCategoryFormComponent extends FormHandler<CostCategoryModel> {

    constructor(
        private activatedRoute: ActivatedRoute,
        private costCategoryApi: CostCategoryResourceService,
        private router: Router,
        private utils: UtilsService,
        formBuilder: FormBuilder
    ) {
        super(formBuilder);
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    initializeForm(): FormGroup {
        return this.formBuilder.group({
            name: [null, [Validators.required]],
            description: [null, Validators.maxLength(500)]
        });
    }

    @AsyncMethod({
        taskName: 'get_cost_category'
    })
    get(id: number): Promise<CostCategoryModel> {
        return this.costCategoryApi.get(id);
    }

    @AsyncMethod({
        taskName: 'add_cost_category',
        success: AppSuccess.COST_CATEGORY_ADD,
        error: AppError.COST_CATEGORY_ADD_GLOBAL
    })
    add(model: CostCategoryModel): Promise<any> {
        return this.costCategoryApi.add(model)
            .then(() => this.router.navigateByUrl(`/u/manage-cost-category?fresh=${Date.now()}`));
    }

    @AsyncMethod({
        taskName: 'update_cost_category',
        success: AppSuccess.COST_CATEGORY_EDIT,
        error: AppError.COST_CATEGORY_ADD_GLOBAL
    })
    edit(id: number, model: CostCategoryModel): Promise<any> {
        return this.costCategoryApi
            .edit(id, model)
            .then(() => {
                this.router.navigateByUrl(`/u/manage-cost-category?fresh=${Date.now()}`);
            });
    }

    serialize(model: CostCategoryModel): any {
        return this.utils.transformInto(model, {
            name: { to: 'name' },
            description: { to: 'description' }
        });
    }

    deserialize(formValue: any): CostCategoryModel {
        return new CostCategoryModel(this.utils.transformInto(formValue, {
            name: { to: 'name' },
            description: { to: 'description' }
        }));
    }
}
