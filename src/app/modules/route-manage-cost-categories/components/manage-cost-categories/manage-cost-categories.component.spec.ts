import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCostCategoriesComponent } from './manage-cost-categories.component';

describe('ManageCostCategoriesComponent', () => {
  let component: ManageCostCategoriesComponent;
  let fixture: ComponentFixture<ManageCostCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageCostCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCostCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
