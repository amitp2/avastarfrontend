import { Component, OnInit, ViewChild } from '@angular/core';
import { TableHandler } from '../../../../classes/table-handler';
import { UtilsService } from '../../../../services/utils.service';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppError } from '../../../../enums/app-error.enum';
import { AuthService } from '../../../../services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { RevenueCategoryResourceService } from '../../../../services/resources/revenue-category-resource.service';
import { CostCategoryResourceService } from '../../../../services/resources/cost-category-resource.service';

@Component({
  selector: 'app-manage-cost-categories',
  templateUrl: './manage-cost-categories.component.html',
  styleUrls: ['./manage-cost-categories.component.styl']
})
export class ManageCostCategoriesComponent extends TableHandler {
    @ViewChild('table') table: PagingTableComponent;

    constructor(
        private costCategoryApi: CostCategoryResourceService,
        private authService: AuthService,
        private utils: UtilsService,
        activatedRoute: ActivatedRoute
    ) {
        super(activatedRoute);
    }

    getTable(): PagingTableComponent {
        return this.table;
    }

    loadFunction = (params) => {
        return this.costCategoryApi.query(this.utils.tableToHttpParams(params));
    }

    @AsyncMethod({
        taskName: 'delete_cost_category',
        error: AppError.COST_CATEGORY_DELETE
    })
    async delete(item) {
        await this.costCategoryApi.delete(item.id);
        this.table.delete((x) => +x.id === +item.id);
    }
}
