import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageCostCategoryFormComponent } from './components/manage-cost-category-form/manage-cost-category-form.component';
import { ManageCostCategoriesComponent } from './components/manage-cost-categories/manage-cost-categories.component';
import { Route, RouterModule } from '@angular/router';
import { AllowOnlyService } from '../../services/guards/allow-only.service';
import { UserRole } from '../../enums/user-role.enum';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SelectModule } from '../select/select.module';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { TableResizeModule } from '../table-resize/table-resize.module';

const routes: Route[] = [
    {
        path: '',
        component: ManageCostCategoriesComponent,
        canActivate: [AllowOnlyService],
        data: {
            roles: [UserRole.SUPER_ADMIN]
        },
        children: [
            { path: ':id', component: ManageCostCategoryFormComponent }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes),
        PagingTableModule,
        ReactiveFormsModule,
        SelectModule,
        TableResizeModule
    ],
    declarations: [
        ManageCostCategoryFormComponent,
        ManageCostCategoriesComponent
    ]
})
export class RouteManageCostCategoriesModule {
}
