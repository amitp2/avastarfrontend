import { Injectable } from '@angular/core';
import {AuthService} from '../../../services/auth.service';
import {AllowedFileType} from '../../../enums/allowed-file-type.enum';
import {AppSuccess} from '../../../enums/app-success.enum';
import {ConfigService} from '../../../services/config.service';
import {StandardUploaderV2} from '../../../classes/standard-uploader-v2';

@Injectable()
export class PackageLogoUploaderService extends StandardUploaderV2 {

    constructor(private configService: ConfigService, authService: AuthService) {
        super(authService);
    }

    uploadUrl(): string {
        return `${this.configService.config().API_URL}files/package/picture`;
    }

    asyncTaskName() {
        return 'package_logo_upload';
    }

    uploadAppSuccess() {
        return AppSuccess.PACKAGE_LOGO_UPLOAD;
    }

    allowedFileType(): AllowedFileType {
        return AllowedFileType.Images;
    }

}
