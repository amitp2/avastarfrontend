import {Injectable} from '@angular/core';
import {PackageItemType} from '../classes/package-item-type';
import {PackageItem} from '../classes/package-item';
import {IdentityArray} from '../../../classes/identity-array';

@Injectable()
export class PackageItemsManagerService {

    private selectedItems: any;
    private selectedActualItems: any;
    private actualItems: IdentityArray<PackageItem>;

    get actualItemsList(): PackageItem[] {
        return this.actualItems.value;
    }

    get actualItemsSystems(): PackageItem[] {
        return this.actualItems.filter((item: PackageItem) => item.type === PackageItemType.System).value;
    }

    get actualItemsEquipments(): PackageItem[] {
        return this.actualItems.filter((item: PackageItem) => item.type === PackageItemType.Equipment).value;
    }

    constructor() {
        this.actualItems = new IdentityArray<PackageItem>();
        this.initSelectedItems();
        this.initSelectedActualItems();
    }

    private initSelectedActualItems() {
        this.selectedActualItems = {};
        this.selectedActualItems[PackageItemType.System] = new IdentityArray<PackageItem>();
        this.selectedActualItems[PackageItemType.Equipment] = new IdentityArray<PackageItem>();
    }

    private initSelectedItems() {
        this.selectedItems = {};
        this.selectedItems[PackageItemType.System] = new IdentityArray<PackageItem>();
        this.selectedItems[PackageItemType.Equipment] = new IdentityArray<PackageItem>();
    }

    isActual(item: PackageItem): boolean {
        return this.actualItems.is(item);
    }

    setActualItems(item: PackageItem[]) {
        this.actualItems = new IdentityArray<PackageItem>(item);
    }

    addActualItem(item: PackageItem) {
        this.actualItems.push(item);
    }

    isSelected(item: PackageItem): boolean {
        return this.selectedItems[item.type].is(item);
    }

    addSelected(item: PackageItem) {
        this.selectedItems[item.type].push(item);
    }

    deleteSelected(item: PackageItem) {
        this.selectedItems[item.type].remove(item);
    }

    addSelectedActual(item: PackageItem) {
        this.selectedActualItems[item.type].push(item);
    }

    deleteSelectedActual(item: PackageItem) {
        this.selectedActualItems[item.type].remove(item);
    }

    mergeSelectedWithActual() {
        this.actualItems = this.actualItems.concat(this.selectedItems[PackageItemType.Equipment]);
        this.actualItems = this.actualItems.concat(this.selectedItems[PackageItemType.System]);
        this.initSelectedItems();
    }

    removeActuals() {
        this.actualItems = this.actualItems.filter((item: PackageItem) => {
            return !this.selectedActualItems[item.type].is(item);
        });
        this.initSelectedActualItems();
    }

}
