import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { UtilsService } from '../../../../services/utils.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { EquipmentSystemModel } from '../../../../models/equipment-system-model';
import { SystemStatus } from '../../../../enums/system-status.enum';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { AppState } from '../../../../store/store';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { Store } from '@ngrx/store';
import {PackageResourceService} from '../../../../services/resources/package-resource.service';
import {PromisedStoreService} from '../../../../services/promised-store.service';
import {AppError} from '../../../../enums/app-error.enum';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { UserRole } from '../../../../enums/user-role.enum';
import { AuthService } from '../../../../services/auth.service';

@Component({
    selector: 'app-packages',
    templateUrl: './packages.component.html',
    styleUrls: ['./packages.component.styl']
})
export class PackagesComponent implements OnInit, OnDestroy {

    @ViewChild('table') table: PagingTableComponent;
    status = SystemStatus;

    filterForm: FormGroup;

    minPriceSubs: Subscription;
    maxPriceSubs: Subscription;


    columns: PagingTableHeaderColumn[] = [
        {isSortable: true, title: 'Package Name', key: 'name'},
        // {isSortable: true, title: 'Workflow Description', key: 'description'},
        {isSortable: true, title: 'Sales Description', key: 'salesDescription'},
        {isSortable: false, title: 'Actual Price', key: 'price'},
        {isSortable: false, title: 'Actions', key: 'actions'}
    ];

    actionsAllow = [UserRole.SUPER_ADMIN, UserRole.ADMIN, UserRole.TECH_TEAM_LEAD, UserRole.TECH_TEAM];

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            const queryParams = this.utils.tableToHttpParams(params);
            queryParams.otherParams = {};

            this.utils.formValue(this.filterForm, 'minPrice', (val) => queryParams.otherParams.minPrice = val);
            this.utils.formValue(this.filterForm, 'maxPrice', (val) => queryParams.otherParams.maxPrice = val);

            // queryParams.otherParams.minPrice = 500;
            // queryParams.otherParams.minPrice = 2000;
            return this.promisedStore
                .currentVenueId()
                .then((venueId: number) => this.packages.queryByVenueId(queryParams, venueId));
        }).bind(this);
    }

    constructor(private store: Store<AppState>,
                private utils: UtilsService,
                private packages: PackageResourceService,
                private promisedStore: PromisedStoreService,
                private auth: AuthService) {
        this.filterForm = new FormGroup({
            minPrice: new FormControl(),
            maxPrice: new FormControl()
        });

    }

    ngOnInit() {
        this.minPriceSubs = this.filterForm.get('minPrice').valueChanges
            .debounceTime(500)
            .subscribe(() => {
                this.table.refresh();
            });

        this.maxPriceSubs = this.filterForm.get('maxPrice').valueChanges
            .debounceTime(500)
            .subscribe(() => {
                this.table.refresh();
            });
        this.checkRoles();
    }

    async checkRoles() {
        const user = await this.auth.currentUser();
        if (!this.utils.userHasAnyRole(user, this.actionsAllow)) {
            this.columns[4].title = '';
        }
    }

    ngOnDestroy() {
        this.minPriceSubs.unsubscribe();
        this.maxPriceSubs.unsubscribe();
    }

    @AsyncMethod({
        taskName: 'delete_package',
        // error: AppError.DELETE_PACKAGE
    })
    delete(model: EquipmentSystemModel) {
        return this.packages
            .delete(model.id)
            .then(() => this.table.refresh());
    }

}
