import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-package-price-changes-table',
    templateUrl: './package-price-changes-table.component.html',
    styleUrls: ['./package-price-changes-table.component.styl']
})
export class PackagePriceChangesTableComponent implements OnInit {

    @Input() history: any[] = [];

    constructor() {
    }

    ngOnInit() {
    }

}
