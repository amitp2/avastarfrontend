import { Component, forwardRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR} from '@angular/forms';
import {PagingTableService} from '../../../../../services/paging-table.service';
import {PagingTableLoadResult} from '../../../../../interfaces/paging-table-load-result';
import {PagingTableLoadParams} from '../../../../../interfaces/paging-table-load-params';
import {PromisedStoreService} from '../../../../../services/promised-store.service';
import {HttpResourceQueryParams} from '../../../../../interfaces/http-resource-query-params';
import {EquipmentSystemResourceService} from '../../../../../services/resources/equipment-system-resource.service';
import {UtilsService} from '../../../../../services/utils.service';
import {EquipmentSystemModel} from '../../../../../models/equipment-system-model';
import {PagingTableComponent} from '../../../../paging-table/components/paging-table/paging-table.component';
import {EquipmentResourceService} from '../../../../../services/resources/equipment-resource.service';
import {EquipmentModel} from '../../../../../models/equipment-model';
import {PackageItemsManagerService} from '../../../services/package-items-manager.service';
import {PackageItemType} from '../../../classes/package-item-type';
import {PackageItem} from '../../../classes/package-item';
import { Subscription } from 'rxjs/Subscription';
import { SelectFetchType } from '../../../../select/types/select-fetch-type';
import { InventorySubCategoryFetchFactory } from '../../../../select/fetches/factories/inventory-sub-category.fetch.factory';


@Component({
    selector: 'app-package-items',
    templateUrl: './package-items.component.html',
    styleUrls: ['./package-items.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => PackageItemsComponent),
            multi: true
        },
        PagingTableService,
        PackageItemsManagerService
    ]
})
export class PackageItemsComponent implements ControlValueAccessor, OnInit, OnDestroy {

    @ViewChild('table') table: PagingTableComponent;

    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;

    filterForm: FormGroup;
    packageItemType = PackageItemType;
    selectecCheckboxes: HTMLInputElement[];
    itemTypeSubs: Subscription;

    SelectFetchType = SelectFetchType;

    constructor(private promisedStore: PromisedStoreService,
                private systems: EquipmentSystemResourceService,
                private equipments: EquipmentResourceService,
                private utils: UtilsService,
                public packageItemsManager: PackageItemsManagerService,
                public inventorySubCategoryFetchFactory: InventorySubCategoryFetchFactory) {
        this.filterForm = new FormGroup(({
            itemType: new FormControl(PackageItemType.Equipment),
            search: new FormControl(),
            categoryId: new FormControl(),
            subCategoryId: new FormControl()
        }));
        this.selectecCheckboxes = [];
    }

    applyFilters() {
        this.table.refresh();
    }

    cancel() {
        this.filterForm.patchValue({
            categoryId: '',
            search: '',
            subCategoryId: ''
        });
        this.table.refresh();
    }

    private notifyUpdateOutside() {
        if (this.onChange) {
            this.onChange({
                equipments: this.packageItemsManager.actualItemsEquipments.map((item: PackageItem) => ({
                    equipmentId: item.id,
                    count: item.count ? item.count : 0
                }))
            });
        }
    }

    checkedLeft(event: MouseEvent, item: PackageItem) {
        const element = <HTMLInputElement> event.target;
        if (element.checked) {
            this.packageItemsManager.addSelected(item);
            this.selectecCheckboxes.push(element);
        } else {
            this.packageItemsManager.deleteSelected(item);
        }
    }

    checkedRight(event: MouseEvent, item: PackageItem) {
        const element = <HTMLInputElement> event.target;
        if (element.checked) {
            this.packageItemsManager.addSelectedActual(item);
        } else {
            this.packageItemsManager.deleteSelectedActual(item);
        }
    }

    add() {
        this.packageItemsManager.mergeSelectedWithActual();
        this.selectecCheckboxes.forEach((e: HTMLInputElement) => e.checked = false);
        this.selectecCheckboxes = [];
        this.notifyUpdateOutside();
    }

    remove() {
        this.packageItemsManager.removeActuals();
        this.notifyUpdateOutside();
    }

    loadSystems(params: HttpResourceQueryParams): Promise<PagingTableLoadResult> {

        params.otherParams = {};
        this.utils.formValue(this.filterForm, 'search', (val: string) => params.otherParams.search = val);

        return this.promisedStore.currentVenueId()
             .then((venueId: number) => this.systems.queryByVenueId(params, venueId))
            .then((resp: PagingTableLoadResult) => {

                resp.items = resp.items.map((eq: EquipmentSystemModel) => ({
                    id: eq.id,
                    title: eq.name,
                    type: PackageItemType.System,
                    url: `/u/systems/${eq.id}`
                }));

                return resp;
            });
    }

    loadEquipments(params: HttpResourceQueryParams): Promise<PagingTableLoadResult> {
        params.otherParams = {
            type: '',
            searchByDescription: 'false'
        };

        this.utils.formValue(this.filterForm, 'categoryId', (val: string) => {
            params.otherParams.type = 'EQUIPMENT';
            params.otherParams.categoryId = val;
        });
        this.utils.formValue(this.filterForm, 'subCategoryId', (val: string) => params.otherParams.subCategoryId = val);
        this.utils.formValue(this.filterForm, 'search', (val: string) => params.otherParams.search = val);

        return this.promisedStore.currentVenueId()
            .then((venueId: number) => this.equipments.queryByVenueId(params, venueId))
            .then((resp: PagingTableLoadResult) => {

                resp.items = resp.items.map((eq: EquipmentModel) => ({
                    id: eq.id,
                    title: eq.subCategoryItemName,
                    type: PackageItemType.Equipment
                }));

                return resp;
            });
    }

    loadStrategy(params: HttpResourceQueryParams) {
        if (this.filterForm.get('itemType').value === PackageItemType.System) {
            return this.loadSystems(params);
        }
        return this.loadEquipments(params);
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return this.loadStrategy(this.utils.tableToHttpParams(params));
        }).bind(this);
    }

    ngOnInit() {
        this.itemTypeSubs = this.filterForm.get('itemType').valueChanges
            .subscribe(() => {
                this.filterForm.get('search').setValue('');
                this.table.refresh();
            });
    }

    ngOnDestroy() {
        this.itemTypeSubs.unsubscribe();
    }

    getSystems(obj: any): Promise<PackageItem[]> {
        if (!obj || !obj.systemIds || obj.systemIds.length === 0) {
            return Promise.resolve([]);
        }
        return this.promisedStore.currentVenueId()
            .then((venueId: number) => this.systems.getManyByVenueId(obj.systemIds, venueId))
            .then((resp) => {
                return resp
                    .map((system: EquipmentSystemModel) => ({
                        id: system.id,
                        title: system.name,
                        type: PackageItemType.System
                    }));
            });
    }

    getEquipments(obj: any): Promise<PackageItem[]> {
        if (!obj || !obj.equipments || obj.equipments.length === 0) {
            return Promise.resolve([]);
        }
        return this.promisedStore.currentVenueId()
            .then((venueId: number) => this.equipments.getManyByVenueId(obj.equipments.map(eq => eq.equipmentId), venueId))
            .then((resp) => {
                return resp
                    .map((equipment: EquipmentModel, index: number) => ({
                        id: equipment.id,
                        title: equipment.subCategoryItemName,
                        type: PackageItemType.Equipment,
                        count: obj.equipments[index].count
                    }));
            });
    }

    writeValue(obj: any): void {
        this.currentValue = obj;
        console.log(obj)
        Promise.all([
            this.getEquipments(obj),
            this.getSystems(obj)
        ]).then((res: any[]) => {
            this.packageItemsManager.setActualItems([]);
            this.packageItemsManager.setActualItems(res[0].concat(res[1]));
        });
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }


    changedCount() {
        this.notifyUpdateOutside();
    }

}
