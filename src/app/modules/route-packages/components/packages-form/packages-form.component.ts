import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormHandler } from '../../../../classes/form-handler';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PackageModel } from '../../../../models/package-model';
import { ActivatedRoute, Router } from '@angular/router';
import { PackageResourceService } from '../../../../services/resources/package-resource.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { UtilsService } from '../../../../services/utils.service';
import { PromisedStoreService } from '../../../../services/promised-store.service';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { PackageLogoUploaderService } from '../../services/package-logo-uploader.service';
import { Subscription } from 'rxjs/Subscription';
import { EquipmentSystemResourceService } from '../../../../services/resources/equipment-system-resource.service';
import { EquipmentResourceService } from '../../../../services/resources/equipment-resource.service';
import { EquipmentModel } from '../../../../models/equipment-model';
import { EquipmentSystemModel } from '../../../../models/equipment-system-model';

export interface PackageContents {
    // equipmentIds: number[];
    // systemIds: number[];
    equipments: any[];
}

@Component({
    selector: 'app-packages-form',
    templateUrl: './packages-form.component.html',
    styleUrls: ['./packages-form.component.styl']
})
export class PackagesFormComponent extends FormHandler<PackageModel> implements OnInit, OnDestroy {

    priceHistory: any[] = [];
    contentSubs: Subscription;

    constructor(
        private activatedRoute: ActivatedRoute,
        private packageResource: PackageResourceService,
        private promisedStore: PromisedStoreService,
        private utils: UtilsService,
        private router: Router,
        public uploader: PackageLogoUploaderService,
        private systems: EquipmentSystemResourceService,
        private equipments: EquipmentResourceService
    ) {
        super();
    }

    cancel() {
        this.router.navigateByUrl('/u/packages');
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    initializeForm(): FormGroup {
        const form = new FormGroup({
            calculatedPrice: new FormControl(),
            name: new FormControl(null, [Validators.required]),
            // description: new FormControl(null, [Validators.required]),
            salesDescription: new FormControl(null, [Validators.required]),
            price: new FormControl(null, [Validators.required]),
            priceFromDate: new FormControl(null, [Validators.required]),
            priceToDate: new FormControl(null, [Validators.required]),
            pictureUrl: new FormControl(),
            contents: new FormControl({
                equipments: []
            })
        });
        // form.get('calculatedPrice').disable();
        return form;
    }

    get(id: number): Promise<PackageModel> {
        return this.packageResource.get(id)
            .then((model: PackageModel) => {
                this.priceHistory = model.oldPrices || [];
                return model;
            });
    }

    @AsyncMethod({
        taskName: 'add_package',
        success: AppSuccess.ADD_PACKAGE,
        error: AppError.ADD_PACKAGE
    })
    add(model: PackageModel): Promise<any> {
        return this.promisedStore
            .currentVenueId()
            .then((venueId: number) => this.packageResource.addByVenueId(model, venueId))
            .then((added: PackageModel) => {
                return this.router.navigateByUrl(`/u/packages/${added.id}`);
            });
    }

    @AsyncMethod({
        taskName: 'edit_package',
        success: AppSuccess.EDIT_PACKAGE,
        error: AppError.EDIT_PACKAGE
    })
    edit(id: number, model: PackageModel): Promise<any> {
        return this.packageResource.edit(id, model)
            .then((updated: PackageModel) => this.get(updated.id))
            .then((fetched: PackageModel) => {
                this.priceHistory = fetched.oldPrices || [];
            });
    }

    copy() {
        this.submit()
            .then(() => this.utils.resolveAfter(1000))
            .then(() => {
                this.priceHistory = [];
                this.router.navigateByUrl(`/u/packages/add`)
                    .then(() => {
                        this.form.patchValue({
                            name: this.utils.copyOf(this.form.value.name)
                        });
                    });
            });
    }

    serialize(model: PackageModel): any {
        const serialized = this.utils.transformInto(model, {
            name: { to: 'name' },
            description: { to: 'description' },
            salesDescription: { to: 'salesDescription' },
            price: { to: 'price' },
            priceFromDate: { to: 'priceFromDate' },
            priceToDate: { to: 'priceToDate' },
            pictureUrl: { to: 'pictureUrl' }
        });
        serialized.contents = {
            equipments: model.equipments
            // equipmentIds: model.equipmentIds,
            // systemIds: model.systemIds
        };
        return serialized;
    }

    deserialize(formValue: any): PackageModel {
        const deserialized = new PackageModel(this.utils.transformInto(formValue, {
            name: { to: 'name' },
            description: { to: 'description' },
            salesDescription: { to: 'salesDescription' },
            price: { to: 'price' },
            priceFromDate: { to: 'priceFromDate' },
            priceToDate: { to: 'priceToDate' },
            pictureUrl: { to: 'pictureUrl' }
        }));

        const contents = <PackageContents> formValue.contents;

        // deserialized.systemIds = contents.systemIds;
        // deserialized.equipmentIds = contents.equipmentIds;
        deserialized.equipments = contents.equipments;

        return deserialized;
    }


    equipmentPrices(contents: any[]): Promise<number> {
        if (contents.length === 0) {
            return Promise.resolve(0);
        }
        return this.promisedStore.currentVenueId()
            .then((venueId: number) => this.equipments.getManyByVenueId(
                contents.map(c => c.equipmentId),
                venueId
            ))
            .then((equipments: EquipmentModel[]) => {
                return equipments.reduce((acc, cur, index) => acc + cur.price * contents[index].count, 0);
            });
    }

    systemPrices(ids: number[]): Promise<number> {
        if (ids.length === 0) {
            return Promise.resolve(0);
        }
        return this.promisedStore
            .currentVenueId()
            .then((venueId: number) => this.systems.getManyByVenueId(ids, venueId))
            .then((systems) => {
                return Promise.all(
                    systems
                        .map((s: EquipmentSystemModel) => s.inventoryIds)
                        .map((eqIds) => this.equipmentPrices(eqIds))
                );
            })
            .then((res) => <number> res.reduce((acc, cur) => acc + cur, 0));
    }

    ngOnInit() {
        super.ngOnInit();
        this.contentSubs = this.form.get('contents')
            .valueChanges
            .subscribe((val) => {
                Promise.all([
                    this.equipmentPrices(val.equipments),
                    this.systemPrices([])
                ]).then((sums: number[]) => {
                    this.form.patchValue({
                        calculatedPrice: sums[0] + sums[1]
                    });
                });
            });
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        this.contentSubs.unsubscribe();
    }
}
