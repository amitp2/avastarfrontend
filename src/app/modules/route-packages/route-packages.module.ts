import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackagesComponent } from './components/packages/packages.component';
import { PackagesFormComponent } from './components/packages-form/packages-form.component';
import { RouterModule, Routes } from '@angular/router';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { SharedModule } from '../shared/shared.module';
import { PackagePriceChangesTableComponent } from './components/package-price-changes-table/package-price-changes-table.component';
import { PackageItemsComponent } from './components/form-controls/package-items/package-items.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {PackageLogoUploaderService} from './services/package-logo-uploader.service';
import { SelectModule } from '../select/select.module';
import { TableResizeModule } from '../table-resize/table-resize.module';
import { MaskedInputModule } from '../masked-input/masked-input.module';

const routes: Routes = [
    {path: '', component: PackagesComponent, pathMatch: 'exact'},
    {path: ':id', component: PackagesFormComponent}
];

@NgModule({
    imports: [
        CommonModule,
        PagingTableModule,
        SharedModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        TableResizeModule,
        MaskedInputModule,
        FormsModule,
        SelectModule
    ],
    declarations: [
        PackagesComponent,
        PackagesFormComponent,
        PackagePriceChangesTableComponent,
        PackageItemsComponent
    ],
    providers: [
        PackageLogoUploaderService
    ]
})
export class RoutePackagesModule {
}
