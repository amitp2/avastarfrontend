import {PackageItemType} from './package-item-type';

export interface PackageItem {
    id: number;
    title: string;
    type: PackageItemType;
    url?: string;
    count?: number;
}
