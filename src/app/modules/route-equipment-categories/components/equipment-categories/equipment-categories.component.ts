import { Component, OnInit } from '@angular/core';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { PagingTableService } from '../../../../services/paging-table.service';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';

@Component({
    selector: 'app-equipment-categories',
    templateUrl: './equipment-categories.component.html',
    styleUrls: ['./equipment-categories.component.styl'],
    providers: [PagingTableService]
})
export class EquipmentCategoriesComponent implements OnInit {

    public columns: PagingTableHeaderColumn[] = [
        { title: 'Equipment/Service Item', key: '3', isSortable: false },
        { title: 'Category', key: '1', isSortable: false },
        { title: 'Sub Category', key: '2', isSortable: false },
    ];

    constructor() {
    }

    loadFunction(): any {
        return (async (params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return {total: 50, items: []};
        }).bind(this);
    }

    ngOnInit() {
    }

}
