import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EquipmentCategoriesComponent } from './components/equipment-categories/equipment-categories.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { PagingTableModule } from '../paging-table/paging-table.module';

const routes: Routes = [
    {path: '', component: EquipmentCategoriesComponent}
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        PagingTableModule
    ],
    declarations: [EquipmentCategoriesComponent]
})
export class RouteEquipmentCategoriesModule {
}
