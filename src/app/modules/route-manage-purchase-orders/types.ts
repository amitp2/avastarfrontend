export enum PurchaseOrderStatus {
    Paid = 'PAID',
    Ordered = 'ORDERED',
    Canceled = 'CANCELED',
    Tentative = 'TENTATIVE'
}

export enum PurchaseOrderType {
    Rental = 'RENTAL',
    ShowOrder = 'SHOW_ORDER',
    Purchase = 'PURCHASE'
}

export class PurchaseOrder {
    contactName: string;
    id: number = null;
    poNumber: number = null;
    createDate: Date = null;
    totalCost: number;
    vendorId: number = null;
    eventId: number = null;
    eventName: string = null;
    contactId: number = null;
    accountId: number = null;
    month: number = null;
    year: number = null;
    deliveryCharges: number = null;
    tax: number = null;
    orderBy: string = null;
    invoiceNumber: string = null;
    notes: string = null;
    status: PurchaseOrderStatus;
    type: PurchaseOrderType;
    purchaseOrderItems: Partial<PurchaseOrderItem>[] = [];

    constructor(data?: Partial<PurchaseOrder>) {

    }

    toJson(): Partial<PurchaseOrder> {
        return {
            poNumber: this.poNumber,
            vendorId: this.vendorId,
            eventId: this.eventId,
            contactId: this.contactId,
            month: this.month,
            year: this.year,
            accountId: this.accountId,
            deliveryCharges: this.deliveryCharges,
            tax: this.tax,
            notes: this.notes,
            status: this.status,
            type: this.type,
            orderBy:this.orderBy,
            invoiceNumber: this.invoiceNumber,
            purchaseOrderItems: this.purchaseOrderItems.map(x => x.toJson())
        };
    }

    // tslint:disable-next-line:member-ordering
    static fromJson(data: any) {
        const model = new PurchaseOrder();

        model.id = data.id;
        model.poNumber = data.poNumber;
        model.createDate = data.createDate ? new Date(data.createDate) : null;
        model.year = data.year;
        model.contactId = data.contactId;
        model.vendorId = data.vendorId;
        model.month = data.month;
        model.invoiceNumber = data.invoiceNumber;
        model.deliveryCharges = data.deliveryCharges;
        model.tax = data.tax;
        model.accountId = data.accountId;
        model.notes = data.notes;
        model.status = data.status;
        model.purchaseOrderItems = (data.purchaseOrderItems || []).map(PurchaseOrderItem.fromJson);
        model.totalCost = data.totalCost;
        model.type = data.type;
        model.orderBy = data.orderBy;

        if (data.event) {
            model.eventId = data.event.id;
            model.eventName = data.event.name;
        } else {
            model.eventId = data.eventId;
        }

        if (data.contact) {
            model.contactName = data.contact.firstName + ' ' + data.contact.lastName;
        }

        return model;
    }
}

export class PurchaseOrderItem {
    id: number = null;
    itemId: number = null;
    categoryId: number = null;
    subCategoryId: number = null;
    subCategoryItemId: number = null;
    arriveDate: Date | number | string = null;
    pickupDate: Date | number | string = null;
    rentalDays: number = null;
    quantity: number = null;
    guestCharge: number = null;
    unitCost: number = null;
    itemName: string;
    notes: string;

    get profit() {
        const {
            quantity,
            guestCharge,
            unitCost
        } = this;

        if (guestCharge && quantity && unitCost) {
            return guestCharge - quantity * unitCost;
        }
        return 0;
    }

    get profitPercent() {
        const {
            guestCharge,
            quantity,
            unitCost
        } = this;

        if (guestCharge && quantity && unitCost) {
            return 100 - ((quantity * unitCost) / (guestCharge / 100));
        }
        return 0;
    }

    toJson(): Partial<PurchaseOrderItem> {
        return {
            itemId: this.itemId,
            // arriveDate: (<Date>this.arriveDate).getTime(),
            // pickupDate: (<Date>this.pickupDate).getTime(),
            arriveDate: this.arriveDate ? (<Date>this.arriveDate).getTime() : null,
            pickupDate: this.pickupDate ? (<Date>this.pickupDate).getTime() : null,
            rentalDays: this.rentalDays,
            quantity: this.quantity,
            guestCharge: this.guestCharge,
            unitCost: this.unitCost,
            notes: this.notes
        };
    }

    toFormValue() {
        return {
            itemId: this.itemId,
            categoryId: this.categoryId,
            subCategoryId: this.subCategoryId,
            arriveDate: this.arriveDate,
            pickupDate: this.pickupDate,
            rentalDays: this.rentalDays,
            quantity: this.quantity,
            guestCharge: this.guestCharge,
            unitCost: this.unitCost,
            notes: this.notes
        };
    }

    // tslint:disable-next-line:member-ordering
    static fromJson(data: any, PurchaseType: string) {
        const model = new PurchaseOrderItem();
        model.quantity = data.quantity;
        model.unitCost = data.unitCost;
        // model.rentalDays = data.rentalDays;
        // model.arriveDate = new Date(data.arriveDate);
        // model.pickupDate = new Date(data.pickupDate);
        // model.guestCharge = data.guestCharge;
        if (PurchaseType != "PURCHASE") {
            model.rentalDays = data.rentalDays;
            model.arriveDate = data.arriveDate !== null ? new Date(data.arriveDate) : null ;
            model.pickupDate = data.pickupDate !== null ? new Date(data.pickupDate) : null ;
            model.guestCharge = data.guestCharge;
        } else {
            model.rentalDays = null;
            model.arriveDate = null;
            model.pickupDate = null;
            model.guestCharge = null
        }
     
        model.notes = data.notes;

        if (data.subCategoryItem) {
            model.itemId = data.subCategoryItem.id;
            model.categoryId = data.subCategoryItem.categoryId;
            model.subCategoryId = data.subCategoryItem.subCategoryId;
            model.itemName = data.subCategoryItem.subCategoryItemName || data.subCategoryItem.name;
            model.subCategoryItemId = data.subCategoryItem.id;
        }

        return model;
    }
}
