import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormMode } from '../../../../enums/form-mode.enum';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PurchaseOrderItem, PurchaseOrderType } from '../../types';
// tslint:disable-next-line:max-line-length
import { InventorySubCategoryItemsDropdownComponent } from '../../../shared/components/form-controls/inventory-sub-category-items-dropdown/inventory-sub-category-items-dropdown.component';
import { UtilsService } from '../../../../services/utils.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
// tslint:disable-next-line:max-line-length
import { InventorySubCategoryDropdownComponent } from '../../../shared/components/form-controls/inventory-sub-category-dropdown/inventory-sub-category-dropdown.component';
import { InventoryCategoryDropdownComponent } from '../../../shared/components/form-controls/inventory-category-dropdown/inventory-category-dropdown.component';
import { EquipmentDropdownComponent } from '../../../shared/components/form-controls/equipment-dropdown/equipment-dropdown.component';
import { InventorySubCategoryItemFetchFactory } from '../../../select/fetches/factories/inventory-sub-category-item.fetch.factory';
import { InventorySubCategoryFetchFactory } from '../../../select/fetches/factories/inventory-sub-category.fetch.factory';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { EquipmentResourceService } from '../../../../services/resources/equipment-resource.service';
import { ToastrService } from 'ngx-toastr';

import 'rxjs/add/operator/filter';
import { InventoryCategoryResourceService } from '../../../../services/resources/inventory-category-resource.service';
import { currencyStringToNumber } from '../../../../helpers/functions/currency-string-to-number';

export interface UpdatePurchaseOrderItemEvent {
    index: number;
    model: PurchaseOrderItem;
}

@Component({
    selector: 'app-equipment-item-form',
    templateUrl: './equipment-item-form.component.html',
    styleUrls: ['./equipment-item-form.component.styl']
})
export class EquipmentItemFormComponent implements OnInit {
    isLoading = false;
    popupVisible = false;
    form: FormGroup;
    formMode = FormMode.Add;
    editableIndex: number;
    PurchaseType: string = '';
    SelectFetchType = SelectFetchType;

    latestEquipmentName: string;
    latestEquipmentId: number;
    selectedPurchaseType: boolean = false;

    @Output() created: EventEmitter<PurchaseOrderItem> = new EventEmitter();
    @Output() updated: EventEmitter<UpdatePurchaseOrderItemEvent> = new EventEmitter();

    @ViewChild('category')
    categoryDropdown: InventoryCategoryDropdownComponent;

    @ViewChild('subCategory')
    subCategoryDropdown: InventorySubCategoryDropdownComponent;

    @ViewChild('subCategoryItem')
    subCategoryItemDropDown: EquipmentDropdownComponent;

    constructor(private fb: FormBuilder,
        private utils: UtilsService,
        private equipmentResource: EquipmentResourceService,
        private toast: ToastrService,
        private inventoryCategory: InventoryCategoryResourceService,
        public inventorySubCategoryFetchFactory: InventorySubCategoryFetchFactory,
        public inventorySubCategoryItemFetchFactory: InventorySubCategoryItemFetchFactory) {
        this.buildForm();
    }

    get profit() {
        const {
            guestCharge,
            quantity,
            unitCost
        } = this.form.value;

        if (guestCharge && quantity && unitCost) {
            return guestCharge - quantity * unitCost;
        }

        return 0;
    }

    get profitPercent() {
        const {
            guestCharge,
            quantity,
            unitCost
        } = this.form.value;

        if (guestCharge && quantity && unitCost) {
            return 100 - ((quantity * unitCost) / (guestCharge / 100));
        }

        return 0;
    }

    get totalCost() {
        const {
            quantity,
            unitCost,
            rentalDays
        } = this.form.value;

        if (quantity && unitCost && rentalDays) {
            return quantity * unitCost * rentalDays;
        }
        else {
            return quantity * unitCost;
        }
        // return 0;
    }

    ngOnInit() {
        this.form.get('itemId')
            .valueChanges
            .filter(val => !!val)
            .subscribe((val) => this.fetchUnitCost(val));
    }

    async fetchUnitCost(subCategoryItemId) {
        const subCategoryId = this.form.get('subCategoryId').value;
        const res = await this.inventoryCategory.getAllSubcategoryItems(subCategoryId);
        const subCatItem = res.find(i => +i.id === +subCategoryItemId);

        if (!subCatItem) {
            this.latestEquipmentName = null;
            this.latestEquipmentId = null;
        } else {
            this.latestEquipmentName = subCatItem.name;
            this.latestEquipmentId = subCategoryItemId;
        }
    }

    buildForm() {
        this.form = this.fb.group({
            categoryId: [null, Validators.required],
            subCategoryId: [null, Validators.required],
            itemId: [null, Validators.required],
            arriveDate: ['', Validators.required],
            pickupDate: ['', Validators.required],
            rentalDays: ['', Validators.required],
            quantity: ['', Validators.required],
            guestCharge: ['', Validators.required],
            unitCost: ['', Validators.required],
            notes: ['', Validators.maxLength(512)]
        });
    }

    @AsyncMethod({
        taskName: 'edit_purchase_order_item'
    })
    async edit(item: PurchaseOrderItem, index: number, val) {
        if (val == "PURCHASE") {
            this.selectedPurchaseType = true;
            this.form.get('arriveDate').setValidators([]);
            this.form.get('arriveDate').updateValueAndValidity();
            this.form.get('pickupDate').setValidators([]);
            this.form.get('pickupDate').updateValueAndValidity();
            this.form.get('rentalDays').setValidators([]);
            this.form.get('rentalDays').updateValueAndValidity();
            this.form.get('guestCharge').setValidators([]);
            this.form.get('guestCharge').updateValueAndValidity();
        }
        else {
            this.selectedPurchaseType = false;
            this.form.get('arriveDate').setValidators([Validators.required]);
            this.form.get('arriveDate').updateValueAndValidity();
            this.form.get('pickupDate').setValidators([Validators.required]);
            this.form.get('pickupDate').updateValueAndValidity();
            this.form.get('rentalDays').setValidators([Validators.required]);
            this.form.get('rentalDays').updateValueAndValidity();
            this.form.get('guestCharge').setValidators([Validators.required]);
            this.form.get('guestCharge').updateValueAndValidity();
        }

        this.formMode = FormMode.Edit;
        this.editableIndex = index;
        this.popupVisible = true;

        // this.form.get('categoryId').setValue(null);
        // this.form.get('subCategoryId').setValue(null);
        // this.form.get('categoryId').setValue(item.categoryId);
        // await this.utils.resolveAfter(750);
        // this.form.get('subCategoryId').setValue(item.subCategoryId);
        // await this.utils.resolveAfter(750);
        // this.form.get('itemId').setValue(item.itemId);

        // console.log(item.toFormValue());

        const set = item.toFormValue();
        set.itemId = item.subCategoryItemId;
        this.form.reset(set);

        this.form.markAsPristine();
    }

    close() {
        this.popupVisible = false;
        this.formMode = FormMode.Add;
        this.editableIndex = null;
        this.form.reset();
    }

    open(val) {
        this.PurchaseType = val;
        if (val == "PURCHASE") {
            this.selectedPurchaseType = true;
            this.form.get('arriveDate').setValidators([]);
            this.form.get('arriveDate').updateValueAndValidity();
            this.form.get('pickupDate').setValidators([]);
            this.form.get('pickupDate').updateValueAndValidity();
            this.form.get('rentalDays').setValidators([]);
            this.form.get('rentalDays').updateValueAndValidity();
            this.form.get('guestCharge').setValidators([]);
            this.form.get('guestCharge').updateValueAndValidity();
        }
        else {
            this.selectedPurchaseType = false;
            this.form.get('arriveDate').setValidators([Validators.required]);
            this.form.get('arriveDate').updateValueAndValidity();
            this.form.get('pickupDate').setValidators([Validators.required]);
            this.form.get('pickupDate').updateValueAndValidity();
            this.form.get('rentalDays').setValidators([Validators.required]);
            this.form.get('rentalDays').updateValueAndValidity();
            this.form.get('guestCharge').setValidators([Validators.required]);
            this.form.get('guestCharge').updateValueAndValidity();
        }

        this.formMode = FormMode.Add;
        this.popupVisible = true;
        if (this.categoryDropdown) {
            this.categoryDropdown.refresh();
        }
    }

    // setUnitCost() {
    //     if (this.subCategoryItemDropDown) {
    //         this.form.get('unitCost').setValue(
    //             this.subCategoryItemDropDown.getPrice()
    //         );
    //     }
    // }

    submit() {
        if (this.form.invalid) {
            return;
        }

        this.isLoading = true;

        const formValue = this.form.value;
        const model = PurchaseOrderItem.fromJson({
            ...formValue,
            subCategoryItem: {
                id: this.latestEquipmentId,
                categoryId: formValue.categoryId,
                subCategoryId: formValue.subCategoryId,
                subCategoryItemName: this.latestEquipmentName,
                subCategoryItemId: formValue.itemId
            }
        }, this.PurchaseType);

        console.log(formValue, model);

        if (typeof this.editableIndex === 'number') {
            this.updated.emit({ model, index: this.editableIndex });
        } else {
            this.created.emit(model);
        }

        this.close();
    }
}
