import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UtilsService } from '../../../../services/utils.service';
import { PurchaseOrderStatus, PurchaseOrder } from '../../types';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { PurchaseOrderService } from '../../../../services/resources/purchase-order.service';
import { CurrentPropertyService } from '../../../../services/current-property.service';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { VenueModel } from '../../../../models/venue-model';
import { AppError } from '../../../../enums/app-error.enum';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { PromisedStoreService } from '../../../../services/promised-store.service';
import { VenueSettingsResourceService } from '../../../../services/resources/venue-settings-resource.service';
import { SelectFetchType } from '../../../select/types/select-fetch-type';

@Component({
    selector: 'app-manage-purchase-orders',
    templateUrl: './manage-purchase-orders.component.html',
    styleUrls: ['./manage-purchase-orders.component.styl']
})
export class ManagePurchaseOrdersComponent implements OnInit {
    filterForm: FormGroup;
    purchaseOrderStatuses: typeof PurchaseOrderStatus = PurchaseOrderStatus;
    columns: PagingTableHeaderColumn[] = [
        {isSortable: true, title: 'PO Number', key: 'poNumber'},
        {isSortable: true, title: 'PO Date', key: 'createdAt'},
        {isSortable: true, title: 'Status', key: 'status'},
        {isSortable: false, title: 'Event', key: 'event.name'},
        {isSortable: false, title: 'Vendor', key: 'vendor'},
        {isSortable: false, title: 'Contact', key: 'contact'},
        {isSortable: false, title: 'Total Cost', key: 'totalCost'},
        {isSortable: false, title: 'Actions', key: 'actions'}
    ];

    @ViewChild('table') table: PagingTableComponent;

    poPrefix: any;
    SelectFetchType = SelectFetchType;

    constructor(private fb: FormBuilder,
                private utils: UtilsService,
                private purchaseOrderApi: PurchaseOrderService,
                private currentVenue: CurrentPropertyService,
                private promisedStore: PromisedStoreService,
                private venueSettings: VenueSettingsResourceService) {
        this.buildFilterForm();
        this.fetchPeoPrefix();
    }

    private buildFilterForm() {
        this.filterForm = this.fb.group({
            // eventId: '',
            status: '',
            vendorId: '',
            start: '',
            finish: '',
            poNumber: ''
        });
    }

    ngOnInit() {
    }

    async fetchPeoPrefix(): Promise<any> {
        const venueId = await this.promisedStore.currentVenueId();
        const settings = await this.venueSettings.getForVenue(venueId);
        this.poPrefix = settings.po;
    }

    applyFilters() {
        this.table.refresh();
    }

    clearFilters() {
        this.filterForm.reset({
            // eventId: '',
            vendorId: '',
            status: ''
        });
        this.table.refresh();
    }

    loadFunction() {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            console.log("Params ", params);
            return this.currentVenue.currentVenueAsync()
                .then((venue: VenueModel) => {
                    const queryParams = this.utils.tableToHttpParams(params);

                    queryParams.otherParams = {...this.filterForm.value};
                    
                    if (!queryParams.otherParams.vendorId) {
                        delete queryParams.otherParams.vendorId;
                    }
                    // if (!queryParams.otherParams.eventId) {
                    //     delete queryParams.otherParams.eventId;
                    // }
                    if (!queryParams.otherParams.status) {
                        delete queryParams.otherParams.status;
                    }
                    if (!queryParams.otherParams.start) {
                        delete queryParams.otherParams.start;
                    } else {
                        queryParams.otherParams.start = queryParams.otherParams.start.split('-').join('/');
                    }
                    if (!queryParams.otherParams.finish) {
                        delete queryParams.otherParams.finish;
                    } else {
                        queryParams.otherParams.finish = queryParams.otherParams.finish.split('-').join('/');
                    }
                    return this.purchaseOrderApi.queryByVenueId(queryParams, venue.id);
                });
        }).bind(this);
    }

    @AsyncMethod({
        taskName: 'purchase_order_delete',
        error: AppError.DELETE_PURCHASE_ORDER,
        success: AppSuccess.DELETE_PURCHASE_ORDER
    })
    delete(equipment: PurchaseOrder) {
        return this.purchaseOrderApi.delete(equipment.id).then(() => {
            this.table.delete((x) => +x.id === +equipment.id);
        });
    }
}
