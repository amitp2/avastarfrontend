import { Component, OnInit } from '@angular/core';
import { PurchaseOrderStatus, PurchaseOrderItem, PurchaseOrder, PurchaseOrderType } from '../../types';
import { FormHandler } from '../../../../classes/form-handler';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { PurchaseOrderService } from '../../../../services/resources/purchase-order.service';
import { UpdatePurchaseOrderItemEvent } from '../equipment-item-form/equipment-item-form.component';
import { CurrentPropertyService } from '../../../../services/current-property.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { VenueSettingsModel } from '../../../../models/venue-settings-model';
import { PromisedStoreService } from '../../../../services/promised-store.service';
import { VenueSettingsResourceService } from '../../../../services/resources/venue-settings-resource.service';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { VendorContactFetchFactory } from '../../../select/fetches/factories/vendor-contact.fetch.factory';
import { EventFetchFactory } from '../../../select/fetches/factories/event.fetch.factory';
import { currencyStringToNumber } from '../../../../helpers/functions/currency-string-to-number';

@Component({
    selector: 'app-manage-purchase-order-form',
    templateUrl: './manage-purchase-order-form.component.html',
    styleUrls: ['./manage-purchase-order-form.component.styl']
})
export class ManagePurchaseOrderFormComponent extends FormHandler<PurchaseOrder> {
    PurchaseOrderStatus: typeof PurchaseOrderStatus = PurchaseOrderStatus;
    PurchaseOrderType: typeof PurchaseOrderType = PurchaseOrderType;
    items: Partial<PurchaseOrderItem>[] = [];
    poNumber: number;
    poPrefix: string;

    SelectFetchType = SelectFetchType;
    defaultDate = new Date(null);
    //defaultDate.toString();

    constructor(formBuilder: FormBuilder,
                private activatedRoute: ActivatedRoute,
                private purchaseOrderApi: PurchaseOrderService,
                private propertyApi: CurrentPropertyService,
                private router: Router,
                private venueSettings: VenueSettingsResourceService,
                private promisedStore: PromisedStoreService,
                public vendorContactFetchFactory: VendorContactFetchFactory,
                public eventFetchFactory: EventFetchFactory,

                private route:ActivatedRoute
                ) {
        super(formBuilder);
        this.fetchPeoPrefix();
    }

    get today() {
        return new Date();
    }

    isInvalidDate(date) {
        let invalidDate = new Date("1970-01-01")
        return invalidDate.toString() !== date.toString();
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    async fetchPeoPrefix(): Promise<any> {
        const venueId = await this.promisedStore.currentVenueId();
        const settings = await this.venueSettings.getForVenue(venueId);
        this.poPrefix = settings.po;
    }

    initializeForm(): FormGroup {
        const form = this.formBuilder.group({
            eventId: null,
            vendorId: [null, Validators.required],
            contactId: [null, Validators.required],
            accountId: null,
            status: [null, Validators.required],
            type: [null, Validators.required],
            month: [null, Validators.required],
            year: [null, Validators.required],
            notes: ['', Validators.maxLength(512)],
            invoiceNumber: '',
            deliveryCharges: '',
            tax:'',
            orderBy:['', Validators.maxLength(512)]
        });

        this.bindListeners(form);

        return form;
    }

    bindListeners(form: FormGroup): any {

    }

    @AsyncMethod({
        taskName: 'get_purchase_order'
    })
    get(id: number): Promise<PurchaseOrder> {
        return this.purchaseOrderApi.get(id).then(order => {
            this.items = [...order.purchaseOrderItems];
            this.poNumber = order.poNumber || order.id;
            return order;
        });
    }

    @AsyncMethod({
        taskName: 'add_purchase_order',
        success: AppSuccess.ADD_PURCHASE_ORDER,
        error: AppError.ADD_PURCHASE_ORDER
    })
    add(model: PurchaseOrder): Promise<any> {
        model.purchaseOrderItems = this.items;
        return this.propertyApi.currentVenueAsync()
            .then(venue => this.purchaseOrderApi.addByVenueId(venue.id, model))
            .then(order => this.router.navigateByUrl(`u/manage-purchase-orders/${order.id}`));
    }

    @AsyncMethod({
        taskName: 'edit_purchase_order',
        success: AppSuccess.EDIT_PURCHASE_ORDER,
        error: AppError.EDIT_PURCHASE_ORDER
    })
    edit(id: number, model: PurchaseOrder): Promise<any> {
        model.purchaseOrderItems = this.items;
        return this.purchaseOrderApi.edit(id, model)
            .then(order => this.router.navigateByUrl(`u/manage-purchase-orders/${order.id}`));
    }

    serialize(model: PurchaseOrder) {
        const serialized = <any> model.toJson();
        serialized.month = `${serialized.month}`;
        return serialized;
    }

    deserialize(formValue: any) {
        const po = PurchaseOrder.fromJson(formValue);
        po.deliveryCharges = <any> currencyStringToNumber(formValue.deliveryCharges);
        return po;
    }

    onUpdateItem(updateEvent: UpdatePurchaseOrderItemEvent) {
        this.items.splice(updateEvent.index, 1, updateEvent.model);
        this.form.markAsDirty();
    }

    onCreateItem(model: PurchaseOrderItem) {
        this.items.push(model);
        this.form.markAsDirty();
    }

    deleteItem(model) {
        this.items.splice(this.items.indexOf(model), 1);
        this.form.markAsDirty();
    }

    cancel() {
        if (this.form.dirty && !confirm('Your changes will be lost. Do you want to continue?')) {
            return;
        }
        this.router.navigateByUrl('/u/manage-purchase-orders');
    }

    //manage purchase order screen cloned the form on basic of item id.
    ngAfterViewInit() {
        this.checkCopyRequest();
    }

    async checkCopyRequest() {
        try {
            const copy = this.route.snapshot.queryParamMap.get('copy');
            if (!copy) {
                return;
            }
            const purchaseOrderCloneForm = await this.purchaseOrderApi.get(+copy);
            const formValue: any = [];
            if (purchaseOrderCloneForm.id) {
                    formValue.vendorId = +purchaseOrderCloneForm.vendorId;
                    formValue.accountId = +purchaseOrderCloneForm.accountId;
                    formValue.contactId = +purchaseOrderCloneForm.contactId;
                    formValue.contactName = +purchaseOrderCloneForm.contactName;
                    formValue.deliveryCharges = +purchaseOrderCloneForm.deliveryCharges;
                    formValue.eventName = +purchaseOrderCloneForm.eventName;
                    //formValue.eventId = +purchaseOrderCloneForm.eventId;
                    formValue.invoiceNumber = purchaseOrderCloneForm.invoiceNumber;
                    formValue.month = +purchaseOrderCloneForm.month;
                    formValue.notes = purchaseOrderCloneForm.notes;
                    formValue.status = purchaseOrderCloneForm.status;
                    formValue.tax =  purchaseOrderCloneForm.tax;
                    formValue.type =  purchaseOrderCloneForm.type;
                    formValue.year = +purchaseOrderCloneForm.year;

                    formValue.orderBy  = +purchaseOrderCloneForm.orderBy;
                    this.items = [...purchaseOrderCloneForm.purchaseOrderItems];
            }
            this.form.patchValue(formValue);
            this.form.markAsPristine();
        } catch (e) {
            console.log(e)
        }
    }


}
