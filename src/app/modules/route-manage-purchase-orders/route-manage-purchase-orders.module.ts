import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagePurchaseOrdersComponent } from './components/manage-purchase-orders/manage-purchase-orders.component';
import { ManagePurchaseOrderFormComponent } from './components/manage-purchase-order-form/manage-purchase-order-form.component';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { PurchaseOrderService } from '../../services/resources/purchase-order.service';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { EquipmentItemFormComponent } from './components/equipment-item-form/equipment-item-form.component';
import { SelectModule } from '../select/select.module';
import { TableResizeModule } from '../table-resize/table-resize.module';
import { MaskedInputModule } from '../masked-input/masked-input.module';

const routes: Routes = [{
    path: '',
    component: ManagePurchaseOrdersComponent
}, {
    path: ':id',
    component: ManagePurchaseOrderFormComponent
}];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        PagingTableModule,
        ReactiveFormsModule,
        MaskedInputModule,
        TableResizeModule,
        RouterModule.forChild(routes),
        SelectModule
    ],
    declarations: [
        ManagePurchaseOrdersComponent,
        ManagePurchaseOrderFormComponent,
        EquipmentItemFormComponent
    ]
})
export class RouteManagePurchaseOrdersModule { }
