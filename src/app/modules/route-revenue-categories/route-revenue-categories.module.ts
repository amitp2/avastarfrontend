import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { RevenueCategoryComponent } from './components/revenue-category/revenue-category.component';
import { RevenueCategoryFormComponent } from './components/revenue-category-form/revenue-category-form.component';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { ReactiveFormsModule } from '@angular/forms';
import { BoolTextPipe } from './pipes/bool-text.pipe';
import { AllowOnlyService } from '../../services/guards/allow-only.service';
import { UserRole } from '../../enums/user-role.enum';
import { TableResizeModule } from '../table-resize/table-resize.module';

const routes: Routes = [
    {
        path: '',
        component: RevenueCategoryComponent,
        canActivate: [AllowOnlyService],
        data: {
            roles: [UserRole.ADMIN]
        },
        children: [
            {path: ':id', component: RevenueCategoryFormComponent}
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes),
        PagingTableModule,
        TableResizeModule,
        ReactiveFormsModule
    ],
    declarations: [RevenueCategoryComponent, RevenueCategoryFormComponent, BoolTextPipe]
})
export class RouteRevenueCategoriesModule {
}
