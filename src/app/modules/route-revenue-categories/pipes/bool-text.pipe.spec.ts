import { BoolTextPipe } from './bool-text.pipe';

describe('BoolTextPipe', () => {
    it('create an instance', () => {
        const pipe = new BoolTextPipe();
        expect(pipe).toBeTruthy();
    });
    it('should return "Yes" for boolean [true]', () => {
        const pipe = new BoolTextPipe();
        expect(pipe.transform(true)).toEqual('Yes');
    });
    it('should return "No" for boolean [false]', () => {
        const pipe = new BoolTextPipe();
        expect(pipe.transform(false)).toEqual('No');
    });
    it('should return empty string for [null] and [undefined]', () => {
        const pipe = new BoolTextPipe();
        expect(pipe.transform(null)).toEqual('');
        expect(pipe.transform(undefined)).toEqual('');
    });
});
