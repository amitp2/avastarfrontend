import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'boolText'
})
export class BoolTextPipe implements PipeTransform {

    transform(value: any, args?: any): any {
        if (value === null || value == undefined) {
            return '';
        }
        if (value === true) {
            return 'Yes';
        }
        if (value === false) {
            return 'No';
        }
        throw new Error('Invalid data type provided');
    }

}
