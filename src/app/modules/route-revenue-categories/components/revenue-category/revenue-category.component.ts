import { Component, OnInit, ViewChild } from '@angular/core';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { AuthService } from '../../../../services/auth.service';
import { UtilsService } from '../../../../services/utils.service';
import { ActivatedRoute } from '@angular/router';
import { RevenueCategoryResourceService } from '../../../../services/resources/revenue-category-resource.service';
import { TableHandler } from '../../../../classes/table-handler';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';

@Component({
    selector: 'app-revenue-category',
    templateUrl: './revenue-category.component.html',
    styleUrls: ['./revenue-category.component.styl']
})
export class RevenueCategoryComponent extends TableHandler {

    @ViewChild('table') table: PagingTableComponent;

    constructor(
        private revenueCategory: RevenueCategoryResourceService,
        private authService: AuthService,
        private utils: UtilsService,
        activatedRoute: ActivatedRoute
    ) {
        super(activatedRoute);
    }

    getTable(): PagingTableComponent {
        return this.table;
    }

    loadFunction(): any {
        return (params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return this.revenueCategory.queryBySubscriberId(
                this.utils.tableToHttpParams(params),
                this.authService.currentUserSnapshot.subscriberId
            );
        };
    }

}
