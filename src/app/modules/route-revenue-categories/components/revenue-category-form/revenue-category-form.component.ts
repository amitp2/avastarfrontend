import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../../../../services/utils.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../../services/auth.service';
import { RevenueCategoryResourceService } from '../../../../services/resources/revenue-category-resource.service';
import { RevenueCategoryModel } from '../../../../models/revenue-category-model';
import { FormHandler } from '../../../../classes/form-handler';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';

@Component({
    selector: 'app-revenue-category-form',
    templateUrl: './revenue-category-form.component.html',
    styleUrls: ['./revenue-category-form.component.styl']
})
export class RevenueCategoryFormComponent extends FormHandler<RevenueCategoryModel> {

    constructor(
        private activatedRoute: ActivatedRoute,
        private revenueCategory: RevenueCategoryResourceService,
        private authService: AuthService,
        private router: Router,
        private utils: UtilsService
    ) {
        super();
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    initializeForm(): FormGroup {
        return new FormGroup({
            revenueCategoryName: new FormControl({ value: null, disabled: true }),
            revenueCategoryId: new FormControl(),
            revenueCode: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
            discountable: new FormControl(),
            serviceCharge: new FormControl()
        });
    }

    @AsyncMethod({
        taskName: 'get_revenue_category'
    })
    get(id: number): Promise<RevenueCategoryModel> {
        return this.revenueCategory.getBySubscriberId(id, this.authService.currentUserSnapshot.subscriberId);
    }

    add(model: RevenueCategoryModel): Promise<any> {
        return undefined;
    }

    @AsyncMethod({
        taskName: 'update_revenue_category',
        success: AppSuccess.REVENUE_CATEGORY_EDIT,
        error: AppError.REVENUE_CATEGORY_EDIT
    })
    edit(id: number, model: RevenueCategoryModel): Promise<any> {
        return this.revenueCategory
            .updateForSubscriber(model, this.authService.currentUserSnapshot.subscriberId)
            .then(() => this.router.navigateByUrl(`/u/revenue-category?fresh=${Date.now()}`));
    }

    serialize(model: RevenueCategoryModel): any {
        return this.utils.transformInto(model, {
            revenueCategoryName: { to: 'revenueCategoryName' },
            revenueCategoryId: { to: 'revenueCategoryId' },
            revenueCode: { to: 'revenueCode' },
            discountable: { to: 'discountable' },
            serviceCharge: { to: 'serviceCharge' }
        });
    }

    deserialize(formValue: any): RevenueCategoryModel {
        return new RevenueCategoryModel(this.utils.transformInto(formValue, {
            revenueCategoryName: { to: 'revenueCategoryName' },
            revenueCategoryId: { to: 'revenueCategoryId' },
            revenueCode: { to: 'revenueCode' },
            discountable: { to: 'discountable' },
            serviceCharge: { to: 'serviceCharge' }
        }));
    }

}
