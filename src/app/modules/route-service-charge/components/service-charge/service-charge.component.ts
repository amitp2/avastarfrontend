import { Component, OnInit, ViewChild } from '@angular/core';
import { TableHandler } from '../../../../classes/table-handler';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { ServiceChargeResourceService } from '../../../../services/resources/service-charge-resource.service';
import { AuthService } from '../../../../services/auth.service';
import { UtilsService } from '../../../../services/utils.service';
import { ActivatedRoute } from '@angular/router';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { ServiceChargeModel } from '../../../../models/service-charge-model';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';

@Component({
    selector: 'app-service-charge',
    templateUrl: './service-charge.component.html',
    styleUrls: ['./service-charge.component.styl']
})
export class ServiceChargeComponent extends TableHandler {

    @ViewChild('table') table: PagingTableComponent;

    constructor(
        private serviceCharge: ServiceChargeResourceService,
        private authService: AuthService,
        private utils: UtilsService,
        activatedRoute: ActivatedRoute
    ) {
        super(activatedRoute);
    }

    getTable(): PagingTableComponent {
        return this.table;
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return this.serviceCharge.queryBySubscriberId(
                this.utils.tableToHttpParams(params),
                this.authService.currentUserSnapshot.subscriberId
            );
        }).bind(this);
    }

    @AsyncMethod({
        taskName: 'delete_service_charge'
    })
    remove(item: ServiceChargeModel) {
        if (confirm('Do you want to delete service charge?')) {
            return this.serviceCharge.delete(item.id).then(() => {
                this.getTable().refresh();
            });
        }
        return Promise.resolve();
    }

}
