import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceChargeModel } from '../../../../models/service-charge-model';
import { ServiceChargeResourceService } from '../../../../services/resources/service-charge-resource.service';
import { AuthService } from '../../../../services/auth.service';
import { UtilsService } from '../../../../services/utils.service';
import { FormHandler } from '../../../../classes/form-handler';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError, AppErrorDefaultMessages } from '../../../../enums/app-error.enum';
import { RevenueCategoryResourceService } from '../../../../services/resources/revenue-category-resource.service';
import { RevenueCategoryModel } from '../../../../models/revenue-category-model';
import { ServiceChargeStatus } from '../../../../enums/service-charge.enum';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-service-charge-form',
    templateUrl: './service-charge-form.component.html',
    styleUrls: ['./service-charge-form.component.styl']
})
export class ServiceChargeFormComponent extends FormHandler<ServiceChargeModel> {

    revenueCategoryList: RevenueCategoryModel[] = [];
    serviceChargeStatus: typeof ServiceChargeStatus = ServiceChargeStatus;

    constructor(
        private activatedRoute: ActivatedRoute,
        private serviceCharge: ServiceChargeResourceService,
        private revenueCategory: RevenueCategoryResourceService,
        private authService: AuthService,
        private router: Router,
        private utils: UtilsService,
        private toastrService: ToastrService,
        fb: FormBuilder
    ) {
        super(fb);

        this.loadRevenueCategories();
    }

    loadRevenueCategories() {
        const httpQueryParams = {
            page: 0,
            size: 999999,
            sortFields: [],
            sortAsc: true
        };

        this.revenueCategory.queryBySubscriberId(httpQueryParams, this.authService.currentUserSnapshot.subscriberId)
            .then((response) => this.revenueCategoryList = response.items.filter(x => x.revenueCode !== null));
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    initializeForm(): FormGroup {
        const form = this.formBuilder.group({
            name: ['', [Validators.required]],
            charge: ['', [Validators.required, Validators.min(0), Validators.max(100)]],
            taxRate:['', [Validators.min(0), Validators.max(100)]],
            startDate: ['', Validators.required],
            endDate: ['', [Validators.required]],
            revenueCategoryId: [null, []],
            status: ['', [Validators.required]]
        });
        form.get('status').disable();
        return form;
    }

    submit() {
        const startDate = new Date(this.form.get('startDate').value);
        const endDate = new Date(this.form.get('endDate').value);
        if (startDate > endDate) {
            this.toastrService.error(AppErrorDefaultMessages[AppError.ENDING_DATE], 'Error', {
                positionClass: 'toast-bottom-right',
                tapToDismiss: false
            });
            return;
        }
        super.submit();
    }

    @AsyncMethod({
        taskName: 'get_service_charge'
    })
    get(id: number): Promise<ServiceChargeModel> {
        return this.serviceCharge.get(id);
    }

    @AsyncMethod({
        taskName: 'add_service_charge',
        success: AppSuccess.SERVICE_CHARGE_ADD
    })
    add(model: ServiceChargeModel): Promise<any> {
        return this.serviceCharge.addBySubscriberId(model, this.authService.currentUserSnapshot.subscriberId).then(() => {
            return this.router.navigateByUrl(`/u/service-charge?fresh=${Date.now()}`);
        })
            .catch(err => {
                if (err.status === 409) {
                    throw AppError.SERVICE_CHARGE_DATES;
                }
                throw AppError.SERVICE_CHARGE_ADD;
            });
    }

    @AsyncMethod({
        taskName: 'update_service_charge',
        success: AppSuccess.SERVICE_CHARGE_EDIT
    })
    edit(id: number, model: ServiceChargeModel): Promise<any> {
        console.log("Edit", model);
        return this.serviceCharge.edit(id, model)
            .then(() => this.router.navigateByUrl(`/u/service-charge?fresh=${Date.now()}`))
            .catch(err => {
                if (err.status === 409) {
                    throw AppError.SERVICE_CHARGE_DATES;
                }
                throw AppError.SERVICE_CHARGE_EDIT;
            });
    }

    serialize(model: ServiceChargeModel): any {
        return this.utils.transformInto(model, {
            name: { to: 'name' },
            charge: { to: 'charge' },
            taxRate: { to: 'taxRate' },
            status: { to: 'status' },
            endDate: { to: 'endDate' },
            startDate: { to: 'startDate' },
            revenueCategoryId: { to: 'revenueCategoryId' }
        });
    }

    deserialize(formValue: any): ServiceChargeModel {
        return new ServiceChargeModel(this.utils.transformInto(formValue, {
            name: { to: 'name' },
            charge: { to: 'charge' },
            taxRate: { to: 'taxRate' },
            status: { to: 'status' },
            endDate: { to: 'endDate' },
            startDate: { to: 'startDate' },
            revenueCategoryId: { to: 'revenueCategoryId' }
        }));
    }

}
