import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceChargeComponent } from './components/service-charge/service-charge.component';
import { ServiceChargeFormComponent } from './components/service-charge-form/service-charge-form.component';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { ServiceChargeStatusTextPipe } from './pipes/service-charge-status-text.pipe';
import { RevenueTextPipe } from './pipes/revenue-text.pipe';
import { AllowOnlyService } from '../../services/guards/allow-only.service';
import { UserRole } from '../../enums/user-role.enum';
import { TableResizeModule } from '../table-resize/table-resize.module';

const routes: Routes = [
    {
        path: '',
        component: ServiceChargeComponent,
        canActivate: [AllowOnlyService],
        data: {
            roles: [UserRole.ADMIN]
        },
        children: [
            { path: ':id', component: ServiceChargeFormComponent }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        TableResizeModule,
        PagingTableModule
    ],
    declarations: [
        ServiceChargeComponent,
        ServiceChargeFormComponent,
        ServiceChargeStatusTextPipe,
        RevenueTextPipe
    ]
})
export class RouteServiceChargeModule {
}
