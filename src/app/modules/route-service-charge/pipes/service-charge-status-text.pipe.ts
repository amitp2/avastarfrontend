import { Pipe, PipeTransform } from '@angular/core';
import { ServiceChargeModel } from '../../../models/service-charge-model';
import { ServiceChargeStatus } from '../../../enums/service-charge.enum';

@Pipe({
    name: 'serviceChargeStatusText'
})
export class ServiceChargeStatusTextPipe implements PipeTransform {

    transform(status: ServiceChargeStatus, serviceCharge: ServiceChargeModel): string {
        if (status === ServiceChargeStatus.Inactive) {
            return 'Inactive';
        }

        const endDate = new Date(serviceCharge.endDate);
        const today = new Date();

        today.setMilliseconds(0);
        today.setMinutes(0);
        today.setHours(0);

        return endDate < today ? 'Expired' : 'Active';
    }

}
