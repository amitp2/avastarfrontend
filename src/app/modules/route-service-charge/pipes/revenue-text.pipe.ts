import { Pipe, PipeTransform } from '@angular/core';
import { RevenueCategoryModel } from '../../../models/revenue-category-model';

@Pipe({
    name: 'revenueText'
})
export class RevenueTextPipe implements PipeTransform {

    transform(revenue: RevenueCategoryModel): any {
        if (revenue.revenueCode) {
            return `${revenue.revenueCode} - ${revenue.revenueCategoryName}`;
        }
        return revenue.revenueCategoryName;
    }

}
