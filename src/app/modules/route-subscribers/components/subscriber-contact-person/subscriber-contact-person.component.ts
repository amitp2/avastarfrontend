import { Component } from '@angular/core';
import { SubscriberContactResourceService } from '../../../../services/resources/subscriber-contact-resource.service';
import { ResourceQueryResponse } from '../../../../interfaces/resource-query-response';
import { SubscriberContactModel } from '../../../../models/subscriber-contact-model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserRole } from '../../../../enums/user-role.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { AsyncTasksService } from '../../../../services/async-tasks.service';
import { AppError } from '../../../../enums/app-error.enum';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { SubscriberResourceService } from '../../../../services/resources/subscriber-resource.service';
import { SubscriberModel } from '../../../../models/subscriber-model';
import { Location } from '@angular/common';
import { UtilsService } from '../../../../services/utils.service';

@Component({
    selector: 'app-subscriber-contact-person',
    templateUrl: './subscriber-contact-person.component.html',
    styleUrls: ['./subscriber-contact-person.component.styl']
})
export class SubscriberContactPersonComponent {

    id: any;
    edit: boolean;
    form: FormGroup;
    userRole = UserRole;
    contactId: number;
    currentSubscriber: Promise<SubscriberModel>;
    userRoles = [
        { id: UserRole.NONE, text: this.utilsService.userRoleToDisplayString(UserRole.NONE) },
        { id: UserRole.ADMIN, text: this.utilsService.userRoleToDisplayString(UserRole.ADMIN) },
        { id: UserRole.TECH_TEAM_LEAD, text: this.utilsService.userRoleToDisplayString(UserRole.TECH_TEAM_LEAD) },
        { id: UserRole.TECH_TEAM, text: this.utilsService.userRoleToDisplayString(UserRole.TECH_TEAM) },
        { id: UserRole.EVENT_PLANNER, text: this.utilsService.userRoleToDisplayString(UserRole.EVENT_PLANNER) }
    ];

    constructor(
        private subscriberContactResourceService: SubscriberContactResourceService,
        private activatedRoute: ActivatedRoute,
        private asyncTasksService: AsyncTasksService,
        private subscriberResource: SubscriberResourceService,
        private router: Router,
        private utilsService: UtilsService,
        private location: Location
    ) {
        this.edit = false;

        this.currentSubscriber = this.subscriberResource.get(this.activatedRoute.snapshot.parent.params['id']);

        const contactLoadTask = 'contact_load_task';
        this.asyncTasksService.taskStart(contactLoadTask);
        this.activatedRoute.parent.params.subscribe((params: any) => {
            const val = params.id;
            this.id = val;
            this.subscriberContactResourceService.queryBySubscriberId({
                page: 0,
                size: 100,
                sortFields: ['id'],
                sortAsc: true
            }, +val)
                .then((resp: ResourceQueryResponse<SubscriberContactModel>) => {
                    this.asyncTasksService.taskSuccess(contactLoadTask);
                    this.edit = resp.total !== 0;
                    if (this.edit) {
                        this.contactId = resp.items[0].id;
                        const item: any = {
                            ...resp.items[0]
                        };
                        console.log(item);
                        delete item.id;
                        this.form.patchValue(item);
                    }
                })
                .catch(() => {
                    // this.asyncTasksService.taskError(contactLoadTask, AppError.SUBSCRIBER_CONTACT_PERSON_LOAD);
                });
        }, () => {
            // this.asyncTasksService.taskError(contactLoadTask, AppError.SUBSCRIBER_CONTACT_PERSON_LOAD);
        });

        this.form = new FormGroup({
            firstName: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            lastName: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            title: new FormControl(null, [Validators.maxLength(100)]),
            email: new FormControl(null, [Validators.required, Validators.email, Validators.maxLength(100)]),
            mobile: new FormControl(null, [Validators.maxLength(30)]),
            phone: new FormControl(null, [Validators.required, Validators.maxLength(30)]),
            role: new FormControl(null, []),
            notes: new FormControl(null, [Validators.maxLength(400)]),
            fax: new FormControl(null, [Validators.maxLength(30)]),
            department: new FormControl(null, [Validators.maxLength(250)])
        });
    }

    goBack() {
        this.location.back();
    }

    submit() {
        const contactPersonModifyTask = 'subscriber_modify_task';
        const model = new SubscriberContactModel({
            ...this.form.value,
            role: this.form.value.role
        });

        if (this.edit) {
            this.asyncTasksService.taskStart(contactPersonModifyTask);
            this.subscriberContactResourceService
                .edit(this.contactId, model)
                .then(() => this.asyncTasksService.taskSuccess(contactPersonModifyTask, AppSuccess.SUBSCRIBER_CONTACT_PERSON_UPDATE))
                .then(() => this.goBack())
                // .then(() => this.router.navigateByUrl('/u/subscribers?fresh=' + Date.now()))
                .catch(() => this.asyncTasksService.taskError(contactPersonModifyTask, AppError.SUBSCRIBER_CONTACT_PERSON_UPDATE));

        } else {
            this.asyncTasksService.taskStart(contactPersonModifyTask);
            this.subscriberContactResourceService
                .addForSubscriber(model, +this.id)
                .then(() => this.asyncTasksService.taskSuccess(contactPersonModifyTask, AppSuccess.SUBSCRIBER_CONTACT_PERSON_UPDATE))
                .then(() => this.goBack())
                // .then(() => this.router.navigateByUrl('/u/subscribers?fresh=' + Date.now()))
                .catch(() => this.asyncTasksService.taskError(contactPersonModifyTask, AppError.SUBSCRIBER_CONTACT_PERSON_UPDATE));
        }
    }

}
