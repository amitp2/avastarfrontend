import { Component, OnInit } from '@angular/core';
import { ProductResourceService } from '../../../../services/resources/product-resource.service';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { UtilsService } from '../../../../services/utils.service';
import { ActivatedRoute } from '@angular/router';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { PagingTableService } from '../../../../services/paging-table.service';
import { ProductModel } from '../../../../models/product-model';
import { productModelCloner } from '../../../../helpers/cloners/product-model.cloner';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { Debounce } from '../../../../decorators/debounce.decorator';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';
import { selectSubscriberProductAccess, selectSubscriberProductUsage } from '../../../../store/selectors';
import {
    SetSubscriberProductAccess,
    SetSubscriberProductAccessForProduct
} from '../../../../store/subscriber-product-access/subscriber-product-access.actions';
import { ListToSubscriberAccessMapper } from '../../../../mappers/list-to-subscriber-access.mapper';
import { SetSubscriberProductUsage } from '../../../../store/subscriber-product-usage/subscriber-product-usage.actions';

@Component({
    selector: 'app-access',
    templateUrl: './access.component.html',
    styleUrls: ['./access.component.styl'],
    providers: [PagingTableService]
})
export class AccessComponent implements OnInit {

    columns: PagingTableHeaderColumn[] = [
        {isSortable: false, title: 'Product', key: 'actions'},
        {isSortable: false, title: 'Has Access', key: 'product'},
        {isSortable: false, title: 'Number of licensed users', key: 'actions'}
    ];

    get subscriberId(): any {
        return this.route.parent.snapshot.params.id;
    }

    constructor(private productResource: ProductResourceService,
                private utils: UtilsService,
                private route: ActivatedRoute,
                private store: Store<AppState>,
                private mapper: ListToSubscriberAccessMapper) {
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            const subscriberId = this.subscriberId;
            const httpParams = this.utils.tableToHttpParams(params);
            return this.productResource.findBySubscriberId(subscriberId, httpParams).then(resp => {
                this.store.dispatch(new SetSubscriberProductAccess({
                    subscriberId,
                    access: this.mapper.map(resp.items)
                }));
                return resp;
            });
        }).bind(this);
    }

    ngOnInit() {

        this.productResource.usageBySubscriber(this.subscriberId).then(res => {
            this.store.dispatch(new SetSubscriberProductUsage({
                subscriberId: this.subscriberId,
                usage: res
            }));
        });
    }

    @AsyncMethod({
        taskName: 'access_update',
        success: AppSuccess.ACCESS_UPDATED,
        error: AppError.ACCESS_UPDATED
    })
    changedAccess(event: any, product: ProductModel) {
        const hasAccess = event.target.value === 'true';
        const cloned = productModelCloner(product);
        cloned.hasAccess = hasAccess;
        product.hasAccess = hasAccess;
        return this.productResource.edit(cloned.id, cloned);
    }

    @Debounce(500)
    @AsyncMethod({
        taskName: 'access_update',
        success: AppSuccess.ACCESS_UPDATED,
        error: AppError.ACCESS_UPDATED
    })
    changedCount(event: any, product: ProductModel) {
        const cloned = productModelCloner(product);
        const count = +event.target.value;
        cloned.licensedUsersCount = count;
        product.licensedUsersCount = count;

        this.store.dispatch(new SetSubscriberProductAccessForProduct({
            subscriberId: this.subscriberId,
            product: cloned.product,
            count: cloned.licensedUsersCount
        }));

        return this.productResource.edit(cloned.id, cloned);
    }

}
