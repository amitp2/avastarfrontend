import { Component, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AccountType } from '../../../../enums/account-type.enum';
import { SubscriberStatus } from '../../../../enums/subscriber-status.enum';
import { SubscriberResourceService } from '../../../../services/resources/subscriber-resource.service';
import { SubscriberModel } from '../../../../models/subscriber-model';
import { ActivatedRoute, Router } from '@angular/router';

import { SubscriberLogoUploaderService } from '../../services/subscriber-logo-uploader.service';
import { UploadType } from '../../../../enums/upload-type.enum';
import { FormMode } from '../../../../enums/form-mode.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';

import 'rxjs/add/operator/concatMap';
import 'rxjs/add/operator/combineLatest';
import { CountryModel } from '../../../../models/country-model';
import { StateModel } from '../../../../models/state-model';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';
import { countryByCode, stateByCode, userLocation } from '../../../../store/selectors';
import { GeolocationDataModel } from '../../../../models/geolocation-data.model';
import { UtilsService } from '../../../../services/utils.service';

// TODO: refactoring needed
@Component({
    selector: 'app-subscriber-info',
    templateUrl: './subscriber-info.component.html',
    styleUrls: ['./subscriber-info.component.styl'],
    providers: [SubscriberLogoUploaderService]
})
export class SubscriberInfoComponent implements OnDestroy {

    id: any;

    form: FormGroup;
    accountType = AccountType;
    status = SubscriberStatus;

    uploader: any;
    mode: FormMode;

    modeObs: BehaviorSubject<FormMode>;
    modeObsSubscription: Subscription;
    initialisedObs: BehaviorSubject<boolean>;
    initialisedItems: number;
    countries: CountryModel[];
    states: StateModel[];
    SelectFetchType = SelectFetchType;
    submitting = false;

    constructor(
        private subscriberResourceService: SubscriberResourceService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private logoUploader: SubscriberLogoUploaderService,
        private utils: UtilsService,
        private store: Store<AppState>
    ) {

        this.uploader = logoUploader;
        this.initialisedItems = 0;
        this.modeObs = new BehaviorSubject<FormMode>(FormMode.Add);
        this.initialisedObs = new BehaviorSubject<boolean>(false);

        this.form = new FormGroup({
            name: new FormControl(null, [Validators.required]),
            accountType: new FormControl(AccountType.Client),
            status: new FormControl(false),
            address: new FormControl(null, [Validators.required]),
            address2: new FormControl(null),
            city: new FormControl(null, [Validators.required]),
            stateId: new FormControl(),
            postCode: new FormControl(null, [Validators.required, Validators.maxLength(10)]),
            countryId: new FormControl(null, [Validators.required]),
            branch: new FormControl(),
            phone: new FormControl(null, [Validators.required]),
            fax: new FormControl(null),
            website: new FormControl(),
            agreement: new FormControl(true),
            logoUrl: new FormControl()
        });

        this.activatedRoute.parent.params.subscribe((params: any) => {
            this.id = params.id;
            if (this.id !== 'add') {
                this.mode = FormMode.Edit;
            } else {
                this.mode = FormMode.Add;
                this.uploader.uploadType = UploadType.Add;
                this.populateForm();
            }
            this.modeObs.next(this.mode);
        });

        this.modeObsSubscription = this.modeObs
            .combineLatest(this.initialisedObs.filter(inited => inited))
            .map(combined => combined[0])
            .subscribe((mode: FormMode) => {
                if (mode === FormMode.Edit) {
                    this.load();
                    return;
                }
            });

        this.form.get('stateId').disable();

        this.form.get('countryId').valueChanges.subscribe((val: number) => {
            if (+val === 233) {
                this.form.get('stateId').enable();
            } else {
                this.form.get('stateId').disable();
            }
        });
    }

    private async populateForm() {
        const location: GeolocationDataModel = await this.utils.toPromise(this.store.select(userLocation()));
        if (location.countryCode) {
            const country: CountryModel = await this.utils.toPromise(this.store.select(countryByCode(location.countryCode)));
            this.form.get('countryId').setValue(country.id);
        }
        // if (location.regionCode) {
        //     const state: StateModel = await this.utils.toPromise(this.store.select(stateByCode(location.regionCode)));
        //     if (state) {
        //         this.form.get('stateId').setValue(state.id);
        //     }
        // }
        // if (location.city) {
        //     this.form.get('city').setValue(location.city);
        // }
        // if (location.zipCode) {
        //     this.form.get('postCode').setValue(location.zipCode);
        // }
        this.form.markAsPristine();
    }

    countriesAreLoaded(countries: CountryModel[]) {
        this.initialisedItems++;
        this.countries = countries;
        if (this.initialisedItems === 2) {
            this.initialisedObs.next(true);
        }
    }

    statesAreLoaded(states: StateModel[]) {
        this.initialisedItems++;
        this.states = states;
        if (this.initialisedItems === 2) {
            this.initialisedObs.next(true);
        }
    }

    submit() {
        if (this.submitting) {
            return;
        }

        this.submitting = true;

        const newModel = this.deserialize(this.form.value);
        if (this.mode === FormMode.Add) {
            this.add(newModel)
                .then(() => this.submitting = false)
                .then(() => this.router.navigateByUrl('/u/subscribers?fresh=' + Date.now()));
            return;
        }
        this.update(+this.id, newModel)
            .then(() => this.submitting = false)
            .then(() => this.router.navigateByUrl('/u/subscribers?fresh=' + Date.now()));
    }

    deserialize(formValue: any): SubscriberModel {
        const modelVal: any = {
            ...formValue
        };
        modelVal.status = formValue.status ? SubscriberStatus.Active : SubscriberStatus.Locked;
        modelVal.masterAgreementAgreed = formValue.agreement;
        return new SubscriberModel(modelVal);
    }

    serialize(model: SubscriberModel): any {
        return {
            name: model.name,
            accountType: model.accountType,
            status: model.status === SubscriberStatus.Active ? true : false,
            address: model.address,
            address2: model.address2,
            city: model.city,
            stateId: model.stateId,
            postCode: model.postCode,
            countryId: model.countryId,
            branch: model.branch,
            phone: model.phone,
            fax: model.fax,
            website: model.website,
            agreement: model.masterAgreementAgreed,
            logoUrl: model.logoUrl
        };
    }

    @AsyncMethod({
        taskName: 'SUBSCRIBER_LOAD',
        success: AppSuccess.SUBSCRIBER_LOAD,
        error: AppError.SUBSCRIBER_LOAD
    })
    load() {
        return this.subscriberResourceService
            .get(+this.id)
            .then((subscriber: SubscriberModel) => {
                this.uploader.uploadType = UploadType.Update;
                this.uploader.oldUrl = subscriber.logoUrl;
                this.form.setValue(this.serialize(subscriber));
            });
    }

    @AsyncMethod({
        taskName: 'SUBSCRIBER_ADD',
        success: AppSuccess.SUBSCRIBER_ADD,
        error: AppError.SUBSCRIBER_ADD
    })
    add(model: SubscriberModel) {
        return this.subscriberResourceService.add(model);
    }

    @AsyncMethod({
        taskName: 'SUBSCRIBER_UPDATE',
        success: AppSuccess.SUBSCRIBER_INFO_UPDATE,
        error: AppError.SUBSCRIBER_UPDATED_ERROR
    })
    update(id: number, model: SubscriberModel) {
        return this.subscriberResourceService.edit(id, model);
    }

    ngOnDestroy() {
        this.modeObsSubscription.unsubscribe();
    }

}
