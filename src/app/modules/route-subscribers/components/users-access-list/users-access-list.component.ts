import { Component, OnInit, ViewChild } from '@angular/core';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { UserResourceService } from '../../../../services/resources/user-resource.service';
import { UtilsService } from '../../../../services/utils.service';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { ActivatedRoute } from '@angular/router';
import { PagingTableService } from '../../../../services/paging-table.service';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';
import { ProductResourceService } from '../../../../services/resources/product-resource.service';
import { ProductModel } from '../../../../models/product-model';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { UserProductResourceService } from '../../../../services/resources/user-product-resource.service';

@Component({
    selector: 'app-users-access-list',
    templateUrl: './users-access-list.component.html',
    styleUrls: ['./users-access-list.component.styl'],
    providers: [PagingTableService]
})
export class UsersAccessListComponent implements OnInit {

    columns: PagingTableHeaderColumn[] = [
        {isSortable: false, title: 'Actions', key: 'actions'},
        {isSortable: true, title: 'Contact', key: 'firstName'},
        {isSortable: true, title: 'Direct Phone', key: 'phone'},
        {isSortable: true, title: 'Mobile Phone', key: 'mobile'},
        {isSortable: true, title: 'Email', key: 'username'},
        {isSortable: true, title: 'Role', key: 'role'}
    ];

    get subscriberId(): any {
        return this.activatedRoute.parent.snapshot.params.id;
    }

    products: ProductModel[];

    @ViewChild('table')
    table: PagingTableComponent;

    constructor(private activatedRoute: ActivatedRoute,
                private userResourceService: UserResourceService,
                private utilsService: UtilsService,
                private store: Store<AppState>,
                private productResource: ProductResourceService,
                private userProductResource: UserProductResourceService) {
    }

    ngOnInit() {
        this.fetchAllProducts();
    }

    async fetchAllProducts() {
        const products = await this.productResource.findAllBySubscriberId(this.subscriberId);
        this.products = products;
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return this.userResourceService.bySubscriber(
                this.subscriberId,
                this.utilsService.tableToHttpParams(params)
            );
        }).bind(this);
    }

}
