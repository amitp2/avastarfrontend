import { Component, ViewChild } from '@angular/core';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { SubscriberResourceService } from '../../../../services/resources/subscriber-resource.service';
import { SubscriberModel } from '../../../../models/subscriber-model';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { UtilsService } from '../../../../services/utils.service';
import { PagingTableService } from '../../../../services/paging-table.service';
import { UserRole } from '../../../../enums/user-role.enum';
import { TableHandler } from '../../../../classes/table-handler';
import { ActivatedRoute } from '@angular/router';
import { SubscriberContactResourceService } from '../../../../services/resources/subscriber-contact-resource.service';
import { SubscriberContactModel } from '../../../../models/subscriber-contact-model';

@Component({
    selector: 'app-subscribers',
    templateUrl: './subscribers.component.html',
    styleUrls: ['./subscribers.component.styl'],
    providers: [PagingTableService]
})
export class SubscribersComponent extends TableHandler {

    columns: PagingTableHeaderColumn[] = [
        {isSortable: true, title: 'Company/Organization', key: 'name'},
        {isSortable: true, title: 'Office / Branch', key: 'address'},
        // {isSortable: true, title: 'Phone', key: 'phone'},
        {isSortable: true, title: 'Primary Contact', key: 'website'},
        {isSortable: false, title: 'Users Count', key: 'usersCount'},
        {isSortable: true, title: 'Status', key: 'status'},
        {isSortable: false, title: 'Actions', key: 'actions'}
    ];

    userRole = UserRole;

    @ViewChild('subscribersTable') subscribersTable: PagingTableComponent;


    constructor(private subscriberResourceService: SubscriberResourceService,
                private contacts: SubscriberContactResourceService,
                private utilsService: UtilsService,
                activatedRoute: ActivatedRoute) {
        super(activatedRoute);
    }

    getTable(): PagingTableComponent {
        return this.subscribersTable;
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return this.subscriberResourceService
                .query(this.utilsService.tableToHttpParams(params))
                .then((resp) => {

                    return this.contacts
                        .getManyBySubscriberIds(resp.items.map((s: SubscriberModel) => s.id))
                        .then((conts) => {

                            const contactsBySubscriberId = {};

                            conts.forEach((c: SubscriberContactModel) => contactsBySubscriberId[c.subscriberId] = c);

                            resp.items = resp.items.map((m: SubscriberModel) => {
                                const currentContact = contactsBySubscriberId[m.id];
                                if (currentContact) {
                                    m['contactFirstName'] = currentContact.firstName;
                                    if (currentContact.lastName) {
                                        m['contactFirstName'] += ' ' + currentContact.lastName;
                                    }
                                } else {
                                    m['contactFirstName'] = '';
                                }
                                return m;
                            });

                            return resp;
                        });
                });
        }).bind(this);
    }

    delete(item: SubscriberModel) {
        this.subscriberResourceService
            .delete(item.id)
            .then(() => {
                this.subscribersTable.delete(tableItem => tableItem.id === item.id);
            });
    }

}
