import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserModel } from '../../../../models/user-model';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-subscriber-users',
    templateUrl: './subscriber-users.component.html',
    styleUrls: ['./subscriber-users.component.styl']
})
export class SubscriberUsersComponent implements OnDestroy {


    subscriberIdObs: Observable<any>;
    subscriberId: number;
    subscriberIdSubscription: Subscription;

    constructor(private activatedRoute: ActivatedRoute,
                private router: Router) {
        this.subscriberIdObs = this.activatedRoute.parent.params.map(params => params.id);
        this.subscriberIdSubscription = this.subscriberIdObs.subscribe((id: number) => this.subscriberId = id);
    }

    edit(user: UserModel) {
        this.router.navigateByUrl(`u/subscribers/${this.subscriberId}/subscriber-users/${user.id}`);
    }

    ngOnDestroy() {
        this.subscriberIdSubscription.unsubscribe();
    }

}
