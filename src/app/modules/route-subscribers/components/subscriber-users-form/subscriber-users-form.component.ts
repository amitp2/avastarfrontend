import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserModel } from '../../../../models/user-model';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-subscriber-users-form',
    templateUrl: './subscriber-users-form.component.html',
    styleUrls: ['./subscriber-users-form.component.styl']
})
export class SubscriberUsersFormComponent implements OnDestroy {
    subscriberIdObs: Observable<any>;
    userIdObs: Observable<any>;
    subscriberId: number;
    subscriberIdObsSubscription: Subscription;

    constructor(private activatedRoute: ActivatedRoute,
                private router: Router) {
        this.subscriberIdObs = this.activatedRoute.parent.params.map(params => params.id);
        this.userIdObs = this.activatedRoute.params.map(params => params.userId);
        this.subscriberIdObsSubscription = this.subscriberIdObs.subscribe((id: number) => this.subscriberId = id);
    }

    added() {
        // this.router.navigateByUrl('/u/subscribers?fresh=' + Date.now());
        this.router.navigateByUrl(`/u/subscribers/${this.subscriberId}/subscriber-users`);
    }

    ngOnDestroy() {
        this.subscriberIdObsSubscription.unsubscribe();
    }

}
