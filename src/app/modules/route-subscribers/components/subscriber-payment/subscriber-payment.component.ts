import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {
    PaymentMethodNames,
    RenewalPeriodNames,
    SubscriptionStatusNames,
    SubscriptionTypeNames,
    PaymentMethod,
    RenewalPeriod,
    SubscriptionStatus,
    SubscriptionType
} from '../../../../enums/subscription-type';
import { ContractUploaderService } from '../../services/contract-uploader.service';
import { InitialInvoiceUploaderService } from '../../services/initial-invoice-uploader.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SubscriberResourceService } from '../../../../services/resources/subscriber-resource.service';
import { PaymentInfo } from '../../../../models/subscriber-payment.model';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';

@Component({
    selector: 'app-subscriber-payment',
    templateUrl: './subscriber-payment.component.html',
    styleUrls: ['./subscriber-payment.component.styl']
})
export class SubscriberPaymentComponent implements OnInit {
    private form: FormGroup;
    private PaymentMethod: typeof PaymentMethod = PaymentMethod;
    private RenewalPeriod: typeof RenewalPeriod = RenewalPeriod;
    private SubscriptionType: typeof SubscriptionType = SubscriptionType;
    private SubscriptionStatus: typeof SubscriptionStatus = SubscriptionStatus;

    private PaymentMethodNames: typeof PaymentMethodNames = PaymentMethodNames;
    private RenewalPeriodNames: typeof RenewalPeriodNames = RenewalPeriodNames;
    private SubscriptionTypeNames: typeof SubscriptionTypeNames = SubscriptionTypeNames;
    private SubscriptionStatusNames: typeof SubscriptionStatusNames = SubscriptionStatusNames;
    private submitting: boolean;
    private now = new Date();

    constructor(
        private location: Location,
        private fb: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private subscriberApi: SubscriberResourceService,
        private contractUploader: ContractUploaderService,
        private invoiceUploader: InitialInvoiceUploaderService
    ) {
        this.buildForm();
    }

    ngOnInit() {
        this.loadInfo();
    }

    buildForm() {
        this.form = this.fb.group({
            accountType: [null, [Validators.required]],
            renewalPeriod: [null, [Validators.required]],
            creationDate: [null],
            subscriptionDate: [null, [Validators.required]],
            contractUrl: [null],
            amountDue: [null, [Validators.required]],
            implementationFee: [null, [Validators.required]],
            initialInvoiceUrl: [null],
            paymentMethod: [null, [Validators.required]],
            status: [null, [Validators.required]],
            customerPo: [null],
            contractNumber: [null, [Validators.required]],
            nextPaymentDate: [null]
        });
    }

    @AsyncMethod({
        taskName: 'get_payment_info'
    })
    async loadInfo() {
        const subscriberId = this.route.snapshot.parent.params['id'];
        const paymentInfo = await this.subscriberApi.getPaymentInfo(subscriberId);

        this.form.patchValue({ ...paymentInfo });
        this.form.get('creationDate').disable();
        this.form.markAsPristine();
    }

    @AsyncMethod({
        taskName: 'update_payment_info',
        success: AppSuccess.EDIT_PAYMENT_INFO
    })
    async submit() {
        if (this.form.invalid) {
            return;
        }
        
        this.submitting = true;
        const subscriberId = this.route.snapshot.parent.params['id'];
        const paymentInfo = new PaymentInfo({ ...this.form.value });

        try {
            await this.subscriberApi.editPaymentInfo(subscriberId, paymentInfo);
            await this.loadInfo();
        } catch (e) {
            throw e;
        } finally {
            this.submitting = false;
        }
    }

    goBack() {
        this.router.navigateByUrl(`/u/subscribers`);
    }
}
