import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../../../../services/utils.service';
import { SubscriberPaymentHistoryService } from '../../../../services/resources/subscriber-payment-history.service';
import { ActivatedRoute } from '@angular/router';
import { PagingTableService } from '../../../../services/paging-table.service';
import { subscriptionStatusToName, SubscriptionStatus, SubscriptionStatusNames } from '../../../../enums/subscription-type';

@Component({
    selector: 'app-subscriber-payment-history',
    templateUrl: './subscriber-payment-history.component.html',
    styleUrls: ['./subscriber-payment-history.component.styl'],
    providers: [
        PagingTableService
    ]
})
export class SubscriberPaymentHistoryComponent implements OnInit {
    private SubscriptionStatus: typeof SubscriptionStatus = SubscriptionStatus;
    private SubscriptionStatusNames: typeof SubscriptionStatusNames = SubscriptionStatusNames;
    constructor(
        private utils: UtilsService,
        private route: ActivatedRoute,
        private historyApi: SubscriberPaymentHistoryService
    ) {
    }

    ngOnInit() {
    }

    load = (params) => {
        params.sortAsc = false;

        const httpParams = this.utils.tableToHttpParams(params);
        const subscriberId = this.route.snapshot.parent.params['id'];

        return this.historyApi.queryForSubscriber(httpParams, subscriberId);
    }

    getStatusName(status: SubscriptionStatus) {
        return subscriptionStatusToName(status);
    }
}
