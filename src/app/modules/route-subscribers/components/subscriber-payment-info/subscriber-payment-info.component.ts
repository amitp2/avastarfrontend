import { Component, OnInit } from '@angular/core';
import { SubscriptionStatus, SubscriptionStatusNames, SubscriptionTypeNames } from '../../../../enums/subscription-type';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { PaymentHistory } from '../../../../models/subscriber-payment.model';
import { SubscriberPaymentHistoryService } from '../../../../services/resources/subscriber-payment-history.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppError } from '../../../../enums/app-error.enum';
import { AppSuccess } from '../../../../enums/app-success.enum';

@Component({
    selector: 'app-subscriber-payment-info',
    templateUrl: './subscriber-payment-info.component.html',
    styleUrls: ['./subscriber-payment-info.component.styl']
})
export class SubscriberPaymentInfoComponent implements OnInit {
    private form: FormGroup;
    private submitting = false;
    private SubscriptionStatus: typeof SubscriptionStatus = SubscriptionStatus;
    private SubscriptionStatusNames: typeof SubscriptionStatusNames = SubscriptionStatusNames;

    constructor(
        private fb: FormBuilder,
        private location: Location,
        private route: ActivatedRoute,
        private historyApi: SubscriberPaymentHistoryService
    ) {
    }

    ngOnInit() {
        this.form = this.fb.group({
            date: [null, [Validators.required]],
            price: [null, [Validators.required]],
            status: [null, [Validators.required]]
        });
    }

    @AsyncMethod({
        taskName: 'add_payment',
        error: AppError.SUBSCRIBER_PAYMENT_ADD,
        success: AppSuccess.SUBSCRIBER_PAYMENT_ADD
    })
    async submit() {
        this.submitting = true;

        const model = new PaymentHistory({ ...this.form.value });
        const subscriberId = this.route.snapshot.parent.params['id'];

        await this.historyApi.addForSubscriber(subscriberId, model);

        this.location.back();
    }
}
