import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormMode } from '../../../../enums/form-mode.enum';

@Component({
    selector: 'app-subscribers-form',
    templateUrl: './subscribers-form.component.html',
    styleUrls: ['./subscribers-form.component.styl']
})
export class SubscribersFormComponent implements OnInit {

    id: string;
    mode: FormMode;
    formMode = FormMode;

    constructor(private activatedRoute: ActivatedRoute, public router: Router) {
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: any) => {
            this.id = params.id;
            if (this.id === 'add') {
                this.mode = FormMode.Add;
                return;
            }
            this.mode = FormMode.Edit;
        });
    }

    linkPrefix(): string {
        return '/u/subscribers/' + this.id + '/';
    }

}
