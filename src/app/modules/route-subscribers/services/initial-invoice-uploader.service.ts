import { Injectable } from '@angular/core';
import { StandardUploaderV2 } from '../../../classes/standard-uploader-v2';
import { AppSuccess } from '../../../enums/app-success.enum';
import { AllowedFileType } from '../../../enums/allowed-file-type.enum';
import { ConfigService } from '../../../services/config.service';
import { AuthService } from '../../../services/auth.service';
import { AppError } from '../../../enums/app-error.enum';

@Injectable()
export class InitialInvoiceUploaderService extends StandardUploaderV2 {

    constructor(private config: ConfigService, auth: AuthService) {
        super(auth);
    }

    uploadUrl(): string {
        return this.config.getApiUrl('files/subscribers/payment/initial-invoice');
    }

    asyncTaskName(): string {
        return 'UPLOAD_INITIAL_INVOICE';
    }

    uploadAppSuccess(): AppSuccess {
        return AppSuccess.UPLOAD_INITIAL_INVOICE;
    }

    uploadAppError(): AppError {
        return AppError.UPLOAD_INITIAL_INVOICE;
    }

    allowedFileType(): AllowedFileType {
        return AllowedFileType.All;
    }
}
