import { Injectable } from '@angular/core';
import { ConfigService } from '../../../services/config.service';
import { AuthService } from '../../../services/auth.service';
import { StandardUploader } from '../../../classes/standard-uploader';
import { AppSuccess } from '../../../enums/app-success.enum';
import { AllowedFileType } from '../../../enums/allowed-file-type.enum';

@Injectable()
export class SubscriberLogoUploaderService extends StandardUploader {

    constructor(private configService: ConfigService, authService: AuthService) {
        super(authService);
    }

    uploadUrl(): string {
        return `${this.configService.config().API_URL}files/subscribers/logo`;
    }

    asyncTaskName() {
        return 'SUBSCRIBER_LOGO_UPLOAD';
    }

    uploadAppSuccess() {
        return AppSuccess.SUBSCRIBER_LOGO_UPLOAD;
    }

    allowedFileType(): AllowedFileType {
        return AllowedFileType.Images;
    }

}
