import { Injectable } from '@angular/core';
import { StandardUploaderV2 } from '../../../classes/standard-uploader-v2';
import { AppSuccess } from '../../../enums/app-success.enum';
import { ConfigService } from '../../../services/config.service';
import { AuthService } from '../../../services/auth.service';
import { AllowedFileType } from '../../../enums/allowed-file-type.enum';
import { AppError } from '../../../enums/app-error.enum';

@Injectable()
export class ContractUploaderService extends StandardUploaderV2 {

    constructor(private config: ConfigService, auth: AuthService) {
        super(auth);
    }

    uploadUrl(): string {
        return this.config.getApiUrl('files/subscribers/payment/contract');
    }

    asyncTaskName(): string {
        return 'UPLOAD_SUBSCRIBER_CONTRACT';
    }

    uploadAppSuccess(): AppSuccess {
        return AppSuccess.UPLOAD_SUBSCRIBER_CONTRACT;
    }

    uploadAppError(): AppError {
        return AppError.UPLOAD_SUBSCRIBER_CONTRACT;
    }

    allowedFileType(): AllowedFileType {
        return AllowedFileType.All;
    }
}
