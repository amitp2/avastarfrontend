import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { SubscribersComponent } from './components/subscribers/subscribers.component';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { AccountTypePipe } from './pipes/account-type.pipe';
import { SubscriberStatusPipe } from './pipes/subscriber-status.pipe';
import { SubscribersFormComponent } from './components/subscribers-form/subscribers-form.component';
import { SubscriberInfoComponent } from './components/subscriber-info/subscriber-info.component';
import { SubscriberContactPersonComponent } from './components/subscriber-contact-person/subscriber-contact-person.component';
import { SubscriberUsersComponent } from './components/subscriber-users/subscriber-users.component';
import { SubscriberUsersFormComponent } from './components/subscriber-users-form/subscriber-users-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SubscriberUsersModule } from '../subscriber-users/subscriber-users.module';
import { SubscriberLogoUploaderService } from './services/subscriber-logo-uploader.service';
import { AccessComponent } from './components/access/access.component';
import { UsersAccessListComponent } from './components/users-access-list/users-access-list.component';
import { SelectModule } from '../select/select.module';
import { MaskedInputModule } from '../masked-input/masked-input.module';
import { SubscriberPaymentInfoComponent } from './components/subscriber-payment-info/subscriber-payment-info.component';
import { SubscriberPaymentHistoryComponent } from './components/subscriber-payment-history/subscriber-payment-history.component';
import { SubscriberPaymentComponent } from './components/subscriber-payment/subscriber-payment.component';
import { ContractUploaderService } from './services/contract-uploader.service';
import { InitialInvoiceUploaderService } from './services/initial-invoice-uploader.service';
import { TableResizeModule } from '../table-resize/table-resize.module';

const routes: Routes = [
    {
        path: '',
        component: SubscribersComponent,
        children: [
            {
                path: ':id', component: SubscribersFormComponent,
                children: [
                    { path: 'subscriber-info', component: SubscriberInfoComponent },
                    { path: 'subscriber-contact-person', component: SubscriberContactPersonComponent },
                    { path: 'subscriber-users', component: SubscriberUsersComponent, pathMatch: 'full' },
                    { path: 'subscriber-users/:userId', component: SubscriberUsersFormComponent },
                    { path: 'access', component: AccessComponent },
                    { path: 'payment', component: SubscriberPaymentComponent, pathMatch: 'full' },
                    { path: 'payment/add', component: SubscriberPaymentInfoComponent },
                ]
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        PagingTableModule,
        SharedModule,
        ReactiveFormsModule,
        SubscriberUsersModule,
        SelectModule,
        MaskedInputModule,
        TableResizeModule
    ],
    declarations: [
        SubscribersComponent,
        AccountTypePipe,
        SubscriberStatusPipe,
        SubscribersFormComponent,
        SubscriberInfoComponent,
        SubscriberContactPersonComponent,
        SubscriberUsersComponent,
        SubscriberUsersFormComponent,
        AccessComponent,
        UsersAccessListComponent,
        SubscriberPaymentInfoComponent,
        SubscriberPaymentHistoryComponent,
        SubscriberPaymentComponent,
    ],
    providers: [
        SubscriberLogoUploaderService,
        ContractUploaderService,
        InitialInvoiceUploaderService
    ]
})
export class RouteSubscribersModule {
}
