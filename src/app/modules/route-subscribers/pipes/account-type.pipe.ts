import { Pipe, PipeTransform } from '@angular/core';
import { AccountType } from '../../../enums/account-type.enum';

@Pipe({
    name: 'accountType'
})
export class AccountTypePipe implements PipeTransform {

    transform(value: AccountType, args?: any): any {

        switch (value) {
            case AccountType.Client:
                return 'Client';
            case AccountType.House:
                return 'House';
        }

        return '';
    }

}
