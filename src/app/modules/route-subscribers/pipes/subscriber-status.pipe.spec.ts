import { SubscriberStatusPipe } from './subscriber-status.pipe';
import { SubscriberStatus } from '../../../enums/subscriber-status.enum';

describe('SubscriberStatusPipe', () => {
    it('should map subscriber statuses to proper text representations', () => {
        const pipe = new SubscriberStatusPipe();

        expect(pipe.transform(SubscriberStatus.Active)).toEqual('Active');
        expect(pipe.transform(SubscriberStatus.Locked)).toEqual('Inactive');

    });
});
