import { Pipe, PipeTransform } from '@angular/core';
import { SubscriberStatus } from '../../../enums/subscriber-status.enum';

@Pipe({
    name: 'subscriberStatus'
})
export class SubscriberStatusPipe implements PipeTransform {

    transform(value: SubscriberStatus, args?: any): any {
        switch (value) {
            case SubscriberStatus.Locked:
                return 'Inactive';
            case SubscriberStatus.Active:
                return 'Active';
        }
        return '';
    }

}
