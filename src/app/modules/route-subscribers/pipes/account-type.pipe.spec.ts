import { AccountTypePipe } from './account-type.pipe';
import { AccountType } from '../../../enums/account-type.enum';

describe('AccountTypePipe', () => {

    it('should transform account types to proper text displays', () => {
        const pipe = new AccountTypePipe();

        expect(pipe.transform(AccountType.Client)).toEqual('Client');
        expect(pipe.transform(AccountType.House)).toEqual('House');
    });

});
