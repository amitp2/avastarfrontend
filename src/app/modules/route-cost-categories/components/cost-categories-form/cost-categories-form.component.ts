import { Component, OnInit } from '@angular/core';
import { FormHandler } from '../../../../classes/form-handler';
import { CostCategoryModel } from '../../../../models/cost-category-model';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CostCategoryResourceService } from '../../../../services/resources/cost-category-resource.service';
import { AuthService } from '../../../../services/auth.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { UtilsService } from '../../../../services/utils.service';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';

@Component({
    selector: 'app-cost-categories-form',
    templateUrl: './cost-categories-form.component.html',
    styleUrls: ['./cost-categories-form.component.styl']
})
export class CostCategoriesFormComponent extends FormHandler<CostCategoryModel> {


    constructor(
        private activatedRoute: ActivatedRoute,
        private costCategory: CostCategoryResourceService,
        private authService: AuthService,
        private router: Router,
        private utils: UtilsService
    ) {
        super();
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    initializeForm(): FormGroup {
        return new FormGroup({
            costCategoryName: new FormControl({ value: null, disabled: true }),
            costCategoryId: new FormControl(),
            costCode: new FormControl(null, [Validators.required, Validators.maxLength(20)])
        });
    }

    @AsyncMethod({
        taskName: 'get_cost_category'
    })
    get(id: number): Promise<CostCategoryModel> {
        return this.costCategory.getBySubscriberId(id, this.authService.currentUserSnapshot.subscriberId);
    }

    add(model: CostCategoryModel): Promise<any> {
        return undefined;
    }

    @AsyncMethod({
        taskName: 'update_cost_category',
        success: AppSuccess.COST_CATEGORY_EDIT,
        error: AppError.COST_CATEGORY_EDIT
    })
    edit(id: number, model: CostCategoryModel): Promise<any> {
        return this.costCategory
            .updateForSubscriber(model, this.authService.currentUserSnapshot.subscriberId)
            .then(() => this.router.navigateByUrl(`/u/cost-category?fresh=${Date.now()}`));
    }

    serialize(model: CostCategoryModel): any {
        return this.utils.transformInto(model, {
            costCategoryName: { to: 'costCategoryName' },
            costCategoryId: { to: 'costCategoryId' },
            costCode: { to: 'costCode' }
        });
    }

    deserialize(formValue: any): CostCategoryModel {
        return new CostCategoryModel(this.utils.transformInto(formValue, {
            costCategoryName: { to: 'costCategoryName' },
            costCategoryId: { to: 'costCategoryId' },
            costCode: { to: 'costCode' }
        }));
    }

}
