import { Component, ViewChild } from '@angular/core';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { CostCategoryResourceService } from '../../../../services/resources/cost-category-resource.service';
import { AuthService } from '../../../../services/auth.service';
import { TableHandler } from '../../../../classes/table-handler';
import { ActivatedRoute } from '@angular/router';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { UtilsService } from '../../../../services/utils.service';

@Component({
    selector: 'app-cost-categories',
    templateUrl: './cost-categories.component.html',
    styleUrls: ['./cost-categories.component.styl']
})
export class CostCategoriesComponent extends TableHandler {

    @ViewChild('table') table: PagingTableComponent;

    columns: PagingTableHeaderColumn[] = [
        {isSortable: false, title: 'Category', key: 'costCategoryName'},
        {isSortable: false, title: 'GL CODE', key: 'costCode'},
        {isSortable: false, title: 'Actions', key: 'actions'}
    ];

    constructor(private costCategory: CostCategoryResourceService,
                private authService: AuthService,
                private utils: UtilsService,
                activatedRoute: ActivatedRoute) {
        super(activatedRoute);
        // this.costCategory
        //     .queryBySubscriberId(this.authService.currentUserSnapshot.subscriberId)
        //     .then((resp) => {
        //         console.log(resp);
        //     });
    }

    getTable(): PagingTableComponent {
        return this.table;
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return this.costCategory.queryBySubscriberId(
                this.utils.tableToHttpParams(params),
                this.authService.currentUserSnapshot.subscriberId
            );
        }).bind(this);
    }

}
