import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { CostCategoriesComponent } from './components/cost-categories/cost-categories.component';
import { CostCategoriesFormComponent } from './components/cost-categories-form/cost-categories-form.component';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AllowOnlyService } from '../../services/guards/allow-only.service';
import { UserRole } from '../../enums/user-role.enum';
import { TableResizeModule } from '../table-resize/table-resize.module';

const routes: Routes = [
    {
        path: '',
        component: CostCategoriesComponent,
        canActivate: [AllowOnlyService],
        data: {
            roles: [UserRole.ADMIN]
        },
        children: [
            {path: ':id', component: CostCategoriesFormComponent}
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes),
        PagingTableModule,
        ReactiveFormsModule,
        FormsModule,
        TableResizeModule
    ],
    declarations: [CostCategoriesComponent, CostCategoriesFormComponent]
})
export class RouteCostCategoriesModule {
}
