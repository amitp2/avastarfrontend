import { Converter } from '../../../helpers/converter';
import { Injectable } from '@angular/core';
import { PackageModel } from '../../../models/package-model';
import { SelectItem } from '../types/select-item';

@Injectable()
export class FunctionPackageToSelectItemConverter implements Converter<PackageModel, SelectItem> {


    async convert(from: PackageModel): Promise<SelectItem> {
        return new SelectItem(from.id, from.name, false, null);
    }

}
