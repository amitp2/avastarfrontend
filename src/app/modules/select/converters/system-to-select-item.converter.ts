import { Converter } from '../../../helpers/converter';
import { EquipmentSystemModel } from '../../../models/equipment-system-model';
import { SelectItem } from '../types/select-item';

export class SystemToSelectItemConverter implements Converter<EquipmentSystemModel, SelectItem> {

    async convert(from: EquipmentSystemModel): Promise<SelectItem> {
        return new SelectItem(from.id, from.name, false, null);
    }

}
