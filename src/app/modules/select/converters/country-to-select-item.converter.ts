import { Converter } from '../../../helpers/converter';
import { CountryModel } from '../../../models/country-model';
import { SelectItem } from '../types/select-item';

export class CountryToSelectItemConverter implements Converter<CountryModel, SelectItem> {


    async convert(from: CountryModel): Promise<SelectItem> {
        return new SelectItem(from.id, from.name, false, null);
    }

}
