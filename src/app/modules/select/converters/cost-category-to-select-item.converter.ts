import { Converter } from '../../../helpers/converter';
import { CostCategoryModel } from '../../../models/cost-category-model';
import { SelectItem } from '../types/select-item';

export class CostCategoryToSelectItemConverter implements Converter<CostCategoryModel, SelectItem> {

    async convert(from: CostCategoryModel): Promise<SelectItem> {
        const display = from.costCode ? `${from.costCode} - ${from.costCategoryName}` : from.costCategoryName;
        return new SelectItem(from.costCategoryId, display, false, null);
    }

}
