import { Converter } from '../../../helpers/converter';
import { PurchaseOrder } from '../../route-manage-purchase-orders/types';
import { SelectItem } from '../types/select-item';

export class PurchaseOrderToSelectItemConverter implements Converter<PurchaseOrder, SelectItem> {

    async convert(from: PurchaseOrder): Promise<SelectItem> {
        return new SelectItem(from.id, from.poNumber.toString(), false, null);
    }

}
