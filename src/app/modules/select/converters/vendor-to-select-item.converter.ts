import { Converter } from '../../../helpers/converter';
import { VendorModel } from '../../../models/vendor-model';
import { SelectItem } from '../types/select-item';

export class VendorToSelectItemConverter implements Converter<VendorModel, SelectItem> {

    async convert(from: VendorModel): Promise<SelectItem> {
        return new SelectItem(from.id, from.name, false, null);
    }

}
