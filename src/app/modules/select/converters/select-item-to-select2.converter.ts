import { Converter } from '../../../helpers/converter';
import { SelectItem } from '../types/select-item';

export class SelectItemToSelect2Converter implements Converter<SelectItem, any> {

    async convert(from: SelectItem): Promise<any> {
        return {
            id: from.id + '',
            text: from.display
        };
    }

}
