import { Converter } from '../../../helpers/converter';
import { VenueClientModel } from '../../../models/venue-client-model';
import { SelectItem } from '../types/select-item';

export class VenueClientToSelectItemConverter implements Converter<VenueClientModel, SelectItem> {

    async convert(from: VenueClientModel): Promise<SelectItem> {
        return new SelectItem(from.id, from.name, false, null);
    }

}
