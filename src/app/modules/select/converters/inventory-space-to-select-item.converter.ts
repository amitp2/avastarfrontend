import { Converter } from '../../../helpers/converter';
import { InventorySpace } from '../../route-inventory/services/inventory-spaces.service';
import { SelectItem } from '../types/select-item';

export class InventorySpaceToSelectItemConverter implements Converter<InventorySpace, SelectItem> {

    async convert(from: InventorySpace): Promise<SelectItem> {
        return new SelectItem(from.id, from.name, false, null);
    }

}
