import { Converter } from '../../../helpers/converter';
import { InventoryOwner } from '../../route-inventory/services/inventory-owner.service';
import { SelectItem } from '../types/select-item';

export class InventoryOwnerToSelectItemConverter implements Converter<InventoryOwner, SelectItem> {

    async convert(from: InventoryOwner): Promise<SelectItem> {
        return new SelectItem(from.id, from.name, false, null);
    }

}
