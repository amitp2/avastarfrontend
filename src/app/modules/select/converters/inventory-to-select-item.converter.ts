import { Converter } from '../../../helpers/converter';
import { InventoryModel } from '../../../models/inventory-model';
import { SelectItem } from '../types/select-item';

export class InventoryToSelectItemConverter implements Converter<InventoryModel, SelectItem> {

    async convert(from: InventoryModel): Promise<SelectItem> {
        const display = from.subCategoryItemName + (from.tagNumber ? `${from.subCategoryItemName ? ' - ' : ''}${from.tagNumber}` : '');
        return new SelectItem(from.id, display, false, null);
    }

}
