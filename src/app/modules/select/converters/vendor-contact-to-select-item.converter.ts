import { Converter } from '../../../helpers/converter';
import { VendorContactModel } from '../../../models/vendor-contact-model';
import { SelectItem } from '../types/select-item';

export class VendorContactToSelectItemConverter implements Converter<VendorContactModel, SelectItem> {


    async convert(from: VendorContactModel): Promise<SelectItem> {
        return new SelectItem(from.id, `${from.firstName} ${from.lastName}`, false, null);
    }

}
