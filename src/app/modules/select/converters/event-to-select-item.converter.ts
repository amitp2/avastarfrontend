import { Converter } from '../../../helpers/converter';
import { VenueEventModel } from '../../../models/venue-event-model';
import { SelectItem } from '../types/select-item';

export class EventToSelectItemConverter implements Converter<VenueEventModel, SelectItem> {
    async convert(from: VenueEventModel): Promise<SelectItem> {
        return new SelectItem(from.id, from.name, false, null);
    }
}
