import { Converter } from '../../../helpers/converter';
import { InventorySubcategoryItemModel } from '../../../models/inventory-subcategory-item-model';
import { SelectItem } from '../types/select-item';

export class InventorySubCategoryItemToSelectItemConverter implements Converter<InventorySubcategoryItemModel, SelectItem> {

    async convert(from: InventorySubcategoryItemModel): Promise<SelectItem> {
        return new SelectItem(from.id, from.name, false, from);
    }

}
