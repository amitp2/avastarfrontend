import { Converter } from '../../../helpers/converter';
import { FunctionSpaceModel } from '../../../models/function-space-model';
import { SelectItem } from '../types/select-item';

export class FunctionSpaceToSelectItemConverter implements Converter<FunctionSpaceModel, SelectItem> {

    async convert(from: FunctionSpaceModel): Promise<SelectItem> {
        return new SelectItem(from.id, from.name, false, null);
    }

}
