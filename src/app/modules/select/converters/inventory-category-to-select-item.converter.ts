import { Converter } from '../../../helpers/converter';
import { InventoryCategoryModel } from '../../../models/inventory-category-model';
import { SelectItem } from '../types/select-item';

export class InventoryCategoryToSelectItemConverter implements Converter<InventoryCategoryModel, SelectItem> {

    async convert(from: InventoryCategoryModel): Promise<SelectItem> {
        return new SelectItem(from.id, from.name, false, null);
    }

}
