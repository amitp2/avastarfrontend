import { Converter } from '../../../helpers/converter';
import { StateModel } from '../../../models/state-model';
import { SelectItem } from '../types/select-item';
import { Injectable } from '@angular/core';

@Injectable()
export class StateToSelectItemConverter implements Converter<StateModel, SelectItem> {

    async convert(from: StateModel): Promise<SelectItem> {
        return new SelectItem(from.id, from.name, false, null);
    }

}
