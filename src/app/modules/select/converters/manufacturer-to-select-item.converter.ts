import { Converter } from '../../../helpers/converter';
import { ManufacturerModel } from '../../../models/manufacturer-model';
import { SelectItem } from '../types/select-item';

export class ManufacturerToSelectItemConverter implements Converter<ManufacturerModel, SelectItem> {


    async convert(from: ManufacturerModel): Promise<SelectItem> {
        return new SelectItem(from.id, from.name, false, null);
    }

}
