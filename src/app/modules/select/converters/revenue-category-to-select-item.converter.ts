import { Converter } from '../../../helpers/converter';
import { RevenueCategoryModel } from '../../../models/revenue-category-model';
import { SelectItem } from '../types/select-item';

export class RevenueCategoryToSelectItemConverter implements Converter<RevenueCategoryModel, SelectItem> {
    async convert(from: RevenueCategoryModel): Promise<SelectItem> {
        const baseDisplay = `${from.code} - ${from.revenueCategoryName}`;
        const display = from.revenueCode ? `${from.revenueCode} - ${baseDisplay}` : baseDisplay;
        return new SelectItem(from.revenueCategoryId, display, false, null);
    }
}
