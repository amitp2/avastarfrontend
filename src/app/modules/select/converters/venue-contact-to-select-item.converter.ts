import { Converter } from '../../../helpers/converter';
import { VenueContactModel } from '../../../models/venue-contact-model';
import { SelectItem } from '../types/select-item';

export class VenueContactToSelectItemConverter implements Converter<VenueContactModel, SelectItem> {

    async convert(from: VenueContactModel): Promise<SelectItem> {
        return new SelectItem(from.id, `${from.firstName} ${from.lastName}`, false, null);
    }

}
