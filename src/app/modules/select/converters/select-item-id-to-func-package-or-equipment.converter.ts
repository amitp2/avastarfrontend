import { Converter } from '../../../helpers/converter';
import { PackageModel } from '../../../models/package-model';
import { EquipmentModel } from '../../../models/equipment-model';
import { Injectable } from '@angular/core';
import { PackageResourceService } from '../../../services/resources/package-resource.service';
import { EquipmentResourceService } from '../../../services/resources/equipment-resource.service';

import { FUNCTION_PACKAGE_AND_EQUIPMENT_ID_THRESHOLD } from './function-equipment-to-select-item.converter';

declare var R: any;

@Injectable()
export class SelectItemIdToFuncPackageOrEquipmentConverter implements Converter<number, PackageModel | EquipmentModel> {

    constructor(private packages: PackageResourceService,
                private equipments: EquipmentResourceService) {
    }

    async convert(from: number): Promise<PackageModel | EquipmentModel> {
        
        const getEquipmentId = R.partialRight(R.modulo, [FUNCTION_PACKAGE_AND_EQUIPMENT_ID_THRESHOLD]);
        const isPackage = R.partialRight(R.lt, [FUNCTION_PACKAGE_AND_EQUIPMENT_ID_THRESHOLD]);
        const getPackageById = (id: number) => this.packages.get(id);
        const getEquipmentById = (id: number) => this.equipments.get(id);
        const fetch = R.ifElse(isPackage, getPackageById, R.pipe(getEquipmentId, getEquipmentById));

        const res = await fetch(from);

        return res;
    }

}
