import { Converter } from '../../../helpers/converter';
import { EquipmentModel } from '../../../models/equipment-model';
import { SelectItem } from '../types/select-item';
import { Injectable } from '@angular/core';

export const FUNCTION_PACKAGE_AND_EQUIPMENT_ID_THRESHOLD = 1e6;

@Injectable()
export class FunctionEquipmentToSelectItemConverter implements Converter<EquipmentModel, SelectItem> {

    async convert(from: EquipmentModel): Promise<SelectItem> {
        const id = FUNCTION_PACKAGE_AND_EQUIPMENT_ID_THRESHOLD + from.id;
        return new SelectItem(id, from.clientDescription, false, null);
    }

}
