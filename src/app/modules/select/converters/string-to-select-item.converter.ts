import { Converter } from '../../../helpers/converter';
import { SelectItem } from '../types/select-item';

export class StringToSelectItemConverter implements Converter<string, SelectItem> {


    async convert(from: string): Promise<SelectItem> {
        return new SelectItem(from, from, false, null);
    }
}
