import { Converter } from '../../../helpers/converter';
import { SubscriberModel } from '../../../models/subscriber-model';
import { SelectItem } from '../types/select-item';
import { Injectable } from '@angular/core';

@Injectable()
export class SubsToSelectItemConverter implements Converter<SubscriberModel, SelectItem> {


    async convert(from: SubscriberModel): Promise<SelectItem> {
        return new SelectItem(from.id, from.name, false, null);
    }

}
