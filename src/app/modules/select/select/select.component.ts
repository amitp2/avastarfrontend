import {
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    forwardRef,
    HostListener,
    Input,
    NgZone,
    OnChanges,
    OnDestroy,
    OnInit,
    Output,
    SimpleChanges,
    ViewChild
} from '@angular/core';
import { SelectFetch } from '../types/select-fetch';
import { SelectFetchType } from '../types/select-fetch-type';
import { SelectFetchTypeRegistryService } from '../services/select-fetch-type-registry.service';
import { GlobalsService } from '../../../services/globals.service';
import { isSelectFetch } from '../types/guards/is-select-fetch.typeguard';
import { isSelectFetchType } from '../types/guards/is-select-fetch-type.typeguard';
import { SelectItemToSelect2Converter } from '../converters/select-item-to-select2.converter';
import { UtilsService } from '../../../services/utils.service';
import { SelectItem } from '../types/select-item';

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';

import { AbstractControlValueAccessor } from '../../../helpers/abstract-control-value-accessor';
import { UnsubscribePoint } from '../../../decorators/unsubscribe-point.decorator';
import { Subscription } from 'rxjs/Subscription';
import { ObservableSubscription } from '../../../decorators/observable-subscription.decorator';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SelectFetchFactory } from '../types/select-fetch-factory';
import { isSelectFetchFactory } from '../types/guards/is-select-fetch-factory.typeguard';
import { AsyncValue } from '../../../classes/async-value';

const PLACEHOLDER_TEXT = 'Please Select...';
const ALL_KEY = 'SOME_ALL_KEY';


@Component({
    selector: 'app-select',
    templateUrl: './select.component.html',
    styleUrls: ['./select.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => SelectComponent),
            multi: true
        }
    ]
})
export class SelectComponent extends AbstractControlValueAccessor implements OnInit, OnChanges, OnDestroy, AfterViewInit {

    @ViewChild('select')
    select: ElementRef;

    @Input()
    fetcher: SelectFetch | SelectFetchType | SelectFetchFactory;

    @Input()
    factoryArgs: any;

    @Input()
    allOption: boolean;

    @Input() openOnFocus = false;

    @Output()
    loaded: EventEmitter<any>;

    @Output()
    change: EventEmitter<any>;

    @Input()
    hidePlaceholder: boolean;

    @Output()
    itemNotFound: EventEmitter<any> = new EventEmitter();

    private get $(): any {
        return this.globals.globals().$;
    }

    private get $select(): any {
        return this.$(this.select.nativeElement);
    }

    private $select2: any;

    private actualFetch: SelectFetch;

    private consumed = false;

    @ObservableSubscription
    valueChangesSubs: Subscription;

    inputChanges: Subject<any>;

    @ObservableSubscription
    inputChangesSubs: Subscription;

    currentItemsInTheList: AsyncValue<any[]>;

    constructor(
        private fetchTypeRegistry: SelectFetchTypeRegistryService,
        private select2Converter: SelectItemToSelect2Converter,
        private utils: UtilsService,
        private globals: GlobalsService,
        private zone: NgZone,
        private elementRef: ElementRef
    ) {
        super();
        this.loaded = new EventEmitter<any>();
        this.change = new EventEmitter<any>();
        this.inputChanges = new BehaviorSubject<any>(null);
        this.currentItemsInTheList = new AsyncValue<any[]>([]);
    }

    get placeholderText() {
        return this.hidePlaceholder ? '' : PLACEHOLDER_TEXT;
    }

    ngOnInit() {
        this.$select2 = this.$select.select2({
            data: [{ id: '', text: '' }],
            placeholder: this.placeholderText
        });

        this.valueChangesSubs = this.valuesFromOutside
            .changes
            .subscribe((val) => {
                if (val === '') {
                    this.setValueOnSelect(val);
                    this.consumed = false;
                    return;
                }
                if (!val) {
                    this.consumed = true;
                    return;
                }
                this.setValueOnSelect(val);
                this.consumed = false;
            });

        this.inputChangesSubs = this.inputChanges
            .debounceTime(500)
            .subscribe(() => this.onInputChanges());
    }

    openDropdown = (event) => this.$select.select2('open');

    ngAfterViewInit() {
        this.$(this.elementRef.nativeElement)
            .on('focus', '.select2-selection--single', this.openDropdown);
    }

    @UnsubscribePoint
    ngOnDestroy(): void {
        this.$(this.elementRef.nativeElement)
            .off('focus', '.select2-selection--single', this.openDropdown);
    }

    changeOutside(obj: any): void {
        this.zone.run(() => {
            this.change.emit(obj);
            this.onChange(obj);
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        this.inputChanges.next(changes);
    }

    refresh() {
        this.onInputChanges();
    }

    async onInputChanges() {
        this.updateActualFetch();
        const { data, items } = await this.loadData();
        this.currentItemsInTheList.value = data;
        this.reinitialize(data, items);
    }

    private updateActualFetch() {
        if (isSelectFetch(this.fetcher)) {
            this.actualFetch = this.fetcher;
        } else if (isSelectFetchType(this.fetcher)) {
            this.actualFetch = this.fetchTypeRegistry.getFetch(this.fetcher);
        } else if (isSelectFetchFactory(this.fetcher)) {
            this.actualFetch = this.fetcher.create(this.factoryArgs);
        } else {
            throw new Error('Fetch is required for select component');
        }
    }

    private async loadData() {
        const items = await this.actualFetch.fetch();
        if (this.allOption) {
            items.unshift(new SelectItem(ALL_KEY, 'All', false, null));
        }
        const data = await this.utils.convertMany<SelectItem, any>(items, this.select2Converter);
        return { data, items };
    }

    private async reinitialize(data: any, rawData: any) {
        this.$select2.select2('destroy');
        this.$select.off('select2:select');
        this.$select.html('');
        await this.utils.resolveAfter(500);
        this.$select2 = this.$select.select2({ data, placeholder: this.placeholderText });

        this.$select.on('select2:select', () => {
            const [selected] = this.$select.select2('data');
            const outsideVal = selected.id === ALL_KEY ? '' : selected.id;
            this.changeOutside(outsideVal);
        });

        this.loaded.emit(rawData);
        if (!this.consumed) {
            this.setValueOnSelect(this.valuesFromOutside.value);
            this.consumed = true;
        } else {
            this.resetValueOnSelect();
        }
    }

    public resetValueOnSelect() {
        this.$select.val([]).trigger('change');
    }

    private setValueOnSelect(val: any) {
        let putVal: any = val === '' ? ALL_KEY : val + '';
        if (val && !this.currentItemsInTheList.value.some(x => x.id == val)) {
            putVal = [];
            this.consumed = false;
            this.itemNotFound.emit(val);
        }
        this.$select.val(putVal).trigger('change');
    }
}
