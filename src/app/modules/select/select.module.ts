import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectComponent } from './select/select.component';
import { CountryFetch } from './fetches/country.fetch';
import { CountryToSelectItemConverter } from './converters/country-to-select-item.converter';
import { SelectFetchTypeRegistryService } from './services/select-fetch-type-registry.service';
import { SelectItemToSelect2Converter } from './converters/select-item-to-select2.converter';
import { StateFetch } from './fetches/state.fetch';
import { StateToSelectItemConverter } from './converters/state-to-select-item.converter';
import { SubsToSelectItemConverter } from './converters/subs-to-select-item.converter';
import { SubscriberFetch } from './fetches/subscriber.fetch';
import { AccountFetch } from './fetches/account.fetch';
import { VenueClientToSelectItemConverter } from './converters/venue-client-to-select-item.converter';
import { EventFetchFactory } from './fetches/factories/event.fetch.factory';
import { EventToSelectItemConverter } from './converters/event-to-select-item.converter';
import { FunctionSpaceToSelectItemConverter } from './converters/function-space-to-select-item.converter';
import { FunctionSpaceFetch } from './fetches/function-space.fetch';
import { TimeFetch } from './fetches/time.fetch';
import { StringToSelectItemConverter } from './converters/string-to-select-item.converter';
import { RevenueCategoryCodeFetch, RevenueCategoryLocalCodeFetch } from './fetches/revenue-category-code.fetch';
import { InventoryCategoryFetch } from './fetches/inventory-category.fetch';
import { InventoryCategoryToSelectItemConverter } from './converters/inventory-category-to-select-item.converter';
import { InventorySubCategoryFetchFactory } from './fetches/factories/inventory-sub-category.fetch.factory';
import { InventorySubCategoryItemToSelectItemConverter } from './converters/inventory-sub-category-item-to-select-item.converter';
import { InventorySubCategoryItemFetchFactory } from './fetches/factories/inventory-sub-category-item.fetch.factory';
import { RevenueCategoryToSelectItemConverter } from './converters/revenue-category-to-select-item.converter';
import { RevenueCategoryFetch } from './fetches/revenue-category.fetch';
import { CostCategoryToSelectItemConverter } from './converters/cost-category-to-select-item.converter';
import { CostCategoryFetch } from './fetches/cost-category.fetch';
import { ManufacturerToSelectItemConverter } from './converters/manufacturer-to-select-item.converter';
import { ManufacturerFetch } from './fetches/manufacturer.fetch';
import { VendorToSelectItemConverter } from './converters/vendor-to-select-item.converter';
import { VendorFetch } from './fetches/vendor.fetch';
import { InventorySpaceToSelectItemConverter } from './converters/inventory-space-to-select-item.converter';
import { StorageLocationFetchFactory } from './fetches/factories/storage-location.fetch.factory';
import { InventoryOwnerToSelectItemConverter } from './converters/inventory-owner-to-select-item.converter';
import { InventoryOwnerFetchFactory } from './fetches/factories/inventory-owner.fetch.factory';
import { VendorContactFetchFactory } from './fetches/factories/vendor-contact.fetch.factory';
import { VendorContactToSelectItemConverter } from './converters/vendor-contact-to-select-item.converter';
import { VenueContactToSelectItemConverter } from './converters/venue-contact-to-select-item.converter';
import { VenueContactFetch } from './fetches/venue-contact.fetch';
import { InventoryToSelectItemConverter } from './converters/inventory-to-select-item.converter';
import { InventoryFetchFactory } from './fetches/factories/inventory.fetch.factory';
import { SystemToSelectItemConverter } from './converters/system-to-select-item.converter';
import { SystemFetchFactory } from './fetches/factories/system.fetch.factory';
import { MonthFetch } from './fetches/month.fetch';
import { YearFetch } from './fetches/year.fetch';
import { PurchaseOrderToSelectItemConverter } from './converters/purchase-order-to-select-item.converter';
import { PurchaseOrderFetchFactory } from './fetches/factories/purchase-order.fetch.factory';
import { RevenueCategoryWithCodeFetch } from './fetches/revenue-category-with-code.fetch';
import { CostCategoryFetchWithCode } from './fetches/cost-category-with-code.fetch';
import { FunctionEquipmentFetch } from './fetches/function-equipment.fetch';
import { FunctionPackageToSelectItemConverter } from './converters/function-package-to-select-item.converter';
import { FunctionEquipmentToSelectItemConverter } from './converters/function-equipment-to-select-item.converter';
import { SelectItemIdToFuncPackageOrEquipmentConverter } from './converters/select-item-id-to-func-package-or-equipment.converter';
import { LocationFetch } from './fetches/location.fetch';

@NgModule({
    imports: [
        CommonModule
    ],
    providers: [
        CountryFetch,
        StateFetch,
        SubscriberFetch,
        AccountFetch,
        FunctionSpaceFetch,
        TimeFetch,
        RevenueCategoryCodeFetch,
        InventoryCategoryFetch,
        RevenueCategoryFetch,
        RevenueCategoryWithCodeFetch,
        CostCategoryFetch,
        CostCategoryFetchWithCode,
        ManufacturerFetch,
        VendorFetch,
        VenueContactFetch,
        MonthFetch,
        YearFetch,
        RevenueCategoryLocalCodeFetch,
        FunctionEquipmentFetch,
        LocationFetch,

        EventFetchFactory,
        InventorySubCategoryFetchFactory,
        InventorySubCategoryItemFetchFactory,
        StorageLocationFetchFactory,
        InventoryOwnerFetchFactory,
        VendorContactFetchFactory,
        InventoryFetchFactory,
        SystemFetchFactory,
        PurchaseOrderFetchFactory,


        CountryToSelectItemConverter,
        StateToSelectItemConverter,
        SelectItemToSelect2Converter,
        SubsToSelectItemConverter,
        VenueClientToSelectItemConverter,
        EventToSelectItemConverter,
        FunctionSpaceToSelectItemConverter,
        StringToSelectItemConverter,
        InventoryCategoryToSelectItemConverter,
        InventorySubCategoryItemToSelectItemConverter,
        RevenueCategoryToSelectItemConverter,
        CostCategoryToSelectItemConverter,
        ManufacturerToSelectItemConverter,
        VendorToSelectItemConverter,
        InventorySpaceToSelectItemConverter,
        InventoryOwnerToSelectItemConverter,
        VendorContactToSelectItemConverter,
        VenueContactToSelectItemConverter,
        InventoryToSelectItemConverter,
        SystemToSelectItemConverter,
        PurchaseOrderToSelectItemConverter,
        FunctionPackageToSelectItemConverter,
        FunctionEquipmentToSelectItemConverter,
        SelectItemIdToFuncPackageOrEquipmentConverter,

        SelectFetchTypeRegistryService
    ],
    declarations: [
        SelectComponent
    ],
    exports: [
        SelectComponent
    ]
})
export class SelectModule {
}
