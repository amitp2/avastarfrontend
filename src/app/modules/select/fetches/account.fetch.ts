import { SelectFetch } from '../types/select-fetch';
import { SelectItem } from '../types/select-item';
import { Injectable } from '@angular/core';
import { VenueClientResourceService } from '../../../services/resources/venue-client-resource.service';
import { PromisedStoreService } from '../../../services/promised-store.service';
import { UtilsService } from '../../../services/utils.service';
import { VenueClientToSelectItemConverter } from '../converters/venue-client-to-select-item.converter';

@Injectable()
export class AccountFetch implements SelectFetch {

    constructor(private accounts: VenueClientResourceService,
                private promisedStore: PromisedStoreService,
                private utils: UtilsService,
                private accountConverter: VenueClientToSelectItemConverter) {

    }

    async fetch(): Promise<SelectItem[]> {
        const venueId = await this.promisedStore.currentVenueId();
        const accounts = await this.accounts.getAllByVenueID(venueId);
        const converted = await this.utils.convertMany(accounts, this.accountConverter);
        return converted;
    }

}
