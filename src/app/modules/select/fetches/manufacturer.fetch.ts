import { SelectFetch } from '../types/select-fetch';
import { SelectItem } from '../types/select-item';
import { Injectable } from '@angular/core';
import { UtilsService } from '../../../services/utils.service';
import { ManufacturerToSelectItemConverter } from '../converters/manufacturer-to-select-item.converter';
import { ManufacturerResourceService } from '../../../services/resources/manufacturer-resource.service';

@Injectable()
export class ManufacturerFetch implements SelectFetch {


    constructor(
        private utils: UtilsService,
        private manufacturerConverter: ManufacturerToSelectItemConverter,
        private inventoryManufacturer: ManufacturerResourceService
    ) {
    }

    async fetch(): Promise<SelectItem[]> {
        const mans = await this.inventoryManufacturer.searchByName('');
        const withUsage = mans.filter(m => m.usage >= 0);
        const converted = await this.utils.convertMany(withUsage, this.manufacturerConverter);
        return converted;
    }

}
