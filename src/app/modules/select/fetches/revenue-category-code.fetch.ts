import { SelectFetch } from '../types/select-fetch';
import { SelectItem } from '../types/select-item';
import { RevenueCategoryToCodeConverter } from '../../../helpers/converters/revenue-category-to-code.converter';
import { UtilsService } from '../../../services/utils.service';
import { AuthService } from '../../../services/auth.service';
import { RevenueCategoryResourceService } from '../../../services/resources/revenue-category-resource.service';
import { StringToSelectItemConverter } from '../converters/string-to-select-item.converter';
import { Injectable } from '@angular/core';

@Injectable()
export class RevenueCategoryCodeFetch implements SelectFetch {

    constructor(
        private revenueCategoryConverter: RevenueCategoryToCodeConverter,
        private utils: UtilsService,
        private authService: AuthService,
        private revenueCats: RevenueCategoryResourceService,
        private stringConverter: StringToSelectItemConverter
    ) {
    }

    async fetch(): Promise<SelectItem[]> {
        const user = await this.authService.currentUser();
        const subsId = user.subscriberId;
        const cats = await this.revenueCats.getAllBySubscriberId(subsId);
        const codes = await this.utils.convertMany(cats, this.revenueCategoryConverter);
        const filteredCodes = codes.filter(c => !!c);
        const converted = await this.utils.convertMany(filteredCodes, this.stringConverter);
        return converted;
    }
}


@Injectable()
export class RevenueCategoryLocalCodeFetch implements SelectFetch {

    constructor(
        private utils: UtilsService,
        private stringConverter: StringToSelectItemConverter
    ) {
    }

    async fetch(): Promise<SelectItem[]> {
        const codes = ['E', 'X', 'N', 'I', 'P', 'R', 'L', 'B', 'T', 'W', 'Z', 'H', 'O', 'A', 'S', 'D', 'C', 'G'];
        const converted = await this.utils.convertMany(codes, this.stringConverter);
        return converted;
    }
}
