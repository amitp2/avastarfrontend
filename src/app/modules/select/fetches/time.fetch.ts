import { SelectFetch } from '../types/select-fetch';
import { SelectItem } from '../types/select-item';
import { Injectable } from '@angular/core';
import { StringToSelectItemConverter } from '../converters/string-to-select-item.converter';
import { UtilsService } from '../../../services/utils.service';

@Injectable()
export class TimeFetch implements SelectFetch {

    constructor(private stringConverter: StringToSelectItemConverter,
                private utils: UtilsService) {
    }

    private buildTimesStrings(): string[] {
        const times = [];
        for (let i = 0; i <= 23; i++) {
            for (let j = 0; j <= 30; j += 30) {
                 times.push(`${i < 10 ? 0 : ''}${i}:${j < 10 ? 0 : ''}${j}`);
            }
        }
        return times;
    }

    async fetch(): Promise<SelectItem[]> {
        const times = this.buildTimesStrings();
        const converted = await this.utils.convertMany(times, this.stringConverter);
        return converted;
    }
}
