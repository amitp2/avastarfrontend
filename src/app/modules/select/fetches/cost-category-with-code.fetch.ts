import { SelectFetch } from '../types/select-fetch';
import { SelectItem } from '../types/select-item';
import { Injectable } from '@angular/core';
import { UtilsService } from '../../../services/utils.service';
import { CostCategoryToSelectItemConverter } from '../converters/cost-category-to-select-item.converter';
import { CostCategoryResourceService } from '../../../services/resources/cost-category-resource.service';

@Injectable()
export class CostCategoryFetchWithCode implements SelectFetch {


    constructor(private utils: UtilsService,
                private costCategoryConverter: CostCategoryToSelectItemConverter,
                private costCategory: CostCategoryResourceService) {
    }

    async fetch(): Promise<SelectItem[]> {
        const cats = await this.costCategory.getAllForCurrentSubscriber();
        const catsWithCode = cats.filter(c => !!c.costCode);
        const converted = await this.utils.convertMany(catsWithCode, this.costCategoryConverter);
        return converted;
    }

}
