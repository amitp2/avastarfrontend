import { SelectFetch } from '../types/select-fetch';
import { Injectable } from '@angular/core';
import { SelectItem } from '../types/select-item';
import { FunctionSpaceFetch } from './function-space.fetch';
import { StorageLocationFetchFactory } from './factories/storage-location.fetch.factory';


@Injectable()
export class LocationFetch implements SelectFetch {
    private storageSpaceFetcher: SelectFetch;

    constructor(private functionSpaceFetcher: FunctionSpaceFetch, storageSpaceFetchFactory: StorageLocationFetchFactory) {
        this.storageSpaceFetcher = storageSpaceFetchFactory.create(true);
    }

    async fetch(): Promise<SelectItem[]> {
        const functionSpaces = await this.functionSpaceFetcher.fetch();
        const storageSpaces = await this.storageSpaceFetcher.fetch();

        storageSpaces.forEach(x => x.id += 9999999);
        storageSpaces.unshift(new SelectItem(-1, 'Unassigned Location', false, null));

        return [...storageSpaces, ...functionSpaces];
    }
}
