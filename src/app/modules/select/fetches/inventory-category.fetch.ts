import { SelectFetch } from '../types/select-fetch';
import { SelectItem } from '../types/select-item';
import { Injectable } from '@angular/core';
import { InventoryCategoryToSelectItemConverter } from '../converters/inventory-category-to-select-item.converter';
import { UtilsService } from '../../../services/utils.service';
import { InventoryCategoryResourceService } from '../../../services/resources/inventory-category-resource.service';

@Injectable()
export class InventoryCategoryFetch implements SelectFetch {

    constructor(private categoryConverter: InventoryCategoryToSelectItemConverter,
                private utils: UtilsService,
                private inventoryCategory: InventoryCategoryResourceService) {

    }

    async fetch(): Promise<SelectItem[]> {
        const invs = await this.inventoryCategory.getAll();
        const converted = await this.utils.convertMany(invs, this.categoryConverter);
        return converted;
    }

}
