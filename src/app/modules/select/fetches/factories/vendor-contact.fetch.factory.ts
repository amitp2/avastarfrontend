import { SelectFetchFactory } from '../../types/select-fetch-factory';
import { SelectFetch } from '../../types/select-fetch';
import { Injectable } from '@angular/core';
import { UtilsService } from '../../../../services/utils.service';
import { VendorContactToSelectItemConverter } from '../../converters/vendor-contact-to-select-item.converter';
import { VendorContactResourceService } from '../../../../services/resources/vendor-contact-resource.service';
import { SelectItem } from '../../types/select-item';

@Injectable()
export class VendorContactFetchFactory implements SelectFetchFactory {

    constructor(private utils: UtilsService,
                private contactConverter: VendorContactToSelectItemConverter,
                private vendorContact: VendorContactResourceService) {

    }

    create(vendorId: any): SelectFetch {
        if (!vendorId) {
            return this.utils.emptyFetch();
        }
        const utils = this.utils;
        const contactConverter = this.contactConverter;
        const vendorContact = this.vendorContact;
        return new (class VendorContactFetch implements SelectFetch {

            async fetch(): Promise<SelectItem[]> {
                const contacts = await vendorContact.getAllByVendorId(vendorId);
                const converted = await utils.convertMany(contacts, contactConverter);
                return converted;
            }

        });
    }

}
