import { SelectFetchFactory } from '../../types/select-fetch-factory';
import { SelectFetch } from '../../types/select-fetch';
import { Injectable } from '@angular/core';
import { UtilsService } from '../../../../services/utils.service';
import { InventoryResourceService } from '../../../../services/resources/inventory-resource.service';
import { PromisedStoreService } from '../../../../services/promised-store.service';
import { InventoryToSelectItemConverter } from '../../converters/inventory-to-select-item.converter';
import { SelectItem } from '../../types/select-item';

@Injectable()
export class InventoryFetchFactory implements SelectFetchFactory {

    constructor(private utils: UtilsService,
                private inventory: InventoryResourceService,
                private promisedStore: PromisedStoreService,
                private inventoryConverter: InventoryToSelectItemConverter) {
    }

    create(args: any): SelectFetch {
        if (!args) {
            return this.utils.emptyFetch();
        }
        const categoryId = args.categoryId;
        const subCategoryId = args.subCategoryId;

        const utils = this.utils;
        const inventory = this.inventory;
        const promisedStore = this.promisedStore;
        const inventoryConverter = this.inventoryConverter;
        return new (class InventoryFetch implements SelectFetch {

            async fetch(): Promise<SelectItem[]> {
                const venueId = await promisedStore.currentVenueId();
                let invs;
                if (!categoryId) {
                    invs = await inventory.getAllByVenueId(venueId);
                } else if (subCategoryId) {
                    invs = await inventory.getAllBySubCategoryId(subCategoryId, venueId);
                } else {
                    invs = await inventory.getAllByCategoryId(categoryId, venueId);
                }
                const converted = await utils.convertMany(invs, inventoryConverter);
                return converted;
            }

        });
    }

}
