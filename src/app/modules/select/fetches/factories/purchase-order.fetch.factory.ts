import { SelectFetchFactory } from '../../types/select-fetch-factory';
import { SelectFetch } from '../../types/select-fetch';
import { Injectable } from '@angular/core';
import { UtilsService } from '../../../../services/utils.service';
import { PurchaseOrderService } from '../../../../services/resources/purchase-order.service';
import { PromisedStoreService } from '../../../../services/promised-store.service';
import { PurchaseOrderToSelectItemConverter } from '../../converters/purchase-order-to-select-item.converter';
import { SelectItem } from '../../types/select-item';

@Injectable()
export class PurchaseOrderFetchFactory implements SelectFetchFactory {

    constructor(private utils: UtilsService,
                private purchaseOrderApi: PurchaseOrderService,
                private promisedStore: PromisedStoreService,
                private purchaseOrderConverter: PurchaseOrderToSelectItemConverter) {

    }

    create(vendorId: any): SelectFetch {
        const utils = this.utils;
        const purchaseOrderApi = this.purchaseOrderApi;
        const promisedStore = this.promisedStore;
        const purchaseOrderConverter = this.purchaseOrderConverter;
        return new (class PurchaseOrderFetch implements SelectFetch {

            async fetch(): Promise<SelectItem[]> {
                const venueId = await promisedStore.currentVenueId();
                const orders = await purchaseOrderApi.getAllByVenueId(venueId, {vendorId});
                const converted = await utils.convertMany(orders, purchaseOrderConverter);
                return converted;
            }

        });
    }

}
