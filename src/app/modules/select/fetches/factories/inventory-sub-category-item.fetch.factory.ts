import { SelectFetchFactory } from '../../types/select-fetch-factory';
import { SelectFetch } from '../../types/select-fetch';
import { Injectable } from '@angular/core';
import { UtilsService } from '../../../../services/utils.service';
import { InventoryCategoryResourceService } from '../../../../services/resources/inventory-category-resource.service';
import { SelectItem } from '../../types/select-item';
import { InventorySubCategoryItemToSelectItemConverter } from '../../converters/inventory-sub-category-item-to-select-item.converter';

@Injectable()
export class InventorySubCategoryItemFetchFactory implements SelectFetchFactory {

    constructor(private subCategoryItemConverter: InventorySubCategoryItemToSelectItemConverter,
                private utils: UtilsService,
                private inventoryCategory: InventoryCategoryResourceService) {
    }

    create(args: any): SelectFetch {
        if (!args) {
            return this.utils.emptyFetch();
        }
        const inventorySubCategoryId = args;
        const utils = this.utils;
        const inventoryCategory = this.inventoryCategory;
        const subCategoryItemConverter = this.subCategoryItemConverter;
        return new (class InventorySubCategoryFetch implements SelectFetch {
            async fetch(): Promise<SelectItem[]> {
                const items = await inventoryCategory.getAllSubcategoryItems(inventorySubCategoryId);
                const converted = await utils.convertMany(items, subCategoryItemConverter);
                return converted;
            }
        });
    }

}
