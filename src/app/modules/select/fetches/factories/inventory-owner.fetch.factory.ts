import { SelectFetchFactory } from '../../types/select-fetch-factory';
import { SelectFetch } from '../../types/select-fetch';
import { Injectable } from '@angular/core';
import { InventoryOwnerService } from '../../../route-inventory/services/inventory-owner.service';
import { UtilsService } from '../../../../services/utils.service';
import { InventoryOwnerToSelectItemConverter } from '../../converters/inventory-owner-to-select-item.converter';
import { InventoryAssetOwnerType } from '../../../../enums/inventory-asset-owner-type.enum';
import { SelectItem } from '../../types/select-item';

@Injectable()
export class InventoryOwnerFetchFactory implements SelectFetchFactory {

    constructor(private inventoryOwnerService: InventoryOwnerService,
                private utils: UtilsService,
                private ownerConverter: InventoryOwnerToSelectItemConverter) {

    }

    create(ownerType: InventoryAssetOwnerType): SelectFetch {
        if (!ownerType) {
            return this.utils.emptyFetch();
        }
        const utils = this.utils;
        const ownerConverter = this.ownerConverter;
        const inventoryOwnerService = this.inventoryOwnerService;
        return new (class InventoryOwnerFetch implements SelectFetch {
            async fetch(): Promise<SelectItem[]> {
                let owners = [];

                switch (ownerType) {
                    case InventoryAssetOwnerType.Vendor:
                        owners = await inventoryOwnerService.getOnlyVendors();
                        break;
                    case InventoryAssetOwnerType.Installed:
                        owners = await inventoryOwnerService.getOnlyOperator();
                        break;
                    case InventoryAssetOwnerType.Owner:
                        owners = await inventoryOwnerService.getOnlyOwner();
                        break;
                }

                const converted = await utils.convertMany(owners, ownerConverter);
                return converted;
            }
        });
    }

}
