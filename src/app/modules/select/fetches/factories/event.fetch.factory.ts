import { SelectFetchFactory } from '../../types/select-fetch-factory';
import { SelectFetch } from '../../types/select-fetch';
import { Injectable } from '@angular/core';
import { UtilsService } from '../../../../services/utils.service';
import { SelectItem } from '../../types/select-item';
import { VenueEventResourceService } from '../../../../services/resources/venue-event-resource.service';
import { EventToSelectItemConverter } from '../../converters/event-to-select-item.converter';

@Injectable()
export class EventFetchFactory implements SelectFetchFactory {

    constructor(private utils: UtilsService,
                private events: VenueEventResourceService,
                private eventConverter: EventToSelectItemConverter) {

    }

    create(args: any): SelectFetch {
        if (!args || !args.accountId) {
            return this.utils.emptyFetch();
        }
        const accountId = args.accountId;
        const events = this.events;
        const converter = this.eventConverter;
        const utils = this.utils;
        return new (class EventFetch implements SelectFetch {
            async fetch(): Promise<SelectItem[]> {
                const eventsList = await events.getAllByAccountId(accountId, null);
                const converted = await utils.convertMany(eventsList, converter);
                return converted;
            }
        });
    }

}
