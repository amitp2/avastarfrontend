import { SelectFetch } from '../../types/select-fetch';
import { SelectFetchFactory } from '../../types/select-fetch-factory';
import { Injectable } from '@angular/core';
import { UtilsService } from '../../../../services/utils.service';
import { InventorySpacesService } from '../../../route-inventory/services/inventory-spaces.service';
import { InventorySpaceToSelectItemConverter } from '../../converters/inventory-space-to-select-item.converter';
import { SelectItem } from '../../types/select-item';

@Injectable()
export class StorageLocationFetchFactory implements SelectFetchFactory {

    constructor(private utils: UtilsService,
                private inventorySpaces: InventorySpacesService,
                private locationConverter: InventorySpaceToSelectItemConverter) {
    }

    create(args: any): SelectFetch {
        const isPortable = !!args;
        const utils = this.utils;
        const inventorySpaces = this.inventorySpaces;
        const locationConverter = this.locationConverter;
        return new (class StorageLocationFetch implements SelectFetch {

            async fetch(): Promise<SelectItem[]> {
                let spaces;

                if (isPortable) {
                    spaces = await inventorySpaces.getOnlyStorageSpaces();
                } else {
                    spaces = await inventorySpaces.getOnlyEventSpaces();
                }
                const converted = await utils.convertMany(spaces, locationConverter);

                return converted;
            }

        });
    }

}
