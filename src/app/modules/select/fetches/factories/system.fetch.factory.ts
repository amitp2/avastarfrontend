import { SelectFetchFactory } from '../../types/select-fetch-factory';
import { Injectable } from '@angular/core';
import { SelectFetch } from '../../types/select-fetch';
import { UtilsService } from '../../../../services/utils.service';
import { PromisedStoreService } from '../../../../services/promised-store.service';
import { EquipmentSystemResourceService } from '../../../../services/resources/equipment-system-resource.service';
import { SelectItem } from '../../types/select-item';
import { SystemToSelectItemConverter } from '../../converters/system-to-select-item.converter';

@Injectable()
export class SystemFetchFactory implements SelectFetchFactory {

    constructor(private utils: UtilsService,
                private promisedStore: PromisedStoreService,
                private system: EquipmentSystemResourceService,
                private systemConverter: SystemToSelectItemConverter) {

    }

    create(search: any): SelectFetch {
        const utils = this.utils;
        const promisedStore = this.promisedStore;
        const system = this.system;
        const systemConverter = this.systemConverter;
        return new (class SystemFetch implements SelectFetch {


            async fetch(): Promise<SelectItem[]> {
                const venueId = await promisedStore.currentVenueId();
                const systems = await system.getAllByVenueId(venueId, { search });
                const converted = await utils.convertMany(systems, systemConverter);
                return converted;
            }

        });
    }

}
