import { SelectFetchFactory } from '../../types/select-fetch-factory';
import { Injectable } from '@angular/core';
import { SelectFetch } from '../../types/select-fetch';
import { InventoryCategoryToSelectItemConverter } from '../../converters/inventory-category-to-select-item.converter';
import { UtilsService } from '../../../../services/utils.service';
import { InventoryCategoryResourceService } from '../../../../services/resources/inventory-category-resource.service';
import { SelectItem } from '../../types/select-item';

@Injectable()
export class InventorySubCategoryFetchFactory implements SelectFetchFactory {

    constructor(private categoryConverter: InventoryCategoryToSelectItemConverter,
                private utils: UtilsService,
                private inventoryCategory: InventoryCategoryResourceService) {
    }

    create(args: any): SelectFetch {
        if (!args) {
            return this.utils.emptyFetch();
        }
        const inventoryCategoryId = args;
        const utils = this.utils;
        const inventoryCategory = this.inventoryCategory;
        const categoryConverter = this.categoryConverter;
        return new (class InventorySubCategoryFetch implements SelectFetch {
            async fetch(): Promise<SelectItem[]> {
                const cats = await inventoryCategory.getAllSubcategories(inventoryCategoryId);
                const converted = await utils.convertMany(cats, categoryConverter);
                return converted;
            }
        });
    }

}
