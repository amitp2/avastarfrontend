import { SelectFetch } from '../types/select-fetch';
import { SelectItem } from '../types/select-item';
import { Injectable } from '@angular/core';
import { StateResourceService } from '../../../services/resources/state-resource.service';
import { StateToSelectItemConverter } from '../converters/state-to-select-item.converter';
import { UtilsService } from '../../../services/utils.service';
import { StateModel } from '../../../models/state-model';

@Injectable()
export class StateFetch implements SelectFetch {

    constructor(private stateResource: StateResourceService,
                private stateConverter: StateToSelectItemConverter,
                private utils: UtilsService) {
    }

    async fetch(): Promise<SelectItem[]> {
        const states = await this.stateResource.getAll();
        const converted = await this.utils.convertMany<StateModel, SelectItem>(states, this.stateConverter);
        return converted;
    }

}
