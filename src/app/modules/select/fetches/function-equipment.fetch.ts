import { SelectFetch } from '../types/select-fetch';
import { SelectItem } from '../types/select-item';
import { Injectable } from '@angular/core';
import { PromisedStoreService } from '../../../services/promised-store.service';
import { PackageResourceService } from '../../../services/resources/package-resource.service';
import { EquipmentResourceService } from '../../../services/resources/equipment-resource.service';
import { UtilsService } from '../../../services/utils.service';
import { FunctionEquipmentToSelectItemConverter } from '../converters/function-equipment-to-select-item.converter';
import { FunctionPackageToSelectItemConverter } from '../converters/function-package-to-select-item.converter';

declare var R: any;

@Injectable()
export class FunctionEquipmentFetch implements SelectFetch {


    constructor(private promisedStore: PromisedStoreService,
                private packages: PackageResourceService,
                private equipments: EquipmentResourceService,
                private utils: UtilsService,
                private equipmentConverter: FunctionEquipmentToSelectItemConverter,
                private packageConverter: FunctionPackageToSelectItemConverter) {
    }

    async fetch(): Promise<SelectItem[]> {
        const venueId = await this.promisedStore.currentVenueId();

        const convertPackages = R.partialRight(this.utils.convertMany, [this.packageConverter]);
        const convertEquipments = R.partialRight(this.utils.convertMany, [this.equipmentConverter]);

        const [packages, equipments] = await Promise.all([
            this.packages.getAllByVenueId(venueId).then(convertPackages),
            //this.equipments.getAllByVenueId(venueId).then(convertEquipments)
            this.equipments.getAllByRentableTrue(venueId).then(convertEquipments)
        ]);

        const res = R.concat(packages, equipments);
        return res;
    }

}
