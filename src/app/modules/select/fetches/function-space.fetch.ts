import { SelectFetch } from '../types/select-fetch';
import { Injectable } from '@angular/core';
import { SelectItem } from '../types/select-item';
import { UtilsService } from '../../../services/utils.service';
import { FunctionSpaceResourceService } from '../../../services/resources/function-space-resource.service';
import { PromisedStoreService } from '../../../services/promised-store.service';
import { FunctionSpaceToSelectItemConverter } from '../converters/function-space-to-select-item.converter';

@Injectable()
export class FunctionSpaceFetch implements SelectFetch {

    constructor(private utils: UtilsService,
                private promisedStore: PromisedStoreService,
                private functionSpace: FunctionSpaceResourceService,
                private functionSpaceConverter: FunctionSpaceToSelectItemConverter) {
    }

    async fetch(): Promise<SelectItem[]> {
        const venueId = await this.promisedStore.currentVenueId();
        const spaces = await this.functionSpace.getAllFunctionSpacesByVenueId(venueId);
        const converted = await this.utils.convertMany(spaces, this.functionSpaceConverter);
        return converted;
    }
}
