import { SelectFetch } from '../types/select-fetch';
import { SelectItem } from '../types/select-item';

export class YearFetch implements SelectFetch {

    async fetch(): Promise<SelectItem[]> {
        const res = [];
        let i = 2000;
        for (; i <= 2050; i++) {
            res.push(new SelectItem(i, i.toString(), false, null));
        }
        return res;
    }

}
