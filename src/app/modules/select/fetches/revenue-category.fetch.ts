import { SelectFetch } from '../types/select-fetch';
import { UtilsService } from '../../../services/utils.service';
import { SelectItem } from '../types/select-item';
import { RevenueCategoryResourceService } from '../../../services/resources/revenue-category-resource.service';
import { RevenueCategoryToSelectItemConverter } from '../converters/revenue-category-to-select-item.converter';
import { Injectable } from '@angular/core';

@Injectable()
export class RevenueCategoryFetch implements SelectFetch {

    constructor(private revenueCategoryConverter: RevenueCategoryToSelectItemConverter,
                private utils: UtilsService,
                private revenueCats: RevenueCategoryResourceService) {
    }

    async fetch(): Promise<SelectItem[]> {
        const cats = await this.revenueCats.getAllForCurrentSubscriber();
        const converted = await this.utils.convertMany(cats, this.revenueCategoryConverter);
        return converted;
    }

}
