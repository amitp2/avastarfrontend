import { SelectFetch } from '../types/select-fetch';
import { SelectItem } from '../types/select-item';
import { Injectable } from '@angular/core';
import { UtilsService } from '../../../services/utils.service';
import { VendorToSelectItemConverter } from '../converters/vendor-to-select-item.converter';
import { VendorResourceService } from '../../../services/resources/vendor-resource.service';
import { AuthService } from '../../../services/auth.service';

@Injectable()
export class VendorFetch implements SelectFetch {

    constructor(private utils: UtilsService,
                private vendorConverter: VendorToSelectItemConverter,
                private vendorResource: VendorResourceService,
                private authService: AuthService) {

    }

    async fetch(): Promise<SelectItem[]> {
        const user = await this.authService.currentUser();
        const subsId = user.subscriberId;
        const vendors = await this.vendorResource.getAllBySubscriber(subsId);
        const converted = await this.utils.convertMany(vendors, this.vendorConverter);
        return converted;
    }
}
