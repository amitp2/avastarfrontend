import { SelectFetch } from '../types/select-fetch';
import { SelectItem } from '../types/select-item';

export class MonthFetch implements SelectFetch {

    private months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ];

    async fetch(): Promise<SelectItem[]> {
        return this.months.map((m, i) => new SelectItem(i, m, false, null));
    }

}
