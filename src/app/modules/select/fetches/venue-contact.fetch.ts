import { SelectFetch } from '../types/select-fetch';
import { SelectItem } from '../types/select-item';
import { Injectable } from '@angular/core';
import { UtilsService } from '../../../services/utils.service';
import { VenueContactResourceService } from '../../../services/resources/venue-contact-resource.service';
import { VenueContactToSelectItemConverter } from '../converters/venue-contact-to-select-item.converter';
import { PromisedStoreService } from '../../../services/promised-store.service';

@Injectable()
export class VenueContactFetch implements SelectFetch {

    constructor(private utils: UtilsService,
                private venueContactApi: VenueContactResourceService,
                private venueContactConverter: VenueContactToSelectItemConverter,
                private promisedStore: PromisedStoreService) {

    }

    async fetch(): Promise<SelectItem[]> {
        const venueId = await this.promisedStore.currentVenueId();
        const contacts = await this.venueContactApi.getAllByVenueId(venueId);
        const converted = await this.utils.convertMany(contacts, this.venueContactConverter);
        return converted;
    }

}
