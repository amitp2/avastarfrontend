import { SelectFetch } from '../types/select-fetch';
import { SelectItem } from '../types/select-item';
import { Injectable } from '@angular/core';
import { CountryResourceService } from '../../../services/resources/country-resource.service';
import { UtilsService } from '../../../services/utils.service';
import { CountryToSelectItemConverter } from '../converters/country-to-select-item.converter';
import { CountryModel } from '../../../models/country-model';

@Injectable()
export class CountryFetch implements SelectFetch {

    constructor(private countryResourceService: CountryResourceService,
                private utils: UtilsService,
                private countryConverter: CountryToSelectItemConverter) {
    }

    async fetch(): Promise<SelectItem[]> {
        const countries = await this.countryResourceService.getAll();
        const converted = await this.utils.convertMany<CountryModel, SelectItem>(countries, this.countryConverter);
        return converted;
    }

}
