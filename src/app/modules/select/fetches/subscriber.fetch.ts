import { SelectFetch } from '../types/select-fetch';
import { Injectable } from '@angular/core';
import { SelectItem } from '../types/select-item';
import { UtilsService } from '../../../services/utils.service';
import { SubscriberResourceService } from '../../../services/resources/subscriber-resource.service';
import { SubscriberModel } from '../../../models/subscriber-model';
import { SubsToSelectItemConverter } from '../converters/subs-to-select-item.converter';

@Injectable()
export class SubscriberFetch implements SelectFetch {

    constructor(private utils: UtilsService,
                private subscriberResourceService: SubscriberResourceService,
                private subsConverter: SubsToSelectItemConverter) {

    }

    async fetch(): Promise<SelectItem[]> {
        const subs = await this.subscriberResourceService.getAll();
        const converted = await this.utils.convertMany<SubscriberModel, SelectItem>(subs, this.subsConverter);
        return converted;
    }

}
