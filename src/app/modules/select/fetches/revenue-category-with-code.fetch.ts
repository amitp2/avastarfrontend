import { SelectFetch } from '../types/select-fetch';
import { Injectable } from '@angular/core';
import { SelectItem } from '../types/select-item';
import { UtilsService } from '../../../services/utils.service';
import { RevenueCategoryResourceService } from '../../../services/resources/revenue-category-resource.service';
import { RevenueCategoryToSelectItemConverter } from '../converters/revenue-category-to-select-item.converter';

@Injectable()
export class RevenueCategoryWithCodeFetch implements SelectFetch {

    constructor(private revenueCategoryConverter: RevenueCategoryToSelectItemConverter,
                private utils: UtilsService,
                private revenueCats: RevenueCategoryResourceService) {
    }

    async fetch(): Promise<SelectItem[]> {
        const cats = await this.revenueCats.getAllForCurrentSubscriber();
        const withCodes = cats.filter(c => !!c.revenueCode);
        const converted = await this.utils.convertMany(withCodes, this.revenueCategoryConverter);
        return converted;
    }

}
