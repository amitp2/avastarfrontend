import { Injectable } from '@angular/core';
import { SelectFetchTypeRegistry } from '../types/select-fetch-type-registry';
import { SelectFetchType } from '../types/select-fetch-type';
import { SelectFetch } from '../types/select-fetch';
import { CountryFetch } from '../fetches/country.fetch';
import { StateFetch } from '../fetches/state.fetch';
import { SubscriberFetch } from '../fetches/subscriber.fetch';
import { AccountFetch } from '../fetches/account.fetch';
import { FunctionSpaceFetch } from '../fetches/function-space.fetch';
import { TimeFetch } from '../fetches/time.fetch';
import { RevenueCategoryCodeFetch, RevenueCategoryLocalCodeFetch } from '../fetches/revenue-category-code.fetch';
import { InventoryCategoryFetch } from '../fetches/inventory-category.fetch';
import { RevenueCategoryFetch } from '../fetches/revenue-category.fetch';
import { CostCategoryFetch } from '../fetches/cost-category.fetch';
import { ManufacturerFetch } from '../fetches/manufacturer.fetch';
import { VendorFetch } from '../fetches/vendor.fetch';
import { VenueContactFetch } from '../fetches/venue-contact.fetch';
import { MonthFetch } from '../fetches/month.fetch';
import { YearFetch } from '../fetches/year.fetch';
import { RevenueCategoryWithCodeFetch } from '../fetches/revenue-category-with-code.fetch';
import { CostCategoryFetchWithCode } from '../fetches/cost-category-with-code.fetch';
import { FunctionEquipmentFetch } from '../fetches/function-equipment.fetch';
import { LocationFetch } from '../fetches/location.fetch';

@Injectable()
export class SelectFetchTypeRegistryService implements SelectFetchTypeRegistry {

    constructor(
        private countryFetch: CountryFetch,
        private stateFetch: StateFetch,
        private subscriberFetch: SubscriberFetch,
        private accountFetch: AccountFetch,
        private functionSpaceFetch: FunctionSpaceFetch,
        private timeFetch: TimeFetch,
        private revenueCategoryCodeFetch: RevenueCategoryCodeFetch,
        private revenueCategoryCodeLocalFetch: RevenueCategoryLocalCodeFetch,
        private inventoryCategoryFetch: InventoryCategoryFetch,
        private revenueCategoryFetch: RevenueCategoryFetch,
        private revenueCategoryWithCodeFetch: RevenueCategoryWithCodeFetch,
        private costCategoryFetch: CostCategoryFetch,
        private costCategoryWithCodeFetch: CostCategoryFetchWithCode,
        private manufacturerFetch: ManufacturerFetch,
        private vendorFetch: VendorFetch,
        private venueContactFetch: VenueContactFetch,
        private monthFetch: MonthFetch,
        private yearFetch: YearFetch,
        private functionEquipment: FunctionEquipmentFetch,
        private locationFetch: LocationFetch
    ) {
    }

    getFetch(type: SelectFetchType): SelectFetch {
        switch (type) {
            case SelectFetchType.Country:
                return this.countryFetch;
            case SelectFetchType.State:
                return this.stateFetch;
            case SelectFetchType.Subscriber:
                return this.subscriberFetch;
            case SelectFetchType.Account:
                return this.accountFetch;
            case SelectFetchType.FunctionSpace:
                return this.functionSpaceFetch;
            case SelectFetchType.Time:
                return this.timeFetch;
            case SelectFetchType.RevenueCategoryCode:
                return this.revenueCategoryCodeFetch;
            case SelectFetchType.InventoryCategory:
                return this.inventoryCategoryFetch;
            case SelectFetchType.RevenueCategory:
                return this.revenueCategoryFetch;
            case SelectFetchType.RevenueCategoryWithCode:
                return this.revenueCategoryWithCodeFetch;
            case SelectFetchType.CostCategory:
                return this.costCategoryFetch;
            case SelectFetchType.CostCategoryWithCode:
                return this.costCategoryWithCodeFetch;
            case SelectFetchType.RevenueCategoryLocalCode:
                return this.revenueCategoryCodeLocalFetch;
            case SelectFetchType.Manufacturer:
                return this.manufacturerFetch;
            case SelectFetchType.Vendor:
                return this.vendorFetch;
            case SelectFetchType.VenueContact:
                return this.venueContactFetch;
            case SelectFetchType.Month:
                return this.monthFetch;
            case SelectFetchType.Year:
                return this.yearFetch;
            case SelectFetchType.FunctionEquipment:
                return this.functionEquipment;
            case SelectFetchType.Location:
                return this.locationFetch;
        }
        throw new Error(`Fetch type [${type}] doesn\'t have actual fetch, implement one or create factory`);
    }

}
