import { SelectFetchType } from './select-fetch-type';
import { SelectFetch } from './select-fetch';

export interface SelectFetchTypeRegistry {
    getFetch(type: SelectFetchType): SelectFetch;
}
