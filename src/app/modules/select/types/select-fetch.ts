import { SelectItem } from './select-item';

export interface SelectFetch {
    fetch(): Promise<SelectItem[]>;
}
