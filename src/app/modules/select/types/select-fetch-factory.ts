import { SelectFetch } from './select-fetch';

export interface SelectFetchFactory {
    create(args: any): SelectFetch;
}
