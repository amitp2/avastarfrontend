import { Copyable } from '../../../helpers/copyable';

export class SelectItem implements Copyable<SelectItem> {
    id: any;
    readonly display: string;
    readonly disabled: boolean;
    readonly payload: any;

    constructor(id: any, display: string, disabled: boolean, payload: any) {
        this.id = id;
        this.display = display;
        this.disabled = disabled;
        this.payload = payload;
    }

    copy(): SelectItem {
        return new SelectItem(
            this.id,
            this.display,
            this.disabled,
            this.payload
        );
    }

}
