import { SelectFetchType } from '../select-fetch-type';
import { SelectFetch } from '../select-fetch';
import { SelectFetchFactory } from '../select-fetch-factory';

export function isSelectFetchType(item: SelectFetchType | SelectFetch | SelectFetchFactory): item is SelectFetchType {
    return (<SelectFetch>item).fetch === undefined && (<SelectFetchFactory>item).create === undefined;
}
