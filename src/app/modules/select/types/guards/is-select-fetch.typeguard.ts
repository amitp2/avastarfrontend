import { SelectFetch } from '../select-fetch';

export function isSelectFetch(item: SelectFetch | any): item is SelectFetch {
    return (<SelectFetch> item).fetch !== undefined;
}
