import { SelectFetchFactory } from '../select-fetch-factory';

export function isSelectFetchFactory(item: SelectFetchFactory | any): item is SelectFetchFactory {
    return (<SelectFetchFactory>item).create !== undefined;
}
