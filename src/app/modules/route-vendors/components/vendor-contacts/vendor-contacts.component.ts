import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { UtilsService } from '../../../../services/utils.service';
import { VendorContactResourceService } from '../../../../services/resources/vendor-contact-resource.service';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { VendorContactModel } from '../../../../models/vendor-contact-model';
import { PagingTableService } from '../../../../services/paging-table.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-vendor-contacts',
    templateUrl: './vendor-contacts.component.html',
    styleUrls: ['./vendor-contacts.component.styl'],
    providers: [PagingTableService]
})
export class VendorContactsComponent implements OnInit, OnDestroy {

    @Input() vendorId: number;
    @ViewChild('table') table: PagingTableComponent;

    columns: PagingTableHeaderColumn[] = [
        {isSortable: true, title: 'Name', key: 'firstName'},
        {isSortable: true, title: 'Direct Phone', key: 'phone'},
        {isSortable: true, title: 'Email', key: 'email'},
        {isSortable: true, title: 'Department', key: 'department'},
        {isSortable: false, title: 'Actions', key: 'actions'}
    ];
    freshSubs: Subscription;

    constructor(private utilsService: UtilsService,
                private vendorContacts: VendorContactResourceService,
                private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.freshSubs = this.activatedRoute.queryParams
            .map((params: any) => params.fresh)
            .filter(fresh => fresh ? true : false)
            .subscribe(() => {
                this.table.refresh();
            });
    }

    ngOnDestroy() {
        this.freshSubs.unsubscribe();
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return this.vendorContacts.queryByVendorId(this.utilsService.tableToHttpParams(params), this.vendorId);
        }).bind(this);
    }

    delete(vendor: VendorContactModel) {
        this.vendorContacts
            .delete(vendor.id)
            .then(() => {
                this.table.delete((u) => +u.id === +vendor.id);
            });
    }

}
