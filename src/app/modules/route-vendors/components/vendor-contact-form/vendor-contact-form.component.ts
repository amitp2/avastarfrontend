import { Component } from '@angular/core';
import { FormHandler } from '../../../../classes/form-handler';
import { VendorContactModel } from '../../../../models/vendor-contact-model';
import { UserRole } from '../../../../enums/user-role.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { VendorContactResourceService } from '../../../../services/resources/vendor-contact-resource.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';

@Component({
    selector: 'app-vendor-contact-form',
    templateUrl: './vendor-contact-form.component.html',
    styleUrls: ['./vendor-contact-form.component.styl']
})
export class VendorContactFormComponent extends FormHandler<VendorContactModel> {

    private currentVendorId: number;
    userRole = UserRole;

    constructor(private activatedRoute: ActivatedRoute,
                private vendorContacts: VendorContactResourceService,
                private router: Router) {
        super();
        this.activatedRoute.parent.params.subscribe((params: any) => this.currentVendorId = +params.id);
    }

    initializeForm(): FormGroup {
        return new FormGroup({
            firstName: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            lastName: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            title: new FormControl(null, [Validators.maxLength(100)]),
            email: new FormControl(null, [Validators.required, Validators.email, Validators.maxLength(100)]),
            mobile: new FormControl(null, [Validators.maxLength(30)]),
            phone: new FormControl(null, [Validators.required, Validators.maxLength(30)]),
            role: new FormControl(UserRole.NONE, [Validators.required]),
            notes: new FormControl(null, [Validators.maxLength(400)]),
            fax: new FormControl(null, [Validators.maxLength(30)]),
            type: new FormControl(null, [Validators.maxLength(100)]),
            department: new FormControl(null, [Validators.maxLength(250)])
        });
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    @AsyncMethod({
        taskName: 'add_vendor_contact',
        success: AppSuccess.VENDOR_CONTACT_ADD,
        error: AppError.VENDOR_CONTACT_ADD
    })
    add(model: VendorContactModel): Promise<any> {
        return this.vendorContacts
            .addForVendor(model, this.currentVendorId)
            .then(() => this.router.navigateByUrl(`/u/vendors/${this.currentVendorId}?fresh=${Date.now()}`));
    }

    @AsyncMethod({
        taskName: 'load_vendor_contact'
    })
    get(id: number): Promise<VendorContactModel> {
        return this.vendorContacts.get(id);
    }

    @AsyncMethod({
        taskName: 'edit_vendor_contact',
        success: AppSuccess.VENDOR_CONTACT_EDIT,
        error: AppError.VENDOR_CONTACT_EDIT
    })
    edit(id: number, model: VendorContactModel): Promise<any> {
        return this.vendorContacts
            .edit(id, model)
            .then(() => this.router.navigateByUrl(`/u/vendors/${this.currentVendorId}?fresh=${Date.now()}`));
    }

    serialize(model: VendorContactModel): any {
        const serialized: any = {
            ...model
        };
        delete serialized.id;
        return serialized;
    }

    deserialize(formValue: any): VendorContactModel {
        return new VendorContactModel({
            ...formValue,
            role: +formValue.role
        });
    }

    backLink() {
        return `/u/vendors/${this.currentVendorId}`;
    }

}
