import { Component, OnInit, ViewChild } from '@angular/core';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { UserRole } from '../../../../enums/user-role.enum';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { VendorModel } from '../../../../models/vendor-model';
import { UtilsService } from '../../../../services/utils.service';
import { VendorResourceService } from '../../../../services/resources/vendor-resource.service';
import { AuthService } from '../../../../services/auth.service';
import { CurrentUser } from '../../../../interfaces/current-user';
import { PagingTableService } from '../../../../services/paging-table.service';

@Component({
    selector: 'app-vendors',
    templateUrl: './vendors.component.html',
    styleUrls: ['./vendors.component.styl'],
    providers: [PagingTableService]
})
export class VendorsComponent implements OnInit {

    @ViewChild('table') table: PagingTableComponent;

    columns: PagingTableHeaderColumn[] = [
        {isSortable: true, title: 'Company Name', key: 'name'},
        {isSortable: true, title: 'Phone', key: 'phone'},
        {isSortable: true, title: 'Fax', key: 'fax'},
        {isSortable: false, title: 'Actions', key: 'actions'}
    ];

    userRole = UserRole;

    constructor(private utilsService: UtilsService,
                private vendors: VendorResourceService,
                private authService: AuthService) {
    }

    ngOnInit() {
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return new Promise((resolve, reject) => {

                this.authService
                    .currentUser()
                    .then((user: CurrentUser) => {
                        this.vendors
                            .queryBySubscriber(this.utilsService.tableToHttpParams(params), user.subscriberId)
                            .then((data) => {
                                resolve(data);
                            })
                            .catch(() => reject());
                    })
                    .catch(() => reject());

            });
        }).bind(this);
    }

    delete(vendor: VendorModel) {
        this.vendors
            .delete(vendor.id)
            .then(() => {
                this.table.delete((u) => +u.id === +vendor.id);
            });
    }

}
