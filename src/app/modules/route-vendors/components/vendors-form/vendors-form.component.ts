import { Component, OnInit } from '@angular/core';
import { FormHandler } from '../../../../classes/form-handler';
import { VendorModel } from '../../../../models/vendor-model';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { VendorResourceService } from '../../../../services/resources/vendor-resource.service';
import { UtilsService } from '../../../../services/utils.service';
import { ObjectTransformerType } from '../../../../enums/object-transformer-type.enum';
import { AuthService } from '../../../../services/auth.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { UserRole } from '../../../../enums/user-role.enum';
import { VendorStatus } from '../../../../enums/vendor-status.enum';
import { PagingTableService } from '../../../../services/paging-table.service';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';
import { countryByCode, stateByCode, userLocation } from '../../../../store/selectors';
import { StateModel } from '../../../../models/state-model';
import { CountryModel } from '../../../../models/country-model';
import { GeolocationDataModel } from '../../../../models/geolocation-data.model';
import * as $ from 'jQuery';
const transformer = {
    address: {
        type: ObjectTransformerType.Default,
        to: 'address'
    },
    name: {
        type: ObjectTransformerType.Default,
        to: 'name'
    },
    phone: {
        type: ObjectTransformerType.Default,
        to: 'phone'
    },
    fax: {
        type: ObjectTransformerType.Default,
        to: 'fax'
    },
    website: {
        type: ObjectTransformerType.Default,
        to: 'website'
    }
};

@Component({
    selector: 'app-vendors-form',
    templateUrl: './vendors-form.component.html',
    styleUrls: ['./vendors-form.component.css'],
    providers: [PagingTableService]
})
export class VendorsFormComponent extends FormHandler<VendorModel> implements OnInit {

    userRole = UserRole;
    mode = FormMode;

    SelectFetchType = SelectFetchType;

    constructor(
        private activatedRoute: ActivatedRoute,
        private vendors: VendorResourceService,
        private utilsService: UtilsService,
        private authService: AuthService,
        private store: Store<AppState>,
        private router: Router
    ) {
        super();
    }

    ngOnInit() {
        super.ngOnInit();
        this.form.get('address.countryId')
            .valueChanges
            .subscribe((id: number) => +id !== 233 ? this.form.get('address.stateId').disable() : this.form.get('address.stateId').enable());

        this.formMode().subscribe(mode => {
            if (mode === FormMode.Add) {
                this.populateForm();
            }
        });
    }


    hasError(vnt) {
        console.log("Has error", vnt);
    }

    getNumber(vnt) {
        console.log("Get number error", vnt);
    }
    telInputObject(vnt) {
        console.log("Tel Input", vnt);
    }
    onCountryChange(vnt) {
        console.log("Country Chnage", vnt);
    }


    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    initializeForm(): FormGroup {
        return new FormGroup({
            name: new FormControl(null, [Validators.required]),
            address: new FormGroup(({
                countryId: new FormControl(233, [Validators.required]),
                stateId: new FormControl(),
                city: new FormControl(null, [Validators.required]),
                address: new FormControl(null, [Validators.required]),
                address2: new FormControl(),
                postCode: new FormControl(null, [Validators.required, Validators.maxLength(10)])
            })),
            phone: new FormControl(),
            fax: new FormControl(),
            website: new FormControl()
        });
    }

    private async populateForm() {
        const location: GeolocationDataModel = await this.utilsService.toPromise(this.store.select(userLocation()));
        if (location.countryCode) {
            const country: CountryModel = await this.utilsService.toPromise(this.store.select(countryByCode(location.countryCode)));
            this.form.get('address.countryId').setValue(country.id);
        }
        // if (location.regionCode) {
        //     const state: StateModel = await this.utilsService.toPromise(this.store.select(stateByCode(location.regionCode)));
        //     if (state) {
        //         this.form.get('address.stateId').setValue(state.id);
        //     }
        // }
        // if (location.city) {
        //     this.form.get('address.city').setValue(location.city);
        // }
        // if (location.zipCode) {
        //     this.form.get('address.postCode').setValue(location.zipCode);
        // }
        this.form.markAsPristine();
    }

    @AsyncMethod({
        taskName: 'get_vendor'
    })
    get(id: number): Promise<VendorModel> {
        return this.vendors.get(id);
    }

    @AsyncMethod({
        taskName: 'vendor_add',
        success: AppSuccess.VENDOR_ADD,
        error: AppError.VENDOR_ADD
    })
    add(model: VendorModel): Promise<any> {
        model.status = VendorStatus.Active;
        return this.vendors
            .addForSubscriber(model, this.authService.currentUserSnapshot.subscriberId)
            .then((newOne: VendorModel) => {
                this.router.navigateByUrl(`/u/vendors/${newOne.id}`);
            });
    }

    @AsyncMethod({
        taskName: 'vendor_add',
        success: AppSuccess.VENDOR_EDIT,
        error: AppError.VENDOR_EDIT
    })
    edit(id: number, model: VendorModel): Promise<any> {
        model.status = VendorStatus.Active;
        return this.vendors
            .edit(id, model);
    }

    serialize(model: VendorModel): any {
        return this.utilsService.transformInto(model, transformer);
    }

    deserialize(formValue: any): VendorModel {
        return new VendorModel(this.utilsService.transformInto(formValue, {
            ...transformer
        }));
    }

}
