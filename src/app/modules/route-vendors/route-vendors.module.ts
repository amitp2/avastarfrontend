import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorsComponent } from './components/vendors/vendors.component';
import { VendorsFormComponent } from './components/vendors-form/vendors-form.component';
import { SharedModule } from '../shared/shared.module';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VendorContactsComponent } from './components/vendor-contacts/vendor-contacts.component';
import { VendorContactFormComponent } from './components/vendor-contact-form/vendor-contact-form.component';
import { SelectModule } from '../select/select.module';
import { MaskedInputModule } from '../masked-input/masked-input.module';
import { TableResizeModule } from '../table-resize/table-resize.module';

const routes: Routes = [
    {path: '', pathMatch: 'full', component: VendorsComponent},
    {
        path: ':id', component: VendorsFormComponent,
        children: [
            {path: 'contact/:id', component: VendorContactFormComponent}
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        PagingTableModule,
        FormsModule,
        ReactiveFormsModule,
        TableResizeModule,
        RouterModule.forChild(routes),
        SelectModule,
        MaskedInputModule
    ],
    declarations: [
        VendorsComponent,
        VendorsFormComponent,
        VendorContactsComponent,
        VendorContactFormComponent
    ]
})
export class RouteVendorsModule {
}
