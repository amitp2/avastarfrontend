import { Component, forwardRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { InventoryAssetStatus } from '../../../../../enums/inventory-asset-status.enum';
import { EquipmentResourceService } from '../../../../../services/resources/equipment-resource.service';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../../store/store';
import { UtilsService } from '../../../../../services/utils.service';
import { EquipmentSystemResourceService } from '../../../../../services/resources/equipment-system-resource.service';
import { PagingTableService } from '../../../../../services/paging-table.service';
import { PagingTableLoadParams } from '../../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../../interfaces/paging-table-load-result';
import { VenueModel } from '../../../../../models/venue-model';
import { PagingTableComponent } from '../../../../paging-table/components/paging-table/paging-table.component';
import { InventoryStorageType } from '../../../../../enums/inventory-storage-type.enum';
import { DragulaService } from 'ng2-dragula';
import { Subscription } from 'rxjs/Subscription';
import { InventoryResourceService } from '../../../../../services/resources/inventory-resource.service';
import { PromisedStoreService } from '../../../../../services/promised-store.service';
import { InventoryModel } from '../../../../../models/inventory-model';
import { SelectFetchType } from '../../../../select/types/select-fetch-type';
import { InventorySubCategoryFetchFactory } from '../../../../select/fetches/factories/inventory-sub-category.fetch.factory';

interface EquipmentCouple {
    model: InventoryModel;
    element: HTMLInputElement;
}

@Component({
    selector: 'app-inventory-select',
    templateUrl: './inventory-select.component.html',
    styleUrls: ['./inventory-select.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => InventorySelectComponent),
            multi: true
        },
        PagingTableService
    ]
})
export class InventorySelectComponent implements ControlValueAccessor, OnInit, OnDestroy {

    @ViewChild('table') table: PagingTableComponent;

    onChange: any;
    onTouched: any;
    isDisabled: boolean;
    currentValue: any;
    dropModelSubs: Subscription;

    filterForm: FormGroup;
    status = InventoryAssetStatus;

    selectedEquipment: InventoryModel[];
    selectedEquipmentIds: any;
    leftSideEquipment: EquipmentCouple[];
    rightSideEquipment: InventoryModel[];
    SelectFetchType = SelectFetchType;


    constructor(
        private equipmentResource: EquipmentResourceService,
        private equipmentSystemResource: EquipmentSystemResourceService,
        private store: Store<AppState>,
        private utils: UtilsService,
        private dragulaService: DragulaService,
        private inventory: InventoryResourceService,
        private promisedStore: PromisedStoreService,
        public inventorySubCategoryFetchFactory: InventorySubCategoryFetchFactory
    ) {
        this.filterForm = new FormGroup({
            categoryId: new FormControl(),
            subCategoryId: new FormControl(),
            barcode: new FormControl(),
            search: new FormControl(),
            tagNumber: new FormControl(),
            condition: new FormControl(InventoryAssetStatus.All)
        });

        this.selectedEquipmentIds = {};
        this.leftSideEquipment = [];
        this.rightSideEquipment = [];
        this.selectedEquipment = [];

        this.dropModelSubs = this.dragulaService.dropModel.subscribe(() => {
            this.notifyUpdateOutside();
            console.log(this.selectedEquipment);
        });
    }

    applyFilters() {
        this.table.refresh();
    }

    cancel() {
        this.filterForm.reset({
            categoryId: '',
            storage: { id: '', type: InventoryStorageType.StorageSpace },
            condition: InventoryAssetStatus.All
        });
        this.applyFilters();
    }

    private attachFormFilter(obj: any, filter: string): boolean {
        if (this.filterForm.get(filter).value) {
            obj[filter] = this.filterForm.get(filter).value;
            return true;
        }
        return false;
    }

    private serializeCondition(condition: InventoryAssetStatus): string {
        switch (condition) {
            case InventoryAssetStatus.All:
                return null;
            case InventoryAssetStatus.Active:
                return 'ACTIVE';
            case InventoryAssetStatus.AtRepair:
                return 'AT_REPAIR';
            case InventoryAssetStatus.Broken:
                return 'BROKEN';
            case InventoryAssetStatus.Missing:
                return 'MISSING';
            case InventoryAssetStatus.Removed:
                return 'REMOVED';
            case InventoryAssetStatus.ServiceRequired:
                return 'SERVICE_REQUIRED';
        }
    }

    private syncSelectedEquipmentAndIds() {
        this.selectedEquipmentIds = {};
        this.selectedEquipment.forEach((m: InventoryModel) => this.selectedEquipmentIds[m.id] = true);
    }

    private notifyUpdateOutside() {
        if (this.onChange) {
            this.onChange(this.selectedEquipment.map((m: InventoryModel) => m.id));
        }
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            const httpParams = this.utils.tableToHttpParams(params);

            httpParams.otherParams = {};

            this.attachFormFilter(httpParams.otherParams, 'categoryId');
            this.attachFormFilter(httpParams.otherParams, 'subCategoryId');
            this.attachFormFilter(httpParams.otherParams, 'search');
            this.attachFormFilter(httpParams.otherParams, 'barcode');
            this.attachFormFilter(httpParams.otherParams, 'tagNumber');

            if (this.filterForm.get('condition').value) {
                httpParams.otherParams.condition = this.serializeCondition(this.filterForm.get('condition').value);
            }

            return this.promisedStore.currentVenueId()
                .then((venueId: number) => this.inventory.queryByVenueIdWithManufacturerNames(httpParams, venueId));
        }).bind(this);
    }

    checkedLeft(event: MouseEvent, model: InventoryModel) {
        const element = <HTMLInputElement> event.target;
        if (element.checked) {
            this.leftSideEquipment.push({ model, element });
        } else {
            this.leftSideEquipment = this.leftSideEquipment.filter((m: EquipmentCouple) => +m.model.id !== +model.id);
        }
    }

    checkedRight(event: MouseEvent, model: InventoryModel) {
        const element = <HTMLInputElement> event.target;
        if (element.checked) {
            this.rightSideEquipment.push(model);
        } else {
            this.rightSideEquipment = this.rightSideEquipment.filter((m: InventoryModel) => +m.id !== +model.id);
        }
    }

    add() {
        this.selectedEquipment = this.selectedEquipment.concat(this.leftSideEquipment.map((m: EquipmentCouple) => m.model));
        this.leftSideEquipment.forEach((m: EquipmentCouple) => m.element.checked = false);
        this.leftSideEquipment = [];
        this.syncSelectedEquipmentAndIds();
        this.notifyUpdateOutside();
    }

    remove() {
        this.selectedEquipment = this.selectedEquipment.filter((m: InventoryModel) => {
            return !this.rightSideEquipment.find((mm: InventoryModel) => +mm.id === +m.id);
        });
        this.rightSideEquipment = [];
        this.syncSelectedEquipmentAndIds();
        this.notifyUpdateOutside();
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.dropModelSubs.unsubscribe();
    }

    writeValue(obj: any): void {
        this.currentValue = obj;
        const idsOrders = {};

        if (obj && obj.length > 0) {
            const result = [];
            obj.forEach((id, index) => {
                idsOrders[id] = index;
                result.push(null);
            });


            this.utils
                .toPromise(this.store.select('currentVenue').filter((venue: VenueModel) => venue.id ? true : false))
                .then((venue: VenueModel) => venue.id)
                .then((venueId: number) => this.inventory.getManyWithManufacturerNamesByVenueId(obj, venueId))
                .then((resp) => {
                    resp.forEach(item => {
                        result[idsOrders[item.id]] = item;
                    });
                    this.selectedEquipment = result;
                    this.syncSelectedEquipmentAndIds();
                });
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

}
