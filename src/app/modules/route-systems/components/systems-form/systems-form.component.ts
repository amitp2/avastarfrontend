import { Component } from '@angular/core';
import { FormHandler } from '../../../../classes/form-handler';
import { EquipmentSystemModel } from '../../../../models/equipment-system-model';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EquipmentSystemResourceService } from '../../../../services/resources/equipment-system-resource.service';
import { ObjectTransformer } from '../../../../interfaces/object-transformer';
import { UtilsService } from '../../../../services/utils.service';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';
import { VenueModel } from '../../../../models/venue-model';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';

@Component({
    selector: 'app-systems-form',
    templateUrl: './systems-form.component.html',
    styleUrls: ['./systems-form.component.styl']
})
export class SystemsFormComponent extends FormHandler<EquipmentSystemModel> {

    get transformer(): ObjectTransformer {
        return {
            name: { to: 'name' },
            description: { to: 'description' },
            inventoryIds: { to: 'inventoryIds' }
        };
    }

    constructor(
        private activatedRoute: ActivatedRoute,
        private equipmentSystem: EquipmentSystemResourceService,
        private utils: UtilsService,
        private store: Store<AppState>,
        private router: Router
    ) {
        super();
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    initializeForm(): FormGroup {
        return new FormGroup({
            name: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            description: new FormControl(null, [Validators.required, Validators.maxLength(255)]),
            inventoryIds: new FormControl([])
        });
    }

    @AsyncMethod({
        taskName: 'fetch_equipment_system'
    })
    get(id: number): Promise<EquipmentSystemModel> {
        return this.equipmentSystem.get(id);
    }

    @AsyncMethod({
        taskName: 'add_equipment_system',
        success: AppSuccess.ADD_EQUIPMENT_SYSTEM,
        error: AppError.ADD_EQUIPMENT_SYSTEM
    })
    add(model: EquipmentSystemModel): Promise<any> {
        return this.utils
            .toPromise(this.store.select('currentVenue'))
            .then((venue: VenueModel) => venue.id)
            .then(venueId => this.equipmentSystem.addForVenue(model, venueId))
            .then((added: EquipmentSystemModel) => this.router.navigateByUrl(`/u/systems/${added.id}`));
    }

    @AsyncMethod({
        taskName: 'edit_equipment_system',
        success: AppSuccess.EDIT_EQUIPMENT_SYSTEM,
        error: AppError.EDIT_EQUIPMENT_SYSTEM
    })
    edit(id: number, model: EquipmentSystemModel): Promise<any> {
        return this.equipmentSystem.edit(
            id,
            model
        ).then((updated: EquipmentSystemModel) => this.router.navigateByUrl(`/u/systems/${updated.id}`));
    }

    serialize(model: EquipmentSystemModel): any {
        return this.utils.transformInto(model, this.transformer);
    }

    deserialize(formValue: any): EquipmentSystemModel {
        return new EquipmentSystemModel(this.utils.transformInto(formValue, this.transformer));
    }

    goBack() {
        this.router.navigateByUrl(`/u/systems`);
    }


}
