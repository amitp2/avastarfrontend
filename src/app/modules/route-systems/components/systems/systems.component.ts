import { Component, OnInit, ViewChild } from '@angular/core';
import { SystemStatus } from '../../../../enums/system-status.enum';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { PagingTableService } from '../../../../services/paging-table.service';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { VenueModel } from '../../../../models/venue-model';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';
import { UtilsService } from '../../../../services/utils.service';
import { EquipmentSystemResourceService } from '../../../../services/resources/equipment-system-resource.service';
import { EquipmentSystemModel } from '../../../../models/equipment-system-model';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { AppError } from '../../../../enums/app-error.enum';
import { UserRole } from '../../../../enums/user-role.enum';
import { PromisedStoreService } from '../../../../services/promised-store.service';
import { AuthService } from '../../../../services/auth.service';

@Component({
    selector: 'app-systems',
    templateUrl: './systems.component.html',
    styleUrls: ['./systems.component.styl'],
    providers: [PagingTableService]
})
export class SystemsComponent implements OnInit {

    @ViewChild('table') table: PagingTableComponent;
    status = SystemStatus;


    columns: PagingTableHeaderColumn[] = [
        {isSortable: true, title: 'System Name', key: 'name'},
        {isSortable: true, title: 'Description', key: 'description'},
        {isSortable: false, title: 'Actions', key: 'actions'}
    ];

    actionsAllow = [UserRole.SUPER_ADMIN, UserRole.ADMIN, UserRole.TECH_TEAM_LEAD, UserRole.TECH_TEAM];

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return this.utils
                .toPromise(this.store.select('currentVenue').filter((venue: VenueModel) => venue.id ? true : false))
                .then((venue: VenueModel) => venue.id)
                .then((venueId: number) => this.equipmentSystemResource.queryByVenueId(
                    this.utils.tableToHttpParams(params),
                    venueId
                ));
        }).bind(this);
    }

    constructor(private store: Store<AppState>,
                private utils: UtilsService,
                private equipmentSystemResource: EquipmentSystemResourceService,
                private auth: AuthService) {
    }

    ngOnInit() {
        this.checkRoles();
    }

    async checkRoles() {
        const user = await this.auth.currentUser();
        if (!this.utils.userHasAnyRole(user, this.actionsAllow)) {
            this.columns[2].title = '';
        }
    }

    @AsyncMethod({
        taskName: 'delete_equipment_system',
        error: AppError.DELETE_EQUIPMENT_SYSTEM
    })
    delete(model: EquipmentSystemModel) {
        return this.equipmentSystemResource
            .delete(model.id)
            .then(() => this.table.refresh());
    }

}
