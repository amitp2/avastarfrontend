import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SystemsComponent } from './components/systems/systems.component';
import { SystemsFormComponent } from './components/systems-form/systems-form.component';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { InventorySelectComponent } from './components/form-controls/inventory-select/inventory-select.component';
import { DragulaModule } from 'ng2-dragula';
import { SelectModule } from '../select/select.module';
import { TableResizeModule } from '../table-resize/table-resize.module';

const routes: Routes = [
    { path: '', component: SystemsComponent, pathMatch: 'exact' },
    { path: ':id', component: SystemsFormComponent }
];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        DragulaModule,
        ReactiveFormsModule,
        TableResizeModule,
        RouterModule.forChild(routes),
        FormsModule,
        PagingTableModule,
        SelectModule
    ],
    declarations: [
        SystemsComponent,
        SystemsFormComponent,
        InventorySelectComponent
    ]
})
export class RouteSystemsModule {
}
