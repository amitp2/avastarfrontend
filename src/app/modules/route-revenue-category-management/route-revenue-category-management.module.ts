import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageRevenueCategoryFormComponent } from './components/manage-revenue-category-forn/manage-revenue-category-forn.component';
import { ManageRevenueCategoriesComponent } from './components/manage-revenue-categories/manage-revenue-categories.component';
import { RouterModule, Routes } from '@angular/router';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { UserRole } from '../../enums/user-role.enum';
import { AllowOnlyService } from '../../services/guards/allow-only.service';
import { SelectModule } from '../select/select.module';
import { TableResizeModule } from '../table-resize/table-resize.module';

const routes: Routes = [
    {
        path: '',
        component: ManageRevenueCategoriesComponent,
        canActivate: [AllowOnlyService],
        data: {
            roles: [UserRole.SUPER_ADMIN]
        },
        children: [
            { path: ':id', component: ManageRevenueCategoryFormComponent }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes),
        PagingTableModule,
        ReactiveFormsModule,
        SelectModule,
        TableResizeModule
    ],
    declarations: [ManageRevenueCategoryFormComponent, ManageRevenueCategoriesComponent]
})
export class RouteRevenueCategoryManagementModule {
}
