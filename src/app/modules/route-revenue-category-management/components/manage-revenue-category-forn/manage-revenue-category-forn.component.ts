import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { FormMode } from '../../../../enums/form-mode.enum';
import { UtilsService } from '../../../../services/utils.service';
import { AuthService } from '../../../../services/auth.service';
import { RevenueCategoryModel } from '../../../../models/revenue-category-model';
import { AppError } from '../../../../enums/app-error.enum';
import { RevenueCategoryResourceService } from '../../../../services/resources/revenue-category-resource.service';
import { Observable } from 'rxjs/Observable';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { ActivatedRoute, Router } from '@angular/router';
import { FormHandler } from '../../../../classes/form-handler';
import { SelectFetchType } from '../../../select/types/select-fetch-type';

@Component({
  selector: 'app-manage-revenue-category-forn',
  templateUrl: './manage-revenue-category-forn.component.html',
  styleUrls: ['./manage-revenue-category-forn.component.styl']
})
export class ManageRevenueCategoryFormComponent extends FormHandler<RevenueCategoryModel> {
    SelectFetchType = SelectFetchType;

    constructor(
        private activatedRoute: ActivatedRoute,
        private revenueCategory: RevenueCategoryResourceService,
        private authService: AuthService,
        private router: Router,
        private utils: UtilsService,
        formBuilder: FormBuilder
    ) {
        super(formBuilder);
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    initializeForm(): FormGroup {
        return this.formBuilder.group({
            name: [null, [Validators.required]],
            description: [null, Validators.maxLength(500)],
            code: [null, [Validators.required, Validators.maxLength(100)]]
            // calculateCode: [null]
        });
    }

    @AsyncMethod({
        taskName: 'get_revenue_category'
    })
    get(id: number): Promise<RevenueCategoryModel> {
        return this.revenueCategory.get(id);
    }

    @AsyncMethod({
        taskName: 'add_revenue_category',
        success: AppSuccess.REVENUE_CATEGORY_ADD,
        error: AppError.REVENUE_CATEGORY_ADD_GLOBAL
    })
    add(model: RevenueCategoryModel): Promise<any> {
        return this.revenueCategory.add(model)
            .then(() => this.router.navigateByUrl(`/u/manage-revenue-category?fresh=${Date.now()}`));
    }

    @AsyncMethod({
        taskName: 'update_revenue_category',
        success: AppSuccess.REVENUE_CATEGORY_EDIT,
        error: AppError.REVENUE_CATEGORY_ADD_GLOBAL
    })
    edit(id: number, model: RevenueCategoryModel): Promise<any> {
        return this.revenueCategory
            .edit(id, model)
            .then(() => {
                this.router.navigateByUrl(`/u/manage-revenue-category?fresh=${Date.now()}`);
            });
    }

    serialize(model: RevenueCategoryModel): any {
        return this.utils.transformInto(model, {
            name: { to: 'name' },
            description: { to: 'description' },
            code: { to: 'code' },
            calculateCode: { to: 'calculateCode' }
        });
    }

    deserialize(formValue: any): RevenueCategoryModel {
        return new RevenueCategoryModel(this.utils.transformInto(formValue, {
            name: { to: 'name' },
            description: { to: 'description' },
            code: { to: 'code' },
            calculateCode: { to: 'calculateCode' }
        }));
    }
}
