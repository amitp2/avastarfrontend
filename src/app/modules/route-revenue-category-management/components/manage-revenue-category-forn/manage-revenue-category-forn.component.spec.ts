import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageRevenueCategoryFormComponent } from './manage-revenue-category-forn.component';

describe('ManageRevenueCategoryFormComponent', () => {
  let component: ManageRevenueCategoryFormComponent;
  let fixture: ComponentFixture<ManageRevenueCategoryFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ManageRevenueCategoryFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageRevenueCategoryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
