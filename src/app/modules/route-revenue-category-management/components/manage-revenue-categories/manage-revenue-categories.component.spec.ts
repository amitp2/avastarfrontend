import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageRevenueCategoriesComponent } from './manage-revenue-categories.component';

describe('ManageRevenueCategoriesComponent', () => {
  let component: ManageRevenueCategoriesComponent;
  let fixture: ComponentFixture<ManageRevenueCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageRevenueCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageRevenueCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
