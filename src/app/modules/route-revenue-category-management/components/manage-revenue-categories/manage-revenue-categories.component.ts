import { Component, OnInit, ViewChild } from '@angular/core';
import { TableHandler } from '../../../../classes/table-handler';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { UtilsService } from '../../../../services/utils.service';
import { AuthService } from '../../../../services/auth.service';
import { RevenueCategoryResourceService } from '../../../../services/resources/revenue-category-resource.service';
import { ActivatedRoute } from '@angular/router';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppError } from '../../../../enums/app-error.enum';

@Component({
    selector: 'app-manage-revenue-categories',
    templateUrl: './manage-revenue-categories.component.html',
    styleUrls: ['./manage-revenue-categories.component.styl']
})
export class ManageRevenueCategoriesComponent extends TableHandler {

    @ViewChild('table') table: PagingTableComponent;

    constructor(
        private revenueCategory: RevenueCategoryResourceService,
        private authService: AuthService,
        private utils: UtilsService,
        activatedRoute: ActivatedRoute
    ) {
        super(activatedRoute);
    }

    getTable(): PagingTableComponent {
        return this.table;
    }

    loadFunction = (params) => {
        return this.revenueCategory.query(this.utils.tableToHttpParams(params));
    }

    @AsyncMethod({
        taskName: 'delete_revenue_category',
        error: AppError.REVENUE_CATEGORY_DELETE
    })
    async delete(item) {
        await this.revenueCategory.delete(item.id);
        this.table.delete((x) => +x.id === +item.id);
    }
}
