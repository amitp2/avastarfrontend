import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EquipmentsComponent } from './components/equipments/equipments.component';
import { EquipmentFormComponent } from './components/equipment-form/equipment-form.component';
import { SharedModule } from '../shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EquipmentResourceService } from '../../services/resources/equipment-resource.service';
import { PriceHistoryComponent } from './components/price-history/price-history.component';
import { SelectModule } from '../select/select.module';
import { EquipmentPagingNextComponent } from './components/equipment-paging-next/equipment-paging-next.component';
import { EquipmentPagingPrevComponent } from './components/equipment-paging-prev/equipment-paging-prev.component';
import { TableResizeModule } from '../table-resize/table-resize.module';
import { MaskedInputModule } from '../masked-input/masked-input.module';

const routes: Routes = [
    {
        path: '',
        component: EquipmentsComponent
    },
    {
        path: ':id',
        component: EquipmentFormComponent
    }
];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        SelectModule,
        ReactiveFormsModule,
        PagingTableModule,
        MaskedInputModule,
        TableResizeModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        EquipmentsComponent,
        EquipmentFormComponent,
        PriceHistoryComponent,
        EquipmentPagingNextComponent,
        EquipmentPagingPrevComponent
    ],
    providers: [
        EquipmentResourceService
    ]
})
export class RouteEquipmentModule { }
