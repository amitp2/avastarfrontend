import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AppState } from '../../../../store/store';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { InventoryResourceService } from '../../../../services/resources/inventory-resource.service';
import { EquipmentResourceService } from '../../../../services/resources/equipment-resource.service';
import { EquipmentPagingCurrent } from '../../../../store/equipment-paging/equipment-paging.actions';
import { UtilsService } from '../../../../services/utils.service';

@Component({
    selector: 'app-equipment-paging-prev',
    templateUrl: './equipment-paging-prev.component.html',
    styleUrls: ['./equipment-paging-prev.component.styl']
})
export class EquipmentPagingPrevComponent implements OnInit, OnDestroy {
    private sub: Subscription;
    private prev: any;

    @Output('prev')
    public onPrev: EventEmitter<any> = new EventEmitter();

    constructor(
        private equipment: EquipmentResourceService,
        private store: Store<AppState>,
        private route: ActivatedRoute,
        private router: Router,
        private utils: UtilsService
    ) {
    }

    async ngOnInit() {
        this.sub = this.route.params.subscribe(
            () => this.loadPrev()
        );
        this.loadPrev();
    }

    ngOnDestroy() {
        if (this.sub) {
            this.sub.unsubscribe();
        }
    }

    private async loadPrev() {
        this.prev = await this.equipment.getPrevForPaging();
    }

    private goToPrev() {
        if (this.prev) {
            this.onPrev.emit(this.handle.bind(this));
        }
    }

    public async handle() {
        if (this.prev) {
            this.store.dispatch(new EquipmentPagingCurrent(this.prev));
            this.router.navigateByUrl(`/u/equipment`);
            await this.utils.resolveAfter(100);
            if (this.prev.model.equipmentServiceId) {
                this.router.navigateByUrl(`/u/equipment/${this.prev.model.equipmentServiceId}`);
            } else {
                this.router.navigate(['/u/equipment/add'], {
                    queryParams: {
                        categoryId: this.prev.model.categoryId,
                        subCategoryId: this.prev.model.subCategoryId,
                        subCategoryItemId: this.prev.model.subCategoryItemId
                    }
                });
            }
        }
    }
}
