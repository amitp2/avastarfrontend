import { Component, Input } from '@angular/core';
import { EquipmentActualPrice } from '../../../../models/equipment-model';

@Component({
    selector: 'app-equipment-price-history',
    templateUrl: './price-history.component.html',
    styleUrls: ['./price-history.component.styl']
})
export class PriceHistoryComponent {

    public _prices: EquipmentActualPrice[] = [];

    @Input()
    set prices(prices: EquipmentActualPrice[]) {
        this._prices = prices;
    }

    get prices() {
        return this._prices;
    }
}
