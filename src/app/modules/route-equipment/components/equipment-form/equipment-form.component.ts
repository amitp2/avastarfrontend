import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FormHandler } from '../../../../classes/form-handler';
import { FormMode } from '../../../../enums/form-mode.enum';
import { EquipmentResourceService } from '../../../../services/resources/equipment-resource.service';
import { AuthService } from '../../../../services/auth.service';
import { UtilsService } from '../../../../services/utils.service';
import { EquipmentServiceType, EquipmentCrossRentalType, EquipmentProductInformation, EquipmentProductInformationNames } from '../../../../enums/equipment';
import { DropdownItem } from '../../../../interfaces/dropdown-item';
import { InventoryCategoryResourceService } from '../../../../services/resources/inventory-category-resource.service';
import { AsyncValue } from '../../../../classes/async-value';
import { DynamicDropdownComponent } from '../../../shared/components/form-controls/dynamic-dropdown/dynamic-dropdown.component';
import 'rxjs/add/operator/distinctUntilChanged';
import { Subscription } from 'rxjs/Subscription';
import { RevenueCategoryResourceService } from '../../../../services/resources/revenue-category-resource.service';
import { InventoryResourceService } from '../../../../services/resources/inventory-resource.service';
import { CurrentPropertyService } from '../../../../services/current-property.service';
import { EquipmentModel } from '../../../../models/equipment-model';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AsyncTasksService } from '../../../../services/async-tasks.service';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError, AppErrorDefaultMessages } from '../../../../enums/app-error.enum';
import { ToastrService } from 'ngx-toastr';
import { FormConstructorService } from '../../../../services/form-constructor/form-constructor.service';
import { EquipmentFormModel } from '../../../../form-models/equipment-form.model';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { InventorySubCategoryFetchFactory } from '../../../select/fetches/factories/inventory-sub-category.fetch.factory';
import { InventoryCategoryModel } from '../../../../models/inventory-category-model';
import { InventorySubCategoryItemFetchFactory } from '../../../select/fetches/factories/inventory-sub-category-item.fetch.factory';
import { currencyStringToNumber } from '../../../../helpers/functions/currency-string-to-number';

@Component({
    selector: 'app-equipment-form',
    templateUrl: './equipment-form.component.html',
    styleUrls: ['./equipment-form.component.styl']
})
export class EquipmentFormComponent extends FormHandler<EquipmentModel> implements OnInit, OnDestroy, AfterViewInit {
    public equipmentType = EquipmentServiceType;
    public equipmentProductInformation = EquipmentProductInformation;
    public equipmentProductInformationNames = EquipmentProductInformationNames;
    public rentalType = EquipmentCrossRentalType;
    public isServiceType = false;
    public oldPrices: any[] = [];
    private subscriptions: Subscription[] = [];

    @ViewChild('inventorySubCategory')
    private inventorySubCategoryDropdown: DynamicDropdownComponent;
    @ViewChild('inventory')
    private inventoryDropdown: DynamicDropdownComponent;
    @ViewChild('inventoryCategory')
    private inventoryCategoryDropdown: DynamicDropdownComponent;

    get isRentable() {
        return this.form.get('rentable').value;
    }

    SelectFetchType = SelectFetchType;

    private categoryParams: any;

    constructor(
        private activatedRoute: ActivatedRoute,
        private equipmentService: EquipmentResourceService,
        private authService: AuthService,
        private router: Router,
        private utils: UtilsService,
        private inventoryCategory: InventoryCategoryResourceService,
        private revenueCategory: RevenueCategoryResourceService,
        private inventory: InventoryResourceService,
        private currentProperty: CurrentPropertyService,
        private asyncTask: AsyncTasksService,
        private toastrService: ToastrService,
        formBuilder: FormBuilder,
        private formConstructor: FormConstructorService,
        private utilsService: UtilsService,
        public inventorySubCategoryFetchFactory: InventorySubCategoryFetchFactory,
        public inventorySubCategoryItemFetchFactory: InventorySubCategoryItemFetchFactory
    ) {
        super(<any>formConstructor);

    }

    initializeForm(): FormGroup {
        return (<FormConstructorService>(<any>this.formBuilder)).build(EquipmentFormModel);
    }

    @AsyncMethod({
        taskName: 'get_equipment'
    })
    get(id: number): Promise<any> {
        return this.utilsService.resolveAfter(1000)
            .then(() => {
                return this.equipmentService.get(id)
                    .then((equipment) => {
                        this.isServiceType = equipment.type === EquipmentServiceType.Service;
                        this.oldPrices = equipment.oldPrices || [];
                        this.form.get('type').disable();
                        this.form.get('categoryId').disable();
                        this.form.get('subCategoryId').disable();
                        this.form.get('subCategoryItemId').disable();
                        this.categoryParams = {
                            categoryId: equipment.categoryId,
                            subCategoryId: equipment.subCategoryId,
                            subCategoryItemId: equipment.subCategoryItemId
                        };
                        return equipment;
                    });
            });
    }

    @AsyncMethod({
        taskName: 'add_equipment'
    })
    add(model: any): Promise<any> {
        return this.currentProperty.currentVenueAsync()
            .then((venue) => this.equipmentService.addByVenueId(model, venue.id))
            .then((equipment) => {
                this.asyncTask.taskSuccess(
                    'add_equipment',
                    model.type === EquipmentServiceType.Equipment ? AppSuccess.EQUIPMENT_ADD : AppSuccess.SERVICE_ADD
                );
                return equipment;
            })
            .then((equipment) => {
                return this.router.navigateByUrl(`/u/equipment`);
                // this.router.navigateByUrl(`/u/equipment/${equipment.id}`).then(() => {
                //     this.form.get('type').disable();
                //     this.form.get('type').markAsPristine();
                // });
            })
            .catch((error) => {
                this.asyncTask.taskError(
                    'add_equipment',
                    model.type === EquipmentServiceType.Equipment ? AppError.EQUIPMENT_ADD : AppError.SERVICE_ADD
                );
            });
    }

    submit() {
        const isRentable = this.form.get('rentable').value;
        if (isRentable) {
            const fromDate = new Date(this.form.get('priceFromDate').value);
            const toDate = new Date(this.form.get('priceToDate').value);
            if (fromDate > toDate) {
                this.toastrService.error(AppErrorDefaultMessages[AppError.ENDING_DATE], 'Error', {
                    positionClass: 'toast-bottom-right',
                    tapToDismiss: false
                });
                return;
            }
        }
        return super.submit();
    }

    @AsyncMethod({
        taskName: 'edit_equipment'
    })
    edit(id: number, model: any): Promise<any> {
        const type = this.form.get('type').value;
        const data: any = {
            ...model,
            type
        };
        return this.equipmentService.edit(id, data)
            .then(() => this.get(id))
            .then(() => {
                this.asyncTask.taskSuccess(
                    'edit_equipment',
                    type === EquipmentServiceType.Equipment ? AppSuccess.EQUIPMENT_EDIT : AppSuccess.SERVICE_EDIT
                );
                this.form.markAsPristine();
            })
            .catch(() => {
                this.asyncTask.taskError(
                    'edit_equipment',
                    type === EquipmentServiceType.Equipment ? AppError.EQUIPMENT_EDIT : AppError.SERVICE_EDIT
                );
            });
    }

    deserialize(formValue: any): EquipmentModel {
        const converted = this.utils.transformInto(formValue, {
            name: { to: 'name' },
            type: { to: 'type' },
            clientDescription: { to: 'clientDescription' },
            workflowDescription: { to: 'workflowDescription' },
            revenueCategoryId: { to: 'revenueCategoryId' },
            crossRentalType: { to: 'crossRentalType' },
            setupTime: { to: 'setupTime' },
            price: { to: 'price' },
            priceFromDate: { to: 'priceFromDate' },
            priceToDate: { to: 'priceToDate' },
            subCategoryItemId: { to: 'subCategoryItemId' },
            categoryId: { to: 'categoryId' },
            subCategoryId: { to: 'subCategoryId' },
            costCategoryId: { to: 'costCategoryId' },
            rentable: { to: 'rentable' },
            thirdParty: { to: 'thirdParty' },
            excludeFromReport: { to: 'excludeFromReport' },
            equipmentProductInformation: { to: 'equipmentProductInformation' }
        });

        converted.price = currencyStringToNumber(converted.price);

        if (this.categoryParams) {
            converted.categoryId = this.categoryParams.categoryId;
            converted.subCategoryId = this.categoryParams.subCategoryId;
            converted.subCategoryItemId = this.categoryParams.subCategoryItemId;
        }

        return new EquipmentModel(converted);
    }

    serialize(model: EquipmentModel) {
        const transformers: any = {
            name: { to: 'name' },
            type: { to: 'type' },
            clientDescription: { to: 'clientDescription' },
            workflowDescription: { to: 'workflowDescription' },
            revenueCategoryId: { to: 'revenueCategoryId' },
            crossRentalType: { to: 'crossRentalType' },
            excludeFromReport: { to: 'excludeFromReport' },
            setupTime: { to: 'setupTime' },
            price: { to: 'price' },
            priceFromDate: { to: 'priceFromDate' },
            priceToDate: { to: 'priceToDate' },
            equipmentProductInformation: { to: 'equipmentProductInformation' },
            subCategoryItemId: { to: 'subCategoryItemId' },
            categoryId: { to: 'categoryId' },
            subCategoryId: { to: 'subCategoryId' },
            costCategoryId: { to: 'costCategoryId' },
            rentable: { to: 'rentable' },
            thirdParty: { to: 'thirdParty' },
        };

        const serialized = this.utils.transformInto(model, transformers);

        return serialized;
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    ngOnInit() {
        super.ngOnInit();

        this.subscriptions.push(
            this.form.get('type')
                .valueChanges
                .subscribe((val: EquipmentServiceType) => {
                    this.isServiceType = val === EquipmentServiceType.Service;
                    this.form.get('type').markAsPristine();
                })
        );

        this.checkAddRequest();
    }

    ngAfterViewInit() {
        this.formMode().subscribe(mode => {
            if (mode === FormMode.Add && !this.activatedRoute.snapshot.queryParamMap.get('copy')) {
                if (this.inventoryCategoryDropdown) {
                    this.inventoryCategoryDropdown.refresh();
                }
            }
        });
    }

    async checkAddRequest() {
        const currentEntityId = this.activatedRoute.snapshot.paramMap.get('id');
        if (currentEntityId !== 'add') {
            return;
        }
        const categoryId = +this.activatedRoute.snapshot.queryParamMap.get('categoryId');
        const subCategoryId = +this.activatedRoute.snapshot.queryParamMap.get('subCategoryId');
        const subCategoryItemId = +this.activatedRoute.snapshot.queryParamMap.get('subCategoryItemId');

        this.categoryParams = {
            categoryId,
            subCategoryId,
            subCategoryItemId
        };

        this.form.get('categoryId').setValue(+categoryId);

        await this.utilsService.resolveAfter(150);

        this.form.get('subCategoryId').setValue(+subCategoryId);

        await this.utilsService.resolveAfter(150);

        this.form.get('subCategoryItemId').setValue(+subCategoryItemId);

        await this.utilsService.resolveAfter(150);

        this.form.get('categoryId').disable();
        this.form.get('subCategoryId').disable();
        this.form.get('subCategoryItemId').disable();
    }

    checkCopyRequest() {
        const copy = this.activatedRoute.snapshot.queryParamMap.get('copy');
        if (!copy) {
            return;
        }
        this.form.disable();
        this.equipmentService.get(+copy).then((x: EquipmentModel) => {
            this.form.enable();
            this.form.patchValue({
                name: x.name,
                type: x.type,
                clientDescription: x.clientDescription,
                workflowDescription: x.workflowDescription,
                revenueCategoryId: x.revenueCategoryId,
                crossRentalType: x.crossRentalType,
                setupTime: x.setupTime,
                subCategoryItemId: x.subCategoryItemId,
                categoryId: x.categoryId,
                subCategoryId: x.subCategoryId,
                costCategoryId: x.costCategoryId,
                rentable: x.rentable,
                equipmentProductInformation: x.equipmentProductInformation
            });
            this.form.markAsPristine();
            this.inventoryCategoryDropdown.refresh();
        });
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        this.subscriptions.forEach(x => x.unsubscribe());
    }

    async saveAndNew() {
        await this.submit();
        await this.router.navigateByUrl('/u/equipment/add');
        location.reload();
    }


    cancel() {
        if (this.form.dirty) {
            if (confirm('Clicking Cancel will result in losing all changes. Click “OK” to go to the equipment List, or “Cancel” to go back to your changes')) {
                this.router.navigateByUrl('/u/equipment');
            }
            return;
        }
        this.router.navigateByUrl('/u/equipment');
    }

    async onNext(next: Function) {
        if (this.form.pristine) {
            return next();
        }
        if (confirm('Save any changes and move to the next asset?')) {
            if (!this.form.valid) {
                return alert('Please fill all required fields before saving.');
            }
            await this.edit(this.currentEntityId, this.deserialize(this.form.value));
            next();
        }
    }

    async onPrev(prev: Function) {
        if (this.form.pristine) {
            return prev();
        }
        if (confirm('Save any changes and move to the next asset')) {
            if (!this.form.valid) {
                return alert('Please fill all required fields before saving.');
            }
            await this.edit(this.currentEntityId, this.deserialize(this.form.value));
            prev();
        }
    }
}
