import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { EquipmentResourceService } from '../../../../services/resources/equipment-resource.service';
import { AuthService } from '../../../../services/auth.service';
import { UtilsService } from '../../../../services/utils.service';
import { ActivatedRoute } from '@angular/router';
import { TableHandler } from '../../../../classes/table-handler';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { VenueModel } from '../../../../models/venue-model';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { CurrentPropertyService } from '../../../../services/current-property.service';
import { AsyncValue } from '../../../../classes/async-value';
import { DropdownItem } from '../../../../interfaces/dropdown-item';
import { InventoryCategoryResourceService } from '../../../../services/resources/inventory-category-resource.service';
import { InventoryCategoryModel } from '../../../../models/inventory-category-model';
import { DynamicDropdownComponent } from '../../../shared/components/form-controls/dynamic-dropdown/dynamic-dropdown.component';
import { EquipmentServiceType } from '../../../../enums/equipment';
import { EquipmentModel } from '../../../../models/equipment-model';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppError } from '../../../../enums/app-error.enum';
import { UserRole } from '../../../../enums/user-role.enum';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { InventorySubCategoryFetchFactory } from '../../../select/fetches/factories/inventory-sub-category.fetch.factory';
import { PagingTableService } from '../../../../services/paging-table.service';
import { InventoryPagingCurrent } from '../../../../store/inventory-paging/inventory-paging.actions';
import { InventoryModel } from '../../../../models/inventory-model';
import { EquipmentPagingCurrent } from '../../../../store/equipment-paging/equipment-paging.actions';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';
import { EquipmentServiceItemStatus } from '../../../../enums/equipment-service-item-status.enum';
import { EquipmentSubcategoryItem } from '../../../../models/equipment-subcategory-item.model';

@Component({
    selector: 'app-equipments',
    templateUrl: './equipments.component.html',
    styleUrls: ['./equipments.component.styl'],
    providers: [PagingTableService]
})
export class EquipmentsComponent extends TableHandler implements OnDestroy {
    @ViewChild('table')
    public table: PagingTableComponent;

    public statusType = EquipmentServiceItemStatus;
    public filterForm: FormGroup;

    public columns: PagingTableHeaderColumn[] = [
        { title: 'Equipment/Service Item', key: 'subCategoryItem', isSortable: false },
        { title: 'Category', key: 'sucatery', isSortable: false },
        { title: 'Sub Category', key: 'subCategory', isSortable: false },
    ];

    UserRole = UserRole;
    actionsAllow = [UserRole.SUPER_ADMIN, UserRole.ADMIN, UserRole.TECH_TEAM_LEAD];

    private subscriptions: Subscription[] = [];

    SelectFetchType = SelectFetchType;
    private latestParams: any;

    constructor(
        private utils: UtilsService,
        private formBuilder: FormBuilder,
        activatedRoute: ActivatedRoute,
        private equipmentService: EquipmentResourceService,
        private currentVenue: CurrentPropertyService,
        public subCategoryFetchFactory: InventorySubCategoryFetchFactory,
        private store: Store<AppState>
    ) {
        super(activatedRoute);

        this.buildFilterForm();
    }

    private buildFilterForm() {
        this.filterForm = this.formBuilder.group({
            status: EquipmentServiceItemStatus.All,
            search: ''
        });

        this.subscriptions.push(
            this.filterForm
                .valueChanges
                .debounceTime(350)
                .distinctUntilChanged()
                .subscribe(() => {
                    this.table.refresh();
                })
        );
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        if (this.subscriptions) {
            this.subscriptions.forEach(x => x.unsubscribe());
        }
    }

    getTable(): PagingTableComponent {
        return this.table;
    }

    @AsyncMethod({
        taskName: 'equipment_delete',
        error: AppError.DELETE_EQUIPMENT
    })
    delete(equipment: EquipmentSubcategoryItem) {
        return this.equipmentService.delete(equipment.equipmentServiceId).then(() => {
            this.table.delete((x) => +x.id === +equipment.equipmentServiceId);
        });
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return this.currentVenue.currentVenueAsync()
                .then((venue: VenueModel) => {
                    const queryParams = this.utils.tableToHttpParams(params);

                    queryParams.otherParams = { ...this.filterForm.value };

                    if (!queryParams.otherParams.search) {
                        delete queryParams.otherParams.search;
                    }

                    this.latestParams = queryParams;

                    return this.equipmentService.queryItemsByVenueId(queryParams, venue.id);
                });
        }).bind(this);
    }

    edit($event, item: EquipmentSubcategoryItem, index: number) {
        const current = {
            page: (this.latestParams.page * this.latestParams.size + index) as number,
            model: item
        };

        this.store.dispatch(new EquipmentPagingCurrent(current));
    }

    queryParamsForModel(model: EquipmentSubcategoryItem) {
        if (model.equipmentServiceId) {
            return {};
        }
        return {
            categoryId: model.categoryId,
            subCategoryId: model.subCategoryId,
            subCategoryItemId: model.subCategoryItemId
        };
    }

    editUrlForModel(model: EquipmentSubcategoryItem) {
        if (model.equipmentServiceId) {
            return `/u/equipment/${model.equipmentServiceId}`;
        }
        return '/u/equipment/add';
    }
}

// @Component({
//     selector: 'app-equipments',
//     templateUrl: './equipments.component.html',
//     styleUrls: ['./equipments.component.styl'],
//     providers: [PagingTableService]
// })
// export class EquipmentsComponent extends TableHandler implements OnDestroy {
//     @ViewChild('table')
//     public table: PagingTableComponent;

//     public equipmentType = EquipmentServiceType;
//     public filterForm: FormGroup;

//     public columns: PagingTableHeaderColumn[] = [
//         { title: 'Type', key: 'type', isSortable: false },
//         { title: 'Category', key: 'subCategoryItem.subCategory.category.name', isSortable: false },
//         { title: 'Sub Category', key: 'subCategoryItem.subCategory.name', isSortable: false },
//         { title: 'Items', key: 'subCategoryItem.name', isSortable: false },
//         { title: 'Price', key: 'price', isSortable: false },
//         // { title: 'Start Date', key: 'startDate', isSortable: false },
//         // { title: 'End Date', key: 'endDate', isSortable: false }
//     ];

//     UserRole = UserRole;
//     actionsAllow = [UserRole.SUPER_ADMIN, UserRole.ADMIN, UserRole.TECH_TEAM_LEAD];

//     private subscriptions: Subscription[] = [];

//     SelectFetchType = SelectFetchType;
//     private latestParams: any;

//     constructor(
//         private utils: UtilsService,
//         private authService: AuthService,
//         private formBuilder: FormBuilder,
//         activatedRoute: ActivatedRoute,
//         private equipmentService: EquipmentResourceService,
//         private currentVenue: CurrentPropertyService,
//         private inventoryCategory: InventoryCategoryResourceService,
//         public subCategoryFetchFactory: InventorySubCategoryFetchFactory,
//         private store: Store<AppState>
//     ) {
//         super(activatedRoute);

//         this.buildFilterForm();
//     }

//     private buildFilterForm() {
//         this.filterForm = this.formBuilder.group({
//             categoryId: '',
//             subCategoryId: '',
//             type: '',
//             search: ''
//         });

//         this.filterForm.get('subCategoryId').disable();

//         this.subscriptions.push(
//             this.filterForm
//                 .valueChanges
//                 .debounceTime(350)
//                 .distinctUntilChanged()
//                 .subscribe(() => {
//                     this.table.refresh();
//                 })
//         );

//         this.subscriptions.push(
//             this.filterForm.get('categoryId')
//                 .valueChanges
//                 .subscribe((newCategoryId) => {
//                     if (newCategoryId) {
//                         this.filterForm.get('subCategoryId').enable();
//                     } else {
//                         this.filterForm.get('subCategoryId').disable();
//                     }
//                 })
//         );
//     }

//     ngOnDestroy() {
//         super.ngOnDestroy();
//         if (this.subscriptions) {
//             this.subscriptions.forEach(x => x.unsubscribe());
//         }
//     }

//     getTable(): PagingTableComponent {
//         return this.table;
//     }


//     @AsyncMethod({
//         taskName: 'equipment_delete',
//         error: AppError.DELETE_EQUIPMENT
//     })
//     delete(equipment: EquipmentModel) {
//         return this.equipmentService.delete(equipment.id).then(() => {
//             this.table.delete((x) => +x.id === +equipment.id);
//         });
//     }

//     loadFunction(): any {
//         return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
//             return this.currentVenue.currentVenueAsync()
//                 .then((venue: VenueModel) => {
//                     const queryParams = this.utils.tableToHttpParams(params);

//                     queryParams.otherParams = { ...this.filterForm.value };

//                     if (!queryParams.otherParams.search) {
//                         delete queryParams.otherParams.search;
//                     }
//                     if (!queryParams.otherParams.type) {
//                         delete queryParams.otherParams.type;
//                     }
//                     if (!queryParams.otherParams.subCategoryId) {
//                         delete queryParams.otherParams.subCategoryId;
//                     }
//                     if (!queryParams.otherParams.categoryId) {
//                         delete queryParams.otherParams.categoryId;
//                         delete queryParams.otherParams.categoryId;
//                     }

//                     if (queryParams.otherParams.search) {
//                         queryParams.otherParams.clientDescription = queryParams.otherParams.search;
//                         delete queryParams.otherParams.search;
//                     }

//                     this.latestParams = queryParams;

//                     return this.equipmentService.queryByVenueId(queryParams, venue.id);
//                 });
//         }).bind(this);
//     }

//     edit($event, item: EquipmentModel, index: number) {
//         const current = {
//             page: (this.latestParams.page * this.latestParams.size + index) as number,
//             id: item.id
//         };

//         this.store.dispatch(new EquipmentPagingCurrent(current));
//     }

// }
