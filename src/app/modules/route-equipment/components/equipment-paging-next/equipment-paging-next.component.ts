import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AppState } from '../../../../store/store';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { InventoryResourceService } from '../../../../services/resources/inventory-resource.service';
import { EquipmentResourceService } from '../../../../services/resources/equipment-resource.service';
import { EquipmentPagingCurrent } from '../../../../store/equipment-paging/equipment-paging.actions';
import { UtilsService } from '../../../../services/utils.service';

@Component({
    selector: 'app-equipment-paging-next',
    templateUrl: './equipment-paging-next.component.html',
    styleUrls: ['./equipment-paging-next.component.styl']
})
export class EquipmentPagingNextComponent implements OnInit, OnDestroy {
    private sub: Subscription;
    private next: any;

    @Output('next')
    public onNext: EventEmitter<any> = new EventEmitter();

    constructor(
        private equipment: EquipmentResourceService,
        private store: Store<AppState>,
        private route: ActivatedRoute,
        private router: Router,
        private utils: UtilsService
    ) {
    }

    async ngOnInit() {
        this.sub = this.route.params.subscribe(
            () => this.loadNext()
        );
        this.loadNext();
    }

    ngOnDestroy() {
        if (this.sub) {
            this.sub.unsubscribe();
        }
    }

    private async loadNext() {
        this.next = await this.equipment.getNextForPaging();
    }

    private async handle() {
        if (this.next) {
            this.store.dispatch(new EquipmentPagingCurrent(this.next));
            this.router.navigateByUrl(`/u/equipment`);
            await this.utils.resolveAfter(100);
            if (this.next.model.equipmentServiceId) {
                this.router.navigateByUrl(`/u/equipment/${this.next.model.equipmentServiceId}`);
            } else {
                this.router.navigate(['/u/equipment/add'], {
                    queryParams: {
                        categoryId: this.next.model.categoryId,
                        subCategoryId: this.next.model.subCategoryId,
                        subCategoryItemId: this.next.model.subCategoryItemId
                    }
                });
            }
        }
    }

    private goToNext() {
        if (this.next) {
            this.onNext.emit(this.handle.bind(this));
        }
    }
}
