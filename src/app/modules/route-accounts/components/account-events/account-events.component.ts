import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { VenueEventResourceService } from '../../../../services/resources/venue-event-resource.service';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { PromisedStoreService } from '../../../../services/promised-store.service';
import { UtilsService } from '../../../../services/utils.service';
import { PagingTableService } from '../../../../services/paging-table.service';
import { SubscriberModel } from '../../../../models/subscriber-model';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';

@Component({
    selector: 'app-account-events',
    templateUrl: './account-events.component.html',
    styleUrls: ['./account-events.component.styl'],
    providers: [PagingTableService]
})
export class AccountEventsComponent implements OnInit {

    @ViewChild('eventsTable') eventsTable: PagingTableComponent;
    accountId: number;


    get addUrl(): string {
        return `/u/event-management/add/info`;
    }

    get addQueryParams(): object {
        return {
            accountId: this.accountId
        };
    }

    get accountUrl() {
        return `/u/accounts/${this.accountId}/`;
    }

    columns: PagingTableHeaderColumn[] = [
        {isSortable: true, title: 'Event Name', key: 'name'},
        {isSortable: true, title: 'Master Account Number', key: 'masterAccountNumber'},
        {isSortable: true, title: 'Start Date', key: 'eventStartDate'},
        {isSortable: true, title: 'End Date', key: 'eventEndDate'},
        {isSortable: true, title: 'Status', key: 'status'},
        {isSortable: false, title: 'Actions', key: 'actions'}
    ];

    constructor(private activatedRoute: ActivatedRoute,
                private events: VenueEventResourceService,
                private promisedStore: PromisedStoreService,
                private utils: UtilsService) {
        this.accountId = +this.activatedRoute.parent.snapshot.params['id'];
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return this.promisedStore.currentVenueId().then((venueId: number) => {
                return this.events.queryByAccountId(this.utils.tableToHttpParams(params), this.accountId, venueId);
                // return this.events.queryByVenueId(this.utils.tableToHttpParams(params), venueId);
            });
        }).bind(this);
    }

    delete(item: SubscriberModel) {
        this.events
            .delete(item.id)
            .then(() => {
                this.eventsTable.delete(tableItem => tableItem.id === item.id);
            });
    }

    editUrl(item) {
        return `/u/event-management/${item.id}/info`;
    }

    ngOnInit() {
    }

}
