import { Component, OnDestroy } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { StateModel } from '../../../../models/state-model';
import { CountryModel } from '../../../../models/country-model';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { SubscriberLogoUploaderService } from '../../../route-subscribers/services/subscriber-logo-uploader.service';
import { SubscriberStatus } from '../../../../enums/subscriber-status.enum';
import { UploadType } from '../../../../enums/upload-type.enum';
import { Subscription } from 'rxjs/Subscription';
import { AppError } from '../../../../enums/app-error.enum';
import { FormMode } from '../../../../enums/form-mode.enum';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { ActivatedRoute, Router } from '@angular/router';
import { VenueClientType } from '../../../../enums/venue-client-type.enum';
import { VenueClientStatus } from '../../../../enums/venue-client-status.enum';
import { VenueClientResourceService } from '../../../../services/resources/venue-client-resource.service';
import { VenueClientModel } from '../../../../models/venue-client-model';
import { PromisedStoreService } from '../../../../services/promised-store.service';
import { FunctionServiceChargeCalculationType } from '../../../../models/function-model';
import { AccountLogoUploaderService } from '../../services/account-logo-uploader.service';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { UtilsService } from '../../../../services/utils.service';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';
import { countryByCode, stateByCode, userLocation } from '../../../../store/selectors';
import { GeolocationDataModel } from '../../../../models/geolocation-data.model';
import { UserRole } from '../../../../enums/user-role.enum';
import { VenueClientContactResourceService } from '../../../../services/resources/venue-client-contact-resource.service';
import { VenueClientContactModel } from '../../../../models/venue-client-contact-model';
import { getBackToUrl, percentFormControl } from '../../../../helpers/helper-functions';
import { addQueryParamToUrl } from '../../../../helpers/functions/add-query-param-to-url';

@Component({
    selector: 'app-accounts-info',
    templateUrl: './accounts-info.component.html',
    styleUrls: ['./accounts-info.component.styl'],
    providers: [SubscriberLogoUploaderService]
})
export class AccountsInfoComponent implements OnDestroy {

    id: any;

    form: FormGroup;
    accountType = VenueClientType;
    status = VenueClientStatus;

    uploader: any;
    mode: FormMode;

    modeObs: BehaviorSubject<FormMode>;
    modeObsSubscription: Subscription;
    initialisedObs: BehaviorSubject<boolean>;
    initialisedItems: number;
    countries: CountryModel[];
    states: StateModel[];
    functionServiceChargeCalculationType = FunctionServiceChargeCalculationType;

    submitting = false;

    cancelLink: string;

    SelectFetchType = SelectFetchType;
    private primaryContact: VenueClientContactModel;

    constructor(private venueClientResource: VenueClientResourceService,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private logoUploader: AccountLogoUploaderService,
                private utilsService: UtilsService,
                private store: Store<AppState>,
                private promisedStore: PromisedStoreService,
                private clientContactService: VenueClientContactResourceService) {

        // this.uploader = logoUploader;
        this.uploader = logoUploader;
        this.initialisedItems = 0;
        this.modeObs = new BehaviorSubject<FormMode>(FormMode.Add);
        this.initialisedObs = new BehaviorSubject<boolean>(false);

        this.form = new FormGroup({
            name: new FormControl(null, [Validators.required]),
            accountType: new FormControl(VenueClientType.Client),
            status: new FormControl(true),
            address: new FormControl(null, [Validators.required]),
            address2: new FormControl(null),
            city: new FormControl(null, [Validators.required]),
            stateId: new FormControl(),
            postCode: new FormControl(null, [Validators.required, Validators.maxLength(10)]),
            countryId: new FormControl(null, [Validators.required]),
            branch: new FormControl(),
            agreement: new FormControl(false),
            logoUrl: new FormControl(),
            standardDiscountRate: percentFormControl(),
            discountRateForSetupDays: percentFormControl(),
            alternativeServiceCharge: new FormControl(),
            alternativeServiceChargePercentage: percentFormControl(),
            taxExempt: new FormControl(),
            serviceChargeCalculation: new FormControl(),
            primaryContact: new FormGroup({
                firstName: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
                lastName: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
                title: new FormControl(null, [Validators.maxLength(100)]),
              //email: new FormControl(null, [Validators.required,Validators.email, Validators.maxLength(100)]),
               email: new FormControl(null),
                mobile: new FormControl(null, [Validators.maxLength(30)]),
                phone: new FormControl(null),
                notes: new FormControl(null, [Validators.maxLength(50)]),
                fax: new FormControl(null, [Validators.maxLength(30)])
            })
        });

        this.activatedRoute.parent.params.subscribe((params: any) => {
            this.id = params.id;
            if (this.id !== 'add') {
                this.mode = FormMode.Edit;
            } else {
                this.mode = FormMode.Add;
                this.uploader.uploadType = UploadType.Add;
            }
            this.modeObs.next(this.mode);
        });

        this.modeObsSubscription = this.modeObs
            .combineLatest(this.initialisedObs.filter(inited => inited))
            .map(combined => combined[0])
            .subscribe((mode: FormMode) => {
                if (mode === FormMode.Edit) {
                    this.load();
                    return;
                }
                this.populateForm();
            });

        this.form.get('alternativeServiceCharge')
            .valueChanges
            .subscribe(val => {
                if (!val) {
                    this.form
                        .get('alternativeServiceChargePercentage')
                        .setValue('');
                }
            });

        this.form.get('stateId').disable();

        this.form.get('countryId').valueChanges.subscribe((val: number) => {
            if (+val === 233) {
                this.form.get('stateId').enable();
            } else {
                this.form.get('stateId').disable();
            }
        });

        this.setupCancelLink();
    }

    countriesAreLoaded(countries: CountryModel[]) {
        this.initialisedItems++;
        this.countries = countries;
        if (this.initialisedItems === 2) {
            this.initialisedObs.next(true);
        }
    }

    statesAreLoaded(states: StateModel[]) {
        this.initialisedItems++;
        this.states = states;
        if (this.initialisedItems === 2) {
            this.initialisedObs.next(true);
        }
    }

    isBackToUrlSet(): boolean {
        return !!this.activatedRoute.snapshot.queryParams.backToUrl;
    }

    getBackToUrl() {
        return decodeURIComponent(this.activatedRoute.snapshot.queryParams.backToUrl);
    }

    setupCancelLink() {
        this.cancelLink = `/u/accounts`;
        if (this.isBackToUrlSet()) {
            this.cancelLink = this.getBackToUrl();
        }
    }

    cancel() {
        this.router.navigateByUrl(this.cancelLink);
    }

    private async populateForm() {
        const location: GeolocationDataModel = await this.utilsService.toPromise(this.store.select(userLocation()));
        if (location.countryCode) {
            const country: CountryModel = await this.utilsService.toPromise(this.store.select(countryByCode(location.countryCode)));
            this.form.get('countryId').setValue(country.id);
        }
        // if (location.regionCode) {
        //     const state: StateModel = await this.utilsService.toPromise(this.store.select(stateByCode(location.regionCode)));
        //     if (state) {
        //         this.form.get('stateId').setValue(state.id);
        //     }
        // }
        // if (location.city) {
        //     this.form.get('city').setValue(location.city);
        // }
        // if (location.zipCode) {
        //     this.form.get('postCode').setValue(location.zipCode);
        // }
        this.form.markAsPristine();
    }

    submit() {
        if (this.submitting) {
            return;
        }

        this.submitting = true;

        const newModel = this.deserialize(this.form.value);
        if (this.mode === FormMode.Add) {
            this.add(newModel)
                .then(added => {
                    if (this.isBackToUrlSet()) {
                        const url = addQueryParamToUrl(getBackToUrl(this.activatedRoute), 'addedAccount', `${added.id}`);
                        this.router.navigateByUrl(url);
                    } else {
                        this.router.navigateByUrl(`/u/accounts/${added.id}/account-info`);
                    }
                })
                .then(() => this.submitting = false);
            // .then(() => this.router.navigateByUrl('/u/accounts?fresh=' + Date.now()));
            return;
        }
        this.update(+this.id, newModel)
            .then(() => this.submitting = false)
            .then(() => this.router.navigateByUrl('/u/accounts?fresh=' + Date.now()));
    }

    deserialize(formValue: any): VenueClientModel {
        const modelVal: any = {
            ...formValue
        };
        modelVal.status = formValue.status ? SubscriberStatus.Active : SubscriberStatus.Locked;
        modelVal.masterAgreementAgreed = formValue.agreement;

        const model = new VenueClientModel(modelVal);

        model['primaryContact'] = formValue.primaryContact;

        return model;
    }

    serialize(model: VenueClientModel): any {
        const serialized = {
            name: model.name,
            accountType: model.accountType,
            status: model.status === VenueClientStatus.Active ? true : false,
            address: model.address,
            address2: model.address2,
            city: model.city,
            stateId: model.stateId,
            postCode: model.postCode,
            countryId: model.countryId,
            alternativeServiceChargePercentage: model.alternativeServiceChargePercentage,
            branch: model.branch,
            phone: model.phone,
            fax: model.fax,
            website: model.website,
            agreement: model.masterAgreementAgreed,
            logoUrl: model.logoUrl,
            serviceChargeCalculation: model.serviceChargeCalculation
        };
        serialized['standardDiscountRate'] = model.standardDiscountRate;
        serialized['discountRateForSetupDays'] = model.discountRateForSetupDays;
        serialized['alternativeServiceCharge'] = model.alternativeServiceCharge;
        serialized['taxExempt'] = model.taxExempt;
        return serialized;
    }

    @AsyncMethod({
        taskName: 'account_loaded'
    })
    async load() {
        const model = await this.venueClientResource.get(+this.id);

        this.uploader.uploadType = UploadType.Update;
        this.uploader.oldUrl = model.logoUrl;

        const modelValue = this.serialize(model);

        try {
            const primaryContact = await this.clientContactService.queryByClientId({
                page: 0,
                size: 100,
                sortFields: ['id'],
                sortAsc: true
            }, model.id);
            modelValue.primaryContact = {
                ...primaryContact.items[0]
            };
            this.primaryContact = primaryContact.items[0];
        } catch (ex) {

        }

        this.form.patchValue(modelValue);

        await this.utilsService.resolveAfter(300);

        this.form.markAsPristine();

        return model;
    }

    @AsyncMethod({
        taskName: 'account_add',
        success: AppSuccess.ACCOUNT_ADD,
        error: AppError.ACCOUNT_ADD
    })
    add(model: VenueClientModel) {
        const primaryContact = model['primaryContact'];

        delete model['primaryContact'];

        return this.promisedStore.currentVenueId()
            .then((venueId: number) => {
                model.venueId = venueId;
                return this.venueClientResource.add(model);
            }).then(account => {
                const contactModel = new VenueClientContactModel({
                    ...primaryContact
                });
                return this.clientContactService.addForClient(contactModel, +account.id).then(x => account);
            });
    }

    @AsyncMethod({
        taskName: 'account_update',
        success: AppSuccess.ACCOUNT_UPDATE,
        error: AppError.ACCOUNT_UPDATE
    })
    update(id: number, model: VenueClientModel) {
        const primaryContact = model['primaryContact'];

        delete model['primaryContact'];

        return this.promisedStore.currentVenueId()
            .then((venueId: number) => {
                model.venueId = venueId;
                return this.venueClientResource.edit(id, model);
            }).then(account => {
                const contactModel = new VenueClientContactModel({
                    ...primaryContact
                });
                return this.clientContactService.edit(this.primaryContact.id, contactModel).then(x => account);
            });
    }

    ngOnDestroy() {
        this.modeObsSubscription.unsubscribe();
    }

}
