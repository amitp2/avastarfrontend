import { Component, OnInit, ViewChild } from '@angular/core';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { SubscriberModel } from '../../../../models/subscriber-model';
import { UtilsService } from '../../../../services/utils.service';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { UserRole } from '../../../../enums/user-role.enum';
import { ActivatedRoute } from '@angular/router';
import { TableHandler } from '../../../../classes/table-handler';
import {VenueClientResourceService} from '../../../../services/resources/venue-client-resource.service';
import {PromisedStoreService} from '../../../../services/promised-store.service';
import { PagingTableService } from '../../../../services/paging-table.service';
import { VenueClientModel } from '../../../../models/venue-client-model';
import { VenueClientContactResourceService } from '../../../../services/resources/venue-client-contact-resource.service';
import { VenueClientContactModel } from '../../../../models/venue-client-contact-model';
import { SubscriberContactModel } from '../../../../models/subscriber-contact-model';

@Component({
    selector: 'app-accounts',
    templateUrl: './accounts.component.html',
    styleUrls: ['./accounts.component.styl'],
    providers: [PagingTableService]
})
export class AccountsComponent extends TableHandler {

    columns: PagingTableHeaderColumn[] = [
        {isSortable: true, title: 'Company/Organization', key: 'name'},
        {isSortable: true, title: 'Account Type', key: 'accountType'},
        {isSortable: false, title: 'Primary Contact', key: 'website'},
        {isSortable: true, title: 'Status', key: 'status'},
        {isSortable: false, title: 'Actions', key: 'actions'}
    ];

    userRole = UserRole;

    @ViewChild('accountsTable') accountsTable: PagingTableComponent;


    constructor(private venueClient: VenueClientResourceService,
                private contacts: VenueClientContactResourceService,
                private utilsService: UtilsService,
                private promisedStore: PromisedStoreService,
                activatedRoute: ActivatedRoute) {
        super(activatedRoute);
    }

    getTable(): PagingTableComponent {
        return this.accountsTable;
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return this.promisedStore.currentVenueId().then((venueId: number) => {
                return this.venueClient
                    .queryBuVenueId(this.utilsService.tableToHttpParams(params), venueId)
                    .then((resp) => this.contacts
                        .getManyByVenueIds(resp.items.map((v: VenueClientModel) => v.id))
                        .then((conts: VenueClientContactModel[]) => {

                            const contactsByAccountId = {};

                            conts.forEach((c: VenueClientContactModel) => contactsByAccountId[c.accountId] = c);

                            resp.items = resp.items.map((m: VenueClientModel) => {
                                const currentContact = contactsByAccountId[m.id];
                                if (currentContact) {
                                    m['contactFirstName'] = currentContact.firstName;
                                    if (currentContact.lastName) {
                                        m['contactFirstName'] += ' ' + currentContact.lastName;
                                    }
                                } else {
                                    m['contactFirstName'] = '';
                                }
                                return m;
                            });

                            return resp;
                        })
                    );
            });
        }).bind(this);
    }

    delete(item: SubscriberModel) {
        this.venueClient
            .delete(item.id)
            .then(() => {
                this.accountsTable.delete(tableItem => tableItem.id === item.id);
            });
    }

}
