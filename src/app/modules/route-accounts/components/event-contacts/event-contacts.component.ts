import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { EventContactResourceService } from '../../../../services/resources/event-contact-resource.service';
import { UtilsService } from '../../../../services/utils.service';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppError } from '../../../../enums/app-error.enum';
import { AppSuccess } from '../../../../enums/app-success.enum';

@Component({
    selector: 'app-event-contacts',
    templateUrl: './event-contacts.component.html',
    styleUrls: ['./event-contacts.component.styl']
})
export class EventContactsComponent implements OnInit {
    @Input() eventId: number;
    @ViewChild('table') table: PagingTableComponent;

    columns: PagingTableHeaderColumn[] = [
        { isSortable: true, title: 'Name', key: 'firstName' },
        { isSortable: true, title: 'Direct Phone', key: 'phone' },
        { isSortable: true, title: 'Email', key: 'email' },
        { isSortable: true, title: 'Department', key: 'department'},
        { isSortable: false, title: 'Actions', key: 'actions' }
    ]

    constructor(
        private contactApi: EventContactResourceService,
        private utils: UtilsService
    ) { }

    load = (params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
        return this.contactApi.queryByEventId(this.utils.tableToHttpParams(params), this.eventId);
    }


    ngOnInit() {
    }

    @AsyncMethod({
        taskName: 'delete_event_contact',
        error: AppError.EVENT_CONTACT_DELETE,
        success: AppSuccess.EVENT_CONTACT_DELETE
    })
    delete(item: any) {
        return this.contactApi.delete(item.id)
            .then(() => this.table.refresh());
    }

}
