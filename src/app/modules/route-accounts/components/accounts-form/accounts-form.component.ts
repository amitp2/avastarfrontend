import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormMode } from '../../../../enums/form-mode.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { backToUrlExists, getBackToUrl } from '../../../../helpers/helper-functions';

@Component({
    selector: 'app-accounts-form',
    templateUrl: './accounts-form.component.html',
    styleUrls: ['./accounts-form.component.styl']
})
export class AccountsFormComponent implements OnInit, OnDestroy {

    id: string;
    mode: FormMode;
    formMode = FormMode;
    backTo: any;

    constructor(private activatedRoute: ActivatedRoute,
                public router: Router) {
        this.backTo = this.activatedRoute.snapshot.queryParams.backTo;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: any) => {
            this.id = params.id;
            if (this.id === 'add') {
                this.mode = FormMode.Add;
                return;
            }
            this.mode = FormMode.Edit;
        });
    }

    ngOnDestroy() {

    }

    goBack() {


        if (backToUrlExists(this.activatedRoute)) {
            this.router.navigateByUrl(getBackToUrl(this.activatedRoute));
            return;
        }

        switch (this.backTo) {
            case 'events':
                return this.router.navigateByUrl('/u/event-management');
            default:
                return this.router.navigateByUrl('/u/accounts');
        }

    }

    linkPrefix(): string {
        return '/u/accounts/' + this.id + '/';
    }

}
