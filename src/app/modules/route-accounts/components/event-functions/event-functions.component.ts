import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { PagingTableService } from '../../../../services/paging-table.service';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { FunctionResourceService } from '../../../../services/resources/function-resource.service';
import { UtilsService } from '../../../../services/utils.service';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';

@Component({
    selector: 'app-event-functions',
    templateUrl: './event-functions.component.html',
    styleUrls: ['./event-functions.component.styl'],
    providers: [PagingTableService]
})
export class EventFunctionsComponent implements OnInit {

    @Input()
    eventId: number;

    @ViewChild('table')
    table: PagingTableComponent;

    columns: PagingTableHeaderColumn[] = [
        {isSortable: true, title: 'Location', key: 'functionSpace.name'},
        {isSortable: true, title: 'Name', key: 'functionName'},
        {isSortable: true, title: 'type', key: 'type'},
        {isSortable: true, title: 'Start Date', key: 'startDate'},
        {isSortable: true, title: 'End Date', key: 'endDate'},
        {isSortable: false, title: 'Actions', key: 'actions'}
    ];


    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return this.functions.queryByEventId(
                this.utils.tableToHttpParams(params),
                this.eventId
            ).then((resp) => {
                return resp;
            });
        }).bind(this);
    }

    constructor(private functions: FunctionResourceService,
                private utils: UtilsService) {
    }

    ngOnInit() {
        // console.log(this.eventId, 'eventt');
    }

    delete(item: any) {
        this.functions.delete(item.id)
            .then(() => this.table.refresh());
    }

}
