import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { EventContactResourceService } from '../../../../services/resources/event-contact-resource.service';
import { FormMode } from '../../../../enums/form-mode.enum';
import { VenueEventContact } from '../../../../models/venue-event-model';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppError } from '../../../../enums/app-error.enum';
import { AppSuccess } from '../../../../enums/app-success.enum';

@Component({
    selector: 'app-event-contact-form',
    templateUrl: './event-contact-form.component.html',
    styleUrls: ['./event-contact-form.component.styl']
})
export class EventContactFormComponent implements OnInit {
    isLoading = false;
    popupVisible = false;
    form: FormGroup;
    formMode = FormMode.Add;
    editableContactId: number;

    @Input() eventId: number;
    @Output() changed: EventEmitter<VenueEventContact> = new EventEmitter();

    constructor(
        private fb: FormBuilder,
        private contactApi: EventContactResourceService
    ) {
        this.buildForm();
    }

    buildForm(): any {
        this.form = this.fb.group({
            firstName: [null, [Validators.required, Validators.maxLength(50)]],
            lastName: [null, [Validators.required, Validators.maxLength(50)]],
            title: [null, [Validators.maxLength(100)]],
            email: [null, [Validators.required, Validators.email, Validators.maxLength(100)]],
            mobile: [null, [Validators.maxLength(100)]],
            phone: [null, [Validators.required, Validators.maxLength(30)]],
            notes: [null, [Validators.maxLength(400)]],
            fax: [null, [Validators.maxLength(30)]],
            department: [null, [Validators.maxLength(250)]]
        });
    }

    ngOnInit() {
    }

    close() {
        this.popupVisible = false;
        this.formMode = FormMode.Add;
        this.form.reset();
    }

    open() {
        this.formMode = FormMode.Add;
        this.popupVisible = true;
    }

    edit(model: VenueEventContact) {
        this.editableContactId = model.id;
        this.formMode = FormMode.Edit;
        this.form.reset(model.toJson());
        this.form.markAsPristine();
        this.popupVisible = true;
    }

    submit(e: Event) {
        if (e) {
            e.preventDefault();
            e.stopPropagation();
        }
        if (this.form.invalid) {
            return;
        }
        if (this.formMode === FormMode.Edit && this.editableContactId) {
            this.update(this.editableContactId, VenueEventContact.fromJson(this.form.value));
        } else {
            this.add(VenueEventContact.fromJson(this.form.value));
        }
    }

    @AsyncMethod({
        taskName: 'edit_event_contact',
        error: AppError.EVENT_CONTACT_EDIT,
        success: AppSuccess.EVENT_CONTACT_EDIT
    })
    private update(id: number, model: VenueEventContact) {
        return this.contactApi.edit(id, model)
            .then(contact => this.changed.emit(contact))
            .then(contact => this.close());
    }

    @AsyncMethod({
        taskName: 'add_event_contact',
        error: AppError.EVENT_CONTACT_ADD,
        success: AppSuccess.EVENT_CONTACT_ADD
    })
    private add(model: VenueEventContact) {
        return this.contactApi.addByEventId(model, this.eventId)
            .then(contact => this.changed.emit(contact))
            .then(contact => this.close());
    }

}
