import { Component } from '@angular/core';
import { ResourceQueryResponse } from '../../../../interfaces/resource-query-response';
import { ActivatedRoute, Router } from '@angular/router';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AsyncTasksService } from '../../../../services/async-tasks.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserRole } from '../../../../enums/user-role.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { VenueClientContactResourceService } from '../../../../services/resources/venue-client-contact-resource.service';
import { VenueClientContactModel } from '../../../../models/venue-client-contact-model';
import { VenueClientModel } from '../../../../models/venue-client-model';
import { VenueClientResourceService } from '../../../../services/resources/venue-client-resource.service';

@Component({
    selector: 'app-account-contact-person',
    templateUrl: './account-contact-person.component.html',
    styleUrls: ['./account-contact-person.component.styl']
})
export class AccountContactPersonComponent {

    id: any;
    edit: boolean;
    form: FormGroup;
    userRole = UserRole;
    contactId: number;
    currentOrganization: Promise<VenueClientModel>;

    constructor(
        private venueClientContact: VenueClientContactResourceService,
        private venueClient: VenueClientResourceService,
        private activatedRoute: ActivatedRoute,
        private asyncTasksService: AsyncTasksService,
        private router: Router
    ) {
        this.edit = false;

        this.currentOrganization = this.venueClient.get(this.activatedRoute.snapshot.parent.params['id']);

        const contactLoadTask = 'contact_load_task';
        this.asyncTasksService.taskStart(contactLoadTask);
        this.activatedRoute.parent.params.subscribe((params: any) => {
            const val = params.id;
            this.id = val;
            this.venueClientContact.queryByClientId({
                page: 0,
                size: 100,
                sortFields: ['id'],
                sortAsc: true
            }, +val)
                .then((resp: ResourceQueryResponse<VenueClientContactModel>) => {
                    this.asyncTasksService.taskSuccess(contactLoadTask);
                    this.edit = resp.total !== 0;
                    if (this.edit) {
                        this.contactId = resp.items[0].id;
                        const item: any = {
                            ...resp.items[0]
                        };
                        delete item.id;
                        this.form.patchValue(item);
                    }
                })
                .catch(() => {
                    // this.asyncTasksService.taskError(contactLoadTask, AppError.SUBSCRIBER_CONTACT_PERSON_LOAD);
                });
        }, () => {
            // this.asyncTasksService.taskError(contactLoadTask, AppError.SUBSCRIBER_CONTACT_PERSON_LOAD);
        });

        this.form = new FormGroup({
            firstName: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            lastName: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            title: new FormControl(null, [Validators.maxLength(100)]),
            email: new FormControl(null, [Validators.required, Validators.email, Validators.maxLength(100)]),
            mobile: new FormControl(null, [Validators.maxLength(30)]),
            phone: new FormControl(null, [Validators.required]),
            role: new FormControl(UserRole.NONE, []),
            notes: new FormControl(null, [Validators.maxLength(50)]),
            fax: new FormControl(null, [Validators.maxLength(30)]),
            department: new FormControl(null, [Validators.maxLength(250)])
        });
    }


    submit() {
        const contactPersonModifyTask = 'venue_client_contact_person';
        const model = new VenueClientContactModel({
            ...this.form.value,
            role: +this.form.value.role
        });

        if (this.edit) {
            this.asyncTasksService.taskStart(contactPersonModifyTask);
            this.venueClientContact
                .edit(this.contactId, model)
                .then(() => this.asyncTasksService.taskSuccess(contactPersonModifyTask, AppSuccess.VENUE_CONTACT_UPDATE))
                // .then(() => this.router.navigateByUrl('/u/accounts?fresh=' + Date.now()))
                .catch((err) => {
                    console.log(err);
                    this.asyncTasksService.taskError(contactPersonModifyTask, AppError.VENUE_CONTACT_UPDATE);
                });

        } else {
            this.asyncTasksService.taskStart(contactPersonModifyTask);
            this.venueClientContact
                .addForClient(model, +this.id)
                .then(() => this.asyncTasksService.taskSuccess(contactPersonModifyTask, AppSuccess.VENUE_CONTACT_UPDATE))
                // .then(() => this.router.navigateByUrl('/u/accounts?fresh=' + Date.now()))
                .catch(() => this.asyncTasksService.taskError(contactPersonModifyTask, AppError.VENUE_CONTACT_UPDATE));
        }
    }

}
