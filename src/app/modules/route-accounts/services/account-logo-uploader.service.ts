import { SubscriberLogoUploaderService } from '../../route-subscribers/services/subscriber-logo-uploader.service';
import { AppSuccess } from '../../../enums/app-success.enum';
import { Injectable } from '@angular/core';

@Injectable()
export class AccountLogoUploaderService extends SubscriberLogoUploaderService {

    uploadAppSuccess() {
        return AppSuccess.ACCOUNT_LOGO_UPLOAD;
    }

}
