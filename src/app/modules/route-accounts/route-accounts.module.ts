import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AccountsComponent } from './components/accounts/accounts.component';
import { AccountsFormComponent } from './components/accounts-form/accounts-form.component';
import { AccountsInfoComponent } from './components/accounts-info/accounts-info.component';
import { AccountEventsComponent } from './components/account-events/account-events.component';
import { AccountEventsFormComponent } from './components/account-events-form/account-events-form.component';
import { AccountContactPersonComponent } from './components/account-contact-person/account-contact-person.component';
import { EventFunctionsComponent } from './components/event-functions/event-functions.component';
import { EventContactsComponent } from './components/event-contacts/event-contacts.component';
import { EventContactFormComponent } from './components/event-contact-form/event-contact-form.component';
import { AccountLogoUploaderService } from './services/account-logo-uploader.service';
import { SelectModule } from '../select/select.module';
import { MaskedInputModule } from '../masked-input/masked-input.module';
import { TableResizeModule } from '../table-resize/table-resize.module';

const routes: Routes = [
    {
        path: '',
        component: AccountsComponent,
        children: [
            {
                path: ':id', component: AccountsFormComponent,
                children: [
                    {path: 'account-info', component: AccountsInfoComponent},
                    // {path: 'account-contact-person', component: AccountContactPersonComponent},
                    {path: 'account-events', component: AccountEventsComponent, pathMatch: 'full'},
                    // {path: 'account-events/:eventId', component: AccountEventsFormComponent}
                ]
            }
        ]
    }

];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        PagingTableModule,
        TableResizeModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
        SelectModule,
        MaskedInputModule
    ],
    declarations: [
        AccountsComponent,
        AccountsFormComponent,
        AccountsInfoComponent,
        AccountEventsComponent,
        AccountEventsFormComponent,
        AccountContactPersonComponent,
        EventFunctionsComponent,
        EventContactsComponent,
        EventContactFormComponent
    ],
    providers: [
        AccountLogoUploaderService
    ]
})
export class RouteAccountsModule {
}
