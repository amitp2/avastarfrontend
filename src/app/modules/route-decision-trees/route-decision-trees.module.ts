import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { DecisionTreeFormComponent } from './components/decision-tree-form/decision-tree-form.component';
import { DecisionTreesComponent } from './components/decision-trees/decision-trees.component';
import { Route, RouterModule } from '@angular/router';
import { SubscriberNamesComponent } from './components/subscriber-names/subscriber-names.component';
import { SelectModule } from '../select/select.module';
import { ReactiveFormsModule } from '@angular/forms';
import { TableResizeModule } from '../table-resize/table-resize.module';

const routes: Route[] = [
    {
        path: '',
        component: DecisionTreesComponent,
        children: [
            { path: ':id', component: DecisionTreeFormComponent }
        ]
    },
];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        PagingTableModule,
        SelectModule,
        ReactiveFormsModule,
        TableResizeModule,
        RouterModule.forChild(routes)
    ],
    declarations: [DecisionTreeFormComponent, DecisionTreesComponent, SubscriberNamesComponent]
})
export class RouteDecisionTreesModule {
}
