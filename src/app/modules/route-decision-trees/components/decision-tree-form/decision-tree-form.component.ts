import { Component } from '@angular/core';
import { FormHandler } from '../../../../classes/form-handler';
import { DecisionTreeModel, DecisionTreeType } from '../../../../models/decision-tree.model';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilsService } from '../../../../services/utils.service';
import { DecisionTreeResourceService } from '../../../../services/resources/decision-tree-resource.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { MultiSelectItem } from '../../../shared/components/form-controls/multi-select/multi-select.component';
import { SubscriberResourceService } from '../../../../services/resources/subscriber-resource.service';
import { AppError } from '../../../../enums/app-error.enum';
import { AppSuccess } from '../../../../enums/app-success.enum';

@Component({
    selector: 'app-decision-tree-form',
    templateUrl: './decision-tree-form.component.html',
    styleUrls: ['./decision-tree-form.component.styl']
})
export class DecisionTreeFormComponent extends FormHandler<DecisionTreeModel> {
    private DecisionTreeType: typeof DecisionTreeType = DecisionTreeType;
    private subscribers: MultiSelectItem[];

    constructor(
        private activatedRoute: ActivatedRoute,
        private treeApi: DecisionTreeResourceService,
        private authService: AuthService,
        private router: Router,
        private utils: UtilsService,
        private subscriberApi: SubscriberResourceService
    ) {
        super();

        this.loadSubscribers();
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    initializeForm(): FormGroup {
        return new FormGroup({
            type: new FormControl(DecisionTreeType.Private, [Validators.required]),
            subscriberIds: new FormControl()
        });
    }

    @AsyncMethod({
        taskName: 'get_decision_tree'
    })
    get(id: number): Promise<DecisionTreeModel> {
        return this.treeApi.get(id);
    }

    add(model: DecisionTreeModel): Promise<any> {
        return undefined;
    }

    @AsyncMethod({
        taskName: 'update_decision_tree',
        success: AppSuccess.UPDATE_DECISION_TREE,
        error: AppError.UPDATE_DECISION_TREE
    })
    edit(id: number, model: DecisionTreeModel): Promise<any> {
        return this.treeApi.edit(id, model)
            .then(() => {
                this.router.navigateByUrl(`/u/decision-trees?fresh=${Date.now()}`);
            });
    }

    serialize(model: DecisionTreeModel): any {
        return {
            type: model.type,
            subscriberIds: model.subscriberIds
        };
    }

    deserialize(formValue: any): DecisionTreeModel {
        return new DecisionTreeModel({
            type: formValue.type,
            subscriberIds: (formValue.subscriberIds || []).map(id => Number(id))
        });
    }

    async loadSubscribers() {
        const models = await this.subscriberApi.getAll();
        this.subscribers = models.map(sub => ({ id: sub.id, text: sub.name }));
    }
}
