import { Component, Input, OnInit } from '@angular/core';
import { SubscriberResourceService } from '../../../../services/resources/subscriber-resource.service';

@Component({
    selector: 'app-subscriber-names',
    templateUrl: './subscriber-names.component.html',
    styleUrls: ['./subscriber-names.component.styl']
})
export class SubscriberNamesComponent {
    private _subscriberIds: number[];
    private names = '-';

    @Input() set subscriberIds(value: number[]) {
        this._subscriberIds = value;
        if (Array.isArray(value) && value.length) {
            this.populate();
        } else {
            this.names = '';
        }
    }

    get subscriberIds() {
        return this._subscriberIds;
    }

    constructor(private api: SubscriberResourceService) {
    }

    async populate() {
        this.names = (await this.api.getNames(this.subscriberIds)).join(', ');
    }
}
