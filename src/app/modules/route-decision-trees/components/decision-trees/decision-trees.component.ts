import { Component, OnInit, ViewChild } from '@angular/core';
import { DecisionTreeResourceService } from '../../../../services/resources/decision-tree-resource.service';
import { UtilsService } from '../../../../services/utils.service';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { TableHandler } from '../../../../classes/table-handler';
import { ActivatedRoute } from '@angular/router';
import { PagingTableService } from '../../../../services/paging-table.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';

@Component({
    selector: 'app-decision-trees',
    templateUrl: './decision-trees.component.html',
    styleUrls: ['./decision-trees.component.styl'],
    providers: [
        PagingTableService
    ]
})
export class DecisionTreesComponent extends TableHandler {
    @ViewChild('table') table: PagingTableComponent;
    private isSyncing = false;

    columns: PagingTableHeaderColumn[] = [
        { isSortable: true, title: 'Name', key: 'name' },
        { isSortable: true, title: 'Tree ID', key: 'treeId' },
        { isSortable: true, title: 'Description', key: 'description' },
        { isSortable: true, title: 'Type', key: 'type' },
        { isSortable: false, title: 'Subscribers', key: 'subscriberIds' },
    ];

    constructor(
        private treeApi: DecisionTreeResourceService,
        private utils: UtilsService,
        activatedRoute: ActivatedRoute
    ) {
        super(activatedRoute);
    }

    getTable(): PagingTableComponent {
        return this.table;
    }

    loadFunction() {
        return params => {
            return this.treeApi.query(this.utils.tableToHttpParams(params));
        };
    }

    @AsyncMethod({
        taskName: 'sync_trees'
    })
    async sync() {
        this.isSyncing = true;
        try {
            await this.treeApi.sync();
            this.getTable().refresh();
        } finally {
            this.isSyncing = false;
        }
    }
}
