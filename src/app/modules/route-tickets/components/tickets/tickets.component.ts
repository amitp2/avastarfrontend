import { Component, ViewChild } from '@angular/core';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { FormGroup } from '@angular/forms';
import { CurrentPropertyService } from '../../../../services/current-property.service';
import { UtilsService } from '../../../../services/utils.service';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppError } from '../../../../enums/app-error.enum';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { InventoryResourceService } from '../../../../services/resources/inventory-resource.service';
import { TicketResourceService } from '../../../../services/resources/ticket-resource.service';
import { PromisedStoreService } from '../../../../services/promised-store.service';
import { TicketModel, TicketStatus } from '../../../../models/ticket-model';
import { FormConstructorService } from '../../../../services/form-constructor/form-constructor.service';
import { TicketsFilterFormModel } from '../../form-models/tickets-filter-form.model';
import { CommandConstructorService } from '../../../../services/command-constructor/command-constructor.service';
import { PagingTableService } from '../../../../services/paging-table.service';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { SelectComponent } from '../../../select/select/select.component';

@Component({
    selector: 'app-tickets',
    templateUrl: './tickets.component.html',
    styleUrls: ['./tickets.component.styl'],
    providers: [
        PagingTableService
    ]
})
export class TicketsComponent {

    @ViewChild('table') table: PagingTableComponent;
    @ViewChild('locationDropdown') locationDropdown: SelectComponent;

    columns: PagingTableHeaderColumn[] = [
        { isSortable: true, title: 'Ticket Title', key: 'title' },
        { isSortable: false, title: 'Update date', key: 'id' },
        { isSortable: false, title: 'Ticket <br> Number', key: 'name' },
        { isSortable: false, title: 'Inventory <br> System', key: 'inventory_system' },
        { isSortable: false, title: 'Location', key: 'location' },
        { isSortable: false, title: 'Submitted by', key: 'make' },
        { isSortable: false, title: 'Submitted to', key: 'make' },
        { isSortable: false, title: 'Status', key: 'make' },
        { isSortable: false, title: 'Actions', key: 'actions' }
    ];

    filterForm: FormGroup;
    status = TicketStatus;
    currentFilters: TicketsFilterFormModel;
    SelectFetchType: typeof SelectFetchType = SelectFetchType;

    constructor(
        private inventory: InventoryResourceService,
        private currentVenue: CurrentPropertyService,
        private utilsService: UtilsService,
        private utils: UtilsService,
        private ticket: TicketResourceService,
        private promisedStore: PromisedStoreService,
        private commandConstructor: CommandConstructorService
    ) {
        this.filterForm = FormConstructorService.build(TicketsFilterFormModel);
        this.currentFilters = new TicketsFilterFormModel();
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {

            const httpParams = this.utils.tableToHttpParams(params);

            if (this.currentFilters) {
                httpParams.otherParams = this.commandConstructor.build(this.currentFilters);
                console.log(this.currentFilters.locationId);
                if (this.currentFilters.locationId) {
                    if (this.currentFilters.locationId > 9999999) {
                        httpParams.otherParams.locationId = this.currentFilters.locationId % 9999999;
                        httpParams.otherParams.portable = true;
                    } else {
                        httpParams.otherParams.portable = false;
                    }
                }
            }

            return this.promisedStore.currentVenueId()
                .then((venueId: number) => this.ticket.queryByVenueId(httpParams, venueId));
        }).bind(this);
    }

    @AsyncMethod({
        taskName: 'delete_inventory',
        error: AppError.DELETE_TICKET
    })
    delete(ticket: TicketModel) {
        return this.ticket
            .delete(ticket.id)
            .then(() => {
                this.table.delete((u) => +u.id === +ticket.id);
            });
    }

    applyFilters() {
        this.currentFilters = FormConstructorService.extractModel(this.filterForm.value, TicketsFilterFormModel);
        this.table.refresh();
    }

    clearFilters() {
        this.currentFilters = null;
        this.filterForm.get('locationId').setValue(null);
        this.filterForm.reset({
            ticketStatus: TicketStatus.All
        });
        this.table.refresh();
    }

}
