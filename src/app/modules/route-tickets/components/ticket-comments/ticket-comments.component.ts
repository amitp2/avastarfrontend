import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-ticket-comments',
    templateUrl: './ticket-comments.component.html',
    styleUrls: ['./ticket-comments.component.styl']
})
export class TicketCommentsComponent implements OnInit {

    @Input() comments: any[];
    @Input() ticketId: string;

    constructor() {
    }

    ngOnInit() {
    }

}
