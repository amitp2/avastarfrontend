import { AfterViewInit, Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { AuthService } from '../../../../services/auth.service';
import { UtilsService } from '../../../../services/utils.service';
import { ActivatedRoute, Router } from '@angular/router';
import { VendorResourceService } from '../../../../services/resources/vendor-resource.service';
import { TicketResourceService } from '../../../../services/resources/ticket-resource.service';
import { PromisedStoreService } from '../../../../services/promised-store.service';
import { VendorContactResourceService } from '../../../../services/resources/vendor-contact-resource.service';
import { AbstractTicketForm, TicketFor } from '../../../../classes/abstract-ticket-form';
import { TicketAssignee, TicketModel } from '../../../../models/ticket-model';
import { InventorySubCategoryFetchFactory } from '../../../select/fetches/factories/inventory-sub-category.fetch.factory';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { InventoryFetchFactory } from '../../../select/fetches/factories/inventory.fetch.factory';
import { SystemFetchFactory } from '../../../select/fetches/factories/system.fetch.factory';
import { VendorContactFetchFactory } from '../../../select/fetches/factories/vendor-contact.fetch.factory';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { SelectComponent } from '../../../select/select/select.component';


@Component({
    selector: 'app-tickets-form',
    templateUrl: './tickets-form.component.html',
    styleUrls: ['./tickets-form.component.styl']
})
export class TicketsFormComponent extends AbstractTicketForm implements OnInit, AfterViewInit {
    @Output()
    ticketAdded: EventEmitter<TicketModel>;
    today: any;

    SelectFetchType = SelectFetchType;

    @ViewChild('inventory')
    private inventoryDropdown: SelectComponent;

    constructor(
        public route: ActivatedRoute,
        vendors: VendorResourceService,
        vendorContact: VendorContactResourceService,
        utilsService: UtilsService,
        authService: AuthService,
        public rout: Router,
        tickets: TicketResourceService,
        utils: UtilsService,
        promisedStore: PromisedStoreService,
        public inventorySubCategoryFetchFactory: InventorySubCategoryFetchFactory,
        public inventoryFetchFactory: InventoryFetchFactory,
        public systemFetchFactory: SystemFetchFactory,
        public vendorContactFetchFactory: VendorContactFetchFactory
    ) {
        super(
            route,
            vendors,
            vendorContact,
            utilsService,
            authService,
            rout,
            tickets,
            utils,
            promisedStore
        );
        this.ticketAdded = new EventEmitter<TicketModel>();
        this.today = new Date();
    }

    afterAdd(added: TicketModel): any {
        this.rout.navigateByUrl(`/u/tickets/${added.id}`);
    }

    getId(): number {
        return +this.route.snapshot.params.id;
    }

    getIdParam(): string {
        return 'id';
    }

    getVendorId() {
        const value = this.form.get('vendorId').value;
        if (value == null || value === undefined || value === 'null' || value === 'undefined' || value === '') {
            return undefined;
        }
        return value;
    }

    ngOnInit() {
        super.ngOnInit();
    }

    ngAfterViewInit() {
        this.checkCopyRequest();
    }

    @AsyncMethod({
        taskName: 'get_ticket'
    })
    async checkCopyRequest() {
        try {
            const copy = this.route.snapshot.queryParamMap.get('copy');
            if (!copy) {
                return;
            }

            const ticket = await this.tickets.get(+copy);
            const formValue: any = {};

            if (ticket.systemId) {
                formValue.ticketFor = TicketFor.System;
                if (ticket.extras.system) {
                    formValue.systemId = ticket.systemId;
                }
            } else if (ticket.inventoryId) {
                formValue.ticketFor = TicketFor.Inventory;
                if (ticket.extras.inventory) {
                    formValue.inventoryId = ticket.inventoryId;
                    formValue.inventoryCategoryId = +ticket.extras.inventory.categoryId;
                    formValue.inventorySubCategoryId = +ticket.extras.inventory.subCategoryId;
                }
            }

            this.form.patchValue(formValue);
            this.form.markAsPristine();
        } catch (e) {
            console.log(e)
        }
    }
}
