import { Component, Input, OnInit } from '@angular/core';
import { InventoryResourceService } from '../../../../services/resources/inventory-resource.service';
import { InventoryStorageType } from '../../../../enums/inventory-storage-type.enum';
import { EventSpaceResourceService } from '../../../../services/resources/event-space-resource.service';
import { StorageSpaceResourceService } from '../../../../services/resources/storage-space-resource.service';
import { FunctionSpaceResourceService } from '../../../../services/resources/function-space-resource.service';

@Component({
    selector: 'app-storage-location',
    templateUrl: './storage-location.component.html',
    styleUrls: ['./storage-location.component.styl']
})
export class StorageLocationComponent implements OnInit {

    private _inventoryId: number;
    private locationName: string;

    @Input() formField = false;

    @Input()
    set inventoryId(id: number) {
        this._inventoryId = id;
        this.loadLocation();
    }

    get inventoryId(): number {
        return this._inventoryId;
    }

    constructor(
        private inventoryApi: InventoryResourceService,
        private eventSpaceApi: FunctionSpaceResourceService,
        private storageSpaceApi: StorageSpaceResourceService
    ) {
    }

    ngOnInit() {
    }

    private async loadLocation() {
        if (!this.inventoryId) {
            this.locationName = null;
            return;
        }
        try {
            const inventory = await this.inventoryApi.get(this.inventoryId);
            if (!inventory.storageId) {
                this.locationName = null;
                return;
            }
            console.log(inventory);
            if (inventory.portable === false) {
                const eventSpace = await this.eventSpaceApi.get(inventory.storageId);
                this.locationName = eventSpace.name;
            } else {
                const storageSpace = await this.storageSpaceApi.get(inventory.storageId);
                this.locationName = storageSpace.name;
            }
        } catch (e) {
            this.locationName = null;
        }
    }
}
