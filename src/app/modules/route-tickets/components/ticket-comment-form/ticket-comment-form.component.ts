import { Component, OnInit } from '@angular/core';
import { FormHandler } from '../../../../classes/form-handler';
import { TicketCommentModel } from '../../../../models/ticket-comment-model';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TicketResourceService } from '../../../../services/resources/ticket-resource.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { UtilsService } from '../../../../services/utils.service';

@Component({
    selector: 'app-ticket-comment-form',
    templateUrl: './ticket-comment-form.component.html',
    styleUrls: ['./ticket-comment-form.component.styl']
})
export class TicketCommentFormComponent extends FormHandler<TicketCommentModel> {


    ticketId: number;

    get backLink(): string {
        return `/u/tickets/${this.ticketId}`;
    }

    constructor(
        private route: ActivatedRoute,
        private ticket: TicketResourceService,
        private router: Router,
        private utils: UtilsService
    ) {
        super();
        this.ticketId = +this.route.parent.snapshot.params.id;
    }

    entityId(): Observable<number> {
        return this.paramsId(this.route, 'commentId');
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.route, 'commentId');
    }

    initializeForm(): FormGroup {
        return new FormGroup({
            description: new FormControl(null, [Validators.required])
        });
    }

    get(id: number): Promise<TicketCommentModel> {
        return undefined;
    }

    @AsyncMethod({
        taskName: 'add_ticket_comment'
    })
    add(model: TicketCommentModel): Promise<any> {
        return this.ticket.addCommentByTicketId(model, this.ticketId)
            .then(() => this.router.navigateByUrl(`${this.backLink}?fresh=${Date.now()}`));
    }

    edit(id: number, model: TicketCommentModel): Promise<any> {
        return undefined;
    }

    serialize(model: TicketCommentModel): any {
        return this.utils.transformInto(model, {
            description: { to: 'description' }
        });
    }

    deserialize(formValue: any): TicketCommentModel {
        return new TicketCommentModel(this.utils.transformInto(formValue, {
            description: { to: 'description' }
        }));
    }

}
