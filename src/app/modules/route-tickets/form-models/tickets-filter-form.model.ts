import { TicketStatus, TicketStatusToApiMapper } from '../../../models/ticket-model';
import { FormField } from '../../../services/form-constructor/form-field';
import { DefaultValue } from '../../../services/form-constructor/default-value';
import { CommandField } from '../../../services/command-constructor/command-field.decorator';
import { CommandFieldMapper } from '../../../services/command-constructor/command-field-mapper.decorator';
import { dateFormatMapper } from '../../../mappers/date-format-mapper';

export class TicketsFilterFormModel {

    @CommandField()
    @FormField
    id: any;

    @CommandFieldMapper(TicketStatusToApiMapper)
    @CommandField()
    @DefaultValue(TicketStatus.All)
    @FormField
    ticketStatus: TicketStatus;

    @CommandField()
    @FormField
    search: string;

    @CommandField()
    @FormField
    systemName: string;

    @CommandField()
    @FormField
    tagNumber: string;

    @CommandField()
    @FormField
    barcode: string;

    @CommandFieldMapper(dateFormatMapper('YYYY/MM/DD'))
    @CommandField()
    @FormField
    from: any;

    @CommandFieldMapper(dateFormatMapper('YYYY/MM/DD'))
    @CommandField()
    @FormField
    to: any;

    @CommandField()
    @FormField
    inventoryName: string;

    @CommandField()
    @FormField
    locationId: number;

    @CommandField()
    portable: boolean;
}
