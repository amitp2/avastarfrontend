import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { SharedModule } from '../shared/shared.module';
import { TicketsComponent } from './components/tickets/tickets.component';
import { TicketsFormComponent } from './components/tickets-form/tickets-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TicketCommentsComponent } from './components/ticket-comments/ticket-comments.component';
import { TicketCommentFormComponent } from './components/ticket-comment-form/ticket-comment-form.component';
import { SelectModule } from '../select/select.module';
import { StorageLocationComponent } from './components/storage-location/storage-location.component';
import { TableResizeModule } from '../table-resize/table-resize.module';

const routes: Routes = [
    { path: '', component: TicketsComponent, pathMatch: 'full' },
    {
        path: ':id', component: TicketsFormComponent,
        children: [
            {path: 'comment/:commentId', component: TicketCommentFormComponent}
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        PagingTableModule,
        SharedModule,
        TableResizeModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        SelectModule
    ],
    declarations: [TicketsComponent, TicketsFormComponent, TicketCommentsComponent, TicketCommentFormComponent, StorageLocationComponent]
})
export class RouteTicketsModule {
}
