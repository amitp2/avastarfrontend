import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaskedInputComponent } from './masked-input/masked-input.component';

@NgModule({
    imports: [
        CommonModule
    ],
    exports: [MaskedInputComponent],
    declarations: [MaskedInputComponent]
})
export class MaskedInputModule {
}
