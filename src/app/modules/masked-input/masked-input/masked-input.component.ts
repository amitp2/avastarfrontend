import { AfterViewInit, Component, ElementRef, forwardRef, Input, NgZone, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { AbstractControlValueAccessor } from '../../../helpers/abstract-control-value-accessor';
import { GlobalsService } from '../../../services/globals.service';
import { MaskedInputType } from '../types/masked-input-type';
import { Subscription } from 'rxjs/Subscription';
import { ObservableSubscription } from '../../../decorators/observable-subscription.decorator';
import { UnsubscribePoint } from '../../../decorators/unsubscribe-point.decorator';

import 'rxjs/add/operator/filter';
import { currencyStringToNumber } from '../../../helpers/functions/currency-string-to-number';

@Component({
    selector: 'app-masked-input',
    templateUrl: './masked-input.component.html',
    styleUrls: ['./masked-input.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => MaskedInputComponent),
            multi: true
        }
    ]
})
export class MaskedInputComponent extends AbstractControlValueAccessor implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild('input')
    inputRef: ElementRef;

    @Input()
    type: MaskedInputType;

    @ObservableSubscription
    valuesFromOutsideSubs: Subscription;

    @Input()
    mask = '999-999-9999';

    @Input()
    options = {};

    get $(): any {
        return this.globals.globals().$;
    }

    get inputElement(): any {
        return this.inputRef.nativeElement;
    }

    get $inputElement(): any {
        return this.$(this.inputElement);
    }

    initialized: Promise<any>;

    constructor(private globals: GlobalsService,
                private zone: NgZone) {
        super();
    }

    ngOnInit() {
    }

    @UnsubscribePoint
    ngOnDestroy(): void {
        this.$inputElement.off('input');
    }

    private isComplete(): boolean {
        return this.$inputElement.inputmask('isComplete');
    }

    ngAfterViewInit(): void {
        this.initialized = new Promise<any>(resolve => {
            console.log(this.mask);
            this.$inputElement.inputmask(this.mask, {
                oncomplete: function () {
                    this.zone.run(() => {
                        this.changeOutside(this.inputElement.value);
                        this.onTouched();
                    });
                }.bind(this),
                onincomplete: function () {
                    this.zone.run(() => {
                        this.changeOutside('');
                        this.onTouched();
                    });
                }.bind(this),
                ...this.options
            });

            this.$inputElement.on('input', () => {
                this.zone.run(() => {
                    if (this.isComplete()) {
                        this.changeOutside(this.inputElement.value);
                    } else {
                        this.changeOutside('');
                    }
                    this.onTouched();
                });
            });
            resolve();
        });

        this.valuesFromOutsideSubs = this.valuesFromOutside
            .changes
            .filter(val => val !== null)
            .subscribe((val: any) => {
                this.initialized.then(() => {
                    this.inputElement.value = val;
                });
            });
    }

    changeOutside(obj: any): void {
        if (this.mask === 'currency') {
            obj = currencyStringToNumber(obj);
        }
        this.onChange(obj);
    }

}
