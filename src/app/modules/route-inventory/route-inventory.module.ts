import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { InventoriesComponent } from './components/inventories/inventories.component';
import { InventoriesFormAssetInformationComponent } from './components/inventories-form-asset-information/inventories-form-asset-information.component';
import { InventoriesFormServiceMaintenanceComponent } from './components/inventories-form-service-maintenance/inventories-form-service-maintenance.component';
import { InventoriesFormAssetStatusComponent } from './components/inventories-form-asset-status/inventories-form-asset-status.component';
import { InventoriesFormDocumentsComponent } from './components/inventories-form-documents/inventories-form-documents.component';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { InventoryCreationStepsComponent } from './components/layout/inventory-creation-steps/inventory-creation-steps.component';
import { InventorySpacesService } from './services/inventory-spaces.service';
import { InventoryOwnerService } from './services/inventory-owner.service';
import { InventoryLogoUploaderService } from './services/inventory-logo-uploader.service';
import { InventoriesFormDocumentFormComponent } from './components/inventories-form-document-form/inventories-form-document-form.component';
import { InventoryDocumentUploaderService } from './services/inventory-document-uploader.service';
import { VendorsLoaderService } from '../../services/guards/loaders/vendors-loader.service';
import { InventoryUploaderComponent } from './components/inventory-uploader/inventory-uploader.component';
import { AssetStatusHistoryComponent } from './components/asset-status-history/asset-status-history.component';
import { InventoryTicketFormComponent } from './components/inventory-ticket-form/inventory-ticket-form.component';
import { SelectModule } from '../select/select.module';
import { InventoryPagingNextComponent } from './components/layout/inventory-paging-next/inventory-paging-next.component';
import { InventoryPagingPrevComponent } from './components/layout/inventory-paging-prev/inventory-paging-prev.component';
import { TableResizeModule } from '../table-resize/table-resize.module';
import { MaskedInputModule } from '../masked-input/masked-input.module';

const routes: Routes = [
    {path: '', pathMatch: 'full', component: InventoriesComponent},
    {path: ':id/asset-information', component: InventoriesFormAssetInformationComponent},
    {
        path: ':id/service-and-maintenance',
        component: InventoriesFormServiceMaintenanceComponent,
        canActivate: [VendorsLoaderService]
    },
    {
        path: ':id/asset-status',
        component: InventoriesFormAssetStatusComponent,
        children: [
            { path: 'ticket/:ticketId', component: InventoryTicketFormComponent }
        ]
    },
    {
        path: ':id/documents', component: InventoriesFormDocumentsComponent,
        children: [
            {path: ':id', component: InventoriesFormDocumentFormComponent}
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        PagingTableModule,
        ReactiveFormsModule,
        FormsModule,
        SharedModule,
        TableResizeModule,
        MaskedInputModule,
        RouterModule.forChild(routes),
        SelectModule
    ],
    declarations: [
        InventoriesComponent,
        InventoriesFormAssetInformationComponent,
        InventoriesFormServiceMaintenanceComponent,
        InventoriesFormAssetStatusComponent,
        InventoriesFormDocumentsComponent,
        InventoryCreationStepsComponent,
        InventoriesFormDocumentFormComponent,
        InventoryUploaderComponent,
        AssetStatusHistoryComponent,
        InventoryTicketFormComponent,
        InventoryPagingNextComponent,
        InventoryPagingPrevComponent
    ],
    providers: [
        InventorySpacesService,
        InventoryOwnerService,
        InventoryLogoUploaderService,
        InventoryDocumentUploaderService
    ]
})
export class RouteInventoryModule {
}
