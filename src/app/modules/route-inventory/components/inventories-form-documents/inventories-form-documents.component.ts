import { Component, ViewChild } from '@angular/core';
import { InventoryCreationStep } from '../../../../enums/inventory-creation-step.enum';
import { ActivatedRoute } from '@angular/router';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { InventoryDocumentResourceService } from '../../../../services/resources/inventory-document-resource.service';
import { UtilsService } from '../../../../services/utils.service';
import { TableHandler } from '../../../../classes/table-handler';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { InventoryDocument } from '../../../../models/inventory-document';
import { PagingTableService } from '../../../../services/paging-table.service';

@Component({
    selector: 'app-inventories-form-documents',
    templateUrl: './inventories-form-documents.component.html',
    styleUrls: ['./inventories-form-documents.component.styl'],
    providers: [PagingTableService]
})
export class InventoriesFormDocumentsComponent extends TableHandler {
    stepType = InventoryCreationStep;
    currentInventoryId: number;
    @ViewChild('table') table: PagingTableComponent;

    columns: PagingTableHeaderColumn[] = [
        { isSortable: true, title: 'Document Title', key: 'name' },
        { isSortable: true, title: 'Document Name', key: 'fileName' },
        { isSortable: false, title: 'Actions', key: 'actions' }
    ];

    constructor(
        private acRoute: ActivatedRoute,
        private documents: InventoryDocumentResourceService,
        private utils: UtilsService
    ) {
        super(acRoute);
        this.currentInventoryId = this.acRoute.snapshot.params.id;
    }

    getTable(): PagingTableComponent {
        return this.table;
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return this.documents.queryByInventory(this.utils.tableToHttpParams(params), this.currentInventoryId);
        }).bind(this);
    }

    delete(item: InventoryDocument) {
        this.documents.delete(item.id)
            .then(() => {
                this.table.delete(tableItem => tableItem.id === item.id);
            });
    }

    onNext(next: Function) {
        next();
    }

    onPrev(prev) {
        prev();
    }

}
