import { Component, ViewChild } from '@angular/core';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { PagingTableService } from '../../../../services/paging-table.service';
import { InventoryResourceService } from '../../../../services/resources/inventory-resource.service';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { CurrentPropertyService } from '../../../../services/current-property.service';
import { UtilsService } from '../../../../services/utils.service';
import { VenueModel } from '../../../../models/venue-model';
import { InventoryModel } from '../../../../models/inventory-model';
import { FormControl, FormGroup } from '@angular/forms';
import { DropdownItem } from '../../../../interfaces/dropdown-item';
import { InventoryCategoryResourceService } from '../../../../services/resources/inventory-category-resource.service';
import { InventoryCategoryModel } from '../../../../models/inventory-category-model';
import { ManufacturerResourceService } from '../../../../services/resources/manufacturer-resource.service';
import { ManufacturerModel } from '../../../../models/manufacturer-model';
import { InventoryAssetStatus } from '../../../../enums/inventory-asset-status.enum';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppError } from '../../../../enums/app-error.enum';
import { CheckboxTracker } from '../../../../classes/checkbox-tracker';
import { Router } from '@angular/router';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';
import { HttpResourceQueryParams } from '../../../../interfaces/http-resource-query-params';
import { InventoryPagingCurrent } from '../../../../store/inventory-paging/inventory-paging.actions';

@Component({
    selector: 'app-inventories',
    templateUrl: './inventories.component.html',
    styleUrls: ['./inventories.component.styl'],
    providers: [PagingTableService]
})
export class InventoriesComponent {

    @ViewChild('table') table: PagingTableComponent;

    columns: PagingTableHeaderColumn[] = [
        { isSortable: false, title: 'Make', key: 'make' },
        { isSortable: true, title: 'Model', key: 'model' },
        { isSortable: true, title: 'Equipment<br>Service Item', key: 'name' },
        { isSortable: true, title: 'Inventory Tag #', key: 'tagNumber' },
        { isSortable: false, title: 'Status', key: 'status' },
        { isSortable: false, title: 'Actions', key: 'actions' }
    ];

    filterForm: FormGroup;
    status = InventoryAssetStatus;
    currentFilters: any;
    checked: CheckboxTracker<number>;

    SelectFetchType = SelectFetchType;

    private latestParams: HttpResourceQueryParams;

    constructor(
        private inventory: InventoryResourceService,
        private currentVenue: CurrentPropertyService,
        private utilsService: UtilsService,
        private inventoryCategory: InventoryCategoryResourceService,
        private inventoryManufacturer: ManufacturerResourceService,
        private utils: UtilsService,
        private manufacturer: ManufacturerResourceService,
        private router: Router,
        private store: Store<AppState>
    ) {
        this.filterForm = new FormGroup({
            categoryId: new FormControl(),
            manufacturerId: new FormControl(),
            condition: new FormControl(InventoryAssetStatus.All),
            tagNumber: new FormControl(),
            barcode: new FormControl(),
            purchaseDateFrom: new FormControl(),
            purchaseDateTo: new FormControl()
        });
        this.currentFilters = null;

        this.checked = new CheckboxTracker<number>();
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return this.currentVenue.currentVenueAsync()
                .then((venue: VenueModel) => {
                    const queryParams = this.utilsService.tableToHttpParams(params);
                    if (this.currentFilters) {
                        queryParams.otherParams = {
                            ...this.currentFilters
                        };
                    }
                    this.latestParams = queryParams;
                    return this.inventory.queryByVenueIdWithManufacturerNames(queryParams, venue.id);
                });
        }).bind(this);
    }

    @AsyncMethod({
        taskName: 'delete_inventory',
        error: AppError.DELETE_INVENTORY
    })
    async delete(inventory: InventoryModel) {
        try {
            await this.inventoryManufacturer.decreaseUsage(inventory.manufacturerId);
        } catch (exc) {
            console.log(exc);
        }
        await this.inventory.delete(inventory.id);
        this.table.delete((u) => +u.id === +inventory.id);
    }

    async applyFilters() {
        this.currentFilters = {
            ...this.filterForm.value
        };

        if (this.currentFilters.categoryId === '') {
            this.currentFilters.categoryId = null;
        }

        if (this.currentFilters.manufacturerId === '') {
            this.currentFilters.manufacturerId = null;
        }

        if (this.currentFilters.purchaseDateFrom) {
            this.currentFilters.purchaseDateFrom = this.currentFilters.purchaseDateFrom.split('-').join('');
        }

        if (this.currentFilters.purchaseDateTo) {
            this.currentFilters.purchaseDateTo = this.currentFilters.purchaseDateTo.split('-').join('');
        }

        switch (this.currentFilters.condition) {
            case InventoryAssetStatus.All:
                this.currentFilters.condition = null;
                break;
            case InventoryAssetStatus.Active:
                this.currentFilters.condition = 'ACTIVE';
                break;
            case InventoryAssetStatus.AtRepair:
                this.currentFilters.condition = 'AT_REPAIR';
                break;
            case InventoryAssetStatus.Broken:
                this.currentFilters.condition = 'BROKEN';
                break;
            case InventoryAssetStatus.Missing:
                this.currentFilters.condition = 'MISSING';
                break;
            case InventoryAssetStatus.Removed:
                this.currentFilters.condition = 'REMOVED';
                break;
            case InventoryAssetStatus.ServiceRequired:
                this.currentFilters.condition = 'SERVICE_REQUIRED';
                break;
        }

        await this.table.resetPage();
        this.table.refresh();
    }

    async clearFilters() {
        this.currentFilters = null;
        this.filterForm.reset({
            categoryId: '',
            manufacturerId: '',
            condition: InventoryAssetStatus.All
        });
        await this.table.resetPage();
        this.table.refresh();
    }

    async deleteChecked() {
        await this.utilsService.toPromise(this.inventory.deleteMany(this.checked.checked.map((item: any) => item.id)));
        this.checked.checked.forEach((item: any) => {
            this.inventoryManufacturer.decreaseUsage(item.manufacturerId);
        });
        this.table.refresh();
    }

    async copy(item: InventoryModel) {
        const cloned = await this.inventory.clone(item.id);
        this.router.navigateByUrl(`/u/inventory/${cloned.id}/asset-information`);
    }

    edit($event, item: InventoryModel, index: number) {
        const current = {
            page: (this.latestParams.page * this.latestParams.size + index) as number,
            id: item.id
        };

        this.store.dispatch(new InventoryPagingCurrent(current));
    }

}
