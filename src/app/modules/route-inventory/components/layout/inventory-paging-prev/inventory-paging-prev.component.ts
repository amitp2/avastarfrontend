import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { InventoryResourceService } from '../../../../../services/resources/inventory-resource.service';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../../store/store';
import { InventoryPagingCurrent } from '../../../../../store/inventory-paging/inventory-paging.actions';
import { UtilsService } from '../../../../../services/utils.service';

@Component({
    selector: 'app-inventory-paging-prev',
    templateUrl: './inventory-paging-prev.component.html',
    styleUrls: ['./inventory-paging-prev.component.styl']
})
export class InventoryPagingPrevComponent implements OnInit, OnDestroy {
    private sub: Subscription;
    private prev: any;

    @Output('prev')
    public onPrev: EventEmitter<any> = new EventEmitter();

    constructor(
        private inventory: InventoryResourceService,
        private store: Store<AppState>,
        private route: ActivatedRoute,
        private router: Router,
        private utils: UtilsService
    ) {
    }

    async ngOnInit() {
        this.sub = this.route.params.subscribe(
            () => this.loadPrev()
        );
        this.loadPrev();
    }

    ngOnDestroy() {
        if (this.sub) {
            this.sub.unsubscribe();
        }
    }

    private async loadPrev() {
        this.prev = await this.inventory.getPrevForPaging();
    }

    private goToPrev() {
        if (this.prev) {
            this.onPrev.emit(this.handle.bind(this));
        }
    }

    public async handle() {
        if (this.prev) {
            this.store.dispatch(new InventoryPagingCurrent(this.prev));
            this.router.navigateByUrl(`/u/inventory`);
            await this.utils.resolveAfter(100);
            this.router.navigateByUrl(`/u/inventory/${this.prev.id}/asset-information`);
        }
    }
}
