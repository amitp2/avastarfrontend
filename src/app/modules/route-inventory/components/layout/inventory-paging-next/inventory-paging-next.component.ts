import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { InventoryResourceService } from '../../../../../services/resources/inventory-resource.service';
import { AppState } from '../../../../../store/store';
import { Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { InventoryPagingCurrent } from '../../../../../store/inventory-paging/inventory-paging.actions';
import { UtilsService } from '../../../../../services/utils.service';

@Component({
    selector: 'app-inventory-paging-next',
    templateUrl: './inventory-paging-next.component.html',
    styleUrls: ['./inventory-paging-next.component.styl']
})
export class InventoryPagingNextComponent implements OnInit, OnDestroy {
    private sub: Subscription;
    private next: any;

    @Output('next')
    public onNext: EventEmitter<any> = new EventEmitter();

    constructor(
        private inventory: InventoryResourceService,
        private store: Store<AppState>,
        private route: ActivatedRoute,
        private router: Router,
        private utils: UtilsService
    ) {
    }

    async ngOnInit() {
        this.sub = this.route.params.subscribe(
            () => this.loadNext()
        );
        this.loadNext();
    }

    ngOnDestroy() {
        if (this.sub) {
            this.sub.unsubscribe();
        }
    }

    private async loadNext() {
        this.next = await this.inventory.getNextForPaging();
    }

    private async handle() {
        if (this.next) {
            this.store.dispatch(new InventoryPagingCurrent(this.next));

            this.router.navigateByUrl(`/u/inventory`);
            await this.utils.resolveAfter(100);
            this.router.navigateByUrl(`/u/inventory/${this.next.id}/asset-information`);
        }
    }

    private goToNext() {
        if (this.next) {
            this.onNext.emit(this.handle.bind(this));
        }
    }
}
