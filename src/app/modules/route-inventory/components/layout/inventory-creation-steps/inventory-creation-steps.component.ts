import { Component, Input, OnInit } from '@angular/core';
import { InventoryCreationStep } from '../../../../../enums/inventory-creation-step.enum';

@Component({
    selector: 'app-inventory-creation-steps',
    templateUrl: './inventory-creation-steps.component.html',
    styleUrls: ['./inventory-creation-steps.component.styl']
})
export class InventoryCreationStepsComponent implements OnInit {

    @Input() step: InventoryCreationStep;
    @Input() inventoryId: any;

    stepType = InventoryCreationStep;

    constructor() {
    }

    ngOnInit() {
    }

    get isEdit() {
        return Number.isInteger(+this.inventoryId);
    }

    firstStepIsActive(): boolean {
        if (this.isEdit) {
            return this.step !== this.stepType.AssetInformation;
        }
        return this.step === this.stepType.AssetInformation ||
            this.step === this.stepType.ServiceAndMaintenance ||
            this.step === this.stepType.AssetStatus ||
            this.step === this.stepType.Documents;
    }

    secondStepIsActive(): boolean {
        if (this.isEdit) {
            return this.step !== this.stepType.ServiceAndMaintenance;
        }
        return this.step === this.stepType.ServiceAndMaintenance ||
            this.step === this.stepType.AssetStatus ||
            this.step === this.stepType.Documents;
    }

    thirdStepIsActive(): boolean {
        if (this.isEdit) {
            return this.step !== this.stepType.AssetStatus;
        }
        return this.step === this.stepType.AssetStatus ||
            this.step === this.stepType.Documents;
    }

    fourthStepIsActive(): boolean {
        if (this.isEdit) {
            return this.step !== this.stepType.Documents;
        }
        return this.step === this.stepType.Documents;
    }

}
