import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormHandler } from '../../../../classes/form-handler';
import { InventoryDocument } from '../../../../models/inventory-document';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { InventoryDocumentUploaderService } from '../../services/inventory-document-uploader.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { UtilsService } from '../../../../services/utils.service';
import { InventoryDocumentResourceService } from '../../../../services/resources/inventory-document-resource.service';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';

@Component({
    selector: 'app-inventories-form-document-form',
    templateUrl: './inventories-form-document-form.component.html',
    styleUrls: ['./inventories-form-document-form.component.styl']
})
export class InventoriesFormDocumentFormComponent extends FormHandler<InventoryDocument> {


    currentInventoryId: number;
    uploader: any;

    constructor(
        private activatedRoute: ActivatedRoute,
        private documentUploader: InventoryDocumentUploaderService,
        private utils: UtilsService,
        private document: InventoryDocumentResourceService,
        private router: Router
    ) {
        super();
        this.currentInventoryId = this.activatedRoute.parent.snapshot.params.id;
        this.uploader = this.documentUploader;
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    initializeForm(): FormGroup {
        return new FormGroup({
            name: new FormControl(null, [Validators.required, Validators.maxLength(100)]),
            url: new FormControl(null, [Validators.required])
        });
    }

    get(id: number): Promise<InventoryDocument> {
        throw new Error('Method not implemented.');
    }

    @AsyncMethod({
        taskName: 'document_creation',
        success: AppSuccess.INVENTORY_DOCUMENT_ADD,
        error: AppError.INVENTORY_DOCUMENT_ADD
    })
    add(model: InventoryDocument): Promise<any> {
        return this.document
            .addForInventory(model, this.currentInventoryId)
            .then(() => this.router.navigateByUrl(`/u/inventory/${this.currentInventoryId}/documents?fresh=${Date.now()}`))
            .catch(() => null);
    }

    edit(id: number, model: InventoryDocument): Promise<any> {
        throw new Error('Method not implemented.');
    }

    serialize(model: InventoryDocument) {
        return this.utils.transformInto(model, {
            name: { to: 'name' },
            url: { to: 'url' }
        });
    }

    deserialize(formValue: any): InventoryDocument {
        return this.utils.transformInto(formValue, {
            name: { to: 'name' },
            url: { to: 'url' }
        });
    }

}
