import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { InventoryCreationStep } from '../../../../enums/inventory-creation-step.enum';
import { DropdownItem } from '../../../../interfaces/dropdown-item';
import { InventoryCategoryResourceService } from '../../../../services/resources/inventory-category-resource.service';
import { InventoryCategoryModel } from '../../../../models/inventory-category-model';
import { FormHandler } from '../../../../classes/form-handler';
import { InventoryModel } from '../../../../models/inventory-model';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DynamicDropdownComponent } from '../../../shared/components/form-controls/dynamic-dropdown/dynamic-dropdown.component';
import { AsyncValue } from '../../../../classes/async-value';
import { Subscription } from 'rxjs/Subscription';
import { ManufacturerResourceService } from '../../../../services/resources/manufacturer-resource.service';
import { ManufacturerModel } from '../../../../models/manufacturer-model';
import { VendorResourceService } from '../../../../services/resources/vendor-resource.service';
import { AuthService } from '../../../../services/auth.service';
import { InventorySpacesService } from '../../services/inventory-spaces.service';
import { InventoryAssetOwnerType } from '../../../../enums/inventory-asset-owner-type.enum';
import { InventoryOwnerService } from '../../services/inventory-owner.service';
import { FileUploader } from '../../../../interfaces/file-uploader';
import { InventoryLogoUploaderService } from '../../services/inventory-logo-uploader.service';
import {
    InventoryResourceService,
    inventoryResourceTransformer
} from '../../../../services/resources/inventory-resource.service';
import { UtilsService } from '../../../../services/utils.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { CurrentPropertyService } from '../../../../services/current-property.service';
import { ObjectTransformerType } from '../../../../enums/object-transformer-type.enum';

import 'rxjs/add/operator/distinctUntilChanged';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { InventorySubCategoryFetchFactory } from '../../../select/fetches/factories/inventory-sub-category.fetch.factory';
import { InventorySubCategoryItemFetchFactory } from '../../../select/fetches/factories/inventory-sub-category-item.fetch.factory';
import { StorageLocationFetchFactory } from '../../../select/fetches/factories/storage-location.fetch.factory';
import { InventoryOwnerFetchFactory } from '../../../select/fetches/factories/inventory-owner.fetch.factory';

@Component({
    selector: 'app-inventories-form-asset-information',
    templateUrl: './inventories-form-asset-information.component.html',
    styleUrls: ['./inventories-form-asset-information.component.styl']
})
export class InventoriesFormAssetInformationComponent extends FormHandler<InventoryModel> implements OnInit, OnDestroy {


    @ViewChild('subCategory')
    subCategoryDropdown: DynamicDropdownComponent;

    @ViewChild('vendorsDropdown')
    vendorsDropdown: DynamicDropdownComponent;

    stepType = InventoryCreationStep;
    assetOwnerType = InventoryAssetOwnerType;
    currentCategoryId: AsyncValue<number>;
    subscriptions: Subscription[];
    pictureUploader: FileUploader;
    SelectFetchType = SelectFetchType;

    currentManufacturerId: number;


    constructor(
        private inventoryCategory: InventoryCategoryResourceService,
        private activatedRoute: ActivatedRoute,
        private manufacturer: ManufacturerResourceService,
        private vendor: VendorResourceService,
        private authService: AuthService,
        private inventorySpaces: InventorySpacesService,
        private inventoryOwner: InventoryOwnerService,
        private inventoryLogoUploader: InventoryLogoUploaderService,
        private inventory: InventoryResourceService,
        private utilsService: UtilsService,
        private currentVenue: CurrentPropertyService,
        private router: Router,
        public inventorySubCategoryFetchFactory: InventorySubCategoryFetchFactory,
        public inventorySubCategoryItemFetchFactory: InventorySubCategoryItemFetchFactory,
        public storageLocationFetchFactory: StorageLocationFetchFactory,
        public inventoryOwnerFetchFactory: InventoryOwnerFetchFactory
    ) {
        super();
        this.currentCategoryId = new AsyncValue();
        this.subscriptions = [];
        this.pictureUploader = this.inventoryLogoUploader;
    }

    ngOnInit() {
        super.ngOnInit();
        const copyId = this.activatedRoute.snapshot.queryParams.copy;
        if (copyId) {
            this.get(copyId)
                .then((inv: InventoryModel) => this.form.patchValue({
                    categoryId: inv.categoryId,
                    subCategoryId: inv.subCategoryId,
                    name: inv.name,
                    manufacturerName: inv.manufacturerName,
                    model: inv.model
                }));
        }
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        this.subscriptions.forEach((subs: Subscription) => {
            subs.unsubscribe();
        });
    }

    loadManufacturers = (term: string): Promise<any[]> => {
        return this.manufacturer
            .searchByName(term)
            .then((resp: ManufacturerModel[]) => resp.map((model: ManufacturerModel) => model.name));
    };

    vendorCreated() {
        this.vendorsDropdown.refresh();
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    initializeForm(): FormGroup {
        return new FormGroup({
            categoryId: new FormControl(null, [Validators.required]),
            subCategoryId: new FormControl(null, [Validators.required]),
            name: new FormControl(null, []),
            subCategoryItemId: new FormControl(null, [Validators.required]),
            manufacturerName: new FormControl(null, [Validators.required]),
            model: new FormControl(),
            serialNumber: new FormControl(null),
            tagNumber: new FormControl(null),
            barcode: new FormControl(null),
            purchaseDate: new FormControl(null),
            purchasePrice: new FormControl(null),
            vendorId: new FormControl(null),
            portable: new FormControl(true),
            storageId: new FormControl(),
            assetOwnerType: new FormControl(InventoryAssetOwnerType.Owner),
            assetOwnerId: new FormControl(null),
            pictureUrl: new FormControl(null)
        });
    }

    private async checkManufacturer(): Promise<ManufacturerModel> {
        const name = this.form.get('manufacturerName').value;
        const man: ManufacturerModel[] = await this.manufacturer.searchByName(name);
        const found = man.find((m: ManufacturerModel) => m.name.toLowerCase() === name.toLowerCase());
        if (found) {
            return found;
        }
        const added = await this.manufacturer.add(new ManufacturerModel({ name }));
        return added;
        // return this.manufacturer
        //     .searchByName(name)
        //     .then((man: ManufacturerModel[]) => {
        //         const found = man.find((m: ManufacturerModel) => m.name.toLowerCase() === name.toLowerCase());
        //         if (found) {
        //             return found;
        //         }
        //         return this.manufacturer.add(new ManufacturerModel({name}));
        //     });
    }

    private async transformModel(model): Promise<InventoryModel> {
        const storageId = this.form.get('storageId').value;
        const man: ManufacturerModel = await this.checkManufacturer();

        const isAdd = !this.currentManufacturerId;

        if (isAdd) {
            await this.manufacturer.increaseUsage(man.id);
        } else {
            if (this.currentManufacturerId !== man.id) {
                await this.manufacturer.decreaseUsage(this.currentManufacturerId);
                await this.manufacturer.increaseUsage(man.id);
            }
        }

        model.manufacturerId = man.id;
        model.storageId = this.inventorySpaces.getRealId(storageId);
        model.storageType = this.inventorySpaces.getSpaceType(storageId);

        return model;
        // return this.checkManufacturer()
        //     .then((man: ManufacturerModel) => {
        //         model.manufacturerId = man.id;
        //         model.storageId = this.inventorySpaces.getRealId(storageId);
        //         model.storageType = this.inventorySpaces.getSpaceType(storageId);
        //         return model;
        //     });
    }

    @AsyncMethod({
        taskName: 'load_inventory'
    })
    async get(id: number): Promise<InventoryModel> {
        await this.utilsService.resolveAfter(1000);
        const inv = await this.inventory.get(id);

        try {
            const man = await this.manufacturer.get(inv.manufacturerId);
            inv.manufacturerName = man.name;
            this.currentManufacturerId = man.id;
        } catch (exc) {
            console.log(exc);
        }

        // TODO: workaround
        this.utilsService.resolveAfter(500).then(() => {
            this.form.patchValue({
                subCategoryId: inv.subCategoryId,
                storageId: inv.storageId
            });
            this.form.markAsPristine();
        });

        return inv;
    }

    @AsyncMethod({
        taskName: 'add_inventory',
        success: AppSuccess.INVENTORY_ADD,
        error: AppError.INVENTORY_ADD
    })
    add(model: InventoryModel): Promise<any> {
        
        return this.transformModel(model)
            .then((newModel: InventoryModel) => {
                return this.inventory.addForProperty(newModel, this.currentVenue.snapshot.id);
            })
            .then((added: InventoryModel) => {
                this.router.navigateByUrl(`/u/inventory/${added.id}/service-and-maintenance`);
            });
    }

    @AsyncMethod({
        taskName: 'edit_inventory',
        success: AppSuccess.INVENTORY_EDIT,
        error: AppError.INVENTORY_EDIT
    })
    edit(id: number, model: InventoryModel): Promise<any> {
        return this.transformModel(model)
            .then((newModel: InventoryModel) => {
                return this.inventory.edit(id, newModel);
            })
            .then(() => {
                this.router.navigateByUrl(`/u/inventory/${id}/service-and-maintenance`);
            });
    }

    serialize(model: InventoryModel): any {
        const transform = {
            ...inventoryResourceTransformer,
            storageType: {
                to: 'storageType'
            },
            assetOwnerType: {
                to: 'assetOwnerType'
            },
            manufacturerName: {
                to: 'manufacturerName'
            },
            storageId: {
                to: 'storageId',
                type: ObjectTransformerType.Function,
                transform: (val) => this.inventorySpaces.getId(val, model.storageType)
            }
        };
        delete transform.id;
        delete transform.manufacturerId;
        delete transform.storageType;

        return this.utilsService.transformInto(model, transform);
    }

    deserialize(formValue: any): InventoryModel {
        const transformer = {
            ...inventoryResourceTransformer,
            storageType: {
                to: 'storageType',
                type: ObjectTransformerType.Default
            },
            assetOwnerType: {
                to: 'assetOwnerType',
                type: ObjectTransformerType.Default
            }
        };
        delete transformer.id;
        delete transformer.manufacturerId;
        delete transformer.storageType;
        return this.utilsService.transformInto(formValue, transformer);
    }

    removeImage() {
        this.form.get('pictureUrl').setValue('');
    }

    cancel() {
        if (this.form.dirty) {
            if (confirm('Clicking Cancel will result in losing all changes. Click “OK” to go to the inventory List, or “Cancel” to go back to your changes')) {
                this.router.navigateByUrl('/u/inventory');
            }
            return;
        }
        this.router.navigateByUrl('/u/inventory');
    }

    @AsyncMethod({
        taskName: 'edit_inventory',
        success: AppSuccess.INVENTORY_EDIT,
        error: AppError.INVENTORY_EDIT
    })
    private justEdit() {
        const handleResponse = () => this.submitting = false;
        const model = this.deserialize(this.form.value);

        return this.transformModel(model)
            .then((newModel: InventoryModel) => {
                return this.inventory.edit(this.currentEntityId, newModel).then(handleResponse);
            }).catch(handleResponse);
    }

    async onNext(next: Function) {
        if (this.form.pristine) {
            return next();
        }
        if (confirm('Save any changes and move to the next asset?')) {
            if (!this.form.valid) {
                return alert('Please fill all required fields before saving.');
            }
            await this.justEdit();
            next();
        }
    }

    async onPrev(prev: Function) {
        if (this.form.pristine) {
            return prev();
        }
        if (confirm('Save any changes and move to the next asset')) {
            if (!this.form.valid) {
                return alert('Please fill all required fields before saving.');
            }
            await this.justEdit();
            prev();
        }
    }
}
