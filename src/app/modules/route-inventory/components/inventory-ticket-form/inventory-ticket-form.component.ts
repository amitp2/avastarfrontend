import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AbstractTicketForm } from '../../../../classes/abstract-ticket-form';
import { PromisedStoreService } from '../../../../services/promised-store.service';
import { UtilsService } from '../../../../services/utils.service';
import { TicketResourceService } from '../../../../services/resources/ticket-resource.service';
import { VendorResourceService } from '../../../../services/resources/vendor-resource.service';
import { AuthService } from '../../../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { VendorContactResourceService } from '../../../../services/resources/vendor-contact-resource.service';
import { TicketModel } from '../../../../models/ticket-model';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { VendorContactFetchFactory } from '../../../select/fetches/factories/vendor-contact.fetch.factory';

@Component({
    selector: 'app-inventory-ticket-form',
    templateUrl: './inventory-ticket-form.component.html',
    styleUrls: ['./inventory-ticket-form.component.styl']
})
export class InventoryTicketFormComponent extends AbstractTicketForm implements OnInit {


    get backLink(): string {
        return `/u/inventory/${this.inventoryId}/asset-status`;
    }

    get inventoryId(): string {
        return this.route.parent.snapshot.params.id;
    }

    SelectFetchType = SelectFetchType;

    constructor(public route: ActivatedRoute,
                vendors: VendorResourceService,
                vendorContact: VendorContactResourceService,
                utilsService: UtilsService,
                authService: AuthService,
                public rout: Router,
                tickets: TicketResourceService,
                utils: UtilsService,
                promisedStore: PromisedStoreService,
                public vendorContactFetchFactory: VendorContactFetchFactory) {
        super(route,
            vendors,
            vendorContact,
            utilsService,
            authService,
            rout,
            tickets,
            utils,
            promisedStore
        );
    }

    ngOnInit() {
        super.ngOnInit();
        this.form.patchValue({
            inventoryId: this.inventoryId
        });
    }


    afterAdd(added: TicketModel): any {
        this.rout.navigateByUrl(`${this.backLink}?refreshTickets=${Date.now()}`);
    }

    getId(): number {
        return +this.route.snapshot.params.ticketId;
    }

    getIdParam(): string {
        return 'ticketId';
    }

}
