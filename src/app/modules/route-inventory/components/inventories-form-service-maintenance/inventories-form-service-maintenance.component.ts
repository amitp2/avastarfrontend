import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { InventoryCreationStep } from '../../../../enums/inventory-creation-step.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { FormHandler } from '../../../../classes/form-handler';
import { InventoryMaintenanceModel } from '../../../../models/inventory-maintenance-model';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { InventoryMaintenanceSchedule } from '../../../../enums/inventory-maintenance-schedule.enum';
import { InventoryMaintenanceResourceService } from '../../../../services/resources/inventory-maintenance-resource.service';
import { UtilsService } from '../../../../services/utils.service';
import { DropdownItem } from '../../../../interfaces/dropdown-item';
import { VendorResourceService } from '../../../../services/resources/vendor-resource.service';
import { AuthService } from '../../../../services/auth.service';
import { VendorModel } from '../../../../models/vendor-model';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';
import { DynamicDropdownComponent } from '../../../shared/components/form-controls/dynamic-dropdown/dynamic-dropdown.component';
import { Subscription } from 'rxjs/Subscription';
import { AppCache } from '../../../../interfaces/cache';
import { UnsubscribePoint } from '../../../../decorators/unsubscribe-point.decorator';
import { ObservableSubscription } from '../../../../decorators/observable-subscription.decorator';
import * as moment from 'moment';
import { SelectFetchType } from '../../../select/types/select-fetch-type';

@Component({
    selector: 'app-inventories-form-service-maintenance',
    templateUrl: './inventories-form-service-maintenance.component.html',
    styleUrls: ['./inventories-form-service-maintenance.component.styl']
})
export class InventoriesFormServiceMaintenanceComponent extends FormHandler<InventoryMaintenanceModel> implements OnInit, OnDestroy {

    @ViewChild('warranyVendor') warranyVendor: DynamicDropdownComponent;
    @ViewChild('serviceVendor') serviceVendor: DynamicDropdownComponent;

    stepType = InventoryCreationStep;
    schedule = InventoryMaintenanceSchedule;

    @ObservableSubscription
    vendorsSubs: Subscription;

    SelectFetchType = SelectFetchType;

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute,
                private inventoryMaintenance: InventoryMaintenanceResourceService,
                private utils: UtilsService,
                private vendor: VendorResourceService,
                private authService: AuthService,
                private store: Store<AppState>,
                private ch: AppCache) {
        super();
    }

    ngOnInit() {
        super.ngOnInit();

        this.form.get('schedule')
            .valueChanges
            .merge(this.form.get('lastServiceDate').valueChanges)
            .debounceTime(500)
            .skip(1)
            .subscribe(() => {

                const schedule = this.form.value.schedule;
                const startDate = this.form.value.lastServiceDate;
                if (!startDate || !schedule) {
                    return;
                }

                let nextServiceDate;

                switch (schedule) {
                    case InventoryMaintenanceSchedule.ANNUAL:
                        nextServiceDate = moment(startDate).add(1, 'years').format('YYYY-MM-DD');
                        break;
                    case InventoryMaintenanceSchedule.MONTHLY:
                        nextServiceDate = moment(startDate).add(1, 'months').format('YYYY-MM-DD');
                        break;
                    case InventoryMaintenanceSchedule.QUARTERLY:
                        nextServiceDate = moment(startDate).add(1, 'quarters').format('YYYY-MM-DD');
                        break;
                    case InventoryMaintenanceSchedule.SEMI_ANNUAL:
                        nextServiceDate = moment(startDate).add(6, 'months').format('YYYY-MM-DD');
                        break;
                }

                this.form.patchValue({
                    nextServiceDate
                });

            });

        this.vendorsSubs = this.store.select('vendors').subscribe(() => {
            this.warranyVendor.refresh();
            this.serviceVendor.refresh();
        });

    }

    @UnsubscribePoint
    ngOnDestroy() {
        super.ngOnDestroy();
    }

    cache() {
        return this.ch;
    }

    formName() {
        return 'inventories_form_service_and_maintenance_' + this.activatedRoute.snapshot.params.id;
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    initializeForm(): FormGroup {
        return new FormGroup({
            warrantyExpDate: new FormControl(),
            warrantyVendorId: new FormControl(),
            serviceExpDate: new FormControl(),
            serviceVendorId: new FormControl(),
            schedule: new FormControl(null, [Validators.required]),
            lastServiceDate: new FormControl(null, [Validators.required]),
            nextServiceDate: new FormControl(null, [Validators.required])
        });
    }

    @AsyncMethod({
        taskName: 'load_maintenance_info'
    })
    get(id: number): Promise<InventoryMaintenanceModel> {
        return this.utils.resolveAfter(1000)
            .then(() => this.inventoryMaintenance.getByInventoryId(id));
    }

    add(model: InventoryMaintenanceModel): Promise<any> {
        throw new Error("Method not implemented.");
    }

    @AsyncMethod({
        taskName: 'update_inventory_asset_maintenance',
        success: AppSuccess.INVENTORY_ASSET_MAINTENANCE_EDIT,
        error: AppError.INVENTORY_ASSET_MAINTENANCE_EDIT
    })
    edit(id: number, model: InventoryMaintenanceModel): Promise<any> {
        return this.inventoryMaintenance
            .updateforInventory(model, id)
            .then(() => {
                this.router.navigateByUrl(`/u/inventory/${this.currentEntityId}/asset-status`);
            });
    }

    serialize(model: InventoryMaintenanceModel) {
        return this.utils.transformInto(model, {
            warrantyExpDate: {to: 'warrantyExpDate'},
            warrantyVendorId: {to: 'warrantyVendorId'},
            serviceExpDate: {to: 'serviceExpDate'},
            serviceVendorId: {to: 'serviceVendorId'},
            lastServiceDate: {to: 'lastServiceDate'},
            nextServiceDate: {to: 'nextServiceDate'},
            schedule: {to: 'schedule'}
        });
    }

    deserialize(formValue: any): InventoryMaintenanceModel {
        return this.utils.transformInto(formValue, {
            warrantyExpDate: {to: 'warrantyExpDate'},
            warrantyVendorId: {to: 'warrantyVendorId'},
            serviceExpDate: {to: 'serviceExpDate'},
            serviceVendorId: {to: 'serviceVendorId'},
            lastServiceDate: {to: 'lastServiceDate'},
            nextServiceDate: {to: 'nextServiceDate'},
            schedule: {to: 'schedule'}
        });
    }

    cancel() {
        // console.log(this.form.dirty);
        if (this.form.dirty) {
            if (confirm('Clicking Cancel will result in losing all changes. Click “OK” to go to the inventory List, or “Cancel” to go back to your changes')) {
                this.router.navigateByUrl('/u/inventory');
                this.clearFormCache();
            }
            return;
        }
        this.router.navigateByUrl('/u/inventory');
        this.clearFormCache();
    }

    @AsyncMethod({
        taskName: 'update_inventory_asset_maintenance',
        success: AppSuccess.INVENTORY_ASSET_MAINTENANCE_EDIT,
        error: AppError.INVENTORY_ASSET_MAINTENANCE_EDIT
    })
    private justEdit() {
        const handleResponse = () => this.submitting = false;
        const model = this.deserialize(this.form.value);

        return this.inventoryMaintenance
            .updateforInventory(model, this.currentEntityId)
            .then(handleResponse)
            .catch(handleResponse);
    }

    async onNext(next: Function) {
        if (this.form.pristine) {
            return next();
        }
        if (confirm('Save any changes and move to the next asset?')) {
            if (!this.form.valid) {
                return alert('Please fill all required fields before saving.');
            }
            await this.justEdit();
            next();
        }
    }

    async onPrev(prev: Function) {
        if (this.form.pristine) {
            return prev();
        }
        if (confirm('Save any changes and move to the next asset')) {
            if (!this.form.valid) {
                return alert('Please fill all required fields before saving.');
            }
            await this.justEdit();
            prev();
        }
    }
}
