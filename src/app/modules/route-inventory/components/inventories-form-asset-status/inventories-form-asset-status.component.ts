import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { InventoryCreationStep } from '../../../../enums/inventory-creation-step.enum';
import { FormHandler } from '../../../../classes/form-handler';
import { InventoryAssetStatusModel } from '../../../../models/inventory-asset-status-model';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { InventoryAssetRating } from '../../../../enums/inventory-asset-rating.enum';
import { InventoryAssetStatus } from '../../../../enums/inventory-asset-status.enum';
import { InventoryAssetStatusResourceService } from '../../../../services/resources/inventory-asset-status-resource.service';
import { UtilsService } from '../../../../services/utils.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { AppCache } from '../../../../interfaces/cache';
import { AssetStatusHistoryComponent } from '../asset-status-history/asset-status-history.component';
import { Subscription } from 'rxjs/Subscription';
import { InventoryModel } from '../../../../models/inventory-model';

@Component({
    selector: 'app-inventories-form-asset-status',
    templateUrl: './inventories-form-asset-status.component.html',
    styleUrls: ['./inventories-form-asset-status.component.styl']
})
export class InventoriesFormAssetStatusComponent extends FormHandler<InventoryAssetStatusModel> implements OnInit, OnDestroy {


    @ViewChild('history') history: AssetStatusHistoryComponent;

    stepType = InventoryCreationStep;
    rating = InventoryAssetRating;
    status = InventoryAssetStatus;

    refreshTicketsSubs: Subscription;

    constructor(private activatedRoute: ActivatedRoute,
                private inventoryAssetStatus: InventoryAssetStatusResourceService,
                private utils: UtilsService,
                private router: Router,
                private ch: AppCache) {
        super();
    }

    ngOnInit() {
        super.ngOnInit();
        this.refreshTicketsSubs = this.activatedRoute.queryParams
            .map((params: any) => params.refreshTickets)
            .filter(fresh => fresh ? true : false)
            .subscribe(() => this.history.refresh());
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        this.refreshTicketsSubs.unsubscribe();
    }

    cache() {
        return this.ch;
    }

    formName() {
        return 'inventories_form_assset_status_' + this.activatedRoute.snapshot.params.id;
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    initializeForm(): FormGroup {
        return new FormGroup({
            rating: new FormControl(InventoryAssetRating.Excellent),
            condition: new FormControl(null, [Validators.required]),
            comment: new FormControl()
        });
    }

    @AsyncMethod({
        taskName: 'load_asset_status'
    })
    get(id: number): Promise<InventoryAssetStatusModel> {
        return this.inventoryAssetStatus
            .getByInventoryId(id);
    }

    add(model: InventoryAssetStatusModel): Promise<any> {
        return this.inventoryAssetStatus.updateForInventory(model, this.currentEntityId);
    }

    @AsyncMethod({
        taskName: 'asset_status_update',
        success: AppSuccess.INVENTORY_ASSET_STATUS_EDIT,
        error: AppError.INVENTORY_ASSET_STATUS_EDIT
    })
    edit(id: number, model: InventoryAssetStatusModel): Promise<any> {
        return this.inventoryAssetStatus
            .updateForInventory(model, id)
            .then(() => this.router.navigateByUrl(`/u/inventory/${this.currentEntityId}/documents`));
    }

    serialize(model: InventoryAssetStatusModel) {
        const transform = {
            rating: {
                to: 'rating'
            },
            condition: {
                to: 'condition'
            },
            comment: {
                to: 'comment'
            }
        };
        return this.utils.transformInto(model, transform);
    }

    deserialize(formValue: any): InventoryAssetStatusModel {
        return this.utils.transformInto(formValue, {
            rating: {
                to: 'rating'
            },
            condition: {
                to: 'condition'
            },
            comment: {
                to: 'comment'
            }
        });
    }

    cancel() {
        if (this.form.dirty) {
            if (confirm('Clicking Cancel will result in losing all changes. Click “OK” to go to the inventory List, or “Cancel” to go back to your changes')) {
                this.router.navigateByUrl('/u/inventory');
                this.clearFormCache();
            }
            return;
        }
        this.router.navigateByUrl('/u/inventory');
        this.clearFormCache();
    }

    @AsyncMethod({
        taskName: 'asset_status_update',
        success: AppSuccess.INVENTORY_ASSET_STATUS_EDIT,
        error: AppError.INVENTORY_ASSET_STATUS_EDIT
    })
    private justEdit() {
        const handleResponse = () => this.submitting = false;
        const model = this.deserialize(this.form.value);

        return this.inventoryAssetStatus
            .updateForInventory(model, this.currentEntityId)
            .then(handleResponse)
            .catch(handleResponse);
    }

    async onNext(next: Function) {
        if (this.form.pristine) {
            return next();
        }
        if (confirm('Save any changes and move to the next asset?')) {
            if (!this.form.valid) {
                return alert('Please fill all required fields before saving.');
            }
            await this.justEdit();
            next();
        }
    }

    async onPrev(prev: Function) {
        if (this.form.pristine) {
            return prev();
        }
        if (confirm('Save any changes and move to the next asset')) {
            if (!this.form.valid) {
                return alert('Please fill all required fields before saving.');
            }
            await this.justEdit();
            prev();
        }
    }
}
