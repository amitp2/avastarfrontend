import { Component, OnInit, ElementRef, ViewChild, EventEmitter, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { ConfigService } from '../../../../services/config.service';
import { CurrentPropertyService } from '../../../../services/current-property.service';
import { HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { AppError } from '../../../../enums/app-error.enum';

interface InventoryUploadResult {
    okCount: number;
    errorCount: number;
    readError: number;
    brokenLines: number[];
    brokenLinesLogic: number[];
}

@Component({
    selector: 'app-inventory-uploader',
    templateUrl: './inventory-uploader.component.html',
    styleUrls: ['./inventory-uploader.component.styl']
})
export class InventoryUploaderComponent implements OnInit {
    apiUrl: string;

    @ViewChild('file') fileInput: ElementRef;
    @Output() inventoryUploaded: EventEmitter<number> = new EventEmitter();

    constructor(
        private http: HttpClient,
        private config: ConfigService,
        private currentVenue: CurrentPropertyService,
        private toastr: ToastrService
    ) {
        this.apiUrl = config.config().API_URL;
    }

    ngOnInit() {
    }

    onChange(files: File[]) {
        if (files.length) {
            this.upload(files[0]);
        }
    }

    open() {
        if (this.fileInput) {
            this.fileInput.nativeElement.click();
        }
    }

    @AsyncMethod({
        taskName: 'upload_inventory_from_file',
        error: AppError.INVENTORY_UPLOAD
    })
    upload(file: File) {
        return this.currentVenue.currentVenueAsync()
            .then(venue => {
                const form = new FormData();
                form.append('file', file, file.name);

                return this.http.put(`${this.apiUrl}venues/inventories/import?venueId=${venue.id}`, form)
                    .toPromise()
                    .then((data: InventoryUploadResult) => {
                        const allLines = data.errorCount + data.okCount + data.readError;
                        let message = `
                            <div>lines in the file: ${allLines}</div>
                            <div>lines uploaded: ${data.okCount}</div>
                            <div>lines rejected: ${data.readError + data.errorCount}</div>
                        `;
                        const toastrConfig = {
                            positionClass: 'toast-bottom-right',
                            tapToDismiss: false,
                            enableHtml: true
                        };

                        const brokenLines = [
                            ...(data.brokenLines || []),
                            ...(data.brokenLinesLogic || [])
                        ];

                        if (brokenLines.length) {
                            message += `
                                <div>Failing lines: ${brokenLines.join(', ')}</div>
                            `;
                        }

                        if (data.okCount) {
                            if (data.errorCount || data.readError) {
                                this.toastr.warning(message, 'Warning', toastrConfig);
                            } else {
                                this.toastr.success(message, 'Success', toastrConfig);
                            }
                            this.inventoryUploaded.emit(data.okCount);
                        } else {
                            this.toastr.error(message, 'Error', toastrConfig);
                        }
                        this.fileInput.nativeElement.value = '';
                        this.fileInput.nativeElement.type = '';
                        this.fileInput.nativeElement.type = 'file';
                    });
            });
    }

}
