import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';
import { TicketResourceService } from '../../../../services/resources/ticket-resource.service';
import { PromisedStoreService } from '../../../../services/promised-store.service';
import { TicketModel } from '../../../../models/ticket-model';

@Component({
    selector: 'app-asset-status-history',
    templateUrl: './asset-status-history.component.html',
    styleUrls: ['./asset-status-history.component.styl']
})
export class AssetStatusHistoryComponent implements OnInit, OnDestroy {

    @Input() set inventoryId(val: string) {
        this.inventoryIdObs.next(val);
        this.inventoryIdCurrent = val;
    }

    inventoryIdCurrent: string;
    inventoryIdObs: BehaviorSubject<string>;
    inventoryIdObsSubs: Subscription;
    ticketsList: TicketModel[];

    constructor(private tickets: TicketResourceService,
                private promisedStore: PromisedStoreService) {
        this.inventoryIdObs = new BehaviorSubject<string>(null);
    }

    refresh() {
        if (this.inventoryIdCurrent) {
            this.inventoryIdObs.next(this.inventoryIdCurrent);
        }
    }

    ngOnInit() {
        this.inventoryIdObsSubs = this.inventoryIdObs
            .filter((val: string) => !!val)
            .subscribe((inventoryId: string) => {
                this.promisedStore
                    .currentVenueId()
                    .then((venueId: number) => this.tickets.getAllByInventoryId(+inventoryId, venueId))
                    .then((tickets: TicketModel[]) => this.ticketsList = tickets);
            });
    }

    ngOnDestroy() {
        this.inventoryIdObsSubs.unsubscribe();
    }

}
