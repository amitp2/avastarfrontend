import { Injectable } from '@angular/core';
import { EventSpaceResourceService } from '../../../services/resources/event-space-resource.service';
import { StorageSpaceResourceService } from '../../../services/resources/storage-space-resource.service';
import { CurrentPropertyService } from '../../../services/current-property.service';
import { VenueModel } from '../../../models/venue-model';
import { EventSpaceModel } from '../../../models/event-space-model';
import { StorageSpaceModel } from '../../../models/storage-space-model';
import { InventoryStorageType } from '../../../enums/inventory-storage-type.enum';

export interface InventorySpace {
    id: number;
    name: string;
}


export interface InventorySpaceInfo {
    id: number;
    type: InventoryStorageType;
}

@Injectable()
export class InventorySpacesService {

    private threshold: number;

    constructor(private eventSpace: EventSpaceResourceService,
                private storageSpace: StorageSpaceResourceService,
                private currentVenue: CurrentPropertyService) {
        this.threshold = 9999999;
    }

    async getOnlyEventSpaces(): Promise<InventorySpace[]> {
        const venue = await this.currentVenue.currentVenueAsync();
        const eventSpaces = await this.eventSpace.getAllByVenueId(venue.id);
        const functionSpaces = eventSpaces.map(es => es.functionSpaces).filter(fs => !!fs);
        return functionSpaces.reduce((acc, cur) => acc.concat(cur), []);
    }

    getOnlyStorageSpaces(): Promise<InventorySpace[]> {
        return this.currentVenue.currentVenueAsync()
            .then((venue: VenueModel) => {
                return Promise.all([
                    this.storageSpace.getAllByVenueId(venue.id)
                ]).then(([storageSpaces]) => {
                    const res = storageSpaces.map((ss: StorageSpaceModel) => {
                        return {
                            id: ss.id,
                            name: ss.name
                        };
                    })
                    return res;
                });
            });
    }

    getSpaces(): Promise<InventorySpace[]> {
        return this.currentVenue.currentVenueAsync()
            .then((venue: VenueModel) => {
                return Promise.all([
                    this.eventSpace.getAllByVenueId(venue.id),
                    this.storageSpace.getAllByVenueId(venue.id)
                ]).then(([eventSpaces, storageSpaces]) => {
                    let res = eventSpaces.map((es: EventSpaceModel) => {
                        return {
                            id: this.threshold + es.id,
                            name: es.name
                        };
                    });
                    res.push({id: -1, name: '----------------------------'});
                    res = res.concat(storageSpaces.map((ss: StorageSpaceModel) => {
                        return {
                            id: ss.id,
                            name: ss.name
                        };
                    }));
                    return res;
                });
            });
    }

    getRealId(id: number): number {
        return +id % this.threshold;
    }

    getId(id: number, type: InventoryStorageType) {
        if (type === InventoryStorageType.EventSpace) {
            return this.threshold + id;
        }
        return id;
    }

    getSpaceType(id: number): InventoryStorageType {
        if (id > this.threshold) {
            return InventoryStorageType.EventSpace;
        }
        return InventoryStorageType.StorageSpace;
    }

}
