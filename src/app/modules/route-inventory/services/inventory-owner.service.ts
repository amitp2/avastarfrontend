import { Injectable } from '@angular/core';
import { VendorResourceService } from '../../../services/resources/vendor-resource.service';
import { CurrentPropertyService } from '../../../services/current-property.service';
import { VenueModel } from '../../../models/venue-model';
import { AuthService } from '../../../services/auth.service';
import { VendorModel } from '../../../models/vendor-model';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/store';
import { PromisedStoreService } from '../../../services/promised-store.service';

export interface InventoryOwner {
    id: number;
    name: string;
}

@Injectable()
export class InventoryOwnerService {

    constructor(private vendor: VendorResourceService,
                private currentVenue: CurrentPropertyService,
                private authService: AuthService,
                private store: Store<AppState>,
                private promisedStore: PromisedStoreService) {
    }

    async getOnlyOwner(): Promise<InventoryOwner[]> {
        const venue = await this.promisedStore.currentVenue();
        const res = [ {id: -1, name: venue.owner} ];
        return res;
    }

    async getOnlyOperator(): Promise<InventoryOwner[]> {
        const venue = await this.promisedStore.currentVenue();
        const res = [ {id: -2, name: venue.operator } ];
        return res;
    }

    async getOnlyVendors(): Promise<InventoryOwner[]> {
        const user = await this.authService.currentUser();
        const vendors = await this.vendor.getAllBySubscriber(user.subscriberId);
        const res = vendors.map(v => ({id: v.id, name: v.name}));
        return res;
    }

}
