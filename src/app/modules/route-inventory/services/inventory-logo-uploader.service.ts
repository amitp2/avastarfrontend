import { Injectable } from '@angular/core';
import { StandardUploader } from '../../../classes/standard-uploader';
import { ConfigService } from '../../../services/config.service';
import { AuthService } from '../../../services/auth.service';
import { AppSuccess } from '../../../enums/app-success.enum';
import { AllowedFileType } from '../../../enums/allowed-file-type.enum';

@Injectable()
export class InventoryLogoUploaderService extends StandardUploader {

    constructor(private configService: ConfigService, authService: AuthService) {
        super(authService);
    }

    uploadUrl(): string {
        return `${this.configService.config().API_URL}files/inventory/picture`;
    }

    asyncTaskName() {
        return 'inventory_logo_upload';
    }

    uploadAppSuccess() {
        return AppSuccess.INVENTORY_LOGO_UPLOAD;
    }

    allowedFileType(): AllowedFileType {
        return AllowedFileType.Images;
    }

}
