import { Injectable } from '@angular/core';
import { StandardUploaderV2 } from '../../../classes/standard-uploader-v2';
import { AppSuccess } from '../../../enums/app-success.enum';
import { AllowedFileType } from '../../../enums/allowed-file-type.enum';
import { AuthService } from '../../../services/auth.service';
import { ConfigService } from '../../../services/config.service';

@Injectable()
export class InventoryDocumentUploaderService extends StandardUploaderV2 {

    constructor(authService: AuthService,
                private config: ConfigService) {
        super(authService);
    }

    uploadUrl(): string {
        return `${this.config.config().API_URL}files/inventory/documents`;
    }

    asyncTaskName(): string {
        return 'inventory_doc_upload';
    }

    uploadAppSuccess(): AppSuccess {
        return AppSuccess.INVENTORY_DOCUMENT_UPLOAD;
    }

    allowedFileType(): AllowedFileType {
        return AllowedFileType.All;
    }

}
