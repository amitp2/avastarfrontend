import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { FunctionsFormComponent } from './components/functions-form/functions-form.component';
import { FunctionDayComponent } from './components/function-day/function-day.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FunctionDayListComponent } from './components/function-day-list/function-day-list.component';
import { FunctionEquipmentsComponent } from './components/function-equipments/function-equipments.component';
import { FunctionEquipmentsSearchComponent } from './components/function-equipments-search/function-equipments-search.component';
import { FunctionEquipmentsSelectedComponent } from './components/function-equipments-selected/function-equipments-selected.component';
import { DragulaModule } from 'ng2-dragula';
import { FunctionTotalsComponent } from './components/function-totals/function-totals.component';
import { SelectModule } from '../select/select.module';
import { MaskedInputModule } from '../masked-input/masked-input.module';
import { FunctionPagingNextComponent } from './components/function-paging-next/function-paging-next.component';
import { FunctionPagingPrevComponent } from './components/function-paging-prev/function-paging-prev.component';

const routes: Routes = [
    {path: ':id', component: FunctionsFormComponent}
];


@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        PagingTableModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        FormsModule,
        DragulaModule,
        SelectModule,
        MaskedInputModule
    ],
    declarations: [
        FunctionsFormComponent,
        FunctionDayComponent,
        FunctionDayListComponent,
        FunctionEquipmentsComponent,
        FunctionEquipmentsSearchComponent,
        FunctionEquipmentsSelectedComponent,
        FunctionTotalsComponent,
        FunctionPagingNextComponent,
        FunctionPagingPrevComponent
    ]
})
export class RouteFunctionsModule {
}
