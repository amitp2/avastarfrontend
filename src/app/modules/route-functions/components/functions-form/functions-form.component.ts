import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
    FunctionEquipmentModel, FunctionModel, FunctionServiceChargeCalculationType, FunctionStatus,
    FunctionType
} from '../../../../models/function-model';
import { FormConstructorService } from '../../../../services/form-constructor/form-constructor.service';
import { FormHandler } from '../../../../classes/form-handler';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { FunctionResourceService } from '../../../../services/resources/function-resource.service';
import { UtilsService } from '../../../../services/utils.service';
import { UnsubscribePoint } from '../../../../decorators/unsubscribe-point.decorator';
import { Subscription } from 'rxjs/Subscription';
import { ObservableSubscription } from '../../../../decorators/observable-subscription.decorator';
import { VenueEventResourceService } from '../../../../services/resources/venue-event-resource.service';
import { VenueEventModel } from '../../../../models/venue-event-model';
import * as moment from 'moment';
import { AsyncTasksService } from '../../../../services/async-tasks.service';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { PromisedStoreService } from '../../../../services/promised-store.service';
import { VenueSettingsResourceService } from '../../../../services/resources/venue-settings-resource.service';
import { VenueClientResourceService } from '../../../../services/resources/venue-client-resource.service';
import { VenueClientModel } from '../../../../models/venue-client-model';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { EventFetchFactory } from '../../../select/fetches/factories/event.fetch.factory';
import { ToastrService } from 'ngx-toastr';
import { FunctionContactResourceService } from '../../../../services/resources/function-contact-resource.service';
import { FunctionContactModel } from '../../../../models/function-contact-model';
import { InMemoryStorageService } from '../../../../services/in-memory-storage.service';
import { formModeIsEdit, getIdParam, randomString } from '../../../../helpers/helper-functions';
import { isSetProp } from '../../../../helpers/functions/is-set-prop';
import { setFormFieldValue } from '../../../../helpers/functions/set-form-field-value';
import { getFormField } from '../../../../helpers/functions/get-form-field';
import { SelectComponent } from '../../../select/select/select.component';
import { GlobalsService } from '../../../../services/globals.service';
declare var R: any;

export interface FunctionsFormPageData {
    selectedEquipments: FunctionEquipmentModel[];
}

@Component({
    selector: 'app-functions-form',
    templateUrl: './functions-form.component.html',
    styleUrls: ['./functions-form.component.styl']
})
export class FunctionsFormComponent extends FormHandler<FunctionModel> implements OnInit, OnDestroy {

    functionStatus = FunctionStatus;
    functionType = FunctionType;
    mode = FormMode;
    teoPrefix: string;
    teoNumber: any;
    CodeCount: number = 0;
    functionServiceChargeCalculationType = FunctionServiceChargeCalculationType;

    @ViewChild('functionSpaceId')
    functionSpaceId: SelectComponent;

    IsCodeValid = [];

    get maxDatePossible(): any {
        if (!this.form.value.startDate) {
            return null;
        }
        return moment(this.form.value.startDate).add(6, 'days').format('YYYY-MM-DD');
    }

    get minDateForEndDate() {
        if (!this.form.value.startDate) {
            return null;
        }
        return moment(this.form.value.startDate).format('YYYY-MM-DD');
    }


    private get $(): any {
        return this.globals.globals().$;
    }

    @ObservableSubscription
    eventIdSubs: Subscription;

    @ObservableSubscription
    queryParamsSubs: Subscription;

    @ObservableSubscription
    modeSubs: Subscription;

    @ObservableSubscription
    startDateSubs: Subscription;

    @ObservableSubscription
    makeFormDirtySubs: Subscription;

    cancelLink: string;

    SelectFetchType = SelectFetchType;

    contactToAutocompleteItem = (contact: FunctionContactModel) => contact.name;

    constructor(
        private route: ActivatedRoute,
        private functions: FunctionResourceService,
        private utils: UtilsService,
        private router: Router,
        private events: VenueEventResourceService,
        private asyncTask: AsyncTasksService,
        private promisedStore: PromisedStoreService,
        private venueSettings: VenueSettingsResourceService,
        private venueClient: VenueClientResourceService,
        public eventFetchFactory: EventFetchFactory,
        private contacts: FunctionContactResourceService,
        private toast: ToastrService,
        private inMemoryStorage: InMemoryStorageService,
        private globals: GlobalsService,
    ) {
        super();
    }

    loadSalesContacts = async (term: string): Promise<any[]> => {
        const items = await this.contacts.searchSalesContact(term);
        const convertResults = R.map(this.contactToAutocompleteItem);
        const res = convertResults(items);
        return res;
    }

    loadPlanningContacts = async (term: string): Promise<any[]> => {
        const items = await this.contacts.searchPlanningContact(term);
        const convertResults = R.map(this.contactToAutocompleteItem);
        const res = convertResults(items);
        return res;
    }

    ngOnInit() {
        super.ngOnInit();

        this.fetchTeoPrefix();


        this.makeFormDirtySubs = this.asyncTask.track('mark_function_form_dirty').subscribe(() => {
            console.log('make form dirty');
            this.form.markAsDirty();
        });

        this.eventIdSubs = this.form.get('eventId')
            .valueChanges
            // .distinctUntilChanged()
            .filter(val => !!val)
            .subscribe((val: number) => {
                const pullFormDataFromQueryParams = this.route.snapshot.queryParams.putFromQueryParams;
                if (this.currentFormMode === FormMode.Add && !pullFormDataFromQueryParams) {
                    this.events.get(val).then((event: VenueEventModel) => {
                        this.form.patchValue({
                            masterAccountNumber: event.masterAccountNumber,
                            eventCode: event.code
                        });
                    });
                }
            });

        this.modeSubs = this.formMode().subscribe((mode: FormMode) => {
            this.setCancelLink();
            if (mode === FormMode.Add) {
                if (!this.route.snapshot.queryParams.eventId) {
                    return;
                }
                this.events
                    .get(this.route.snapshot.queryParams.eventId)
                    .then((event) => this.form.patchValue({ status: event.status }));
            }
        });
        
        this.queryParamsSubs = this.route.queryParams.subscribe(async (params: any) => {
            this.setCancelLink();

            await this.utils.resolveAfter(2000);

            const isFormModeAdd = this.currentFormMode === FormMode.Add;

            if (!isFormModeAdd) {
                await this.utils.resolveAfter(1000);
                this.setAddedEvent(params);
                this.setAddedAccount(params);
                this.checkCopy(params);
                return;
            }

            const pullFormDataFromQueryParams = params.putFromQueryParams;
            const pullFormDataFromInMemoryStorage = params.getDataFrom;

            if (pullFormDataFromInMemoryStorage) {
                const previousData = await this.inMemoryStorage.get(params.getDataFrom);
                if (previousData) {
                    this.form.patchValue(previousData);
                }
                this.setAddedEvent(params);
                this.setAddedAccount(params);
                return;
            }

            if (pullFormDataFromQueryParams) {
                const isSetQueryParam = R.partial(isSetProp, [params]);
                const getQueryParam = R.ifElse(isSetQueryParam, R.pipe(R.partialRight(R.prop, [params]), decodeURIComponent), R.always(''));
                const getField = R.partial(getFormField, [this.form]);
                const setFieldValueFromQueryParam = (field: string) => {
                    const setFieldValue = R.partial(setFormFieldValue, [getField(field)]);
                    const set = R.pipe(getQueryParam, setFieldValue);
                    set(field);
                };

                setFieldValueFromQueryParam('masterAccountNumber');
                setFieldValueFromQueryParam('clientId');
                setFieldValueFromQueryParam('eventId');
                setFieldValueFromQueryParam('eventCode');
                setFieldValueFromQueryParam('status');
                return;
            }


            if (!params.eventId || !params.accountId) {
                return;
            }

            this.form.patchValue({
                eventId: +params.eventId,
                clientId: +params.accountId
            });

            this.venueClient
                .get(+params.accountId)
                .then((venue: VenueClientModel) => {
                    if (venue.masterAgreementAgreed) {
                        this.form.patchValue({
                            alternativeServiceChargePercentage: venue.alternativeServiceChargePercentage,
                            standardDiscountRate: venue.standardDiscountRate,
                            discountRateForSetupDays: venue.discountRateForSetupDays,
                            alternativeServiceCharge: venue.alternativeServiceCharge,
                            taxExempt: venue.taxExempt,
                            serviceChargeCalculation: venue.serviceChargeCalculation
                        });
                    }
                });
        });
    }

    setAddedEvent(queryParams: any) {
        const prop = 'addedEvent';
        const isSetAddedEvent = R.partialRight(isSetProp, [prop]);
        const setEventIdValue = R.partial(setFormFieldValue, [this.form.get('eventId')]);
        const setEventId = R.pipe(R.prop(prop), setEventIdValue);
        const set = R.when(isSetAddedEvent, setEventId);
        set(queryParams);
    }

    setAddedAccount(queryParams: any) {
        const prop = 'addedAccount';
        const isSetAddedAccount = R.partialRight(isSetProp, [prop]);
        const setClientIdValue = R.partial(setFormFieldValue, [this.form.get('clientId')]);
        const setClientId = R.pipe(R.prop(prop), setClientIdValue);
        const set = R.when(isSetAddedAccount, setClientId);
        set(queryParams);
    }

    async checkCopy(queryParams: any) {
        const backTo = queryParams.backTo ? `backTo=${encodeURIComponent(queryParams.backTo)}` : ``;
        if (queryParams.copy) {
            await this.router.navigateByUrl(`/u/functions/add?backTo=${backTo}`);
            this.form.patchValue({
                functionSpaceId: null,
                locationNote: null,
                functionName: null
            });
            this.functionSpaceId.refresh();
        }
    }

    async setCancelLink() {
        const backTo = this.route.snapshot.queryParamMap.get('backTo');
        this.cancelLink = backTo ? backTo : '/u/event-management';
    }

    async fetchTeoPrefix(): Promise<any> {
        const venueId = await this.promisedStore.currentVenueId();
        const settings = await this.venueSettings.getForVenue(venueId);
        this.teoPrefix = settings.teo;
    }

    @UnsubscribePoint
    ngOnDestroy() {
        super.ngOnDestroy();
    }

    entityId(): Observable<number> {
        return this.paramsId(this.route);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.route);
    }

    initializeForm(): FormGroup {
        return FormConstructorService.build(FunctionModel);
    }

    @AsyncMethod({
        taskName: 'get_function'
    })
    get(id: number): Promise<FunctionModel> {
        return this.utils
            .resolveAfter(1000)
            .then(() => this.functions.get(id))
            .then((func) => {
                this.teoNumber = func.teoNumber;
                return func;
            });
    }

    private processLastRequestMeta() {
        const meta = this.functions.getLastRequestMeta();
        if (meta) {
            this.toast.warning(meta, 'Warning', {
                positionClass: 'toast-bottom-right',
                tapToDismiss: false
            });
        }
    }

    @AsyncMethod({
        taskName: 'add_function'
    })
    add(model: FunctionModel): Promise<any> {
        return new Promise((resolve, reject) => {
            let itemCodeIndex = 0;
            for (const itemCode of model.eventFunctionEquipments) {
                this.IsCodeValid[itemCodeIndex] = false;
                if (itemCode.code == undefined) {
                    this.CodeCount++;
                    this.IsCodeValid[itemCodeIndex] = true;
                }
                itemCodeIndex++
            }
            if (this.CodeCount == 0) {
                resolve(true)
                    this.functions
                    .add(model)
                    .then((added: FunctionModel) => {
                        this.router.navigateByUrl(`/u/functions/${added.id}`);
                        this.processLastRequestMeta();
                        this.asyncTask.taskSuccess('edit_function', AppSuccess.ADD_FUNCTION);
                    })
                    .catch(err => {
                        const error = JSON.parse(err.error);
                        if (+error.errorCode === 1004) {
                            this.asyncTask.taskError('edit_function', AppError.NOT_ENOUGH_ITEMS);
                        }
                        if (+error.errorCode === 1005) {
                            this.asyncTask.taskError('edit_function', AppError.NOT_ENOUGH_ITEMS);
                        }
                        if (+error.errorCode === 2005) {
                            this.asyncTask.taskError('edit_function', AppError.PRICE_EXTENSION_ERRROR);
                        }
                    })
               resolve(true);
            }
            else {
                this.toast.warning("Code is required", "Warning", {
                    positionClass: "toast-bottom-right",
                    tapToDismiss: false,
                })
                this.CodeCount = 0;
                reject(true)
            }
        })

    }



    @AsyncMethod({
        taskName: 'edit_function'
    })
    edit(id: number, model: FunctionModel): Promise<any> {
        return new Promise((resolve, reject) => {
            let itemCodeIndex = 0;
            for (const itemCode of model.eventFunctionEquipments) {
                this.IsCodeValid[itemCodeIndex] = false;
                if (itemCode.code == undefined) {
                    this.CodeCount++;
                    this.IsCodeValid[itemCodeIndex] = true;
                }
                itemCodeIndex++;
            }
            if (this.CodeCount == 0) {
                resolve(true);
                return this.functions.edit(id, model)
                    .then((updated: FunctionModel) => this.router.navigateByUrl(`/u/functions/${updated.id}`))
                    .then(() => {
                        this.processLastRequestMeta();
                        this.asyncTask.taskSuccess('edit_function', AppSuccess.EDIT_FUNCTION);
                    })
                    .catch(err => {
                        const error = err.error;

                        if (+error.errorCode === 1004) {
                            this.asyncTask.taskError('edit_function', AppError.NOT_ENOUGH_ITEMS);
                        }

                        if (+error.errorCode === 1005) {
                            this.asyncTask.taskError('edit_function', AppError.NOT_ENOUGH_ITEMS);
                        }

                        if (+error.errorCode === 2005) {
                            this.asyncTask.taskError('edit_function', AppError.PRICE_EXTENSION_ERRROR);
                        }

                        //return null;
                        resolve(true);
                    });
            }
            else {
                this.toast.warning('code are required', 'Warning', {
                    positionClass: 'toast-bottom-right',
                    tapToDismiss: false
                });
                this.CodeCount = 0;
                reject(true);
            }
        })
    }

    serialize(model: FunctionModel): any {
        return FormConstructorService.putModel(model);
    }

    deserialize(formValue: any): FunctionModel {
        return FormConstructorService.extractModel(formValue, FunctionModel);
    }


    async saveAndNew() {
        await this.submit();
        const model: FunctionModel = FormConstructorService.extractModel(this.form.value, FunctionModel);
        const queryString = this.utils.buildQueryStringUsingObject({
            masterAccountNumber: model.masterAccountNumber,
            clientId: model.clientId,
            eventId: model.eventId,
            eventCode: model.eventCode,
            status: model.status
        });
        await this.router.navigateByUrl(`/u/functions/add?${queryString}putFromQueryParams=true`);
        location.reload();
    }

    async saveAndClose() {
        await this.submit();
        await this.router.navigateByUrl(this.cancelLink);
    }


    goToUrl = (url: string) => this.router.navigateByUrl(url);
    buildAddUrl = (randomToken) => `/u/functions/add?getDataFrom=${randomToken}`;
    buildEditUrl = (id: string, randomToken: string) => `/u/functions/${id}?getDataFrom=${randomToken}`;

    async addAccount() {
        const randomToken = randomString(20);

        await this.inMemoryStorage.put(randomToken, this.form.value);

        const buildAddUrl = R.partial(this.buildAddUrl, [randomToken]);
        const buildEditUrl = R.partialRight(this.buildEditUrl, [randomToken]);
        const goToAccountAddUrl = (backToUrl: string) => this.goToUrl(`/u/accounts/add/account-info?backToUrl=${backToUrl}`);

        const buildUrl = R.ifElse(formModeIsEdit, buildEditUrl, buildAddUrl);
        const addAcountForFunction = R.pipe(getIdParam, buildUrl, encodeURIComponent, goToAccountAddUrl);

        addAcountForFunction(this.route);
    }

    async addEvent(accountId: string) {
        const randomToken = randomString(20);
        await this.inMemoryStorage.put(randomToken, this.form.value);

        const buildAddUrl = R.partial(this.buildAddUrl, [randomToken]);
        const buildEditUrl = R.partialRight(this.buildEditUrl, [randomToken]);
        const goToAddEventUrl = (backToUrl: string) => this.goToUrl(`/u/event-management/add/info?accountId=${accountId}&backToUrl=${backToUrl}`);

        const buildUrl = R.ifElse(formModeIsEdit, buildEditUrl, buildAddUrl);
        const addEventForFunction = R.pipe(getIdParam, buildUrl, encodeURIComponent, goToAddEventUrl);

        addEventForFunction(this.route);
    }

    async onNext(next: Function) {
        if (this.form.pristine) {
            return next();
        }
        if (confirm('Save any changes and move to the next asset?')) {
            if (!this.form.valid) {
                return alert('Please fill all required fields before saving.');
            }
            await this.edit(this.currentEntityId, this.deserialize(this.form.value));
            next();
        }
    }

    async onPrev(prev: Function) {
        if (this.form.pristine) {
            return prev();
        }
        if (confirm('Save any changes and move to the next asset')) {
            if (!this.form.valid) {
                return alert('Please fill all required fields before saving.');
            }
            await this.edit(this.currentEntityId, this.deserialize(this.form.value));
            prev();
        }
    }

    async onPatchValue() {
        await this.utils.resolveAfter(500);
        this.form.markAsPristine();
    }
}
