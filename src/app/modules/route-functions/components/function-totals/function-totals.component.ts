import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { UnsubscribePoint } from '../../../../decorators/unsubscribe-point.decorator';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';
import { Subscription } from 'rxjs/Subscription';
import { ObservableSubscription } from '../../../../decorators/observable-subscription.decorator';
import {
    FunctionReport,
    FunctionRevenueCategoryCalculatorService
} from '../../../../services/function-revenue-category-calculator.service';
import { UtilsService } from '../../../../services/utils.service';

@Component({
    selector: 'app-function-totals',
    templateUrl: './function-totals.component.html',
    styleUrls: ['./function-totals.component.styl']
})
export class FunctionTotalsComponent implements OnInit, OnDestroy {

    visible = false;

    @ObservableSubscription
    selectedEquipmentsSubs: Subscription;

    @ViewChild('content')
    content: any;

    report: FunctionReport[];

    constructor(private store: Store<AppState>,
                private calcService: FunctionRevenueCategoryCalculatorService,
                private utils: UtilsService) {
    }

    ngOnInit() {
        this.selectedEquipmentsSubs = this.selectedEquipmentsSubs = this.store.select('functionsFormPage')
            .map((data) => data.selectedEquipments)
            .debounceTime(1000)
            .subscribe((val) => this.generateReport(val));
    }

    @UnsubscribePoint
    ngOnDestroy(): void {
    }

    async generateReport(equipments: any) {
        this.report = await this.calcService.calculate(equipments);
    }

    show() {
        this.visible = true;
    }

    close() {
        this.visible = false;
    }

    print() {
        const content = this.content.nativeElement.innerHTML;
        this.utils.print(content);
    }

}
