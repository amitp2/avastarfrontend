import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';
import { FunctionDay, FunctionEquipment, FunctionEquipmentDateModel, FunctionEquipmentModel } from '../../../../models/function-model';
import { Observable } from 'rxjs/Observable';
import { UnsubscribePoint } from '../../../../decorators/unsubscribe-point.decorator';
import { Subscription } from 'rxjs/Subscription';
import { ObservableSubscription } from '../../../../decorators/observable-subscription.decorator';
import * as moment from 'moment';
import {
    AddSelectedEquipments,
    ChangeDayCount,
    RemoveSelectedEquipments,
    UpdateDayQuantityForEquipment
} from '../../../../store/functions-form-page/functions-form-page.actions';
import { RevenueCategoryResourceService } from '../../../../services/resources/revenue-category-resource.service';
import { RevenueCategoryModel } from '../../../../models/revenue-category-model';
import { AuthService } from '../../../../services/auth.service';
import { CurrentUser } from '../../../../interfaces/current-user';
import { DragulaService } from 'ng2-dragula';
import { UtilsService } from '../../../../services/utils.service';
import { GlobalsService } from '../../../../services/globals.service';
import { FunctionRevenueCategoryCalculatorService } from '../../../../services/function-revenue-category-calculator.service';
import { FunctionTotalsComponent } from '../function-totals/function-totals.component';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { FormControl, FormGroup } from '@angular/forms';
import { SelectItemIdToFuncPackageOrEquipmentConverter } from '../../../select/converters/select-item-id-to-func-package-or-equipment.converter';
import { EquipmentModel } from '../../../../models/equipment-model';
import { PackageModel } from '../../../../models/package-model';
import { PackageOrEquipmentToFunctionEquipmentConverter } from '../../../../helpers/converters/package-or-equipment-to-function-equipment.converter';
import { ToastrService } from 'ngx-toastr';
import { resetFormField } from '../../../../helpers/functions/reset-form-field';
import { SelectComponent } from '../../../select/select/select.component';
import { setSelectionRange } from '../../../../helpers/functions/set-selection-range';
import { AsyncTasksService } from '../../../../services/async-tasks.service';


declare var R: any;

@Component({
    selector: 'app-function-equipments-selected',
    templateUrl: './function-equipments-selected.component.html',
    styleUrls: ['./function-equipments-selected.component.styl'],
    providers: [DragulaService]
})
export class FunctionEquipmentsSelectedComponent implements OnInit, OnDestroy, OnChanges {

    selectedItemsObs: Observable<FunctionEquipment[]>;
    selectedItems: FunctionEquipmentModel[];

    @ViewChild('totals')
    totals: FunctionTotalsComponent;

    @Input() IsCodeValid = [];

    private shouldFocusLastItem: boolean;

    get colsCount(): number {
        return 6 + this.daysCount;
    }

    daysCount: number;

    dayLabels: string[];
    days: Date[];

    @ObservableSubscription
    selectedItemsObsSubs: Subscription;

    @ObservableSubscription
    addFormSubs: Subscription;

    @Input()
    startDate: any;

    @Input()
    endDate: any;

    @Input()
    functionDays: FunctionDay[];

    @ViewChild('select')
    select: SelectComponent;

    get totalCost(): number {
        return this.selectedItems && this.selectedItems
            .filter(item => item.code !== 'C')
            .map(item => item.totalPrice).reduce((acc, cur) => acc + cur, 0);
    }

    revenueCategories: RevenueCategoryModel[];

    addForm: FormGroup;

    SelectFetchType = SelectFetchType;

    spacingBetweenDayColumns = 6;

    constructor(private store: Store<AppState>,
        private revenueCats: RevenueCategoryResourceService,
        private authService: AuthService,
        private dragulaService: DragulaService,
        private utilsService: UtilsService,
        private globals: GlobalsService,
        private calcService: FunctionRevenueCategoryCalculatorService,
        private selectedItemIdConverter: SelectItemIdToFuncPackageOrEquipmentConverter,
        private functionEquipmentConverter: PackageOrEquipmentToFunctionEquipmentConverter,
        private toastr: ToastrService,
        private asyncTask: AsyncTasksService) {

        this.daysCount = 0;
        this.selectedItemsObs = this.store.select('functionsFormPage')
            .map((data) => data.selectedEquipments)
            .debounceTime(1000);

        this.addForm = new FormGroup({
            item: new FormControl()
        });
    }

    private generateLabels(startDate: any, days: number) {
        this.dayLabels = [];

        let i = 1;
        for (; i <= days; i++) {
            this.dayLabels.push(startDate.format('MM / DD'));
            startDate = startDate.startOf('day').add(1, 'days').startOf('day');
        }
    }

    private generateDays(startDate: any, days: number) {
        this.days = [];

        let i = 1;
        for (; i <= days; i++) {
            this.days.push(startDate.startOf('day').format('YYYY-MM-DD'));
            startDate = startDate.startOf('day').add(1, 'days').startOf('day');
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        let start = this.startDate;
        let end = this.endDate;

        if (start && end) {
            start = moment(start).startOf('day');
            end = moment(end).startOf('day');

            let dif = end.diff(start, 'days');
            dif++;
            this.daysCount = dif;

            this.generateLabels(moment(start), this.daysCount);
            this.generateDays(moment(start), this.daysCount);
            this.store.dispatch(new ChangeDayCount({
                count: dif,
                startDay: moment(start)
            }));
        }
    }

    ngOnInit() {
        this.selectedItemsObsSubs = this.selectedItemsObs.subscribe((data) => {
            this.selectedItems = data;
            this.selectedItems.sort((a, b) => a.pos - b.pos);

            // this.utilsService.resolveAfter(1000)
            //     .then(() => {
            //         const selector = '.content-table-body tr:nth-last-child(2) input[type="number"]:first-child';
            //         $(selector).focus();
            //     });

            if (this.shouldFocusLastItem) {
                const $ = this.globals.globals().$;
                this.utilsService.resolveAfter(1000)
                    .then(() => {
                        const selector = 'tbody .function-item:last input[type="number"]:first-child';
                        $($(selector)[0]).focus();
                        this.shouldFocusLastItem = false;
                    });
            }
        });

        this.authService.currentUser()
            .then((user: CurrentUser) => this.revenueCats.getAllBySubscriberId(user.subscriberId))
            .then((resp) => this.revenueCategories = resp);
        this.dragulaService.drop.subscribe(val => {
            const table = val[2];
            this.orderChange(table);
        });

        this.addFormSubs = this.addForm
            .get('item')
            .valueChanges
            .subscribe((val) => this.addNewItem(val));
    }

    // TODO: we might need to change the name, later
    orderChange(table: any) {
        const $ = this.globals.globals().$;
        const items = $(table).find('tr').map(function () {
            return this.id;
        }).get();
        const posObject = {};

        items.forEach((item, index) => {
            posObject[item] = index;
        });

        this.selectedItems.forEach((item) => {
            item.pos = posObject[item.name];
        });
    }

    quantityChanged(event: any, item: FunctionEquipment, day: any) {
        this.store.dispatch(new UpdateDayQuantityForEquipment({
            day,
            item,
            quantity: event.target.value
        }));
    }

    changedCode(itemIndex) {
        this.IsCodeValid[itemIndex] = false;
        this.asyncTask.taskNotify('mark_function_form_dirty');
    }

    @UnsubscribePoint
    ngOnDestroy() {

    }

    delete(item: FunctionEquipment) {
        this.store.dispatch(new RemoveSelectedEquipments([item]));
    }

    showTotals() {
        this.totals.show();
    }

    async addNewItem(id) {
        if (!id) {
            return;
        }

        //const packageOrEquipment: PackageModel | EquipmentModel = await this.selectedItemIdConverter.convert(id);
        const packageOrEquipment: any = await this.selectedItemIdConverter.convert(id);
        const item: FunctionEquipmentModel = await this.functionEquipmentConverter.convert(packageOrEquipment);
        const isSet = R.pipe(R.isNil, R.not);

        const startDateIsSet = isSet(this.startDate);
        const endDateIsSet = isSet(this.endDate);
        const datesAreSet = startDateIsSet && endDateIsSet;

        const momentDay = (day) => moment.utc(day);
        const startOfMomentDay = (day) => day.startOf('day');
        const startOfDay = R.pipe(momentDay, startOfMomentDay);
        const daysBetweenMomentDays = (start, end) => end.diff(start, 'days') + 1;
        const formatMomentDay = (day) => day.format('YYYY-MM-DD');

        // Equiment & service expried pop up message shown
       //if (packageOrEquipment.clientDescription !== null && packageOrEquipment.name !== null) {
            if ((this.startDate < packageOrEquipment.priceFromDate) || (this.endDate > packageOrEquipment.priceToDate) && packageOrEquipment.type === "EQUIPMENT") {
                this.toastr.warning(packageOrEquipment.clientDescription + ' Price Expired. Please extend price end date for selected items.', 'Warning', {
                    positionClass: 'toast-bottom-right',
                    tapToDismiss: false
                });
                //this.addForm.get('item').reset();
               //this.selectedItems.
            }
       //}
        else {
            const isSetupDay = (days: FunctionDay[], index) => {
                const getDayAtIndex = R.partialRight(R.nth, [days]);
                const dayExistsAtIndex = R.pipe(getDayAtIndex, isSet);
                const isSetupDayAtIndex = R.pipe(getDayAtIndex, R.propEq('setupDay', true));
                const isSetup = R.ifElse(dayExistsAtIndex, isSetupDayAtIndex, R.always(false));
                return isSetup(index);
            };

            const getNthDayAfterMomentDay = (day: any, nth: number) => {
                const start = startOfMomentDay(moment(day));
                const res = startOfMomentDay(start.add(nth, 'days'));
                return res;
            };

            const formattedNthDay = R.pipe(R.partial(getNthDayAfterMomentDay, [momentDay(this.startDate)]), formatMomentDay);
            const isNthDaySetupDay = R.partial(isSetupDay, [this.functionDays]);
            const buildNthFunctionEquipmentDate = (index: number) => {
                const res = new FunctionEquipmentDateModel();
                res.quantity = 0;
                res.date = formattedNthDay(index);
                res.setupDay = isNthDaySetupDay(index);
                return res;
            };
            const generateFunctionEquipmentDateList = R.pipe(R.partial(R.range, [0]), R.partial(R.map, [buildNthFunctionEquipmentDate]));

            if (datesAreSet) {
                const daysCount = daysBetweenMomentDays(startOfDay(this.startDate), startOfDay(this.endDate));
                const equipmentDates = generateFunctionEquipmentDateList(daysCount);
                item.eventFunctionEquipmentDates = equipmentDates;
            }

            const itemIsInTheList = (item, list) => R.pipe(R.find(R.propEq('name', item.name)), isSet)(list);
            const itemIsInSelectedList = R.partialRight(itemIsInTheList, [this.selectedItems]);
            const addItemInTheList = (item: FunctionEquipmentModel) => {

                this.shouldFocusLastItem = true;
                this.store.dispatch(new AddSelectedEquipments([item]));
            };
            const showWarningMessage = () => this.toastr.warning('Selected item is already in the list', 'Warning', {
                positionClass: 'toast-bottom-right',
                tapToDismiss: false
            });
            const resetAddForm = () => this.select.refresh();
            const send = R.ifElse(itemIsInSelectedList, showWarningMessage, R.pipe(addItemInTheList, resetAddForm));

            send(item);
        }
    }


    onKeyPress(event: KeyboardEvent) {
        if (!isCharCodeEnabled(event.charCode || event.keyCode)) {
            event.preventDefault();
        }
    }
}

const enabledCharCodes = [
    9, 16, 37, 38, 39, 40, 46, 48,
    49, 50, 51, 52, 53, 54, 55, 56, 57,
];

function isCharCodeEnabled(charCode: number) {
    return enabledCharCodes.indexOf(charCode) !== -1;
}
