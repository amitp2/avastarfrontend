import { Component, forwardRef, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { ControlValueAccessor, FormArray, FormControl, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import * as moment from 'moment';
import { UnsubscribePoint } from '../../../../decorators/unsubscribe-point.decorator';
import { Subscription } from 'rxjs/Subscription';
import { ObservableSubscription } from '../../../../decorators/observable-subscription.decorator';
import { FunctionDayModel } from '../../../../models/function-model';

@Component({
    selector: 'app-function-day-list',
    templateUrl: './function-day-list.component.html',
    styleUrls: ['./function-day-list.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => FunctionDayListComponent),
            multi: true
        }
    ]
})
export class FunctionDayListComponent implements ControlValueAccessor, OnInit, OnDestroy, OnChanges {

    form: FormGroup;

    onChange: any;

    @Input()
    startDate: any;

    @Input()
    endDate: any;

    @ObservableSubscription
    formSubs: Subscription;

    dayLabels: string[];

    selectedDay: number;

    currentValue: any;

    get daysFormArray(): FormArray {
        return <FormArray> this.form.get('days');
    }

    constructor() {
        this.form = new FormGroup({
            days: new FormArray([])
        });
    }

    private generateLabels(startDate: any, days: number) {
        this.dayLabels = [];

        let i = 0;
        for (; i <= days; i++) {
            this.dayLabels.push(startDate.format('YYYY-MM-DD'));
            startDate = startDate.startOf('day').add(1, 'days').startOf('day');
        }
    }

    private clearDayArray() {
        while (this.daysFormArray.length > 0) {
            this.daysFormArray.removeAt(0);
        }
    }

    private generateDayArray(days: number) {
        let i = 0;
        for (; i <= days; i++) {
            this.daysFormArray.push(new FormControl());
        }
    }

    private consumeValue() {
        this.currentValue.forEach((val: any, index: number) => {
            if (this.daysFormArray.controls[index]) {
                this.daysFormArray.controls[index].setValue(val);
            }
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        let start = this.startDate;
        let end = this.endDate;

        if (start && end) {
            start = moment(start).startOf('day');
            end = moment(end).startOf('day');
            const dif = end.diff(start, 'days');
            if (dif >= 0) {
                this.selectedDay = 0;
                this.generateLabels(start, dif);
                this.clearDayArray();
                this.generateDayArray(dif);
                this.consumeValue();
            }
        }
    }


    ngOnInit() {
        this.form.valueChanges.subscribe((val: any) => this.updateOutside(val));
    }

    @UnsubscribePoint
    ngOnDestroy() {
    }

    writeValue(obj: any): void {
        this.currentValue = obj;
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
    }

    setDisabledState(isDisabled: boolean): void {
    }

    updateOutside(val: any) {
        if (this.onChange) {
            const days = val.days.map(day => day ? day : new FunctionDayModel());
            this.onChange(days);
        } else {
            console.log('on change hasn\'t registered on day list control');
        }
    }
}
