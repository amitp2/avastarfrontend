import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AppState } from '../../../../store/store';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FunctionResourceService } from '../../../../services/resources/function-resource.service';
import { FunctionPagingCurrent } from '../../../../store/function-paging/function-paging.actions';
import { UtilsService } from '../../../../services/utils.service';

@Component({
    selector: 'app-function-paging-prev',
    templateUrl: './function-paging-prev.component.html',
    styleUrls: ['./function-paging-prev.component.styl']
})
export class FunctionPagingPrevComponent implements OnInit, OnDestroy {
    private sub: Subscription;
    private prev: any;

    @Output('prev')
    public onPrev: EventEmitter<any> = new EventEmitter();

    constructor(
        private functionApi: FunctionResourceService,
        private store: Store<AppState>,
        private route: ActivatedRoute,
        private router: Router,
        private utils: UtilsService
    ) {
    }

    async ngOnInit() {
        this.sub = this.route.params.subscribe(
            () => this.loadPrev()
        );
        this.loadPrev();
    }

    ngOnDestroy() {
        if (this.sub) {
            this.sub.unsubscribe();
        }
    }

    private async loadPrev() {
        this.prev = await this.functionApi.getPrevForPaging();
    }

    private goToPrev() {
        if (this.prev) {
            this.onPrev.emit(this.handle.bind(this));
        }
    }

    public async handle() {
        if (this.prev) {
            this.store.dispatch(new FunctionPagingCurrent(this.prev));
            this.router.navigateByUrl(`/u/functions`);
            await this.utils.resolveAfter(100);
            this.router.navigateByUrl(`/u/functions/${this.prev.id}`);
        }
    }
}
