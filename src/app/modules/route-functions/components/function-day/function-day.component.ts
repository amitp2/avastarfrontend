import { Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormConstructorService } from '../../../../services/form-constructor/form-constructor.service';
import { FunctionDayModel } from '../../../../models/function-model';
import { UnsubscribePoint } from '../../../../decorators/unsubscribe-point.decorator';
import { Subscription } from 'rxjs/Subscription';
import { ObservableSubscription } from '../../../../decorators/observable-subscription.decorator';
import { SelectFetchType } from '../../../select/types/select-fetch-type';

@Component({
    selector: 'app-function-day',
    templateUrl: './function-day.component.html',
    styleUrls: ['./function-day.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => FunctionDayComponent),
            multi: true
        }
    ]
})
export class FunctionDayComponent implements ControlValueAccessor, OnInit, OnDestroy {


    onChange: any;
    form: FormGroup;

    @Input()
    date: string;

    @ObservableSubscription
    formSubs: Subscription;

    times: string[];

    SelectFetchType = SelectFetchType;

    private buildTimesStrings(): void {
        this.times = [];
        for (let i = 0; i <= 23; i++) {
            for (let j = 0; j <= 30; j += 30) {
                this.times.push(`${i < 10 ? 0 : ''}${i}:${j < 10 ? 0 : ''}${j}`);
            }
        }
    }

    constructor() {
        this.form = FormConstructorService.build(FunctionDayModel);
        this.buildTimesStrings();
    }


    ngOnInit() {
        this.formSubs = this.form.valueChanges.subscribe((formValue: any) => this.updateOutside(formValue));
    }

    @UnsubscribePoint
    ngOnDestroy(): void {
    }

    writeValue(obj: any): void {
        if (obj) {
            this.form.patchValue(FormConstructorService.putModel(obj));
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
    }

    updateOutside(obj: any) {
        if (this.onChange) {
            this.onChange(FormConstructorService.extractModel(obj, FunctionDayModel));
        } else {
            console.log('on change for function day hasnt registered yet');
        }
    }

}
