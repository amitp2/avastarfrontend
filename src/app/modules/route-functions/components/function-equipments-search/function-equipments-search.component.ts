import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { EquipmentResourceService } from '../../../../services/resources/equipment-resource.service';
import { PackageResourceService } from '../../../../services/resources/package-resource.service';
import { FormGroup } from '@angular/forms';
import { FormField } from '../../../../services/form-constructor/form-field';
import { DefaultValue } from '../../../../services/form-constructor/default-value';
import { FormConstructorService } from '../../../../services/form-constructor/form-constructor.service';
import { UnsubscribePoint } from '../../../../decorators/unsubscribe-point.decorator';
import { Subscription } from 'rxjs/Subscription';
import { ObservableSubscription } from '../../../../decorators/observable-subscription.decorator';
import { Mapping, mappingsBuildFrom, mappingsBuildTo } from '../../../../decorators/mapping.decorator';
import { PromisedStoreService } from '../../../../services/promised-store.service';
import { constantValueMapper } from '../../../../mappers/constant-value-mapper';
import { FunctionDay, FunctionEquipmentDate, FunctionEquipmentDateModel, FunctionEquipmentModel } from '../../../../models/function-model';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';
import { AddSelectedEquipments, ChangeDayCount } from '../../../../store/functions-form-page/functions-form-page.actions';
import { FunctionsFormPageData } from '../functions-form/functions-form.component';
import { Observable } from 'rxjs/Observable';
import * as moment from 'moment';
import { RevenueCategoryResourceService } from '../../../../services/resources/revenue-category-resource.service';
import { AuthService } from '../../../../services/auth.service';

export enum FunctionEquipmentsItemType {
    Equipments = 'Equipment &amp; Services',
    Packages = 'Package'
}

const equipmentsMappingNamespace = 'equipmentsMappingNamespace';
const packagesMappingNamespace = 'packagesMappingNamespace';
const functionEquipmentModelMappingNamespace = 'functionEquipmentModelMappingNamespace';

class FunctionEquipmentsSearchModel {

    @DefaultValue(FunctionEquipmentsItemType.Equipments)
    @FormField
    searchType: FunctionEquipmentsItemType;

    @FormField
    search: string;
}

export class FunctionEquipmentsItem {

    selected = false;

    disabled = false;

    @Mapping(packagesMappingNamespace, '', constantValueMapper(FunctionEquipmentsItemType.Packages))
    @Mapping(equipmentsMappingNamespace, '', constantValueMapper(FunctionEquipmentsItemType.Equipments))
    type: FunctionEquipmentsItemType;

    @Mapping(packagesMappingNamespace, 'id')
    @Mapping(equipmentsMappingNamespace, 'id')
    id: number;

    @Mapping(functionEquipmentModelMappingNamespace, 'name')
    @Mapping(packagesMappingNamespace, 'name')
    @Mapping(equipmentsMappingNamespace, 'clientDescription')
    title: string;

    @Mapping(packagesMappingNamespace, 'description')
    @Mapping(equipmentsMappingNamespace, 'clientDescription')
    description: string;

    @Mapping(functionEquipmentModelMappingNamespace, 'price')
    @Mapping(packagesMappingNamespace, 'price')
    @Mapping(equipmentsMappingNamespace, 'price')
    unitPrice: number;

    @Mapping(equipmentsMappingNamespace, 'revenueCategoryId')
    revenueCategoryId: number;

    code: any;
}

function equipmentIds(idKey: string) {
    return function (data: FunctionsFormPageData) {
        const ids = {};
        data.selectedEquipments.forEach((item) => {
            if (item[idKey]) {
                ids[item[idKey]] = true;
            }
        });
        return ids;
    };
}

@Component({
    selector: 'app-function-equipments-search',
    templateUrl: './function-equipments-search.component.html',
    styleUrls: ['./function-equipments-search.component.styl']
})
export class FunctionEquipmentsSearchComponent implements OnInit, OnDestroy {

    @Input()
    startDate: any;

    @Input()
    endDate: any;

    @Input()
    functionDays: FunctionDay[];

    form: FormGroup;
    searchType = FunctionEquipmentsItemType;
    itemsList: FunctionEquipmentsItem[];

    @ObservableSubscription
    formSubs: Subscription;

    @ObservableSubscription
    selectedServiceIdsObs: Subscription;

    @ObservableSubscription
    selectedPackageIdsObs: Subscription;

    selectedServiceIds: Observable<any>;
    selectedPackageIds: Observable<any>;

    get areItemsSelected(): boolean {
        return this.itemsList.filter((item) => item.selected).length > 0;
    }

    get currentType(): FunctionEquipmentsItemType {
        return this.form.value.searchType;
    }

    constructor(private equipments: EquipmentResourceService,
                private packages: PackageResourceService,
                private promisedStore: PromisedStoreService,
                private revenueCategory: RevenueCategoryResourceService,
                private auth: AuthService,
                private store: Store<AppState>) {
        this.form = FormConstructorService.build(FunctionEquipmentsSearchModel);
        this.itemsList = [];
        this.selectedServiceIds = this.store.select('functionsFormPage').map(equipmentIds('equipmentServiceId'));
        this.selectedPackageIds = this.store.select('functionsFormPage').map(equipmentIds('equipmentPackageId'));
    }

    ngOnInit() {
        this.formSubs = this.form.valueChanges.subscribe((val) => this.loadData(val.searchType));
        // this.loadData(this.form.value.searchType);

        this.selectedServiceIdsObs = this.selectedServiceIds.subscribe((ids) => {
            this.uncheckData(FunctionEquipmentsItemType.Equipments, ids);
        });

        this.selectedPackageIdsObs = this.selectedPackageIds.subscribe((ids) => {
            this.uncheckData(FunctionEquipmentsItemType.Packages, ids);
        });
    }

    private uncheckData(type: FunctionEquipmentsItemType, ids: any) {
        if (type === this.currentType) {
            this.itemsList.forEach((item: FunctionEquipmentsItem) => {
                if (ids[item.id]) {
                    item.selected = false;
                }
            });
        }
    }

    private loadData(type: FunctionEquipmentsItemType) {
        if (!this.form.value.search) {
            this.itemsList = [];
            return;
        }
        switch (type) {
            case FunctionEquipmentsItemType.Packages:
                this.loadPackages();
                break;
            case FunctionEquipmentsItemType.Equipments:
                this.loadEquipments();
                break;
        }
    }

    private loadEquipments() {
        this.promisedStore.currentVenueId()
            .then((venueId: number) => this.equipments.getAllByVenueId(venueId, this.form.value.search))
            .then(resp => mappingsBuildFrom(resp, FunctionEquipmentsItem, equipmentsMappingNamespace))
            .then((resp: any) => this.itemsList = resp);
    }

    private loadPackages() {
        this.promisedStore.currentVenueId()
            .then((venueId: number) => this.packages.getAllByVenueId(venueId, this.form.value.search))
            .then(resp => mappingsBuildFrom(resp, FunctionEquipmentsItem, packagesMappingNamespace))
            .then((resp: any) => this.itemsList = resp);
    }

    private generateDays(startDate: any, days: number) {
        const daysList = [];
        let i = 1;
        for (; i <= days; i++) {
            daysList.push(startDate.startOf('day').format('YYYY-MM-DD'));
            startDate = startDate.startOf('day').add(1, 'days').startOf('day');
        }
        return daysList;
    }

    async add() {
        const checked = this.itemsList.filter(item => item.selected);
        let result = mappingsBuildTo(checked, functionEquipmentModelMappingNamespace, FunctionEquipmentModel);


        const currentUser = await this.auth.currentUser();
        const subscriberId = currentUser.subscriberId;
        const categories = await this.revenueCategory.getAllBySubscriberId(subscriberId);
        const categoriesById = {};
        categories.forEach(c => {
            categoriesById[c.revenueCategoryId] = c;
        });

        console.log(result);
        result = result.map((item: any, index: number) => {
            if (this.currentType === FunctionEquipmentsItemType.Equipments) {
                item.equipmentServiceId = checked[index].id;
                if (categoriesById[checked[index].revenueCategoryId]) {
                    item.code = categoriesById[checked[index].revenueCategoryId].code;
                }
            } else {
                item.equipmentPackageId = checked[index].id;
            }

            let start = this.startDate;
            let end = this.endDate;

            if (start && end) {
                start = moment(start).startOf('day');
                end = moment(end).startOf('day');
                let dif = end.diff(start, 'days');
                dif++;
                item.eventFunctionEquipmentDates = this.generateDays(moment(start), dif).map((day, index2: number) => {
                    const dayModel = new FunctionEquipmentDateModel();
                    dayModel.quantity = 0;
                    dayModel.date = day;
                    dayModel.setupDay = this.functionDays[index2] && this.functionDays[index2].setupDay ? this.functionDays[index2].setupDay : false;
                    return dayModel;
                });
            } else {
                item.eventFunctionEquipmentDates = [];
            }
            return item;
        });
        console.log(result);
        // this.store.dispatch(new AddSelectedEquipments(result));
    }


    @UnsubscribePoint
    ngOnDestroy(): void {
    }
}
