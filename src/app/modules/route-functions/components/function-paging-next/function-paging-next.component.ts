import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AppState } from '../../../../store/store';
import { ActivatedRoute, Router } from '@angular/router';
import { EquipmentResourceService } from '../../../../services/resources/equipment-resource.service';
import { Store } from '@ngrx/store';
import { FunctionResourceService } from '../../../../services/resources/function-resource.service';
import { FunctionPagingCurrent } from '../../../../store/function-paging/function-paging.actions';
import { UtilsService } from '../../../../services/utils.service';

@Component({
    selector: 'app-function-paging-next',
    templateUrl: './function-paging-next.component.html',
    styleUrls: ['./function-paging-next.component.styl']
})
export class FunctionPagingNextComponent implements OnInit, OnDestroy {
    private sub: Subscription;
    private next: any;

    @Output('next')
    public onNext: EventEmitter<any> = new EventEmitter();

    constructor(
        private functionApi: FunctionResourceService,
        private store: Store<AppState>,
        private route: ActivatedRoute,
        private router: Router,
        private utils: UtilsService
    ) {
    }

    async ngOnInit() {
        this.sub = this.route.params.subscribe(
            () => this.loadNext()
        );
        this.loadNext();
    }

    ngOnDestroy() {
        if (this.sub) {
            this.sub.unsubscribe();
        }
    }

    private async loadNext() {
        this.next = await this.functionApi.getNextForPaging();
    }

    private async handle() {
        if (this.next) {
            this.store.dispatch(new FunctionPagingCurrent(this.next));
            this.router.navigateByUrl(`/u/functions`);
            await this.utils.resolveAfter(100);
            this.router.navigateByUrl(`/u/functions/${this.next.id}`);
        }
    }

    private goToNext() {
        if (this.next) {
            this.onNext.emit(this.handle.bind(this));

        }
    }
}
