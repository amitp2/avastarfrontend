import { Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';
import { Observable } from 'rxjs/Observable';
import { FunctionDay, FunctionEquipmentModel } from '../../../../models/function-model';
import { UnsubscribePoint } from '../../../../decorators/unsubscribe-point.decorator';
import { Subscription } from 'rxjs/Subscription';
import { ObservableSubscription } from '../../../../decorators/observable-subscription.decorator';
import { SetEquipments } from '../../../../store/functions-form-page/functions-form-page.actions';

@Component({
    selector: 'app-function-equipments',
    templateUrl: './function-equipments.component.html',
    styleUrls: ['./function-equipments.component.styl'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => FunctionEquipmentsComponent),
            multi: true
        }
    ]
})
export class FunctionEquipmentsComponent implements ControlValueAccessor, OnInit, OnDestroy {

    onChange: any;

    @Input() IsCodeValid: boolean

    @Input()
    startDate: any;

    @Input()
    endDate: any;

    @Input()
    days: FunctionDay[];

    selectedEquipmentsObs: Observable<FunctionEquipmentModel[]>;

    @ObservableSubscription
    selectedEquipmentsObsSubs: Subscription;

    shouldNotifyOutside = true;

    constructor(private store: Store<AppState>) {
        this.selectedEquipmentsObs = this.store.select('functionsFormPage')
            .map((data) => data.selectedEquipments);
    }

    ngOnInit() {
        this.selectedEquipmentsObsSubs = this.selectedEquipmentsObs.subscribe((data: FunctionEquipmentModel[]) => {
            if (this.shouldNotifyOutside) {
                if (this.onChange) {
                    this.onChange(data);
                } else {
                    console.log('change method hasnt registered yet');
                }
            }
            setTimeout(() => this.shouldNotifyOutside = true, 0);
        });
    }

    @UnsubscribePoint
    ngOnDestroy() {
    }

    writeValue(obj: any): void {
        if (obj && obj instanceof Array) {
            this.shouldNotifyOutside = false;
            obj = obj.map((entity) => {
               if (entity.service) {
                   // entity.name = entity.service.subCategoryItemName;
                   entity.name = entity.service.clientDescription;
               } else if (entity.equipment) {
                   entity.name = entity.equipment.name;
               }
               return entity;
            });
            this.store.dispatch(new SetEquipments(obj));
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
    }

    setDisabledState(isDisabled: boolean): void {
    }
}
