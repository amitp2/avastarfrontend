import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { VenuesComponent } from './components/venues/venues.component';
import { VenuesFormComponent } from './components/venues-form/venues-form.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VenueLogoUploaderService } from './services/venue-logo-uploader.service';
import { VenueContactsComponent } from './components/venue-contacts/venue-contacts.component';
import { VenueContactsFormComponent } from './components/venue-contacts-form/venue-contacts-form.component';
import { VenueSettingsFormComponent } from './components/venue-settings-form/venue-settings-form.component';
import { SelectModule } from '../select/select.module';
import { MaskedInputModule } from '../masked-input/masked-input.module';

const routes: Routes = [
    {path: '', pathMatch: 'full', component: VenuesComponent},
    {
        path: ':id',
        component: VenuesFormComponent,
        children: [
            {path: 'contact/:id', component: VenueContactsFormComponent}
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        PagingTableModule,
        RouterModule.forChild(routes),
        SelectModule,
        MaskedInputModule
    ],
    providers: [
        VenueLogoUploaderService
    ],
    declarations: [VenuesComponent, VenuesFormComponent, VenueContactsComponent, VenueContactsFormComponent, VenueSettingsFormComponent]
})
export class RouteVenuesModule {
}
