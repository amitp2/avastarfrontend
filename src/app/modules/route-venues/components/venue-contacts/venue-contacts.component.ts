import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { PagingTableService } from '../../../../services/paging-table.service';
import { UtilsService } from '../../../../services/utils.service';
import { VenueContactModel } from '../../../../models/venue-contact-model';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { VenueContactResourceService } from '../../../../services/resources/venue-contact-resource.service';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { TableHandler } from '../../../../classes/table-handler';
import { ActivatedRoute } from '@angular/router';
import { VenueModel } from '../../../../models/venue-model';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { VenueResourceService } from '../../../../services/resources/venue-resource.service';

@Component({
    selector: 'app-venue-contacts',
    templateUrl: './venue-contacts.component.html',
    styleUrls: ['./venue-contacts.component.styl'],
    providers: [PagingTableService]
})
export class VenueContactsComponent extends TableHandler {
    private _venueId: number;

    @Input() set venueId(value: number) {
        this._venueId = value;
        this.loadVenue();
    }

    get venueId() {
        return this._venueId;
    }

    private venue: VenueModel;

    @ViewChild('table') table: PagingTableComponent;

    columns: PagingTableHeaderColumn[] = [
        { isSortable: true, title: 'Name', key: 'firstName' },
        { isSortable: true, title: 'Direct Phone', key: 'phone' },
        { isSortable: true, title: 'Email', key: 'email' },
        { isSortable: true, title: 'Department', key: 'department' },
        { isSortable: false, title: '', key: 'owner' },
        { isSortable: false, title: 'Actions', key: 'actions' }
    ];

    private venueOwner: number;
    private venueOperator: number;

    constructor(
        private utilsService: UtilsService,
        private venueContactResourceService: VenueContactResourceService,
        private venueService: VenueResourceService,
        activatedRoute: ActivatedRoute
    ) {
        super(activatedRoute);
    }

    getTable(): PagingTableComponent {
        return this.table;
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return this.venueContactResourceService.queryByVenueId(this.utilsService.tableToHttpParams(params), this.venueId);
        }).bind(this);
    }

    async loadVenue() {
        this.venue = await this.venueService.get(this.venueId);
        this.venueOperator = this.venue.operatorContactId;
        this.venueOwner = this.venue.ownerContactId;
    }

    delete(venue: VenueContactModel) {
        this.venueContactResourceService
            .delete(venue.id)
            .then(() => {
                this.table.delete((u) => +u.id === +venue.id);
            });
    }

    isVenueOwner(record: VenueContactModel) {
        return this.venueOwner === record.id;
    }

    isVenueOperator(record: VenueContactModel) {
        return this.venueOperator === record.id;
    }

    @AsyncMethod({
        taskName: 'update_venue_owner'
    })
    async toggleVenueOwner(record: VenueContactModel) {
        if (this.isVenueOwner(record)) {
            this.setVenueOwner(undefined);
            await this.venueService.updateOwner(this.venueId, null);
        } else {
            this.setVenueOwner(record.id);
            await this.venueService.updateOwner(this.venueId, record.id);
        }
    }

    @AsyncMethod({
        taskName: 'update_venue_operator'
    })
    async toggleVenueOperator(record: VenueContactModel) {
        if (this.isVenueOperator(record)) {
            this.setVenueOperator(undefined);
            await this.venueService.updateOperator(this.venueId, null);
        } else {
            this.setVenueOperator(record.id);
            await this.venueService.updateOperator(this.venueId, record.id);
        }
    }

    private setVenueOperator(id: number | undefined) {
        this.venueOperator = id;
    }

    private setVenueOwner(id: number | undefined) {
        this.venueOwner = id;
    }
}
