import { Component, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FormMode } from '../../../../enums/form-mode.enum';
import { Subscription } from 'rxjs/Subscription';
import { VenueLogoUploaderService } from '../../services/venue-logo-uploader.service';
import { VenueResourceService } from '../../../../services/resources/venue-resource.service';
import { VenueModel } from '../../../../models/venue-model';
import { UploadType } from '../../../../enums/upload-type.enum';
import { SubscriberResourceService } from '../../../../services/resources/subscriber-resource.service';
import { ResourceQueryResponse } from '../../../../interfaces/resource-query-response';
import { SubscriberModel } from '../../../../models/subscriber-model';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { StateResourceService } from '../../../../services/resources/state-resource.service';
import { StateModel } from '../../../../models/state-model';
import { AuthService } from '../../../../services/auth.service';
import { CurrentUser } from '../../../../interfaces/current-user';
import { UtilsService } from '../../../../services/utils.service';
import { UserRole } from '../../../../enums/user-role.enum';
import { VenueSettingsResourceService } from '../../../../services/resources/venue-settings-resource.service';
import { VenueSettingsModel } from '../../../../models/venue-settings-model';
import { FormStatus } from '../../../../enums/form-status.enum';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../store/store';
import { VenueUpdated } from '../../../../store/current-venue/current-venue.actions';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { countryByCode, stateByCode, userLocation } from '../../../../store/selectors';
import { CountryModel } from '../../../../models/country-model';
import { GeolocationDataModel } from '../../../../models/geolocation-data.model';

@Component({
    selector: 'app-venues-form',
    templateUrl: './venues-form.component.html',
    styleUrls: ['./venues-form.component.styl']
})
export class VenuesFormComponent implements OnDestroy {

    form: FormGroup;
    formMode: FormMode;
    paramsSubscription: Subscription;
    uploader: any;
    subscribers: SubscriberModel[];
    id: any;
    selectedStateCode: string;
    userRole = UserRole;
    mode = FormMode;
    settings: VenueSettingsModel;
    settingsFormStatus: FormStatus;
    formStatus = FormStatus;
    SelectFetchType = SelectFetchType;

    constructor(
        private activatedRoute: ActivatedRoute,
        private venueLogoUploader: VenueLogoUploaderService,
        private venueResourceService: VenueResourceService,
        private router: Router,
        private subscriberResourceService: SubscriberResourceService,
        private stateResourceService: StateResourceService,
        private authService: AuthService,
        private utilsService: UtilsService,
        private venueSettingsResourceService: VenueSettingsResourceService,
        private store: Store<AppState>
    ) {
        this.formMode = FormMode.Add;
        this.uploader = this.venueLogoUploader;
        this.settingsFormStatus = FormStatus.Valid;
        this.form = new FormGroup({
            name: new FormControl(null, [Validators.required]),
            subscriberId: new FormControl(),
            countryId: new FormControl(233, [Validators.required]),
            stateId: new FormControl(),
            city: new FormControl(null, [Validators.required]),
            address: new FormControl(null, [Validators.required]),
            address2: new FormControl(),
            postCode: new FormControl(null, [Validators.required, Validators.maxLength(10)]),
            mainPhone: new FormControl(),
            mainFax: new FormControl(),
            website: new FormControl(),
            logoUrl: new FormControl(),
            owner: new FormControl(null),
            operator: new FormControl(null)
        });

        this.form.get('countryId').valueChanges.subscribe((val: number) => {
            if (+val === 233) {
                this.form.get('stateId').enable();
            } else {
                this.form.get('stateId').disable();
            }
        });

        this.paramsSubscription = this.activatedRoute.params.subscribe((params: any) => {
            this.id = params.id;
            if (params.id === 'add') {
                this.formMode = FormMode.Add;
                this.populateForm();
            } else {
                this.formMode = FormMode.Edit;
                this.form.get('subscriberId').disable();
                this.venueResourceService
                    .get(+this.id)
                    .then((venue: VenueModel) => {
                        this.uploader.uploadType = UploadType.Update;
                        this.uploader.oldUrl = venue.logoUrl;
                        const formValue: any = { ...venue };
                        delete formValue.id;
                        this.form.patchValue(formValue);
                    });
            }
        });

        this.form.get('stateId').valueChanges.subscribe((id: number) => {
            this.stateResourceService.get(+id).then((state: StateModel) => {
                this.selectedStateCode = state.code;
            }).catch(() => {
            });
        });


        this.authService.currentUser().then((user: CurrentUser) => {
            if (this.utilsService.userHasRole(user, UserRole.SUPER_ADMIN)) {
                this.subscriberResourceService.query({
                    page: 0,
                    size: 1000,
                    sortFields: ['name'],
                    sortAsc: true
                }).then((resp: ResourceQueryResponse<SubscriberModel>) => {
                    this.subscribers = resp.items;
                    if (this.formMode === FormMode.Add && resp.items.length > 0) {
                        this.form.get('subscriberId').setValue(resp.items[0].id);
                    }
                });
                return;
            }

            this.form.get('subscriberId').setValue(user.subscriberId);
        });
    }

    submit() {
        if (this.formMode === FormMode.Add) {
            this.add(new VenueModel(this.form.value));
        } else {
            this.update(new VenueModel(this.form.value));
        }
    }

    private async populateForm() {
        const location: GeolocationDataModel = await this.utilsService.toPromise(this.store.select(userLocation()));
        if (location.countryCode) {
            const country: CountryModel = await this.utilsService.toPromise(this.store.select(countryByCode(location.countryCode)));
            if (country) {
                this.form.get('countryId').setValue(country.id);
            }
        }
        // if (location.regionCode) {
        //     const state: StateModel = await this.utilsService.toPromise(this.store.select(stateByCode(location.regionCode)));
        //     if (state) {
        //         this.form.get('stateId').setValue(state.id);
        //     }
        // }
        // if (location.city) {
        //     this.form.get('city').setValue(location.city);
        // }
        // if (location.zipCode) {
        //     this.form.get('postCode').setValue(location.zipCode);
        // }
        this.form.markAsPristine();
    }


    @AsyncMethod({
        taskName: 'venue_add',
        success: AppSuccess.VENUE_ADD,
        error: AppError.VENUE_ADD
    })
    add(model: VenueModel) {
        return this.venueResourceService
            .add(model)
            .then((venue: VenueModel) => this.router.navigateByUrl(`/u/venues/${venue.id}`));
    }

    @AsyncMethod({
        taskName: 'venue_update',
        success: AppSuccess.VENUE_UPDATE,
        error: AppError.VENUE_UPDATE
    })
    update(model: VenueModel) {
        return Promise.all([
            this.venueResourceService.edit(+this.id, model),
            this.settings ? this.venueSettingsResourceService.updateForVenue(this.settings, +this.id) : Promise.resolve(null)
        ]).then(() => {
            model.id = +this.id;
            this.store.dispatch(new VenueUpdated(model));
        });
    }

    settingsStatusChanged(status: FormStatus) {
        this.settingsFormStatus = status;
    }

    settingsUpdated(model: VenueSettingsModel) {
        this.settings = model;
    }

    ngOnDestroy() {
        this.paramsSubscription.unsubscribe();
    }

}
