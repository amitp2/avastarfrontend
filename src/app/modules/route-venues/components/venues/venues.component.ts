import { Component, ViewChild } from '@angular/core';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { UtilsService } from '../../../../services/utils.service';
import { VenueResourceService } from '../../../../services/resources/venue-resource.service';
import { VenueModel } from '../../../../models/venue-model';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { PagingTableService } from '../../../../services/paging-table.service';
import { UserRole } from '../../../../enums/user-role.enum';

@Component({
    selector: 'app-venues',
    templateUrl: './venues.component.html',
    styleUrls: ['./venues.component.styl'],
    providers: [PagingTableService]
})
export class VenuesComponent {

    @ViewChild('table') table: PagingTableComponent;

    columns: PagingTableHeaderColumn[] = [
        {isSortable: true, title: 'Name', key: 'name'},
        {isSortable: false, title: 'Actions', key: 'actions'}
    ];

    userRole = UserRole;

    constructor(private utilsService: UtilsService,
                private venueResourceService: VenueResourceService) {
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return this.venueResourceService.query(this.utilsService.tableToHttpParams(params));
        }).bind(this);
    }

    delete(venue: VenueModel) {
        this.venueResourceService
            .delete(venue.id)
            .then(() => {
                this.table.delete((u) => +u.id === +venue.id);
            });
    }

}
