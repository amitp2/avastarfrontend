import { Component } from '@angular/core';
import { VenueContactModel } from '../../../../models/venue-contact-model';
import { FormHandler } from '../../../../classes/form-handler';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserRole } from '../../../../enums/user-role.enum';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { VenueContactResourceService } from '../../../../services/resources/venue-contact-resource.service';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';

@Component({
    selector: 'app-venue-contacts-form',
    templateUrl: './venue-contacts-form.component.html',
    styleUrls: ['./venue-contacts-form.component.styl']
})
export class VenueContactsFormComponent extends FormHandler<VenueContactModel> {

    private currentVenueId: number;
    userRole = UserRole;
    visible: boolean;

    constructor(private activatedRoute: ActivatedRoute,
                private venueContactResourceService: VenueContactResourceService,
                private router: Router) {
        super();
        this.visible = false;
        this.activatedRoute.parent.params.subscribe((params: any) => this.currentVenueId = +params.id);
        setTimeout(() => this.visible = true, 200);
    }

    initializeForm(): FormGroup {
        return new FormGroup({
            firstName: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            lastName: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            title: new FormControl(null, [Validators.maxLength(100)]),
            email: new FormControl(null, [Validators.required, Validators.email]),
            mobile: new FormControl(null, [Validators.maxLength(30)]),
            phone: new FormControl(null, [Validators.required, Validators.maxLength(30)]),
            role: new FormControl(UserRole.NONE, [Validators.required]),
            notes: new FormControl(null, [Validators.maxLength(400)]),
            fax: new FormControl(null, [Validators.maxLength(30)]),
            type: new FormControl(null, [Validators.maxLength(100)]),
            department: new FormControl(null, [Validators.maxLength(250)])
        });
    }

    entityId(): Observable<number> {
        return this
            .activatedRoute
            .params
            .map((params: any) => params.id);
    }

    formMode(): Observable<FormMode> {
        return this
            .activatedRoute
            .params
            .map((params: any) => params.id === 'add' ? FormMode.Add : FormMode.Edit);
    }

    @AsyncMethod({
        taskName: 'ADD_VENUE_CONTACT',
        success: AppSuccess.ADD_VENUE_CONTACT,
        error: AppError.ADD_VENUE_CONTACT
    })
    add(model: VenueContactModel): Promise<any> {
        return this.venueContactResourceService
            .addForVenue(model, this.currentVenueId)
            .then(() => this.router.navigateByUrl(`/u/venues/${this.currentVenueId}?fresh=${Date.now()}`));
    }

    @AsyncMethod({
        taskName: 'LOAD_VENUE_CONTACT',
        success: AppSuccess.VENUE_CONTACT_LOAD,
        error: AppError.VENUE_CONTACT_LOAD
    })
    get(id: number): Promise<VenueContactModel> {
        return this.venueContactResourceService.get(id);
    }

    @AsyncMethod({
        taskName: 'EDIT_VENUE_CONTACT',
        success: AppSuccess.EDIT_VENUE_CONTACT,
        error: AppError.EDIT_VENUE_CONTACT
    })
    edit(id: number, model: VenueContactModel): Promise<any> {
        return this.venueContactResourceService
            .edit(id, model)
            .then(() => this.router.navigateByUrl(`/u/venues/${this.currentVenueId}?fresh=${Date.now()}`));
    }

    serialize(model: VenueContactModel): any {
        const serialized: any = {
            ...model
        };
        delete serialized.id;
        return serialized;
    }

    deserialize(formValue: any): VenueContactModel {
        return new VenueContactModel({
            ...formValue,
            role: +formValue.role
        });
    }

    backLink() {
        return `/u/venues/${this.currentVenueId}`;
    }

}
