import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ServiceCharge } from '../../../../enums/service-charge.enum';
import { VenueSettingsModel } from '../../../../models/venue-settings-model';
import { VenueSettingsResourceService } from '../../../../services/resources/venue-settings-resource.service';
import { Subscription } from 'rxjs/Subscription';
import { FormStatus } from '../../../../enums/form-status.enum';
import { BillingFormat } from '../../../../enums/billing-format.enum';
import { EventType } from '../../../../enums/event-type.enum';
import { SelectFetchType } from '../../../select/types/select-fetch-type';
import { PurchaseOrderService } from '../../../../services/resources/purchase-order.service';
import { FunctionResourceService } from '../../../../services/resources/function-resource.service';

@Component({
    selector: 'app-venue-settings-form',
    templateUrl: './venue-settings-form.component.html',
    styleUrls: ['./venue-settings-form.component.styl']
})
export class VenueSettingsFormComponent implements OnInit, OnDestroy {

    @Input() venueId: number;
    @Output() settingsUpdated: EventEmitter<VenueSettingsModel>;
    @Output() submitted: EventEmitter<any>;
    @Output() statusChanged: EventEmitter<FormStatus>;

    form: FormGroup;
    formSubs: Subscription;
    serviceCharge = ServiceCharge;
    billingFormat = BillingFormat;
    eventType = EventType;
    SelectFetchType = SelectFetchType;

    constructor(private venueSettings: VenueSettingsResourceService,
                private purchaseOrderApi: PurchaseOrderService,
                private functionResource: FunctionResourceService) {
        this.settingsUpdated = new EventEmitter<VenueSettingsModel>();
        this.submitted = new EventEmitter<any>();
        this.statusChanged = new EventEmitter<any>();

        this.form = new FormGroup({
            billingFormat: new FormControl(BillingFormat.DAILY, [Validators.required]),
            currency: new FormControl('usd', [Validators.required]),
            serviceChargeDefault: new FormControl(ServiceCharge.PreDiscount),
            laborSummaryIncluded: new FormControl(false),
            sortedByCategory: new FormControl(false),
            revisionDateShown: new FormControl(false),
            teo: new FormControl(),
            teoStart: new FormControl(),
            po: new FormControl(),
            poStart: new FormControl(),
            finYearFrom: new FormControl(1),
            finYearTo: new FormControl(1),
            reportDisclaimer: new FormControl(),
            emailDisclaimer: new FormControl(),
            eventType: new FormControl(null, [Validators.required])
        });

        this.form.statusChanges.subscribe(() => {
            if (this.form.valid) {
                this.statusChanged.emit(FormStatus.Valid);
                return;
            }
            this.statusChanged.emit(FormStatus.Invalid);
        });

        this.formSubs = this.form.valueChanges.subscribe(() => {
            this.update();
        });
    }

    ngOnInit() {
        this.checkFunctionsForTEONumber();
        this.checkPurchaseOrdersForPONumber();

        this.statusChanged.emit(FormStatus.Invalid);
        this.venueSettings
            .getForVenue(this.venueId)
            .then((settings: VenueSettingsModel) => {
                const formVal: any = {
                    ...settings
                };
                delete formVal.id;
                // delete formVal.eventType;

                const deserializeEventType = {};
                deserializeEventType[1] = EventType.Group;
                deserializeEventType[2] = EventType.Local;
                deserializeEventType[3] = EventType.Internal;

                formVal.eventType = deserializeEventType[formVal.eventType];
                this.form.setValue(formVal);
            });
    }

    async checkPurchaseOrdersForPONumber() {
        const purchaseOrders = await this.purchaseOrderApi.getAllByVenueId(this.venueId);

        if (purchaseOrders.length > 0) {
            this.form.get('poStart').disable();
        }
    }

    async checkFunctionsForTEONumber() {
        const functions = await this.functionResource.getAllByVenueId(this.venueId);

        if (functions.length > 0) {
            this.form.get('teoStart').disable();
        }
    }

    ngOnDestroy() {
        this.formSubs.unsubscribe();
    }

    submit() {
        this.update();
        this.submitted.emit();
    }

    update() {

        const serializeEventType = {};
        serializeEventType[EventType.Group] = 1;
        serializeEventType[EventType.Local] = 2;
        serializeEventType[EventType.Internal] = 3;

        const modelVal: any = {
            ...this.form.value,
            serviceChargeDefault: +this.form.value.serviceChargeDefault,
            eventType: serializeEventType[this.form.value.eventType]
        };

        // Group = 'Group',
        //     Local = 'Local',
        //     Internal = 'Internal'

        this.settingsUpdated.emit(new VenueSettingsModel(modelVal));
    }

}
