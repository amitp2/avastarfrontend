import { Component, OnInit } from '@angular/core';
import { TaxChargeModel } from '../../../../models/tax-charge-model';
import { FormHandler } from '../../../../classes/form-handler';
import { RevenueCategoryModel } from '../../../../models/revenue-category-model';
import { ActivatedRoute, Router } from '@angular/router';
import { TaxChargeResourceService } from '../../../../services/resources/tax-charge-resource.service';
import { RevenueCategoryResourceService } from '../../../../services/resources/revenue-category-resource.service';
import { AuthService } from '../../../../services/auth.service';
import { UtilsService } from '../../../../services/utils.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError, AppErrorDefaultMessages } from '../../../../enums/app-error.enum';
import { TaxChargeStatus } from '../../../../enums/tax-charge.enum';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-tax-charge-form',
    templateUrl: './tax-charge-form.component.html',
    styleUrls: ['./tax-charge-form.component.styl']
})
export class TaxChargeFormComponent extends FormHandler<TaxChargeModel> {

    revenueCategoryList: RevenueCategoryModel[] = [];
    taxChargeStatus: typeof TaxChargeStatus = TaxChargeStatus;

    constructor(private activatedRoute: ActivatedRoute,
                private taxCharge: TaxChargeResourceService,
                private revenueCategory: RevenueCategoryResourceService,
                private authService: AuthService,
                private router: Router,
                private utils: UtilsService,
                private toastrService: ToastrService,
                fb: FormBuilder) {
        super(fb);

        this.loadRevenueCategories();
    }

    loadRevenueCategories() {
        const httpQueryParams = {
            page: 0,
            size: 999999,
            sortFields: [],
            sortAsc: true
        };

        this.revenueCategory.queryBySubscriberId(httpQueryParams, this.authService.currentUserSnapshot.subscriberId)
            .then((response) => this.revenueCategoryList = response.items.filter(x => x.revenueCode !== null));
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    initializeForm(): FormGroup {
        const form = this.formBuilder.group({
            name: ['', [Validators.required]],
            charge: ['', [Validators.required, Validators.min(0), Validators.max(100)]],
            startDate: ['', Validators.required],
            endDate: ['', [Validators.required]],
            revenueCategoryId: ['', []],
            status: ['', [Validators.required]]
        });

        form.get('status').disable();
        return form;
    }

    @AsyncMethod({
        taskName: 'get_tax_charge'
    })
    get(id: number): Promise<TaxChargeModel> {
        return this.taxCharge.get(id);
    }

    @AsyncMethod({
        taskName: 'add_tax_charge',
        success: AppSuccess.TAX_CHARGE_ADD
    })
    add(model: TaxChargeModel): Promise<any> {
        return this.taxCharge.addBySubscriberId(model, this.authService.currentUserSnapshot.subscriberId)
            .then(() => this.router.navigateByUrl(`/u/tax-charge?fresh=${Date.now()}`))
            .catch(err => {
                if (err.status === 409) {
                    throw AppError.TAX_CHARGE_DATES;
                }
                throw AppError.TAX_CHARGE_ADD;
            });
    }

    @AsyncMethod({
        taskName: 'update_tax_charge',
        success: AppSuccess.TAX_CHARGE_EDIT
    })
    edit(id: number, model: TaxChargeModel): Promise<any> {
        return this.taxCharge.edit(id, model)
            .then(() => this.router.navigateByUrl(`/u/tax-charge?fresh=${Date.now()}`))
            .catch(err => {
                if (err.status === 409) {
                    throw AppError.TAX_CHARGE_DATES;
                }
                throw AppError.TAX_CHARGE_EDIT;
            });
    }

    submit() {
        const startDate = new Date(this.form.get('startDate').value);
        const endDate = new Date(this.form.get('endDate').value);
        if (startDate > endDate) {
            this.toastrService.error(AppErrorDefaultMessages[AppError.ENDING_DATE], 'Error', {
                positionClass: 'toast-bottom-right',
                tapToDismiss: false
            });
            return;
        }
        super.submit();
    }

    serialize(model: TaxChargeModel): any {
        return this.utils.transformInto(model, {
            name: {to: 'name'},
            charge: {to: 'charge'},
            status: {to: 'status'},
            endDate: {to: 'endDate'},
            startDate: {to: 'startDate'},
            revenueCategoryId: {to: 'revenueCategoryId'}
        });
    }

    deserialize(formValue: any): TaxChargeModel {
        return new TaxChargeModel(this.utils.transformInto(formValue, {
            name: {to: 'name'},
            charge: {to: 'charge'},
            status: {to: 'status'},
            endDate: {to: 'endDate'},
            startDate: {to: 'startDate'},
            revenueCategoryId: {to: 'revenueCategoryId'}
        }));
    }

}
