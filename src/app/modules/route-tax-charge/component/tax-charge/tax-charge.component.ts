import { Component, OnInit, ViewChild } from '@angular/core';
import { TaxChargeResourceService } from '../../../../services/resources/tax-charge-resource.service';
import { TableHandler } from '../../../../classes/table-handler';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { AuthService } from '../../../../services/auth.service';
import { UtilsService } from '../../../../services/utils.service';
import { ActivatedRoute } from '@angular/router';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { PagingTableLoadResult } from '../../../../interfaces/paging-table-load-result';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { TaxChargeModel } from '../../../../models/tax-charge-model';

@Component({
    selector: 'app-tax-charge',
    templateUrl: './tax-charge.component.html',
    styleUrls: ['./tax-charge.component.styl']
})
export class TaxChargeComponent extends TableHandler {

    @ViewChild('table') table: PagingTableComponent;

    constructor(
        private taxCharge: TaxChargeResourceService,
        private authService: AuthService,
        private utils: UtilsService,
        activatedRoute: ActivatedRoute
    ) {
        super(activatedRoute);
    }

    getTable(): PagingTableComponent {
        return this.table;
    }

    loadFunction(): any {
        return ((params: PagingTableLoadParams): Promise<PagingTableLoadResult> => {
            return this.taxCharge.queryBySubscriberId(
                this.utils.tableToHttpParams(params),
                this.authService.currentUserSnapshot.subscriberId
            );
        }).bind(this);
    }

    @AsyncMethod({
        taskName: 'delete_tax_charge'
    })
    remove(item: TaxChargeModel) {
        if (confirm('Do you want to delete tax charge?')) {
            return this.taxCharge.delete(item.id).then(() => {
                this.getTable().refresh();
            });
        }
        return Promise.resolve();
    }

}
