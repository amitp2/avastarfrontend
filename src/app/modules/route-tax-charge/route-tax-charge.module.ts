import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { TaxChargeComponent } from './component/tax-charge/tax-charge.component';
import { TaxChargeFormComponent } from './component/tax-charge-form/tax-charge-form.component';
import { TaxChargeStatusTextPipe } from './pipes/tax-charge-status-text.pipe';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { RevenueTextPipe } from './pipes/revenue-text.pipe';
import { AllowOnlyService } from '../../services/guards/allow-only.service';
import { UserRole } from '../../enums/user-role.enum';
import { TableResizeModule } from '../table-resize/table-resize.module';

const routes: Routes = [
    {
        path: '',
        component: TaxChargeComponent,
        canActivate: [AllowOnlyService],
        data: {
            roles: [UserRole.ADMIN]
        },
        children: [
            {path: ':id', component: TaxChargeFormComponent}
        ]
    }
];


@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        TableResizeModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        PagingTableModule
    ],
    declarations: [TaxChargeComponent, TaxChargeFormComponent, TaxChargeStatusTextPipe, RevenueTextPipe]
})
export class RouteTaxChargeModule {
}
