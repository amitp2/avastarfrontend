import { Pipe, PipeTransform } from '@angular/core';
import { TaxChargeStatus } from '../../../enums/tax-charge.enum';
import { TaxChargeModel } from '../../../models/tax-charge-model';

@Pipe({
    name: 'taxChargeStatusText'
})
export class TaxChargeStatusTextPipe implements PipeTransform {

    transform(status: TaxChargeStatus, taxCharge: TaxChargeModel): string {
        if (status === TaxChargeStatus.Inactive) {
            return 'Inactive';
        }

        const endDate = new Date(taxCharge.endDate);
        const today = new Date();

        today.setMilliseconds(0);
        today.setMinutes(0);
        today.setHours(0);

        return endDate < today ? 'Expired' : 'Active';
    }
}
