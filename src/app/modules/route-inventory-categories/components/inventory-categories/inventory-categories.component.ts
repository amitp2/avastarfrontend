import { Component, OnInit, ViewChild } from '@angular/core';
import { PagingTableComponent } from '../../../paging-table/components/paging-table/paging-table.component';
import { PagingTableHeaderColumn } from '../../../../interfaces/paging-table-header-column';
import { InventoryCategoryResourceService } from '../../../../services/resources/inventory-category-resource.service';
import { PagingTableLoadParams } from '../../../../interfaces/paging-table-load-params';
import { UtilsService } from '../../../../services/utils.service';
import { TableHandler } from '../../../../classes/table-handler';
import { ActivatedRoute } from '@angular/router';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { InventoryCategoryModel } from '../../../../models/inventory-category-model';
import { InventorySubCategoryModel } from '../../../../models/inventory-sub-category-model';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { InventorySubcategoryItemModel } from '../../../../models/inventory-subcategory-item-model';
import { PagingTableService } from '../../../../services/paging-table.service';

@Component({
    selector: 'app-inventory-categories',
    templateUrl: './inventory-categories.component.html',
    styleUrls: ['./inventory-categories.component.styl'],
    providers: [PagingTableService]
})
export class InventoryCategoriesComponent extends TableHandler {
    @ViewChild('table') table: PagingTableComponent;

    expandedCategories = new Set();
    expandedSubCategories = new Set();

    columns: PagingTableHeaderColumn[] = [
        { isSortable: true, title: 'Category | Sub Category | Equipment/Service Item', key: 'name' },
        { isSortable: false, title: 'Actions', key: 'actions' }
    ];

    constructor(private inventoryCategoryApi: InventoryCategoryResourceService, private utils: UtilsService, activatedRoute: ActivatedRoute) {
        super(activatedRoute);
    }

    loadFunction = (params: PagingTableLoadParams) => {
        this.expandedCategories.clear();
        this.expandedSubCategories.clear();
        return this.inventoryCategoryApi.queryForTable(this.utils.tableToHttpParams(params));
    }

    refresh = () => {
        this.inventoryCategoryApi.clearCache();
        this.getTable().refresh();
    }

    getTable(): PagingTableComponent {
        return this.table;
    }

    @AsyncMethod({
        taskName: 'delete_category',
        error: AppError.INVENTORY_CATEGORY_DELETE
    })
    delete(category: InventoryCategoryModel) {
        return this.inventoryCategoryApi.delete(category.id).then(this.refresh);
    }

    @AsyncMethod({
        taskName: 'delete_sub_category',
        error: AppError.INVENTORY_SUB_CATEGORY_DELETE
    })
    deleteSubCategory(category: InventorySubCategoryModel) {
        return this.inventoryCategoryApi.deleteSubCategory(category.id).then(this.refresh);
    }

    @AsyncMethod({
        taskName: 'delete_sub_category',
        error: AppError.INVENTORY_SUB_CATEGORY_ITEM_DELETE
    })
    deleteSubCategoryItem(category: InventorySubcategoryItemModel) {
        return this.inventoryCategoryApi.deleteSubCategoryItem(category.id).then(this.refresh);
    }

    @AsyncMethod({
        taskName: 'expand_category'
    })
    async toggleCategory(category: InventoryCategoryModel) {
        if (this.expandedCategories.has(category.id)) {
            this.expandedCategories.delete(category.id);
        } else {
            const subcategories = await this.inventoryCategoryApi.getAllSubcategories(category.id);
            category['subcategories'] = subcategories;
            this.expandedCategories.add(category.id);
        }
    }

    @AsyncMethod({
        taskName: 'expand_category'
    })
    async toggleSubCategory(category: InventorySubCategoryModel) {
        if (this.expandedSubCategories.has(category.id)) {
            category['expanded'] = false;
            this.expandedSubCategories.delete(category.id);
        } else {
            const items = await this.inventoryCategoryApi.getAllSubcategoryItems(category.id);
            category['items'] = items;
            category['expanded'] = true;
            this.expandedSubCategories.add(category.id);
        }
    }

    isCategoryExpanded(id: number) {
        return this.expandedCategories.has(id);
    }

    isSubCategoryExpanded(id: number) {
        return this.expandedSubCategories.delete(id);
    }

}
