import { Component, OnInit } from '@angular/core';
import { FormHandler } from '../../../../classes/form-handler';
import { InventoryCategoryResourceService } from '../../../../services/resources/inventory-category-resource.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormMode } from '../../../../enums/form-mode.enum';
import { Observable } from 'rxjs/Observable';
import { InventorySubCategoryModel } from '../../../../models/inventory-sub-category-model';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';

@Component({
    selector: 'app-inventory-sub-category-form',
    templateUrl: './inventory-sub-category-form.component.html',
    styleUrls: ['./inventory-sub-category-form.component.styl']
})
export class InventorySubCategoryFormComponent extends FormHandler<InventorySubCategoryModel> {
    constructor(
        formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private categoryApi: InventoryCategoryResourceService
    ) {
        super(formBuilder);
    }

    entityId(): Observable<number> {
        return this.paramsId(this.route);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.route);
    }

    initializeForm(): FormGroup {
        return this.formBuilder.group({
            name: [null, Validators.required]
        });
    }

    get(id: number): Promise<InventorySubCategoryModel> {
        return this.categoryApi.getSubCategory(id);
    }

    @AsyncMethod({
        taskName: 'add_sub_category',
        success: AppSuccess.INVENTORY_SUB_CATEGORY_ADD,
        error: AppError.INVENTORY_SUB_CATEGORY_ADD
    })
    add(model: InventorySubCategoryModel): Promise<any> {
        model.categoryId = this.route.snapshot.paramMap.get('categoryId') as any;

        return this.categoryApi.addSubCategory(model)
            .then(() => this.router.navigateByUrl('/u/category-management?fresh=' + Date.now()));
    }

    @AsyncMethod({
        taskName: 'edit_sub_category',
        success: AppSuccess.INVENTORY_SUB_CATEGORY_EDIT,
        error: AppError.INVENTORY_SUB_CATEGORY_EDIT
    })
    edit(id: number, model: InventorySubCategoryModel): Promise<any> {
        return this.categoryApi.editSubCategory(id, model)
            .then(() => this.router.navigateByUrl('/u/category-management?fresh=' + Date.now()));
    }

    serialize(model: InventorySubCategoryModel): any {
        return model.toJson();
    }

    deserialize(formValue: any): InventorySubCategoryModel {
        return InventorySubCategoryModel.fromJson(formValue);
    }
}

