import { Component, OnInit } from '@angular/core';
import { FormHandler } from '../../../../classes/form-handler';
import { InventoryCategoryModel } from '../../../../models/inventory-category-model';
import { InventoryCategoryResourceService } from '../../../../services/resources/inventory-category-resource.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../../../../enums/form-mode.enum';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';

@Component({
    selector: 'app-inventory-category-form',
    templateUrl: './inventory-category-form.component.html',
    styleUrls: ['./inventory-category-form.component.styl']
})
export class InventoryCategoryFormComponent extends FormHandler<InventoryCategoryModel> {
    constructor(
        formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private categoryApi: InventoryCategoryResourceService
    ) {
        super(formBuilder);
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute);
    }

    initializeForm(): FormGroup {
        return this.formBuilder.group({
            name: [null, Validators.required]
        });
    }

    @AsyncMethod({
        taskName: 'get_inventory_category'
    })
    get(id: number): Promise<InventoryCategoryModel> {
        return this.categoryApi.get(id);
    }

    @AsyncMethod({
        taskName: 'add_inventory_category',
        success: AppSuccess.INVENTORY_CATEGORY_ADD,
        error: AppError.INVENTORY_CATEGORY_ADD
    })
    add(model: InventoryCategoryModel): Promise<any> {
        return this.categoryApi.add(model)
            .then(() => this.router.navigateByUrl('/u/category-management?fresh=' + Date.now()));
    }

    @AsyncMethod({
        taskName: 'edit_inventory_category',
        success: AppSuccess.INVENTORY_CATEGORY_EDIT,
        // error: AppError.INVENTORY_CATEGORY_EDIT
    })
    edit(id: number, model: InventoryCategoryModel): Promise<any> {
        return new Promise((resolve, reject) => {
             this.categoryApi.edit(id, model)
                .then(() => resolve(this.router.navigateByUrl('/u/category-management?fresh=' + Date.now())))
                .catch(ex => reject(ex.error && ex.error.message));
        });
    }

    serialize(model: InventoryCategoryModel): any {
        return model.toJson();
    }

    deserialize(formValue: any): InventoryCategoryModel {
        return InventoryCategoryModel.fromJson(formValue);
    }
}
