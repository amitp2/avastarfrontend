import { Component, OnInit } from '@angular/core';
import { FormHandler } from '../../../../classes/form-handler';
import { InventoryCategoryResourceService } from '../../../../services/resources/inventory-category-resource.service';
import { InventoryCategoryModel } from '../../../../models/inventory-category-model';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormMode } from '../../../../enums/form-mode.enum';
import { Observable } from 'rxjs/Observable';
import { InventorySubcategoryItemModel } from '../../../../models/inventory-subcategory-item-model';
import { AsyncMethod } from '../../../../decorators/async-method.decorator';
import { AppSuccess } from '../../../../enums/app-success.enum';
import { AppError } from '../../../../enums/app-error.enum';

@Component({
    selector: 'app-inventory-sub-category-item-form',
    templateUrl: './inventory-sub-category-item-form.component.html',
    styleUrls: ['./inventory-sub-category-item-form.component.styl']
})
export class InventorySubCategoryItemFormComponent extends FormHandler<InventorySubcategoryItemModel> {
    constructor(
        formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private categoryApi: InventoryCategoryResourceService
    ) {
        super(formBuilder);
    }

    entityId(): Observable<number> {
        return this.paramsId(this.route);
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.route);
    }

    initializeForm(): FormGroup {
        return this.formBuilder.group({
            name: [null, Validators.required]
        });
    }

    get(id: number): Promise<InventorySubcategoryItemModel> {
        return this.categoryApi.getSubCategoryItem(id);
    }

    @AsyncMethod({
        taskName: 'add_sub_category_item',
        success: AppSuccess.INVENTORY_SUB_CATEGORY_ITEM_ADD,
        error: AppError.INVENTORY_SUB_CATEGORY_ITEM_ADD
    })
    add(model: InventorySubcategoryItemModel): Promise<any> {
        model.subCategoryId = this.route.snapshot.paramMap.get('subCategoryId') as any;

        return this.categoryApi.addSubCategoryItem(model)
            .then(() => this.router.navigateByUrl('/u/category-management?fresh=' + Date.now()));
    }

    @AsyncMethod({
        taskName: 'edit_sub_category_item',
        success: AppSuccess.INVENTORY_SUB_CATEGORY_ITEM_EDIT,
        error: AppError.INVENTORY_SUB_CATEGORY_ITEM_EDIT
    })
    edit(id: number, model: InventorySubcategoryItemModel): Promise<any> {
        return this.categoryApi.editSubCategoryItem(id, model)
            .then(() => this.router.navigateByUrl('/u/category-management?fresh=' + Date.now()));
    }

    serialize(model: InventorySubcategoryItemModel): any {
        return model.toJson();
    }

    deserialize(formValue: any): InventorySubcategoryItemModel {
        return InventorySubcategoryItemModel.fromJson(formValue);
    }
}

