import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InventoryCategoriesComponent } from './components/inventory-categories/inventory-categories.component';
import { InventoryCategoryFormComponent } from './components/inventory-category-form/inventory-category-form.component';
import { InventorySubCategoryFormComponent } from './components/inventory-sub-category-form/inventory-sub-category-form.component';
import { InventorySubCategoryItemFormComponent } from './components/inventory-sub-category-item-form/inventory-sub-category-item-form.component';
import { SharedModule } from '../shared/shared.module';
import { Route, RouterModule } from '@angular/router';
import { PagingTableModule } from '../paging-table/paging-table.module';
import { ReactiveFormsModule } from '@angular/forms';

const routes: Route[] = [
    {
        path: '',
        component: InventoryCategoriesComponent,
        children: [
            { path: ':id', component: InventoryCategoryFormComponent },
            { path: ':categoryId/subcategories/:id', component: InventorySubCategoryFormComponent },
            { path: ':categoryId/subcategories/:subCategoryId/items/:id', component: InventorySubCategoryItemFormComponent }
        ]
    },
];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        PagingTableModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        InventoryCategoriesComponent,
        InventoryCategoryFormComponent,
        InventorySubCategoryFormComponent,
        InventorySubCategoryItemFormComponent
    ]
})
export class RouteInventoryCategoriesModule {
}
