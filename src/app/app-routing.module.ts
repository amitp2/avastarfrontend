import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VisitorComponent } from './routes/visitor/visitor.component';
import { UserComponent } from './routes/user/user.component';
import { VisitorOnlyGuardService } from './services/guards/visitor-only-guard.service';
import { UserOnlyGuardService } from './services/guards/user-only-guard.service';
import { LoginComponent } from './routes/visitor/login/login.component';
import { ForgotComponent } from './routes/visitor/forgot/forgot.component';
import { ResetComponent } from './routes/visitor/reset/reset.component';
import { DashboardComponent } from './routes/user/dashboard/dashboard.component';
import { VerificationComponent } from './routes/visitor/verification/verification.component';
import { AllowOnlyService } from './services/guards/allow-only.service';
import { UserRole } from './enums/user-role.enum';
import { CurrentUserGuardService } from './services/guards/current-user-guard.service';
import { RouteTicketsModule } from './modules/route-tickets/route-tickets.module';
import { RouteEventManagementModule } from './modules/route-event-management/route-event-management.module';
import { RouteInventoryCategoriesModule } from './modules/route-inventory-categories/route-inventory-categories.module';
import { LocationGuardService } from './services/guards/location-guard.service';
import { RouteRevenueCategoryManagementModule } from './modules/route-revenue-category-management/route-revenue-category-management.module';
import { RouteManageCostCategoriesModule } from './modules/route-manage-cost-categories/route-manage-cost-categories.module';
import { RouteEventScriptingModule } from './modules/route-event-scripting/route-event-scripting.module';
import { RouteEquipmentCategoriesModule } from './modules/route-equipment-categories/route-equipment-categories.module';

const routes: Routes = [
    {
        path: '', component: VisitorComponent, canActivate: [VisitorOnlyGuardService],
        children: [
            {path: '', pathMatch: 'full', component: LoginComponent},
            {path: 'forgot', component: ForgotComponent},
            {path: 'reset/:token', component: ResetComponent},
            {path: 'verification/:token', component: VerificationComponent}
        ]
    },
    {
        path: 'u', component: UserComponent, canActivate: [UserOnlyGuardService, CurrentUserGuardService, LocationGuardService],
        children: [
            {
                path: '',
                component: DashboardComponent,
                pathMatch: 'full'
            },
            {
                path: 'subscribers',
                canActivate: [AllowOnlyService],
                data: {
                    roles: [UserRole.SUPER_ADMIN]
                },
                loadChildren: './modules/route-subscribers/route-subscribers.module#RouteSubscribersModule',
            },
            {
                canActivate: [AllowOnlyService],
                data: {
                    roles: [UserRole.SUPER_ADMIN, UserRole.ADMIN]
                },
                path: 'users',
                loadChildren: './modules/route-users/route-users.module#RouteUsersModule'
            },
            {
                canActivate: [AllowOnlyService],
                data: {
                    roles: [UserRole.SUPER_ADMIN, UserRole.ADMIN]
                },
                path: 'venues',
                loadChildren: './modules/route-venues/route-venues.module#RouteVenuesModule'
            },
            {
                path: 'event-spaces',
                loadChildren: './modules/route-event-spaces/route-event-spaces.module#RouteEventSpacesModule'
            },
            {
                path: 'storage-spaces',
                loadChildren: './modules/route-storage-spaces/route-storage-spaces.module#RouteStorageSpacesModule'
            },
            {
                path: 'vendors',
                loadChildren: './modules/route-vendors/route-vendors.module#RouteVendorsModule'
            },
            {
                path: 'inventory',
                loadChildren: './modules/route-inventory/route-inventory.module#RouteInventoryModule'
            },
            {
                path: 'cost-category',
                loadChildren: './modules/route-cost-categories/route-cost-categories.module#RouteCostCategoriesModule'
            },
            {
                path: 'revenue-category',
                loadChildren: './modules/route-revenue-categories/route-revenue-categories.module#RouteRevenueCategoriesModule'
            },
            {
                path: 'manage-revenue-category',
                loadChildren: './modules/route-revenue-category-management/route-revenue-category-management.module#RouteRevenueCategoryManagementModule'
            },
            {
                path: 'manage-cost-category',
                loadChildren: './modules/route-manage-cost-categories/route-manage-cost-categories.module#RouteManageCostCategoriesModule'
            },
            {
                path: 'service-charge',
                loadChildren: './modules/route-service-charge/route-service-charge.module#RouteServiceChargeModule'
            },
            {
                path: 'tax-charge',
                loadChildren: './modules/route-tax-charge/route-tax-charge.module#RouteTaxChargeModule'
            },
            {
                path: 'budget-input',
                loadChildren: './modules/route-budget/route-budget.module#RouteBudgetModule'
            },
            {
                path: 'pricing-extension',
                loadChildren: './modules/route-pricing-extension/route-pricing-extension.module#RoutePricingExtensionModule'
            },
            {
                path: 'equipment',
                loadChildren: './modules/route-equipment/route-equipment.module#RouteEquipmentModule'
            },
            {
                path: 'equipment-categories',
                loadChildren: './modules/route-equipment-categories/route-equipment-categories.module#RouteEquipmentCategoriesModule'
            },
            {
                path: 'systems',
                loadChildren: './modules/route-systems/route-systems.module#RouteSystemsModule'
            },
            {
                canActivate: [AllowOnlyService],
                data: {
                    roles: [UserRole.ADMIN]
                },
                path: 'terms',
                loadChildren: './modules/route-terms-and-conditions/route-terms-and-conditions.module#RouteTermsAndConditionsModule'
            },
            {
                path: 'packages',
                loadChildren: './modules/route-packages/route-packages.module#RoutePackagesModule'
            },
            {
                canActivate: [AllowOnlyService],
                data: {
                    roles: [UserRole.ADMIN, UserRole.EVENT_PLANNER]
                },
                path: 'accounts',
                loadChildren: './modules/route-accounts/route-accounts.module#RouteAccountsModule'
            },
            {
                path: 'functions',
                loadChildren: './modules/route-functions/route-functions.module#RouteFunctionsModule'
            },
            {
                path: 'tickets',
                loadChildren: './modules/route-tickets/route-tickets.module#RouteTicketsModule'
            },
            {
                path: 'reports',
                loadChildren: './modules/route-reports/route-reports.module#RouteReportsModule'
            },
            {
                path: 'manage-purchase-orders',
                loadChildren: './modules/route-manage-purchase-orders/route-manage-purchase-orders.module#RouteManagePurchaseOrdersModule'
            },
            {
                path: 'contracts',
                loadChildren: './modules/route-contract-management/route-contract-management.module#RouteContractManagementModule'
            },
            {
                path: 'event-management',
                loadChildren: './modules/route-event-management/route-event-management.module#RouteEventManagementModule'
            },
            {
                path: 'sales-scripting',
                loadChildren: './modules/route-event-scripting/route-event-scripting.module#RouteEventScriptingModule'
            },
            {
                canActivate: [AllowOnlyService],
                data: {
                    roles: [UserRole.SUPER_ADMIN]
                },
                path: 'category-management',
                loadChildren: './modules/route-inventory-categories/route-inventory-categories.module#RouteInventoryCategoriesModule'
            },
            {
                canActivate: [AllowOnlyService],
                data: {
                    roles: [UserRole.ADMIN]
                },
                path: 'payment-info',
                loadChildren: './modules/route-payment-info/route-payment-info.module#RoutePaymentInfoModule'
            },
            {
                canActivate: [AllowOnlyService],
                data: {
                    roles: [UserRole.SUPER_ADMIN]
                },
                path: 'decision-trees',
                loadChildren: './modules/route-decision-trees/route-decision-trees.module#RouteDecisionTreesModule'
            }
        ]
    }
];

@NgModule({
    declarations: [],
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: [
        VisitorOnlyGuardService,
        UserOnlyGuardService,
        AllowOnlyService,
        CurrentUserGuardService,
        LocationGuardService
    ]
})
export class AppRoutingModule {
}
