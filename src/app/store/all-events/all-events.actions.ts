import { Action } from '@ngrx/store';
import { VenueClientModel } from '../../models/venue-client-model';
import { VenueEventModel } from '../../models/venue-event-model';

export const SET_ALL_EVENTS = 'SET_ALL_EVENTS';
export const EVENT_ADDED = 'EVENT_ADDED';

export class SetAllEvents implements Action {
    readonly type = SET_ALL_EVENTS;

    constructor(public payload: VenueEventModel[]) {
    }
}

export class EventAdded implements Action {
    readonly type = EVENT_ADDED;

    constructor(public payload: VenueEventModel) {
    }
}

export type AllEventsActions = SetAllEvents | EventAdded;
