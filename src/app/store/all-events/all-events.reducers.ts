import { AllEventsActions, EVENT_ADDED, SET_ALL_EVENTS } from './all-events.actions';

export function allEventsReducer(state = [], action: AllEventsActions) {

    switch (action.type) {
        case SET_ALL_EVENTS:
            return action.payload;
        case EVENT_ADDED:
            const newState = [...state];
            newState.unshift(action.payload);
            return newState;
    }

    return state;
}
