import { Action } from '@ngrx/store';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';

export const FUNCTION_PAGING_NEXT = 'FUNCTION_PAGING_NEXT';
export const FUNCTION_PAGING_PREV = 'FUNCTION_PAGING_PREV';
export const FUNCTION_PAGING_CURRENT = 'FUNCTION_PAGING_CURRENT';
export const FUNCTION_PAGING_FILTER = 'FUNCTION_PAGING_FILTER';
export const FUNCTION_PAGING_LAST_PARAMS = 'FUNCTION_PAGING_LAST_PARAMS';

export interface FunctionPage {
    id: number;
    page: number;
}

export class FunctionPagingNext implements Action {
    readonly type = FUNCTION_PAGING_NEXT;

    constructor(public payload: FunctionPage) {
    }
}

export class FunctionPagingPrev implements Action {
    readonly type = FUNCTION_PAGING_PREV;

    constructor(public payload: FunctionPage) {
    }
}

export class FunctionPagingCurrent implements Action {
    readonly type = FUNCTION_PAGING_CURRENT;

    constructor(public payload: FunctionPage) {
    }
}

export class FunctionPagingFilter implements Action {
    readonly type = FUNCTION_PAGING_FILTER;

    constructor(public payload: any) {
    }
}

export class FunctionPagingLastParams implements Action {
    readonly type = FUNCTION_PAGING_LAST_PARAMS;

    constructor(public payload: HttpResourceQueryParams) {
    }
}

export type FunctionPagingActions =
    FunctionPagingNext
    | FunctionPagingPrev
    | FunctionPagingCurrent
    | FunctionPagingFilter
    | FunctionPagingLastParams;
