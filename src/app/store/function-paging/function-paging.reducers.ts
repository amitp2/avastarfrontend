import {
    FUNCTION_PAGING_CURRENT,
    FUNCTION_PAGING_LAST_PARAMS,
    FUNCTION_PAGING_NEXT,
    FUNCTION_PAGING_PREV,
    FunctionPage,
    FunctionPagingActions
} from './function-paging.actions';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';

export interface FunctionPagingState {
    hasPrev: boolean;
    hasNext: boolean;
    next: FunctionPage;
    prev: FunctionPage;
    current: FunctionPage;
    lastParams: HttpResourceQueryParams;
}

const initialState: FunctionPagingState = {
    hasPrev: false,
    hasNext: false,
    next: null,
    prev: null,
    current: null,
    lastParams: null
};

export function functionPagingReducer(state = initialState, action: FunctionPagingActions): FunctionPagingState {
    switch (action.type) {
        case FUNCTION_PAGING_NEXT:
            return {
                ...state,
                next: action.payload,
                hasNext: !!action.payload
            };
        case FUNCTION_PAGING_PREV:
            return {
                ...state,
                prev: action.payload,
                hasPrev: !!action.payload
            };
        case FUNCTION_PAGING_CURRENT:
            return {
                ...state,
                current: action.payload
            };
        case FUNCTION_PAGING_LAST_PARAMS:
            return {
                ...state,
                lastParams: action.payload
            };
    }
    return state;
}
