import { Action } from '@ngrx/store';
import { VendorModel } from '../../models/vendor-model';

export const VENDORS_LOADED = 'VENDORS_LOADED';
export const VENDOR_ADDED = 'VENDOR_ADDED';

export class VendorsLoaded implements Action {
    readonly type = VENDORS_LOADED;

    constructor(public payload: VendorModel[]) {
    }
}

export class VendorAdded implements Action {
    readonly type = VENDOR_ADDED;

    constructor(public payload: VendorModel) {
    }
}

export type VendorsActions = VendorsLoaded | VendorAdded;
