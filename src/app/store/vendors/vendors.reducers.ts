import { VENDOR_ADDED, VENDORS_LOADED, VendorsActions } from './vendors.actions';

const initialState = [];

export function vendorsReducer(state = initialState, action: VendorsActions) {

    switch (action.type) {
        case VENDORS_LOADED:
            return action.payload;
        case VENDOR_ADDED:
            return [
                ...state,
                action.payload
            ];
    }

    return state;
}
