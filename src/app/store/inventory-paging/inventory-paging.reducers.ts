import {
    INVENTORY_PAGING_CURRENT,
    INVENTORY_PAGING_LAST_PARAMS,
    INVENTORY_PAGING_NEXT,
    INVENTORY_PAGING_PREV,
    InventoryPage,
    InventoryPagingActions
} from './inventory-paging.actions';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';

export interface InventoryPagingState {
    hasPrev: boolean;
    hasNext: boolean;
    next: InventoryPage;
    prev: InventoryPage;
    current: InventoryPage;
    lastParams: HttpResourceQueryParams;
}

const initialState: InventoryPagingState = {
    hasPrev: false,
    hasNext: false,
    next: null,
    prev: null,
    current: null,
    lastParams: null
};

export function inventoryPagingReducer(state = initialState, action: InventoryPagingActions): InventoryPagingState {
    switch (action.type) {
        case INVENTORY_PAGING_NEXT:
            return {
                ...state,
                next: action.payload,
                hasNext: !!action.payload
            };
        case INVENTORY_PAGING_PREV:
            return {
                ...state,
                prev: action.payload,
                hasPrev: !!action.payload
            };
        case INVENTORY_PAGING_CURRENT:
            return {
                ...state,
                current: action.payload
            };
        case INVENTORY_PAGING_LAST_PARAMS:
            return {
                ...state,
                lastParams: action.payload
            };
    }
    return state;
}
