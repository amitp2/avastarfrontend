import { Action } from '@ngrx/store';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';

export const INVENTORY_PAGING_NEXT = 'INVENTORY_PAGING_NEXT';
export const INVENTORY_PAGING_PREV = 'INVENTORY_PAGING_PREV';
export const INVENTORY_PAGING_CURRENT = 'INVENTORY_PAGING_CURRENT';
export const INVENTORY_PAGING_FILTER = 'INVENTORY_PAGING_FILTER';
export const INVENTORY_PAGING_LAST_PARAMS = 'INVENTORY_PAGING_LAST_PARAMS';

export interface InventoryPage {
    id: number;
    page: number;
}

export class InventoryPagingNext implements Action {
    readonly type = INVENTORY_PAGING_NEXT;

    constructor(public payload: InventoryPage) {
    }
}

export class InventoryPagingPrev implements Action {
    readonly type = INVENTORY_PAGING_PREV;

    constructor(public payload: InventoryPage) {
    }
}

export class InventoryPagingCurrent implements Action {
    readonly type = INVENTORY_PAGING_CURRENT;

    constructor(public payload: InventoryPage) {
    }
}

export class InventoryPagingFilter implements Action {
    readonly type = INVENTORY_PAGING_FILTER;

    constructor(public payload: any) {
    }
}

export class InventoryPagingLastParams implements Action {
    readonly type = INVENTORY_PAGING_LAST_PARAMS;

    constructor(public payload: HttpResourceQueryParams) {
    }
}

export type InventoryPagingActions =
    InventoryPagingNext
    | InventoryPagingPrev
    | InventoryPagingCurrent
    | InventoryPagingFilter
    | InventoryPagingLastParams;
