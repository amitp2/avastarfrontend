import { AppState } from './store';
import { ProductType } from '../enums/product-type.enum';
import { GeolocationDataModel } from '../models/geolocation-data.model';
import { CountryModel } from '../models/country-model';
import { StateModel } from '../models/state-model';

export const selectSubscriberProductAccess = (state: AppState) => state.subscriberProductAccess;

export const selectSubscriberProductUsage = (state: AppState) => state.subscriberProductUsage;

export const subscriberProductLicenseCount = (subscriberId: any, product: ProductType) => {
    return (state: AppState) => {
        if (!state.subscriberProductAccess) {
            return 0;
        }
        if (!state.subscriberProductAccess[subscriberId]) {
            return 0;
        }

        if (!state.subscriberProductAccess[subscriberId][product]) {
            return 0;
        }
        return state.subscriberProductAccess[subscriberId][product];
    };
};

export const subscriberProductUsageCount = (subscriberId: any, product: ProductType) => {
    return (state: AppState) => {
        if (!state.subscriberProductUsage) {
            return 0;
        }
        if (!state.subscriberProductUsage[subscriberId]) {
            return 0;
        }

        if (!state.subscriberProductUsage[subscriberId][product]) {
            return 0;
        }
        return state.subscriberProductUsage[subscriberId][product];
    };
};

export const subscriberHasAvailableSlotsForProduct = (subscriberId: any, product: ProductType) => {
    return (state: AppState) => {
        const available = subscriberProductLicenseCount(subscriberId, product)(state);
        const used = subscriberProductUsageCount(subscriberId, product)(state);
        return used < available;
    };
};

export const userLocation = () => (state: AppState): GeolocationDataModel => state.location.current;
export const countryByCode = (code: string) => (state: AppState): CountryModel => state.location.countries.find(x => x.code === code);
export const stateByCode = (code: string) => (state: AppState): StateModel => state.location.states.find(x => x.code === code);
