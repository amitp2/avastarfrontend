import { Action } from '@ngrx/store';
import { GeolocationDataModel } from '../../models/geolocation-data.model';
import { CountryModel } from '../../models/country-model';
import { StateModel } from '../../models/state-model';

export const SET_LOCATION = 'SET_LOCATION';
export const SET_COUNTRIES = 'SET_COUNTRIES';
export const SET_STATES = 'SET_STATES';

export class SetLocation implements Action {
    readonly type = SET_LOCATION;

    constructor(public payload: GeolocationDataModel) {
    }
}

export class SetCountries implements Action {
    readonly type = SET_COUNTRIES;

    constructor(public payload: CountryModel[]) {
    }
}

export class SetStates implements Action {
    readonly type = SET_STATES;

    constructor(public payload: StateModel[]) {
    }
}

export type LocationActions = SetLocation | SetCountries | SetStates;
