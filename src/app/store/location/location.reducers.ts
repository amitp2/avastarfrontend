import { LocationActions, SET_COUNTRIES, SET_LOCATION, SET_STATES, SetLocation } from './location.actions';
import { GeolocationDataModel } from '../../models/geolocation-data.model';
import { StateModel } from '../../models/state-model';
import { CountryModel } from '../../models/country-model';

export interface LocationState {
    current: GeolocationDataModel;
    states: StateModel[];
    countries: CountryModel[];
}

const initialState: LocationState = {
    current: new GeolocationDataModel(),
    states: [],
    countries: []
};

export function locationReducer(state = initialState, action: LocationActions) {
    switch (action.type) {
        case SET_LOCATION:
            return {
                ...state,
                current: action.payload
            };
        case SET_COUNTRIES:
            return {
                ...state,
                countries: action.payload
            };
        case SET_STATES:
            return {
                ...state,
                states: action.payload
            };
    }
    return state;
}
