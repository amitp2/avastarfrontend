import { Action } from '@ngrx/store';
import { SubscriberProductUsage } from '../../models/product-model';
import { ProductType } from '../../enums/product-type.enum';

export const SET_SUBSCRIBER_PRODUCT_USAGE = 'SET_SUBSCRIBER_PRODUCT_USAGE';

export const SUBSCRIBER_PRODUCT_USAGE_INCREASED = 'SUBSCRIBER_PRODUCT_USAGE_INCREASED';

export const SUBSCRIBER_PRODUCT_USAGE_DECREASED = 'SUBSCRIBER_PRODUCT_USAGE_DECREASED';

export class SetSubscriberProductUsage implements Action {
    public readonly type = SET_SUBSCRIBER_PRODUCT_USAGE;

    constructor(public payload: {subscriberId: number, usage: SubscriberProductUsage}) {
    }
}

export class SubscriberProductUsageIncreased implements Action {
    readonly type = SUBSCRIBER_PRODUCT_USAGE_INCREASED;
    constructor(public payload: {subscriberId: number, product: ProductType}) {
    }
}

export class SubscriberProductUsageDecreased implements Action {
    readonly type = SUBSCRIBER_PRODUCT_USAGE_DECREASED;
    constructor(public payload: {subscriberId: number, product: ProductType}) {
    }
}

export type SubscriberProductUsageActions = SetSubscriberProductUsage | SubscriberProductUsageIncreased | SubscriberProductUsageDecreased;
