import { SubscriberProductUsage } from '../../models/product-model';

export interface SubscriberProductUsageMap {
    [subscriberId: number]: SubscriberProductUsage;
}
