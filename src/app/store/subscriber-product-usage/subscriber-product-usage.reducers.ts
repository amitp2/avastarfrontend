import {
    SET_SUBSCRIBER_PRODUCT_USAGE, SUBSCRIBER_PRODUCT_USAGE_DECREASED, SUBSCRIBER_PRODUCT_USAGE_INCREASED,
    SubscriberProductUsageActions
} from './subscriber-product-usage.actions';

const initialState = {};

export function subscriberProductUsageReducer(state = initialState, action: SubscriberProductUsageActions) {

    switch (action.type) {
        case SET_SUBSCRIBER_PRODUCT_USAGE:
            return {
                ...state,
                [action.payload.subscriberId]: action.payload.usage
            };
        case SUBSCRIBER_PRODUCT_USAGE_INCREASED:
            return {
                ...state,
                [action.payload.subscriberId]: {
                    ...state[action.payload.subscriberId],
                    [action.payload.product]: state[action.payload.subscriberId][action.payload.product] + 1
                }
            };
        case SUBSCRIBER_PRODUCT_USAGE_DECREASED:
            return {
                ...state,
                [action.payload.subscriberId]: {
                    ...state[action.payload.subscriberId],
                    [action.payload.product]: state[action.payload.subscriberId][action.payload.product] - 1
                }
            };
    }

    return state;
}
