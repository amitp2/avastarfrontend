import { VenueModel } from '../models/venue-model';

import { ActionReducerMap } from '@ngrx/store';
import { currentVenueReducer } from './current-venue/current-venue.reducers';
import { VendorModel } from '../models/vendor-model';
import { vendorsReducer } from './vendors/vendors.reducers';
import { VenueClientModel } from '../models/venue-client-model';
import { allAccountsReducer } from './all-accounts/all-accounts.reducers';
import { VenueEventModel } from '../models/venue-event-model';
import { allEventsReducer } from './all-events/all-events.reducers';
import { FunctionsFormPageData } from '../modules/route-functions/components/functions-form/functions-form.component';
import { functionsFormPageReducer } from './functions-form-page/functions-form-page.reducers';
import { SubscriberProductAccess } from './subscriber-product-access/subscriber-product-access';
import { subscriberProductAccessReducer } from './subscriber-product-access/subscriber-product-access.reducers';
import { SubscriberProductUsageMap } from './subscriber-product-usage/subscriber-product-usage';
import { subscriberProductUsageReducer } from './subscriber-product-usage/subscriber-product-usage.reducers';
import { locationReducer, LocationState } from './location/location.reducers';
import { inventoryPagingReducer, InventoryPagingState } from './inventory-paging/inventory-paging.reducers';
import { equipmentPagingReducer, EquipmentPagingState } from './equipment-paging/equipment-paging.reducers';
import { functionPagingReducer, FunctionPagingState } from './function-paging/function-paging.reducers';

export interface AppState {
    currentVenue: VenueModel;
    vendors: VendorModel[];
    location: LocationState;
    allAccounts: VenueClientModel[];
    allEvents: VenueEventModel[];
    functionsFormPage: FunctionsFormPageData;
    subscriberProductAccess: SubscriberProductAccess;
    subscriberProductUsage: SubscriberProductUsageMap;
    inventoryPaging: InventoryPagingState;
    equipmentPaging: EquipmentPagingState;
    functionPaging: FunctionPagingState;
}

export const AppStore: ActionReducerMap<AppState> = {
    currentVenue: currentVenueReducer,
    vendors: vendorsReducer,
    allAccounts: allAccountsReducer,
    allEvents: allEventsReducer,
    location: locationReducer,
    functionsFormPage: functionsFormPageReducer,
    subscriberProductAccess: subscriberProductAccessReducer,
    subscriberProductUsage: subscriberProductUsageReducer,
    inventoryPaging: inventoryPagingReducer,
    equipmentPaging: equipmentPagingReducer,
    functionPaging: functionPagingReducer,
};
