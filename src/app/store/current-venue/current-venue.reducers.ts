import { VenueModel } from '../../models/venue-model';
import { CurrentVenueActions, SET_CURRENT_VENUE, VENUE_UPDATED } from './current-venue.actions';

const initialState = new VenueModel();

export function currentVenueReducer(state = initialState, action: CurrentVenueActions) {

    switch (action.type) {
        case SET_CURRENT_VENUE:
            return action.payload;
        case VENUE_UPDATED:
            if (+state.id === +action.payload.id) {
                return action.payload;
            }
            return state;
    }

    return state;
}
