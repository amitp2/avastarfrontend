import { Action } from '@ngrx/store';
import { VenueModel } from '../../models/venue-model';

export const SET_CURRENT_VENUE = 'set_current_venue';
export const VENUE_UPDATED = 'venue_updated';

export class SetCurrentVenue implements Action {
    readonly type = SET_CURRENT_VENUE;

    constructor(public payload: VenueModel) {
    }
}

export class VenueUpdated implements Action {
    readonly type = VENUE_UPDATED;

    constructor(public payload: VenueModel) {
    }
}

export type CurrentVenueActions = SetCurrentVenue | VenueUpdated;
