import { Action } from '@ngrx/store';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { EquipmentSubcategoryItem } from '../../models/equipment-subcategory-item.model';

export const EQUIPMENT_PAGING_NEXT = 'EQUIPMENT_PAGING_NEXT';
export const EQUIPMENT_PAGING_PREV = 'EQUIPMENT_PAGING_PREV';
export const EQUIPMENT_PAGING_CURRENT = 'EQUIPMENT_PAGING_CURRENT';
export const EQUIPMENT_PAGING_FILTER = 'EQUIPMENT_PAGING_FILTER';
export const EQUIPMENT_PAGING_LAST_PARAMS = 'EQUIPMENT_PAGING_LAST_PARAMS';

export interface EquipmentPage {
    model: EquipmentSubcategoryItem;
    page: number;
}

export class EquipmentPagingNext implements Action {
    readonly type = EQUIPMENT_PAGING_NEXT;

    constructor(public payload: EquipmentPage) {
    }
}

export class EquipmentPagingPrev implements Action {
    readonly type = EQUIPMENT_PAGING_PREV;

    constructor(public payload: EquipmentPage) {
    }
}

export class EquipmentPagingCurrent implements Action {
    readonly type = EQUIPMENT_PAGING_CURRENT;

    constructor(public payload: EquipmentPage) {
    }
}

export class EquipmentPagingFilter implements Action {
    readonly type = EQUIPMENT_PAGING_FILTER;

    constructor(public payload: any) {
    }
}

export class EquipmentPagingLastParams implements Action {
    readonly type = EQUIPMENT_PAGING_LAST_PARAMS;

    constructor(public payload: HttpResourceQueryParams) {
    }
}

export type EquipmentPagingActions =
    EquipmentPagingNext
    | EquipmentPagingPrev
    | EquipmentPagingCurrent
    | EquipmentPagingFilter
    | EquipmentPagingLastParams;
