import {
    EQUIPMENT_PAGING_CURRENT,
    EQUIPMENT_PAGING_LAST_PARAMS,
    EQUIPMENT_PAGING_NEXT,
    EQUIPMENT_PAGING_PREV,
    EquipmentPage,
    EquipmentPagingActions
} from './equipment-paging.actions';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';

export interface EquipmentPagingState {
    hasPrev: boolean;
    hasNext: boolean;
    next: EquipmentPage;
    prev: EquipmentPage;
    current: EquipmentPage;
    lastParams: HttpResourceQueryParams;
}

const initialState: EquipmentPagingState = {
    hasPrev: false,
    hasNext: false,
    next: null,
    prev: null,
    current: null,
    lastParams: null
};

export function equipmentPagingReducer(state = initialState, action: EquipmentPagingActions): EquipmentPagingState {
    switch (action.type) {
        case EQUIPMENT_PAGING_NEXT:
            return {
                ...state,
                next: action.payload,
                hasNext: !!action.payload
            };
        case EQUIPMENT_PAGING_PREV:
            return {
                ...state,
                prev: action.payload,
                hasPrev: !!action.payload
            };
        case EQUIPMENT_PAGING_CURRENT:
            return {
                ...state,
                current: action.payload
            };
        case EQUIPMENT_PAGING_LAST_PARAMS:
            return {
                ...state,
                lastParams: action.payload
            };
    }
    return state;
}
