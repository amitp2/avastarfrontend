import { Action } from '@ngrx/store';
import { FunctionEquipmentModel } from '../../models/function-model';

export const ADD_SELECTED_EQUIPMENTS = 'ADD_SELECTED_EQUIPMENTS';
export const REMOVE_SELECTED_EQUIPMENTS = 'REMOVE_SELECTED_EQUIPMENTS';
export const UPDATE_DAY_QUANTITY = 'UPDATE_DAY_QUANTITY';
export const CHANGED_DAY_COUNT = 'CHANGED_DAY_COUNT';
export const SET_EQUIPMENTS = 'SET_EQUIPMENTS';

export class AddSelectedEquipments implements Action {
    readonly type = ADD_SELECTED_EQUIPMENTS;

    constructor(public payload: FunctionEquipmentModel[]) {
    }
}

export class RemoveSelectedEquipments implements Action {
    readonly type = REMOVE_SELECTED_EQUIPMENTS;

    constructor(public payload: FunctionEquipmentModel[]) {
    }
}

export interface UpdateDayQuantityForEquipmentPayload {
    day: any;
    item: FunctionEquipmentModel;
    quantity: number;
}

export class UpdateDayQuantityForEquipment implements Action {
    readonly type = UPDATE_DAY_QUANTITY;

    constructor(public payload: UpdateDayQuantityForEquipmentPayload) {
    }
}

export interface ChangeDayCountPayload {
    count: number;
    startDay: any;
}

export class ChangeDayCount implements Action {
    readonly type = CHANGED_DAY_COUNT;

    constructor(public payload: ChangeDayCountPayload) {
    }
}

export class SetEquipments implements Action {
    readonly type = SET_EQUIPMENTS;

    constructor(public payload: FunctionEquipmentModel[]) {
    }
}

export type FunctionsFormPageActions = SetEquipments |
    AddSelectedEquipments |
    RemoveSelectedEquipments |
    UpdateDayQuantityForEquipment |
    ChangeDayCount;
