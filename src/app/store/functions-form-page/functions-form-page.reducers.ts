import {
    ADD_SELECTED_EQUIPMENTS,
    CHANGED_DAY_COUNT,
    ChangeDayCountPayload,
    FunctionsFormPageActions,
    REMOVE_SELECTED_EQUIPMENTS, SET_EQUIPMENTS,
    UPDATE_DAY_QUANTITY, UpdateDayQuantityForEquipmentPayload
} from './functions-form-page.actions';
import { FunctionEquipmentDate, FunctionEquipmentDateModel, FunctionEquipmentModel } from '../../models/function-model';
import * as moment from 'moment';

const initialState = {
    selectedEquipments: []
};

export function functionsFormPageReducer(state = initialState, action: FunctionsFormPageActions) {

    let payload;
    let selectedEquipments;

    switch (action.type) {
        case SET_EQUIPMENTS:
            return {
                ...state,
                selectedEquipments: action.payload
            };
        case CHANGED_DAY_COUNT:
            payload = <ChangeDayCountPayload> action.payload;
            selectedEquipments = state.selectedEquipments.map((item: FunctionEquipmentModel) => {
                const eventFunctionEquipmentDates = [];
                let i = 1;
                let eqDay;
                let startDay = moment(payload.startDay).startOf('day');
                for (; i <= payload.count; i++) {

                    if (item.eventFunctionEquipmentDates[i - 1] && item.eventFunctionEquipmentDates[i - 1].date === startDay.format('YYYY-MM-DD')) {
                        eventFunctionEquipmentDates.push(item.eventFunctionEquipmentDates[i - 1]);
                    } else {
                        eqDay = new FunctionEquipmentDateModel();
                        eqDay.date = startDay.format('YYYY-MM-DD');
                        eqDay.quantity = 0;
                        // eqDay.setupDay
                        eventFunctionEquipmentDates.push(eqDay);
                    }
                    startDay = startDay.startOf('day').add(1, 'days').startOf('day');
                }
                item.eventFunctionEquipmentDates = eventFunctionEquipmentDates;
                return item;
            });
            return {
                ...state,
                selectedEquipments
            };
        case UPDATE_DAY_QUANTITY:
            payload = <UpdateDayQuantityForEquipmentPayload> action.payload;
            selectedEquipments = state.selectedEquipments.map((item: FunctionEquipmentModel) => {
                const is = (item.equipmentPackageId === payload.item.equipmentPackageId && item.equipmentPackageId) || (item.equipmentServiceId && item.equipmentServiceId === payload.item.equipmentServiceId);
                let eventFunctionEquipmentDates = item.eventFunctionEquipmentDates;
                if (is) {
                    eventFunctionEquipmentDates = item.eventFunctionEquipmentDates.map((day: FunctionEquipmentDate) => {
                        if (day.date === payload.day) {
                            day.quantity = payload.quantity;
                        }
                        return day;
                    });
                }
                item.eventFunctionEquipmentDates = eventFunctionEquipmentDates;
                return item;
            });
            return {
                ...state,
                selectedEquipments
            };
        case ADD_SELECTED_EQUIPMENTS:
            return {
                ...state,
                selectedEquipments: state.selectedEquipments.concat(action.payload)
            };
        case REMOVE_SELECTED_EQUIPMENTS:
            const serviceIds = {};
            const packageIds = {};

            action.payload.forEach((item: FunctionEquipmentModel) => {
                if (item.equipmentPackageId) {
                    packageIds[item.equipmentPackageId] = true;
                }
                if (item.equipmentServiceId) {
                    serviceIds[item.equipmentServiceId] = true;
                }
            });

            selectedEquipments = state.selectedEquipments.filter((item) => {
                if (item.equipmentPackageId) {
                    return !packageIds[item.equipmentPackageId];
                }
                if (item.equipmentServiceId) {
                    return !serviceIds[item.equipmentServiceId];
                }
                return true;
            });

            return {
                ...state,
                selectedEquipments
            };
    }

    return state;
}
