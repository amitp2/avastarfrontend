import { ACCOUNT_ADDED, AllAccountsActions, SET_ALL_ACCOUNTS } from './all-accounts.actions';

const initialState = [];

export function allAccountsReducer(state = initialState, action: AllAccountsActions) {

    switch (action.type) {
        case SET_ALL_ACCOUNTS:
            return action.payload;
        case ACCOUNT_ADDED:
            const newState = [...state];
            newState.unshift(action.payload);
            return newState;
    }

    return state;
}
