import { Action } from '@ngrx/store';
import { VenueClientModel } from '../../models/venue-client-model';

export const SET_ALL_ACCOUNTS = 'SET_ALL_ACCOUNTS';
export const ACCOUNT_ADDED = 'ACCOUNT_ADDED';

export class SetAllAccounts implements Action {
    readonly type = SET_ALL_ACCOUNTS;

    constructor(public payload: VenueClientModel[]) {
    }
}

export class AccountAdded implements Action {
    readonly type = ACCOUNT_ADDED;

    constructor(public payload: VenueClientModel) {
    }
}

export type AllAccountsActions = SetAllAccounts | AccountAdded;
