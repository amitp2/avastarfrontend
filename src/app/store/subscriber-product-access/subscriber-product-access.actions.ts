import { Action } from '@ngrx/store';
import { SubscriberProducts } from './subscriber-product-access';
import { ProductType } from '../../enums/product-type.enum';

export const SET_SUBSCRIBER_PRODUCT_ACCESS = 'SET_SUBSCRIBER_PRODUCT_ACCESS';
export const SET_SUBSCRIBER_ACCESS_FOR_PRODUCT = 'SET_SUBSCRIBER_ACCESS_FOR_PRODUCT';

export class SetSubscriberProductAccess implements Action {
    readonly type = SET_SUBSCRIBER_PRODUCT_ACCESS;

    constructor(public payload: { subscriberId: number, access: SubscriberProducts}) {
    }
}

export class SetSubscriberProductAccessForProduct implements Action {
    readonly type = SET_SUBSCRIBER_ACCESS_FOR_PRODUCT;

    constructor(public payload: {subscriberId: number, product: ProductType, count: number}) {
    }
}

export type SubscriberProductAccessActions = SetSubscriberProductAccess | SetSubscriberProductAccessForProduct;
