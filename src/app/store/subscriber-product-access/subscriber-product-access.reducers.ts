import {
    SET_SUBSCRIBER_ACCESS_FOR_PRODUCT, SET_SUBSCRIBER_PRODUCT_ACCESS,
    SubscriberProductAccessActions
} from './subscriber-product-access.actions';

const initialState = {};

export function subscriberProductAccessReducer(state = initialState, action: SubscriberProductAccessActions) {

    switch (action.type) {
        case SET_SUBSCRIBER_PRODUCT_ACCESS:
            return {
                ...state,
                [action.payload.subscriberId]: action.payload.access
            };
        case SET_SUBSCRIBER_ACCESS_FOR_PRODUCT:
            return {
                ...state,
                [action.payload.subscriberId]: {
                    ...state[action.payload.subscriberId],
                    [action.payload.product]: action.payload.count
                }
            };
    }

    return state;
}
