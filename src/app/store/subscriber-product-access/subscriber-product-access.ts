export interface SubscriberProducts {
    [product: string]: number;
}

export interface SubscriberProductAccess {
    [id: number]: SubscriberProducts;
}
