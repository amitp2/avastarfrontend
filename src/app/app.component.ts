import { AfterViewInit, Component, Injector, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ActivityTrackerService } from './services/activity-tracker.service';
import { ModalService } from './services/modal/modal.service';
import { ModalMarkerDirective } from './directives/modal-marker.directive';
import { AuthService } from './services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { AsyncTasksService, AsyncTaskStatus } from './services/async-tasks.service';
import { AppErrorDefaultMessages } from './enums/app-error.enum';
import { AppSuccessDefaultMessages } from './enums/app-success.enum';

import 'rxjs/add/operator/merge';
import { LocationService } from './services/location.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.styl']
})
export class AppComponent implements OnInit, AfterViewInit {
    public static rootInjector: Injector;

    @ViewChild(ModalMarkerDirective, { read: ViewContainerRef }) marker;
    asyncTasksInProgress: number;

    constructor(
        private activityTrackerService: ActivityTrackerService,
        private modalService: ModalService,
        private authService: AuthService,
        private toastrService: ToastrService,
        private asyncTasksService: AsyncTasksService,
        private locationService: LocationService,
        private injector: Injector
    ) {
        this.asyncTasksInProgress = 0;
        AppComponent.rootInjector = this.injector;
    }

    ngOnInit(): void {
        this.activityTrackerService.track();
        this.authService.startTokenRefreshProcess();
        this.authService.refreshToken().catch(() => {
        });
        this.asyncTasksService.trackAnyError().subscribe((task: AsyncTaskStatus) => {
            if (AppErrorDefaultMessages[task.payload]) {
                this.toastrService.error(AppErrorDefaultMessages[task.payload], 'Error', {
                    positionClass: 'toast-bottom-right',
                    tapToDismiss: false
                });
            } else if (typeof  task.payload === 'string') {
                this.toastrService.error(task.payload, 'Error', {
                    positionClass: 'toast-bottom-right',
                    tapToDismiss: false
                });
            }
            console.log(task.payload);
        });
        this.asyncTasksService.trackAnySuccess().subscribe((task: AsyncTaskStatus) => {
            if (AppSuccessDefaultMessages[task.payload]) {
                this.toastrService.success(AppSuccessDefaultMessages[task.payload], 'Success', {
                    positionClass: 'toast-bottom-right',
                    tapToDismiss: false
                });
            }
        });
        this.asyncTasksService.trackAnyStart().subscribe(() => this.increaseAsyncTasksCount());
        this.asyncTasksService.trackAnySuccess()
            .merge(this.asyncTasksService.trackAnyFinish())
            .merge(this.asyncTasksService.trackAnyError())
            .subscribe(() => this.decreaseAsyncTasksCount());
    }

    increaseAsyncTasksCount() {
        setTimeout(() => {
            this.asyncTasksInProgress++;
        });
    }

    decreaseAsyncTasksCount() {
        setTimeout(() => {
            if (this.asyncTasksInProgress > 0) {
                this.asyncTasksInProgress--;
            }
        });
    }

    ngAfterViewInit() {
        this.modalService.setModalRef(this.marker);
    }
}
