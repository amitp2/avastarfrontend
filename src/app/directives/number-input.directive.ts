import { Directive, HostListener } from '@angular/core';

@Directive({
    selector: '[appNumberInput]'
})
export class NumberInputDirective {

    constructor() { }

    @HostListener('keydown', ['$event'])
    onKeyDown(event: KeyboardEvent) {
        if (event.keyCode === 69) {
            event.preventDefault();
        }
    }

}
