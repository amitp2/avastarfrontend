import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { ModalService } from '../services/modal/modal.service';

@Directive({
    selector: '[appConfirmClick]'
})
export class ConfirmClickDirective {

    @Input() confirmMessage: string;
    @Output() appConfirmClick: EventEmitter<any>;

    @HostListener('click') clicked() {
        this.modalService
            .showConfirm(this.confirmMessage)
            .then((yes: boolean) => {
                if (yes) {
                    this.appConfirmClick.emit();
                }
            })
            .catch(() => {
            });
    }

    constructor(private modalService: ModalService) {
        this.appConfirmClick = new EventEmitter<any>();
    }

}
