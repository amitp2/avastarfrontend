import { AsyncTasksShowOnSuccessDirective } from './async-tasks-show-on-success.directive';
import { Component } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { AsyncTasksService } from '../../services/async-tasks.service';
import { By } from '@angular/platform-browser';

@Component({
    template: `
        <div *appAsyncTasksShowOnSuccess="'ASYNC_TASK'"></div>
    `
})
class TestHostComponent {

}

describe('AsyncTasksShowOnSuccessDirective', () => {

    const task = 'ASYNC_TASK';
    let fixture: ComponentFixture<TestHostComponent>;
    let component: TestHostComponent;
    let asyncTasksService: AsyncTasksService;


    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                TestHostComponent,
                AsyncTasksShowOnSuccessDirective
            ],
            providers: [
                AsyncTasksService
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(TestHostComponent);
        component = fixture.componentInstance;
        asyncTasksService = TestBed.get(AsyncTasksService);
        fixture.detectChanges();
    }));

    it('on attached async task\'s success it should show element, hide otherwise', fakeAsync(() => {
        let div;

        asyncTasksService.taskSuccess(task);
        tick();
        div = fixture.debugElement.query(By.css('div'));
        expect(div && div.nativeElement).toBeTruthy();

        asyncTasksService.taskError(task);
        tick();
        div = fixture.debugElement.query(By.css('div'));
        expect(div && div.nativeElement).toBeFalsy();

    }));
});
