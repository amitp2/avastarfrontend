import { Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { AsyncTasksService, AsyncTaskState, AsyncTaskStatus } from '../../services/async-tasks.service';
import { Subscription } from 'rxjs/Subscription';

@Directive({
    selector: '[appAsyncTasksShowOnSuccess]'
})
export class AsyncTasksShowOnSuccessDirective implements OnInit, OnDestroy {

    @Input() appAsyncTasksShowOnSuccess: string;

    subscription: Subscription;

    constructor(private templateRef: TemplateRef<any>,
                private viewContainerRef: ViewContainerRef,
                private asyncTasksService: AsyncTasksService) {
    }

    ngOnInit() {
        this.subscription = this.asyncTasksService
            .track(this.appAsyncTasksShowOnSuccess)
            .subscribe((status: AsyncTaskStatus) => {
                this.viewContainerRef.clear();
                if (status.state === AsyncTaskState.SUCCESS) {
                    this.viewContainerRef.createEmbeddedView(this.templateRef);
                }
            });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
