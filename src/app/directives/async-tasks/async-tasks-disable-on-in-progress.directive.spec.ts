import { AsyncTasksDisableOnInProgressDirective } from './async-tasks-disable-on-in-progress.directive';
import { Component, DebugElement } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { AsyncTasksService, AsyncTaskState } from '../../services/async-tasks.service';
import { By } from '@angular/platform-browser';

@Component({
    template: `
        <button type="button" appAsyncTasksDisableOnInProgress="TEST_TASK" (click)="click()"></button>
    `
})
class TestComponent {

    constructor() {
    }

    click() {

    }
}

describe('AsyncTasksDisableOnInProgressDirective', () => {

    let asyncTasksService: AsyncTasksService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                AsyncTasksDisableOnInProgressDirective,
                TestComponent
            ],
            imports: [],
            providers: [AsyncTasksService]
        });

        asyncTasksService = TestBed.get(AsyncTasksService);
    });

    it('should disable element when task state is IN_PROGRESS and enable otherwise', fakeAsync(() => {
        const fixture: ComponentFixture<TestComponent> = TestBed.createComponent(TestComponent);
        const button: DebugElement = fixture.debugElement.query(By.css('button'));
        fixture.detectChanges();

        expect(button.nativeElement.getAttribute('disabled')).toEqual(null);

        asyncTasksService.task('TEST_TASK', AsyncTaskState.IN_PROGRESS);
        tick();
        fixture.detectChanges();
        expect(button.nativeElement.getAttribute('disabled')).not.toEqual(null);

        asyncTasksService.task('TEST_TASK', AsyncTaskState.FINISHED);
        tick();
        fixture.detectChanges();
        expect(button.nativeElement.getAttribute('disabled')).toEqual(null);
    }));
});
