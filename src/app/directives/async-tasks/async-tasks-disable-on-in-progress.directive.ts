import { Directive, HostBinding, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AsyncTasksService, AsyncTaskState, AsyncTaskStatus } from '../../services/async-tasks.service';

@Directive({
    selector: '[appAsyncTasksDisableOnInProgress]'
})
export class AsyncTasksDisableOnInProgressDirective implements OnInit, OnDestroy {

    @HostBinding('disabled') disabled: boolean;
    @Input('appAsyncTasksDisableOnInProgress') taskName: string;
    subscription: Subscription;


    constructor(private asyncTasksService: AsyncTasksService) {
    }

    ngOnInit(): void {
        this.subscription = this.asyncTasksService.track(this.taskName).subscribe((status: AsyncTaskStatus) => {
            this.disabled = false;
            if (status.state === AsyncTaskState.IN_PROGRESS) {
                this.disabled = true;
            }
        });
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

}
