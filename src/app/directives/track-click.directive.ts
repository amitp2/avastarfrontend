import { Directive, HostListener } from '@angular/core';
import { ActivityTrackerService } from '../services/activity-tracker.service';

@Directive({
    selector: '[appTrackClick]'
})
export class TrackClickDirective {

    constructor(private activityTrackerService: ActivityTrackerService) {
    }

    @HostListener('click') clicked() {
        this.activityTrackerService.track();
    }

}
