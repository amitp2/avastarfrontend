import { ConfirmClickDirective } from './confirm-click.directive';
import { Component, DebugElement } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ModalService } from '../services/modal/modal.service';
import { By } from '@angular/platform-browser';

@Component({
    template: `
        <button (appConfirmClick)="clicked('some title')" confirmMessage="Some message?">
            Some button
        </button>
    `
})
class TestHostComponent {
    clicked(title: string) {

    }
}

class ModalServiceStub {
    showConfirm(title: string) {
        return Promise.resolve(true);
    }
}

describe('ConfirmClickDirective', () => {

    let fixture: ComponentFixture<TestHostComponent>;
    let component: TestHostComponent;
    let button: DebugElement;
    let modalService: ModalService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                TestHostComponent,
                ConfirmClickDirective
            ],
            providers: [
                {provide: ModalService, useClass: ModalServiceStub}
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TestHostComponent);
        component = fixture.componentInstance;
        modalService = TestBed.get(ModalService);
        button = fixture.debugElement.query(By.css('button'));
        fixture.detectChanges();
    });

    it('by clicking on the attached element it should show confirm message with given message', () => {
        const spy = spyOn(modalService, 'showConfirm').and.callThrough();
        button.triggerEventHandler('click', null);
        fixture.detectChanges();
        expect(spy.calls.argsFor(0)[0]).toEqual('Some message?');
    });

    it('on confirm true it should call handler method with given params', fakeAsync(() => {
        const spy = spyOn(component, 'clicked').and.callThrough();
        button.triggerEventHandler('click', null);
        tick();
        fixture.detectChanges();
        expect(spy.calls.argsFor(0)[0]).toEqual('some title');
    }));

    it('on confirm false it should call nothing', fakeAsync(() => {
        spyOn(modalService, 'showConfirm').and.returnValue(Promise.resolve(false));
        const spy = spyOn(component, 'clicked').and.callThrough();
        button.triggerEventHandler('click', null);
        tick();
        fixture.detectChanges();
        expect(spy.calls.argsFor(0)[0]).not.toEqual('some title');
    }));

    it('on cancel it should call nothing', fakeAsync(() => {
        spyOn(modalService, 'showConfirm').and.returnValue(Promise.reject(''));
        const spy = spyOn(component, 'clicked').and.callThrough();
        button.triggerEventHandler('click', null);
        tick();
        fixture.detectChanges();

        expect(spy.calls.argsFor(0)[0]).not.toEqual('some title');
    }));
});
