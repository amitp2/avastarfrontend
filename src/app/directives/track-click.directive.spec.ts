import { TrackClickDirective } from './track-click.directive';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { ActivityTrackerService } from '../services/activity-tracker.service';

@Component({
    template: `
        <button appTrackClick>click me</button>
        <div appTrackClick></div>
    `
})
class TestClickButtonComponent {
}

class ActivityTrackerServiceStub {
    track() {

    }
}

describe('TrackClickDirective', () => {

    let activityTrackerService: ActivityTrackerService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                TrackClickDirective,
                TestClickButtonComponent
            ],
            providers: [
                {provide: ActivityTrackerService, useClass: ActivityTrackerServiceStub}
            ]
        });
        activityTrackerService = TestBed.get(ActivityTrackerService);
    });


    it('should track clicks on clicking host element', () => {
        const spy = spyOn(activityTrackerService, 'track');
        const fixture: ComponentFixture<TestClickButtonComponent> = TestBed.createComponent(TestClickButtonComponent);
        const button: DebugElement = fixture.debugElement.query(By.css('button'));
        const div: DebugElement = fixture.debugElement.query(By.css('div'));

        button.triggerEventHandler('click', null);
        fixture.detectChanges();
        expect(spy.calls.all().length).toEqual(1);

        button.triggerEventHandler('click', null);
        fixture.detectChanges();
        expect(spy.calls.all().length).toEqual(2);

        div.triggerEventHandler('click', null);
        fixture.detectChanges();
        expect(spy.calls.all().length).toEqual(3);
    });

});
