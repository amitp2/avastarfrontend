import { InvalidFormControlDirective } from './invalid-form-control.directive';
import { Component, DebugElement } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

@Component({
    template: `
        <form [formGroup]="testForm">
            <div class="form-group name-group" [appInvalidFormControl]="this.testForm.get('name')">
                <input type="text" formControlName="name" class="form-control name">
            </div>
        </form>
    `
})
class TestComponent {

    testForm: FormGroup;

    constructor() {
        this.testForm = new FormGroup({
            name: new FormControl(null, [Validators.required]),
            lastName: new FormControl(null, [Validators.required])
        });
    }
}


describe('InvalidFormControlDirective', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                InvalidFormControlDirective,
                TestComponent
            ],
            imports: [ReactiveFormsModule]
        });
    });

    it('should add class has-error to element, if user has touched form-control and input is invalid', () => {
        const fixture: ComponentFixture<TestComponent> = TestBed.createComponent(TestComponent);
        const inputName: DebugElement = fixture.debugElement.query(By.css('.name'));
        const groupName: DebugElement = fixture.debugElement.query(By.css('.name-group'));

        fixture.detectChanges();
        expect(groupName.nativeElement.className.indexOf('has-error')).toEqual(-1);

        inputName.nativeElement.value = 'text1';
        inputName.nativeElement.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        expect(groupName.nativeElement.className.indexOf('has-error')).toEqual(-1);

        inputName.nativeElement.value = '';
        inputName.nativeElement.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        expect(groupName.nativeElement.className.indexOf('has-error')).not.toEqual(-1);

    });
});
