import { Directive, HostBinding, Input, OnDestroy, OnInit, ViewRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';

@Directive({
    selector: '[appInvalidFormControl]'
})
export class InvalidFormControlDirective implements OnInit, OnDestroy {

    @HostBinding('class.has-error') hasError: boolean;
    @Input('appInvalidFormControl') control: FormControl;
    subscription: Subscription;

    constructor() {
    }

    ngOnInit(): void {
        this.subscription = this.control.valueChanges.subscribe(() => {
            if (this.control.invalid) {
                this.hasError = true;
            }
        });
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

}
