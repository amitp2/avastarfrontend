export class EquipmentSubcategoryItem {
    public categoryId: number = null;
    public subCategoryId: number = null;
    public subCategoryItemId: number = null;
    public equipmentServiceId: number = null;
    public categoryName: string = null;
    public subCategoryName: string = null;
    public subCategoryItemName: string = null;

    constructor(data?: Partial<EquipmentSubcategoryItem>) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }
}
