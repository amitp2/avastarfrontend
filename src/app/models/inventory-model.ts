import { InventoryStorageType } from '../enums/inventory-storage-type.enum';
import { InventoryAssetOwnerType } from '../enums/inventory-asset-owner-type.enum';
import { InventoryAssetStatus } from '../enums/inventory-asset-status.enum';

export class InventoryModel {
    id: number = undefined;
    name: string = undefined;
    subCategoryId: number = undefined;
    categoryId: number = undefined;
    manufacturerId: number = undefined;
    manufacturerName: string = undefined;
    model: string = undefined;
    serialNumber: string = undefined;
    tagNumber: string = undefined;
    barcode: string = undefined;
    purchaseDate: string = undefined;
    purchasePrice: number = undefined;
    vendorId: number = undefined;
    portable: boolean = undefined;
    storageType: InventoryStorageType = undefined;
    storageId: number = undefined;
    assetOwnerType: InventoryAssetOwnerType = undefined;
    assetOwnerId: number = undefined;
    pictureUrl: string = undefined;
    condition: InventoryAssetStatus = undefined;
    subCategoryItemId: number = undefined;
    subCategoryItemName: string = undefined;

    constructor(data) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }
}
