import { ServiceCharge } from '../enums/service-charge.enum';
import { BillingFormat } from '../enums/billing-format.enum';

export class VenueSettingsModel {

    id: number = undefined;
    billingFormat: BillingFormat = undefined;
    currency: string = undefined;
    eventType: number = undefined;
    serviceChargeDefault: ServiceCharge = undefined;
    laborSummaryIncluded: boolean = undefined;
    sortedByCategory: boolean = undefined;
    revisionDateShown: boolean = undefined;
    teo: string = undefined;
    teoStart: string = undefined;
    po: string = undefined;
    poStart: string = undefined;
    finYearFrom: number = undefined;
    finYearTo: number = undefined;
    reportDisclaimer: string = undefined;
    emailDisclaimer: string = undefined;

    constructor(data: any = null) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }
}
