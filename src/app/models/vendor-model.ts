import { VendorStatus } from '../enums/vendor-status.enum';

export interface VendorAddress {
    countryId: number;
    stateId: number;
    city: string;
    address: string;
    address2: string;
    postCode: string;
}

export class VendorModel {

    id: number = undefined;
    name: string = undefined;
    status: VendorStatus = undefined;
    address: VendorAddress = undefined;
    phone: string = undefined;
    fax: string = undefined;
    website: string = undefined;
    subscriberId: number = undefined;

    constructor(data: any = null) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }

}
