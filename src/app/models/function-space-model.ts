import { FunctionSpaceStatus } from '../enums/function-space-status.enum';

export class FunctionSpaceModel {
    id: number = undefined;
    name: string = undefined;
    roomName: string = undefined;
    status: FunctionSpaceStatus = undefined;

    constructor(data: any) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }
}
