import { UserStatus } from '../enums/user-status.enum';
import { UserRole } from '../enums/user-role.enum';
import { ProductType } from '../enums/product-type.enum';
import { apiToProductType } from '../helpers/mapper-functions/api-to-product-type.mapper-function';
import { UserProductModel } from './user-product-model';
import { productTypeToApi } from '../helpers/mapper-functions/product-type-to-api.mapper-function';
export class UserModel {

    id: number = undefined;
    firstName: string = undefined;
    lastName: string = undefined;
    status: UserStatus = undefined;
    title: string = undefined;
    username: string = undefined;
    mobile: string = undefined;
    phone: string = undefined;
    roles: UserRole[] = undefined;
    notes: string = undefined;
    fax: string = undefined;
    subscriberId: number = undefined;
    subscriberName: string = undefined;
    userProducts: any = undefined;

    hasProduct(productType: ProductType): boolean {
        let has = false;
        this.userProducts.forEach(p => {
            if (apiToProductType(p.product) === productType) {
                has = true;
            }
        });
        return has;
    }

    productId(productType: ProductType): number {
        let id;
        this.userProducts.forEach(p => {
            if (apiToProductType(p.product) === productType) {
                id = p.id;
            }
        });
        return id;
    }

    getProduct(productType: ProductType): any {
        let product;
        this.userProducts.forEach(p => {
            if (apiToProductType(p.product) === productType) {
                product = p;
            }
        });
        return product;
    }

    removeProduct(productType: ProductType) {
        if (this.userProducts) {
            let removeIndex = null;
            this.userProducts.forEach((p, i) => {
                if (apiToProductType(p.product) === productType) {
                    removeIndex = i;
                }
            });
            if (removeIndex !== null) {
                this.userProducts.splice(removeIndex, 1);
            }
            console.log(this.userProducts);
        }
    }

    addProduct(product: UserProductModel) {
        if (!this.userProducts) {
            this.userProducts = [];
        }
        this.userProducts.push({
            id: product.id,
            product: productTypeToApi(product.product)
        });
    }

    constructor(data: any = null) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }
}
