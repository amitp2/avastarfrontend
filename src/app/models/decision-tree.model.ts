import { Status } from '../enums/status.enum';

export class DecisionTreeModel {
    id: number = undefined;
    treeId: string = undefined;
    name: string = undefined;
    description: string = undefined;
    tags: string = undefined;
    type: DecisionTreeType = undefined;
    status: Status = undefined;
    subscriberIds: number[] = undefined;

    constructor(data?) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }
}

export enum DecisionTreeType {
    Public = 'PUBLIC',
    Private = 'PRIVATE'
}
