import {VenueClientType} from '../enums/venue-client-type.enum';
import {VenueClientStatus} from '../enums/venue-client-status.enum';
import { FunctionServiceChargeCalculationType } from './function-model';

export class VenueClientModel {

    id: number = undefined;
    name: string = undefined;
    accountType: VenueClientType = undefined;
    status: VenueClientStatus = undefined;
    countryId: number = undefined;
    stateId: number = undefined;
    city: string = undefined;
    address: string = undefined;
    address2: string = undefined;
    postCode: string = undefined;
    phone: string = undefined;
    fax: string = undefined;
    branch: string = undefined;
    website: string = undefined;
    logoUrl: string = undefined;
    masterAgreementAgreed: boolean = undefined;
    venueId: number = undefined;

    standardDiscountRate: number = undefined;
    discountRateForSetupDays: number = undefined;
    alternativeServiceCharge: boolean = undefined;
    alternativeServiceChargePercentage: number = undefined;
    taxExempt: boolean = undefined;
    serviceChargeCalculation: FunctionServiceChargeCalculationType = undefined;

    constructor(data: any = null) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }

}
