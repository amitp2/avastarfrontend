import { CommandField } from '../services/command-constructor/command-field.decorator';
import { Mapper } from '../interfaces/mapper';
import { CommandFieldMapper } from '../services/command-constructor/command-field-mapper.decorator';
import { ResourceField } from '../services/resource-constructor/resource-field.decorator';
import { ResourceFieldMapper } from '../services/resource-constructor/resource-field-mapper.decorator';
import { FormField } from '../services/form-constructor/form-field';
import { Required } from '../services/form-constructor/required';
import { DefaultValue } from '../services/form-constructor/default-value';
import { IdMapper } from '../mappers/id-mapper';
import { AbstractControl, Validators } from '@angular/forms';
import { CustomValidator } from '../services/form-constructor/custom-validator';
import { ResourceConstructorService } from '../services/resource-constructor/resource-constructor.service';
import { CommandConstructorService } from '../services/command-constructor/command-constructor.service';
import { NumberMapper } from '../mappers/number-mapper';
import { BooleanMapper } from '../mappers/boolean-mapper';
import { Min } from '../services/form-constructor/min';
import { Max } from '../services/form-constructor/max';
import { Copyable } from '../helpers/copyable';
import { DateToUtcTimestampMapper } from '../mappers/date-to-utc-timestamp-mapper';
import { UtcToDateStringMapper } from '../mappers/utc-to-date-string-mapper';

export enum FunctionType {
    MainGroup = 'Main Group', // MAIN_GROUP
    Affiliate = 'Affiliate', // AFFILIATE
    Exhibitor = 'Exhibitor' // EXHIBITOR
}

export enum FunctionServiceChargeCalculationType {
    PreDiscounted = 'Pre Discounted Pricing',
    PostDiscounted = 'Post Discounted Pricing'
}

export enum FunctionStatus {
    Active = 'Active',
    Inactive = 'Inactive',
    Definite = 'Definite',
    Tenative = 'Tentative',
    Proposal = 'Proposal'
}

export interface FunctionDay {
    setTime: string;
    startTime: string;
    endTime: string;
    beo: string;
    workflowNote: string;
    clientNote: string;
    houseHold24: boolean;
    setupDay: boolean;
}

export interface FunctionEquipmentDate {
    date: any;
    quantity: number;
    setupDay: boolean;
}

export interface FunctionEquipment {
    name: string;
    saved: boolean;
    equipmentServiceId: number;
    equipmentPackageId: number;
    equipmentSystemId: number;
    price: number;
    eventFunctionEquipmentDates: FunctionEquipmentDate[];
    code: string;
    totalPrice: number;
    equipment: any;
    service: any;
    pos: number;
    id: any;

    priceTotalRaw: any;
    appliedDiscountTotal: any;
    appliedServiceChargeTotal: any;
    appliedTaxTotal: any;

    calcTotal(): number;

    calcExtPrice(): number;
}

export class FunctionStatusToCommandStatusMapper implements Mapper<FunctionStatus, any> {

    get mappings(): any {
        return {
            [FunctionStatus.Inactive]: 'LOCKED',
            [FunctionStatus.Active]: 'ACTIVE',
            [FunctionStatus.Definite]: 'DEFINITIVE',
            [FunctionStatus.Proposal]: 'PROPOSAL',
            [FunctionStatus.Tenative]: 'TENTATIVE'
        };
    }

    map(from: FunctionStatus): any {
        return this.mappings[from];
    }
}

export class ResourceStatusToFunctionStatusMapper implements Mapper<any, FunctionStatus> {

    get mappings(): any {
        return {
            'LOCKED': FunctionStatus.Inactive,
            'ACTIVE': FunctionStatus.Active,
            'DEFINITIVE': FunctionStatus.Definite,
            'PROPOSAL': FunctionStatus.Proposal,
            'TENTATIVE': FunctionStatus.Tenative
        };
    }

    map(from: FunctionStatus): any {
        return this.mappings[from];
    }

}

export class FunctionTypeToCommandTypeMapper implements Mapper<FunctionType, any> {

    get mappings(): any {
        return {
            [FunctionType.Affiliate]: 'AFFILLIATE',
            [FunctionType.MainGroup]: 'MAIN_GROUP',
            [FunctionType.Exhibitor]: 'EXHIBITOR'
        };
    }

    map(from: FunctionType): any {
        return this.mappings[from];
    }

}

export class ResourceTypeMapperToFunctionType implements Mapper<any, FunctionType> {

    get mappings(): any {
        return {
            'AFFILLIATE': FunctionType.Affiliate,
            'MAIN_GROUP': FunctionType.MainGroup,
            'EXHIBITOR': FunctionType.Exhibitor
        };
    }

    map(from: FunctionType): FunctionType {
        return this.mappings[from];
    }

}

export class FunctionCalculationTypeToCommandCalculationType implements Mapper<FunctionServiceChargeCalculationType, any> {

    get mappings(): any {
        return {
            [FunctionServiceChargeCalculationType.PostDiscounted]: 'POST_DISCOUNT_PRICE',
            [FunctionServiceChargeCalculationType.PreDiscounted]: 'PRE_DISCOUNT_PRICE'
        };
    }

    map(from: FunctionServiceChargeCalculationType): any {
        return this.mappings[from];
    }
}

export class ResourceCalculationTypeToFunctionCalculationType implements Mapper<any, FunctionServiceChargeCalculationType> {

    get mappings(): any {
        return {
            'POST_DISCOUNT_PRICE': FunctionServiceChargeCalculationType.PostDiscounted,
            'PRE_DISCOUNT_PRICE': FunctionServiceChargeCalculationType.PreDiscounted
        };
    }

    map(from: FunctionServiceChargeCalculationType): FunctionServiceChargeCalculationType {
        return this.mappings[from];
    }
}

export class ResourceDaysToFunctionDaysMapper implements Mapper<any, FunctionDay[]> {

    map(from: any): FunctionDay[] {
        if (from && from.length > 0) {
            return from.map(day => ResourceConstructorService.build(day, FunctionDayModel));
        }
        return [];
    }
}

export class ResourceEquipmentsToFunctionEquipmentsMapper implements Mapper<any, FunctionEquipment[]> {

    map(from: any): FunctionEquipment[] {
        if (from && from.length > 0) {
            const res = from.map(eq => ResourceConstructorService.build(eq, FunctionEquipmentModel));
            res.forEach(r => r.saved = true);
            return res;
        }
        return [];
    }
}

export class FunctionEquipmentsToCommandEquipments implements Mapper<FunctionEquipment[], any> {

    map(from: FunctionEquipment[]): any {
        if (from && from.length > 0) {
            return from.map(eq => CommandConstructorService.build(eq));
        }
        return [];
    }
}

export class ResourceDateToFunctionDateMapper implements Mapper<any, FunctionEquipmentDate[]> {

    map(from: any): FunctionEquipmentDate[] {
        if (from && from.length > 0) {
            return from.map(date => ResourceConstructorService.build(date, FunctionEquipmentDateModel));
        }
        return [];
    }
}

export class EquipmentDateToCommandDate implements Mapper<FunctionEquipmentDate[], any> {

    map(from: FunctionEquipmentDate[]): any {
        if (from && from.length > 0) {
            return from.map(date => CommandConstructorService.build(date));
        }
        return [];
    }
}


export function ValidFunctionDays(control: AbstractControl) {
    const days = control.value;
    let invalid = false;
    if (days && days.length > 0) {
        days.forEach((day: FunctionDay) => {
            if (!day.setTime || !day.startTime || !day.endTime) {
                invalid = true;
            }
        });

        if (invalid) {
            return {
                days: 'some of the days are missing required fields'
            };
        }
        return null;
    }
    return {
        days: 'days should not be empty'
    };
}

export class FunctionDayModel implements FunctionDay {

    @Required
    @FormField
    @ResourceField()
    setTime: string;

    @Required
    @FormField
    @ResourceField()
    startTime: string;

    @Required
    @FormField
    @ResourceField()
    endTime: string;

    @FormField
    @ResourceField()
    beo: string;

    @FormField
    @ResourceField()
    workflowNote: string;

    @FormField
    @ResourceField()
    clientNote: string;

    @DefaultValue(false)
    @FormField
    @ResourceField()
    houseHold24: boolean;

    @DefaultValue(false)
    @FormField
    @ResourceField()
    setupDay: boolean;
}

export class FunctionEquipmentDateModel implements FunctionEquipmentDate {

    @DefaultValue(false)
    @ResourceField()
    @CommandFieldMapper(BooleanMapper)
    @CommandField()
    setupDay: boolean;

    @ResourceField()
    @CommandField()
    date: any;

    @ResourceField()
    @CommandFieldMapper(NumberMapper)
    @CommandField()
    quantity: number;

}

export class FunctionEquipmentModel implements FunctionEquipment {

    id: any;

    name: string;

    saved: boolean;

    @ResourceField('equipmentService')
    service: any;

    @ResourceField('equipmentPackage')
    equipment: any;

    @ResourceFieldMapper(IdMapper)
    @ResourceField('equipmentService')
    @CommandField()
    equipmentServiceId: number;

    @ResourceFieldMapper(IdMapper)
    @ResourceField('equipmentPackage')
    @CommandField()
    equipmentPackageId: number;

    @ResourceFieldMapper(IdMapper)
    @ResourceField('equipmentSystem')
    @CommandField()
    equipmentSystemId: number;

    @ResourceField()
    @CommandField()
    price: number;

    @ResourceFieldMapper(ResourceDateToFunctionDateMapper)
    @ResourceField()
    @CommandFieldMapper(EquipmentDateToCommandDate)
    @CommandField()
    eventFunctionEquipmentDates: FunctionEquipmentDate[];

    @Required
    @FormField
    @ResourceField('code')
    @CommandField()
    code: string;

    // @Required
    // @FormField
    // @ResourceFieldMapper(IdMapper)
    // @ResourceField('functionSpace')
    // @CommandField()
    // functionSpaceId: number;

    @ResourceField('priceTotal')
    totalPrice: number;

    @CommandField()
    @ResourceField()
    pos: number;

    @ResourceField()
    priceTotalRaw: any;

    @ResourceField()
    appliedDiscountTotal: any;

    @ResourceField()
    appliedServiceChargeTotal: any;

    @ResourceField()
    appliedTaxTotal: any;

    calcTotal(): number {
        return this.eventFunctionEquipmentDates
            .map((item) => +item.quantity)
            .reduce((acc, cur) => acc + cur, 0);
    }

    calcExtPrice(): any {
        if (!this.price) {
            return '';
        }
        if (!this.calcTotal()) {
            return '';
        }
        return this.price * this.calcTotal();
    }
}

// console.log((new DateToUtcTimestampMapper()).map(1524556800000));

export class FunctionModel {

    @ResourceField()
    id: number;

    @Required
    @DefaultValue(FunctionStatus.Active)
    @FormField
    @ResourceFieldMapper(ResourceStatusToFunctionStatusMapper)
    @ResourceField()
    @CommandFieldMapper(FunctionStatusToCommandStatusMapper)
    @CommandField()
    status: FunctionStatus;

    @Required
    @FormField
    @ResourceFieldMapper(IdMapper)
    @ResourceField('client')
    @CommandField('client_id')
    clientId: number;

    @Required
    @FormField
    @ResourceField()
    @CommandField('event_id')
    eventId: number;

    @FormField
    @ResourceField()
    @CommandField()
    masterAccountNumber: string;

    @FormField
    @ResourceField()
    @CommandField()
    eventCode: string;

    @Required
    @FormField
    @ResourceFieldMapper(IdMapper)
    @ResourceField('functionSpace')
    @CommandField()
    functionSpaceId: number;

    @FormField
    @ResourceField()
    @CommandField()
    locationNote: string;

    @FormField
    @ResourceField()
    @CommandField()
    functionName: string;

    @FormField
    @ResourceField()
    @CommandField()
    planningContact: string;

    @FormField
    @ResourceField()
    @CommandField()
    salesContact: string;

    @Required
    @FormField
    @ResourceFieldMapper(ResourceTypeMapperToFunctionType)
    @ResourceField()
    @CommandFieldMapper(FunctionTypeToCommandTypeMapper)
    @CommandField()
    type: FunctionType;

    @Required
    @FormField
    @ResourceFieldMapper(UtcToDateStringMapper)
    @ResourceField()
    @CommandFieldMapper(DateToUtcTimestampMapper)
    @CommandField()
    startDate: string;

    @Required
    @FormField
    @ResourceFieldMapper(UtcToDateStringMapper)
    @ResourceField()
    @CommandFieldMapper(DateToUtcTimestampMapper)
    @CommandField()
    endDate: string;

    @Max(100)
    @Min(0)
    @FormField
    @ResourceField()
    @CommandField()
    standardDiscountRate: number;

    @Max(100)
    @Min(0)
    @FormField
    @ResourceField()
    @CommandField()
    discountRateForSetupDays: number;

    @FormField
    @ResourceField()
    @CommandField()
    alternativeServiceCharge: boolean;

    @Max(100)
    @Min(0)
    @FormField
    @ResourceField()
    @CommandField()
    alternativeServiceChargePercentage: number;

    @FormField
    @ResourceField()
    @CommandField()
    taxExempt: boolean;

    @Required
    @DefaultValue(FunctionServiceChargeCalculationType.PostDiscounted)
    @FormField
    @ResourceFieldMapper(ResourceCalculationTypeToFunctionCalculationType)
    @ResourceField()
    @CommandFieldMapper(FunctionCalculationTypeToCommandCalculationType)
    @CommandField()
    serviceChargeCalculation: FunctionServiceChargeCalculationType;

    @CustomValidator(ValidFunctionDays)
    @DefaultValue([])
    @FormField
    @ResourceFieldMapper(ResourceDaysToFunctionDaysMapper)
    @ResourceField()
    @CommandField()
    eventFunctionDates: FunctionDay[];

    @DefaultValue([])
    @FormField
    @ResourceFieldMapper(ResourceEquipmentsToFunctionEquipmentsMapper)
    @ResourceField()
    @CommandFieldMapper(FunctionEquipmentsToCommandEquipments)
    @CommandField()
    eventFunctionEquipments: FunctionEquipment[];

    @ResourceField()
    functionSpace: any;

    @ResourceField()
    teoNumber: any;

    @FormField
    @ResourceField()
    @CommandField()
    @CustomValidator(Validators.maxLength(500))
    setupStyle: string;

    @FormField
    @ResourceField()
    @CommandField()
    @CustomValidator(Validators.maxLength(500))
    stagingRequirements: string;

    @FormField
    @ResourceField()
    @CommandField()
    @CustomValidator(Validators.max(99999999))
    participantsCount: number;
}
