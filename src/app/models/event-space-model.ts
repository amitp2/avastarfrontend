import { EventSpaceType } from '../enums/event-space-type.enum';

export class EventSpaceModel {

    id: number = undefined;
    name: string = undefined;
    status: EventSpaceType = undefined;
    functionSpaces: any = undefined;

    constructor(data: any = null) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }

}
