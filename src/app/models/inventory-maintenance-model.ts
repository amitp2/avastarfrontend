import { InventoryMaintenanceSchedule } from '../enums/inventory-maintenance-schedule.enum';

export class InventoryMaintenanceModel {

    warrantyExpDate: string = undefined;
    warrantyVendorId: number = undefined;
    serviceExpDate: string = undefined;
    serviceVendorId: number = undefined;
    schedule: InventoryMaintenanceSchedule = undefined;
    lastServiceDate: string = undefined;
    nextServiceDate: string = undefined;

    constructor(data) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }

}
