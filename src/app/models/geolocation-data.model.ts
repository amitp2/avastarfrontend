export class GeolocationDataModel {
    id: number = null;
    ip: string = null;
    countryCode: string = null;
    regionCode: string = null;
    countryName: string = null;
    regionName: string = null;
    city: string = null;
    zipCode: string = null;

    constructor(data?: any) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }
}
