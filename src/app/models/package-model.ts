export class PackageActualPrice {
    fromDate: string = null;
    toDate: string = null;
    price: number = null;
    creationTime: Date = null;
    updateTime: Date = null;
    fromDateAmended = false;
    toDateAmended = false;
    priceAmended = false;
    id: number = null;

    constructor(data: Partial<PackageActualPrice>, oldPrice?: Partial<PackageActualPrice>) {
        Object.keys(data).forEach(key => {
            if (this.hasOwnProperty(key)) {
                this[key] = data[key];
            }
        });
        if (oldPrice) {
            this.setAmendedValues(oldPrice);
        }
    }

    private setAmendedValues(oldPrice: Partial<PackageActualPrice>) {
        this.fromDateAmended = this.fromDate !== oldPrice.fromDate;
        this.toDateAmended = this.toDate !== oldPrice.toDate;
        this.priceAmended = this.price !== oldPrice.price;
    }
}

export class PackageModel {

    id: number = undefined;
    name: string = undefined;
    description: string = undefined;
    salesDescription: string = undefined;
    equipments: any[] = undefined;
    clientDescription = undefined;
    // equipmentIds: number[] = undefined;
    // systemIds: number[] = undefined;
    price: string = undefined;
    priceFromDate: string = undefined;
    priceToDate: string = undefined;
    pictureUrl: string = undefined;
    oldPrices: any[] = undefined;
    priceCreationDate: string = undefined;

    constructor(data) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
            data.oldPrices = data.oldPrices || [];
            const id = data.oldPrices.length === 0 ? 1 : data.oldPrices.reduce((min, b) => min > b.id ? min : b.id, 0) + 1;
            data.oldPrices.unshift(<any>{
                id,
                price: data.price,
                fromDate: data.priceFromDate,
                toDate: data.priceToDate,
                creationTime: data.priceCreationDate
            });
            const lastIndex = data.oldPrices.length - 1;
            this.oldPrices = data.oldPrices
                .sort((x, y) => x.id > y.id ? -1 : 1)
                .map((price, index) => {
                    if (index === lastIndex) {
                        return new PackageActualPrice(price);
                    }
                    return new PackageActualPrice(price, data.oldPrices[index + 1]);
                });
        }
    }

}
