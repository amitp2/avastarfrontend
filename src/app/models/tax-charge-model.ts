import { TaxChargeStatus } from "../enums/tax-charge.enum";

export class TaxChargeModel {
    id: number = undefined;
    name: string = undefined;
    startDate: string = undefined;
    endDate: string = undefined;
    revenueCode: string = undefined;
    charge: number = undefined;
    revenueCategoryId: number = undefined;
    status: TaxChargeStatus = undefined;

    constructor(data?) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }
}