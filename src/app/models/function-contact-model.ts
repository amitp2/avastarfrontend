import { ResourceField } from '../services/resource-constructor/resource-field.decorator';
import { Mapper } from '../interfaces/mapper';
import { ResourceFieldMapper } from '../services/resource-constructor/resource-field-mapper.decorator';

export class ResourceFunctionContactTypeMapper implements Mapper<any, FunctionContactType> {

    get mappings(): any {
        return {
            'PLANNING_CONTACT': FunctionContactType.Planning,
            'SALES_CONTACT': FunctionContactType.Sales
        };
    }

    map(from: any): FunctionContactType {
        return this.mappings[from];
    }

}

export class CommandFunctionContactTypeMapper implements Mapper<FunctionContactType, any> {
    get mappings(): any {
        return {
            [FunctionContactType.Planning]: 'PLANNING_CONTACT',
            [FunctionContactType.Sales]: 'SALES_CONTACT'
        };
    }

    map(from: FunctionContactType): any  {
        return this.mappings[from];
    }
}

export enum FunctionContactType {
    Planning = 'Planning Contact',
    Sales = 'Sales Contact'
}

export class FunctionContactModel {

    @ResourceField()
    id: number;

    @ResourceField()
    name: string;

    @ResourceFieldMapper(ResourceFunctionContactTypeMapper)
    @ResourceField()
    type: string;

}
