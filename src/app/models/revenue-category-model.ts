export class RevenueCategoryModel {
    id: string = undefined;
    revenueCode: string = undefined;
    discountable: boolean = undefined;
    serviceCharge: boolean = undefined;
    subscriberId: number = undefined;
    revenueCategoryName: string = undefined;
    revenueCategoryId: number = undefined;
    code: string = undefined;
    calculateCode: string = undefined;
    name: string = undefined;
    description: string = undefined;

    constructor(data?) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }

}
