import { Status } from '../enums/status.enum';

export class InventorySubcategoryItemModel {
    id: number = undefined;
    name: string = undefined;
    subCategoryId: number = undefined;
    status: Status = undefined;

    constructor(data) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }

    toJson() {
        return {
            id: this.id,
            name: this.name,
            subCategoryId: this.subCategoryId,
            status: this.status
        };
    }

    static fromJson(data: any) {
        return new InventorySubcategoryItemModel(data);
    }
}
