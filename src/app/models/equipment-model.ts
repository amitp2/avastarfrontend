import { EquipmentServiceType, EquipmentCrossRentalType, EquipmentProductInformation } from '../enums/equipment';
import { InventorySubcategoryItemModel } from './inventory-subcategory-item-model';

export class EquipmentActualPrice {
    fromDate: string = null;
    toDate: string = null;
    price: number = null;
    creationTime: Date = null;
    updateTime: Date = null;
    fromDateAmended = false;
    toDateAmended = false;
    priceAmended = false;
    id: number = null;

    constructor(data: Partial<EquipmentActualPrice>, oldPrice?: Partial<EquipmentActualPrice>) {
        Object.keys(data).forEach(key => {
            if (this.hasOwnProperty(key)) {
                this[key] = data[key];
            }
        });
        if (oldPrice) {
            this.setAmendedValues(oldPrice);
        }
    }

    private setAmendedValues(oldPrice: Partial<EquipmentActualPrice>) {
        this.fromDateAmended = this.fromDate !== oldPrice.fromDate;
        this.toDateAmended = this.toDate !== oldPrice.toDate;
        this.priceAmended = this.price !== oldPrice.price;
    }
}

export class EquipmentModel {
    id: number = null;
    type: EquipmentServiceType = null;
    name: string = null;
    clientDescription: string = null;
    excludeFromReport: boolean = null;
    workflowDescription: string = null;
    customerName: string = null;
    inventoryId: number = null;
    categoryId: number = null;
    subCategoryId: number = null;
    subCategoryItemId: number = null;
    revenueCategoryId: number = null;
    costCategoryId: number = null;
    crossRentalType: EquipmentCrossRentalType = null;
    setupTime: number = null;
    equipmentProductInformation: EquipmentProductInformation = null;
    price: number = null;
    priceFromDate: string = null;
    priceToDate: string = null;
    oldPrices: EquipmentActualPrice[] = null;
    lastPriceUpdateTime: Date = null;
    inventoryBarcode: string = undefined;
    inventoryTagNumber: string = undefined;
    subCategoryItemName: string = undefined;
    rentable: boolean = undefined;
    subCategoryItem: InventorySubcategoryItemModel = undefined;
    thirdParty = false;

    constructor(data?: Partial<EquipmentModel>) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
            data.oldPrices = data.oldPrices || [];
            if (this.rentable) {
                const id = data.oldPrices.length === 0 ? 1 : data.oldPrices.reduce((min, b) => min > b.id ? min : b.id, 0) + 1;
                data.oldPrices.unshift(<any>{
                    id,
                    price: data.price,
                    fromDate: data.priceFromDate,
                    toDate: data.priceToDate,
                    creationTime: data.lastPriceUpdateTime
                });
            }
            if (data.oldPrices.length > 1) {
                const lastIndex = data.oldPrices.length - 1;
                this.oldPrices = data.oldPrices
                    .sort((x, y) => x.id > y.id ? -1 : 1)
                    .map((price, index) => {
                        if (index === lastIndex) {
                            return new EquipmentActualPrice(price);
                        }
                        return new EquipmentActualPrice(price, data.oldPrices[index + 1]);
                    });
            }
        }
    }
}
