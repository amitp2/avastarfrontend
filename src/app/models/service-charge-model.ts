import { ServiceChargeStatus } from '../enums/service-charge.enum';

export class ServiceChargeModel {
    id: number = undefined;
    name: string = undefined;
    startDate: string = undefined;
    endDate: string = undefined;
    revenueCode: string = undefined;
    charge: number = undefined;
    taxRate:number = undefined;
    status: ServiceChargeStatus = undefined;
    revenueCategoryId: number = undefined;
    
    constructor(data?) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }
}