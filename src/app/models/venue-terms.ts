export class VenueTermsModel {

    terms: string = undefined;
    billingComment: string = undefined;
    proposalComment: string = undefined;

    constructor(data: any = null) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }

}
