export enum VenueEventStatus {
    Active = 'Active', // ACTIVE
    Inactive = 'Inactive', // LOCK
    Definite = 'Definite',
    Tenative = 'Tentative',
    Proposal = 'Proposal'
}

export enum VenueEventType {
    Group = 'Group', // MAIN_GROUP
    Local = 'Local', // AFFILIATE
    Internal = 'Internal' // EXHIBITOR
}

export class VenueEventModel {
    id: number = undefined;
    venueId: number = undefined;
    accountId: number = undefined;
    name: string = undefined;
    code: string = undefined;
    status: VenueEventStatus = undefined;
    type: VenueEventType = undefined;
    masterAccountNumber: string = undefined;
    eventDescription: string = undefined;
    clientName: string = undefined;
    functionCount: number = undefined;
    clientPrimaryContactName: string = undefined;
    eventStartDate: any = undefined;
    eventEndDate: any = undefined;
    clientId: any = undefined;

    constructor(data: any = null) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }
}

export class VenueEventContact {
    id: number;
    phone: string;
    mobile: string;
    fax: string;
    firstName: string;
    lastName: string;
    title: string;
    email: string;
    notes: string;
    department: string;

    toJson(): Partial<VenueEventContact> {
        return {
            phone: this.phone,
            mobile: this.mobile,
            fax: this.fax,
            firstName: this.firstName,
            lastName: this.lastName,
            title: this.title,
            email: this.email,
            notes: this.notes,
            department: this.department
        };
    }

    static fromJson(data: Partial<VenueEventContact>): any {
        const model = new VenueEventContact;

        model.id = data.id;
        model.phone = data.phone;
        model.mobile = data.mobile;
        model.fax = data.fax;
        model.firstName = data.firstName;
        model.lastName = data.lastName;
        model.email = data.email;
        model.notes = data.notes;
        model.title = data.title;
        model.department = data.department;

        return model;
    }
}
