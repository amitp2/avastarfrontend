import { InventoryAssetRating } from '../enums/inventory-asset-rating.enum';
import { InventoryAssetStatus } from '../enums/inventory-asset-status.enum';

export class InventoryAssetStatusModel {

    id: number = undefined;
    rating: InventoryAssetRating = undefined;
    condition: InventoryAssetStatus = undefined;
    comment: string = undefined;

    constructor(data) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }
}
