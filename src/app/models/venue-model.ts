export class VenueModel {

    id: number = undefined;
    name: string = undefined;
    subscriberId: number = undefined;
    countryId: number = undefined;
    stateId: number = undefined;
    city: string = undefined;
    address: string = undefined;
    address2: string = undefined;
    postCode: string = undefined;
    mainPhone: string = undefined;
    // directPhone: string = undefined;
    mainFax: string = undefined;
    // directFax: string = undefined;
    // branch: string = undefined;
    website: string = undefined;
    logoUrl: string = undefined;
    owner: string = undefined;
    operator: string = undefined;
    operatorContactId: number = undefined;
    ownerContactId: number = undefined;

    constructor(data: any = null) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
            if (data.ownerContact) {
                this.ownerContactId = data.ownerContact.id;
            }
            if (data.operatorContact) {
                this.operatorContactId = data.operatorContact.id;
            }
        }
    }
}
