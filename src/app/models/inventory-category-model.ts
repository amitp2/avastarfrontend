import { Status } from '../enums/status.enum';

export class InventoryCategoryModel {
    id: number = undefined;
    name: string = undefined;
    status: Status = undefined;

    constructor(data) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }

    toJson() {
        return {
            id: this.id,
            name: this.name,
            status: this.status
        };
    }

    static fromJson(data: any) {
        return new InventoryCategoryModel(data);
    }
}
