import { ResourceField } from '../services/resource-constructor/resource-field.decorator';
import { Mapper } from '../interfaces/mapper';
import { ResourceFieldMapper } from '../services/resource-constructor/resource-field-mapper.decorator';
import { CommandField } from '../services/command-constructor/command-field.decorator';
import { CommandFieldMapper } from '../services/command-constructor/command-field-mapper.decorator';
import { productTypeToApi } from '../helpers/mapper-functions/product-type-to-api.mapper-function';
import { ProductType } from '../enums/product-type.enum';

export class ApiProductToProductMapper implements Mapper<any, ProductType> {

    private mappings = {
        AVASTAR: ProductType.Aavastar,
        SYZYGY: ProductType.Syzygy
    };

    map(from: any): ProductType {
        return this.mappings[from];
    }
}

export class ProductToApiMapper implements Mapper<ProductType, any> {

    map(from: ProductType): any {
        return productTypeToApi(from);
    }
}

export interface SubscriberProductUsage {
    [product: string]: number;
}

export class ProductModel {

    @ResourceField()
    id: number;

    @ResourceFieldMapper(ApiProductToProductMapper)
    @ResourceField()
    @CommandFieldMapper(ProductToApiMapper)
    @CommandField()
    product: ProductType;

    @ResourceField()
    @CommandField()
    licensedUsersCount: number;

    @ResourceField()
    @CommandField()
    hasAccess: boolean;

    constructor(data) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }

}
