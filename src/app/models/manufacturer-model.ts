export class ManufacturerModel {
    id: number = undefined;
    name: string = undefined;
    usage: number = undefined;

    constructor(data) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }
}
