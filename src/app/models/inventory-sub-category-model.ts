export class InventorySubCategoryModel {
    id: number;
    name: string;
    categoryId: number;

    toJson() {
        return {
            id: this.id,
            name: this.name,
            categoryId: this.categoryId
        };
    }

    static fromJson(data: any) {
        const model = new InventorySubCategoryModel();
        model.id = data.id;
        model.name = data.name;
        model.categoryId = data.categoryId;
        return model;
    }
}
