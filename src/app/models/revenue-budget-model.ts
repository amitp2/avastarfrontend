export interface RevenueBudgetModel {
    id: number;
    year: number;
    annual: number;
    january: number;
    february: number;
    march: number;
    april: number;
    may: number;
    june: number;
    july: number;
    august: number;
    september: number;
    october: number;
    november: number;
    december: number;
    revenueCode: string;
    revenueCategoryName: string;
}