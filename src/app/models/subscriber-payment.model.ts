import { PaymentMethod, RenewalPeriod, SubscriptionStatus, SubscriptionType } from '../enums/subscription-type';

export class PaymentHistory {
    date: Date = undefined;
    price: number = undefined;
    status: SubscriptionStatus = undefined;

    constructor(data?) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }
}

export class PaymentInfo {
    id: number = undefined;
    accountType: SubscriptionType = undefined;
    paymentMethod: PaymentMethod = undefined;
    renewalPeriod: RenewalPeriod = undefined;
    status: SubscriptionStatus = undefined;
    subscriptionDate: Date = undefined;
    amountDue: number = undefined;
    implementationFee: number = undefined;
    initialInvoiceUrl: string = undefined;
    contractUrl: string = undefined;
    paypalId: string = undefined;
    contractNumber: string = undefined;
    customerPo: string = undefined;
    creationDate: Date = undefined;
    nextPaymentDate: Date = undefined;

    constructor(data?) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }
}
