import { StorageSpaceStatus } from '../enums/storage-space-status.enum';

export class StorageSpaceModel {

    id: number = undefined;
    name: string = undefined;
    status: StorageSpaceStatus = undefined;
    notes: string = undefined;

    constructor(data: any = null) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }

}
