export class EquipmentSystemModel {

    id: number = undefined;
    name: string = undefined;
    description: string = undefined;
    // equipmentIds: number[] = undefined;
    inventoryIds: number[] = undefined;

    constructor(data: any = null) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }

}
