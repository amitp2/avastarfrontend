import { Mapper } from '../interfaces/mapper';

export enum TicketStatus {
    All = 'All',
    Open = 'Open',
    InProgress = 'In Progress',
    Completed = 'Completed'
}

export enum TicketAssignee {
    Venue = 'VENUE',
    Vendor = 'VENDOR'
}

export class TicketStatusToApiMapper implements Mapper<TicketStatus, any> {

    get mappings(): any {
        return {
            [TicketStatus.Open]: 'OPEN',
            [TicketStatus.InProgress]: 'IN_PROGRESS',
            [TicketStatus.Completed]: 'COMPLETED'
        };
    }

    map(from: TicketStatus): any {
        return this.mappings[from];
    }
}

export class TicketModel {
    id: number = null;
    title: string = null;
    description: string = null;
    creationTime: string = null;
    systemId: number = null;
    inventoryId: number = null;
    reminderTime: string = null;
    venueContactId: any = null;
    venueId: number = null;
    vendorContactId: number = null;
    vendorContactName: string = null;
    vendorId: number = null;
    ticketStatus: TicketStatus = null;
    extras: any;
    assetName: string;
    assigneeType: TicketAssignee = null;
    assigneeName: string = null;

    get contactLink() {
        if (this.assigneeType === TicketAssignee.Vendor) {
            return `/u/vendors/${this.vendorId}/contact/${this.vendorContactId}`;
        }
        return `/u/venues/${this.venueId}/contact/${this.venueContactId}`;
    }

    constructor(data: any = null) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
            if (data.inventory) {
                this.assetName = data.inventory.subCategoryItemName;
                if (data.inventory.tagNumber) {
                    this.assetName += ' ' + data.inventory.tagNumber;
                }
            } else if (data.system) {
                this.assetName = data.system.name;
            }
        }
    }

}
