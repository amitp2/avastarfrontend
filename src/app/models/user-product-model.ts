import { ResourceField } from '../services/resource-constructor/resource-field.decorator';
import { ProductType } from '../enums/product-type.enum';
import { CommandField } from '../services/command-constructor/command-field.decorator';
import { CommandFieldMapper } from '../services/command-constructor/command-field-mapper.decorator';
import { ApiProductToProductMapper, ProductToApiMapper } from './product-model';
import { ResourceFieldMapper } from '../services/resource-constructor/resource-field-mapper.decorator';

export class UserProductModel {

    @ResourceField()
    id: any;

    @ResourceFieldMapper(ApiProductToProductMapper)
    @ResourceField()
    @CommandFieldMapper(ProductToApiMapper)
    @CommandField()
    product: ProductType;

}
