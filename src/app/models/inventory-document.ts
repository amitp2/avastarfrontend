export class InventoryDocument {

    id: number = undefined;
    name: string = undefined;
    url: string = undefined;
    fileName: string = undefined;

    constructor(data) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }
}
