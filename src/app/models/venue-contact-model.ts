import { UserRole } from '../enums/user-role.enum';

export class VenueContactModel {

    id: number = undefined;
    firstName: string = undefined;
    lastName: string = undefined;
    title: string = undefined;
    email: string = undefined;
    mobile: string = undefined;
    phone: string = undefined;
    fax: string = undefined;
    role: UserRole = undefined;
    notes: string = undefined;
    type: string = undefined;
    department: string = undefined;

    constructor(data: any = null) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }

}
