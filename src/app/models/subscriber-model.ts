import { AccountType } from '../enums/account-type.enum';
import { SubscriberStatus } from '../enums/subscriber-status.enum';

export class SubscriberModel {
    id: number = undefined;
    name: string = undefined;
    accountType: AccountType = undefined;
    status: SubscriberStatus = undefined;
    countryId: number = undefined;
    stateId: number = undefined;
    city: string = undefined;
    address: string = undefined;
    address2: string = undefined;
    postCode: string = undefined;
    phone: string = undefined;
    fax: string = undefined;
    branch: string = undefined;
    website: string = undefined;
    logoUrl: string = undefined;
    masterAgreementAgreed: boolean = undefined;
    usersCount: number = undefined;

    constructor(data: any = null) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }
}
