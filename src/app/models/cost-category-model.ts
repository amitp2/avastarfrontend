export class CostCategoryModel {

    id: string = undefined;
    costCode: string = undefined;
    costCategoryId: string = undefined;
    costCategoryName: string = undefined;
    name: string = undefined;
    description: string = undefined;

    constructor(data) {
        if (data) {
            Object.keys(data).forEach(key => {
                if (this.hasOwnProperty(key)) {
                    this[key] = data[key];
                }
            });
        }
    }

}
