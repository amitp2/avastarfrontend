import { isSet } from '../helper-functions';

declare var R: any;

export const isNotSet = R.pipe(isSet, R.not);
