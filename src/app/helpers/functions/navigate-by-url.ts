import { Router } from '@angular/router';

export const navigateByUrl = (router: Router, url: string) => router.navigateByUrl(url);
