import { isSet } from '../helper-functions';

export const isSetProp = (obj: any, prop: string) => isSet(obj[prop]);
