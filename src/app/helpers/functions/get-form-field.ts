import { FormGroup } from '@angular/forms';

export const getFormField = (form: FormGroup, fieldKey: string) => form.get(fieldKey);
