import { toNumber } from './to-number';
import { isValidNumber } from './is-valid-number';

declare var R: any;

export function currencyStringToNumber(str: string): number | '' {
    const replaceSymbol = R.replace('$', '');
    const replaceComma = R.replace(',', '');
    const convertString = R.pipe(replaceSymbol, replaceComma, R.trim, toNumber);
    const convert = R.cond([
        [isValidNumber, R.identity],
        [R.isNil, R.always('')],
        [R.T, convertString]
    ]);

    return convert(str);
}
