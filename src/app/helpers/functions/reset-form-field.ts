import { FormControl } from '@angular/forms';

declare var R: any;

export const resetFormField = (form: FormControl) => form.setValue(null);
