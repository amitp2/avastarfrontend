export const urlHasQueryParams = (url: string) => url.indexOf('?') !== -1;
