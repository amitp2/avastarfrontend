import { FormControl } from '@angular/forms';

export const setFormFieldValue = (field: FormControl, value: any) => field.setValue(value);
