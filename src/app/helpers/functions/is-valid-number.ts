declare var R: any;
export const isValidNumber = R.both(R.is(Number), R.complement(R.equals(NaN)));
