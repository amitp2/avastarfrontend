export const getUrlParamString = (param: string, value: string) => `${param}=${value}`;
