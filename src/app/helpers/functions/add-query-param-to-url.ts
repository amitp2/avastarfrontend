import { urlHasQueryParams } from './url-has-query-params';
import { getUrlParamString } from './get-url-param-string';

declare var R: any;

export const addQueryParamToUrl = (url: string, key: string, value: string) => {
    const paramString = R.partial(getUrlParamString, [key, value]);
    const addParamString = (str: string) => `${str}${paramString()}`;
    const addQuestionMark = R.partialRight(R.concat, ['?']);
    const addAmpersand = R.partialRight(R.concat, ['&']);
    const addSeparator = R.ifElse(urlHasQueryParams, addAmpersand, addQuestionMark);
    const add = R.pipe(addSeparator, addParamString);

    return add(url);
};
