import { MapperFunction } from '../../interfaces/mapper-function';
import { ProductType } from '../../enums/product-type.enum';

export const apiToProductType: MapperFunction<any, ProductType> = function (from: any) {
    const mappings = {
        AVASTAR: ProductType.Aavastar,
        SYZYGY: ProductType.Syzygy
    };
    return mappings[from];
};
