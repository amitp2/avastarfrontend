import { MapperFunction } from '../../interfaces/mapper-function';
import { ProductType } from '../../enums/product-type.enum';

export const productTypeToApi: MapperFunction<ProductType, string> = function (from: ProductType) {
    const mappings = {
        [ProductType.Aavastar]: 'AVASTAR',
        [ProductType.Syzygy]: 'SYZYGY'
    };
    return mappings[from];
};
