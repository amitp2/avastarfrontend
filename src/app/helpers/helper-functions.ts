import { ActivatedRoute } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';

declare var R: any;

export const getRandomInt = (min: number, max: number) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const getRandomIntFromZeroTo = R.partial(getRandomInt, [0]);

export const getNthCharFromString = (str: string, n: number) => str.charAt(n);

export const getRandomCharFromString = (str: string) => {
    const getNthChar = R.partial(getNthCharFromString, [str]);
    const get = R.pipe(R.prop('length'), R.dec, getRandomIntFromZeroTo, getNthChar);
    const res = get(str);
    return res;
};

export const randomString = (length: number) => {
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const getRandomChar = () => getRandomCharFromString(possible);
    const random = R.pipe(R.range(0), R.map(getRandomChar), R.join(''));
    const res = random(length);
    return res;
};

export const backToUrlExists = (route: ActivatedRoute): boolean => !!route.snapshot.queryParams.backToUrl;

export const getBackToUrl = (route: ActivatedRoute): string => decodeURIComponent(route.snapshot.queryParams.backToUrl);

export const getIdParam = (route: ActivatedRoute): any => route.snapshot.params.id;

export const formModeIsEdit = (id: any) => id !== 'add';

export const isSet = R.pipe(R.isNil, R.not);

export const percentFormControl = () => new FormControl(null, [Validators.min(0), Validators.max(100)]);
