export interface KeyValueStorage<K, V> {

    put(key: K, value: V): Promise<void>;
    get(key: K): Promise<V>;
    drop(key: K): Promise<void>;

}
