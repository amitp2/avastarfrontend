import { Cloner } from '../../interfaces/cloner';
import { FunctionEquipmentModel } from '../../models/function-model';

export const functionEquipmentModelCloner: Cloner<FunctionEquipmentModel> = function (from: FunctionEquipmentModel) {

    const res = new FunctionEquipmentModel();

    res.name = from.name;
    res.equipmentServiceId = from.equipmentServiceId;
    res.equipmentPackageId = from.equipmentPackageId;
    res.equipmentSystemId = from.equipmentSystemId;
    res.service = from.service;
    res.equipment = from.equipment;
    res.price = from.price;
    res.eventFunctionEquipmentDates = from.eventFunctionEquipmentDates;
    res.code = from.code;
    res.totalPrice = from.totalPrice;

    return res;
};
