import { Cloner } from '../../interfaces/cloner';
import { ProductModel } from '../../models/product-model';

export const productModelCloner: Cloner<ProductModel> = function (from: ProductModel) {
    const res = new ProductModel({});
    res.hasAccess = from.hasAccess;
    res.id = from.id;
    res.licensedUsersCount = from.licensedUsersCount;
    res.product = from.product;
    return res;
};
