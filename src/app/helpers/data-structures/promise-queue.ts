export class PromiseQueue {

    private queue = [];
    private ongoing = null;

    constructor() {
    }

    private runTask() {
        if (!this.ongoing) {
            const task = this.queue.shift();
            if (task) {
                this.ongoing = task();
                this.ongoing.then(() => {
                    this.runTask();
                });
            }
        }
        return this.ongoing;
    }

    push(task: any) {
        this.queue.push(task);
        return this.runTask();
    }

}
