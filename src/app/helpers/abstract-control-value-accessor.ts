import { ControlValueAccessor } from '@angular/forms';
import { AsyncValue2 } from './async-value-2';

export abstract class AbstractControlValueAccessor implements ControlValueAccessor {

    private onTouchedInner: any;
    private onChangeInner: any;

    valuesFromOutside: AsyncValue2<any>;
    isDisabled: AsyncValue2<boolean>;

    constructor() {
        this.valuesFromOutside = new AsyncValue2<any>(null);
        this.isDisabled = new AsyncValue2<boolean>(false);
    }

    abstract changeOutside(obj: any): void;

    setDisabledState(isDisabled: boolean): void {
        this.isDisabled.value = isDisabled;
    }

    writeValue(obj: any): void {
        this.valuesFromOutside.value = obj;
    }

    registerOnChange(fn: any): void {
        this.onChangeInner = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouchedInner = fn;
    }

    onChange(val: any) {
        if (!this.onChangeInner) {
            console.log('on change hasnt registered yet');
            return;
        }
        this.onChangeInner(val);
    }

    onTouched() {
        if (this.onTouchedInner) {
            this.onTouchedInner();
        } else {
            console.log('On touched hasn\'t registered yet');
        }
    }

}
