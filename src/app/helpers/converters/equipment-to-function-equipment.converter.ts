import { Converter } from '../converter';
import { EquipmentModel } from '../../models/equipment-model';
import { FunctionEquipmentModel } from '../../models/function-model';
import { Injectable } from '@angular/core';
import { RevenueCategoryResourceService } from '../../services/resources/revenue-category-resource.service';
import { AuthService } from '../../services/auth.service';

declare var R: any;

@Injectable()
export class EquipmentToFunctionEquipmentConverter implements Converter<EquipmentModel, FunctionEquipmentModel> {

    constructor(private revenueCategory: RevenueCategoryResourceService,
                private auth: AuthService) {
    }

    async convert(from: EquipmentModel): Promise<FunctionEquipmentModel> {
        const res = new FunctionEquipmentModel();
        res.price = from.price;
        res.name = from.clientDescription;
        res.equipmentServiceId = from.id;
        res.eventFunctionEquipmentDates = [];

        const currentUser = await this.auth.currentUser();
        const subscriberId = currentUser.subscriberId;
        const categories = await this.revenueCategory.getAllBySubscriberId(subscriberId);
        const getCode = R.pipe(R.find(R.propEq('revenueCategoryId', from.revenueCategoryId)), R.prop('code'));
        res.code = getCode(categories);
        return res;
    }

}
