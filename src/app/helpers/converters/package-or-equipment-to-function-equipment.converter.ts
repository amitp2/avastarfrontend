import { Converter } from '../converter';
import { PackageModel } from '../../models/package-model';
import { EquipmentModel } from '../../models/equipment-model';
import { FunctionEquipmentModel } from '../../models/function-model';
import { Injectable } from '@angular/core';
import { PackageToFunctionEquipmentConverter } from './package-to-function-equipment.converter';
import { EquipmentToFunctionEquipmentConverter } from './equipment-to-function-equipment.converter';

declare var R: any;

@Injectable()
export class PackageOrEquipmentToFunctionEquipmentConverter implements Converter<PackageModel | EquipmentModel, FunctionEquipmentModel> {

    constructor(private packageConverter: PackageToFunctionEquipmentConverter,
                private equipmentConverter: EquipmentToFunctionEquipmentConverter) {

    }

    async convert(from: PackageModel | EquipmentModel): Promise<FunctionEquipmentModel> {
        const isPackage = R.pipe(R.has('type'), R.not);
        const convertEquipment = (eq: EquipmentModel) => this.equipmentConverter.convert(eq);
        const convertPackage = (pck: PackageModel) => this.packageConverter.convert(pck);
        const toFunctionEquipment = R.ifElse(isPackage, convertPackage, convertEquipment);
        const res = await toFunctionEquipment(from);
        return res;
    }

}
