import { Converter } from '../converter';
import { PackageModel } from '../../models/package-model';
import { FunctionEquipmentModel } from '../../models/function-model';
import { Injectable } from '@angular/core';

@Injectable()
export class PackageToFunctionEquipmentConverter implements Converter<PackageModel, FunctionEquipmentModel> {


    async convert(from: PackageModel): Promise<FunctionEquipmentModel> {
        const res = new FunctionEquipmentModel();
        res.price = +from.price;
        res.name = from.name;
        res.equipmentPackageId = from.id;
        res.eventFunctionEquipmentDates = [];
        return res;
    }

}
