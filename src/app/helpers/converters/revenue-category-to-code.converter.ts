import { Injectable } from '@angular/core';
import { Converter } from '../converter';
import { RevenueCategoryModel } from '../../models/revenue-category-model';

@Injectable()
export class RevenueCategoryToCodeConverter implements  Converter<RevenueCategoryModel, string> {


    async convert(from: RevenueCategoryModel): Promise<string> {
        return from.code;
    }
}
