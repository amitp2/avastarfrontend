import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/distinct';


export class AsyncValue2<T> {

    private subject: BehaviorSubject<T>;
    private currentValue: T;

    get value(): T {
        return this.currentValue;
    }

    set value(val: T) {
        this.currentValue = val;
        this.subject.next(val);
    }

    readonly changes: Observable<T>;

    constructor(value: T) {
        this.currentValue = value;
        this.subject = new BehaviorSubject<T>(value);
        this.changes = this.subject.distinct();
    }

}
