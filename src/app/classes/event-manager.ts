import { Subject } from 'rxjs/Subject';

export class EventManager {

    private tracker: any;
    private SubjectType: any;

    constructor(subjectType: any = Subject) {
        this.tracker = {};
        this.SubjectType = subjectType;
    }

    private attach(task: string): void {
        if (!this.tracker[task]) {
            this.tracker[task] = new this.SubjectType();
        }
    }

    protected eventsFor(task: string): Subject<any> {
        this.attach(task);
        return this.tracker[task];
    }

}
