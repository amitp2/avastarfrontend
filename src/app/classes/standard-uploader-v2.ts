import { AuthService } from '../services/auth.service';
import { StandardUploader } from './standard-uploader';
import { FileUploaderProgress } from '../interfaces/file-uploader-progress';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { UploadType } from '../enums/upload-type.enum';
import Axios, { AxiosResponse } from 'axios';

export abstract class StandardUploaderV2 extends StandardUploader {

    constructor(private auth: AuthService) {
        super(auth);
    }

    upload(file: any): Observable<FileUploaderProgress> {
        const progress = new BehaviorSubject<FileUploaderProgress>({completed: 1, fileUrl: ''});
        const uploadFormData = new FormData();
        const config = {
            onUploadProgress (progressEvent) {
                const completed = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                if (completed >= 100) {
                    return;
                }
                progress.next({
                    completed,
                    fileUrl: ''
                });
            },
            headers: {
                Authorization: `Bearer ${this.auth.getToken()}`
            }
        };

        uploadFormData.append('file', file, file.name && file.name.split(' ').join('-'));
        if (this.uploadType === UploadType.Update) {
            uploadFormData.append('oldUrl', this.oldUrl);
        }

        Axios.put(
            this.uploadUrl(),
            uploadFormData,
            config
        ).then((resp: AxiosResponse) => {
            progress.next({
                completed: 100,
                fileUrl: resp.data.url
            });
        }).catch(() => {
            progress.error(null);
        });

        return progress;
    }
}
