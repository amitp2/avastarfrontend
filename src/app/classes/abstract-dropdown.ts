import { Input, OnInit } from '@angular/core';
import { DropdownLoader } from '../interfaces/dropdown-loader';
import { DropdownItem } from '../interfaces/dropdown-item';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export abstract class AbstractDropdown implements OnInit {

    @Input() loadFunction: DropdownLoader;
    @Input() defaultValue: any;
    @Input() addAllOption = false;
    @Input() pleaseSelect = false;

    items: DropdownItem[];
    private loaded: BehaviorSubject<DropdownItem[]>;

    constructor() {
        this.loaded = new BehaviorSubject<DropdownItem[]>(null);
    }

    ngOnInit() {
        this.load();
    }

    private load(): void {
        try {
            this.loadFunction().then((items: DropdownItem[]) => {
                if (this.pleaseSelect) {
                    items = [{value: null, display: 'Please select...'}].concat(items);
                }
                if (this.addAllOption) {
                    items =  [{value: '', display: 'All'}].concat(items);
                }
                this.items = items;
                this.loaded.next(this.items);
            });
        } catch (exc) {
            // console.log(exc);
            console.log('load function doesn\'t exist for dropdown');
        }
    }

    protected loadings(): Observable<DropdownItem[]> {
        return this.loaded.filter(item => item ? true : false);
    }

    refresh() {
        this.load();
    }

}
