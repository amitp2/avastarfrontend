import { FormBuilder, FormGroup } from '@angular/forms';

export abstract class AbstractInlineForm<T> {
    protected forms: Map<number, FormGroup> = new Map<number, FormGroup>();
    protected isSubmitting = false;

    constructor(protected formBuilder?: FormBuilder) {
    }

    abstract buildForm(): FormGroup;

    abstract save(id: number, model: T): Promise<any>;

    abstract serialize(model: T): any;

    abstract deserialize(formValue: any): T;

    edit(model: T) {
        const form = this.buildForm();
        form['entityId'] = model['id'];
        form.patchValue(this.serialize(model));
        form.markAsPristine();
        this.forms.set(model['id'], form);
    }

    submit(id: number): any {
        if (this.isSubmitting || !this.has(id) || this.get(id).invalid) {
            return;
        }
        this.isSubmitting = true;

        const model = this.deserialize(this.get(id).value);

        return this.save(id, model)
            .then((data) => {
                this.isSubmitting = false;
                this.cancel(id);
            })
            .catch(e => this.isSubmitting = false);
    }

    has(id: number) {
        return this.forms.has(id);
    }

    get(id: number) {
        return this.forms.get(id);
    }

    cancel(id: number, message?: string) {
        if (this.has(id) && this.get(id).dirty && message) {
            if (confirm(message)) {
                this.forms.delete(id);
            }
        } else {
            this.forms.delete(id);
        }
    }

    hasChanges() {
        return Array.from(this.forms.values()).some(x => x.pristine === false);
    }

    clear() {
        this.forms.clear();
    }
}
