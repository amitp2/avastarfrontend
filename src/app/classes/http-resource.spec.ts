import { HttpResource } from './http-resource';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ResourceQueryResponse } from '../interfaces/resource-query-response';
import { AppError } from '../enums/app-error.enum';

const resourceUrl = 'http://some-api.com/api';

interface TestType {
    title: string;
    age: number;
}

class TestResource extends HttpResource<TestType> {


    constructor(httpResource: HttpClient) {
        super(httpResource);
    }

    resourceUrl(): string {
        return resourceUrl;
    }

    deserialize(data: any): TestType {
        return {
            title: data.title,
            age: 25
        };
    }

    serialize(data: TestType) {
        return {
            title: data.title
        };
    }

}


const matchingFunction = (r: HttpRequest<any>): boolean => {
    return resourceUrl === r.url;
};

describe('HttpResource', () => {

    let testResource: TestResource;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ]
        });

        testResource = new TestResource(TestBed.get(HttpClient));
        httpMock = TestBed.get(HttpTestingController);
    });

    it('get should send get request to resource/:id', () => {
        testResource.get(5);
        const req = httpMock.expectOne(`${resourceUrl}/5`);
        expect(req.request.method).toEqual('GET');
    });

    it('get should resolve to deserialized response', fakeAsync(() => {
        let res: TestType;
        testResource.get(5).then(resp => res = resp);
        httpMock.expectOne(`${resourceUrl}/5`).flush({title: 'gio', age: 30});
        tick();
        expect(res).toEqual({title: 'gio', age: 25});
    }));

    it('get should reject when request fails', fakeAsync(() => {
        let rejected = false;
        testResource.get(5).catch(() => rejected = true);
        httpMock.expectOne(`${resourceUrl}/5`).error(null);
        tick();
        expect(rejected).toEqual(true);
    }));


    it('query should send get request to resource/ with necessary params', () => {
        testResource.query({
            page: 1,
            size: 10,
            sortFields: ['name', 'age'],
            sortAsc: true,
            search: 'some search'
        });

        const req = httpMock.expectOne(matchingFunction);

        expect(req.request.method).toEqual('GET');
        expect(req.request.params.get('page')).toEqual('1');
        expect(req.request.params.get('size')).toEqual('10');
        expect(req.request.params.get('sort')).toEqual('name,age,asc');
        expect(req.request.params.get('search')).toEqual('some search');
    });

    it('query should resolve to proper resource-query-response', fakeAsync(() => {

        let res: ResourceQueryResponse<TestType>;

        testResource.query({
            page: 1,
            size: 10,
            sortFields: ['name', 'age'],
            sortAsc: false
        }).then(resp => res = resp);

        httpMock.expectOne(matchingFunction).flush({
            content: [{title: 'gio'}, {title: 'nika'}],
            totalElements: 20
        });

        tick();

        expect(res).toEqual({
            total: 20,
            items: [
                {title: 'gio', age: 25},
                {title: 'nika', age: 25}
            ]
        });

    }));

    it('query should reject when request fails', fakeAsync(() => {
        let rejected = false;
        testResource.query({
            page: 1,
            size: 10,
            sortFields: ['name', 'age'],
            sortAsc: false
        }).catch(() => rejected = true);
        httpMock.expectOne(matchingFunction).error(null);
        tick();
        expect(rejected).toEqual(true);
    }));


    it('delete should send delete request to resource/:id', () => {
        testResource.delete(5);
        const req = httpMock.expectOne(`${resourceUrl}/5`);
        expect(req.request.method).toEqual('DELETE');
    });

    it('delete should resolve if request succeeds', fakeAsync(() => {
        let resolved = false;
        testResource.delete(5).then(() => resolved = true);
        httpMock.expectOne(`${resourceUrl}/5`).flush({});
        tick();
        expect(resolved).toEqual(true);
    }));

    it('delete should reject if request fails', fakeAsync(() => {
        let rejected = false;
        testResource.delete(5).catch(() => rejected = true);
        httpMock.expectOne(`${resourceUrl}/5`).error(null);
        tick();
        expect(rejected).toEqual(true);
    }));


    it('add should send post request to resource/ with necessary params', () => {

        testResource.add({title: 'gio', age: 30});

        const req = httpMock.expectOne(`${resourceUrl}`);

        expect(req.request.method).toEqual('POST');
        expect(req.request.body.title).toEqual('gio');
        expect(req.request.body.age).toBeFalsy();
    });

    it('add should return error when something goes wrong', fakeAsync(() => {
        let err;
        testResource.add({title: 'gio', age: 30}).catch(error => err = error);
        httpMock.expectOne(`${resourceUrl}`).flush(JSON.stringify({errorCode: 1003}), {
            status: 400,
            statusText: 'error'
        });
        tick();
        expect(err).toEqual(AppError.USERNAME_ALREADY_EXISTS);
    }));

    it('after successful add it should issue new request to location header and resolve returned data', fakeAsync(() => {
        const newResourceLocation = 'http://loc/';
        let res: TestType;
        testResource.add({title: 'gio', age: 30}).then(resp => res = resp);

        httpMock.expectOne(`${resourceUrl}`).flush('', {
            headers: (new HttpHeaders()).set('location', newResourceLocation)
        });

        tick();
        httpMock.expectOne(newResourceLocation).flush({title: 'zuzu', age: 30});
        tick();

        expect(res).toEqual({title: 'zuzu', age: 25});
    }));


    it('edit should send put request to resource/:id with necessary params', () => {

        testResource.edit(20, {title: 'gio', age: 30});

        const req = httpMock.expectOne(`${resourceUrl}/20`);

        expect(req.request.method).toEqual('PUT');
        expect(req.request.body.title).toEqual('gio');
        expect(req.request.body.age).toBeFalsy();
    });

    it('edit should resolve with updated object when request succeeds', fakeAsync(() => {
        let res: TestType;
        testResource.edit(20, {title: 'gio', age: 30}).then(resp => res = resp);
        httpMock.expectOne(`${resourceUrl}/20`).flush({});
        tick();
        expect(res).toEqual({title: 'gio', age: 30});
    }));

    it('edit should reject when request fails', fakeAsync(() => {
        let rejected = false;
        testResource.edit(20, {title: 'gio', age: 30}).catch(() => rejected = true);
        httpMock.expectOne(`${resourceUrl}/20`).error(null);
        tick();
        expect(rejected).toEqual(true);
    }));
});
