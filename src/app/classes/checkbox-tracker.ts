export class CheckboxTracker<T> {

    private items = {};
    private first = {};
    private checkedAllInner = false;

    item(item: T): any {
        const key = this.key(item);
        const items = this.items;

        if (!this.first[key]) {
            items[key] = false;
        }
        this.first[key] = true;

        return new (class Item {
            set checked(checked: boolean) {
                items[key] = checked;
            }
            get checked(): boolean {
                return items[key];
            }
        })();
    }

    set checkAll(all: boolean) {
        this.checkedAllInner = all;
        Object.keys(this.items)
            .forEach((key) => {
                this.items[key] = all;
            });
    }

    get checkAll(): boolean {
        if (Object.keys(this.items).length === 0) {
            return false;
        }
        return Object.keys(this.items)
            .map((key) => this.items[key])
            .reduce((acc, cur) => acc && cur, true);
    }

    get checked(): T[] {
        return Object.keys(this.items)
            .filter((key) => this.items[key])
            .map((key) => JSON.parse(key));
    }

    private key(obj: any): any {
        return JSON.stringify(obj);
    }

}
