import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

export class AsyncValue<T> {

    private subject: BehaviorSubject<T>;
    private subjectValue: T;

    public get value(): T {
        return this.subjectValue;
    }

    public set value(val: T) {
        this.subjectValue = val;
        this.subject.next(val);
    }

    public get changes(): Observable<T> {
        return this.subject;
    }

    constructor(initial: T = null) {
        this.subject = new BehaviorSubject<T>(initial);
        this.subjectValue = initial;
    }

}
