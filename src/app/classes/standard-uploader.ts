import { FileUploader } from '../interfaces/file-uploader';
import { Observable } from 'rxjs/Observable';
import Axios, { AxiosResponse } from 'axios';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { UploadType } from '../enums/upload-type.enum';
import { AuthService } from '../services/auth.service';
import { FileUploaderProgress } from '../interfaces/file-uploader-progress';
import { AppSuccess } from '../enums/app-success.enum';
import { AllowedFileType } from '../enums/allowed-file-type.enum';

export abstract class StandardUploader implements FileUploader {
    uploadType: UploadType;
    oldUrl: string;

    constructor(private authService: AuthService) {
        this.uploadType = UploadType.Add;
        this.oldUrl = '';
    }

    upload(file: any): Observable<FileUploaderProgress> {
        const progress = new BehaviorSubject<FileUploaderProgress>({completed: 1, fileUrl: ''});
        const uploadFormData = new FormData();
        const config = {
            onUploadProgress (progressEvent) {
                const completed = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                if (completed >= 100) {
                    return;
                }
                progress.next({
                    completed,
                    fileUrl: ''
                });
            },
            headers: {
                Authorization: `Bearer ${this.authService.getToken()}`
            }
        };

        uploadFormData.append('file', file, file.name && file.name.split(' ').join('-'));
        if (this.uploadType === UploadType.Update) {
            uploadFormData.append('oldUrl', this.oldUrl);

            Axios.put(
                this.uploadUrl(),
                uploadFormData,
                config
            ).then((resp: AxiosResponse) => {
                progress.next({
                    completed: 100,
                    fileUrl: resp.data.url
                });
            }).catch(() => {
                progress.error(null);
            });
        } else if (this.uploadType === UploadType.Add) {
            Axios.post(
                this.uploadUrl(),
                uploadFormData,
                config
            ).then((resp: AxiosResponse) => {
                progress.next({
                    completed: 100,
                    fileUrl: resp.headers.location
                });
            }).catch(() => {
                progress.error(null);
            });
        }

        return progress;
    }

    abstract uploadUrl(): string;

    abstract asyncTaskName(): string;

    abstract uploadAppSuccess(): AppSuccess;

    abstract allowedFileType(): AllowedFileType;
}
