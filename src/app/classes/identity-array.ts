
export interface Identifiable {
    id: number;
}

export class IdentityArray<T extends Identifiable> {

    private collection: T[];
    private ids: any;

    constructor(collection: T[] = []) {
        this.collection = collection;
        this.ids = {};
        this.syncCollectionWithIds();
    }

    private syncCollectionWithIds() {
        this.ids = {};
        this.collection.forEach((item: T) => this.ids[+item.id] = true);
    }

    get value(): T[] {
        return [...this.collection];
    }

    is(item: T): boolean {
        return !!this.ids[+item.id];
    }

    push(item: T) {
        this.collection.push(item);
        this.syncCollectionWithIds();
    }

    remove(item: T) {
        this.collection = this.collection.filter((it: T) => +it.id !== +item.id);
        this.syncCollectionWithIds();
    }

    concat(source: IdentityArray<T>): IdentityArray<T> {
        const res = new IdentityArray<T>();

        this.value.forEach((item: T) => res.push(item));
        source.value.forEach((item: T) => res.push(item));

        return res;
    }

    filter(cb: (item: T) => boolean): IdentityArray<T> {
        return new IdentityArray<T>(this.value.filter(cb));
    }

}
