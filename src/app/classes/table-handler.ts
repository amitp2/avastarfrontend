import { ActivatedRoute } from '@angular/router';
import { OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { PagingTableComponent } from '../modules/paging-table/components/paging-table/paging-table.component';

export abstract class TableHandler implements OnInit, OnDestroy {

    private freshSubs: Subscription;

    constructor(private activatedRoute: ActivatedRoute) {
    }

    ngOnInit(): void {
        this.freshSubs = this.activatedRoute.queryParams
            .map((params: any) => params.fresh)
            .filter(fresh => fresh ? true : false)
            .subscribe(() => this.getTable().refresh());
    }

    ngOnDestroy() {
        if (this.freshSubs) {
            this.freshSubs.unsubscribe();
        }
    }

    abstract getTable(): PagingTableComponent;
}
