import { Resource } from '../interfaces/resource';
import { HttpClient, HttpErrorResponse, HttpParams, HttpResponse } from '@angular/common/http';
import { HttpResourceQueryParams } from '../interfaces/http-resource-query-params';
import { ResourceQueryResponse } from '../interfaces/resource-query-response';
import { AppError, AppErrorByCode } from '../enums/app-error.enum';

export abstract class HttpResource<T> implements Resource<T> {

    rentable:any;
    protected httpClient: HttpClient;

    private lastRequestMeta: any;

    constructor(httpClient: HttpClient) {
        this.httpClient = httpClient;
    }

    private parseError(error: HttpErrorResponse): AppError {
        const err = JSON.parse(error.error);
        return AppErrorByCode[+err.errorCode];
    }

    protected queryByUrl(params: HttpResourceQueryParams, url: string): Promise<ResourceQueryResponse<T>> {
        return new Promise<ResourceQueryResponse<T>>((resolve, reject) => {
            let httpParams = new HttpParams();

            httpParams = httpParams.append('page', params.page + '');
            httpParams = httpParams.append('size', params.size + '');
            if (params.search) {
                httpParams = httpParams.append('search', params.search + '');
            }

            if (params.sortFields) {
                httpParams = httpParams.append('sort', `${params.sortFields.join(',')},${params.sortAsc ? 'asc' : 'desc'}`);
            }

            if (params.otherParams) {
                let key;
                for (key in params.otherParams) {
                    if (params.otherParams[key] || params.otherParams[key] === 0 || params.otherParams[key] === false) {
                        httpParams = httpParams.append(key, params.otherParams[key] + '');
                    }
                }
            }

            this.httpClient
                .get(url, {
                    params: httpParams,
                })
                .subscribe((resp: any) => {
                    // resolve({
                    //     total: resp.totalElements,
                    //     items: resp.content.map(item => this.deserialize(item))
                    // });
                    resolve(this.queryResponseDeserializer(resp));
                }, () => {
                    reject();
                });
        });
    }

    queryResponseDeserializer(resp: any) {
        return {
            total: resp.totalElements,
            items: resp.content.map(item => this.deserialize(item))
            
        };
    }

    protected addByUrl(model: T, url: string): Promise<T> {
        return new Promise((resolve, reject) => {
            this.httpClient
                .post(url, this.serialize(model), {
                    observe: 'response',
                    responseType: 'text'
                })
                .subscribe((resp: HttpResponse<any>) => {
                    
                    if (resp.headers.get('meta')) {
                        this.lastRequestMeta = resp.headers.get('meta');
                    } else {
                        this.lastRequestMeta = null;
                    }

                    this.httpClient
                        .get(resp.headers.get('Location'))
                        .subscribe(addedItem => {
                            
                            resolve(this.deserialize(addedItem));
                        }, () => {
                            reject();
                        });

                    // }, (error) => reject(this.parseError(error)));
                }, (error) => {
                    reject(error)
            });
        });
    }


    protected editByUrl(model: T, url: string): Promise<T> {
        return new Promise<T>((resolve, reject) => {
            this.httpClient
                .put(url, this.serialize(model), {
                   observe: 'response'
                })
                .subscribe((resp: HttpResponse<T>) => {

                    if (resp.headers.get('meta')) {
                        this.lastRequestMeta = resp.headers.get('meta');
                    } else {
                        this.lastRequestMeta = null;
                    }

                    const updated = resp.body;

                    if (updated['id']) {
                        model['id'] = updated['id'];
                    }
                    resolve(model);
                }, (err) => {
                    reject(err);
                });
        });
    }


    protected getByUrl(url: string): Promise<T> {
        return new Promise<T>((resolve, reject) => {
            this.httpClient
                .get(url)
                .subscribe(data => {
                    resolve(this.deserialize(data));
                }, () => {
                    reject();
                });
        });
    }

    getAllQueryObject(): HttpResourceQueryParams {
        return {
            page: 0,
            size: 999999,
            sortFields: [],
            sortAsc: true
        };
    }

    protected getManyByUrl(ids: number[], url: string): Promise<T[]> {
        return this.queryByUrl(this.getAllQueryObject(), `${url}?ids=${ids.join(',')}`)
            .then((resp) => resp.items);
    }

    query(params: HttpResourceQueryParams): Promise<ResourceQueryResponse<T>> {
        return this.queryByUrl(params, this.resourceUrl());
    }

    getAll(): Promise<T[]> {
        return this.queryByUrl(this.getAllQueryObject(), this.resourceUrl()).then((resp: ResourceQueryResponse<T>) => {
            return resp.items;
        });
    }

    edit(id: number, model: T): Promise<T> {
        return this.editByUrl(model, `${this.resourceUrl()}/${id}`);
    }

    add(model: T): Promise<T> {
        return this.addByUrl(model, this.resourceUrl());
    }

    delete(id: number): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.httpClient
                .delete(`${this.resourceUrl()}/${id}`)
                .subscribe(() => {
                    resolve();
                }, (err) => {
                    reject(err);
                });
        });
    }

    get(id: number): Promise<T> {
        return this.getByUrl(`${this.resourceUrl()}/${id}`);
    }

    getMany(ids: number[]): Promise<T[]> {
        return this.queryByUrl(this.getAllQueryObject(), `${this.resourceUrl()}?ids=${ids.join(',')}`)
            .then((resp) => resp.items);
    }

    getLastRequestMeta(): any {
        return this.lastRequestMeta;
    }

    abstract deserialize(data: any): T;

    abstract serialize(data: T): any;

    abstract resourceUrl(): string;
}
