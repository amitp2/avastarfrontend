import { HttpResource } from './http-resource';
import { HttpClient } from '@angular/common/http';
import { AppCache } from '../interfaces/cache';
import { HttpResourceQueryParams } from '../interfaces/http-resource-query-params';
import { ResourceQueryResponse } from '../interfaces/resource-query-response';

export abstract class HttpCachableResource<T> extends HttpResource<T> {

    private cachePrefix: string;
    private cacheKeys: string[];

    constructor(httpClient: HttpClient, private cache: AppCache) {
        super(httpClient);
        this.cachePrefix = `resource-${Math.random()}`;
        this.cacheKeys = [];
    }

    private clearAll() {
        this.cacheKeys.forEach((key) => this.cache.remove(key));
        this.cacheKeys = [];
    }

    private getKey(...keyObjects: any[]): string {
        const key = keyObjects.reduce( (acc, obj) => acc + JSON.stringify(obj), this.cachePrefix);
        this.cacheKeys.push(key);
        return key;
    }

    protected queryByUrl(params: HttpResourceQueryParams, url: string): Promise<ResourceQueryResponse<T>> {
        
        const key = this.getKey(params, url);
        return this.cache.hasKey(key)
            .then((is: boolean) => {
                if (is) {
                    return this.cache.get(key);
                }
                return super.queryByUrl(params, url).then((resp) => {
                    this.cache.set(key, resp);
                    return resp;
                });
            });
    }

    protected getByUrl(url: string) {
        const key = this.getKey(url);

        return this.cache.hasKey(key)
            .then((is: boolean) => {
                if (is) {
                    return this.cache.get(key);
                }
                return super.getByUrl(url).then((resp) => {
                    return resp;
                });
            });
    }

    protected addByUrl(model: T, url: string): Promise<T> {
        this.clearAll();
        return super.addByUrl(model, url);
    }

    protected editByUrl(model: T, url: string): Promise<T> {
        this.clearAll();
        return super.editByUrl(model, url);
    }

    delete(id: number): Promise<void> {
        this.clearAll();
        return super.delete(id);
    }

    public clearCache() {
        this.clearAll();
    }

}
