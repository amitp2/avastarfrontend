import { Subscription } from 'rxjs/Subscription';
import { OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { VendorResourceService } from '../services/resources/vendor-resource.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AsyncMethod } from '../decorators/async-method.decorator';
import { AppSuccess } from '../enums/app-success.enum';
import { AppError } from '../enums/app-error.enum';
import { TicketModel, TicketStatus, TicketAssignee } from '../models/ticket-model';
import { VendorContactResourceService } from '../services/resources/vendor-contact-resource.service';
import { PromisedStoreService } from '../services/promised-store.service';
import { FormHandler } from './form-handler';
import { UtilsService } from '../services/utils.service';
import { TicketResourceService } from '../services/resources/ticket-resource.service';
import { FormMode } from '../enums/form-mode.enum';
import { AuthService } from '../services/auth.service';
import { ValidationErrors } from '@angular/forms/src/directives/validators';
import { AbstractControl } from '@angular/forms/src/model';
import { VendorContactModel } from '../models/vendor-contact-model';

export enum TicketFor {
    System = 'System',
    Inventory = 'Inventory'
}

export abstract class AbstractTicketForm extends FormHandler<TicketModel> implements OnInit, OnDestroy {

    ticketFor = TicketFor;
    ticketStatus = TicketStatus;
    comments: any;
    freshSubs: Subscription;
    formModeEnum = FormMode;
    TicketAssignee: typeof TicketAssignee = TicketAssignee;

    constructor(
        private activatedRoute: ActivatedRoute,
        private vendors: VendorResourceService,
        private vendorContact: VendorContactResourceService,
        protected utilsService: UtilsService,
        private authService: AuthService,
        private router: Router,
        protected tickets: TicketResourceService,
        private utils: UtilsService,
        private promisedStore: PromisedStoreService
    ) {
        super();
    }

    abstract getId(): number;

    abstract getIdParam(): string;

    abstract afterAdd(added: TicketModel): any;

    ngOnInit() {
        super.ngOnInit();
        this.freshSubs = this.activatedRoute.queryParams
            .map((params: any) => params.fresh)
            .filter(fresh => fresh ? true : false)
            .subscribe(() => this.get(this.getId()));

        this.form.get('venueContactId').disable();

        this.form.get('assigneeType')
            .valueChanges
            .subscribe(this.assigneeTypeChanged);
    }

    ngOnDestroy() {
        super.ngOnDestroy();
        this.freshSubs.unsubscribe();
    }

    entityId(): Observable<number> {
        return this.paramsId(this.activatedRoute, this.getIdParam());
    }

    formMode(): Observable<FormMode> {
        return this.paramsIdFormMode(this.activatedRoute, this.getIdParam());
    }

    initializeForm(): FormGroup {
        const form = new FormGroup({
            title: new FormControl(null, [Validators.required]),
            description: new FormControl(null, [Validators.required]),
            systemId: new FormControl(),
            inventoryId: new FormControl(),
            reminderTime: new FormControl(),
            vendorContactId: new FormControl(null, [Validators.required]),
            ticketStatus: new FormControl(null, [Validators.required]),

            // filters below
            assigneeType: new FormControl(TicketAssignee.Vendor, [Validators.required]),
            venueContactId: new FormControl(null, [Validators.required]),
            vendorId: new FormControl(),
            ticketFor: new FormControl(TicketFor.Inventory),
            inventoryCategoryId: new FormControl(),
            inventorySubCategoryId: new FormControl(),
            systemSearch: new FormControl()
        }, (c: AbstractControl): ValidationErrors | null => {

            // if (c.get('ticketFor').value === TicketFor.System) {
            //     if (!c.get('systemId').value) {
            //         return {
            //             systemId: 'system id is required'
            //         };
            //     }
            //     return null;
            // }
            //
            // if (!c.get('inventoryId').value) {
            //     return {
            //         inventoryId: 'inventoryId is required'
            //     };
            // }

            return null;
        });

        return form;
    }

    protected assigneeTypeChanged = (assignee: TicketAssignee) => {
        if (assignee === TicketAssignee.Venue) {
            this.form.get('vendorId').disable();
            this.form.get('vendorContactId').disable();
            this.form.get('venueContactId').enable();
        } else {
            this.form.get('venueContactId').disable();
            this.form.get('vendorId').enable();
            this.form.get('vendorContactId').enable();
        }
    }

    @AsyncMethod({
        taskName: 'get_ticket'
    })
    get(id: number): Promise<TicketModel> {
        return this.utils.resolveAfter(2000)
            .then(() => this.tickets.get(id))
            .then(async (model: TicketModel) => {

                if (model.extras.inventory) {
                    this.form.patchValue({
                        inventoryCategoryId: +model.extras.inventory.categoryId,
                        inventorySubCategoryId: +model.extras.inventory.subCategoryId
                    });
                    await this.utilsService.resolveAfter(350);
                    this.form.patchValue({
                        inventoryId: model.inventoryId
                    });
                }

                this.comments = model.extras.comments;

                return model;
            }).then((model: TicketModel) => {
                if (model.assigneeType === TicketAssignee.Venue) {
                    return model;
                }
                return this.vendorContact
                    .get(model.vendorContactId)
                    .then((contact: VendorContactModel) => {
                        model.vendorId = +contact.vendorId;
                        return model;
                    });
            });
    }

    @AsyncMethod({
        taskName: 'add_ticket',
        success: AppSuccess.TICKET_ADD,
        error: AppError.TICKET_ADD
    })
    add(model: TicketModel): Promise<any> {
        return this.promisedStore.currentVenueId()
            .then((venueId: number) => this.tickets.addByVenueId(model, venueId))
            .then((added: TicketModel) => this.afterAdd(added));
    }

    @AsyncMethod({
        taskName: 'edit_ticket',
        success: AppSuccess.TICKET_EDIT,
        error: AppError.TICKET_EDIT
    })
    edit(id: number, model: TicketModel): Promise<any> {
        return this.tickets.edit(id, model);
    }

    serialize(model: TicketModel): any {
        console.log(model);
        const formVal = this.utils.transformInto(model, {
            title: { to: 'title' },
            description: { to: 'description' },
            vendorContactId: { to: 'vendorContactId' },
            venueContactId: { to: 'venueContactId' },
            assigneeType: { to: 'assigneeType' },
            systemId: { to: 'systemId' },
            reminderTime: { to: 'reminderTime' },
            inventoryId: { to: 'inventoryId' },
            ticketStatus: { to: 'ticketStatus' },
            vendorId: { to: 'vendorId' }
        });

        if (model.systemId) {
            formVal.ticketFor = TicketFor.System;
        } else if (model.inventoryId) {
            formVal.ticketFor = TicketFor.Inventory;
        }

        return formVal;
    }

    deserialize(formValue: any): TicketModel {
        const model = new TicketModel(this.utils.transformInto(formValue, {
            title: { to: 'title' },
            description: { to: 'description' },
            vendorContactId: { to: 'vendorContactId' },
            reminderTime: { to: 'reminderTime' },
            ticketStatus: { to: 'ticketStatus' },
            assigneeType: { to: 'assigneeType' },
            venueContactId: { to: 'venueContactId' }
        }));

        if (formValue.ticketFor === TicketFor.System) {
            model.systemId = formValue.systemId;
            model.inventoryId = null;
        } else if (formValue.inventoryId) {
            model.systemId = null;
            model.inventoryId = formValue.inventoryId;
        }

        return model;
    }

}
