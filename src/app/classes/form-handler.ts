import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { FormMode } from '../enums/form-mode.enum';
import { Subscription } from 'rxjs/Subscription';
import { OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppCache } from '../interfaces/cache';
import { FunctionModel } from '../models/function-model';

export interface FormHandler<T> {
    onPatchValue?(): any;
}

export abstract class FormHandler<T> implements OnInit, OnDestroy {

    form: FormGroup;
    private formModeSubs: Subscription;
    private entityIdSubs: Subscription;
    private formChangeSubs: Subscription;
    protected currentFormMode: FormMode;
    currentEntityId: number;
    public isLoading: boolean;
    public submitting: boolean;
    public FormMode: typeof FormMode = FormMode;
    private currentEntity: T;

    constructor(protected formBuilder?: FormBuilder) {
        this.form = this.initializeForm();
        this.isLoading = true;
        this.submitting = false;
    }

    abstract entityId(): Observable<number>;

    abstract formMode(): Observable<FormMode>;

    abstract initializeForm(): FormGroup;

    abstract get(id: number): Promise<T>;

    abstract add(model: T): Promise<any>;

    //abstract add_save(model: T): Promise<any>;

    abstract edit(id: number, model: T): Promise<any>;

    //abstract edit_save(id: number, model: T): Promise<any>;

    abstract serialize(model: T): any;

    abstract deserialize(formValue: any): T;

    protected cache(): AppCache {
        return null;
    }

    protected formName(): string {
        return 'form_name_for_caching';
    }

    submit(): any {
        
        if (this.submitting) {
            return;
        }
        this.submitting = true;

        const handleResponse = () =>{  this.submitting = false};
        const model = this.deserialize(this.form.value);
        if (this.currentFormMode === FormMode.Edit) {
            return this.edit(this.currentEntityId, model)
                .then(handleResponse)
                .catch(handleResponse);
        }
    
  return this.add(model)
            .then(handleResponse)
            .catch(handleResponse);
    }

    ngOnInit() {
        this.entityIdSubs = this.entityId().subscribe((id: number) => this.currentEntityId = +id);

        this.formModeSubs = this.formMode().subscribe((mode: FormMode) => {
            this.currentFormMode = mode;
            if (mode === FormMode.Edit) {
                this.get(this.currentEntityId)
                    .then((model: T) => {
                        this.form.patchValue(this.serialize(model));
                        this.form.markAsPristine();
                        this.setupCaching();
                        this.isLoading = false;
                        this.currentEntity = model;
                        if (this.onPatchValue) {
                            this.onPatchValue();
                        }
                    })
                    .catch(() => {
                        this.setFromCache();
                        this.setupCaching();
                        this.isLoading = false;
                        // console.log('error while loading set from cache');
                    });
                return;
            }
            this.isLoading = false;
            this.setupCaching();
        });

        // set form state to pristine
        // dynamic dropdowns alter state, so we need to reset after one second
        window.setTimeout(() => this.form.markAsPristine(), 1000);
    }

    setupCaching() {
        if (this.cache()) {
            if (this.formChangeSubs) {
                this.formChangeSubs.unsubscribe();
            }
            this.formChangeSubs = this.form.valueChanges.subscribe((val: any) => {
                this.cache().set(this.formName(), val);
                // this.form.markAsPristine();
            });
        }
    }

    clearFormCache() {
        this.cache().remove(this.formName());
    }

    setFromCache() {
        if (this.cache()) {
            this.cache().get(this.formName()).then((val: any) => {
                if (val) {
                    this.form.patchValue({
                        ...val
                    });
                    this.form.markAsPristine();
                }
            });
        }
    }

    ngOnDestroy() {
        if (this.formModeSubs) {
            this.formModeSubs.unsubscribe();
        }
        if (this.entityIdSubs) {
            this.entityIdSubs.unsubscribe();
        }
        if (this.formChangeSubs) {
            this.formChangeSubs.unsubscribe();
        }
    }

    // helpers for child components
    protected paramsId(activatedRoute: ActivatedRoute, idParam: string = 'id'): Observable<number> {
        return activatedRoute.params.map((params: any) => +params[idParam]);
    }

    protected paramsIdFormMode(activatedRoute: ActivatedRoute, idParam: string = 'id'): Observable<FormMode> {
        return activatedRoute.params.map((params: any) => params[idParam] === 'add' ? FormMode.Add : FormMode.Edit);
    }
}
