export function FormControlValue(controlName: string, formProperty: string = 'form'): PropertyDecorator {
    return function (target: any, propertyKey: string) {
        Object.defineProperty(target, propertyKey, {
            enumerable: true,
            configurable: true,
            get() {
                return this[formProperty].get(controlName).value;
            },
            set(value: any) {
                this[formProperty].get(controlName).setValue(value);
            }
        });
    };
}
