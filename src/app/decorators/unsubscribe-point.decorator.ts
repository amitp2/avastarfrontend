import { OBSERVABLE_SUBSCRIPTIONS_KEY } from './observable-subscription.decorator';

export function UnsubscribePoint(target: any, propertyKey: string, descriptor: PropertyDescriptor): any {

    const originalFunction = descriptor.value;
    descriptor.value = function () {
        this[OBSERVABLE_SUBSCRIPTIONS_KEY].forEach((resourceKey) => {
            if (this[resourceKey]) {
                if (this[resourceKey].unsubscribe) {
                    this[resourceKey].unsubscribe();
                }
            }
        });
        originalFunction.bind(this)();
    };

}
