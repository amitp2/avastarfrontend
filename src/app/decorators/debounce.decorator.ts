
export function Debounce(time: number): MethodDecorator {
    let interval;
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        const original = descriptor.value;
        descriptor.value = function () {
            const args = [];
            for (let _i = 0; _i < arguments.length; _i++) {
                args[_i - 0] = arguments[_i];
            }
            clearInterval(interval);
            interval = setTimeout(() => {
                original.apply(this, args);
            }, time);
        };
    };
}
