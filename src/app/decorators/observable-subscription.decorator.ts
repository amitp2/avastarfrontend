export const OBSERVABLE_SUBSCRIPTIONS_KEY = '_OBSERVABLE_SUBSCRIPTIONS_KEY_';

export function ObservableSubscription(target: any, key: string) {
    if (!target[OBSERVABLE_SUBSCRIPTIONS_KEY]) {
        target[OBSERVABLE_SUBSCRIPTIONS_KEY] = [];
    }

    target[OBSERVABLE_SUBSCRIPTIONS_KEY].push(key);
}
