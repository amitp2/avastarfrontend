import { AppComponent } from '../app.component';
import { AsyncTasksService } from '../services/async-tasks.service';
import { AppSuccess } from '../enums/app-success.enum';
import { AppError } from '../enums/app-error.enum';
import { HttpErrorResponse } from '@angular/common/http';

export interface AsyncMethodDescription {
    taskName: string;
    success?: AppSuccess;
    error?: AppError;
}

export function AsyncMethod(asyncMethod: AsyncMethodDescription): MethodDecorator {

    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        const original = descriptor.value;

        descriptor.value = function () {
            const asyncTasksService = AppComponent.rootInjector.get(AsyncTasksService);

            const args = [];
            for (let _i = 0; _i < arguments.length; _i++) {
                args[_i - 0] = arguments[_i];
            }

            asyncTasksService.taskStart(asyncMethod.taskName);
            const res = original.apply(this, args);
            res
                .then(() => {
                    asyncTasksService.taskSuccess(asyncMethod.taskName, asyncMethod.success);
                })
                .catch(rejectionError => {
                    asyncTasksService.taskError(asyncMethod.taskName, getError(Number.isInteger(asyncMethod.error) ? asyncMethod.error : rejectionError));
                });
            return res;
        };

    };
}

function getError(error: any) {
    if (error instanceof HttpErrorResponse) {
        let message = error.error;
        if (typeof message === 'string') {
            try {
                const result = JSON.parse(error.error);
                if (result.statusCode === 500) {
                    return;
                }
                return result.message;
            } catch (e) {}
        } else {
            if (message.statusCode === 500) {
                return;
            }
            message = message.message;
        }
        return message;
    }
    return error;
}
