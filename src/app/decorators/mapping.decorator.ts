import { generateFieldDecorator } from '../mappers/generate-field-decorator';
import { SimpleMapper } from '../mappers/simple-mapper';
import { UtilsService } from '../services/utils.service';

export const MAPPINGS_FIELDS_KEY = 'MAPPINGS_FIELDS_KEY';
export const MAPPINGS_FIELDS_MAPPERS_KEY = 'MAPPINGS_FIELDS_MAPPERS_KEY';

function fieldsKey(namespace: string) {
    return `${MAPPINGS_FIELDS_KEY}_${namespace}`;
}

function mappingsKey(namespace: string) {
    return `${MAPPINGS_FIELDS_MAPPERS_KEY}_${namespace}`;
}

function mappingsBuildFromSingle(source: any, instanceClass: any, namespace: string): any {
    return UtilsService.buildObjectFromMappingDecoratorsInverse(
        source, instanceClass, fieldsKey(namespace), mappingsKey(namespace)
    );
}

function mappingsBuildToSingle(instance: any, namespace: string) {
    return UtilsService.buildObjectFromMappingDecorators(instance, fieldsKey(namespace), mappingsKey(namespace));
}

function mappingsBuildToSingleClass(instance: any, namespace: string, toClass: any) {
    const mapped = mappingsBuildToSingle(instance, namespace);
    if (toClass) {
        const newInstance = new toClass();
        const fieldsKeys = Object.keys(mapped);
        fieldsKeys.forEach((key) => {
            newInstance[key] = mapped[key];
        });
        return newInstance;
    }
    return mapped;
}

export function Mapping(namespace: string, foreignField: string, mapper: any = SimpleMapper) {
    return generateFieldDecorator(
        fieldsKey(namespace),
        mappingsKey(namespace),
        foreignField,
        mapper
    );
}


export function mappingsBuildFrom(source: any | any[], instanceClass: any, namespace: string): any {
    if (source instanceof Array) {
        return source.map(el => mappingsBuildFromSingle(el, instanceClass, namespace));
    }
    return mappingsBuildFromSingle(source, instanceClass, namespace);
}

export function mappingsBuildTo(instance: any | any[], namespace: string, toClass: any = null) {
    if (instance instanceof Array) {
        return instance.map(el => mappingsBuildToSingleClass(el, namespace, toClass));
    }
    return mappingsBuildToSingleClass(instance, namespace, toClass);
}
