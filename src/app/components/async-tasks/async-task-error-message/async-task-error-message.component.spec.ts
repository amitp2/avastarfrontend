import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { AsyncTaskErrorMessageComponent } from './async-task-error-message.component';
import { Component, DebugElement } from '@angular/core';
import { AsyncTasksService } from '../../../services/async-tasks.service';
import { By } from '@angular/platform-browser';
import { AppError, AppErrorDefaultMessages } from '../../../enums/app-error.enum';


@Component({
    template: `
        <div>
            <app-async-task-error-message task="GOOD_TASK"></app-async-task-error-message>
        </div>
    `
})
class TestComponent {
    constructor() {
    }
}


@Component({
    template: `
        <div>
            <app-async-task-error-message task="GOOD_TASK" [messages]="errorMessages"></app-async-task-error-message>
        </div>
    `
})
class Test2Component {
    errorMessages: any;

    constructor() {
        this.errorMessages = {};
        this.errorMessages[AppError.USER_LOGIN_FAILED] = 'custom message for user login';
    }
}

describe('AsyncTaskErrorMessageComponent', () => {

    let asyncTasksService: AsyncTasksService;
    const task = 'GOOD_TASK';

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AsyncTaskErrorMessageComponent,
                TestComponent,
                Test2Component
            ],
            providers: [
                AsyncTasksService
            ]
        }).compileComponents();
        asyncTasksService = TestBed.get(AsyncTasksService);
    }));

    it('should not show anything by default', fakeAsync(() => {
        const fixture: ComponentFixture<TestComponent> = TestBed.createComponent(TestComponent);
        const div: DebugElement = fixture.debugElement.query(By.css('.alert'));
        fixture.detectChanges();
        tick();
        expect(div && div.nativeElement).toBeFalsy();
    }));


    it('should display default error message if desired message is not provided and error happened', fakeAsync(() => {
        const fixture: ComponentFixture<TestComponent> = TestBed.createComponent(TestComponent);
        fixture.detectChanges();

        asyncTasksService.taskError(task, AppError.USER_LOGIN_FAILED);

        fixture.detectChanges();

        const div: DebugElement = fixture.debugElement.query(By.css('.alert'));
        expect(div && div.nativeElement).toBeTruthy();
        expect(div.nativeElement.textContent).toEqual(AppErrorDefaultMessages[AppError.USER_LOGIN_FAILED]);
    }));

    it('should display UNKNOWN error message if desired message is not provided and error is unknown', fakeAsync(() => {
        const fixture: ComponentFixture<TestComponent> = TestBed.createComponent(TestComponent);
        fixture.detectChanges();

        asyncTasksService.taskError(task, undefined);

        fixture.detectChanges();

        const div: DebugElement = fixture.debugElement.query(By.css('.alert'));
        expect(div && div.nativeElement).toBeTruthy();
        expect(div.nativeElement.textContent).toEqual(AppErrorDefaultMessages[AppError.UNKNOWN_ERROR]);
    }));

    it('should desplay desired message if provided', () => {
        const fixture: ComponentFixture<TestComponent> = TestBed.createComponent(Test2Component);
        fixture.detectChanges();

        asyncTasksService.taskError(task, AppError.USER_LOGIN_FAILED);

        fixture.detectChanges();
        const div: DebugElement = fixture.debugElement.query(By.css('.alert'));
        expect(div && div.nativeElement).toBeTruthy();
        expect(div.nativeElement.textContent).toEqual('custom message for user login');
    });

    it('for any other status than error it should hide the message', () => {
        const fixture: ComponentFixture<TestComponent> = TestBed.createComponent(TestComponent);
        fixture.detectChanges();

        asyncTasksService.taskError(task, AppError.USER_LOGIN_FAILED);
        fixture.detectChanges();

        let div: DebugElement = fixture.debugElement.query(By.css('.alert'));
        expect(div && div.nativeElement).toBeTruthy();
        expect(div.nativeElement.textContent).toEqual(AppErrorDefaultMessages[AppError.USER_LOGIN_FAILED]);

        asyncTasksService.taskStart(task);
        fixture.detectChanges();
        div = fixture.debugElement.query(By.css('.alert'));
        expect(div && div.nativeElement).toBeFalsy();
    });

});
