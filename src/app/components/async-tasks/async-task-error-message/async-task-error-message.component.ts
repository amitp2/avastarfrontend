import { Component, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AsyncTasksService, AsyncTaskState, AsyncTaskStatus } from '../../../services/async-tasks.service';
import { AppError, AppErrorDefaultMessages } from '../../../enums/app-error.enum';

@Component({
    selector: 'app-async-task-error-message',
    templateUrl: './async-task-error-message.component.html',
    styleUrls: ['./async-task-error-message.component.styl']
})
export class AsyncTaskErrorMessageComponent implements OnDestroy {

    @Input() messages: any;

    @Input() set task(value: string) {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        this.subscription = this.asyncTasksService.track(value).subscribe(this.handleTracking.bind(this));
    }

    show: boolean;
    message: string;
    subscription: Subscription;

    constructor(private asyncTasksService: AsyncTasksService) {
        this.show = false;
    }

    private handleTracking(status: AsyncTaskStatus): void {
        if (status.state === AsyncTaskState.ERROR) {
            this.show = true;

            if (this.messages && this.messages[status.payload]) {
                this.message = this.messages[status.payload];
                return;
            }

            if (AppErrorDefaultMessages[status.payload]) {
                this.message = AppErrorDefaultMessages[status.payload];
                return;
            }
            this.message = AppErrorDefaultMessages[AppError.UNKNOWN_ERROR];
        } else {
            this.message = null;
            this.show = false;
        }
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

}
