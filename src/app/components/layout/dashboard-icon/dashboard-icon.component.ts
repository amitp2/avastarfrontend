import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-dashboard-icon',
    templateUrl: './dashboard-icon.component.html',
    styleUrls: ['./dashboard-icon.component.styl']
})
export class DashboardIconComponent implements OnInit {

    @Input() title: string;
    @Input() icon: string;

    constructor() {
    }

    ngOnInit() {
    }

    iconClasses(): any {
        const classes = {};
        classes[this.icon] = true;
        return classes;
    }

}
