import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardIconComponent } from './dashboard-icon.component';
import { By } from '@angular/platform-browser';

describe('DashboardIconComponent', () => {
    let component: DashboardIconComponent;
    let fixture: ComponentFixture<DashboardIconComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DashboardIconComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DashboardIconComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should set title to span', () => {
        const title = 'My Title';
        component.title = title;
        fixture.detectChanges();
        expect(fixture.debugElement.query(By.css('span')).nativeElement.textContent.trim()).toEqual(title);
    });

    it('should set icon to i.fa element', () => {
        component.icon = 'fa-stop';
        fixture.detectChanges();

        let icon = fixture.debugElement.query(By.css('i.fa.fa-stop'));
        expect(icon && icon.nativeElement).toBeTruthy();

        component.icon = 'fa-book';
        fixture.detectChanges();

        icon = fixture.debugElement.query(By.css('i.fa.fa-book'));
        expect(icon && icon.nativeElement).toBeTruthy();
    });
});
