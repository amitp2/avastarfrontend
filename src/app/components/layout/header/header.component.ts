import { Component } from '@angular/core';
import { UserRole } from '../../../enums/user-role.enum';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.styl']
})
export class HeaderComponent {

    UserRole = UserRole;

    constructor() {
    }

}
