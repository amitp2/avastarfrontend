import { Component, Input } from '@angular/core';
import { VisualType } from '../../../enums/visual-type.enum';

@Component({
    selector: 'app-panel',
    templateUrl: './panel.component.html',
    styleUrls: ['./panel.component.styl']
})
export class PanelComponent {

    @Input() title: string;
    @Input() type: VisualType;

    constructor() {
    }

    panelClass() {
        switch (this.type) {
            case VisualType.WARNING:
                return {'panel-warning': true};
            case VisualType.DANGER:
                return {'panel-danger': true};
            case VisualType.PRIMARY:
                return {'panel-primary': true};
            case VisualType.INFO:
                return {'panel-info': true};
            case VisualType.DEFAULT:
            default:
                return {'panel-default': true};
        }
    }

}
