import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelComponent } from './panel.component';
import { By } from '@angular/platform-browser';
import { VisualType } from '../../../enums/visual-type.enum';

describe('PanelComponent', () => {
    let component: PanelComponent;
    let fixture: ComponentFixture<PanelComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PanelComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PanelComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('panel heading should contain title property passed from outside', () => {
        const title = 'My Title';
        component.title = title;
        fixture.detectChanges();

        expect(fixture.debugElement.query(By.css('.panel-heading')).nativeElement.textContent.trim()).toEqual(title);
    });

    it('should put panel-default when no type property is present', () => {
        const panel = fixture.debugElement.query(By.css('.panel-default'));
        expect(panel && panel.nativeElement).toBeTruthy();
    });

    it('should put panel-default when type property is DEFAULT', () => {
        component.type = VisualType.DEFAULT;
        fixture.detectChanges();

        const panel = fixture.debugElement.query(By.css('.panel-default'));
        expect(panel && panel.nativeElement).toBeTruthy();
    });

    it('should put panel-warning when type property is WARNING', () => {
        component.type = VisualType.WARNING;
        fixture.detectChanges();

        const panel = fixture.debugElement.query(By.css('.panel-warning'));
        expect(panel && panel.nativeElement).toBeTruthy();
    });

    it('should put panel-danger when type property is DANGER', () => {
        component.type = VisualType.DANGER;
        fixture.detectChanges();

        const panel = fixture.debugElement.query(By.css('.panel-danger'));
        expect(panel && panel.nativeElement).toBeTruthy();
    });

    it('should put panel-primary when type property is PRIMARY', () => {
        component.type = VisualType.PRIMARY;
        fixture.detectChanges();

        const panel = fixture.debugElement.query(By.css('.panel-primary'));
        expect(panel && panel.nativeElement).toBeTruthy();
    });

    it('should put panel-info when type property is INFO', () => {
        component.type = VisualType.INFO;
        fixture.detectChanges();

        const panel = fixture.debugElement.query(By.css('.panel-info'));
        expect(panel && panel.nativeElement).toBeTruthy();
    });

});
