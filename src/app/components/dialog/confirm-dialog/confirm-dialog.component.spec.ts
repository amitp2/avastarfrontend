import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmDialogComponent } from './confirm-dialog.component';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import Spy = jasmine.Spy;

describe('ConfirmDialogComponent', () => {
    let component: ConfirmDialogComponent;
    let fixture: ComponentFixture<ConfirmDialogComponent>;

    function getByCss(css): DebugElement {
        return fixture.debugElement.query(By.css(css));
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ConfirmDialogComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConfirmDialogComponent);
        component = fixture.componentInstance;
        component.cancel = () => {
        };
        component.ok = () => {

        };
        fixture.detectChanges();
    });

    it('should set title to modal-title', () => {
        component.title = 'some title';
        fixture.detectChanges();
        expect(getByCss('.modal-title').nativeElement.textContent.trim()).toEqual('some title');
    });

    it('should set message to modal-body', () => {
        component.message = 'some message';
        fixture.detectChanges();
        expect(getByCss('.modal-body').nativeElement.textContent.trim()).toEqual('some message');
    });

    it('clicking on cancel should call cancel callback', () => {
        const spy: Spy = spyOn(component, 'cancel').and.returnValue({});
        getByCss('.cancel-button').triggerEventHandler('click', null);
        fixture.detectChanges();
        expect(spy.calls.all().length).toEqual(1);
    });

    it('clicking on X should call cancel modal', () => {
        const spy: Spy = spyOn(component, 'cancel').and.returnValue({});
        getByCss('.cancel-x').triggerEventHandler('click', null);
        fixture.detectChanges();
        expect(spy.calls.all().length).toEqual(1);
    });

    it('clicking on ok should call ok callback', () => {
        const spy: Spy = spyOn(component, 'ok').and.returnValue({});
        getByCss('.ok-button').triggerEventHandler('click', null);
        fixture.detectChanges();
        expect(spy.calls.all().length).toEqual(1);
    });
});
