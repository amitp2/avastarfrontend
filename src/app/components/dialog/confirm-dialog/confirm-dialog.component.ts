import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-confirm-dialog',
    templateUrl: './confirm-dialog.component.html',
    styleUrls: ['./confirm-dialog.component.styl']
})
export class ConfirmDialogComponent {

    title: string;
    message: string;
    cancel: () => void;
    ok: () => void;

    constructor() {
    }

    confirmModal() {
        if (this.ok) {
            this.ok();
        }
    }

    cancelModal() {
        if (this.cancel) {
            this.cancel();
        }
    }

}
