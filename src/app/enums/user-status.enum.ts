export enum UserStatus {
    Inactive = 'Inactive',
    Active = 'Active',
    Blocked = 'Blocked',
    NotVerified = 'Not verified'
}
