export enum SubscriptionTypeNames {
    Essentials = 'Essentials',
    Professional = 'Professional',
    //Unlimited = 'Unlimited'
    Enterprise = 'Enterprise'
}

export enum SubscriptionStatusNames {
    GoodStanding = 'Good Standing',
    FirstReminderSent = 'First Reminder Sent',
    SecondReminderSent = 'Second Reminder Sent',
    Canceled = 'Canceled'
}

export enum PaymentMethodNames {
    Check = 'Check',
    Automated = 'Automated'
}

export enum RenewalPeriodNames {
    Monthly = 'Monthly',
    Yearly = 'Yearly'
}

export enum SubscriptionType {
    Essentials = 'ESSENTIAL',
    Professional = 'PROFESSIONAL',
    //Unlimited = 'UNLIMITED'
    Enterprise = 'ENTERPRISE'
}

export enum SubscriptionStatus {
    GoodStanding = 'GOOD_STANDING',
    FirstReminderSent = 'FIRST_REMINDER',
    SecondReminderSent = 'SECOND_REMINDER',
    Canceled = 'CANCELED'
}

export enum PaymentMethod {
    Check = 'MANUAL',
    Automated = 'AUTO',
}

export enum RenewalPeriod {
    Monthly = 'MONTHLY',
    Yearly = 'YEARLY'
}

const statusMapping = {
    [SubscriptionStatus.GoodStanding]: SubscriptionStatusNames.GoodStanding,
    [SubscriptionStatus.FirstReminderSent]: SubscriptionStatusNames.FirstReminderSent,
    [SubscriptionStatus.SecondReminderSent]: SubscriptionStatusNames.SecondReminderSent,
    [SubscriptionStatus.Canceled]: SubscriptionStatusNames.Canceled,
}

export function subscriptionStatusToName(status: SubscriptionStatus) {
    return statusMapping[status];
}

