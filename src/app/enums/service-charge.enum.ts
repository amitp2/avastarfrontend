export enum ServiceCharge {
    PreDiscount,
    PostDiscount
}

export enum ServiceChargeStatus {
    Active = 'ACTIVE',
    Inactive = 'LOCKED',
    Expired = 'EXPIRED'
}