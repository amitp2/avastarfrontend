export enum FunctionRevenueCategory {
    Equipment = 'Equipment',
    ItEquipment = 'IT Equipment',
    InternetNetworkAccess = 'Internet/Network Access',
    PowerEquipment = 'Power Equipment',
    RiggingEquipment = 'Rigging Equipment',
    Labor = 'Labor',
    ITLabor = 'IT Labor',
    ElectricianLabor = 'Electrician Labor',
    RiggingLabor = 'Rigging Labor',
    Operator = 'Operator',
    SalesItems = 'Sales Items',
    Discounts = 'Discounts',
    ComplimentaryItems = 'Complimentary Items',
    ServiceCharge = 'Service Charge',
    Tax = 'Tax',
    Total = 'Total'
}
