import { AppSuccess, AppSuccessDefaultMessages } from './app-success.enum';

export enum AppError {
    REVENUE_CATEGORY_ADD_GLOBAL,
    COST_CATEGORY_ADD_GLOBAL,
    USER_LOGIN_FAILED,
    FORGOT_PASSWORD_FAILED,
    RESET_PASSWORD_FAILED,
    CURRENT_USER_FAILED,
    TOO_MANY_LOGIN_FAILS,
    USERNAME_ALREADY_EXISTS,
    UNKNOWN_ERROR,
    PASSWORDS_DO_NOT_MATCH,
    PASSWORD_MING_LENGTH,
    PASSWORD_IS_REQUIRED,
    SUBSCRIBER_UPDATED_ERROR,
    SUBSCRIBER_ADD,
    SUBSCRIBER_INACTIVE,
    SUBSCRIBER_CONTACT_PERSON_LOAD,
    SUBSCRIBER_CONTACT_PERSON_UPDATE,
    SUBSCRIBER_USER_UPDATE,
    SUBSCRIBER_USER_ADD,
    VENUE_UPDATE,
    VENUE_ADD,
    LOGIN_EMAIL_AND_PASSWORD_REQUIRED,
    FORGOT_PASSWORD_EMAIL_REQUIRED,
    USER_VERIFICATION,
    INVALID_FILE_FORMAT,
    SUBSCRIBER_LOAD,
    VENUE_CONTACT_LOAD,
    ADD_VENUE_CONTACT,
    EDIT_VENUE_CONTACT,
    EVENT_SPACE_EDIT,
    EVENT_SPACE_ADD,
    FUNCTION_SPACE_EDIT,
    FUNCTION_SPACE_ADD,
    EVENT_SPACE_DELETE,
    EVENT_SPACE_DELETE_WITH_FUNCTION_SPACES,
    FUNCTION_SPACE_DELETE,
    STORAGE_SPACE_DELETE,
    STORAGE_SPACE_ADD,
    STORAGE_SPACE_EDIT,
    VENDOR_ADD,
    VENDOR_EDIT,
    TICKET_EDIT,
    VENDOR_CONTACT_ADD,
    INVENTORY_ADD,
    VENDOR_CONTACT_EDIT,
    COST_CATEGORY_EDIT,
    REVENUE_CATEGORY_EDIT,
    REVENUE_CATEGORY_ADD,
    REVENUE_CATEGORY_DELETE,
    COST_CATEGORY_DELETE,
    INVENTORY_EDIT,
    INVENTORY_ASSET_STATUS_EDIT,
    INVENTORY_ASSET_MAINTENANCE_EDIT,
    INVENTORY_DOCUMENT_ADD,
    INPUT_BUDGET_EDIT,
    SERVICE_CHARGE_ADD,
    SERVICE_CHARGE_DATES,
    SERVICE_CHARGE_EDIT,
    TAX_CHARGE_ADD,
    TAX_CHARGE_DATES,
    TAX_CHARGE_EDIT,
    EDIT_PRICING_EXTENSION,
    EQUIPMENT_ADD,
    EQUIPMENT_EDIT,
    SERVICE_ADD,
    ADD_PACKAGE,
    SERVICE_EDIT,
    ADD_EQUIPMENT_SYSTEM,
    EDIT_EQUIPMENT_SYSTEM,
    ENDING_DATE,
    DELETE_EQUIPMENT_SYSTEM,
    DELETE_INVENTORY,
    DELETE_TICKET,
    DELETE_EQUIPMENT,
    EDIT_PACKAGE,
    DELETE_PACKAGE,
    ACCOUNT_ADD,
    ACCOUNT_UPDATE,
    VENUE_CONTACT_UPDATE,
    ADD_EVENT,
    EDIT_EVENT,
    TICKET_ADD,
    ADD_PURCHASE_ORDER,
    EDIT_PURCHASE_ORDER,
    DELETE_PURCHASE_ORDER,
    INVENTORY_UPLOAD,
    ADD_EQUIPMENT,
    NOT_ENOUGH_ITEMS,
    PRICE_EXTENSION_ERRROR,
    ADD_CONTRACT,
    EDIT_CONTRACT,
    DELETE_CONTRACT,
    EVENT_CONTACT_ADD,
    EVENT_CONTACT_EDIT,
    EVENT_CONTACT_DELETE,
    ACCESS_UPDATED,
    USER_ACCESS_UPDATE,
    INVENTORY_CATEGORY_ADD,
    INVENTORY_CATEGORY_EDIT,
    INVENTORY_CATEGORY_DELETE,
    INVENTORY_SUB_CATEGORY_ADD,
    INVENTORY_SUB_CATEGORY_EDIT,
    INVENTORY_SUB_CATEGORY_DELETE,
    INVENTORY_SUB_CATEGORY_ITEM_ADD,
    INVENTORY_SUB_CATEGORY_ITEM_EDIT,
    INVENTORY_SUB_CATEGORY_ITEM_DELETE,
    UPLOAD_CONTRACT_DOCUMENT,
    UPLOAD_INITIAL_INVOICE,
    UPLOAD_SUBSCRIBER_CONTRACT,
    EDIT_PAYMENT_INFO,
    SUBSCRIBER_PAYMENT_ADD,
    UPDATE_DECISION_TREE
}

export const AppErrorByCode = {};

AppErrorByCode[1002] = AppError.USER_LOGIN_FAILED;
AppErrorByCode[2001] = AppError.USER_LOGIN_FAILED;
AppErrorByCode[2002] = AppError.TOO_MANY_LOGIN_FAILS;
AppErrorByCode[1003] = AppError.USERNAME_ALREADY_EXISTS;

export const AppErrorDefaultMessages = {};
AppErrorDefaultMessages[AppError.SUBSCRIBER_USER_ADD] = 'User with the provided username already exists';
AppErrorDefaultMessages[AppError.SUBSCRIBER_USER_UPDATE] = 'Error while updating subscriber';
AppErrorDefaultMessages[AppError.PASSWORD_IS_REQUIRED] = 'Please enter your new password';
AppErrorDefaultMessages[AppError.PASSWORD_MING_LENGTH] = 'Password you entered is invalid. Please try again and make sure your password is at least 8 characters long.';
AppErrorDefaultMessages[AppError.PASSWORDS_DO_NOT_MATCH] = 'Passwords do not match';
AppErrorDefaultMessages[AppError.USER_LOGIN_FAILED] = 'There was a problem with your email or password. Please try again';
AppErrorDefaultMessages[AppError.FORGOT_PASSWORD_FAILED] = 'There was a problem with your email address. Please try again';
AppErrorDefaultMessages[AppError.RESET_PASSWORD_FAILED] = 'Your password reset link is not valid, or already used';
AppErrorDefaultMessages[AppError.CURRENT_USER_FAILED] = 'Error while fetching current users info';
AppErrorDefaultMessages[AppError.TOO_MANY_LOGIN_FAILS] = `
    It looks like your password has been entered incorrectly
    too many times, so we are protecting your account by disabling access. 
    Reset your password before next login
    `;
AppErrorDefaultMessages[AppError.USERNAME_ALREADY_EXISTS] = 'User with the provided username already exists';
AppErrorDefaultMessages[AppError.UNKNOWN_ERROR] = 'We are having difficulty reaching the AVaStar! system right now. Please try again later.';
AppErrorDefaultMessages[AppError.SUBSCRIBER_UPDATED_ERROR] = 'Updating subscriber information has been failed';
AppErrorDefaultMessages[AppError.SUBSCRIBER_ADD] = 'Subscriber creation failed';
AppErrorDefaultMessages[AppError.SUBSCRIBER_INACTIVE] = 'There was a problem with your email or password. Please try again';
AppErrorDefaultMessages[AppError.SUBSCRIBER_CONTACT_PERSON_LOAD] = 'Error while loading contact person information';
AppErrorDefaultMessages[AppError.SUBSCRIBER_CONTACT_PERSON_UPDATE] = 'Error while updating contact person information';
AppErrorDefaultMessages[AppError.VENUE_CONTACT_UPDATE] = 'Error while updating contact person information';
AppErrorDefaultMessages[AppError.VENUE_UPDATE] = 'Error while updating property';
AppErrorDefaultMessages[AppError.VENUE_ADD] = 'Selected subscriber already has a property';
AppErrorDefaultMessages[AppError.LOGIN_EMAIL_AND_PASSWORD_REQUIRED] = 'Please enter your email and password';
AppErrorDefaultMessages[AppError.FORGOT_PASSWORD_EMAIL_REQUIRED] = 'Please enter your email';
AppErrorDefaultMessages[AppError.USER_VERIFICATION] = 'Your verification link is not valid, or already used';
AppErrorDefaultMessages[AppError.INVALID_FILE_FORMAT] = 'Invalid file format';
AppErrorDefaultMessages[AppError.ADD_VENUE_CONTACT] = 'Error while creating venue contact';
AppErrorDefaultMessages[AppError.EDIT_VENUE_CONTACT] = 'Error while updating venue contact';
AppErrorDefaultMessages[AppError.EVENT_SPACE_EDIT] = 'Error while updating event space';
AppErrorDefaultMessages[AppError.EVENT_SPACE_ADD] = 'Error while creating event space';
AppErrorDefaultMessages[AppError.FUNCTION_SPACE_ADD] = 'Error while creating function space';
AppErrorDefaultMessages[AppError.FUNCTION_SPACE_EDIT] = 'Error while updating function space';
AppErrorDefaultMessages[AppError.EVENT_SPACE_DELETE] = 'Event space containing inventory items can not be deleted';
AppErrorDefaultMessages[AppError.FUNCTION_SPACE_DELETE] = 'Function space with inventory can not be deleted';
AppErrorDefaultMessages[AppError.STORAGE_SPACE_DELETE] = 'Storage space containing inventory items can not be deleted';
AppErrorDefaultMessages[AppError.EVENT_SPACE_DELETE_WITH_FUNCTION_SPACES] = 'Event space containing function spaces can not be deleted';
AppErrorDefaultMessages[AppError.STORAGE_SPACE_ADD] = 'Error while creating storage space';
AppErrorDefaultMessages[AppError.VENDOR_ADD] = 'Error while creating vendor';
// AppErrorDefaultMessages[AppError.INVENTORY_ADD] = 'Error while creating inventory asset';
AppErrorDefaultMessages[AppError.INVENTORY_ADD] = 'Duplicate product in the system';
AppErrorDefaultMessages[AppError.VENDOR_CONTACT_ADD] = 'Error while creating vendor contact';
AppErrorDefaultMessages[AppError.INVENTORY_DOCUMENT_ADD] = 'Error while adding inventory document';
AppErrorDefaultMessages[AppError.STORAGE_SPACE_EDIT] = 'Error while updating storage space';
AppErrorDefaultMessages[AppError.VENDOR_EDIT] = 'Error while updating vendor';
AppErrorDefaultMessages[AppError.VENDOR_CONTACT_EDIT] = 'Error while updating vendor contact';
AppErrorDefaultMessages[AppError.COST_CATEGORY_EDIT] = 'Error while updating cost category';
AppErrorDefaultMessages[AppError.REVENUE_CATEGORY_EDIT] = 'Error while updating revenue category';
// AppErrorDefaultMessages[AppError.INVENTORY_EDIT] = 'Error while updating inventory asset';
AppErrorDefaultMessages[AppError.INVENTORY_EDIT] = 'Duplicate product in the system';
AppErrorDefaultMessages[AppError.INVENTORY_ASSET_STATUS_EDIT] = 'Error while updating inventory asset status';
AppErrorDefaultMessages[AppError.INVENTORY_ASSET_MAINTENANCE_EDIT] = 'We couldn’t add the new item to your inventory list, please try again';
AppErrorDefaultMessages[AppError.INPUT_BUDGET_EDIT] = 'Error while updating input budget';
AppErrorDefaultMessages[AppError.SERVICE_CHARGE_ADD] = 'Error while adding Service Charge';
AppErrorDefaultMessages[AppError.SERVICE_CHARGE_EDIT] = 'Error while updating Service Charge';
AppErrorDefaultMessages[AppError.TAX_CHARGE_ADD] = 'Error while adding Tax Charge';
AppErrorDefaultMessages[AppError.ADD_EQUIPMENT_SYSTEM] = 'Error while adding Equipment System';
AppErrorDefaultMessages[AppError.TAX_CHARGE_EDIT] = 'Error while updating Tax Charge';
AppErrorDefaultMessages[AppError.EDIT_PRICING_EXTENSION] = 'Error while updating Pricing Extension';
AppErrorDefaultMessages[AppError.EQUIPMENT_ADD] = 'Equipment with provided category already exists';
AppErrorDefaultMessages[AppError.EQUIPMENT_EDIT] = 'Equipment with provided inventory already exists';
AppErrorDefaultMessages[AppError.SERVICE_ADD] = 'Service with provided category already exists';
AppErrorDefaultMessages[AppError.ADD_PACKAGE] = 'Error while adding package';
AppErrorDefaultMessages[AppError.TICKET_ADD] = 'Error while adding service ticket';
AppErrorDefaultMessages[AppError.ADD_EVENT] = 'Error while adding event';
AppErrorDefaultMessages[AppError.SERVICE_EDIT] = 'Error while updating service';
AppErrorDefaultMessages[AppError.ENDING_DATE] = 'Ending date can not be earlier than starting date';
AppErrorDefaultMessages[AppError.EDIT_EQUIPMENT_SYSTEM] = 'Error while updating Equipment System';
AppErrorDefaultMessages[AppError.EDIT_PACKAGE] = 'Error while updating Package';
AppErrorDefaultMessages[AppError.EDIT_EVENT] = 'Error while updating Event';
AppErrorDefaultMessages[AppError.TICKET_EDIT] = 'Error while updating service ticket';
AppErrorDefaultMessages[AppError.DELETE_EQUIPMENT_SYSTEM] = 'System containing inventory can not be deleted';
AppErrorDefaultMessages[AppError.DELETE_INVENTORY] = 'Inventory can not be deleted while it is being used for system';
AppErrorDefaultMessages[AppError.DELETE_EQUIPMENT] = 'Equipment/Service used in system';
AppErrorDefaultMessages[AppError.DELETE_PACKAGE] = 'Package contains equipment or services';
AppErrorDefaultMessages[AppError.ADD_PURCHASE_ORDER] = 'Error while adding purchase order';
AppErrorDefaultMessages[AppError.EDIT_PURCHASE_ORDER] = 'Error while updating purchase order';
AppErrorDefaultMessages[AppError.DELETE_PURCHASE_ORDER] = 'Error while deleting purchase order';
AppErrorDefaultMessages[AppError.DELETE_TICKET] = 'Error while deleting ticket';
AppErrorDefaultMessages[AppError.INVENTORY_UPLOAD] = 'Failed to upload file, due to bad structure of the file';
AppErrorDefaultMessages[AppError.ADD_EQUIPMENT] = 'Equipment with provided category already exists';
AppErrorDefaultMessages[AppError.ADD_CONTRACT] = 'Error while adding contract';
AppErrorDefaultMessages[AppError.EDIT_CONTRACT] = 'Error while updating contract';
AppErrorDefaultMessages[AppError.DELETE_CONTRACT] = 'Error while deleting contract';
AppErrorDefaultMessages[AppError.NOT_ENOUGH_ITEMS] = 'Requested equipment or package quantity is not available';
AppErrorDefaultMessages[AppError.PRICE_EXTENSION_ERRROR] = 'Price expired, please extend price end date for selected items';
AppErrorDefaultMessages[AppError.EVENT_CONTACT_ADD] = 'Error while adding event contact';
AppErrorDefaultMessages[AppError.EVENT_CONTACT_EDIT] = 'Error while updating event contact';
AppErrorDefaultMessages[AppError.EVENT_CONTACT_DELETE] = 'Error while deleting event contact';
AppErrorDefaultMessages[AppError.ACCESS_UPDATED] = 'Number of licensed users can not be less than allowed users. Please Un-select users before updating number of licensed users information';
AppErrorDefaultMessages[AppError.USER_ACCESS_UPDATE] = 'Error while updating user access';

AppErrorDefaultMessages[AppError.INVENTORY_CATEGORY_ADD] = 'Category with same name already exists';
AppErrorDefaultMessages[AppError.INVENTORY_CATEGORY_EDIT] = 'Category with same name already exists';
AppErrorDefaultMessages[AppError.INVENTORY_CATEGORY_DELETE] = 'Category used in system';
AppErrorDefaultMessages[AppError.INVENTORY_SUB_CATEGORY_ADD] = 'Sub category with same name already exists';
AppErrorDefaultMessages[AppError.INVENTORY_SUB_CATEGORY_EDIT] = 'Sub category with same name already exists';
AppErrorDefaultMessages[AppError.INVENTORY_SUB_CATEGORY_DELETE] = 'Sub category used in system';
AppErrorDefaultMessages[AppError.INVENTORY_SUB_CATEGORY_ITEM_ADD] = 'Equipment/Service with same name already exists';
AppErrorDefaultMessages[AppError.INVENTORY_SUB_CATEGORY_ITEM_EDIT] = 'Equipment/Service with same name already exists';
AppErrorDefaultMessages[AppError.INVENTORY_SUB_CATEGORY_ITEM_DELETE] = 'Equipment/Service item used in system';
AppErrorDefaultMessages[AppError.SERVICE_CHARGE_DATES] = 'Service charge in specified time period already exists';
AppErrorDefaultMessages[AppError.TAX_CHARGE_DATES] = 'Tax charge in specified time period already exists';
AppErrorDefaultMessages[AppError.UPLOAD_CONTRACT_DOCUMENT] = 'Failed to upload file, due to bad structure of the file';
AppErrorDefaultMessages[AppError.REVENUE_CATEGORY_ADD] = 'Error while adding revenue category';
AppErrorDefaultMessages[AppError.REVENUE_CATEGORY_DELETE] = 'Revenue category used in system';
AppErrorDefaultMessages[AppError.COST_CATEGORY_DELETE] = 'Cost category used in system';
AppErrorDefaultMessages[AppError.REVENUE_CATEGORY_ADD_GLOBAL] = 'Revenue category already exists';
AppErrorDefaultMessages[AppError.COST_CATEGORY_ADD_GLOBAL] = 'Cost category already exists';
AppErrorDefaultMessages[AppError.UPLOAD_INITIAL_INVOICE] = 'Error while uploading initial invoice';
AppErrorDefaultMessages[AppError.UPLOAD_SUBSCRIBER_CONTRACT] = 'Error while uploading contract';
AppErrorDefaultMessages[AppError.EDIT_PAYMENT_INFO] = 'Error while updating payment info';
AppErrorDefaultMessages[AppError.SUBSCRIBER_PAYMENT_ADD] = 'Error while adding payment';
AppErrorDefaultMessages[AppError.UPDATE_DECISION_TREE] = 'Error while updating decision tree';
