import { AppError } from './app-error.enum';

export enum AppErrorNamespace {
    Login
}

export const AppErrorNamespaceCodes = {};

AppErrorNamespaceCodes[AppErrorNamespace.Login] = {};
AppErrorNamespaceCodes[AppErrorNamespace.Login][2002] = AppError.TOO_MANY_LOGIN_FAILS;
AppErrorNamespaceCodes[AppErrorNamespace.Login][1002] = AppError.USER_LOGIN_FAILED;
AppErrorNamespaceCodes[AppErrorNamespace.Login][1000] = AppError.USER_LOGIN_FAILED;
AppErrorNamespaceCodes[AppErrorNamespace.Login][2001] = AppError.USER_LOGIN_FAILED;
