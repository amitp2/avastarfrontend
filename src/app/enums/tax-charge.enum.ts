export enum TaxChargeStatus {
    Active = 'ACTIVE',
    Inactive = 'LOCKED',
    Expired = 'EXPIRED'
}