export enum StorageSpaceStatus {
    Active = 'Active',
    Inactive = 'Inactive'
}
