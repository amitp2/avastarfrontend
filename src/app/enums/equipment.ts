export enum EquipmentServiceType {
    Equipment = 'EQUIPMENT',
    Service = 'SERVICE'
}

export enum EquipmentProductInformation {
    Portable = 'PORTABLE',
    Installed = 'INSTALLED',
    ThirdParty = 'THIRD_PARTY',
    ExcludedFromReport = 'EXCLUDED_FROM_REPORT'
}

export enum EquipmentCrossRentalType {
    Rental = 'RENTAL',
    Purchase = 'PURCHASE'
}

export enum EquipmentProductInformationNames {
    Portable = 'Portable Inventory',
    Installed = 'Installed Inventory',
    ThirdParty = '3rd Party Inventory',
    ExcludedFromReport = 'Exclude From Equipment Usage Report'
}
