export enum AppSuccess {
    USER_LOGIN,
    FORGOT_PASSWORD,
    RESET_PASSWORD,
    SUBSCRIBER_LOGO_UPLOAD,
    ACCOUNT_LOGO_UPLOAD,
    INVENTORY_LOGO_UPLOAD,
    VENUE_LOGO_UPLOAD,
    SUBSCRIBER_INFO_UPDATE,
    SUBSCRIBER_ADD,
    SUBSCRIBER_CONTACT_PERSON_UPDATE,
    SUBSCRIBER_USER_UPDATE,
    SUBSCRIBER_USER_ADD,
    VENUE_UPDATE,
    VENUE_ADD,
    USER_VERIFICATION,
    SUBSCRIBER_LOAD,
    VENUE_CONTACT_LOAD,
    ADD_VENUE_CONTACT,
    EDIT_VENUE_CONTACT,
    EVENT_SPACE_EDIT,
    EVENT_SPACE_ADD,
    FUNCTION_SPACE_EDIT,
    FUNCTION_SPACE_ADD,
    EVENT_SPACE_DELETE,
    FUNCTION_SPACE_DELETE,
    STORAGE_SPACE_ADD,
    STORAGE_SPACE_EDIT,
    VENDOR_ADD,
    VENDOR_EDIT,
    TICKET_EDIT,
    VENDOR_CONTACT_ADD,
    INVENTORY_ADD,
    VENDOR_CONTACT_EDIT,
    INVENTORY_EDIT,
    INVENTORY_ASSET_STATUS_EDIT,
    INVENTORY_ASSET_MAINTENANCE_EDIT,
    INVENTORY_DOCUMENT_UPLOAD,
    INVENTORY_DOCUMENT_ADD,
    COST_CATEGORY_EDIT,
    REVENUE_CATEGORY_EDIT,
    REVENUE_CATEGORY_ADD,
    INPUT_BUDGET_EDIT,
    SERVICE_CHARGE_ADD,
    SERVICE_CHARGE_EDIT,
    TAX_CHARGE_ADD,
    TAX_CHARGE_EDIT,
    EDIT_PRICING_EXTENSION,
    EQUIPMENT_ADD,
    EQUIPMENT_EDIT,
    SERVICE_ADD,
    SERVICE_EDIT,
    ADD_EQUIPMENT_SYSTEM,
    ADD_PACKAGE,
    ADD_FUNCTION,
    EDIT_FUNCTION,
    EDIT_EQUIPMENT_SYSTEM,
    EDIT_PACKAGE,
    PACKAGE_LOGO_UPLOAD,
    ACCOUNT_ADD,
    ACCOUNT_UPDATE,
    TERM_UPDATE,
    BILLING_UPDATE,
    PROPOSAL_UPDATE,
    VENUE_CONTACT_UPDATE,
    ADD_EVENT,
    EDIT_EVENT,
    TICKET_ADD,
    ADD_PURCHASE_ORDER,
    EDIT_PURCHASE_ORDER,
    DELETE_PURCHASE_ORDER,
    ADD_CONTRACT,
    EDIT_CONTRACT,
    DELETE_CONTRACT,
    UPLOAD_CONTRACT_DOCUMENT,
    EVENT_CONTACT_ADD,
    EVENT_CONTACT_EDIT,
    EVENT_CONTACT_DELETE,
    ACCESS_UPDATED,
    INVENTORY_CATEGORY_ADD,
    INVENTORY_CATEGORY_EDIT,
    INVENTORY_CATEGORY_DELETE,
    INVENTORY_SUB_CATEGORY_ADD,
    INVENTORY_SUB_CATEGORY_EDIT,
    INVENTORY_SUB_CATEGORY_DELETE,
    INVENTORY_SUB_CATEGORY_ITEM_ADD,
    INVENTORY_SUB_CATEGORY_ITEM_EDIT,
    INVENTORY_SUB_CATEGORY_ITEM_DELETE,
    COST_CATEGORY_ADD,
    UPLOAD_INITIAL_INVOICE,
    UPLOAD_SUBSCRIBER_CONTRACT,
    EDIT_PAYMENT_INFO,
    SUBSCRIBER_PAYMENT_ADD,
    UPDATE_DECISION_TREE
}

export const AppSuccessDefaultMessages = {};

AppSuccessDefaultMessages[AppSuccess.TERM_UPDATE] = 'Terms and Conditions saved successfully';
AppSuccessDefaultMessages[AppSuccess.BILLING_UPDATE] = 'Billing Comment saved successfully';
AppSuccessDefaultMessages[AppSuccess.PROPOSAL_UPDATE] = 'Proposal Comment saved successfully';
AppSuccessDefaultMessages[AppSuccess.SUBSCRIBER_USER_ADD] = 'Subscriber user created successfully. An activation email has been sent to the user. Status will be automatically updated once the user sets a password.';
AppSuccessDefaultMessages[AppSuccess.SUBSCRIBER_USER_UPDATE] = 'Subscriber user updated successfully';
AppSuccessDefaultMessages[AppSuccess.USER_LOGIN] = 'Login was successful';
AppSuccessDefaultMessages[AppSuccess.FORGOT_PASSWORD] = 'Link for password reset has been sent to your email';
AppSuccessDefaultMessages[AppSuccess.RESET_PASSWORD] = 'You can use new password';
AppSuccessDefaultMessages[AppSuccess.SUBSCRIBER_LOGO_UPLOAD] = 'Subscriber logo uploaded successfully';
AppSuccessDefaultMessages[AppSuccess.ACCOUNT_LOGO_UPLOAD] = 'Account logo uploaded successfully';
AppSuccessDefaultMessages[AppSuccess.PACKAGE_LOGO_UPLOAD] = 'Package logo uploaded successfully';
AppSuccessDefaultMessages[AppSuccess.SUBSCRIBER_INFO_UPDATE] = 'Subscriber information has been updated';
AppSuccessDefaultMessages[AppSuccess.SUBSCRIBER_ADD] = 'Subscriber has been added';
AppSuccessDefaultMessages[AppSuccess.SUBSCRIBER_CONTACT_PERSON_UPDATE] = 'Contact person information has been updated';
AppSuccessDefaultMessages[AppSuccess.VENUE_CONTACT_UPDATE] = 'Contact person information has been updated';
AppSuccessDefaultMessages[AppSuccess.VENUE_LOGO_UPLOAD] = 'Property logo uploaded successfully';
AppSuccessDefaultMessages[AppSuccess.VENUE_UPDATE] = 'Property updated successfully';
AppSuccessDefaultMessages[AppSuccess.VENUE_ADD] = 'Property created successfully';
AppSuccessDefaultMessages[AppSuccess.USER_VERIFICATION] = 'You can use new password';
AppSuccessDefaultMessages[AppSuccess.ADD_VENUE_CONTACT] = 'Venue contact created successfully';
AppSuccessDefaultMessages[AppSuccess.EDIT_VENUE_CONTACT] = 'Venue contact updated successfully';
AppSuccessDefaultMessages[AppSuccess.EVENT_SPACE_EDIT] = 'Event Space updated successfully';
AppSuccessDefaultMessages[AppSuccess.EVENT_SPACE_ADD] = 'Event Space created successfully';
AppSuccessDefaultMessages[AppSuccess.FUNCTION_SPACE_ADD] = 'Function Space created successfully';
AppSuccessDefaultMessages[AppSuccess.FUNCTION_SPACE_EDIT] = 'Function Space updated successfully';
AppSuccessDefaultMessages[AppSuccess.STORAGE_SPACE_ADD] = 'Storage Space created successfully';
AppSuccessDefaultMessages[AppSuccess.VENDOR_ADD] = 'Vendor created successfully';
AppSuccessDefaultMessages[AppSuccess.VENDOR_CONTACT_ADD] = 'Vendor Contact created successfully';
AppSuccessDefaultMessages[AppSuccess.INVENTORY_ADD] = 'Inventory asset created successfully';
AppSuccessDefaultMessages[AppSuccess.INVENTORY_DOCUMENT_ADD] = 'Inventory document added';
AppSuccessDefaultMessages[AppSuccess.STORAGE_SPACE_EDIT] = 'Storage Space updated successfully';
AppSuccessDefaultMessages[AppSuccess.COST_CATEGORY_EDIT] = 'Cost Category updated successfully';
AppSuccessDefaultMessages[AppSuccess.REVENUE_CATEGORY_EDIT] = 'Revenue Category updated successfully';
AppSuccessDefaultMessages[AppSuccess.VENDOR_EDIT] = 'Vendor updated successfully';
AppSuccessDefaultMessages[AppSuccess.INVENTORY_EDIT] = 'Inventory asset updated successfully';
AppSuccessDefaultMessages[AppSuccess.VENDOR_CONTACT_EDIT] = 'Vendor Contact updated successfully';
AppSuccessDefaultMessages[AppSuccess.INVENTORY_ASSET_STATUS_EDIT] = 'Inventory asset status updated successfully';
AppSuccessDefaultMessages[AppSuccess.INVENTORY_ASSET_MAINTENANCE_EDIT] = 'Inventory asset maintenance info updated successfully';
AppSuccessDefaultMessages[AppSuccess.INVENTORY_LOGO_UPLOAD] = 'Inventory logo uploaded';
AppSuccessDefaultMessages[AppSuccess.INVENTORY_DOCUMENT_UPLOAD] = 'Inventory document uploaded';
AppSuccessDefaultMessages[AppSuccess.INPUT_BUDGET_EDIT] = 'Input budget updated successfully';
AppSuccessDefaultMessages[AppSuccess.SERVICE_CHARGE_ADD] = 'Service Charge added successfully';
AppSuccessDefaultMessages[AppSuccess.ADD_EQUIPMENT_SYSTEM] = 'Equipment System added successfully';
AppSuccessDefaultMessages[AppSuccess.SERVICE_CHARGE_EDIT] = 'Service Charge updated successfully';
AppSuccessDefaultMessages[AppSuccess.TAX_CHARGE_ADD] = 'Tax Charge added successfully';
AppSuccessDefaultMessages[AppSuccess.TAX_CHARGE_EDIT] = 'Tax Charge updated successfully';
AppSuccessDefaultMessages[AppSuccess.EDIT_PRICING_EXTENSION] = 'Pricing extension updated successfully';
AppSuccessDefaultMessages[AppSuccess.EQUIPMENT_ADD] = 'Equipment added successfully';
AppSuccessDefaultMessages[AppSuccess.EQUIPMENT_EDIT] = 'Equipment updated successfully';
AppSuccessDefaultMessages[AppSuccess.SERVICE_ADD] = 'Service added successfully';
AppSuccessDefaultMessages[AppSuccess.ACCOUNT_ADD] = 'Account added successfully';
AppSuccessDefaultMessages[AppSuccess.ADD_PACKAGE] = 'Package added successfully';
AppSuccessDefaultMessages[AppSuccess.ADD_EVENT] = 'Event added successfully';
AppSuccessDefaultMessages[AppSuccess.TICKET_ADD] = 'Service Ticket added successfully';
AppSuccessDefaultMessages[AppSuccess.SERVICE_EDIT] = 'Service updated successfully';
AppSuccessDefaultMessages[AppSuccess.EDIT_EQUIPMENT_SYSTEM] = 'Equipment System updated successfully';
AppSuccessDefaultMessages[AppSuccess.EDIT_PACKAGE] = 'Package updated successfully';
AppSuccessDefaultMessages[AppSuccess.EDIT_EVENT] = 'Event updated successfully';
AppSuccessDefaultMessages[AppSuccess.TICKET_EDIT] = 'Service Ticket updated successfully';
AppSuccessDefaultMessages[AppSuccess.ACCOUNT_UPDATE] = 'Account updated successfully';
AppSuccessDefaultMessages[AppSuccess.ADD_PURCHASE_ORDER] = 'Purchase order added successfully';
AppSuccessDefaultMessages[AppSuccess.EDIT_PURCHASE_ORDER] = 'Purchase order updated successfully';
AppSuccessDefaultMessages[AppSuccess.DELETE_PURCHASE_ORDER] = 'Purchase order deleted successfully';
AppSuccessDefaultMessages[AppSuccess.ADD_CONTRACT] = 'Contract added successfully';
AppSuccessDefaultMessages[AppSuccess.EDIT_CONTRACT] = 'Contract updated successfully';
AppSuccessDefaultMessages[AppSuccess.DELETE_CONTRACT] = 'Contract deleted successfully';
AppSuccessDefaultMessages[AppSuccess.UPLOAD_CONTRACT_DOCUMENT] = 'Document uploaded successfully';
AppSuccessDefaultMessages[AppSuccess.ADD_FUNCTION] = 'Function is added successfully';
AppSuccessDefaultMessages[AppSuccess.EDIT_FUNCTION] = 'Function is updated successfully';
AppSuccessDefaultMessages[AppSuccess.EVENT_CONTACT_ADD] = 'Event contact added successfully';
AppSuccessDefaultMessages[AppSuccess.EVENT_CONTACT_EDIT] = 'Event contact updated successfully';
AppSuccessDefaultMessages[AppSuccess.EVENT_CONTACT_DELETE] = 'Event contact deleted successfully';
AppSuccessDefaultMessages[AppSuccess.ACCESS_UPDATED] = 'Access updated';
AppSuccessDefaultMessages[AppSuccess.INVENTORY_CATEGORY_ADD] = 'Category added successfully';
AppSuccessDefaultMessages[AppSuccess.INVENTORY_CATEGORY_EDIT] = 'Category updated successfully';
AppSuccessDefaultMessages[AppSuccess.INVENTORY_CATEGORY_DELETE] = 'Category deleted successfully';
AppSuccessDefaultMessages[AppSuccess.INVENTORY_SUB_CATEGORY_ADD] = 'Sub category added successfully';
AppSuccessDefaultMessages[AppSuccess.INVENTORY_SUB_CATEGORY_EDIT] = 'Sub category updated successfully';
AppSuccessDefaultMessages[AppSuccess.INVENTORY_SUB_CATEGORY_DELETE] = 'Sub category deleted successfully';
AppSuccessDefaultMessages[AppSuccess.INVENTORY_SUB_CATEGORY_ITEM_ADD] = 'Equipment/Service item added successfully';
AppSuccessDefaultMessages[AppSuccess.INVENTORY_SUB_CATEGORY_ITEM_EDIT] = 'Equipment/Service item updated successfully';
AppSuccessDefaultMessages[AppSuccess.INVENTORY_SUB_CATEGORY_ITEM_DELETE] = 'Equipment/Service item deleted successfully';
AppSuccessDefaultMessages[AppSuccess.REVENUE_CATEGORY_ADD] = 'Revenue category added successfully';
AppSuccessDefaultMessages[AppSuccess.COST_CATEGORY_ADD] = 'Cost category added successfully';
AppSuccessDefaultMessages[AppSuccess.UPLOAD_INITIAL_INVOICE] = 'Initial invoice uploaded successfully';
AppSuccessDefaultMessages[AppSuccess.UPLOAD_SUBSCRIBER_CONTRACT] = 'Contract uploaded successfully';
AppSuccessDefaultMessages[AppSuccess.EDIT_PAYMENT_INFO] = 'Payment info updated successfully';
AppSuccessDefaultMessages[AppSuccess.SUBSCRIBER_PAYMENT_ADD] = 'Payment added successfully';
AppSuccessDefaultMessages[AppSuccess.UPDATE_DECISION_TREE] = 'Decision Tree updated successfully';

