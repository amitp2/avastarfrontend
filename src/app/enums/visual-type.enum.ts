export enum VisualType {
    DEFAULT,
    WARNING,
    DANGER,
    INFO,
    PRIMARY
}
