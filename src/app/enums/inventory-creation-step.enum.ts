export enum InventoryCreationStep {
    AssetInformation,
    ServiceAndMaintenance,
    AssetStatus,
    Documents
}
