export enum EventType {
    Group = 'Group',
    Local = 'Local',
    Internal = 'Internal'
}