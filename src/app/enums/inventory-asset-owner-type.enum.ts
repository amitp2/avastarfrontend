export enum InventoryAssetOwnerType {
    Owner = 'Owner',
    Installed = 'Installed',
    Vendor = 'Vendor'
}
