export enum InventoryStorageType {
    StorageSpace = 'Storage Space',
    EventSpace = 'Event Space'
}
