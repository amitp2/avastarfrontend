export enum InventoryMaintenanceSchedule {
    MONTHLY = 'Monthly',
    QUARTERLY = 'Quarterly',
    SEMI_ANNUAL = 'Semi Annual',
    ANNUAL = 'Annual'
}
