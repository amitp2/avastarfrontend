export enum VendorStatus {
    Active = 'Active',
    Inactive = 'Inactive'
}
