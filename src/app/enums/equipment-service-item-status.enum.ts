export enum EquipmentServiceItemStatus {
    All = 'ALL',
    Assigned = 'ASSIGNED',
    UnAssigned = 'UNASSIGNED'
}
