export enum FunctionSpaceStatus {
    Active = 'Active',
    Inactive = 'Inactive'
}
