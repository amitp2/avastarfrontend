export enum InventoryAssetStatus {
    Active = 'Active',
    AtRepair = 'At Repair',
    Broken = 'Broken',
    Missing = 'Missing',
    Removed = 'Removed',
    ServiceRequired = 'Service Required',
    All = 'All'
}
