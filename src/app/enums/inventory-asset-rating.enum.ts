export enum InventoryAssetRating {
    Excellent = 'Excellent',
    Acceptable = 'Acceptable',
    SubStandard = 'Substandard'
}
