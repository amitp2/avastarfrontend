export enum UserRole {
    NONE = 'None',
    SUPER_ADMIN = 'Super Admin',
    ADMIN = 'Admin',
    TECH_TEAM_LEAD = 'Tech Team Lead',
    TECH_TEAM = 'Tech Team Member',
    EVENT_PLANNER = 'Event Planner',
    SERVICE_TEAM = 'Service Team'
}
