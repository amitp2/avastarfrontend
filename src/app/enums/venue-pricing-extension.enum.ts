export enum VenuePricingExtension {
    Both = 'BOTH',
    Package = 'PACKAGE',
    Equipment = 'EQUIPMENT'
}
