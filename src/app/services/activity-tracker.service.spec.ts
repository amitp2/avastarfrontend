import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Config, ConfigService } from './config.service';
import { ActivityTrackerService } from './activity-tracker.service';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

class AuthServiceStub {
    logout(): Promise<any> {
        return Promise.resolve();
    }

    isUserRemembered(): boolean {
        return false;
    }
}

class RouterStub {
    navigateByUrl() {
    }
}


describe('ActivityTrackerService', () => {

    let activityTrackerService: ActivityTrackerService;
    let configService: ConfigService;
    let authService: AuthService;
    let router: Router;
    let autoLogoutMilliseconds: number;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                ActivityTrackerService,
                ConfigService,
                {provide: AuthService, useClass: AuthServiceStub},
                {provide: Router, useClass: RouterStub}
            ]
        });

        activityTrackerService = TestBed.get(ActivityTrackerService);
        configService = TestBed.get(ConfigService);
        authService = TestBed.get(AuthService);
        router = TestBed.get(Router);
        autoLogoutMilliseconds = configService.config().AUTO_LOGOUT_SECONDS * 1000;
    });

    xit(`it should call logout on AuthService and redirect to "/" after predetermined
        amount of seconds, after last tracked event`, fakeAsync(() => {
        const spy = spyOn(authService, 'logout');
        const redirectSpy = spyOn(router, 'navigateByUrl');

        spy.and.callFake(() => {
        });

        activityTrackerService.track();
        tick(autoLogoutMilliseconds - 1);
        expect(spy.calls.all().length).toEqual(0);

        activityTrackerService.track();
        tick(autoLogoutMilliseconds - 1);
        expect(spy.calls.all().length).toEqual(0);

        tick(autoLogoutMilliseconds - 1);
        expect(spy.calls.all().length).toEqual(1);
        expect(redirectSpy.calls.first().args[0]).toEqual('/');
    }));

    it('if user should be remembered it should not logout user', fakeAsync(() => {
        spyOn(authService, 'isUserRemembered').and.returnValue(true);
        const logoutSpy = spyOn(authService, 'logout').and.callThrough();

        activityTrackerService.track();
        tick(autoLogoutMilliseconds);
        expect(logoutSpy.calls.all().length).toEqual(0);
    }));
});
