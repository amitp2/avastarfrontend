import { Injectable, NgZone } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ConfigService } from './config.service';
import { AppError, AppErrorByCode } from '../enums/app-error.enum';
import { CurrentUser } from '../interfaces/current-user';
import { UtilsService } from './utils.service';
import { AppCache } from '../interfaces/cache';
import { UserRole } from '../enums/user-role.enum';
import { AppErrorNamespace, AppErrorNamespaceCodes } from '../enums/app-error-namespace.enum';

@Injectable()
export class AuthService {

    interval: any;
    currentUserSnapshot: CurrentUser;

    constructor(private http: HttpClient,
                private configService: ConfigService,
                private zone: NgZone,
                private utilsService: UtilsService,
                private cache: AppCache) {
    }

    login(email: string, password: string, rememberMe: boolean = true): Promise<boolean | AppError> {
        return new Promise((resolve, reject) => {
            this.http
                .post<{ token: string }>(`${this.configService.config().API_URL}auth/login`, {
                    username: email,
                    password
                })
                .subscribe((resp: { token: string, refreshToken: string }) => {
                    console.log("Check login---"+resp);
                    window.localStorage.setItem(this.configService.config().TOKEN_NAME, resp.token);
                    window.localStorage.setItem(this.configService.config().REFRESH_TOKEN_NAME, resp.refreshToken);


                    if (rememberMe) {
                        window.localStorage.setItem(this.configService.config().REMEMBER_ME_PROPERTY_NAME, 'true');
                    } else {
                        window.localStorage.setItem(this.configService.config().REMEMBER_ME_PROPERTY_NAME, 'false');
                    }

                    this.startTokenRefreshProcess();

                    resolve(true);
                }, (error: HttpErrorResponse) => {
                    const code = +error.error.errorCode;
                    reject(AppErrorNamespaceCodes[AppErrorNamespace.Login][code]);
                    // reject(AppErrorByCode[]);
                });
        });
    }

    startTokenRefreshProcess() {
        const interval = this.configService.config().TOKEN_LIFETIME_MINUTES * 60 * 1000;

        this.zone.runOutsideAngular(() => {
            this.interval = window.setInterval(() => {
                this.zone.run(() => {
                    this.refreshToken()
                        .catch(() => {
                            this.logout();
                        });
                });
            }, interval);
        });
    }

    isUserAuthorized(): Promise<boolean> {
        return new Promise<boolean>((resolve) => {

            if (!window.localStorage.getItem(this.configService.config().TOKEN_NAME)) {
                this.logout();
                return resolve(false);
            }

            this.http.get(`${this.configService.config().API_URL}me`).subscribe(() => {
                resolve(true);
            }, () => {

                this.refreshToken()
                    .then(() => {
                        resolve(true);
                    })
                    .catch(() => {
                        this.logout();
                        resolve(false);
                    });
            });

        });
    }

    isUserRemembered(): boolean {
        return window.localStorage.getItem(this.configService.config().REMEMBER_ME_PROPERTY_NAME) === 'true';
    }

    logout(): Promise<any> {
        return new Promise(resolve => {
            window.localStorage.removeItem(this.configService.config().TOKEN_NAME);
            window.localStorage.removeItem(this.configService.config().REFRESH_TOKEN_NAME);
            window.localStorage.removeItem(this.configService.config().REMEMBER_ME_PROPERTY_NAME);
            window.clearInterval(this.interval);
            this.cache.remove(this.configService.config().CURRENT_USER_CACHE_KEY);
            resolve();
        });
    }

    refreshToken(): Promise<any> {
        return new Promise((resolve, reject) => {

            this.http.get(`${this.configService.config().API_URL}auth/token`, {
                headers: new HttpHeaders({
                    Authorization: 'Bearer ' + window.localStorage.getItem(this.configService.config().REFRESH_TOKEN_NAME)
                })
            }).subscribe((resp: { token: string }) => {
                window.localStorage.setItem(this.configService.config().TOKEN_NAME, resp.token);
                resolve();
            }, () => {
                reject();
            });

        });
    }

    forgotPassword(email: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http
                .put(`${this.configService.config().API_URL}users/${email}/reset_password`, {}, {
                    responseType: 'text'
                })
                .subscribe(() => {
                    resolve();
                }, () => {
                    reject(AppError.FORGOT_PASSWORD_FAILED);
                });
        });
    }

    resetPassword(token: string, password: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http
                .put(`${this.configService.config().API_URL}users/reset_password`, {token, password})
                .subscribe(() => {
                    resolve();
                }, () => {
                    reject(AppError.RESET_PASSWORD_FAILED);
                });
        });
    }

    verify(token: string, password: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http
                .put(`${this.configService.config().API_URL}users/verify_email`, {token, password})
                .subscribe(() => {
                    resolve();
                }, () => {
                    reject(AppError.RESET_PASSWORD_FAILED);
                });
        });
    }

    currentUser(): Promise<CurrentUser> {
        return new Promise((resolve, reject) => {

            const cacheKey = this.configService.config().CURRENT_USER_CACHE_KEY;

            this.cache.hasKey(cacheKey).then((has: boolean) => {
                if (has) {
                    this.cache.get(cacheKey).then(user => resolve(user));
                    return;
                }

                this.http
                    .get(`${this.configService.config().API_URL}me`)
                    .subscribe((resp: any) => {
                        const res: any = {
                            ...resp,
                            roles: resp.roles.map(r => this.utilsService.stringToUserRole(r))
                        };

                        this.currentUserSnapshot = res;

                        this.cache.set(cacheKey, res);
                        resolve(res);
                    }, () => {
                        reject(AppError.CURRENT_USER_FAILED);
                    });
            });

        });
    }

    getToken(): string {
        return localStorage.getItem(this.configService.config().TOKEN_NAME);
    }

}
