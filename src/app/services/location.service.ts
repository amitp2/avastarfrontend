import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config.service';
import { GeolocationDataModel } from '../models/geolocation-data.model';
import { Store } from '@ngrx/store';
import { AppState } from '../store/store';
import { SetCountries, SetLocation, SetStates } from '../store/location/location.actions';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import { CountryModel } from '../models/country-model';
import { StateModel } from '../models/state-model';

@Injectable()
export class LocationService {
    constructor(
        private httpClient: HttpClient,
        private config: ConfigService,
        private store: Store<AppState>
    ) {
    }

    fetchCurrent(): Promise<GeolocationDataModel> {
        return this.httpClient.get<GeolocationDataModel>(this.config.getApiUrl() + 'users/location')
            .map(location => new GeolocationDataModel(location))
            .do(location => this.store.dispatch(new SetLocation(location)))
            .toPromise();
    }

    fetchCountries(): Promise<CountryModel[]> {
        const config = {
            params: {
                page: 0,
                size: 999999
            }
        } as any;
        return this.httpClient.get(this.config.getApiUrl() + 'utils/countries', config)
         //return this.httpClient.get(this.config.getApiUrl() + 'utils/countries')
            .map((response: any) => response.content.map(country => new CountryModel(country)))
            .do(countries => this.store.dispatch(new SetCountries(countries)))
            .toPromise();
    }

    fetchStates(): Promise<StateModel[]> {
        const config = {
            params: {
                page: 0,
                size: 999999
            }
        } as any;
        return this.httpClient.get(this.config.getApiUrl() + 'utils/states', config)
         //return this.httpClient.get(this.config.getApiUrl() + 'utils/states')
            .map((response: any) => response.content.map(state => new StateModel(state)))
            .do(states => this.store.dispatch(new SetStates(states)))
            .toPromise();
    }
}
