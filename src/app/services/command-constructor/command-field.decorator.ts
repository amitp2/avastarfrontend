import { SimpleMapper } from '../../mappers/simple-mapper';
import { generateFieldDecorator } from '../../mappers/generate-field-decorator';

export const COMMAND_FIELDS_KEY = '_command_fields_list_';
export const COMMAND_FIELDS_MAPPERS_KEY = '_command_fields_mappers_list_';

export function CommandField(mapTo: string = '', mapper: any = SimpleMapper) {
    return generateFieldDecorator(
        COMMAND_FIELDS_KEY,
        COMMAND_FIELDS_MAPPERS_KEY,
        mapTo,
        mapper
    );
}
