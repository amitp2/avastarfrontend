import { Injectable } from '@angular/core';
import { COMMAND_FIELDS_KEY, COMMAND_FIELDS_MAPPERS_KEY } from './command-field.decorator';
import { UtilsService } from '../utils.service';

@Injectable()
export class CommandConstructorService {

    static build(modelInstance: any): any {
        return UtilsService.buildObjectFromMappingDecorators(
            modelInstance,
            COMMAND_FIELDS_KEY,
            COMMAND_FIELDS_MAPPERS_KEY
        );
    }

    constructor(private utils: UtilsService) {
    }

    build(modelInstance: any): any {
        return this.utils.buildObjectFromMappingDecorators(
            modelInstance,
            COMMAND_FIELDS_KEY,
            COMMAND_FIELDS_MAPPERS_KEY
        );
    }

}
