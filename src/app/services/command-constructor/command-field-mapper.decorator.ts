import { COMMAND_FIELDS_MAPPERS_KEY } from './command-field.decorator';
import { generateFieldMapperDecorator } from '../../mappers/generate-field-mapper-decorator';


export function CommandFieldMapper(mapper: any) {
    return generateFieldMapperDecorator(
        COMMAND_FIELDS_MAPPERS_KEY,
        mapper
    );
}
