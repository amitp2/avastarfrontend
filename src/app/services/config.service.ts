import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

export class Config {
    API_URL: string;
    TOKEN_NAME: string;
    REFRESH_TOKEN_NAME: string;
    REMEMBER_ME_PROPERTY_NAME: string;
    AUTO_LOGOUT_SECONDS: number;
    TOKEN_LIFETIME_MINUTES: number;
    CURRENT_USER_CACHE_KEY: string;
}

@Injectable()
export class ConfigService {
    private readonly _config: Config;

    constructor() {
        this._config = this.getEnvConfig();
    }

    public config(): Config {
        return this._config;
    }

    public getApiUrl(path: string = ''): string {
        return this._config.API_URL + path;
    }

    public getTokenName(): string {
        return this._config.TOKEN_NAME;
    }

    private getEnvConfig(): Config {
        if (environment.isDev) {
            return {
               API_URL: 'https://dev-api.avastar.io/',
                //API_URL: 'http://localhost/',
                TOKEN_NAME: 'AVASTAR_USER_TOKEN',
                REFRESH_TOKEN_NAME: 'AVASTAR_USER_REFRESH_TOKEN',
                REMEMBER_ME_PROPERTY_NAME: 'AVASTAR_USER_REMEMBER',
                TOKEN_LIFETIME_MINUTES: 10,
                AUTO_LOGOUT_SECONDS: 30 * 60,
                CURRENT_USER_CACHE_KEY: 'current_user'
            };
        }

        if (environment.isStaging) {
            return {
                API_URL: 'https://staging-api.avastar.io/',
                TOKEN_NAME: 'STG_AVASTAR_USER_TOKEN',
                REFRESH_TOKEN_NAME: 'STG_AVASTAR_USER_REFRESH_TOKEN',
                REMEMBER_ME_PROPERTY_NAME: 'STG_AVASTAR_USER_REMEMBER',
                TOKEN_LIFETIME_MINUTES: 10,
                AUTO_LOGOUT_SECONDS: 30 * 60,
                CURRENT_USER_CACHE_KEY: 'current_user'
            };
        }

        if (environment.production) {
            return {
                API_URL: 'https://api.avastar.io/',
                TOKEN_NAME: 'PROD_AVASTAR_USER_TOKEN',
                REFRESH_TOKEN_NAME: 'PROD_AVASTAR_USER_REFRESH_TOKEN',
                REMEMBER_ME_PROPERTY_NAME: 'PROD_AVASTAR_USER_REMEMBER',
                TOKEN_LIFETIME_MINUTES: 10,
                AUTO_LOGOUT_SECONDS: 30 * 60,
                CURRENT_USER_CACHE_KEY: 'current_user'
            };
        }

        return {
            API_URL: 'http://localhost:4200/',
            TOKEN_NAME: 'AVASTAR_USER_TOKEN',
            REFRESH_TOKEN_NAME: 'AVASTAR_USER_REFRESH_TOKEN',
            REMEMBER_ME_PROPERTY_NAME: 'AVASTAR_USER_REMEMBER',
            TOKEN_LIFETIME_MINUTES: 10,
            AUTO_LOGOUT_SECONDS: 30 * 60,
            CURRENT_USER_CACHE_KEY: 'current_user'
        };
    }

}
