import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilsService } from './utils.service';
import { ConfigService } from './config.service';
import { CostBudgetModel } from '../models/cost-budget-model';
import { RevenueBudgetModel } from '../models/revenue-budget-model';

@Injectable()
export class BudgetService {

    constructor(
        private httpClient: HttpClient,
        private utils: UtilsService,
        private config: ConfigService
    ) {}

    get apiUrl() {
        return this.config.config().API_URL;
    }

    getCostBudgetBySubscriberIdAndYear(subscriberId: number, year: number): Promise<CostBudgetModel[]> {
        const queryParams: any = { year };
        const url = `${this.apiUrl}accounting/subscribers/${subscriberId}/cost-budgets`;

        return new Promise((resolve, reject) => {
            this.httpClient.get<CostBudgetModel[]>(url, {
                params: queryParams
            }).subscribe(
                items => resolve(items),
                error => reject(error)
            );
        });
    }

    getRevenueBudgetBySubscriberIdAndYear(subscriberId: number, year: number): Promise<RevenueBudgetModel[]> {
        const queryParams: any = { year };
        const url = `${this.apiUrl}accounting/subscribers/${subscriberId}/revenue-budgets`;

        return new Promise((resolve, reject) => {
            this.httpClient.get<RevenueBudgetModel[]>(url, {
                params: queryParams
            }).subscribe(
                items => resolve(items),
                error => reject(error)
            );
        });
    }

    updateRevenueBudgetBySubscriberIdAndYear(subscriberId: number, year: number, budget: CostBudgetModel[]) {
        const url = `${this.apiUrl}accounting/subscribers/${subscriberId}/revenue-budgets/year/${year}`;
        return new Promise((resolve, reject) => {
            this.httpClient.post(url, budget).subscribe(
                data => resolve(data),
                error => reject(error)
            );
        });
    }

    updateCostBudgetBySubscriberIdAndYear(subscriberId: number, year: number, budget: CostBudgetModel[]) {
        const url = `${this.apiUrl}accounting/subscribers/${subscriberId}/cost-budgets/year/${year}`;
        return new Promise((resolve, reject) => {
            this.httpClient.post(url, budget).subscribe(
                data => resolve(data),
                error => reject(error)
            );
        });
    }
}
