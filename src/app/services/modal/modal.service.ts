import { ComponentFactoryResolver, ElementRef, Injectable, ViewContainerRef } from '@angular/core';
import { GlobalsService } from '../globals.service';
import { ConfirmDialogComponent } from '../../components/dialog/confirm-dialog/confirm-dialog.component';

@Injectable()
export class ModalService {

    private modalViewContainerRef: ViewContainerRef;

    constructor(private globalsService: GlobalsService,
                private componentFactoryResolver: ComponentFactoryResolver) {
    }

    setModalRef(modalRef: ViewContainerRef) {
        this.modalViewContainerRef = modalRef;
    }

    private show(nativeElement) {
        this.globalsService.globals().$(nativeElement).modal({
            backdrop: 'static',
            keyboard: true
        });
    }

    private close(nativeElement) {
        this.globalsService.globals().$(nativeElement).modal('hide');
    }

    showModal(elementRef: ElementRef) {
        this.show(elementRef.nativeElement);
    }

    showConfirm(message: string, title: string = 'Warning'): Promise<boolean> {
        return new Promise(resolve => {

            if (confirm(message)) {
                resolve(true);
                return;
            }

            resolve(false);

            // this.modalViewContainerRef.clear();
            // const factory = this.componentFactoryResolver.resolveComponentFactory(ConfirmDialogComponent);
            // const component = factory.create(this.modalViewContainerRef.parentInjector);
            // component.instance.title = title;
            // component.instance.message = message;
            // component.instance.cancel = () => {
            //     this.close(component.location.nativeElement.firstChild);
            //     resolve(false);
            // };
            // component.instance.ok = () => {
            //     this.close(component.location.nativeElement.firstChild);
            //     resolve(true);
            // };
            // this.modalViewContainerRef.insert(component.hostView);
            // this.show(component.location.nativeElement.firstChild);
        });
    }

}
