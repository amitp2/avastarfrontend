import { Injectable } from '@angular/core';
import { UtilsService } from '../utils.service';
import { RESOURCE_FIELDS_KEY, RESOURCE_FIELDS_MAPPERS_KEY } from './resource-field.decorator';

@Injectable()
export class ResourceConstructorService {

    static build(source: any, instanceClass: any): any {
        return UtilsService.buildObjectFromMappingDecoratorsInverse(
            source,
            instanceClass,
            RESOURCE_FIELDS_KEY,
            RESOURCE_FIELDS_MAPPERS_KEY
        );
    }

    constructor() {
    }

    build(source: any, instanceClass: any): any {
        return ResourceConstructorService.build(source, instanceClass);
    }

}
