import { SimpleMapper } from '../../mappers/simple-mapper';
import { generateFieldDecorator } from '../../mappers/generate-field-decorator';

export const RESOURCE_FIELDS_KEY = '_resource_fields_list_';
export const RESOURCE_FIELDS_MAPPERS_KEY = '_resource_fields_mappers_list_';

export function ResourceField(mapTo: string = '', mapper: any = SimpleMapper) {
    return generateFieldDecorator(
        RESOURCE_FIELDS_KEY,
        RESOURCE_FIELDS_MAPPERS_KEY,
        mapTo,
        mapper
    );
}
