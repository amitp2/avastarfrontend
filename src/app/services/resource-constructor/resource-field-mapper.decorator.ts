import { generateFieldMapperDecorator } from '../../mappers/generate-field-mapper-decorator';
import { RESOURCE_FIELDS_MAPPERS_KEY } from './resource-field.decorator';


export function ResourceFieldMapper(mapper: any) {
    return generateFieldMapperDecorator(
        RESOURCE_FIELDS_MAPPERS_KEY,
        mapper
    );
}
