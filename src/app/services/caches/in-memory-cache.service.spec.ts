import { TestBed, inject, fakeAsync, tick } from '@angular/core/testing';

import { InMemoryCacheService } from './in-memory-cache.service';

describe('InMemoryCacheService', () => {

    let inMemoryCacheService: InMemoryCacheService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [InMemoryCacheService]
        });

        inMemoryCacheService = TestBed.get(InMemoryCacheService);
    });

    it('it should store objects and values', fakeAsync(() => {
        let res;
        const key1 = 'some_key_1';
        const key2 = 'some_key_2';
        inMemoryCacheService.set(key1, 'str 1');
        tick();
        inMemoryCacheService.get(key1).then(val => res = val);
        tick();
        expect(res).toEqual('str 1');

        inMemoryCacheService.set(key2, {name: 'some name'});
        tick();
        inMemoryCacheService.get(key2).then(val => res = val);
        tick();
        expect(res).toEqual({name: 'some name'});
    }));

    it('remove should remove value from cache', fakeAsync(() => {
        let res;
        const key1 = 'some_key_1';

        inMemoryCacheService.hasKey(key1).then(val => res = val);
        tick();
        expect(res).toEqual(false);

        inMemoryCacheService.set(key1, 'str 1');
        inMemoryCacheService.hasKey(key1).then(val => res = val);
        tick();
        expect(res).toEqual(true);

        inMemoryCacheService.remove(key1);
        inMemoryCacheService.hasKey(key1).then(val => res = val);
        tick();
        expect(res).toEqual(false);
    }));
});
