import { Injectable } from '@angular/core';
import { AppCache } from '../../interfaces/cache';

@Injectable()
export class InMemoryCacheService implements AppCache {

    private cache: any;

    constructor() {
        this.cache = {};
    }

    set(key: string, value: any): Promise<any> {
        this.cache[key] = value;
        return Promise.resolve();
    }

    get(key: string): Promise<any> {
        return Promise.resolve(this.cache[key]);
    }

    remove(key: string): Promise<any> {
        delete this.cache[key];
        return Promise.resolve();
    }

    hasKey(key: string): Promise<boolean> {
        return Promise.resolve(this.cache.hasOwnProperty(key));
    }

}
