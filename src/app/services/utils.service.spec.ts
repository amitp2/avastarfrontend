import { TestBed, inject } from '@angular/core/testing';

import { UtilsService } from './utils.service';
import { UserRole } from '../enums/user-role.enum';
import { UserStatus } from '../enums/user-status.enum';
import { EventSpaceType } from '../enums/event-space-type.enum';
import { ObjectTransformerType } from '../enums/object-transformer-type.enum';

describe('UtilsService', () => {
    let utilsService: UtilsService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [UtilsService]
        });
        utilsService = TestBed.get(UtilsService);
    });

    it('tableToHttpParams should translate paging table loading params to http resource query params', () => {
        // utilsService
        let res;
        res = utilsService.tableToHttpParams({
            page: 5,
            itemsPerPage: 100,
            search: null,
            sortColumn: 'id',
            sortAsc: true
        });

        expect(res).toEqual({
            page: 4,
            size: 100,
            sortFields: ['id'],
            sortAsc: true
        });

        res = utilsService.tableToHttpParams({
            page: 5,
            itemsPerPage: 100,
            search: 'some search',
            sortColumn: 'id',
            sortAsc: true
        });

        expect(res).toEqual({
            page: 4,
            size: 100,
            search: 'some search',
            sortFields: ['id'],
            sortAsc: true
        });
    });

    it('stringToUserRole should properly convert server strings to user roles', () => {
        expect(utilsService.stringToUserRole('ROLE_ADMIN')).toEqual(UserRole.ADMIN);
        expect(utilsService.stringToUserRole('ROLE_SUPER_ADMIN')).toEqual(UserRole.SUPER_ADMIN);
        expect(utilsService.stringToUserRole(null)).toEqual(UserRole.NONE);
    });

    it('userRoleToString should properly convert user role to string', () => {
        expect(utilsService.userRoleToString(UserRole.ADMIN)).toEqual('ROLE_ADMIN');
        expect(utilsService.userRoleToString(UserRole.SUPER_ADMIN)).toEqual('ROLE_SUPER_ADMIN');
        expect(utilsService.userRoleToString(UserRole.NONE)).toEqual(null);
    });

    it('userRoleToDisplayString should map roles to proper display strings', () => {
        expect(utilsService.userRoleToDisplayString(UserRole.ADMIN)).toEqual('Admin');
        expect(utilsService.userRoleToDisplayString(UserRole.SUPER_ADMIN)).toEqual('Super Admin');
        expect(utilsService.userRoleToDisplayString(UserRole.EVENT_PLANNER)).toEqual('Event Planner');
        expect(utilsService.userRoleToDisplayString(UserRole.TECH_TEAM)).toEqual('Tech Team');
        expect(utilsService.userRoleToDisplayString(UserRole.TECH_TEAM_LEAD)).toEqual('Tech Team Lead');
    });

    it('userHasRole should check if given user has role', () => {
        expect(utilsService.userHasRole({
            id: 1,
            firstName: 'f',
            username: 'u',
            roles: [UserRole.ADMIN, UserRole.SUPER_ADMIN]
        }, UserRole.ADMIN)).toEqual(true);

        expect(utilsService.userHasRole({
            id: 1,
            firstName: 'f',
            username: 'u',
            roles: [UserRole.SUPER_ADMIN]
        }, UserRole.ADMIN)).toEqual(false);
    });

    it('userStatusToString should translate properly to display texts', () => {
        expect(utilsService.userStatusToString(UserStatus.Blocked)).toEqual('Blocked');
        expect(utilsService.userStatusToString(UserStatus.Inactive)).toEqual('Inactive');
        expect(utilsService.userStatusToString(UserStatus.Active)).toEqual('Active');
        expect(utilsService.userStatusToString(UserStatus.NotVerified)).toEqual('Not Verified');
    });

    it('addHttpToUrl should add http to url if one is missing', () => {
        expect(utilsService.addHttpToUrl('fb.com')).toEqual('http://fb.com');
        expect(utilsService.addHttpToUrl('http://fb.com')).toEqual('http://fb.com');
    });

    it('eventSpaceTypeToText should properly translate statuses to text', () => {
        expect(utilsService.eventSpaceTypeToText(EventSpaceType.Active)).toEqual('Active');
        expect(utilsService.eventSpaceTypeToText(EventSpaceType.Inactive)).toEqual('Inactive');
    });

    it('transformInto should transform object with given transformer properly', () => {
        const source = {
            name: 'gio',
            surname: 'bakradze',
            age: 28,
            profession: 'programmer'
        };

        expect(utilsService.transformInto(source, {
            name: {
                type: ObjectTransformerType.Default,
                to: 'firstName'
            },
            surname: {
                type: ObjectTransformerType.Default,
                to: 'lastName'
            }
        })).toEqual({
            firstName: 'gio',
            lastName: 'bakradze'
        });

        expect(utilsService.transformInto(source, {
            name: {
                type: ObjectTransformerType.Function,
                to: 'firstName',
                transform: (val) => val.toUpperCase()
            }
        })).toEqual({
            firstName: 'GIO'
        });
    });

});
