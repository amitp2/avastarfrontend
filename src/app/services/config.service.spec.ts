import { TestBed, inject } from '@angular/core/testing';

import { ConfigService } from './config.service';

describe('ConfigService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [ConfigService]
        });
    });

    it('should be created', inject([ConfigService], (service: ConfigService) => {
        expect(service).toBeTruthy();
    }));

    it('config method should return object with necessary properties', inject([ConfigService], (service: ConfigService) => {
        const configObject = service.config();
        expect(configObject.hasOwnProperty('API_URL')).toBeTruthy();
        expect(configObject.hasOwnProperty('TOKEN_NAME')).toBeTruthy();
        expect(configObject.hasOwnProperty('REFRESH_TOKEN_NAME')).toBeTruthy();
        expect(configObject.hasOwnProperty('REMEMBER_ME_PROPERTY_NAME')).toBeTruthy();
        expect(configObject.hasOwnProperty('AUTO_LOGOUT_SECONDS')).toBeTruthy();
        expect(configObject.hasOwnProperty('TOKEN_LIFETIME_MINUTES')).toBeTruthy();
        expect(configObject.hasOwnProperty('CURRENT_USER_CACHE_KEY')).toBeTruthy();
    }));
});
