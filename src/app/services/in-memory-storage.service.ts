import { Injectable } from '@angular/core';
import { KeyValueStorage } from '../helpers/key-value-storage';

@Injectable()
export class InMemoryStorageService implements KeyValueStorage<string, any> {

    private storage = {};

    constructor() {
    }


    async put(key: string, value: any): Promise<void> {
        this.storage[key] = value;
    }

    async get(key: string): Promise<any> {
        return this.storage[key];
    }

    async drop(key: string): Promise<void> {
        delete this.storage[key];
    }

}
