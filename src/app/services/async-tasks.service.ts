import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { EventManager } from '../classes/event-manager';

const ALL_TASKS = 'ALL_TASKS_SOME_NAME';

export const enum AsyncTaskState {
    IN_PROGRESS,
    FINISHED,
    ERROR,
    SUCCESS,
    NOTIFY
}

export class AsyncTaskStatus {
    constructor(public task: string,
                public state: AsyncTaskState,
                public payload: any) {
    }
}

@Injectable()
export class AsyncTasksService extends EventManager {

    constructor() {
        super();
    }

    task(task: string, state: AsyncTaskState, payload: any = null) {
        this.eventsFor(task).next(new AsyncTaskStatus(task, state, payload));
        this.eventsFor(ALL_TASKS).next(new AsyncTaskStatus(task, state, payload));
    }

    taskSuccess(task: string, payload: any = null) {
        this.task(task, AsyncTaskState.SUCCESS, payload);
    }

    taskError(task: string, payload: any = null) {
        this.task(task, AsyncTaskState.ERROR, payload);
    }

    taskStart(task: string, payload: any = null) {
        this.task(task, AsyncTaskState.IN_PROGRESS, payload);
    }

    taskNotify(task: string, payload: any = null) {
        this.task(task, AsyncTaskState.NOTIFY, payload);
    }

    track(task: string): Observable<any> {
        return this.eventsFor(task);
    }

    trackAnyError(): Observable<any> {
        return this.eventsFor(ALL_TASKS).filter((task: AsyncTaskStatus) => task.state === AsyncTaskState.ERROR);
    }

    trackAnySuccess(): Observable<any> {
        return this.eventsFor(ALL_TASKS).filter((task: AsyncTaskStatus) => task.state === AsyncTaskState.SUCCESS);
    }

    trackAnyStart(): Observable<any> {
        return this.eventsFor(ALL_TASKS).filter((task: AsyncTaskStatus) => task.state === AsyncTaskState.IN_PROGRESS);
    }

    trackAnyFinish(): Observable<any> {
        return this.eventsFor(ALL_TASKS).filter((task: AsyncTaskStatus) => task.state === AsyncTaskState.FINISHED);
    }

}
