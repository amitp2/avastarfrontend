import { Injectable, NgZone } from '@angular/core';
import { ConfigService } from './config.service';
import { AuthService } from './auth.service';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import { Router } from '@angular/router';
import { ModalService } from './modal/modal.service';

@Injectable()
export class ActivityTrackerService {

    private tracker: Subject<any>;
    private timeout: any;

    constructor(private configService: ConfigService,
                private authService: AuthService,
                private router: Router,
                private zone: NgZone,
                private modal: ModalService) {
        this.tracker = new Subject();

        const after = this.configService.config().AUTO_LOGOUT_SECONDS * 1000;

        this.tracker
            .subscribe(() => {

                clearTimeout(this.timeout);

                this.zone.runOutsideAngular(() => {
                    this.timeout = setTimeout(() => {
                        if (!this.authService.isUserRemembered()) {
                            this.modal
                                .showConfirm('You are about to logout, do you want to leave?')
                                .then((is: boolean) => {
                                    if (is) {
                                        this.zone.run(() => {
                                            this.authService.logout();
                                            this.router.navigateByUrl('/');
                                        });
                                    }
                                });
                        }
                    }, after);
                });

            });
    }

    track(): void {
        this.tracker.next();
    }

}
