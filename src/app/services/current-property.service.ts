import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { VenueModel } from '../models/venue-model';
import { VenueResourceService } from './resources/venue-resource.service';
import { ResourceQueryResponse } from '../interfaces/resource-query-response';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/share';
import { Subscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';
import { AppState } from '../store/store';
import { SetCurrentVenue } from '../store/current-venue/current-venue.actions';
import { AuthService } from './auth.service';

@Injectable()
export class CurrentPropertyService {

    private venue: BehaviorSubject<VenueModel>;
    private subscriberHasVenue: BehaviorSubject<boolean>;
    private currentVenueSnapshot: VenueModel;

    private venuesList: BehaviorSubject<VenueModel[]>;

    get currentVenue(): Observable<VenueModel> {
        return this.venue.share();
    }

    get currentSubscriberHasVenue(): Observable<boolean> {
        return this.subscriberHasVenue.share();
    }

    get currentVenuesList(): Observable<VenueModel[]> {
        return this.venuesList;
    }

    get snapshot(): VenueModel {
        return this.currentVenueSnapshot;
    }

    constructor(
        private venueResourceService: VenueResourceService,
        private store: Store<AppState>,
        private authService: AuthService
    ) {
        this.venue = new BehaviorSubject<VenueModel>(new VenueModel());
        this.subscriberHasVenue = new BehaviorSubject(false);
        this.venuesList = new BehaviorSubject<VenueModel[]>([]);

        this.venue.skip(1).subscribe((venue: VenueModel) => this.currentVenueSnapshot = venue);
    }

    load() {
        const subscriberId = this.authService.currentUserSnapshot.subscriberId;

        this.venueResourceService.query({
            page: 0,
            size: 9999,
            sortFields: [],
            sortAsc: true
        }).then((resp: ResourceQueryResponse<VenueModel>) => {
            const items = resp.items.filter(x => +x.subscriberId == +subscriberId);

            this.subscriberHasVenue.next(items.length > 0);
            this.venuesList.next(items);
            if (items.length > 0) {
                this.select(items[0]);
            }
        });
    }

    select(venue: VenueModel) {
        this.venue.next(venue);
        this.store.dispatch(new SetCurrentVenue(venue));
    }

    currentVenueAsync(): Promise<VenueModel> {
        return new Promise((resolve, reject) => {
            let subs: Subscription;

            subs = this.currentVenue
                .filter((venue: VenueModel) => venue.id ? true : false)
                .subscribe((venue: VenueModel) => {
                    resolve(venue);
                    subs.unsubscribe();
                }, () => {
                    reject();
                    subs.unsubscribe();
                });

        });
    }

    currentVenueListAsync(): Promise<VenueModel[]> {
        return new Promise((resolve, reject) => {
            let subs: Subscription;

            subs = this.currentVenuesList
                .filter(venues => venues.length > 0)
                .subscribe((venues: VenueModel[]) => {
                    resolve(venues);
                    subs.unsubscribe();
                }, () => {
                    reject();
                    subs.unsubscribe();
                });

        });
    }

}
