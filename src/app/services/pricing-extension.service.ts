import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PricingExtensionService {

    constructor(
        private config: ConfigService,
        private http: HttpClient
    ) { }

    getByVenueId(venueId: number) {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.config.config().API_URL}venues/${venueId}/pricing-extension`)
                .subscribe(
                    response => resolve(response),
                    error => reject(error)
                );
        });
    }

    updateByVenueId(extension, venueId: number) {
        return new Promise((resolve, reject) => {
            this.http.put(`${this.config.config().API_URL}venues/${venueId}/pricing-extension`, extension)
                .subscribe(
                    response => resolve(response),
                    error => reject(error)
                );
        });
    }

}
