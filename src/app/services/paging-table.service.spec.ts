import { fakeAsync, TestBed, tick } from '@angular/core/testing';

import {
    PagingTableEvent, PagingTableEventType, PagingTableItemDeletedPayload, PagingTableItemsPerPageChangedPayload,
    PagingTablePageChangedPayload,
    PagingTablePageLoadedPayload, PagingTableSearchChangedPayload,
    PagingTableService, PagingTableSortColumnChangedPayload
} from './paging-table.service';

describe('PagingTableService', () => {

    let pagingTableService: PagingTableService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [PagingTableService]
        });
        pagingTableService = TestBed.get(PagingTableService);
    });

    it('itemsCount method should return the list of numbers, asynchronously', fakeAsync(() => {
        let counts;
        pagingTableService.itemsCount().then(items => {
            counts = items;
        });
        tick();
        expect(counts).toEqual([10, 50, 100]);
    }));

    it('should send and receive paging table related events with given payload', fakeAsync(() => {
        let pagingTableEvent = null;
        let response = null;

        pagingTableService.on(PagingTableEventType.PAGE_CHANGED).subscribe((event: PagingTableEvent) => {
            response = event;
        });

        pagingTableService.on(PagingTableEventType.ITEMS_PER_PAGE_CHANGED).subscribe((event: PagingTableEvent) => {
            response = event;
        });

        pagingTableEvent = new PagingTableEvent(PagingTableEventType.PAGE_CHANGED, {page: 1})
        pagingTableService.push(pagingTableEvent);
        tick();
        expect(response).toEqual(pagingTableEvent);

        pagingTableEvent = new PagingTableEvent(PagingTableEventType.ITEMS_PER_PAGE_CHANGED, {items: 50});
        pagingTableService.push(pagingTableEvent);
        tick();
        expect(response).toEqual(pagingTableEvent);
    }));

    it('should provide last event result, even if subscriber subscribed later', fakeAsync(() => {
        const pagingTableEvent = new PagingTableEvent(PagingTableEventType.PAGE_CHANGED, {page: 1});
        let res;
        pagingTableService.push(pagingTableEvent);
        tick();

        pagingTableService.on(PagingTableEventType.PAGE_CHANGED).subscribe((event) => {
            res = event;
        });
        tick();
        expect(res).toEqual(pagingTableEvent);
    }));

    it('firePageChanged should fire PAGE_CHANGED event with given page number', () => {
        let res;
        pagingTableService.on(PagingTableEventType.PAGE_CHANGED).subscribe(event => res = event.payload);
        pagingTableService.firePageChanged(3);
        expect(new PagingTablePageChangedPayload(3)).toEqual(res);
    });

    it('fireItemsPerPageChanged should fire ITEMS_PER_PAGE_CHANGED event with given number', () => {
        let res;
        pagingTableService.on(PagingTableEventType.ITEMS_PER_PAGE_CHANGED).subscribe(event => res = event.payload);
        pagingTableService.fireItemsPerPageChanged(25);
        expect(new PagingTableItemsPerPageChangedPayload(25)).toEqual(res);
    });

    it('firePageLoaded should fire ITEMS_PER_PAGE_CHANGED event with given values', () => {
        let res;
        pagingTableService.on(PagingTableEventType.PAGE_LOADED).subscribe(event => res = event.payload);
        pagingTableService.firePageLoaded(25, []);
        expect(new PagingTablePageLoadedPayload(25, [])).toEqual(res);
    });

    it('fireSearchChanged should fire SEARCH_CHANGED event with given values', () => {
        let res;
        pagingTableService.on(PagingTableEventType.SEARCH_CHANGED).subscribe(event => res = event.payload);
        pagingTableService.fireSearchChanged('123');
        expect(new PagingTableSearchChangedPayload('123')).toEqual(res);
    });

    it('fireSortColumnChanged should fire SORT_COLUMN_CHANGED event with given values', () => {
        let res;
        pagingTableService.on(PagingTableEventType.SORT_COLUMN_CHANGED).subscribe(event => res = event.payload);
        pagingTableService.fireSortColumnChanged('col', true);
        expect(new PagingTableSortColumnChangedPayload('col', true)).toEqual(res);
    });

    it('fireItemDeleted should fire ITEM_DELETED with given payload', () => {
        let res;
        pagingTableService.on(PagingTableEventType.ITEM_DELETED).subscribe(event => res = event.payload);
        pagingTableService.fireItemDeleted({item: 'item'});
        expect(new PagingTableItemDeletedPayload({item: 'item'})).toEqual(res);
    });

});
