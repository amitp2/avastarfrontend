import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../store/store';
import {VenueModel} from '../models/venue-model';
import {UtilsService} from './utils.service';
import { VenueClientModel } from '../models/venue-client-model';
import { VenueEventModel } from '../models/venue-event-model';

@Injectable()
export class PromisedStoreService {

    constructor(private store: Store<AppState>,
                private utils: UtilsService) {
    }

    currentVenue(): Promise<VenueModel> {
        return this.utils
            .toPromise(this.store.select('currentVenue').filter((v: VenueModel) => v.id ? true : false));
    }

    currentVenueId(): Promise<number> {
        return this.currentVenue().then((venue: VenueModel) => venue.id);
    }

    allAccounts(): Promise<VenueClientModel[]> {
        return this.utils
            .toPromise(this.store.select('allAccounts'));
    }

    allEvents(): Promise<VenueEventModel[]> {
        return this.utils.toPromise(this.store.select('allEvents'));
    }

}
