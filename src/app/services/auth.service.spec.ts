import { TestBed, inject, async, fakeAsync, tick, discardPeriodicTasks } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { ConfigService } from './config.service';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AppError } from '../enums/app-error.enum';
import { UserRole } from '../enums/user-role.enum';
import { UtilsService } from './utils.service';
import { InMemoryCacheService } from './caches/in-memory-cache.service';
import { AppCache } from './../interfaces/cache';

describe('AuthService', () => {

    let config: ConfigService;
    let authService: AuthService;
    let httpMock: HttpTestingController;
    let httpClient: HttpClient;
    let cache: AppCache;

    const email = 'gio@gio.com';
    const password = 'password';
    const testToken = 'tkn';
    const testRefreshToken = 'refreshToken';

    let refreshTokenUrl = '';
    let loginUrl = ``;
    let checkAuthorizedUrl = '';
    let tokenLifetimeMilliseconds = 0;
    let resetPasswordUrl = '';
    let verifyPasswordUrl = '';
    let currentUserUrl = '';

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
                HttpClientModule
            ],
            providers: [
                AuthService,
                ConfigService,
                UtilsService,
                {
                    provide: AppCache,
                    useClass: InMemoryCacheService
                }
            ]
        });

        config = TestBed.get(ConfigService);
        authService = TestBed.get(AuthService);
        httpMock = TestBed.get(HttpTestingController);
        httpClient = TestBed.get(HttpClient);
        cache = TestBed.get(AppCache);
        loginUrl = `${config.config().API_URL}auth/login`;
        checkAuthorizedUrl = `${config.config().API_URL}me`;
        refreshTokenUrl = `${config.config().API_URL}auth/token`;
        resetPasswordUrl = `${config.config().API_URL}users/reset_password`;
        verifyPasswordUrl = `${config.config().API_URL}users/verify_email`;
        currentUserUrl = `${config.config().API_URL}me`;
        window.localStorage.removeItem(config.config().TOKEN_NAME);
        tokenLifetimeMilliseconds = config.config().TOKEN_LIFETIME_MINUTES * 60 * 1000;
    });

    afterEach(fakeAsync(() => {
        cache.remove('current_user');
        tick();
    }));

    it('login method should call necessary api correctly to verify credentials', () => {
        authService.login(email, password);
        const req: TestRequest = httpMock.expectOne(loginUrl);
        expect(req.request.method).toEqual('POST');
        expect(req.request.body.username).toEqual(email);
        expect(req.request.body.password).toEqual(password);
    });

    it('login method should resolve to true, upon successful attempt', () => {
        authService.login(email, password).then((ok: boolean) => {
            expect(ok).toBeTruthy();
        });
        httpMock.expectOne(loginUrl).flush({});
    });

    xit('login method should reject to proper error', fakeAsync(() => {
        let error: AppError;

        authService.login(email, password).catch((err) => error = err);
        httpMock.expectOne(loginUrl).flush({errorCode: 2001}, {status: 401, statusText: 'error'});
        tick();
        expect(error).toEqual(AppError.USER_LOGIN_FAILED);

        authService.login(email, password).catch((err) => error = err);
        httpMock.expectOne(loginUrl).flush({errorCode: 1002}, {status: 401, statusText: 'error'});
        tick();
        expect(error).toEqual(AppError.USER_LOGIN_FAILED);

        authService.login(email, password).catch((err) => error = err);
        httpMock.expectOne(loginUrl).flush({errorCode: 2002}, {status: 401, statusText: 'error'});
        tick();
        expect(error).toEqual(AppError.TOO_MANY_LOGIN_FAILS);
    }));

    it('login method should store if user wants to be remembered in local storage in REMEMBER_ME_PROPERTY_NAME', () => {
        authService.login(email, password, true);
        httpMock.expectOne(loginUrl).flush({token: testToken, refreshToken: testRefreshToken});
        expect(localStorage.getItem(config.config().REMEMBER_ME_PROPERTY_NAME)).toEqual('true');

        authService.login(email, password, false);
        httpMock.expectOne(loginUrl).flush({token: testToken, refreshToken: testRefreshToken});
        expect(localStorage.getItem(config.config().REMEMBER_ME_PROPERTY_NAME)).toEqual('false');

        authService.login(email, password, true);
        httpMock.expectOne(loginUrl).flush({token: testToken, refreshToken: testRefreshToken});
        expect(localStorage.getItem(config.config().REMEMBER_ME_PROPERTY_NAME)).toEqual('true');
    });

    it('login method should store token/refreshToken to TOKEN_NAME/REFRESH_TOKEN_NAME property in localStorage, upon success', () => {
        authService.login(email, password);
        httpMock.expectOne(loginUrl).flush({token: testToken, refreshToken: testRefreshToken});
        expect(localStorage.getItem(config.config().TOKEN_NAME)).toEqual(testToken);
        expect(localStorage.getItem(config.config().REFRESH_TOKEN_NAME)).toEqual(testRefreshToken);
    });


    it('login method should run refreshToken method once in every TOKEN_LIFETIME_MINUTES, after successful login', fakeAsync(() => {
        authService.login(email, password);
        const refreshTokenSpy = spyOn(authService, 'refreshToken').and.callThrough();
        httpMock.expectOne(loginUrl).flush({});

        tick(tokenLifetimeMilliseconds - 1);
        expect(refreshTokenSpy.calls.all().length).toEqual(0);

        tick(2);
        expect(refreshTokenSpy.calls.all().length).toEqual(1);

        tick(tokenLifetimeMilliseconds + 1);
        expect(refreshTokenSpy.calls.all().length).toEqual(2);

        tick(tokenLifetimeMilliseconds + 1);
        expect(refreshTokenSpy.calls.all().length).toEqual(3);

        discardPeriodicTasks();
    }));


    it('isUserAuthorized method should check validity of current token, by calling necessary api properly', () => {
        window.localStorage.setItem(config.config().TOKEN_NAME, testToken);
        authService.isUserAuthorized();
        const req: TestRequest = httpMock.expectOne(checkAuthorizedUrl);
        expect(req.request.method).toEqual('GET');
    });

    it('isUserRemembered should return true if in localstorage[REMEMBER_ME_PROPERTY_NAME] equals "true", false otherwise', () => {
        window.localStorage.setItem(config.config().REMEMBER_ME_PROPERTY_NAME, 'true');
        expect(authService.isUserRemembered()).toEqual(true);

        window.localStorage.setItem(config.config().REMEMBER_ME_PROPERTY_NAME, 'false');
        expect(authService.isUserRemembered()).toEqual(false);
    });

    it('isUserAuthorized should resolve to false, if token doesn\'t exist', () => {
        authService.isUserAuthorized().then((is: boolean) => {
            expect(is).toEqual(false);
        });
    });

    it('logout should remove the tokens from local storage and also current_user_token', fakeAsync(() => {
        cache.set(config.config().CURRENT_USER_CACHE_KEY, 'cu');
        window.localStorage.setItem(config.config().TOKEN_NAME, testToken);
        window.localStorage.setItem(config.config().REFRESH_TOKEN_NAME, testRefreshToken);
        window.localStorage.setItem(config.config().REMEMBER_ME_PROPERTY_NAME, 'true');
        authService.logout().then(() => {
        });
        tick();
        let res;
        cache.hasKey(config.config().CURRENT_USER_CACHE_KEY).then(has => res = has);
        tick();
        expect(res).toEqual(false);
        expect(window.localStorage.getItem(config.config().TOKEN_NAME)).toBeFalsy();
        expect(window.localStorage.getItem(config.config().REFRESH_TOKEN_NAME)).toBeFalsy();
        expect(window.localStorage.getItem(config.config().REMEMBER_ME_PROPERTY_NAME)).toBeFalsy();
    }));


    it('refreshToken should call refreshTokenUrl with necessary params', () => {
        window.localStorage.setItem(config.config().TOKEN_NAME, testToken);
        window.localStorage.setItem(config.config().REFRESH_TOKEN_NAME, testRefreshToken);
        authService.refreshToken().then(() => {
        });

        const req: TestRequest = httpMock.expectOne(refreshTokenUrl);
        expect(req.request.method).toEqual('GET');
        expect(req.request.headers.get('Authorization')).toEqual(`Bearer ${testRefreshToken}`);
    });

    it('refreshToken should update local storage with new token if it was successful, and resolve promise', fakeAsync(() => {
        window.localStorage.setItem(config.config().TOKEN_NAME, testToken);
        let resolved = false;
        authService.refreshToken().then(() => {
            resolved = true;
        });
        httpMock.expectOne(refreshTokenUrl).flush({token: 'some other token'});
        tick();
        expect(window.localStorage.getItem(config.config().TOKEN_NAME)).toEqual('some other token');
        expect(resolved).toEqual(true);
    }));

    it('refreshToken should reject if refresh was not successful', fakeAsync(() => {
        let rejected = false;
        authService.refreshToken().catch(() => {
            rejected = true;
        });
        httpMock.expectOne(refreshTokenUrl).error(null);
        tick();
        expect(rejected).toEqual(true);
    }));

    it('isUserAuthorized should call refreshToken method if checkAuthorizedUrl fails', () => {
        window.localStorage.setItem(config.config().TOKEN_NAME, testToken);

        const refreshTokenSpy = spyOn(authService, 'refreshToken').and.returnValue(Promise.resolve(true));

        authService.isUserAuthorized().then(_ => ({}));

        httpMock.expectOne(checkAuthorizedUrl).error(null);

        expect(refreshTokenSpy.calls.all().length).toEqual(1);
    });

    it('isUserAuthorized should resolve to false if refreshToken method rejects when necessary', fakeAsync(() => {
        let is: boolean;
        window.localStorage.setItem(config.config().TOKEN_NAME, testToken);

        const refreshTokenSpy = spyOn(authService, 'refreshToken').and.returnValue(Promise.reject(false));

        authService.isUserAuthorized().then((yes: boolean) => {
            is = yes;
        });
        httpMock.expectOne(checkAuthorizedUrl).error(null);
        tick();
        expect(refreshTokenSpy.calls.all().length).toEqual(1);
        expect(is).toEqual(false);
    }));

    it('isUserAuthorized should resolve to true if refreshToken method resolves when necessary', fakeAsync(() => {
        let is: boolean;
        window.localStorage.setItem(config.config().TOKEN_NAME, testToken);
        const refreshTokenSpy = spyOn(authService, 'refreshToken').and.returnValue(Promise.resolve(true));
        authService.isUserAuthorized().then((yes: boolean) => {
            is = yes;
        });
        httpMock.expectOne(checkAuthorizedUrl).error(null);
        tick();
        expect(refreshTokenSpy.calls.all().length).toEqual(1);
        expect(is).toEqual(true);
    }));


    it('isUserAuthorized should resolve to false, if request fails', () => {
        window.localStorage.setItem(config.config().TOKEN_NAME, testToken);
        authService.isUserAuthorized().then((is: boolean) => {
            expect(is).toEqual(false);
        });
        httpMock.expectOne(checkAuthorizedUrl).error(null);
    });

    it('isUserAuthorized should resolve to true, if request succeeds', () => {
        window.localStorage.setItem(config.config().TOKEN_NAME, testToken);
        authService.isUserAuthorized().then((is: boolean) => {
            expect(is).toEqual(true);
        });
        httpMock.expectOne(checkAuthorizedUrl).flush(null);
    });


    it('forgotPassword should resolve if reset password url returns ok, reject otherwise', fakeAsync(() => {
        let resolved = false;
        let rejected = false;
        let rejectedError = null;

        authService.forgotPassword(email).then(() => {
            resolved = true;
        });
        httpMock.expectOne(`${config.config().API_URL}users/${email}/reset_password`).flush(null);
        tick();
        expect(resolved).toEqual(true);

        authService.forgotPassword(email).catch((error) => {
            rejected = true;
            rejectedError = error;
        });
        httpMock.expectOne(`${config.config().API_URL}users/${email}/reset_password`).error(null);
        tick();
        expect(rejected).toEqual(true);
        expect(rejectedError).toEqual(AppError.FORGOT_PASSWORD_FAILED);
    }));


    it('resetPassword should call necessary API with given params', () => {
        authService.resetPassword(testToken, '123');
        const req = httpMock.expectOne(resetPasswordUrl);
        expect(req.request.method).toEqual('PUT');
        expect(req.request.body.token).toEqual(testToken);
        expect(req.request.body.password).toEqual('123');
    });

    it('resetPassword should resolve if request succeeds', fakeAsync(() => {
        let resolved = false;
        authService.resetPassword(testToken, '123').then(() => resolved = true);
        httpMock.expectOne(resetPasswordUrl).flush({});
        tick();
        expect(resolved).toEqual(true);
    }));

    it('verify should call necessary API with given params', () => {
        authService.verify(testToken, '123');
        const req = httpMock.expectOne(verifyPasswordUrl);
        expect(req.request.method).toEqual('PUT');
        expect(req.request.body.token).toEqual(testToken);
        expect(req.request.body.password).toEqual('123');
    });

    it('verify should resolve if request succeeds', fakeAsync(() => {
        let resolved = false;
        authService.verify(testToken, '123').then(() => resolved = true);
        httpMock.expectOne(verifyPasswordUrl).flush({});
        tick();
        expect(resolved).toEqual(true);
    }));

    it('resetPassword should reject if request fails', fakeAsync(() => {
        let rejected = false;
        let rejectedError = null;
        authService.resetPassword(testToken, '123').catch((error) => {
            rejected = true;
            rejectedError = error;
        });
        httpMock.expectOne(resetPasswordUrl).error(null);
        tick();
        expect(rejected).toEqual(true);
        expect(rejectedError).toEqual(AppError.RESET_PASSWORD_FAILED);
    }));

    it('currentUser should reject if request to currentUserUrl fails', fakeAsync(() => {
        let rejected = false;
        let rejectedError = null;
        authService.currentUser().catch((error) => {
            rejected = true;
            rejectedError = error;
        });
        tick();
        httpMock.expectOne(currentUserUrl).error(null);
        tick();
        expect(rejected).toEqual(true);
        expect(rejectedError).toEqual(AppError.CURRENT_USER_FAILED);
    }));

    it('currentUser should send proper request', fakeAsync(() => {
        authService.currentUser().then(() => ({}));
        tick();
        const req = httpMock.expectOne(currentUserUrl);
        expect(req.request.method).toEqual('GET');
    }));

    it('currentUser should resolve with proper current user data', fakeAsync(() => {
        const currentUser = {username: 'name', roles: ['ROLE_ADMIN', 'ROLE_SUPER_ADMIN']};
        let user = null;
        authService.currentUser().then(current => user = current);
        tick();
        httpMock.expectOne(currentUserUrl).flush(currentUser);
        tick();
        expect(user).toEqual({username: 'name', roles: [UserRole.ADMIN, UserRole.SUPER_ADMIN]});
    }));

    it('getToken should resolve to TOKEN_NAME value if token exists in localhost', () => {
        localStorage.setItem(config.config().TOKEN_NAME, testToken);
        expect(authService.getToken()).toEqual(testToken);
    });

    // caching behaviour
    it('currentUser should store retrieved user to current_user key in cache', fakeAsync(() => {
        const currentUser = {username: 'name', roles: ['ROLE_ADMIN', 'ROLE_SUPER_ADMIN']};
        let user = null;
        authService.currentUser();
        tick();
        httpMock.expectOne(currentUserUrl).flush(currentUser);
        cache.get('current_user').then(res => user = res);
        tick();
        expect(user).toEqual({username: 'name', roles: [UserRole.ADMIN, UserRole.SUPER_ADMIN]});
    }));

});
