import { Injectable } from '@angular/core';

declare var $: any;

export class Globals {
    public $: any;
}

@Injectable()
export class GlobalsService {

    constructor() {
    }

    globals(): Globals {
        return {
            $
        };
    }

}
