import { Injectable } from '@angular/core';
import { RevenueCategoryResourceService } from './resources/revenue-category-resource.service';
import { FunctionEquipment } from '../models/function-model';
import { AuthService } from './auth.service';
import { FunctionRevenueCategory } from '../enums/function-revenue-category.enum';
import { RevenueCategoryModel } from '../models/revenue-category-model';
import { UtilsService } from './utils.service';
import { Copyable } from '../helpers/copyable';

interface GroupedRevenueCats {
    [key: string]: RevenueCategoryModel;
}

interface GroupedFunctionEquipments {
    [key: string]: FunctionEquipment[];
}

export class FunctionReport implements Copyable<FunctionReport> {
    category: FunctionRevenueCategory;
    amount: number;


    copy(): FunctionReport {
        const copy = new FunctionReport();
        copy.amount = this.amount;
        copy.category = this.category;
        return copy;
    }

}

@Injectable()
export class FunctionRevenueCategoryCalculatorService {

    private codeMappings = {
        [FunctionRevenueCategory.Equipment]: ['E', 'X'],
        [FunctionRevenueCategory.ItEquipment]: ['N'],
        [FunctionRevenueCategory.InternetNetworkAccess]: ['I'],
        [FunctionRevenueCategory.PowerEquipment]: ['P'],
        [FunctionRevenueCategory.RiggingEquipment]: ['R'],
        [FunctionRevenueCategory.Labor]: ['L', 'B'],
        [FunctionRevenueCategory.ITLabor]: ['T', 'W'],
        [FunctionRevenueCategory.ElectricianLabor]: ['Z'],
        [FunctionRevenueCategory.RiggingLabor]: ['H'],
        [FunctionRevenueCategory.Operator]: ['O', 'A'],
        [FunctionRevenueCategory.SalesItems]: ['S'],
        [FunctionRevenueCategory.Discounts]: ['D'],
        [FunctionRevenueCategory.ComplimentaryItems]: ['C'],
        [FunctionRevenueCategory.ServiceCharge]: ['G']
    };

    private reportCats = [
        FunctionRevenueCategory.Equipment,
        FunctionRevenueCategory.ItEquipment,
        FunctionRevenueCategory.InternetNetworkAccess,
        FunctionRevenueCategory.PowerEquipment,
        FunctionRevenueCategory.RiggingEquipment,
        FunctionRevenueCategory.Labor,
        FunctionRevenueCategory.ITLabor,
        FunctionRevenueCategory.ElectricianLabor,
        FunctionRevenueCategory.RiggingLabor,
        FunctionRevenueCategory.Operator,
        FunctionRevenueCategory.SalesItems,
        FunctionRevenueCategory.ComplimentaryItems,
        // FunctionRevenueCategory.Discounts,
        // FunctionRevenueCategory.ServiceCharge,
        // FunctionRevenueCategory.Tax
    ];


    constructor(private revenueCategory: RevenueCategoryResourceService,
                private auth: AuthService,
                private utils: UtilsService) {
    }

    private async groupCatsByCode(): Promise<GroupedRevenueCats> {
        const user = await this.auth.currentUser();
        const subscriberId = user.subscriberId;
        const cats = await this.revenueCategory.getAllBySubscriberId(subscriberId);
        const res = {};
        cats.forEach(c => res[c.code] = c);
        return res;
    }

    private async groupEquipmentsByCode(equipments: FunctionEquipment[], cats: GroupedRevenueCats): Promise<GroupedFunctionEquipments> {
        const res = {};
        equipments.forEach(eq => {
            if (!cats[eq.code]) {
                return;
            }
            const calcCode = cats[eq.code].calculateCode;
            if (!res[calcCode]) {
                res[calcCode] = [];
            }
            res[calcCode].push(eq);
        });
        return res;
    }

    private calcForCode(eqs: GroupedFunctionEquipments) {
        return (code: string) => {
            if (!eqs[code]) {
                return 0;
            }
            const prices = eqs[code].map(eq => eq.priceTotalRaw);
            const res = this.utils.sumArray(prices);
            return res;
        };
    }

    private calcForCategory(eqs: GroupedFunctionEquipments) {
        return (cat: FunctionRevenueCategory) => {
            if (!this.codeMappings[cat]) {
                return 0;
            }
            const amounts = this.codeMappings[cat].map(this.calcForCode(eqs));
            const res = this.utils.sumArray(amounts);
            return res;
        };
    }

    private async buildReport(eqs: GroupedFunctionEquipments): Promise<FunctionReport[]> {
        const amounts = this.reportCats.map(c => {
            const report = new FunctionReport();
            report.category = c;
            report.amount = this.calcForCategory(eqs)(c);

            if (report.category === FunctionRevenueCategory.ComplimentaryItems) {
                report.amount = -report.amount;
            }

            return report;
        });
        const res = amounts.filter(a => a.amount !== 0);
        return res;
    }

    private async addDiscountsToReport(eqs: GroupedFunctionEquipments, equipments: FunctionEquipment[], report: FunctionReport[]): Promise<FunctionReport[]> {
        const newReport = this.utils.copyMany(report);
        const discountReport = new FunctionReport();
        discountReport.category = FunctionRevenueCategory.Discounts;
        discountReport.amount = this.utils.sumArray(equipments.map(eq => eq.appliedDiscountTotal));

        const forComplementary = await this.discountsForComplementaryItesm(eqs);
        discountReport.amount -= forComplementary;

        discountReport.amount = -discountReport.amount;
        newReport.push(discountReport);
        return newReport;
    }

    private async discountsForComplementaryItesm(eqs: GroupedFunctionEquipments) {
        if (!eqs['C']) {
            return 0;
        }
        return this.utils.sumArray(eqs['C'].map(eq => eq.appliedDiscountTotal));
    }

    private async addServiceChargeToReport(eqs: GroupedFunctionEquipments, equipments: FunctionEquipment[], report: FunctionReport[]): Promise<FunctionReport[]> {
        const newReport = this.utils.copyMany(report);
        const serviceChargeReport = new FunctionReport();
        serviceChargeReport.category = FunctionRevenueCategory.ServiceCharge;
        serviceChargeReport.amount = this.utils.sumArray(equipments.map(eq => eq.appliedServiceChargeTotal));

        const forComplementary = await this.serviceChargeForComplementaryItesm(eqs);
        serviceChargeReport.amount -= forComplementary;

        newReport.push(serviceChargeReport);
        return newReport;
    }

    private async serviceChargeForComplementaryItesm(eqs: GroupedFunctionEquipments) {
        if (!eqs['C']) {
            return 0;
        }
        return this.utils.sumArray(eqs['C'].map(eq => eq.appliedServiceChargeTotal));
    }

    private async addTaxReport(eqs: GroupedFunctionEquipments, equipments: FunctionEquipment[], report: FunctionReport[]): Promise<FunctionReport[]> {
        const newReport = this.utils.copyMany(report);
        const tax = new FunctionReport();
        tax.category = FunctionRevenueCategory.Tax;
        tax.amount = this.utils.sumArray(equipments.map(eq => eq.appliedTaxTotal));

        const forComplementary = await this.taxChargeForComplementaryItems(eqs);
        tax.amount -= forComplementary;

        newReport.push(tax);
        return newReport;
    }

    private async taxChargeForComplementaryItems(eqs: GroupedFunctionEquipments) {
        if (!eqs['C']) {
            return 0;
        }
        return this.utils.sumArray(eqs['C'].map(eq => eq.appliedTaxTotal));
    }

    private async addTotalReport(report: FunctionReport[]): Promise<FunctionReport[]> {
        const newReport = this.utils.copyMany(report);
        const tax = new FunctionReport();
        const filtered = report.filter(r => r.category !== FunctionRevenueCategory.ComplimentaryItems);
        tax.category = FunctionRevenueCategory.Total;
        tax.amount = this.utils.sumArray(filtered.map(eq => eq.amount));
        newReport.push(tax);
        return newReport;
    }

    async calculate(equipments: FunctionEquipment[]): Promise<FunctionReport[]> {
        const cats = await this.groupCatsByCode();
        const eqs = await this.groupEquipmentsByCode(equipments, cats);
        const report = await this.buildReport(eqs);
        const withDiscount = await this.addDiscountsToReport(eqs, equipments, report);
        const withServiceCharge = await this.addServiceChargeToReport(eqs, equipments, withDiscount);
        const withTax = await this.addTaxReport(eqs, equipments, withServiceCharge);
        const withTotal = await this.addTotalReport(withTax);
        return withTotal;
    }

}
