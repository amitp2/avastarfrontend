import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config.service';
import * as moment from 'moment';
import { UtilsService } from './utils.service';

export enum ReportAction {
    View,
    Print,
    Download
}

@Injectable()
export class ReportService {
    apiUrl: string;

    constructor(private http: HttpClient, private config: ConfigService, private utils: UtilsService) {
        this.apiUrl = config.config().API_URL;
    }

    private async report(action: ReportAction, html: string) {
        const htmlBlob = new Blob([html], {
            type: 'text/html'
        });
        const blobUrl = URL.createObjectURL(htmlBlob);

        if (action === ReportAction.Print) {
            const iframe = document.createElement('iframe');
            iframe.src = blobUrl;
            iframe.hidden = true;

            document.body.appendChild(iframe);

            await this.utils.resolveAfter(450);

            iframe.contentWindow.print();

            await this.utils.resolveAfter(450);

            document.body.removeChild(iframe);
        } else {
            const blobWindow = window.open(blobUrl, '_blank');
        }
    }

    private async reportAsBlob(action: ReportAction, blob: string) {
        const htmlBlob = new Blob([blob], {
            type: 'text/html'
        });
        const blobUrl = URL.createObjectURL(htmlBlob);

        if (action === ReportAction.Print) {
            const iframe = document.createElement('iframe');
            iframe.src = blobUrl;
            iframe.hidden = true;

            document.body.appendChild(iframe);

            await this.utils.resolveAfter(450);

            iframe.contentWindow.print();

            await this.utils.resolveAfter(450);

            document.body.removeChild(iframe);
        } else {
            const blobWindow = window.open(blobUrl, '_blank');
        }
    }

    private openBlob(blob: Blob) {
        const pdfBlog = new Blob([blob], {
            type: 'application/pdf'
        });
    }

    toParams(params = {}): { responseType: 'text' } {
        return <any>{
            responseType: 'text',
            params: {
                ...(params || {}),
                clientDate: moment().format('YYYY/MM/DD, HH:mm')
            }
        };
    }

    toBlobParams(params = {}): { responseType: 'text' } {
        return <any>{
            responseType: 'text',
            params: {
                ...(params || {}),
                clientDate: moment().format('YYYY/MM/DD, HH:mm')
            }
        };
    }

    inventoryReport(venueId: any, action: ReportAction) {
        return this.http
            .get(this.apiUrl + `venues/${venueId}/reports/inventory`, this.toBlobParams())
            .toPromise()
            .then(html => this.reportAsBlob(action, html));
    }

    // inventoryReport(venueId: any, params, action: ReportAction) {
        
    //     return this.http
    //         .get(this.apiUrl + `venues/${venueId}/reports/inventory`, this.toBlobParams(params))
    //         .toPromise()
    //         .then(html => this.reportAsBlob(action, html));
    // }

    eventSpacesReport(action: ReportAction, venueId: number) {
        return this.http
            .get(`${this.apiUrl}venues/${venueId}/reports/event-spaces`, this.toBlobParams())
            .toPromise()
            .then(html => this.reportAsBlob(action, html));
    }

    overdueMaintenanceReport(venueId: number, action: ReportAction) {
        return this.http
            .get(this.apiUrl + `venues/${venueId}/reports/overdue-maintenance`, this.toBlobParams())
            .toPromise()
            .then(html => this.reportAsBlob(action, html));
    }

    //Old method for scheduledMaintenanceReports
    // scheduledMaintenanceReport(venueId: number, action: ReportAction) {
    //     return this.http
    //         .get(this.apiUrl + `venues/${venueId}/reports/scheduled-maintenance`, this.toBlobParams())
    //         .toPromise()
    //         .then(html => this.reportAsBlob(action, html));
    // }

    //new method for scheduledMaintenanceReport
    scheduledMaintenanceReport(action: ReportAction, params: { fromDate: string; toDate: string }, id: number) {
        return this.http
            .get(this.apiUrl + `venues/${id}/reports/scheduled-maintenance`, this.toBlobParams(params))
            .toPromise()
            .then(html => this.reportAsBlob(action, html));
    }

    serviceMaintenanceReport(action: ReportAction, venueId: number, params: any) {
        return this.http
            .get(this.apiUrl + `venues/${venueId}/reports/service-maintenance`, this.toBlobParams(params))
            .toPromise()
            .then(html => this.reportAsBlob(action, html));
    }

    purchaseOrderReport(action: ReportAction, id: number) {
        return this.http
            .get(this.apiUrl + `venues/reports/purchase_order/${id}`, this.toBlobParams())
            .toPromise()
            .then(html => this.reportAsBlob(action, html));
    }

    invoiceReport(action: ReportAction, eventId: number) {
        return this.http
            .get(this.apiUrl + 'venues/reports/invoice/' + eventId, this.toBlobParams())
            .toPromise()
            .then(html => this.reportAsBlob(action, html));
    }

    laborReport(action: ReportAction, params: any, id: number) {
        return this.http
            .get(this.apiUrl + `venues/${id}/reports/labor/`, this.toBlobParams(params))
            .toPromise()
            .then(html => this.reportAsBlob(action, html));
    }

    accrualReport(action: ReportAction, params: any, id: number) {
        return this.http
            .get(this.apiUrl + `venues/${id}/reports/accrual/`, this.toBlobParams(params))
            .toPromise()
            .then(html => this.reportAsBlob(action, html));
    }

    usageReport(action: ReportAction, params: any, id: number) {
        return this.http
            .get(this.apiUrl + `venues/${id}/reports/usage/`, this.toBlobParams(params))
            .toPromise()
            .then(html => this.reportAsBlob(action, html));
    }

    dailyReport(action: ReportAction, params, id: number) {
        return this.http
            .get(this.apiUrl + `venues/${id}/reports/daily`, this.toBlobParams(params))
            .toPromise()
            .then(html => this.reportAsBlob(action, html));
    }

    async upcomingRenewal(action: ReportAction, from: any, to: any) {
        const url = `${this.apiUrl}subscribers/reports/good-standing?from=${from}&to=${to}`;
        const html = await this.utils.toPromise(this.http.get(url, this.toParams()));
        this.report(action, html);
    }

    async unpaidAccounts(action: ReportAction, from: any, to: any) {
        const url = `${this.apiUrl}subscribers/reports/non-good-standing?from=${from}&to=${to}`;
        const html = await this.utils.toPromise(this.http.get(url, this.toParams()));
        this.report(action, html);
    }

    workflowReport(action: ReportAction, params: { fromDate: string; toDate: string }, id: number) {
        return this.http
            .get(this.apiUrl + `venues/${id}/reports/workflow/`, this.toBlobParams(params))
            .toPromise()
            .then(html => this.reportAsBlob(action, html));
    }
}
