import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { InventoryModel } from '../../models/inventory-model';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { UtilsService } from '../utils.service';
import { ObjectTransformerType } from '../../enums/object-transformer-type.enum';
import { InventoryAssetOwnerType } from '../../enums/inventory-asset-owner-type.enum';
import { InventoryStorageType } from '../../enums/inventory-storage-type.enum';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';
import { ManufacturerResourceService } from './manufacturer-resource.service';
import { ManufacturerModel } from '../../models/manufacturer-model';
import { InventoryAssetStatus } from '../../enums/inventory-asset-status.enum';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/store';
import { InventoryPagingLastParams, InventoryPagingNext, InventoryPagingPrev } from '../../store/inventory-paging/inventory-paging.actions';
import { CurrentPropertyService } from '../current-property.service';
import { InventoryPagingState } from '../../store/inventory-paging/inventory-paging.reducers';

export const serializeStorageType = {};
serializeStorageType[InventoryStorageType.StorageSpace] = 1;
serializeStorageType[InventoryStorageType.EventSpace] = 2;

export const deserializeStorageType = {};
deserializeStorageType[1] = InventoryStorageType.StorageSpace;
deserializeStorageType[2] = InventoryStorageType.EventSpace;


export const serializeAssetOwnerType = {};
serializeAssetOwnerType[InventoryAssetOwnerType.Owner] = 1;
serializeAssetOwnerType[InventoryAssetOwnerType.Installed] = 2;
serializeAssetOwnerType[InventoryAssetOwnerType.Vendor] = 3;

export const deserializeAssetOwnerType = {};
deserializeAssetOwnerType[1] = InventoryAssetOwnerType.Owner;
deserializeAssetOwnerType[2] = InventoryAssetOwnerType.Installed;
deserializeAssetOwnerType[3] = InventoryAssetOwnerType.Vendor;

export const inventoryResourceTransformer = {
    id: {
        to: 'id'
    },
    name: {
        to: 'name'
    },
    subCategoryId: {
        to: 'subCategoryId'
    },
    categoryId: {
        to: 'categoryId'
    },
    manufacturerId: {
        to: 'manufacturerId'
    },
    model: {
        to: 'model'
    },
    serialNumber: {
        to: 'serialNumber'
    },
    tagNumber: {
        to: 'tagNumber'
    },
    barcode: {
        to: 'barcode'
    },
    purchaseDate: {
        to: 'purchaseDate'
    },
    purchasePrice: {
        to: 'purchasePrice'
    },
    vendorId: {
        to: 'vendorId'
    },
    portable: {
        to: 'portable'
    },
    storageType: {
        to: 'storageType',
        type: ObjectTransformerType.Function,
        transform: (val) => val ? deserializeStorageType[+val] : deserializeStorageType[1]
    },
    storageId: {
        to: 'storageId'
    },
    assetOwnerType: {
        to: 'assetOwnerType',
        type: ObjectTransformerType.Function,
        transform: (val) => val ? deserializeAssetOwnerType[+val] : deserializeAssetOwnerType[1]
    },
    assetOwnerId: {
        to: 'assetOwnerId'
    },
    pictureUrl: {
        to: 'pictureUrl'
    },
    subCategoryItemId: { to: 'subCategoryItemId' }
};

@Injectable()
export class InventoryResourceService extends HttpResource<InventoryModel> {

    private readonly conditionDeSerializer = {};

    constructor(
        httpClient: HttpClient,
        private configService: ConfigService,
        private utilsService: UtilsService,
        private manufacturer: ManufacturerResourceService,
        private store: Store<AppState>,
        private venueApi: CurrentPropertyService
    ) {
        super(httpClient);

        this.conditionDeSerializer['ACTIVE'] = InventoryAssetStatus.Active;
        this.conditionDeSerializer['AT_REPAIR'] = InventoryAssetStatus.AtRepair;
        this.conditionDeSerializer['BROKEN'] = InventoryAssetStatus.Broken;
        this.conditionDeSerializer['MISSING'] = InventoryAssetStatus.Missing;
        this.conditionDeSerializer['REMOVED'] = InventoryAssetStatus.Removed;
        this.conditionDeSerializer['SERVICE_REQUIRED'] = InventoryAssetStatus.ServiceRequired;
    }

    deserialize(data: any): InventoryModel {
        const transformer = {
            ...inventoryResourceTransformer,
            subCategoryItemName: { to: 'subCategoryItemName' }
        };
        transformer.assetOwnerType = {
            to: 'assetOwnerType',
            type: ObjectTransformerType.Function,
            transform: (val) => val ? deserializeAssetOwnerType[+val] : null
        };
        const model = new InventoryModel(this.utilsService.transformInto(data, transformer));

        if (data.activeCondition && data.activeCondition.condition) {
            model.condition = this.conditionDeSerializer[data.activeCondition.condition];
        }

        return model;
    }

    serialize(data: InventoryModel): any {
        const transform = {
            ...inventoryResourceTransformer,
            storageType: {
                to: 'storageType',
                type: ObjectTransformerType.Function,
                transform: (val) => serializeStorageType[val]
            },
            assetOwnerType: {
                to: 'assetOwnerType',
                type: ObjectTransformerType.Function,
                transform: (val) => serializeAssetOwnerType[val]
            }
        };
        delete transform.categoryId;
        delete transform.id;
        delete transform.subCategoryId;

        return this.utilsService.transformInto(data, transform);
    }

    resourceUrl(): string {
        return `${this.configService.config().API_URL}venues/inventories`;
    }

    getManyByVenueId(ids: number[], venueId: number): Promise<InventoryModel[]> {
        const params = this.getAllQueryObject();
        params.otherParams = { ids };
        return this.queryByUrl(params, `${this.configService.config().API_URL}venues/${venueId}/inventories`)
            .then(resp => resp.items);
    }

    addForProperty(model: InventoryModel, venueId: number): Promise<InventoryModel> {
        return this.addByUrl(model, `${this.configService.config().API_URL}venues/${venueId}/inventories`);
    }

    queryByVenueId(params: HttpResourceQueryParams, venueId: number): Promise<ResourceQueryResponse<InventoryModel>> {
        return this.queryByUrl(params, `${this.configService.config().API_URL}venues/${venueId}/inventories`);
    }

    getAllByCategoryId(categoryId: number, venueId: number): Promise<InventoryModel[]> {
        const params = this.getAllQueryObject();
        params.otherParams = { categoryId };
        return this.queryByUrl(params, `${this.configService.config().API_URL}venues/${venueId}/inventories`)
            .then((resp: ResourceQueryResponse<InventoryModel>) => resp.items);
    }

    getAllBySubCategoryId(subCategoryId: number, venueId: number): Promise<InventoryModel[]> {
        const params = this.getAllQueryObject();
        params.otherParams = { subCategoryId };
        return this.queryByUrl(params, `${this.configService.config().API_URL}venues/${venueId}/inventories`)
            .then((resp: ResourceQueryResponse<InventoryModel>) => resp.items);
    }

    getAllByVenueId(venueId: number): Promise<InventoryModel[]> {
        return this.queryByUrl(this.getAllQueryObject(), `${this.configService.config().API_URL}venues/${venueId}/inventories`)
            .then((resp: ResourceQueryResponse<InventoryModel>) => resp.items);
    }

    queryByVenueIdWithManufacturerNames(params: HttpResourceQueryParams, venueId: number): Promise<ResourceQueryResponse<InventoryModel>> {
        this.store.dispatch(new InventoryPagingLastParams(params));

        return this.queryByUrl(params, `${this.configService.config().API_URL}venues/${venueId}/inventories`)
            .then((res) => {

                return this.manufacturer.getMany(res.items.map((inv: InventoryModel) => inv.manufacturerId))
                    .then((mans) => {
                        const mansIds = {};
                        mans.forEach((man: ManufacturerModel) => mansIds[man.id] = man.name);
                        res.items = res.items.map((inv: InventoryModel) => {
                            inv.manufacturerName = mansIds[inv.manufacturerId];
                            return inv;
                        });
                        return res;
                    });
            });
    }

    async getNextForPaging() {
        const inventoryPaging: InventoryPagingState = await this.utilsService.toPromise(this.store.select(x => x.inventoryPaging));

        if (!inventoryPaging.current) {
            this.store.dispatch(new InventoryPagingNext(null));
            return null;
        }

        const params = {
            ...inventoryPaging.lastParams,
            size: 1,
            page: inventoryPaging.current.page + 1
        };
        const venue = await this.venueApi.currentVenueAsync();
        const result = await this.queryByUrl(params, `${this.configService.config().API_URL}venues/${venue.id}/inventories`);

        let next = null;
        if (result.items.length) {
            next = {
                id: result.items[0].id,
                page: inventoryPaging.current.page + 1
            };
        }
        this.store.dispatch(new InventoryPagingNext(next));

        return next;
    }

    async getPrevForPaging() {
        const inventoryPaging: InventoryPagingState = await this.utilsService.toPromise(this.store.select(x => x.inventoryPaging));

        if (!inventoryPaging.current || inventoryPaging.current.page === 0) {
            this.store.dispatch(new InventoryPagingPrev(null));
            return null;
        }

        const params = {
            ...inventoryPaging.lastParams,
            size: 1,
            page: inventoryPaging.current.page - 1
        };
        const venue = await this.venueApi.currentVenueAsync();
        const result = await this.queryByUrl(params, `${this.configService.config().API_URL}venues/${venue.id}/inventories`);

        let prev = null;
        if (result.items.length) {
            prev = {
                id: result.items[0].id,
                page: inventoryPaging.current.page - 1
            };
        }
        this.store.dispatch(new InventoryPagingPrev(prev));

        return prev;
    }

    getManyWithManufacturerNamesByVenueId(ids: number[], venueId: number): Promise<InventoryModel[]> {
        const params = this.getAllQueryObject();
        params.otherParams = { ids };
        return this.queryByVenueIdWithManufacturerNames(params, venueId).then(resp => resp.items);
    }

    async clone(inventoryId: number): Promise<InventoryModel> {
        const dummyModel = new InventoryModel(null);
        const cloned = await this.addByUrl(dummyModel, `${this.configService.config().API_URL}venues/inventories/${inventoryId}/clone`);
        return cloned;
    }

    deleteMany(ids: number[]) {
        return this.httpClient.post(`${this.resourceUrl()}/delete`, { ids });
    }
}
