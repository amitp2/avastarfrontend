import { Injectable } from '@angular/core';
import { HttpCachableResource } from '../../classes/http-cachable-resource';
import { TicketModel, TicketStatus, TicketAssignee } from '../../models/ticket-model';
import { HttpClient } from '@angular/common/http';
import { AppCache } from '../../interfaces/cache';
import { UtilsService } from '../utils.service';
import { ConfigService } from '../config.service';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';
import { TicketCommentModel } from '../../models/ticket-comment-model';
import { ObjectTransformerType } from '../../enums/object-transformer-type.enum';

// OPEN(1, "OPEN"),
//     IN_PROGRESS(2, "IN_PROGRESS"),
//     COMPLETED(3, "COMPLETED");

const deserializeTicketStatus = {
    'OPEN': TicketStatus.Open,
    'IN_PROGRESS': TicketStatus.InProgress,
    'COMPLETED': TicketStatus.Completed
};

const serializeTicketStatus = {
    [TicketStatus.Open]: 'OPEN',
    [TicketStatus.InProgress]: 'IN_PROGRESS',
    [TicketStatus.Completed]: 'COMPLETED'
};

@Injectable()
export class TicketResourceService extends HttpCachableResource<TicketModel> {

    constructor(
        client: HttpClient,
        cache: AppCache,
        private utils: UtilsService,
        private config: ConfigService
    ) {
        super(client, cache);
    }

    get apiUrl(): string {
        return this.config.config().API_URL;
    }

    private urlByVenueId(venueId: number) {
        
        return `${this.apiUrl}venues/${venueId}/tickets`;
    }

    deserialize(data: any): TicketModel {
        
        const model = new TicketModel(this.utils.transformInto(data, {
            id: { to: 'id' },
            title: { to: 'title' },
            description: { to: 'description' },
            reminderTime: { to: 'reminderTime' },
            inventory: { to: 'inventory' },
            assigneeType: { to: 'assigneeType' },
            system: { to: 'system' },
            ticketStatus: {
                to: 'ticketStatus',
                type: ObjectTransformerType.Function,
                transform: (val) => deserializeTicketStatus[val]
            }
        }));

        model.extras = {};

        if (data.inventory) {
            model.inventoryId = data.inventory.id;
            model.extras.inventory = {
                ...data.inventory
            };
        }

        if (data.comments) {
            model.extras.comments = data.comments;
        }

        if (data.assigneeType === TicketAssignee.Vendor) {
            model.vendorContactId = data.assignee;
            if (data.vendorContact) {
                model.vendorContactName = `${data.vendorContact.firstName} ${data.vendorContact.lastName}`;
                model.assigneeName = `${data.vendorContact.firstName} ${data.vendorContact.lastName}`;
                model.vendorId = data.vendorContact.vendorId;
            }
        } else if (data.assigneeType === TicketAssignee.Venue) {
            model.venueContactId = data.assignee;
            if (data.venueContact) {
                model.assigneeName = `${data.venueContact.firstName} ${data.venueContact.lastName}`;
                model.venueId = data.venueContact.venueId;
            }
        }

        if (data.system) {
            model.systemId = data.system.id;
            model.extras.system = data.system;
        }

        if (data.creator) {
            model.extras.creator = data.creator;
        }

        if (data.creationTime) {
            model.creationTime = data.creationTime;
        }

        if (data.updateTime) {
            model.creationTime = data.updateTime;
        }

        return model;
    }

    serialize(data: TicketModel): any {
        const transformed = this.utils.transformInto(data, {
            title: { to: 'title' },
            description: { to: 'description' },
            assigneeType: { to: 'assigneeType' },
            systemId: { to: 'systemId' },
            reminderTime: { to: 'reminderTime' },
            inventoryId: { to: 'inventoryId' },
            ticketStatus: {
                to: 'ticketStatus',
                type: ObjectTransformerType.Function,
                transform: (val) => serializeTicketStatus[val]
            }
        });

        if (data.assigneeType === TicketAssignee.Vendor) {
            transformed.assigneeId = data.vendorContactId;
        } else if (data.assigneeType === TicketAssignee.Venue) {
            transformed.assigneeId = data.venueContactId;
        }

        return transformed;
    }

    resourceUrl(): string {
        return `${this.apiUrl}venues/tickets`;
    }

    addByVenueId(model: TicketModel, venueId: number): Promise<TicketModel> {
        return this.addByUrl(model, this.urlByVenueId(venueId));
    }

    queryByVenueId(params: HttpResourceQueryParams, venueId: number): Promise<ResourceQueryResponse<TicketModel>> {
        return this.queryByUrl(params, this.urlByVenueId(venueId));
    }

    getAllByInventoryId(inventoryId: number, venueId: number): Promise<TicketModel[]> {
        const params = this.getAllQueryObject();
        params.otherParams = { inventoryId };
        params.sortFields = ['id'];
        params.sortAsc = false;
        return this.queryByUrl(params, `${this.apiUrl}venues/${venueId}/tickets`)
            .then((resp: ResourceQueryResponse<TicketModel>) => resp.items);
    }

    addCommentByTicketId(ticketComment: any, ticketId: number): Promise<TicketCommentModel> {
        return this.editByUrl(ticketComment, `${this.apiUrl}venues/tickets/${ticketId}/comments`);
    }


}
