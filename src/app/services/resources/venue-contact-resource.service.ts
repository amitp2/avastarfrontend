import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { VenueContactModel } from '../../models/venue-contact-model';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { UtilsService } from '../utils.service';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';

@Injectable()
export class VenueContactResourceService extends HttpResource<VenueContactModel> {

    constructor(httpClient: HttpClient,
                private configService: ConfigService,
                private utilsService: UtilsService) {
        super(httpClient);
    }

    deserialize(data: any): VenueContactModel {
        const model = new VenueContactModel(data)
        model.role = this.utilsService.stringToUserRole(data.role);
        return model;
    }

    serialize(data: VenueContactModel): any {
        let serialized: any;
        serialized = {
            ...data
        };
        serialized['role'] = this.utilsService.userRoleToString(data.role);
        return serialized;
    }

    resourceUrl(): string {
        return `${this.configService.config().API_URL}venues/contacts`;
    }

    getAllByVenueId(venueId: number): Promise<VenueContactModel[]> {
        const params = this.getAllQueryObject();
        return this.queryByVenueId(params, venueId).then(resp => resp.items);
    }

    queryByVenueId(params: HttpResourceQueryParams, venueId: number): Promise<ResourceQueryResponse<VenueContactModel>> {
        return this.queryByUrl(params, `${this.configService.config().API_URL}venues/${venueId}/contacts`);
    }

    addForVenue(model: VenueContactModel, venueId: number): Promise<VenueContactModel> {
        return this.addByUrl(model, `${this.configService.config().API_URL}venues/${venueId}/contacts`);
    }

}
