import { Injectable } from '@angular/core';
import { HttpCachableResource } from '../../classes/http-cachable-resource';
import { UserProductModel } from '../../models/user-product-model';
import { HttpClient } from '@angular/common/http';
import { CommandConstructorService } from '../command-constructor/command-constructor.service';
import { ConfigService } from '../config.service';
import { ResourceConstructorService } from '../resource-constructor/resource-constructor.service';
import { AppCache } from '../../interfaces/cache';
import { UtilsService } from '../utils.service';

@Injectable()
export class UserProductResourceService extends HttpCachableResource<UserProductModel> {

    get apiUrl(): string {
        return this.config.config().API_URL;
    }

    constructor(private cl: HttpClient,
                cache: AppCache,
                private commandConstructor: CommandConstructorService,
                private resourceConstructor: ResourceConstructorService,
                private config: ConfigService,
                private utils: UtilsService) {
        super(cl, cache);
    }


    deserialize(data: any): UserProductModel {
        return this.resourceConstructor.build(data, UserProductModel);
    }

    serialize(data: UserProductModel): any {
        return this.commandConstructor.build(data);
    }

    resourceUrl(): string {
        return `${this.apiUrl}users/products`;
    }

    addForUser(userId: any, model: UserProductModel): Promise<UserProductModel> {
        return this.addByUrl(model, `${this.apiUrl}users/${userId}/products`);
    }

}
