import { Injectable } from '@angular/core';
import { EquipmentModel } from '../../models/equipment-model';
import { HttpResource } from '../../classes/http-resource';
import { HttpClient } from '@angular/common/http';
import { UtilsService } from '../utils.service';
import { ConfigService } from '../config.service';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';
import { ObjectTransformerType } from '../../enums/object-transformer-type.enum';
import { PromisedStoreService } from '../promised-store.service';
import { EquipmentServiceType } from '../../enums/equipment';
import { EquipmentPagingLastParams, EquipmentPagingNext, EquipmentPagingPrev } from '../../store/equipment-paging/equipment-paging.actions';
import { EquipmentPagingState } from '../../store/equipment-paging/equipment-paging.reducers';
import { InventoryPagingLastParams } from '../../store/inventory-paging/inventory-paging.actions';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/store';
import { CurrentPropertyService } from '../current-property.service';
import { EquipmentSubcategoryItem } from '../../models/equipment-subcategory-item.model';

@Injectable()
export class EquipmentResourceService extends HttpResource<EquipmentModel> {

    constructor(
        httpClient: HttpClient,
        private utils: UtilsService,
        private config: ConfigService,
        private promisedStore: PromisedStoreService,
        private store: Store<AppState>,
        private currentVenue: CurrentPropertyService
    ) {
        super(httpClient);
    }

    get apiUrl() {
        return this.config.config().API_URL;
    }

    deserialize(data: any): any {
        if ('equipmentServiceId' in data) {
            return new EquipmentSubcategoryItem(data);
        }
        const transformedData: Partial<EquipmentModel> = this.utils.transformInto(data, {
            id: { to: 'id' },
            name: { to: 'name' },
            type: { to: 'type' },
            clientDescription: { to: 'clientDescription' },
            workflowDescription: { to: 'workflowDescription' },
            revenueCategoryId: { to: 'revenueCategoryId' },
            crossRentalType: { to: 'crossRentalType' },
            setupTime: { to: 'setupTime' },
            priceFromDate: { to: 'priceFromDate' },
            priceToDate: { to: 'priceToDate' },
            // inventoryId: { to: 'inventoryId' },
            categoryId: { to: 'categoryId' },
            subCategoryId: { to: 'subCategoryId' },
            subCategoryItemId: { to: 'subCategoryItemId' },
            inventoryBarcode: { to: 'inventoryBarcode' },
            inventoryTagNumber: { to: 'inventoryTagNumber' },
            subCategoryItemName: { to: 'subCategoryItemName' },
            subCategoryItem: { to: 'subCategoryItem' },
            costCategoryId: { to: 'costCategoryId' },
            equipmentProductInformation: { to: 'equipmentProductInformation' },
            excludeFromReport: { to: 'excludeFromReport' },
            rentable: { to: 'rentable' },
            thirdParty: { to: 'thirdParty' },
            oldPrices: {
                to: 'oldPrices',
                type: ObjectTransformerType.Function,
                transform(oldPrices) {
                    return !Array.isArray(oldPrices) ? [] : oldPrices.map(oldPrice => ({
                        ...oldPrice,
                        creationTime: oldPrice.creationTime ? new Date(oldPrice.creationTime) : null,
                        updateTime: oldPrice.updateTime ? new Date(oldPrice.updateTime) : null
                    }));
                }
            }
        });

        if (data.actualPrice) {
            transformedData.price = data.actualPrice.price;
            transformedData.priceFromDate = data.actualPrice.fromDate;
            transformedData.priceToDate = data.actualPrice.toDate;
            transformedData.lastPriceUpdateTime = new Date(data.actualPrice.creationTime);
        }

        return new EquipmentModel(transformedData);
    }

    serialize(data: EquipmentModel) {
        return this.utils.transformInto(data, {
            name: { to: 'name' },
            type: { to: 'type' },
            clientDescription: { to: 'clientDescription' },
            workflowDescription: { to: 'workflowDescription' },
            revenueCategoryId: { to: 'revenueCategoryId' },
            crossRentalType: { to: 'crossRentalType' },
            setupTime: { to: 'setupTime' },
            excludeFromReport: { to: 'excludeFromReport' },
            price: {
                to: 'price',
                type: ObjectTransformerType.Function,
                transform: (val) => val ? val : 0
            },
            priceFromDate: {
                to: 'priceFromDate',
                type: ObjectTransformerType.Function,
                transform: (val) => val ? val : Date.now()
            },
            subCategoryItemId: { to: 'subCategoryItemId' },
            priceToDate: {
                to: 'priceToDate',
                type: ObjectTransformerType.Function,
                transform: (val) => val ? val : Date.now()
            },
            costCategoryId: { to: 'costCategoryId' },
            equipmentProductInformation: { to: 'equipmentProductInformation' },
            rentable: { to: 'rentable' },
            thirdParty: { to: 'thirdParty' }
        });
    }

    resourceUrl(): string {
        
        return `${this.apiUrl}venues/equipments`;
    }

    queryByVenueId(params: HttpResourceQueryParams, venueId: number): Promise<ResourceQueryResponse<EquipmentModel>> {
        
        return this.queryByUrl(params, `${this.apiUrl}venues/${venueId}/equipments`);
    }

    addByVenueId(model: EquipmentModel, venueId: number): Promise<EquipmentModel> {
        
        return this.addByUrl(model, `${this.apiUrl}venues/${venueId}/equipments`);
    }

    getManyByVenueId(ids: number[], venueId: number): Promise<EquipmentModel[]> {
        
        return this.getManyByUrl(ids, `${this.apiUrl}venues/${venueId}/equipments`);
    }

    getAllByVenueId(venueId: number, search: string = null): Promise<EquipmentModel[]> {
        const params = this.getAllQueryObject();
        params.search = search;
        return this.queryByUrl(params, `${this.apiUrl}venues/${venueId}/equipments`)
            .then((resp) => resp.items);
    }

    //this function is used for only for show rentable = true item in add new event functionPage.
    getAllByRentableTrue(venueId: number, search: string = null): Promise<EquipmentModel[]> {
        return this.getAllByVenueId(venueId, search).then((equip) => {
            return equip.filter((equip) => equip.rentable == true);
        })
    }

    queryItemsByVenueId(params: HttpResourceQueryParams, venueId: number): Promise<ResourceQueryResponse<EquipmentSubcategoryItem>> {
        this.store.dispatch(new EquipmentPagingLastParams(params));
        return this.queryByUrl(params, `${this.apiUrl}venues/${venueId}/equipments/items`) as any;
    }

    async findBySubCategoryIdAndSubCategoryItemId(subCategoryId: number, subCategoryItemId: number): Promise<EquipmentModel> {
        const venueId = await this.promisedStore.currentVenueId();
        const params = this.getAllQueryObject();
        params.otherParams = {
            subCategoryId,
            type: EquipmentServiceType.Equipment
        };
        const res = await this.queryByVenueId(params, venueId);
        const items = res.items;
        const match = items.find(i => +i.subCategoryItemId === +subCategoryItemId);

        if (!match || !match.rentable) {
            return null;
        }
        return match;
    }

    async getNextForPaging() {
        const equipmentPaging: EquipmentPagingState = await this.utils.toPromise(this.store.select(x => x.equipmentPaging));

        if (!equipmentPaging.current) {
            this.store.dispatch(new EquipmentPagingNext(null));
            return null;
        }

        const params = {
            ...equipmentPaging.lastParams,
            size: 1,
            page: equipmentPaging.current.page + 1
        };
        const venue = await this.currentVenue.currentVenueAsync();
        const result = await this.queryByUrl(params, `${this.apiUrl}venues/${venue.id}/equipments/items`);

        let next = null;
        if (result.items.length) {
            next = {
                model: result.items[0],
                page: equipmentPaging.current.page + 1
            };
        }
        this.store.dispatch(new EquipmentPagingNext(next));

        return next;
    }

    async getPrevForPaging() {
        const equipmentPaging: EquipmentPagingState = await this.utils.toPromise(this.store.select(x => x.equipmentPaging));

        if (!equipmentPaging.current || equipmentPaging.current.page === 0) {
            this.store.dispatch(new EquipmentPagingPrev(null));
            return null;
        }

        const params = {
            ...equipmentPaging.lastParams,
            size: 1,
            page: equipmentPaging.current.page - 1
        };
        const venue = await this.currentVenue.currentVenueAsync();
        const result = await this.queryByUrl(params, `${this.apiUrl}venues/${venue.id}/equipments/items`);

        let prev = null;
        if (result.items.length) {
            prev = {
                model: result.items[0],
                page: equipmentPaging.current.page - 1
            };
        }
        this.store.dispatch(new EquipmentPagingPrev(prev));

        return prev;
    }
}
