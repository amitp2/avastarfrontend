import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { StorageSpaceModel } from '../../models/storage-space-model';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { UtilsService } from '../utils.service';
import { ObjectTransformerType } from '../../enums/object-transformer-type.enum';
import { StorageSpaceStatus } from '../../enums/storage-space-status.enum';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';

const serializeStatus = {};
serializeStatus[StorageSpaceStatus.Active] = 'ACTIVE';
serializeStatus[StorageSpaceStatus.Inactive] = 'LOCKED';

const deserializeStatus = {};
deserializeStatus['ACTIVE'] = StorageSpaceStatus.Active;
deserializeStatus['LOCKED'] = StorageSpaceStatus.Inactive;

@Injectable()
export class StorageSpaceResourceService extends HttpResource<StorageSpaceModel> {

    constructor(httpClient: HttpClient,
                private configService: ConfigService,
                private utilsService: UtilsService) {
        super(httpClient);
    }

    deserialize(data: any): StorageSpaceModel {
        return new StorageSpaceModel(this.utilsService.transformInto(data, {
            name: {
                type: ObjectTransformerType.Default,
                to: 'name'
            },
            id: {
                type: ObjectTransformerType.Default,
                to: 'id'
            },
            notes: {
                to: 'notes'
            },
            status: {
                type: ObjectTransformerType.Function,
                to: 'status',
                transform: (val) => deserializeStatus[val]
            }
        }));
    }

    serialize(data: StorageSpaceModel): any {
        return this.utilsService.transformInto(data, {
            name: {
                type: ObjectTransformerType.Default,
                to: 'name'
            },
            status: {
                type: ObjectTransformerType.Function,
                to: 'status',
                transform: (val) => serializeStatus[val]
            },
            notes: {
                to: 'notes'
            }
        });
    }

    resourceUrl(): string {
        return `${this.configService.config().API_URL}venues/storage-spaces`;
    }

    addForVenue(model: StorageSpaceModel, venueId: number): Promise<StorageSpaceModel> {
        return this.addByUrl(model, `${this.configService.config().API_URL}venues/${venueId}/storage-spaces`);
    }

    queryByVenue(params: HttpResourceQueryParams, venueId: number) {
        return this.queryByUrl(params, `${this.configService.config().API_URL}venues/${venueId}/storage-spaces`);
    }

    getAllByVenueId(venueId: number) {
        return this.queryByUrl(this.getAllQueryObject(), `${this.configService.config().API_URL}venues/${venueId}/storage-spaces`)
            .then(resp => resp.items);
    }

}
