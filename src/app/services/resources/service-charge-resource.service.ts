import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { ServiceChargeModel } from '../../models/service-charge-model';
import { HttpClient } from '@angular/common/http';
import { UtilsService } from '../utils.service';
import { ConfigService } from '../config.service';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { ObjectTransformerType } from '../../enums/object-transformer-type.enum';
import { ServiceChargeStatus } from '../../enums/service-charge.enum';
import { TaxChargeStatus } from '../../enums/tax-charge.enum';

@Injectable()
export class ServiceChargeResourceService extends HttpResource<ServiceChargeModel> {

    constructor(
        httpClient: HttpClient,
        private utils: UtilsService,
        private config: ConfigService
    ) {
        super(httpClient);
    }

    get apiUrl() {
        return this.config.config().API_URL;
    }

    deserialize(data: any): ServiceChargeModel {
        return new ServiceChargeModel(this.utils.transformInto(data, {
            id: { to: 'id' },
            name: { to: 'name' },
            charge: { to: 'charge' },
            status: {
                to: 'status',
                type: ObjectTransformerType.Function,
                transform: (val: ServiceChargeStatus) => {
                    if (val === ServiceChargeStatus.Inactive) {
                        return val;
                    }
                    const endDate = new Date(data.endDate);
                    const today = new Date();

                    today.setMilliseconds(0);
                    today.setMinutes(0);
                    today.setHours(0);

                    return endDate < today ? ServiceChargeStatus.Expired : val;
                }
            },
            endDate: { to: 'endDate' },
            startDate: { to: 'startDate' },
            taxRate: { to: 'taxRate' },
            revenueCode: { to: 'revenueCode' },
            revenueCategoryId: { to: 'revenueCategoryId' }
        }));
    }

    serialize(data: ServiceChargeModel) {
        return this.utils.transformInto(data, {
            id: { to: 'id' },
            name: { to: 'name' },
            charge: { to: 'charge' },
            status: {
                to: 'status',
                type: ObjectTransformerType.Function,
                transform(status: TaxChargeStatus) {
                    return TaxChargeStatus.Active;
                    // return status === TaxChargeStatus.Expired ? TaxChargeStatus.Active : status;
                }
            },
            endDate: { to: 'endDate' },
            startDate: { to: 'startDate' },
            taxRate: { to: 'taxRate' },
            revenueCode: { to: 'revenueCode' },
            revenueCategoryId: { to: 'revenueCategoryId' }
        });
    }

    resourceUrl(): string {
        return `${this.apiUrl}accounting/subscribers/revenue-categories/service-charges`;
    }

    addBySubscriberId(model: ServiceChargeModel, subscriberId: number): Promise<ServiceChargeModel> {
        return this.addByUrl(model, `${this.apiUrl}accounting/subscribers/${subscriberId}/revenue-categories/service-charges`)
    }

    queryBySubscriberId(params: HttpResourceQueryParams, subscriberId: number): Promise<ResourceQueryResponse<ServiceChargeModel>> {
        return this.queryByUrl(params, `${this.apiUrl}accounting/subscribers/${subscriberId}/revenue-categories/service-charges`);
    }

    getBySubscriberId(id: number, subscriberId: number): Promise<ServiceChargeModel> {
        return this.queryByUrl(this.getAllQueryObject(), `${this.apiUrl}accounting/subscribers/${subscriberId}/revenue-categories/service-charges`)
            .then((resp: ResourceQueryResponse<ServiceChargeModel>) => {
                return resp.items.find((item: ServiceChargeModel) => +item.id === +id);
            });
    }

    updateForSubscriber(model: ServiceChargeModel, subscriberId: number): Promise<ServiceChargeModel> {
        return this.editByUrl(model, `${this.apiUrl}accounting/subscribers/${subscriberId}/revenue-categories/${model.revenueCategoryId}/service-charges`);
    }

}
