import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { DecisionTreeModel } from '../../models/decision-tree.model';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';

@Injectable()
export class DecisionTreeResourceService extends HttpResource<DecisionTreeModel> {

    constructor(httpClient: HttpClient, private config: ConfigService) {
        super(httpClient);
    }

    deserialize(data: any): DecisionTreeModel {
        return new DecisionTreeModel(data);
    }

    serialize(data: DecisionTreeModel): any {
        return {
            type: data.type,
            subscriberIds: data.subscriberIds
        };
    }

    resourceUrl(): string {
        return this.config.getApiUrl('venues/zingtree');
    }

    queryBySubscriberId(params: HttpResourceQueryParams, id: number) {
        return this.queryByUrl(params, `${this.resourceUrl()}/subscribers/${id}`);
    }

    sync() {
        return this.httpClient.post(`${this.resourceUrl()}/sync`, null).toPromise();
    }
}
