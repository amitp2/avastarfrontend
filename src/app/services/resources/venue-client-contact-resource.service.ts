import {Injectable} from '@angular/core';
import {HttpCachableResource} from '../../classes/http-cachable-resource';
import {VenueClientContactModel} from '../../models/venue-client-contact-model';
import {HttpClient} from '@angular/common/http';
import {AppCache} from '../../interfaces/cache';
import {ResourceQueryResponse} from '../../interfaces/resource-query-response';
import {HttpResourceQueryParams} from '../../interfaces/http-resource-query-params';
import {ConfigService} from '../config.service';
import {UtilsService} from '../utils.service';

@Injectable()
export class VenueClientContactResourceService extends HttpCachableResource<VenueClientContactModel> {

    constructor(client: HttpClient,
                cache: AppCache,
                private configService: ConfigService,
                private utilsService: UtilsService) {
        super(client, cache);
    }

    get apiUrl() {
        return this.configService.config().API_URL;
    }

    deserialize(data: any): VenueClientContactModel {
        const model = new VenueClientContactModel(data);
        model.accountId = data.clientId;
        model.role = this.utilsService.stringToUserRole(data.role);
        return model;
    }

    serialize(data: VenueClientContactModel): any {
        let serialized: any;
        serialized = {
            ...data
        };
        delete serialized.accountId;
        serialized['role'] = this.utilsService.userRoleToString(data.role);
        return serialized;
    }

    resourceUrl(): string {
        return `${this.apiUrl}venues/clients/contacts`;
    }

    queryByClientId(params: HttpResourceQueryParams, clientId: number): Promise<ResourceQueryResponse<VenueClientContactModel>> {
        return this.queryByUrl(params, `${this.apiUrl}venues/clients/${clientId}/contacts`);
    }

    addForClient(model: VenueClientContactModel, clientId: number): Promise<VenueClientContactModel> {
        return this.addByUrl(model, `${this.apiUrl}venues/clients/${clientId}/contacts`);
    }

    getManyByVenueIds(ids: number[]) {
        return this.queryByUrl(this.getAllQueryObject(), `${this.resourceUrl()}?clientIds=${ids.join(',')}`)
            .then((resp) => resp.items);
    }

}
