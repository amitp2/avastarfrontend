import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { VenueSettingsModel } from '../../models/venue-settings-model';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { ServiceCharge } from '../../enums/service-charge.enum';
import { BillingFormat } from '../../enums/billing-format.enum';

@Injectable()
export class VenueSettingsResourceService extends HttpResource<VenueSettingsModel> {


    constructor(httpClient: HttpClient,
                private configService: ConfigService) {
        super(httpClient);
    }

    deserialize(data: any): VenueSettingsModel {
        const modelData: any = {
            ...data
        };

        delete modelData.ccy;
        delete modelData.chargeDiscounted;

        modelData.currency = data.ccy;
        if (data.chargeDiscounted) {
            modelData.serviceChargeDefault = ServiceCharge.PostDiscount;
        } else {
            modelData.serviceChargeDefault = ServiceCharge.PreDiscount;
        }

        switch (data.billingFormat) {
            case 'DAILY':
                modelData.billingFormat = BillingFormat.DAILY;
                break;
            case 'EVENT':
                modelData.billingFormat = BillingFormat.EVENT;
                break;
        }

        return new VenueSettingsModel(modelData);
    }

    serialize(data: VenueSettingsModel): any {
        const serialized: any = {
            ...data
        };
        delete serialized.currency;
        delete serialized.serviceChargeDefault;

        serialized['ccy'] = data.currency;

        switch (data.serviceChargeDefault) {
            case ServiceCharge.PreDiscount:
                serialized['chargeDiscounted'] = false;
                break;
            case ServiceCharge.PostDiscount:
                serialized['chargeDiscounted'] = true;
                break;
        }

        switch (data.billingFormat) {
            case BillingFormat.DAILY:
                serialized['billingFormat'] = 'DAILY';
                break;
            case BillingFormat.EVENT:
                serialized['billingFormat'] = 'EVENT';
                break;
        }

        return serialized;
    }

    resourceUrl(): string {
        return `${this.configService.config().API_URL}venues/settings`;
    }

    getForVenue(venueId: number): Promise<VenueSettingsModel> {
        return this.getByUrl(`${this.configService.config().API_URL}venues/${venueId}/settings`);
    }

    updateForVenue(model: VenueSettingsModel, venueId: number): Promise<VenueSettingsModel> {
        return this.editByUrl(model, `${this.configService.config().API_URL}venues/${venueId}/settings`);
    }

}
