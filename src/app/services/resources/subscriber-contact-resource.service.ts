import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { SubscriberContactModel } from '../../models/subscriber-contact-model';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';
import { UtilsService } from '../utils.service';

@Injectable()
export class SubscriberContactResourceService extends HttpResource<SubscriberContactModel> {

    constructor(httpClient: HttpClient,
                private configService: ConfigService,
                private utilsService: UtilsService) {
        super(httpClient);
    }

    get apiUrl() {
        return this.configService.config().API_URL;
    }

    deserialize(data: any): SubscriberContactModel {
        const model = new SubscriberContactModel(data)
        model.role = this.utilsService.stringToUserRole(data.role);
        return model;
    }

    serialize(data: SubscriberContactModel): any {
        let serialized: any;
        serialized = {
            ...data
        };
        serialized['role'] = this.utilsService.userRoleToString(data.role);
        return serialized;
    }

    resourceUrl(): string {
        return `${this.apiUrl}subscribers/contacts`;
    }

    queryBySubscriberId(params: HttpResourceQueryParams, subscriberId: number): Promise<ResourceQueryResponse<SubscriberContactModel>> {
        return this.queryByUrl(params, `${this.apiUrl}subscribers/${subscriberId}/contacts`);
    }

    addForSubscriber(model: SubscriberContactModel, subscriberId: number): Promise<SubscriberContactModel> {
        return this.addByUrl(model, `${this.apiUrl}subscribers/${subscriberId}/contacts`);
    }

    getManyBySubscriberIds(ids: number[]) {
        return this.queryByUrl(this.getAllQueryObject(), `${this.resourceUrl()}?subscriberIds=${ids.join(',')}`)
            .then((resp) => resp.items);
    }

}
