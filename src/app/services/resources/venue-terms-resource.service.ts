import {Injectable} from '@angular/core';
import {HttpCachableResource} from '../../classes/http-cachable-resource';
import {VenueTermsModel} from '../../models/venue-terms';
import {HttpClient} from '@angular/common/http';
import {AppCache} from '../../interfaces/cache';
import {UtilsService} from '../utils.service';
import {ConfigService} from '../config.service';

@Injectable()
export class VenueTermsResourceService extends HttpCachableResource<VenueTermsModel> {


    constructor(client: HttpClient,
                cache: AppCache,
                private utils: UtilsService,
                private config: ConfigService) {
        super(client, cache);
    }

    get apiUrl(): string {
        return this.config.config().API_URL;
    }

    deserialize(data: any): VenueTermsModel {
        return new VenueTermsModel(this.utils.transformInto(data, {
            terms: { to: 'terms' },
            billingComment: { to: 'billingComment' },
            proposalComment: { to: 'proposalComment' },
        }));
    }

    serialize(data: VenueTermsModel): any {
        return this.utils.transformInto(data, {
            terms: { to: 'terms' },
            billingComment: { to: 'billingComment' },
            proposalComment: { to: 'proposalComment' },
        });
    }

    resourceUrl(): string {
        return undefined;
    }

    urlByVenueId(venueId: number) {
        return `${this.apiUrl}venues/${venueId}/terms`;
    }

    editByVenueId(model: VenueTermsModel, venueId: number): Promise<VenueTermsModel>  {
        return this.editByUrl(model, this.urlByVenueId(venueId));
    }

    getByVenueId(venueId: number): Promise<any> {
        return this.getByUrl(this.urlByVenueId(venueId));
    }

}
