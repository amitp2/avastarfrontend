import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { EventSpaceModel } from '../../models/event-space-model';
import { HttpClient } from '@angular/common/http';
import { UtilsService } from '../utils.service';
import { ConfigService } from '../config.service';
import { ObjectTransformerType } from '../../enums/object-transformer-type.enum';
import { EventSpaceType } from '../../enums/event-space-type.enum';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';

const serializeEventSpaceType = {};
serializeEventSpaceType[EventSpaceType.Active] = 'ACTIVE';
serializeEventSpaceType[EventSpaceType.Inactive] = 'LOCKED';

const deserializeEventSpaceType = {};
deserializeEventSpaceType['ACTIVE'] = EventSpaceType.Active;
deserializeEventSpaceType['LOCKED'] = EventSpaceType.Inactive;

@Injectable()
export class EventSpaceResourceService extends HttpResource<EventSpaceModel> {

    constructor(httpClient: HttpClient,
                private configService: ConfigService,
                private utilsService: UtilsService) {
        super(httpClient);
    }

    deserialize(data: any): EventSpaceModel {
        const newModel = this.utilsService.transformInto(data, {
            name: {
                type: ObjectTransformerType.Default,
                to: 'name'
            },
            id: {
                type: ObjectTransformerType.Default,
                to: 'id'
            },
            functionSpaces: {
                type: ObjectTransformerType.Default,
                to: 'functionSpaces'
            },
            status: {
                type: ObjectTransformerType.Function,
                to: 'status',
                transform: (val) => deserializeEventSpaceType[val]
            }
        });
        return new EventSpaceModel(newModel);
    }

    serialize(data: EventSpaceModel): any {
        return this.utilsService.transformInto(data, {
            name: {
                type: ObjectTransformerType.Default,
                to: 'name'
            },
            status: {
                type: ObjectTransformerType.Function,
                to: 'status',
                transform: (val) => serializeEventSpaceType[val]
            }
        });
    }

    resourceUrl(): string {
        return `${this.configService.config().API_URL}venues/event-spaces`;
    }

    queryByVenueId(params: HttpResourceQueryParams, venueId: number) {
        return this.queryByUrl(params, `${this.configService.config().API_URL}venues/${venueId}/event-spaces`);
    }

    addForVenue(model: EventSpaceModel, venueId: number): Promise<EventSpaceModel> {
        return this.addByUrl(model, `${this.configService.config().API_URL}venues/${venueId}/event-spaces`);
    }

    getAllByVenueId(venueId: number) {
        return this
            .queryByUrl(this.getAllQueryObject(), `${this.configService.config().API_URL}venues/${venueId}/event-spaces`)
            .then((resp) => resp.items);
    }

}
