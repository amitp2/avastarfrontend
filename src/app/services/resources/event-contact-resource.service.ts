import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { VenueEventContact } from '../../models/venue-event-model';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';

@Injectable()
export class EventContactResourceService extends HttpResource<VenueEventContact> {
    private apiUrl: string;

    constructor(http: HttpClient, config: ConfigService) {
        super(http);
        this.apiUrl = config.config().API_URL;
    }

    deserialize(data: any): VenueEventContact {
        return VenueEventContact.fromJson(data);
    }

    serialize(data: VenueEventContact) {
        return data.toJson();
    }
    
    resourceUrl(): string {
        return this.apiUrl + 'venues/events/contacts';
    }

    addByEventId(model: VenueEventContact, eventId: number) {
        const url = `${this.apiUrl}venues/events/${eventId}/contacts`;
        return this.addByUrl(model, url);
    }

    queryByEventId(params: HttpResourceQueryParams, eventId: number) {
        const url = `${this.apiUrl}venues/events/${eventId}/contacts`;
        return this.queryByUrl(params, url);
    }

}
