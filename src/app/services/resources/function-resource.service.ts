import { Injectable } from '@angular/core';
import { HttpCachableResource } from '../../classes/http-cachable-resource';
import { FunctionModel } from '../../models/function-model';
import { HttpClient } from '@angular/common/http';
import { AppCache } from '../../interfaces/cache';
import { ConfigService } from '../config.service';
import { ResourceConstructorService } from '../resource-constructor/resource-constructor.service';
import { CommandConstructorService } from '../command-constructor/command-constructor.service';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';
import { VenueEventResourceService } from './venue-event-resource.service';
import { AppState } from '../../store/store';
import { Store } from '@ngrx/store';
import {
    FunctionPagingLastParams, FunctionPagingNext, FunctionPagingPrev
} from '../../store/function-paging/function-paging.actions';
import { UtilsService } from '../utils.service';
import { FunctionPagingState } from '../../store/function-paging/function-paging.reducers';

@Injectable()
export class FunctionResourceService extends HttpCachableResource<FunctionModel> {

    get apiUrl(): string {
        return this.config.config().API_URL;
    }

    constructor(
        http: HttpClient,
        cache: AppCache,
        private config: ConfigService,
        private commandConstructor: CommandConstructorService,
        private resourceConstructor: ResourceConstructorService,
        private eventResource: VenueEventResourceService,
        private store: Store<AppState>,
        private utils: UtilsService
    ) {
        super(http, cache);
    }


    deserialize(data: any): FunctionModel {
        return this.resourceConstructor.build(data, FunctionModel);
    }

    serialize(data: FunctionModel): any {
        const serialized = this.commandConstructor.build(data);

        const sortFunc = (a, b) => a.pos - b.pos;
        data.eventFunctionEquipments.sort(sortFunc);
        serialized.eventFunctionEquipments.sort(sortFunc);


        data.eventFunctionEquipments.forEach((eq, index1) => {
            serialized.eventFunctionEquipments[index1].pos = index1;
            eq.saved = true;
            data.eventFunctionDates.forEach((date, index2) => {
                serialized.eventFunctionEquipments[index1].eventFunctionEquipmentDates[index2].setupDay = date.setupDay;
            });
        });

        return serialized;
    }

    resourceUrl(): string {
        return `${this.apiUrl}venues/events/functions`;
    }

    queryByEventId(params: HttpResourceQueryParams, eventId: number): Promise<ResourceQueryResponse<FunctionModel>> {
        params.otherParams = { eventId };
        this.store.dispatch(new FunctionPagingLastParams(params));
        return this.queryByUrl(params, this.resourceUrl());
    }

    async getAllByEventId(eventId: number): Promise<FunctionModel[]> {
        const params = this.getAllQueryObject();
        const res = await this.queryByEventId(params, eventId);
        return res.items;
    }

    async getAllByVenueId(venueId: number): Promise<FunctionModel[]> {
        const events = await this.eventResource.getAllByVenueId(venueId);
        const functionsByEvents = await Promise.all(events.map(e => this.getAllByEventId(e.id)));
        const functions = functionsByEvents.reduce((acc, cur) => acc.concat(cur), []);
        return functions;
    }

    async getNextForPaging() {
        const functionPaging: FunctionPagingState = await this.utils.toPromise(this.store.select(x => x.functionPaging));

        if (!functionPaging.current) {
            this.store.dispatch(new FunctionPagingNext(null));
            return null;
        }

        const params = {
            ...functionPaging.lastParams,
            size: 1,
            page: functionPaging.current.page + 1,
            otherParams: {
                eventId: functionPaging.lastParams.otherParams.eventId
            }
        };

        const result = await this.queryByUrl(params, this.resourceUrl());

        let next = null;
        if (result.items.length) {
            next = {
                id: result.items[0].id,
                page: functionPaging.current.page + 1
            };
        }

        this.store.dispatch(new FunctionPagingNext(next));

        return next;
    }

    async getPrevForPaging() {
        const functionPaging: FunctionPagingState = await this.utils.toPromise(this.store.select(x => x.functionPaging));

        if (!functionPaging.current || functionPaging.current.page === 0) {
            this.store.dispatch(new FunctionPagingPrev(null));
            return null;
        }

        const params = {
            ...functionPaging.lastParams,
            size: 1,
            page: functionPaging.current.page - 1,
            otherParams: {
                eventId: functionPaging.lastParams.otherParams.eventId
            }
        };

        const result = await this.queryByUrl(params, this.resourceUrl());

        let prev = null;
        if (result.items.length) {
            prev = {
                id: result.items[0].id,
                page: functionPaging.current.page - 1
            };
        }

        this.store.dispatch(new FunctionPagingPrev(prev));

        return prev;
    }
}
