import { Injectable } from '@angular/core';
import { RevenueCategoryModel } from '../../models/revenue-category-model';
import { HttpResource } from '../../classes/http-resource';
import { HttpClient } from '@angular/common/http';
import { UtilsService } from '../utils.service';
import { ConfigService } from '../config.service';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';
import { AuthService } from '../auth.service';

@Injectable()
export class RevenueCategoryResourceService extends HttpResource<RevenueCategoryModel> {

    constructor(
        httpClient: HttpClient,
        private utils: UtilsService,
        private config: ConfigService,
        private authService: AuthService
    ) {
        super(httpClient);
    }

    get apiUrl() {
        return this.config.config().API_URL;
    }

    deserialize(data: any): RevenueCategoryModel {
        return new RevenueCategoryModel(this.utils.transformInto(data, {
            id: { to: 'id' },
            revenueCategoryName: { to: 'revenueCategoryName' },
            revenueCategoryId: { to: 'revenueCategoryId' },
            revenueCode: { to: 'revenueCode' },
            discountable: { to: 'discountable' },
            serviceCharge: { to: 'serviceCharge' },
            code: { to: 'code' },
            calculateCode: { to: 'calculateCode' },
            name: { to: 'name' },
            description: { to: 'description' }
        }));
    }

    serialize(data: RevenueCategoryModel) {
        return this.utils.transformInto(data, {
            revenueCategoryName: { to: 'revenueCategoryName' },
            revenueCategoryId: { to: 'revenueCategoryId' },
            revenueCode: { to: 'revenueCode' },
            discountable: { to: 'discountable' },
            serviceCharge: { to: 'serviceCharge' },
            name: { to: 'name' },
            description: { to: 'description' },
            code: { to: 'code' },
            calculateCode: { to: 'calculateCode' }
        });
    }

    resourceUrl(): string {
        return `${this.apiUrl}accounting/revenue-categories`;
    }

    queryBySubscriberId(params: HttpResourceQueryParams, subscriberId: number): Promise<ResourceQueryResponse<RevenueCategoryModel>> {
        return this.queryByUrl(params, `${this.apiUrl}accounting/subscribers/${subscriberId}/revenue-categories`);
    }

    getBySubscriberId(id: number, subscriberId: number): Promise<RevenueCategoryModel> {
        return this.queryByUrl(this.getAllQueryObject(), `${this.apiUrl}accounting/subscribers/${subscriberId}/revenue-categories`)
            .then((resp: ResourceQueryResponse<RevenueCategoryModel>) => {
               return resp.items.find((item: RevenueCategoryModel) => +item.revenueCategoryId === +id);
            });
    }

    updateForSubscriber(model: RevenueCategoryModel, subscriberId: number): Promise<RevenueCategoryModel> {
        return this.editByUrl(model, `${this.apiUrl}accounting/subscribers/${subscriberId}/revenue-categories`);
    }

    getAllBySubscriberId(subscriberId: number): Promise<RevenueCategoryModel[]> {
        const params = this.getAllQueryObject();
        return this.queryByUrl(params, `${this.apiUrl}accounting/subscribers/${subscriberId}/revenue-categories`)
            .then((resp) => resp.items);
    }

    async getAllForCurrentSubscriber(): Promise<RevenueCategoryModel[]> {
        const user = await this.authService.currentUser();
        const subsId = user.subscriberId;
        const cats = await this.getAllBySubscriberId(subsId);
        return cats;
    }
}
