import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { CountryModel } from '../../models/country-model';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';

@Injectable()
export class CountryResourceService extends HttpResource<CountryModel> {

    constructor(httpClient: HttpClient, private configService: ConfigService) {
        super(httpClient);
    }

    deserialize(data: any): CountryModel {
        return new CountryModel(data);
    }

    serialize(data: CountryModel): any {
        throw new Error('method not implemented');
    }

    resourceUrl(): string {
        return `${this.configService.config().API_URL}utils/countries`;
    }

}
