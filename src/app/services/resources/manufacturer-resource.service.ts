import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { ManufacturerModel } from '../../models/manufacturer-model';
import { HttpClient } from '@angular/common/http';
import { UtilsService } from '../utils.service';
import { ConfigService } from '../config.service';
import { ObjectTransformerType } from '../../enums/object-transformer-type.enum';

@Injectable()
export class ManufacturerResourceService extends HttpResource<ManufacturerModel> {

    constructor(httpClient: HttpClient,
                private utilsService: UtilsService,
                private configService: ConfigService) {
        super(httpClient);
    }

    get apiUrl(): string {
        return this.configService.config().API_URL;
    }

    deserialize(data: any): ManufacturerModel {
        return this.utilsService.transformInto(data, {
            id: {
                to: 'id',
                type: ObjectTransformerType.Default
            },
            name: {
                to: 'name',
                type: ObjectTransformerType.Default
            },
            usage: {
                to: 'usage',
                type: ObjectTransformerType.Default
            }
        });
    }

    serialize(data: ManufacturerModel) {
        return this.utilsService.transformInto(data, {
            name: {
                to: 'name',
                type: ObjectTransformerType.Default
            }
        });
    }

    resourceUrl(): string {
        return `${this.configService.config().API_URL}utils/manufacturers`;
    }

    searchByName(name: string): Promise<ManufacturerModel[]> {
        const query = {
            ...this.getAllQueryObject(),
            otherParams: {
                name
            }
        };
        return this.query(query).then((resp) => resp.items);
    }

    async increaseUsage(id: number) {
        const put = this.httpClient.put(`${this.apiUrl}utils/manufacturers/${id}/increase-usage`, {}, {
            responseType: 'text'
        });
        await this.utilsService.toPromise(put);
    }

    async decreaseUsage(id: number) {
        const put = this.httpClient.put(`${this.apiUrl}utils/manufacturers/${id}/decrease-usage`, {}, {
            responseType: 'text'
        });
        await this.utilsService.toPromise(put);
    }

}
