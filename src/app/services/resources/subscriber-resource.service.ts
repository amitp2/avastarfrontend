import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { SubscriberModel } from '../../models/subscriber-model';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { AccountType } from '../../enums/account-type.enum';
import { SubscriberStatus } from '../../enums/subscriber-status.enum';
import { PaymentInfo } from '../../models/subscriber-payment.model';
import * as moment from 'moment';

@Injectable()
export class SubscriberResourceService extends HttpResource<SubscriberModel> {

    constructor(
        httpClient: HttpClient,
        private configService: ConfigService
    ) {
        super(httpClient);
    }

    deserialize(data: any): SubscriberModel {
        const model = new SubscriberModel();

        model.id = +data.id;
        model.name = data.name;
        model.accountType = data.accountType === 'CLIENT' ? AccountType.Client : AccountType.House;
        model.status = data.status === 'LOCKED' ? SubscriberStatus.Locked : SubscriberStatus.Active;
        model.countryId = data.countryId;
        model.stateId = data.stateId;
        model.city = data.city;
        model.address = data.address;
        model.address2 = data.address2;
        model.postCode = data.postCode;
        model.phone = data.phone;
        model.fax = data.fax;
        model.branch = data.branch;
        model.website = data.website;
        model.logoUrl = data.logoUrl;
        model.masterAgreementAgreed = data.masterAgreementAgreed;
        model.usersCount = data.usersCount;

        return model;
    }

    serialize(data: SubscriberModel): any {
        const serialized = {};

        serialized['status'] = data.status === SubscriberStatus.Locked ? 'LOCKED' : 'ACTIVE';
        serialized['name'] = data.name;
        serialized['accountType'] = data.accountType === AccountType.Client ? 'CLIENT' : 'HOUSE';
        serialized['countryId'] = data.countryId;
        serialized['stateId'] = data.stateId;
        serialized['city'] = data.city;
        serialized['address'] = data.address;
        serialized['address2'] = data.address2;
        serialized['postCode'] = data.postCode;
        serialized['phone'] = data.phone;
        serialized['fax'] = data.fax;
        serialized['branch'] = data.branch;
        serialized['website'] = data.website;
        serialized['logoUrl'] = data.logoUrl;
        serialized['masterAgreementAgreed'] = data.masterAgreementAgreed;

        return serialized;
    }

    resourceUrl(): string {
        return `${this.configService.config().API_URL}subscribers`;
    }

    getPaymentInfo(subscriberId: number) {
        return this.httpClient.get<PaymentInfo>(this.configService.getApiUrl(`subscribers/${subscriberId}/payment-info`))
            .map(response => {
                return new PaymentInfo({
                    ...response,
                    creationDate: new Date(response.creationDate),
                    nextPaymentDate: response.nextPaymentDate ? new Date(response.nextPaymentDate) : undefined,
                    subscriptionDate: response.subscriptionDate ? new Date(response.subscriptionDate) : undefined
                });
            })
            .toPromise();
    }

    editPaymentInfo(subscriberId: number, paymentInfo: PaymentInfo) {
        
        const data = {
            ...paymentInfo,
            subscriptionDate: moment(paymentInfo.subscriptionDate).format('YYYY-MM-DD')
        };
        return this.httpClient.put(this.configService.getApiUrl(`subscribers/${subscriberId}/payment-info`), data)
            .toPromise();
    }

    getNames(ids: number[]): Promise<string[]> {
        return this.httpClient.get<string[]>(
            this.configService.getApiUrl('subscribers/names'),
            { params: { ids: ids.join(',') } }
        )
        .toPromise();
    }
}
