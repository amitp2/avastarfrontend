import {Injectable} from '@angular/core';
import {HttpCachableResource} from '../../classes/http-cachable-resource';
import {HttpClient} from '@angular/common/http';
import {AppCache} from '../../interfaces/cache';
import {PackageModel} from '../../models/package-model';
import {UtilsService} from '../utils.service';
import {ConfigService} from '../config.service';
import {HttpResourceQueryParams} from '../../interfaces/http-resource-query-params';
import {ResourceQueryResponse} from '../../interfaces/resource-query-response';
import { ObjectTransformerType } from '../../enums/object-transformer-type.enum';

@Injectable()
export class PackageResourceService extends HttpCachableResource<PackageModel> {

    constructor(client: HttpClient,
                cache: AppCache,
                private utils: UtilsService,
                private config: ConfigService) {
        super(client, cache);
    }

    get apiUrl(): String {
        return this.config.config().API_URL;
    }

    deserialize(data: any): PackageModel {

        const deserialized = this.utils.transformInto(data, {
            id: { to: 'id' },
            name: { to: 'name' },
            description: { to: 'description' },
            salesDescription: { to: 'salesDescription' },
            pictureUrl: { to: 'pictureUrl' },
            oldPrices: {
                to: 'oldPrices',
                type: ObjectTransformerType.Function,
                transform(oldPrices) {
                    return !Array.isArray(oldPrices) ? [] : oldPrices.map(oldPrice => ({
                        ...oldPrice,
                        creationTime: oldPrice.creationTime ? new Date(oldPrice.creationTime) : null,
                        updateTime: oldPrice.updateTime ? new Date(oldPrice.updateTime) : null
                    }));
                }
            }
        });

        if (data.actualPrice) {
            if (data.actualPrice.price) {
                deserialized.price = data.actualPrice.price;
            }

            if (data.actualPrice.fromDate) {
                deserialized.priceFromDate = data.actualPrice.fromDate;
            }

            if (data.actualPrice.toDate) {
                deserialized.priceToDate = data.actualPrice.toDate;
            }

            if (data.actualPrice.creationTime) {
                deserialized.priceCreationDate = data.actualPrice.creationTime;
            }
        }


        // deserialized.equipmentIds = [];
        if (data.equipments) {
            deserialized.equipments = data.equipments.map((eq: any) => ({
                equipmentId: eq.equipment.id,
                count: eq.count
            }));
        }

        return new PackageModel(deserialized);
    }

    serialize(data: PackageModel): any {
        return this.utils.transformInto(data, {
            name: { to: 'name' },
            description: { to: 'description' },
            salesDescription: { to: 'salesDescription' },
            equipments: { to: 'equipments' },
            // equipmentIds: { to: 'equipmentIds' },
            // systemIds: { to: 'systemIds' },
            price: { to: 'price' },
            priceFromDate: { to: 'priceFromDate' },
            priceToDate: { to: 'priceToDate' },
            pictureUrl: { to: 'pictureUrl' }
        });
    }

    resourceUrl(): string {
        return `${this.apiUrl}venues/packages/`;
    }

    private urlByVenueId(venueId: number): string {
        return `${this.apiUrl}venues/${venueId}/packages`;
    }

    addByVenueId(model: PackageModel, venueId: number): Promise<PackageModel>  {
        return this.addByUrl(model, this.urlByVenueId(venueId));
    }

    queryByVenueId(params: HttpResourceQueryParams, venueId: number): Promise<ResourceQueryResponse<PackageModel>> {
        return this.queryByUrl(params, this.urlByVenueId(venueId));
    }

    getAllByVenueId(venueId: number, search: string = null): Promise<PackageModel[]> {
        const params = this.getAllQueryObject();
        params.search = search;
        return this.queryByUrl(params, this.urlByVenueId(venueId))
            .then((resp) => resp.items);
    }

}
