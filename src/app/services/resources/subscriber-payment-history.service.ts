import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { PaymentHistory } from '../../models/subscriber-payment.model';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import * as moment from 'moment';

@Injectable()
export class SubscriberPaymentHistoryService extends HttpResource<PaymentHistory> {

    constructor(http: HttpClient, private config: ConfigService) {
        super(http);
    }

    deserialize(data: any): PaymentHistory {
        return new PaymentHistory({
            ...data,
            date: new Date(data.date)
        });
    }

    serialize(data: PaymentHistory): any {
        return {
            ...data,
            date: moment(data.date).format('YYYY-MM-DD')
        };
    }

    resourceUrl(): string {
        return this.config.getApiUrl('subscribers/payment-history');
    }

    queryForSubscriber(params: HttpResourceQueryParams, id: number) {
        return this.queryByUrl(params, this.config.getApiUrl(`subscribers/${id}/payment-history`));
    }

    addForSubscriber(subscriberId: number, model: PaymentHistory) {
        return this.addByUrl(model, this.config.getApiUrl(`subscribers/${subscriberId}/payment-history`));
    }

}
