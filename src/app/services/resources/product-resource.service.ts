import { Injectable } from '@angular/core';
import { HttpCachableResource } from '../../classes/http-cachable-resource';
import { ProductModel, SubscriberProductUsage } from '../../models/product-model';
import { HttpClient } from '@angular/common/http';
import { AppCache } from '../../interfaces/cache';
import { CommandConstructorService } from '../command-constructor/command-constructor.service';
import { ResourceConstructorService } from '../resource-constructor/resource-constructor.service';
import { ConfigService } from '../config.service';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { ProductType } from '../../enums/product-type.enum';
import { UtilsService } from '../utils.service';
import { productTypeToApi } from '../../helpers/mapper-functions/product-type-to-api.mapper-function';
import { SubscriberProductAccess, SubscriberProducts } from '../../store/subscriber-product-access/subscriber-product-access';
import { Debounce } from '../../decorators/debounce.decorator';

@Injectable()
export class ProductResourceService extends HttpCachableResource<ProductModel> {

    get apiUrl(): string {
        return this.config.config().API_URL;
    }

    // TODO: hack for the server
    findBySubscriberIdQueue = [];

    constructor(private cl: HttpClient,
                cache: AppCache,
                private commandConstructor: CommandConstructorService,
                private resourceConstructor: ResourceConstructorService,
                private config: ConfigService,
                private utils: UtilsService) {
        super(cl, cache);
    }

    deserialize(data: any): ProductModel {
        return this.resourceConstructor.build(data, ProductModel);
    }

    serialize(data: ProductModel): any {
        return this.commandConstructor.build(data);
    }

    resourceUrl(): string {
        return `${this.apiUrl}subscribers/products`;
    }

    findBySubscriberId(subscriberId: number, params: HttpResourceQueryParams): Promise<ResourceQueryResponse<ProductModel>> {
        return this.queryByUrl(params, `${this.apiUrl}subscribers/${subscriberId}/products`);
    }

    async usageBySubscriberAndProduct(subscriberId: number, product: ProductType): Promise<number> {
        const usage = await this.utils.toPromise(this.cl.get(`${this.apiUrl}subscribers/${subscriberId}/products/${productTypeToApi(product)}/usage`));
        return usage.used;
    }

    async usageBySubscriber(subscriberId: number): Promise<SubscriberProductUsage> {
        const params = this.getAllQueryObject();
        const all = await this.findBySubscriberId(subscriberId, params);
        const products = all.items;
        const usage = await Promise.all(products.map(p => this.usageBySubscriberAndProduct(subscriberId, p.product)));
        const res = {};
        products.forEach((p, i) => {
            res[p.product] = usage[i];
        });
        return res;
    }

    async accessBySubscriber(subscriberId: number): Promise<SubscriberProducts> {
        const products = await this.findAllBySubscriberId(subscriberId);
        const res = {};
        products.forEach((p, i) => {
           res[p.product] = products[i].licensedUsersCount;
        });
        return res;
    }

    async findAllBySubscriberId(subscriberId: number): Promise<ProductModel[]> {
        const params = this.getAllQueryObject();
        const res = await this.findBySubscriberId(subscriberId, params);
        return res.items;
    }

}
