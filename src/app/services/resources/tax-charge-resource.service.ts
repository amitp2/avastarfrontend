import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { TaxChargeModel } from '../../models/tax-charge-model';
import { HttpClient } from '@angular/common/http';
import { UtilsService } from '../utils.service';
import { ConfigService } from '../config.service';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { ObjectTransformerType } from '../../enums/object-transformer-type.enum';
import { TaxChargeStatus } from '../../enums/tax-charge.enum';

@Injectable()
export class TaxChargeResourceService extends HttpResource<TaxChargeModel> {

    constructor(
        httpClient: HttpClient,
        private utils: UtilsService,
        private config: ConfigService
    ) {
        super(httpClient);
    }

    get apiUrl() {
        return this.config.config().API_URL;
    }

    deserialize(data: any): TaxChargeModel {
        return new TaxChargeModel(this.utils.transformInto(data, {
            id: { to: 'id' },
            name: { to: 'name' },
            charge: { to: 'charge' },
            status: {
                to: 'status',
                type: ObjectTransformerType.Function,
                transform: (val: TaxChargeStatus) => {
                    if (val === TaxChargeStatus.Inactive) {
                        return val;
                    }
                    const endDate = new Date(data.endDate);
                    const today = new Date();

                    today.setMilliseconds(0);
                    today.setMinutes(0);
                    today.setHours(0);

                    return endDate < today ? TaxChargeStatus.Expired : val;
                }
            },
            endDate: { to: 'endDate' },
            startDate: { to: 'startDate' },
            revenueCode: { to: 'revenueCode' },
            revenueCategoryId: { to: 'revenueCategoryId' }
        }));
    }

    serialize(data: TaxChargeModel) {
        return this.utils.transformInto(data, {
            id: { to: 'id' },
            name: { to: 'name' },
            charge: { to: 'charge' },
            status: {
                to: 'status',
                type: ObjectTransformerType.Function,
                transform(status: TaxChargeStatus) {
                    return TaxChargeStatus.Active;
                    // return status === TaxChargeStatus.Expired ? TaxChargeStatus.Active : status;
                }
            },
            endDate: { to: 'endDate' },
            startDate: { to: 'startDate' },
            revenueCode: { to: 'revenueCode' },
            revenueCategoryId: { to: 'revenueCategoryId' }
        });
    }

    resourceUrl(): string {
        return `${this.apiUrl}accounting/subscribers/revenue-categories/tax-charges`;
    }

    addBySubscriberId( model: TaxChargeModel, subscriberId: number): Promise<TaxChargeModel> {
        return this.addByUrl(model, `${this.apiUrl}accounting/subscribers/${subscriberId}/revenue-categories/tax-charges`)
    }

    queryBySubscriberId(params: HttpResourceQueryParams, subscriberId: number): Promise<ResourceQueryResponse<TaxChargeModel>> {
        return this.queryByUrl(params, `${this.apiUrl}accounting/subscribers/${subscriberId}/revenue-categories/tax-charges`);
    }

    getBySubscriberId(id: number, subscriberId: number): Promise<TaxChargeModel> {
        const url = `${this.apiUrl}accounting/subscribers/${subscriberId}/revenue-categories/tax-charges`;

        return this.queryByUrl(this.getAllQueryObject(), url)
            .then((resp: ResourceQueryResponse<TaxChargeModel>) => {
                return resp.items.find((item: TaxChargeModel) => +item.id === +id);
            });
    }

    updateForSubscriber(model: TaxChargeModel, subscriberId: number): Promise<TaxChargeModel> {
        const url = `${this.apiUrl}accounting/subscribers/${subscriberId}/revenue-categories/${model.revenueCategoryId}/tax-charges`;

        return this.editByUrl(model, url);
    }
}
