import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { CostCategoryModel } from '../../models/cost-category-model';
import { HttpClient } from '@angular/common/http';
import { UtilsService } from '../utils.service';
import { ConfigService } from '../config.service';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';
import { AuthService } from '../auth.service';

@Injectable()
export class CostCategoryResourceService extends HttpResource<CostCategoryModel> {


    constructor(httpClient: HttpClient,
                private utils: UtilsService,
                private config: ConfigService,
                private authService: AuthService) {
        super(httpClient);
    }

    deserialize(data: any): CostCategoryModel {
        return new CostCategoryModel(this.utils.transformInto(data, {
            costCode: {to: 'costCode'},
            costCategoryId: {to: 'costCategoryId'},
            costCategoryName: {to: 'costCategoryName'},
            name: { to: 'name' },
            description: { to: 'description' },
            id: { to: 'id' },
        }));
    }

    serialize(data: CostCategoryModel): any {
        return this.utils.transformInto(data, {
            costCode: {to: 'costCode'},
            costCategoryId: {to: 'costCategoryId'},
            name: { to: 'name' },
            description: { to: 'description' },
            id: { to: 'id' },
        });
    }

    resourceUrl(): string {
        return `${this.config.config().API_URL}accounting/cost-categories`;
    }

    getAllBySubscriberId(subscriberId: number): Promise<CostCategoryModel[]> {
        const params = this.getAllQueryObject();
        return this.queryBySubscriberId(params, subscriberId).then(resp => resp.items);
    }

    queryBySubscriberId(params: HttpResourceQueryParams, subscriberId: number): Promise<ResourceQueryResponse<CostCategoryModel>> {
        return this.queryByUrl(params, `${this.config.config().API_URL}accounting/subscribers/${subscriberId}/cost-categories`);
    }

    getBySubscriberId(id: number, subscriberId: number): Promise<CostCategoryModel> {
        return this.queryByUrl(this.getAllQueryObject(), `${this.config.config().API_URL}accounting/subscribers/${subscriberId}/cost-categories`)
            .then((resp: ResourceQueryResponse<CostCategoryModel>) => {
               return resp.items.find((item: CostCategoryModel) => +item.costCategoryId === +id);
            });
    }

    updateForSubscriber(model: CostCategoryModel, subscriberId: number): Promise<CostCategoryModel> {
        return this.editByUrl(model, `${this.config.config().API_URL}accounting/subscribers/${subscriberId}/cost-categories`);
    }

    async getAllForCurrentSubscriber(): Promise<CostCategoryModel[]> {
        const user = await this.authService.currentUser();
        const subsId = user.subscriberId;
        const cats = await this.getAllBySubscriberId(subsId);
        return cats;
    }

}
