import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { FunctionSpaceModel } from '../../models/function-space-model';
import { HttpClient } from '@angular/common/http';
import { UtilsService } from '../utils.service';
import { ConfigService } from '../config.service';
import { ObjectTransformerType } from '../../enums/object-transformer-type.enum';
import { FunctionSpaceStatus } from '../../enums/function-space-status.enum';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';
import { EventSpaceResourceService } from './event-space-resource.service';
import { EventSpaceModel } from '../../models/event-space-model';

const serializeFunctionSpaceType = {};
serializeFunctionSpaceType[FunctionSpaceStatus.Active] = 'ACTIVE';
serializeFunctionSpaceType[FunctionSpaceStatus.Inactive] = 'LOCKED';

const deserializeFunctionSpaceType = {};
deserializeFunctionSpaceType['ACTIVE'] = FunctionSpaceStatus.Active;
deserializeFunctionSpaceType['LOCKED'] = FunctionSpaceStatus.Inactive;

@Injectable()
export class FunctionSpaceResourceService extends HttpResource<FunctionSpaceModel> {

    constructor(httpClient: HttpClient,
                private utilsService: UtilsService,
                private configService: ConfigService,
                private eventSpace: EventSpaceResourceService) {
        super(httpClient);
    }

    deserialize(data: any): FunctionSpaceModel {
        return new FunctionSpaceModel(this.utilsService.transformInto(data, {
            name: {
                type: ObjectTransformerType.Default,
                to: 'name'
            },
            id: {
                type: ObjectTransformerType.Default,
                to: 'id'
            },
            roomName: {
                type: ObjectTransformerType.Default,
                to: 'roomName'
            },
            status: {
                type: ObjectTransformerType.Function,
                to: 'status',
                transform: (val) => deserializeFunctionSpaceType[val]
            }
        }));
    }

    serialize(data: FunctionSpaceModel): any {
        return this.utilsService.transformInto(data, {
            name: {
                type: ObjectTransformerType.Default,
                to: 'name'
            },
            roomName: {
                type: ObjectTransformerType.Default,
                to: 'roomName'
            },
            status: {
                type: ObjectTransformerType.Function,
                to: 'status',
                transform: (val) => serializeFunctionSpaceType[val]
            }
        });
    }

    resourceUrl(): string {
        return `${this.configService.config().API_URL}venues/event-spaces/function-spaces`;
    }

    queryByEventSpace(params: HttpResourceQueryParams, eventSpaceId: number): Promise<ResourceQueryResponse<FunctionSpaceModel>> {
        return this.queryByUrl(params, `${this.configService.config().API_URL}venues/event-spaces/${eventSpaceId}/function-spaces`);
    }

    addForEventSpace(model: FunctionSpaceModel, eventSpaceId: number): Promise<FunctionSpaceModel> {
        return this.addByUrl(model, `${this.configService.config().API_URL}venues/event-spaces/${eventSpaceId}/function-spaces`);
    }

    getAllFunctionSpacesByVenueId(venueId: number): Promise<FunctionSpaceModel[]> {
        return this.eventSpace.getAllByVenueId(venueId)
            .then((eventSpaces: EventSpaceModel[]) => eventSpaces.map((es: EventSpaceModel) => es.id))
            .then((eventSpaceIds) => Promise.all(eventSpaceIds.map((esId: number) => {
                const params = this.getAllQueryObject();
                return this.queryByEventSpace(params, esId);
            })))
            .then((resp: ResourceQueryResponse<FunctionSpaceModel>[]) => resp.map(r => r.items))
            .then((resp: FunctionSpaceModel[][]) => resp.reduce( (acc, cur) => acc.concat(cur), []));
    }

}
