import { Injectable } from '@angular/core';
import { HttpCachableResource } from '../../classes/http-cachable-resource';
import { VenueEventModel, VenueEventStatus, VenueEventType } from '../../models/venue-event-model';
import { HttpClient } from '@angular/common/http';
import { AppCache } from '../../interfaces/cache';
import { UtilsService } from '../utils.service';
import { ObjectTransformerType } from '../../enums/object-transformer-type.enum';
import { ConfigService } from '../config.service';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';
import { HttpResource } from '../../classes/http-resource';

const serializeType = {};
serializeType[VenueEventType.Local] = 'LOCAL';
serializeType[VenueEventType.Group] = 'GROUP';
serializeType[VenueEventType.Internal] = 'INTERNAL';

const deserializeType = {};
deserializeType['LOCAL'] = VenueEventType.Local;
deserializeType['GROUP'] = VenueEventType.Group;
deserializeType['INTERNAL'] = VenueEventType.Internal;

const deserializeStatus = {};
deserializeStatus['LOCKED'] = VenueEventStatus.Inactive;
deserializeStatus['ACTIVE'] = VenueEventStatus.Active;
deserializeStatus['DEFINITIVE'] = VenueEventStatus.Definite;
deserializeStatus['TENTATIVE'] = VenueEventStatus.Tenative;
deserializeStatus['PROPOSAL'] = VenueEventStatus.Proposal;

const serializeStatus = {};
serializeStatus[VenueEventStatus.Inactive] = 'LOCKED';
serializeStatus[VenueEventStatus.Active] = 'ACTIVE';
serializeStatus[VenueEventStatus.Definite] = 'DEFINITIVE';
serializeStatus[VenueEventStatus.Tenative] = 'TENTATIVE';
serializeStatus[VenueEventStatus.Proposal] = 'PROPOSAL';

@Injectable()
export class VenueEventResourceService extends HttpResource<VenueEventModel> {

    constructor(client: HttpClient,
                cache: AppCache,
                private utils: UtilsService,
                private config: ConfigService) {
        super(client);
    }

    get apiUrl() {
        return this.config.config().API_URL;
    }

    deserialize(data: any): VenueEventModel {
        const model = new VenueEventModel(this.utils.transformInto(data, {
            id: { to: 'id' },
            venueId: { to: 'venueId' },
            accountId: { to: 'accountId' },
            name: { to: 'name' },
            code: { to: 'code' },
            masterAccountNumber: { to: 'masterAccountNumber' },
            eventDescription: { to: 'eventDescription' },
            functionCount: { to : 'functionCount'},
            eventStartDate: { to : 'eventStartDate'},
            eventEndDate: { to : 'eventEndDate'},
            type: {
                to: 'type',
                type: ObjectTransformerType.Function,
                transform: (val) => deserializeType[val]
            },
            status: {
                to: 'status',
                type: ObjectTransformerType.Function,
                transform: (val) => deserializeStatus[val]
            }
        }));

        if (data.client) {
            model.clientName = data.client.name;
            model.clientId = data.client.id;
        }

        if (Array.isArray(data.eventFunctions)) {
            model.functionCount = data.eventFunctions.length;
        }

        if (data.client && data.client.primaryContact) {
            const { firstName, lastName } = data.client.primaryContact;
            model.clientPrimaryContactName = `${firstName} ${lastName}`;
        }

        return model;
    }

    serialize(data: VenueEventModel): any {
        return this.utils.transformInto(data, {
            venueId: { to: 'venueId' },
            accountId: { to: 'accountId' },
            name: { to: 'name' },
            code: { to: 'code' },
            masterAccountNumber: { to: 'masterAccountNumber' },
            eventDescription: { to: 'eventDescription' },
            type: {
                to: 'type',
                type: ObjectTransformerType.Function,
                transform: (val) => serializeType[val]
            },
            status: {
                to: 'status',
                type: ObjectTransformerType.Function,
                transform: (val) => {
                    console.log(val, serializeStatus[val]);
                    return serializeStatus[val];
                }
            }
        });
    }

    resourceUrl(): string {
        return `${this.apiUrl}venues/events`;
    }

    queryByVenueId(params: HttpResourceQueryParams, venueId: number) {
     let start = params.otherParams.start
        params.otherParams = {
            venueId,
             start            
        };
        
        return this.queryByUrl(params, this.resourceUrl());
    }

    queryByAccountId(params: HttpResourceQueryParams, accountId: number, venueId: number) {
        params.otherParams = {
            accountId
        };
        return this.queryByUrl(params, this.resourceUrl());
    }

    getAllByAccountId(accountId: number, venueId: number): Promise<VenueEventModel[]> {
        const params = this.getAllQueryObject();
        params.otherParams = {
            accountId,
            venueId
        };
        return this.queryByUrl(params, this.resourceUrl())
            .then((resp: ResourceQueryResponse<VenueEventModel>) => resp.items);
    }

    getAllByVenueId(venueId: number): Promise<VenueEventModel[]> {
        const params = this.getAllQueryObject();
        params.otherParams = {venueId};
        return this.queryByUrl(params, this.resourceUrl())
            .then((resp: ResourceQueryResponse<VenueEventModel>) => resp.items);
    }

    queryAllOfTodayEventsByVenueId(venueId: number, search: string = ''): Promise<VenueEventModel[]> {
        const params: any = { venueId };
        if (search) {
            params.search = search;
        }
        return this.httpClient.get<VenueEventModel[]>(`${this.resourceUrl()}/today`, { params })
            .toPromise()
            .then(events => events.map(event => this.deserialize(event)));
    }

}
