import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { VendorModel } from '../../models/vendor-model';
import { HttpClient } from '@angular/common/http';
import { UtilsService } from '../utils.service';
import { ObjectTransformerType } from '../../enums/object-transformer-type.enum';
import { VendorStatus } from '../../enums/vendor-status.enum';
import { ConfigService } from '../config.service';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';

const serializeStatus = {};
serializeStatus[VendorStatus.Active] = 'ACTIVE';
serializeStatus[VendorStatus.Inactive] = 'LOCKED';

const deserializeStatus = {};
deserializeStatus['ACTIVE'] = VendorStatus.Active;
deserializeStatus['LOCKED'] = VendorStatus.Inactive;

@Injectable()
export class VendorResourceService extends HttpResource<VendorModel> {

    constructor(httpClient: HttpClient,
                private utilsService: UtilsService,
                private config: ConfigService) {
        super(httpClient);
    }

    deserialize(data: any): VendorModel {
        return new VendorModel(this.utilsService.transformInto(data, {
            id: {
                type: ObjectTransformerType.Default,
                to: 'id'
            },
            name: {
                type: ObjectTransformerType.Default,
                to: 'name'
            },
            status: {
                type: ObjectTransformerType.Function,
                to: 'status',
                transform: (val) => deserializeStatus[val]
            },
            address: {
                type: ObjectTransformerType.Default,
                to: 'address'
            },
            phone: {
                type: ObjectTransformerType.Default,
                to: 'phone'
            },
            fax: {
                type: ObjectTransformerType.Default,
                to: 'fax'
            },
            website: {
                type: ObjectTransformerType.Default,
                to: 'website'
            },
            subscriberId: {
                type: ObjectTransformerType.Default,
                to: 'subscriberId'
            }
        }));
    }

    serialize(data: VendorModel): any {
        return this.utilsService.transformInto(data, {
            status: {
                type: ObjectTransformerType.Function,
                to: 'status',
                transform: (val) => serializeStatus[val]
            },
            address: {
                type: ObjectTransformerType.Default,
                to: 'address'
            },
            name: {
                type: ObjectTransformerType.Default,
                to: 'name'
            },
            phone: {
                type: ObjectTransformerType.Default,
                to: 'phone'
            },
            fax: {
                type: ObjectTransformerType.Default,
                to: 'fax'
            },
            website: {
                type: ObjectTransformerType.Default,
                to: 'website'
            }
        });
    }

    resourceUrl(): string {
        return `${this.config.config().API_URL}subscribers/vendors`;
    }

    addForSubscriber(model: VendorModel, subscriberId: number): Promise<VendorModel> {
        return this.addByUrl(model, `${this.config.config().API_URL}subscribers/${subscriberId}/vendors`);
    }

    queryBySubscriber(params: HttpResourceQueryParams, subscriberId: number): Promise<ResourceQueryResponse<VendorModel>> {
        return this.queryByUrl(params, `${this.config.config().API_URL}subscribers/${subscriberId}/vendors`);
    }

    getAllBySubscriber(subscriberId: number): Promise<VendorModel[]> {
        const url = `${this.config.config().API_URL}subscribers/${subscriberId}/vendors`;
        return this.queryByUrl(this.getAllQueryObject(), url).then((resp) => resp.items);
    }

}
