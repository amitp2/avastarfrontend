import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { InventoryDocument } from '../../models/inventory-document';
import { HttpClient } from '@angular/common/http';
import { UtilsService } from '../utils.service';
import { ConfigService } from '../config.service';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';

@Injectable()
export class InventoryDocumentResourceService extends HttpResource<InventoryDocument> {

    constructor(httpClient: HttpClient,
                private utils: UtilsService,
                private config: ConfigService) {
        super(httpClient);
    }

    deserialize(data: any): InventoryDocument {
        return new InventoryDocument(this.utils.transformInto(data, {
            id: {to: 'id'},
            name: {to: 'name'},
            url: {to: 'url'},
            fileName: {to: 'fileName'}
        }));
    }

    serialize(data: InventoryDocument): any {
        return this.utils.transformInto(data, {
            name: {to: 'name'},
            url: {to: 'url'}
        });
    }

    resourceUrl(): string {
        return `${this.config.config().API_URL}venues/inventories/documents`;
    }

    queryByInventory(params: HttpResourceQueryParams, inventoryId: number): Promise<ResourceQueryResponse<InventoryDocument>> {
        return this.queryByUrl(params, `${this.config.config().API_URL}venues/inventories/${inventoryId}/documents`);
    }

    addForInventory(model: InventoryDocument, inventoryId: number) {
        return this.addByUrl(model, `${this.config.config().API_URL}venues/inventories/${inventoryId}/documents`);
    }

}
