import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { PurchaseOrder } from '../../modules/route-manage-purchase-orders/types';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';

@Injectable()
export class PurchaseOrderService extends HttpResource<PurchaseOrder> {
    get apiUrl() {
        return this.config.config().API_URL;
    }

    constructor(http: HttpClient, private config: ConfigService) {
        super(http);
    }

    deserialize(data: any): PurchaseOrder {
        return PurchaseOrder.fromJson(data);
    }

    serialize(data: PurchaseOrder) {
        return data.toJson();
    }

    resourceUrl(): string {
        return `${this.apiUrl}venues/orders`;
    }

    addByVenueId(venueId: number, model: PurchaseOrder) {
        return this.addByUrl(model, `${this.apiUrl}venues/${venueId}/orders`);
    }

    getAllByVenueId(venueId: number, otherParams?: any): Promise<PurchaseOrder[]> {
        const params = this.getAllQueryObject();
        if (otherParams) {
            params.otherParams = otherParams;
        }
        return this.queryByVenueId(params, venueId).then(resp => resp.items);
    }

    queryByVenueId(params: HttpResourceQueryParams, venueId: number): Promise<ResourceQueryResponse<PurchaseOrder>> {       
        return this.queryByUrl(params, `${this.apiUrl}venues/${venueId}/orders`);
    }
}
