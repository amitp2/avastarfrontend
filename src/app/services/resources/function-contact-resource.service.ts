import { Injectable } from '@angular/core';
import { HttpCachableResource } from '../../classes/http-cachable-resource';
import {
    CommandFunctionContactTypeMapper, FunctionContactModel, FunctionContactType
} from '../../models/function-contact-model';
import { AppCache } from '../../interfaces/cache';
import { HttpClient } from '@angular/common/http';
import { ResourceConstructorService } from '../resource-constructor/resource-constructor.service';
import { ConfigService } from '../config.service';
import { HttpResource } from '../../classes/http-resource';

@Injectable()
export class FunctionContactResourceService extends HttpResource<FunctionContactModel> {

    private resourceTypeMapper: CommandFunctionContactTypeMapper;

    get apiUrl(): string {
        return this.config.config().API_URL;
    }

    constructor(httpClient: HttpClient,
                private resourceConstructor: ResourceConstructorService,
                private config: ConfigService) {
        super(httpClient);
        this.resourceTypeMapper = new CommandFunctionContactTypeMapper();
    }

    deserialize(data: any): FunctionContactModel {
        return this.resourceConstructor.build(data, FunctionContactModel);
    }

    serialize(data: FunctionContactModel): any {
        return null;
    }

    resourceUrl(): string {
        return `${this.apiUrl}venues/events/functions/contacts`;
    }

    async searchContact(name: string, type: FunctionContactType): Promise<FunctionContactModel[]> {
        const params = this.getAllQueryObject();
        params.otherParams = {
            search: name,
            type: this.resourceTypeMapper.map(type)
        };
        const res = await this.queryByUrl(params, this.resourceUrl());
        return res.items;
    }

    async searchPlanningContact(name: string): Promise<FunctionContactModel[]> {
        const res = await this.searchContact(name, FunctionContactType.Planning);
        return res;
    }

    async searchSalesContact(name: string): Promise<FunctionContactModel[]> {
        const res = await this.searchContact(name, FunctionContactType.Sales);
        return res;
    }

}
