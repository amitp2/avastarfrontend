import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { StateModel } from '../../models/state-model';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';

@Injectable()
export class StateResourceService extends HttpResource<StateModel> {

    constructor(httpClient: HttpClient, private configService: ConfigService) {
        super(httpClient);
    }

    deserialize(data: any): StateModel {
        return new StateModel(data);
    }

    serialize(data: StateModel): any {
        throw new Error('method not implemented');
    }

    resourceUrl(): string {
        return `${this.configService.config().API_URL}utils/states`;
    }

    get(id: number): Promise<StateModel> {
        return new Promise<StateModel>((resolve, reject) => {

            this.query({
                page: 0,
                size: 1000,
                sortFields: [],
                sortAsc: true
            })
                .then((resp: ResourceQueryResponse<StateModel>) => {
                    const filtered = resp.items.filter(state => state.id === id);
                    if (filtered.length === 0) {
                        reject();
                        return;
                    }
                    resolve(filtered[0]);
                })
                .catch(() => {
                    reject();
                });

        });
    }

}
