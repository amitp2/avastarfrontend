import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { UserModel } from '../../models/user-model';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { UserStatus } from '../../enums/user-status.enum';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';
import { UtilsService } from '../utils.service';
import { ProductType } from '../../enums/product-type.enum';
import { UserProductResourceService } from './user-product-resource.service';

@Injectable()
export class UserResourceService extends HttpResource<UserModel> {

    constructor(httpClient: HttpClient,
                private configService: ConfigService,
                private utilsService: UtilsService,
                private userProductResource: UserProductResourceService) {
        super(httpClient);
    }

    deserialize(data: any): UserModel {
        const model = new UserModel(data);

        model.roles = data.roles.map(this.utilsService.stringToUserRole);

        switch (data.status) {
            case 'ACTIVE':
                model.status = UserStatus.Active;
                break;
            case 'TOO_MANY_FAILED_LOGIN':
                model.status = UserStatus.Blocked;
                break;
            case 'EMAIL_NOT_VERIFIED':
                model.status = UserStatus.NotVerified;
                break;
            default:
                model.status = UserStatus.Inactive;
                break;
        }

        // model.status = data.status === 'ACTIVE' ? UserStatus.Active : UserStatus.Inactive;
        return model;
    }

    serialize(data: UserModel) {
        let serialized: any;

        serialized = {
            ...data
        };
        serialized['roles'] = data.roles.map(this.utilsService.userRoleToString);

        switch (data.status) {
            case UserStatus.Active:
                serialized['status'] = 'ACTIVE';
                break;
            case UserStatus.Blocked:
                serialized['status'] = 'TOO_MANY_FAILED_LOGIN';
                break;
            case UserStatus.NotVerified:
                serialized['status'] = 'EMAIL_NOT_VERIFIED';
                break;
            default:
                serialized['status'] = 'LOCKED';
        }

        // serialized['status'] = data.status === UserStatus.Active ? 'ACTIVE' : 'LOCKED';
        // console.log(data);
        delete serialized.role;
        delete serialized.subscriberName;

        return serialized;
    }

    resourceUrl(): string {
        return `${this.configService.config().API_URL}users`;
    }

    bySubscriber(subscriberId: number, params: HttpResourceQueryParams): Promise<ResourceQueryResponse<UserModel>> {
        return this.queryByUrl(params, `${this.resourceUrl()}/subscribers/${subscriberId}`);
    }

    async releaseProducts(user: UserModel) {
        if (user.hasProduct(ProductType.Aavastar)) {
            await this.userProductResource.delete(user.productId(ProductType.Aavastar));
        }
    }

}
