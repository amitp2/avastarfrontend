import { Injectable } from '@angular/core';
import { InventoryCategoryModel } from '../../models/inventory-category-model';
import { HttpClient } from '@angular/common/http';
import { UtilsService } from '../utils.service';
import { ConfigService } from '../config.service';
import { ObjectTransformerType } from '../../enums/object-transformer-type.enum';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';
import { HttpCachableResource } from '../../classes/http-cachable-resource';
import { AppCache } from '../../interfaces/cache';
import { InventorySubcategoryItemModel } from '../../models/inventory-subcategory-item-model';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { InventorySubCategoryModel } from '../../models/inventory-sub-category-model';

@Injectable()
export class InventoryCategoryResourceService extends HttpCachableResource<InventoryCategoryModel
    | InventorySubCategoryModel
    | InventorySubcategoryItemModel
    | any> {

    constructor(
        httpClient: HttpClient,
        private utilsService: UtilsService,
        private configService: ConfigService,
        private ch: AppCache
    ) {
        super(httpClient, ch);
    }

    get apiUrl(): string {
        return this.configService.config().API_URL;
    }

    deserialize(data: any): any {
        if (data.subCategoryId) {
            return InventorySubcategoryItemModel.fromJson(data);
        }
        if (data.categoryId) {
            return InventorySubCategoryModel.fromJson(data);
        }
        return new InventoryCategoryModel(this.utilsService.transformInto(data, {
            id: {
                to: 'id',
                type: ObjectTransformerType.Default
            },
            name: {
                to: 'name',
                type: ObjectTransformerType.Default
            }
        }));
    }

    serialize(data: InventoryCategoryModel): any {
        return data.toJson();
    }

    resourceUrl(): string {
        return `${this.configService.config().API_URL}venues/inventories/categories`;
    }

    queryResponseDeserializer(resp: any) {
        if (resp && resp.hasOwnProperty('totalElements')) {
            return {
                total: resp.totalElements,
                items: resp.content.map(item => this.deserialize(item))
            };
        }
        return {
            total: resp.length,
            items: resp.map(item => this.deserialize(item))
        };
    }


    getAllSubcategories(categoryId: number): Promise<InventoryCategoryModel[]> {
        const url = `${this.configService.config().API_URL}venues/inventories/categories/${categoryId}/subcategories`;
        return this.queryByUrl(this.getAllQueryObject(), url)
            .then((resp: ResourceQueryResponse<InventoryCategoryModel>) => {
                return resp.items;
            });
    }

    getAllSubcategoryItems(subCategoryId: number): Promise<InventorySubcategoryItemModel[]> {
        const url = `${this.apiUrl}venues/inventories/categories/subcategories/${subCategoryId}/subcategoryItems`;

        return this.queryByUrl(this.getAllQueryObject(), url)
            .then((resp: ResourceQueryResponse<any>) => {
                return resp.items.map((item: any) => new InventorySubcategoryItemModel({
                    name: item.name,
                    id: item.id,
                    subCategoryId: item.subCategoryId
                }));
            });
    }

    queryForTable(params: HttpResourceQueryParams) {
        this.clearCache();
        return this.queryByUrl(params, `${this.apiUrl}venues/inventories/categories/pageable`);
    }

    addSubCategory(model: InventorySubCategoryModel) {
        return this.addByUrl(model, `${this.apiUrl}venues/inventories/categories/${model.categoryId}/subcategories`);
    }

    addSubCategoryItem(model: InventorySubcategoryItemModel) {
        return this.addByUrl(model, `${this.apiUrl}venues/inventories/categories/subcategories/${model.subCategoryId}/subcategoryItems`);
    }

    editSubCategory(id: number, model: InventorySubCategoryModel) {
        return this.editByUrl(model, `${this.apiUrl}venues/inventories/categories/subcategories/${id}`);
    }

    editSubCategoryItem(id: number, model: InventorySubcategoryItemModel) {
        return this.editByUrl(model, `${this.apiUrl}venues/inventories/categories/subcategories/subcategoryItems/${id}`);
    }

    getSubCategory(id: number): Promise<InventorySubCategoryModel> {
        return this.getByUrl(`${this.apiUrl}venues/inventories/categories/subcategories/${id}`) as Promise<InventorySubCategoryModel>;
    }

    getSubCategoryItem(id: number): Promise<InventorySubcategoryItemModel> {
        return this.getByUrl(`${this.apiUrl}venues/inventories/categories/subcategories/subcategoryItems/${id}`) as Promise<InventorySubcategoryItemModel>;
    }

    deleteSubCategory(id: number) {
        return this.httpClient.delete(`${this.apiUrl}venues/inventories/categories/subcategories/${id}`).toPromise();
    }

    deleteSubCategoryItem(id: number) {
        return this.httpClient.delete(`${this.apiUrl}venues/inventories/categories/subcategories/subcategoryItems/${id}`).toPromise();
    }
}
