import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { VendorContactModel } from '../../models/vendor-contact-model';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { UtilsService } from '../utils.service';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';

@Injectable()
export class VendorContactResourceService extends HttpResource<VendorContactModel> {

    constructor(httpClient: HttpClient,
                private configService: ConfigService,
                private utilsService: UtilsService) {
        super(httpClient);
    }

    deserialize(data: any): VendorContactModel {
        const model = new VendorContactModel(data);
        model.role = this.utilsService.stringToUserRole(data.role);
        return model;
    }

    serialize(data: VendorContactModel): any {
        let serialized: any;
        serialized = {
            ...data
        };
        delete serialized.vendorId;
        serialized['role'] = this.utilsService.userRoleToString(data.role);
        return serialized;
    }

    resourceUrl(): string {
        return `${this.configService.config().API_URL}subscribers/vendors/contacts`;
    }

    queryByVendorId(params: HttpResourceQueryParams, vendorId: number): Promise<ResourceQueryResponse<VendorContactModel>> {
        return this.queryByUrl(params, `${this.configService.config().API_URL}subscribers/vendors/${vendorId}/contacts`);
    }

    addForVendor(model: VendorContactModel, vendorId: number): Promise<VendorContactModel> {
        return this.addByUrl(model, `${this.configService.config().API_URL}subscribers/vendors/${vendorId}/contacts`);
    }

    getAllByVendorId(vendorId: number): Promise<VendorContactModel[]> {
        return this.queryByUrl(this.getAllQueryObject(), `${this.configService.config().API_URL}subscribers/vendors/${vendorId}/contacts`)
            .then((resp: ResourceQueryResponse<VendorContactModel>) => resp.items);
    }

}
