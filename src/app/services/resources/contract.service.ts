import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { ContractModel } from '../../modules/route-contract-management/types';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';

@Injectable()
export class ContractService extends HttpResource<ContractModel> {

    constructor(http: HttpClient, private config: ConfigService) {
        super(http);
    }

    get apiUrl() {
        return this.config.config().API_URL;
    }

    deserialize(data: any): ContractModel {
        return ContractModel.fromJson(data);
    }

    serialize(data: ContractModel) {
        return data.toJson();
    }

    resourceUrl(): string {
        return this.apiUrl + 'subscribers/vendors/contracts';
    }

    addByVendorId(vendorId: number, model: ContractModel) {
        return this.addByUrl(model, `${this.apiUrl}subscribers/vendors/${vendorId}/contracts`);
    }

    queryBySubscriberId(params: HttpResourceQueryParams, subscriberId: number) {
        return this.queryByUrl(params, `${this.apiUrl}subscribers/${subscriberId}/vendors/contracts`);
    }
}
