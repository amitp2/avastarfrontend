import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { VenueModel } from '../../models/venue-model';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';

@Injectable()
export class VenueResourceService extends HttpResource<VenueModel> {

    constructor(httpClient: HttpClient,
                private configService: ConfigService) {
        super(httpClient);
    }

    deserialize(data: any): VenueModel {
        const model = new VenueModel(data);
        return model;
    }

    serialize(data: VenueModel) {
        let serialized: any;
        serialized = {
            ...data
        };
        return serialized;
    }

    resourceUrl(): string {
        return `${this.configService.config().API_URL}venues`;
    }

    updateOwner(venueId: number, ownerId: number) {
        return this.httpClient.post(`${this.resourceUrl()}/${venueId}/owner`, { ownerId }).toPromise();
    }

    updateOperator(venueId: number, operatorId: number) {
        return this.httpClient.post(`${this.resourceUrl()}/${venueId}/operator`, { operatorId }).toPromise();
    }
}
