import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { EquipmentSystemModel } from '../../models/equipment-system-model';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { UtilsService } from '../utils.service';
import { ObjectTransformer } from '../../interfaces/object-transformer';
import { HttpResourceQueryParams } from '../../interfaces/http-resource-query-params';
import { ResourceQueryResponse } from '../../interfaces/resource-query-response';
import { ObjectTransformerType } from '../../enums/object-transformer-type.enum';
import {EquipmentModel} from '../../models/equipment-model';

@Injectable()
export class EquipmentSystemResourceService extends HttpResource<EquipmentSystemModel> {

    get apiUrl(): string {
        return this.config.config().API_URL;
    }

    get serializeTransformer(): ObjectTransformer {
        return {
            name: {to: 'name'},
            description: {to: 'description'},
            inventoryIds: {to: 'inventoryIds'}
        };
    }

    get deserializeTransformer(): ObjectTransformer {
        return {
            id: {to: 'id'},
            name: {to: 'name'},
            description: {to: 'description'},
            inventories: {
                to: 'inventoryIds',
                type: ObjectTransformerType.Function,
                transform: val => val.map(v => v.id)
            }
        };
    }

    constructor(httpClient: HttpClient,
                private config: ConfigService,
                private utils: UtilsService) {
        super(httpClient);
    }

    deserialize(data: any): EquipmentSystemModel {
        
        return new EquipmentSystemModel(this.utils.transformInto(data, this.deserializeTransformer));
    }

    serialize(data: EquipmentSystemModel): any {
        return this.utils.transformInto(data, this.serializeTransformer);
    }

    resourceUrl(): string {
        return `${this.apiUrl}venues/systems`;
    }

    addForVenue(model: EquipmentSystemModel, venueId: number): Promise<EquipmentSystemModel> {
        return this.addByUrl(model, `${this.apiUrl}venues/${venueId}/systems`);
    }

    queryByVenueId(params: HttpResourceQueryParams, venueId: number): Promise<ResourceQueryResponse<EquipmentSystemModel>> {
        return this.queryByUrl(params, `${this.apiUrl}venues/${venueId}/systems`);
    }

    getAllByVenueId(venueId: number, queryParams = {}): Promise<EquipmentSystemModel[]> {
        const params = this.getAllQueryObject();
        params.otherParams = queryParams;
        return this.queryByUrl(params, `${this.apiUrl}venues/${venueId}/systems`)
            .then((resp: ResourceQueryResponse<EquipmentSystemModel>) => resp.items);
    }

    getManyByVenueId(ids: number[], venueId: number): Promise<EquipmentSystemModel[]> {
        return this.getManyByUrl(ids, `${this.apiUrl}venues/${venueId}/systems`);
    }

}
