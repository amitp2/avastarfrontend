import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { InventoryAssetStatusModel } from '../../models/inventory-asset-status-model';
import { HttpClient } from '@angular/common/http';
import { UtilsService } from '../utils.service';
import { ConfigService } from '../config.service';
import { InventoryAssetRating } from '../../enums/inventory-asset-rating.enum';
import { InventoryAssetStatus } from '../../enums/inventory-asset-status.enum';
import { ObjectTransformerType } from '../../enums/object-transformer-type.enum';

@Injectable()
export class InventoryAssetStatusResourceService extends HttpResource<InventoryAssetStatusModel> {

    private readonly ratingSerializer = {};
    private readonly ratingDeSerializer = {};
    private readonly statusSerializer = {};
    private readonly statusDeSerializer = {};

    constructor(httpClient: HttpClient,
                private utils: UtilsService,
                private config: ConfigService) {
        super(httpClient);

        this.ratingSerializer[InventoryAssetRating.Acceptable] = 'ACCEPTABLE';
        this.ratingSerializer[InventoryAssetRating.SubStandard] = 'SUB_STANDARD';
        this.ratingSerializer[InventoryAssetRating.Excellent] = 'EXCELLENT';

        this.ratingDeSerializer['ACCEPTABLE'] = InventoryAssetRating.Acceptable;
        this.ratingDeSerializer['SUB_STANDARD'] = InventoryAssetRating.SubStandard;
        this.ratingDeSerializer['EXCELLENT'] = InventoryAssetRating.Excellent;

        this.statusSerializer[InventoryAssetStatus.Active] = 'ACTIVE';
        this.statusSerializer[InventoryAssetStatus.AtRepair] = 'AT_REPAIR';
        this.statusSerializer[InventoryAssetStatus.Broken] = 'BROKEN';
        this.statusSerializer[InventoryAssetStatus.Missing] = 'MISSING';
        this.statusSerializer[InventoryAssetStatus.Removed] = 'REMOVED';
        this.statusSerializer[InventoryAssetStatus.ServiceRequired] = 'SERVICE_REQUIRED';

        this.statusDeSerializer['ACTIVE'] = InventoryAssetStatus.Active;
        this.statusDeSerializer['AT_REPAIR'] = InventoryAssetStatus.AtRepair;
        this.statusDeSerializer['BROKEN'] = InventoryAssetStatus.Broken;
        this.statusDeSerializer['MISSING'] = InventoryAssetStatus.Missing;
        this.statusDeSerializer['REMOVED'] = InventoryAssetStatus.Removed;
        this.statusDeSerializer['SERVICE_REQUIRED'] = InventoryAssetStatus.ServiceRequired;
    }

    deserialize(data: any): InventoryAssetStatusModel {
        return new InventoryAssetStatusModel(this.utils.transformInto(data, {
            comment: {
                to: 'comment'
            },
            rating: {
                to: 'rating',
                type: ObjectTransformerType.Function,
                transform: (val) => this.ratingDeSerializer[val]
            },
            condition: {
                to: 'condition',
                type: ObjectTransformerType.Function,
                transform: (val) => this.statusDeSerializer[val]
            }
        }));
    }

    serialize(data: InventoryAssetStatusModel): any {
        return this.utils.transformInto(data, {
            comment: {
                to: 'comment'
            },
            rating: {
                to: 'rating',
                type: ObjectTransformerType.Function,
                transform: (val) => this.ratingSerializer[val]
            },
            condition: {
                to: 'condition',
                type: ObjectTransformerType.Function,
                transform: (val) => this.statusSerializer[val]
            }
        });
    }

    resourceUrl(): string {
        return `${this.config.config().API_URL}venues/inventories/conditions`;
    }

    getByInventoryId(inventoryId: number): Promise<InventoryAssetStatusModel> {
        return this.getByUrl(`${this.config.config().API_URL}venues/inventories/${inventoryId}/conditions/active`);
    }

    updateForInventory(model: InventoryAssetStatusModel, inventoryId: number): Promise<InventoryAssetStatusModel> {
        return this.editByUrl(model, `${this.config.config().API_URL}venues/inventories/${inventoryId}/conditions`);
    }

}
