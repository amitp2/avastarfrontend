import {Injectable} from '@angular/core';
import {HttpCachableResource} from '../../classes/http-cachable-resource';
import {VenueClientModel} from '../../models/venue-client-model';
import {HttpClient} from '@angular/common/http';
import {AppCache} from '../../interfaces/cache';
import {ConfigService} from '../config.service';
import {VenueClientType} from '../../enums/venue-client-type.enum';
import {VenueClientStatus} from '../../enums/venue-client-status.enum';
import {HttpResourceQueryParams} from '../../interfaces/http-resource-query-params';
import {ResourceQueryResponse} from '../../interfaces/resource-query-response';
import {
    FunctionCalculationTypeToCommandCalculationType,
    ResourceCalculationTypeToFunctionCalculationType
} from '../../models/function-model';

@Injectable()
export class VenueClientResourceService extends HttpCachableResource<VenueClientModel> {

    constructor(client: HttpClient,
                cache: AppCache,
                private config: ConfigService) {
        super(client, cache);
    }

    get apiUrl(): string {
        return this.config.config().API_URL;
    }

    deserialize(data: any): VenueClientModel {
        const model = new VenueClientModel();
        model.id = +data.id;
        model.name = data.name;
        model.accountType = data.accountType === 'CLIENT' ? VenueClientType.Client : VenueClientType.House;
        model.status = data.status === 'LOCKED' ? VenueClientStatus.Locked : VenueClientStatus.Active;
        model.countryId = data.countryId;
        model.stateId = data.stateId;
        model.city = data.city;
        model.address = data.address;
        model.address2 = data.address2;
        model.postCode = data.postCode;
        model.phone = data.phone;
        model.fax = data.fax;
        model.branch = data.branch;
        model.website = data.website;
        model.logoUrl = data.logoUrl;
        model.masterAgreementAgreed = data.masterAgreementAgreed;
        model.venueId = data.venueId;
        model.alternativeServiceChargePercentage = data.alternativeServiceChargePercentage;

        model.standardDiscountRate = data.standardDiscountRate;
        model.discountRateForSetupDays = data.discountRateForSetupDays;
        model.alternativeServiceCharge = data.alternativeServiceCharge;
        model.taxExempt = data.taxExempt;
        model.serviceChargeCalculation = (new ResourceCalculationTypeToFunctionCalculationType()).map(data.serviceChargeCalculation);

        return model;
    }

    serialize(data: VenueClientModel): any {
        const serialized = {};

        serialized['status'] = data.status === VenueClientStatus.Locked ? 'LOCKED' : 'ACTIVE';
        serialized['name'] = data.name;
        serialized['accountType'] = data.accountType === VenueClientType.Client ? 'CLIENT' : 'HOUSE';
        serialized['countryId'] = data.countryId;
        serialized['stateId'] = data.stateId;
        serialized['city'] = data.city;
        serialized['address'] = data.address;
        serialized['address2'] = data.address2;
        serialized['postCode'] = data.postCode;
        serialized['phone'] = data.phone;
        serialized['fax'] = data.fax;
        serialized['branch'] = data.branch;
        serialized['website'] = data.website;
        serialized['logoUrl'] = data.logoUrl;
        serialized['masterAgreementAgreed'] = data.masterAgreementAgreed;
        serialized['venueId'] = data.venueId;

        serialized['standardDiscountRate'] = data.standardDiscountRate;
        serialized['discountRateForSetupDays'] = data.discountRateForSetupDays;
        serialized['alternativeServiceCharge'] = data.alternativeServiceCharge;
        serialized['taxExempt'] = data.taxExempt;
        serialized['serviceChargeCalculation'] = (new FunctionCalculationTypeToCommandCalculationType()).map(data.serviceChargeCalculation);
        serialized['alternativeServiceChargePercentage'] = data.alternativeServiceChargePercentage;

        return serialized;
    }

    resourceUrl(): string {
        return `${this.apiUrl}venues/clients`;
    }

    queryBuVenueId(params: HttpResourceQueryParams, venueId: number): Promise<ResourceQueryResponse<VenueClientModel>> {
        params.otherParams = {
            venueId
        };
        return this.queryByUrl(params, this.resourceUrl());
    }

    getAllByVenueID(venueId: number): Promise<VenueClientModel[]> {
        const params = this.getAllQueryObject();
        params.otherParams = { venueId };
        return this.queryByUrl(params, this.resourceUrl())
            .then((resp) => resp.items);
    }

}
