import { Injectable } from '@angular/core';
import { HttpResource } from '../../classes/http-resource';
import { InventoryMaintenanceModel } from '../../models/inventory-maintenance-model';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { UtilsService } from '../utils.service';
import { ObjectTransformerType } from '../../enums/object-transformer-type.enum';
import { InventoryMaintenanceSchedule } from '../../enums/inventory-maintenance-schedule.enum';


@Injectable()
export class InventoryMaintenanceResourceService extends HttpResource<InventoryMaintenanceModel> {

    private readonly scheduleSerializer = {};
    private readonly scheduleDeSerializer = {};

    constructor(httpClient: HttpClient,
                private config: ConfigService,
                private utils: UtilsService) {
        super(httpClient);

        this.scheduleSerializer[InventoryMaintenanceSchedule.MONTHLY] = 'MONTHLY';
        this.scheduleSerializer[InventoryMaintenanceSchedule.QUARTERLY] = 'QUARTERLY';
        this.scheduleSerializer[InventoryMaintenanceSchedule.SEMI_ANNUAL] = 'SEMI_ANNUAL';
        this.scheduleSerializer[InventoryMaintenanceSchedule.ANNUAL] = 'ANNUAL';


        this.scheduleDeSerializer['MONTHLY'] = InventoryMaintenanceSchedule.MONTHLY;
        this.scheduleDeSerializer['QUARTERLY'] = InventoryMaintenanceSchedule.QUARTERLY;
        this.scheduleDeSerializer['SEMI_ANNUAL'] = InventoryMaintenanceSchedule.SEMI_ANNUAL;
        this.scheduleDeSerializer['ANNUAL'] = InventoryMaintenanceSchedule.ANNUAL;
    }

    deserialize(data: any): InventoryMaintenanceModel {
        return new InventoryMaintenanceModel(this.utils.transformInto(data, {
            warrantyExpDate: {to: 'warrantyExpDate'},
            warrantyVendorId: {to: 'warrantyVendorId'},
            serviceExpDate: {to: 'serviceExpDate'},
            serviceVendorId: {to: 'serviceVendorId'},
            lastServiceDate: {to: 'lastServiceDate'},
            nextServiceDate: {to: 'nextServiceDate'},
            schedule: {
                to: 'schedule',
                type: ObjectTransformerType.Function,
                transform: (val) => this.scheduleDeSerializer[val]
            }
        }));
    }

    serialize(data: InventoryMaintenanceModel) {
        return this.utils.transformInto(data, {
            warrantyExpDate: {to: 'warrantyExpDate'},
            warrantyVendorId: {to: 'warrantyVendorId'},
            serviceExpDate: {to: 'serviceExpDate'},
            serviceVendorId: {to: 'serviceVendorId'},
            lastServiceDate: {to: 'lastServiceDate'},
            nextServiceDate: {to: 'nextServiceDate'},
            schedule: {
                to: 'schedule',
                type: ObjectTransformerType.Function,
                transform: (val) => this.scheduleSerializer[val]
            }
        });
    }

    resourceUrl(): string {
        return ``;
    }

    getByInventoryId(inventoryId: number): Promise<InventoryMaintenanceModel> {
        return this.getByUrl(`${this.config.config().API_URL}venues/inventories/${inventoryId}/maintenance`);
    }

    updateforInventory(model: InventoryMaintenanceModel, inventoryId: number): Promise<InventoryMaintenanceModel> {
        return this.editByUrl(model, `${this.config.config().API_URL}venues/inventories/${inventoryId}/maintenance`);
    }

}
