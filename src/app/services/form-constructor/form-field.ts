import { objectDefaultsKey } from './default-value';
import { requiredFieldsKey } from './required';
import { MIN_FIELDS_KEY } from './min';
import { MAX_FIELDS_KEY } from './max';

export const formPropsKey = '_form_props_';
export const FORM_FIELD_VALIDATORS_KEY = '_FORM_FIELD_VALIDATORS_KEY_';

export function FormField(target: any, key: string) {

    if (!target[formPropsKey]) {
        target[formPropsKey] = [];
    }
    if (!target[objectDefaultsKey]) {
        target[objectDefaultsKey] = {};
    }

    if (!target[requiredFieldsKey]) {
        target[requiredFieldsKey] = {};
    }

    if (!target[MIN_FIELDS_KEY]) {
        target[MIN_FIELDS_KEY] = {};
    }

    if (!target[MAX_FIELDS_KEY]) {
        target[MAX_FIELDS_KEY] = {};
    }

    if (!target[FORM_FIELD_VALIDATORS_KEY]) {
        target[FORM_FIELD_VALIDATORS_KEY] = {};
    }

    target[formPropsKey].push(key);
}
