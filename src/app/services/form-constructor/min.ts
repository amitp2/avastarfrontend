export const MIN_FIELDS_KEY = '_MIN_FIELDS_KEY_';

export function Min(min: number) {
    return function (target: any, key: string) {

        if (!target[MIN_FIELDS_KEY]) {
            target[MIN_FIELDS_KEY] = {};
        }
        target[MIN_FIELDS_KEY][key] = min;
    };
}
