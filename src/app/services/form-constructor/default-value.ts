export const objectDefaultsKey = '_object_defaults_';

export function DefaultValue(value: any) {
    return function (target: any, key: string) {
        if (!target[objectDefaultsKey]) {
            target[objectDefaultsKey] = {};
        }
        target[objectDefaultsKey][key] = value;
    };

}
