import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FORM_FIELD_VALIDATORS_KEY } from './form-field';
import { MIN_FIELDS_KEY } from './min';
import { MAX_FIELDS_KEY } from './max';
import { isSet } from '../../helpers/helper-functions';

function buildForm(className: any): FormGroup {
    const classInstance = new className();
    const formGroupObject = {};

    classInstance._form_props_.forEach((key: string) => {
        const defaultValue = classInstance._object_defaults_[key] ? classInstance._object_defaults_[key] : null;
        const validators = [];

        if (classInstance._object_required_fields_[key]) {
            validators.push(Validators.required);
        }

        if (isSet(classInstance[MIN_FIELDS_KEY][key])) {
            validators.push(Validators.min(classInstance[MIN_FIELDS_KEY][key]));
        }

        if (classInstance[MAX_FIELDS_KEY][key]) {
            validators.push(Validators.max(classInstance[MAX_FIELDS_KEY][key]));
        }

        if (classInstance[FORM_FIELD_VALIDATORS_KEY][key]) {
            classInstance[FORM_FIELD_VALIDATORS_KEY][key].forEach((customValidator) => validators.push(customValidator));
        }

        formGroupObject[key] = new FormControl(defaultValue, validators);
    });

    return new FormGroup(formGroupObject);
}

function extractModelFromForm(formValues: any, modelClassName: any): any {
    const classInstance = new modelClassName();
    classInstance._form_props_.forEach((key: string) => {
        classInstance[key] = formValues[key];
    });
    return classInstance;
}

function putModelToForm(modelInstance: any): any {
    const formValues = {};

    modelInstance._form_props_.forEach((key: string) => {
        formValues[key] = modelInstance[key];
    });

    return formValues;
}

@Injectable()
export class FormConstructorService {

    static build(className: any): FormGroup {
        return buildForm(className);
    }

    static extractModel(formValues: any, modelClassName: any): any {
        return extractModelFromForm(formValues, modelClassName);
    }

    static putModel(modelInstance: any): any {
        return putModelToForm(modelInstance);
    }

    constructor() {
    }

    build(className: any): FormGroup {
        return buildForm(className);
    }

}
