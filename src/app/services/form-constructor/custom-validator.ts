import { FORM_FIELD_VALIDATORS_KEY } from './form-field';

export function CustomValidator(validatorFunction: any) {
    return function (target: any, key: string) {
        if (!target[FORM_FIELD_VALIDATORS_KEY]) {
            target[FORM_FIELD_VALIDATORS_KEY] = {};
        }

        if (!target[FORM_FIELD_VALIDATORS_KEY][key]) {
            target[FORM_FIELD_VALIDATORS_KEY][key] = [];
        }
        target[FORM_FIELD_VALIDATORS_KEY][key].push(validatorFunction);
    };
}
