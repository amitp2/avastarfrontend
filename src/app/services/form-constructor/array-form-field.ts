export function ArrayFormField(target: any, key: string) {
    const arrayFormFieldKey = '_array_form_fields_';
    if (!target[arrayFormFieldKey]) {
        target[arrayFormFieldKey] = {};
    }
    target[arrayFormFieldKey][key] = true;
}
