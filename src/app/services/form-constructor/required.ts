export const requiredFieldsKey = '_object_required_fields_';

export function Required(target: any, key: string) {

    if (!target[requiredFieldsKey]) {
        target[requiredFieldsKey] = {};
    }
    target[requiredFieldsKey][key] = true;
}
