export const MAX_FIELDS_KEY = '_MAX_FIELDS_KEY_';

export function Max(max: number) {
    return function (target: any, key: string) {

        if (!target[MAX_FIELDS_KEY]) {
            target[MAX_FIELDS_KEY] = {};
        }
        target[MAX_FIELDS_KEY][key] = max;
    };
}
