import { Injectable } from '@angular/core';
import { EventManager } from '../classes/event-manager';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/filter';

export enum PagingTableEventType {
    PAGE_CHANGED,
    ITEMS_PER_PAGE_CHANGED,
    PAGE_LOADED,
    SEARCH_CHANGED,
    SORT_COLUMN_CHANGED,
    ITEM_DELETED
}

export class PagingTableEvent {
    constructor(public eventType: PagingTableEventType, public payload: any) {
    }
}

export class PagingTablePageChangedPayload {
    constructor(public page: number) {
    }
}

export class PagingTableItemsPerPageChangedPayload {
    constructor(public itemsPerPage: number) {
    }
}

export class PagingTablePageLoadedPayload {
    constructor(public total: number, public items: Array<any>) {
    }
}

export class PagingTableSearchChangedPayload {
    constructor(public search: string) {
    }
}

export class PagingTableSortColumnChangedPayload {
    constructor(public key: string, public asc: boolean) {

    }
}

export class PagingTableItemDeletedPayload {
    constructor(public item: any) {

    }
}

@Injectable()
export class PagingTableService extends EventManager {

    private serviceId: number;

    constructor() {
        super(BehaviorSubject);
        this.serviceId = Math.floor(Math.random() * 10);
        // console.log('pts id: ', this.serviceId);
    }

    itemsCount(): Promise<Array<number>> {
        return Promise.resolve([10, 50, 100]);
    }

    push(event: PagingTableEvent) {
        this.eventsFor(`${event.eventType}`).next(event);
    }

    on(eventType: PagingTableEventType): BehaviorSubject<PagingTableEvent> {
        return <BehaviorSubject<PagingTableEvent>> this.eventsFor(`${eventType}`).filter(item => item !== undefined && item !== null);
    }

    firePageChanged(page: number) {
        this.push(new PagingTableEvent(PagingTableEventType.PAGE_CHANGED, new PagingTablePageChangedPayload(page)));
    }

    fireItemsPerPageChanged(itemsPerPage: number) {
        this.push(new PagingTableEvent(
            PagingTableEventType.ITEMS_PER_PAGE_CHANGED,
            new PagingTableItemsPerPageChangedPayload(itemsPerPage)
        ));
    }

    firePageLoaded(total: number, items: Array<any>) {
        this.push(new PagingTableEvent(
            PagingTableEventType.PAGE_LOADED,
            new PagingTablePageLoadedPayload(total, items)
        ));
    }

    fireSearchChanged(search: string) {
        // console.log(this.serviceId, search);
        this.push(new PagingTableEvent(
            PagingTableEventType.SEARCH_CHANGED,
            new PagingTableSearchChangedPayload(search)
        ));
    }

    fireSortColumnChanged(key: string, asc: boolean) {
        this.push(new PagingTableEvent(
            PagingTableEventType.SORT_COLUMN_CHANGED,
            new PagingTableSortColumnChangedPayload(key, asc)
        ));
    }

    fireItemDeleted(item: any) {
        this.push(new PagingTableEvent(
            PagingTableEventType.ITEM_DELETED,
            new PagingTableItemDeletedPayload(item)
        ));
    }

}
