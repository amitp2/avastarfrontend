import { TestBed, inject, fakeAsync, tick } from '@angular/core/testing';

import { AddTokenInterceptorService } from './add-token-interceptor.service';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { AuthService } from '../auth.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ConfigService } from '../config.service';


class AuthStubService {
    getToken(): string {
        return null;
    }
}

describe('AddTokenInterceptorService', () => {

    let httpClient: HttpClient;
    let authService: AuthService;
    let httpMock: HttpTestingController;
    let configService: ConfigService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: AddTokenInterceptorService,
                    multi: true
                },
                {
                    provide: AuthService,
                    useClass: AuthStubService
                },
                ConfigService
            ],
            imports: [
                HttpClientTestingModule
            ]
        });

        configService = TestBed.get(ConfigService);
        httpClient = TestBed.get(HttpClient);
        authService = TestBed.get(AuthService);
        httpMock = TestBed.get(HttpTestingController);
    });

    it('should add authorized header to every request if token exists', fakeAsync(() => {
        const url = 'http://localhost:4200/some/request';
        const token = '123';
        spyOn(authService, 'getToken').and.returnValue(token);
        httpClient.get(url).subscribe(() => ({}));
        const req = httpMock.expectOne(url);
        req.flush({});
        tick();
        expect(req.request.headers.get('Authorization')).toEqual('Bearer ' + token);
    }));

    it('it should not add any token if url is GET auth/token', fakeAsync(() => {
        const url = `${configService.config().API_URL}auth/token`;
        const token = '123';
        spyOn(authService, 'getToken').and.returnValue(token);
        httpClient.get(url).subscribe(() => ({}));
        const req = httpMock.expectOne(url);
        req.flush({});
        tick();
        expect(req.request.headers.get('Authorization')).not.toEqual('Bearer ' + token);
    }));
});
