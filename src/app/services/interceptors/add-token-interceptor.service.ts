import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../auth.service';
import { ConfigService } from '../config.service';

@Injectable()
export class AddTokenInterceptorService implements HttpInterceptor {

    constructor(private injector: Injector,
                private configService: ConfigService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = this.injector.get(AuthService).getToken();
        const requestIsForRefreshToken = req.url === `${this.configService.config().API_URL}auth/token` && req.method === 'GET';
        const isZingTree = req.url.startsWith('https://zingtree.com/api');

        if (token && !requestIsForRefreshToken && !isZingTree) {
            req = req.clone({
                setHeaders: {
                    Authorization: `Bearer ${token}`
                }
            });
        }
        return next.handle(req);
    }

}
