import { TestBed, fakeAsync, tick } from '@angular/core/testing';

import { AsyncTasksService, AsyncTaskState, AsyncTaskStatus } from './async-tasks.service';

describe('AsyncTasksService', () => {

    let asyncTasksService: AsyncTasksService;
    let payload;
    const task = 'SOME_TASK';


    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [AsyncTasksService]
        });
        asyncTasksService = TestBed.get(AsyncTasksService);
        payload = {name: 'some name', lastName: 'some lastname'};
    });


    it('should dispatch async task state to subscribers', fakeAsync(() => {

        let state: AsyncTaskStatus;

        asyncTasksService.track(task).subscribe((s: AsyncTaskStatus) => {
            state = s;
        });
        asyncTasksService.task(task, AsyncTaskState.IN_PROGRESS);
        tick();
        expect(state.task).toEqual(task);
        expect(state.state).toEqual(AsyncTaskState.IN_PROGRESS);
        expect(state.payload).toEqual(null);

        asyncTasksService.task(task, AsyncTaskState.FINISHED);
        tick();
        expect(state.task).toEqual(task);
        expect(state.state).toEqual(AsyncTaskState.FINISHED);
        expect(state.payload).toEqual(null);

        asyncTasksService.task(task, AsyncTaskState.IN_PROGRESS, payload);
        tick();
        expect(state.task).toEqual(task);
        expect(state.state).toEqual(AsyncTaskState.IN_PROGRESS);
        expect(state.payload).toEqual(payload);
    }));

    it('taskSuccess should dispatch given task with success status and payload', fakeAsync(() => {
        let state: AsyncTaskStatus;
        asyncTasksService.track(task).subscribe((s: AsyncTaskStatus) => {
            state = s;
        });

        asyncTasksService.taskSuccess(task, payload);
        tick();
        expect(state.task).toEqual(task);
        expect(state.state).toEqual(AsyncTaskState.SUCCESS);
        expect(state.payload).toEqual(payload);

        payload.name = 'ddd';
        asyncTasksService.taskSuccess(task, payload);
        tick();
        expect(state.task).toEqual(task);
        expect(state.state).toEqual(AsyncTaskState.SUCCESS);
        expect(state.payload).toEqual(payload);
    }));

    it('taskError should dispatch given task with error status and payload', fakeAsync(() => {
        let state: AsyncTaskStatus;
        asyncTasksService.track(task).subscribe((s: AsyncTaskStatus) => {
            state = s;
        });

        asyncTasksService.taskError(task, payload);
        tick();
        expect(state.task).toEqual(task);
        expect(state.state).toEqual(AsyncTaskState.ERROR);
        expect(state.payload).toEqual(payload);

        payload.name = 'ddd';
        asyncTasksService.taskError(task, payload);
        tick();
        expect(state.task).toEqual(task);
        expect(state.state).toEqual(AsyncTaskState.ERROR);
        expect(state.payload).toEqual(payload);
    }));


    it('taskStart should dispatch given task with IN_PROGRESS status and payload', fakeAsync(() => {
        let state: AsyncTaskStatus;
        asyncTasksService.track(task).subscribe((s: AsyncTaskStatus) => {
            state = s;
        });

        asyncTasksService.taskStart(task);
        tick();
        expect(state.task).toEqual(task);
        expect(state.state).toEqual(AsyncTaskState.IN_PROGRESS);
        expect(state.payload).toEqual(null);

        asyncTasksService.taskStart(task, payload);
        tick();
        expect(state.task).toEqual(task);
        expect(state.state).toEqual(AsyncTaskState.IN_PROGRESS);
        expect(state.payload).toEqual(payload);
    }));


    it('taskNotify should dispatch given task with NOTIFY status and payload', fakeAsync(() => {
        let state: AsyncTaskStatus;
        asyncTasksService.track(task).subscribe((s: AsyncTaskStatus) => {
            state = s;
        });

        asyncTasksService.taskNotify(task);
        tick();
        expect(state.task).toEqual(task);
        expect(state.state).toEqual(AsyncTaskState.NOTIFY);
        expect(state.payload).toEqual(null);

        asyncTasksService.taskNotify(task, payload);
        tick();
        expect(state.task).toEqual(task);
        expect(state.state).toEqual(AsyncTaskState.NOTIFY);
        expect(state.payload).toEqual(payload);
    }));

    it('trackAnyError should notify always whenever some error happens', fakeAsync(() => {
        const statuses: AsyncTaskStatus[] = [];
        asyncTasksService.trackAnyError().subscribe((error: AsyncTaskStatus) => {
            statuses.push(error);
        });

        asyncTasksService.taskError('error_task_1', 'error1');
        asyncTasksService.taskSuccess('some_task_1', 'payload');
        asyncTasksService.taskError('error_task_2', 'error2');

        tick();

        expect(statuses).toEqual([
            new AsyncTaskStatus('error_task_1', AsyncTaskState.ERROR, 'error1'),
            new AsyncTaskStatus('error_task_2', AsyncTaskState.ERROR, 'error2')
        ]);
    }));

    it('trackAnySuccess should notify always whenever some success happens', fakeAsync(() => {
        const statuses: AsyncTaskStatus[] = [];
        asyncTasksService.trackAnySuccess().subscribe((success: AsyncTaskStatus) => {
            statuses.push(success);
        });

        asyncTasksService.taskSuccess('some_task_1', 'payload2');
        asyncTasksService.taskError('error_task_1', 'error1');
        asyncTasksService.taskSuccess('some_task_1', 'payload');
        asyncTasksService.taskError('error_task_2', 'error2');

        tick();

        expect(statuses).toEqual([
            new AsyncTaskStatus('some_task_1', AsyncTaskState.SUCCESS, 'payload2'),
            new AsyncTaskStatus('some_task_1', AsyncTaskState.SUCCESS, 'payload')
        ]);
    }));

    it('trackAnyStart should notify always whenever some start happens', fakeAsync(() => {
        const statuses: AsyncTaskStatus[] = [];
        asyncTasksService.trackAnyStart().subscribe((success: AsyncTaskStatus) => {
            statuses.push(success);
        });

        asyncTasksService.taskStart('some_task_1', 'payload2');
        asyncTasksService.taskError('error_task_1', 'error1');
        asyncTasksService.taskStart('some_task_1', 'payload');
        asyncTasksService.taskError('error_task_2', 'error2');

        tick();

        expect(statuses).toEqual([
            new AsyncTaskStatus('some_task_1', AsyncTaskState.IN_PROGRESS, 'payload2'),
            new AsyncTaskStatus('some_task_1', AsyncTaskState.IN_PROGRESS, 'payload')
        ]);
    }));

    it('trackAnyFinish should notify always whenever some start happens', fakeAsync(() => {
        const statuses: AsyncTaskStatus[] = [];
        asyncTasksService.trackAnyFinish().subscribe((success: AsyncTaskStatus) => {
            statuses.push(success);
        });

        asyncTasksService.task('some_task_1', AsyncTaskState.FINISHED, 'payload2');
        asyncTasksService.taskError('error_task_1', 'error1');
        asyncTasksService.task('some_task_1', AsyncTaskState.FINISHED, 'payload');
        asyncTasksService.taskError('error_task_2', 'error2');

        tick();

        expect(statuses).toEqual([
            new AsyncTaskStatus('some_task_1', AsyncTaskState.FINISHED, 'payload2'),
            new AsyncTaskStatus('some_task_1', AsyncTaskState.FINISHED, 'payload')
        ]);
    }));

});
