import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../auth.service';

@Injectable()
export class VisitorOnlyGuardService implements CanActivate {

    constructor(private authService: AuthService,
                private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
        return this.authService
            .isUserAuthorized()
            .then((is: boolean) => {
                if (is) {
                    this.router.navigateByUrl('/u');
                }
                return !is;
            });
    }

}
