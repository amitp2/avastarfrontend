import { TestBed, inject, tick, fakeAsync } from '@angular/core/testing';

import { UserOnlyGuardService } from './user-only-guard.service';
import { AuthService } from '../auth.service';
import { ConfigService } from '../config.service';
import { HttpClientModule } from '@angular/common/http';
import { Router } from '@angular/router';
import { UtilsService } from '../utils.service';

class RouterStub {
    navigateByUrl() {
    }
}

class AuthStub {
    isUserAuthorized() {

    }
}

describe('UserOnlyGuardService', () => {

    let userOnlyGuardService: UserOnlyGuardService;
    let authService: AuthService;
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientModule
            ],
            providers: [
                UserOnlyGuardService,
                ConfigService,
                UtilsService,
                {provide: Router, useClass: RouterStub},
                {provide: AuthService, useClass: AuthStub}
            ]
        });

        userOnlyGuardService = TestBed.get(UserOnlyGuardService);
        authService = TestBed.get(AuthService);
        router = TestBed.get(Router);
    });

    it('canActivate method should resolve to true if user is authenticated', () => {
        spyOn(authService, 'isUserAuthorized').and.returnValue(Promise.resolve(true));
        userOnlyGuardService.canActivate(null, null).then((is: boolean) => {
            expect(is).toEqual(true);
        });
    });

    it('canActivate method should redirect user to "" route if user is not authorized and resolve to false', fakeAsync(() => {
        spyOn(authService, 'isUserAuthorized').and.returnValue(Promise.resolve(false));
        const spy = spyOn(router, 'navigateByUrl');
        let res: boolean;

        spy.and.callFake(() => {
        });

        userOnlyGuardService.canActivate(null, null).then((is: boolean) => {
            res = is;
        });

        tick();
        expect(res).toEqual(false);
        expect(spy.calls.first().args[0]).toEqual('/');
    }));
});
