import { TestBed, inject, fakeAsync, tick } from '@angular/core/testing';

import { CurrentUserGuardService } from './current-user-guard.service';
import { AuthService } from '../auth.service';

class AuthStub {
    currentUser() {

    }
}

describe('CurrentUserGuardService', () => {

    let currentUserGuardService: CurrentUserGuardService;
    let authService: AuthService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                CurrentUserGuardService,
                {provide: AuthService, useClass: AuthStub}
            ]
        });
        authService = TestBed.get(AuthService);
        currentUserGuardService = TestBed.get(CurrentUserGuardService);
    });

    it('it should fetches current user data and once its fetched it should continue', fakeAsync(() => {
        const spy = spyOn(authService, 'currentUser').and.returnValue(Promise.resolve());
        currentUserGuardService.canActivate(null, null);
        tick();
        expect(spy.calls.all().length).toEqual(1);
    }));


});
