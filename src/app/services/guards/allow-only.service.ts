import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserRole } from '../../enums/user-role.enum';
import { AuthService } from '../auth.service';
import { CurrentUser } from '../../interfaces/current-user';

@Injectable()
export class AllowOnlyService implements CanActivate {

    constructor(private authService: AuthService,
                private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
        const roles: UserRole[] = route.data.roles;
        return new Promise((resolve) => {
            this.authService.currentUser()
                .then((user: CurrentUser) => {
                    let yes = false;
                    user.roles.forEach((role: UserRole) => {
                        if (roles.filter(r => r === role).length > 0) {
                            yes = true;
                        }
                    });
                    if (!yes) {
                        this.router.navigateByUrl('/u');
                    }
                    resolve(yes);
                })
                .catch(() => {
                    this.router.navigateByUrl('/u');
                    resolve(false);
                });
        });
    }

}
