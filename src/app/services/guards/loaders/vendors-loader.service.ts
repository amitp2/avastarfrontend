import { Injectable } from '@angular/core';
import { VendorResourceService } from '../../resources/vendor-resource.service';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/store';
import { AuthService } from '../../auth.service';
import { VendorModel } from '../../../models/vendor-model';
import { VendorsLoaded } from '../../../store/vendors/vendors.actions';

@Injectable()
export class VendorsLoaderService implements CanActivate {

    constructor(private vendors: VendorResourceService,
                private store: Store<AppState>,
                private authService: AuthService) {
    }

    canActivate(route: ActivatedRouteSnapshot,
                state: RouterStateSnapshot): Promise<boolean> {
        return this.vendors
            .getAllBySubscriber(this.authService.currentUserSnapshot.subscriberId)
            .then((vendors: VendorModel[]) => {
                this.store.dispatch(new VendorsLoaded(vendors));
                return true;
            });
    }

}
