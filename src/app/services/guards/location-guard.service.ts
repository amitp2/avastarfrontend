import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../auth.service';
import { Injectable } from '@angular/core';
import { LocationService } from '../location.service';
import { GeolocationDataModel } from '../../models/geolocation-data.model';

@Injectable()
export class LocationGuardService implements CanActivate {

    constructor(private locationService: LocationService) {
    }

    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
        await Promise.all([
            this.locationService.fetchCurrent().catch(() => new GeolocationDataModel()),
            this.locationService.fetchCountries(),
            this.locationService.fetchStates()
        ]);
        return true;
    }
}
