import { fakeAsync, TestBed, tick } from '@angular/core/testing';

import { VisitorOnlyGuardService } from './visitor-only-guard.service';
import { AuthService } from '../auth.service';
import { HttpClientModule } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { Router } from '@angular/router';
import { UtilsService } from '../utils.service';

class RouterStub {
    navigateByUrl() {
    }
}

class AuthStub {
    isUserAuthorized() {

    }
}

describe('VisitorOnlyGuardService', () => {

    let visitorOnlyGuardService: VisitorOnlyGuardService;
    let authService: AuthService;
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientModule
            ],
            providers: [
                VisitorOnlyGuardService,
                ConfigService,
                UtilsService,
                {provide: Router, useClass: RouterStub},
                {provide: AuthService, useClass: AuthStub}
            ]
        });
        visitorOnlyGuardService = TestBed.get(VisitorOnlyGuardService);
        authService = TestBed.get(AuthService);
        router = TestBed.get(Router);
    });

    it('should be created', () => {
        expect(visitorOnlyGuardService).toBeTruthy();
    });

    it('canActivate method should resolve to true if user is NOT authenticated', () => {
        spyOn(authService, 'isUserAuthorized').and.returnValue(Promise.resolve(false));
        visitorOnlyGuardService.canActivate(null, null).then((is: boolean) => {
            expect(is).toEqual(true);
        });
    });

    it('canActivate method should redirect user to "u" route if user is authorized and resolve to false', fakeAsync(() => {
        spyOn(authService, 'isUserAuthorized').and.returnValue(Promise.resolve(true));
        const spy = spyOn(router, 'navigateByUrl');
        let res: boolean;

        spy.and.callFake(() => {
        });

        visitorOnlyGuardService.canActivate(null, null).then((is: boolean) => {
            res = is;
        });

        tick();
        expect(res).toEqual(false);
        expect(spy.calls.first().args[0]).toEqual('/u');
    }));

});
