import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../auth.service';

@Injectable()
export class CurrentUserGuardService implements CanActivate {

    constructor(private authService: AuthService) {
    }

    canActivate(route: ActivatedRouteSnapshot,
                state: RouterStateSnapshot): Promise<boolean> {
        return new Promise<boolean>(resolve => {
            this.authService.currentUser()
                .then(() => resolve(true))
                .catch(() => resolve(false));
        });
    }

}
