import { Injectable } from '@angular/core';
import { HttpResourceQueryParams } from '../interfaces/http-resource-query-params';
import { PagingTableLoadParams } from '../interfaces/paging-table-load-params';
import { UserRole } from '../enums/user-role.enum';
import { CurrentUser } from '../interfaces/current-user';
import { UserModel } from '../models/user-model';
import { UserStatus } from '../enums/user-status.enum';
import { EventSpaceType } from '../enums/event-space-type.enum';
import { ObjectTransformer } from '../interfaces/object-transformer';
import { ObjectTransformerType } from '../enums/object-transformer-type.enum';
import { Observable } from 'rxjs/Observable';
import {FormGroup} from '@angular/forms';
import { Cloner } from '../interfaces/cloner';
import { Copyable } from '../helpers/copyable';
import { ReportAction } from './report.service';
import { Converter } from '../helpers/converter';
import { SelectFetch } from '../modules/select/types/select-fetch';
import { SelectItem } from '../modules/select/types/select-item';
import { isSet } from '../helpers/helper-functions';
import { isNotSet } from '../helpers/functions/is-not-set';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class UtilsService {

public CountryCode=new BehaviorSubject<string>('');

    constructor() {
        this.CountryCode.asObservable();
    }

    tableToHttpParams(params: PagingTableLoadParams): HttpResourceQueryParams {
        const res = {
            page: params.page - 1,
            size: params.itemsPerPage,
            sortFields: [params.sortColumn],
            sortAsc: params.sortAsc
        };

        if (params.search) {
            res['search'] = params.search;
        }

        return res;
    }

    userRoleToDisplayString(role: UserRole): string {
        switch (role) {
            case UserRole.ADMIN:
                return 'Admin';
            case UserRole.SUPER_ADMIN:
                return 'Super Admin';
            case UserRole.TECH_TEAM_LEAD:
                return 'Tech Team Lead';
            case UserRole.TECH_TEAM:
                return 'Tech Team';
            case UserRole.EVENT_PLANNER:
                return 'Event Planner';
            case UserRole.SERVICE_TEAM:
                return 'Service Team';
            case UserRole.NONE:
                return 'None';
        }
        return '';
    }

    stringToUserRole(role: string): UserRole {
        switch (role) {
            case 'ROLE_ADMIN':
                return UserRole.ADMIN;
            case 'ROLE_SUPER_ADMIN':
                return UserRole.SUPER_ADMIN;
            case 'ROLE_TECH_LEAD':
                return UserRole.TECH_TEAM_LEAD;
            case 'ROLE_TECH_MEMBER':
                return UserRole.TECH_TEAM;
            case 'ROLE_EVENT_PLANNER':
                return UserRole.EVENT_PLANNER;
            case 'ROLE_SERVICE_TEAM':
                return UserRole.SERVICE_TEAM;
        }
        return UserRole.NONE;
    }

    userRoleToString(role: UserRole): string {
        switch (role) {
            case UserRole.ADMIN:
                return 'ROLE_ADMIN';
            case UserRole.SUPER_ADMIN:
                return 'ROLE_SUPER_ADMIN';
            case UserRole.TECH_TEAM_LEAD:
                return 'ROLE_TECH_LEAD';
            case UserRole.TECH_TEAM:
                return 'ROLE_TECH_MEMBER';
            case UserRole.EVENT_PLANNER:
                return 'ROLE_EVENT_PLANNER';
            case UserRole.SERVICE_TEAM:
                return 'ROLE_SERVICE_TEAM';
        }
        return null;
    }

    userHasRole(user: CurrentUser | UserModel, role: UserRole): boolean {
        return user.roles.filter((r: UserRole) => r === role).length > 0;
    }

    userHasAnyRole(user: CurrentUser | UserModel, roles: UserRole[]) {
        return user.roles.some(role => roles.indexOf(role) !== -1);
    }

    userStatusToString(status: UserStatus): string {

        switch (status) {
            case UserStatus.Blocked:
                return 'Blocked';
            case UserStatus.Active:
                return 'Active';
            case UserStatus.NotVerified:
                return 'Not Verified';
        }

        return 'Inactive';
    }

    addHttpToUrl(url: string): string {
        if (url.substr(0, 4) !== 'http') {
            return `http://${url}`;
        }
        return url;
    }

    eventSpaceTypeToText(status: EventSpaceType): string {
        switch (status) {
            case EventSpaceType.Active:
                return 'Active';
            case EventSpaceType.Inactive:
                return 'Inactive';
        }
        return null;
    }

    transformInto(source: any, transformer: ObjectTransformer): any {
        let keyTransformer;
        let key;
        const res = {};
        for (key in source) {
            keyTransformer = transformer[key];
            if (keyTransformer) {
                switch (keyTransformer.type) {
                    case undefined:
                    case null:
                    case ObjectTransformerType.Default:
                        res[keyTransformer.to] = source[key];
                        break;
                    case ObjectTransformerType.Function:
                        res[keyTransformer.to] = keyTransformer.transform(source[key]);
                        break;
                }

            }
        }
        return res;
    }

    resolveAfter(millis: number) {
        return new Promise((resolve) => {
            setTimeout(() => resolve(), millis);
        });
    }

    toPromise(observable: Observable<any>): Promise<any> {
        return new Promise((resolve, reject) => {
            const subs = observable.subscribe((val) => {
                resolve(val);
                subs.unsubscribe();
            }, () => {
                reject();
                subs.unsubscribe();
            });
        });
    }

    formValue(form: FormGroup, value: string, cb: (val: string) => void) {
        const val = form.get(value).value;
        if (val) {
            cb(val);
        }
    }

    buildObjectFromMappingDecorators(instance: any, fieldsKey: string, fieldsMappersKey: string): any {
        return UtilsService.buildObjectFromMappingDecorators(instance, fieldsKey, fieldsMappersKey);
    }

    buildObjectFromMappingDecoratorsInverse(source: any, instanceClass: any, fieldsKey: string, fieldsMappersKey: string) {
        return UtilsService.buildObjectFromMappingDecoratorsInverse(source, instanceClass, fieldsKey, fieldsMappersKey);
    }

    copyOf(name: string) {
        const prefix = 'Copy of';
        let end = 1;
        let tmp;

        if (name.startsWith(prefix)) {
            name = name.replace(prefix, '');
            name = name.trim();
            tmp = +name.split(' ')[name.split(' ').length - 1];
            end = tmp + 1;
            tmp = name.split(' ');
            tmp.pop();
            name = tmp.join(' ');
        }

        return `${prefix} ${name} ${end}`;
    }

    cloneMany<T>(many: T[], cloner: Cloner<T>): T[] {
        return many.map(e => cloner(e));
    }

    sumArray(arr: number[]): number {
        return arr.reduce((acc, cur) => acc + cur, 0);
    }

    copyMany<T extends Copyable<T>>(arr: T[]): T[] {
        return arr.map(i => i.copy());
    }

    async convertMany<F, T>(from: F[], converter: Converter<F, T>): Promise<T[]> {
        if (!from || !converter) {
            return [];
        }
        const all = from.map(f => converter.convert(f));
        const res = await Promise.all(all);
        return res;
    }

    print(html: string) {
        console.log("html", html)
        const win = window.open('about:blank', '_blank');
        win.document.write(html);
        win.print();
        win.close();
    }

    emptyFetch(): SelectFetch {
        return new (class EmptyFetch implements SelectFetch {
            async fetch(): Promise<SelectItem[]> {
                return [];
            }
        });
    }

    buildQueryStringUsingObject(obj: any): any {
        let res = '';

        Object.keys(obj).forEach(key => {
            if (isNotSet(obj[key])) {
                return;
            }
            res += `${key}=${encodeURIComponent(obj[key])}&`;
        });

        return res;
    }


    static buildObjectFromMappingDecoratorsInverse(source: any, instanceClass: any, fieldsKey: string, fieldsMappersKey: string) {
        const object = new instanceClass();
        const instance = new instanceClass();

        const fields = instance[fieldsKey];

        if (!fields) {
            return object;
        }

        const fieldsKeys = Object.keys(fields);
        const fieldsMappers = instance[fieldsMappersKey];

        fieldsKeys.forEach((key) => {
            object[key] = fieldsMappers[key].map( source[ fields[key] ] );
        });

        return object;
    }

    static buildObjectFromMappingDecorators(instance: any, fieldsKey: string, fieldsMappersKey: string): any {
        const object = {};

        const fields = instance[fieldsKey];

        if (!fields) {
            return object;
        }

        const fieldsKeys = Object.keys(fields);
        const fieldsMappers = instance[fieldsMappersKey];

        fieldsKeys.forEach((key) => {
            object[fields[key]] = fieldsMappers[key].map(instance[key]);
        });

        return object;
    }


}
