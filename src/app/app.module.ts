import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthService } from './services/auth.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ConfigService } from './services/config.service';
import { ActivityTrackerService } from './services/activity-tracker.service';
import { LoginComponent } from './routes/visitor/login/login.component';
import { InvalidFormControlDirective } from './directives/invalid-form-control.directive';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AsyncTasksDisableOnInProgressDirective } from './directives/async-tasks/async-tasks-disable-on-in-progress.directive';
import { AsyncTasksService } from './services/async-tasks.service';
import { UserComponent } from './routes/user/user.component';
import { GlobalsService } from './services/globals.service';
import { PanelComponent } from './components/layout/panel/panel.component';
import { VisitorComponent } from './routes/visitor/visitor.component';
import { ForgotComponent } from './routes/visitor/forgot/forgot.component';
import { ResetComponent } from './routes/visitor/reset/reset.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { DashboardComponent } from './routes/user/dashboard/dashboard.component';
import { DashboardIconComponent } from './components/layout/dashboard-icon/dashboard-icon.component';
import { ModalService } from './services/modal/modal.service';
import { ConfirmDialogComponent } from './components/dialog/confirm-dialog/confirm-dialog.component';
import { ModalMarkerDirective } from './directives/modal-marker.directive';
import { FileUploaderComponent } from './modules/shared/components/form-controls/file-uploader/file-uploader.component';
import { AddTokenInterceptorService } from './services/interceptors/add-token-interceptor.service';
import { AsyncTasksShowOnSuccessDirective } from './directives/async-tasks/async-tasks-show-on-success.directive';
import { SubscriberResourceService } from './services/resources/subscriber-resource.service';
import { CountryResourceService } from './services/resources/country-resource.service';
import { StateResourceService } from './services/resources/state-resource.service';
import { SubscriberContactResourceService } from './services/resources/subscriber-contact-resource.service';
import { SharedModule } from './modules/shared/shared.module';
import { PagingTableModule } from './modules/paging-table/paging-table.module';
import { UserResourceService } from './services/resources/user-resource.service';
import { UtilsService } from './services/utils.service';
import { VerificationComponent } from './routes/visitor/verification/verification.component';
import { VenueResourceService } from './services/resources/venue-resource.service';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppCache } from './interfaces/cache';
import { InMemoryCacheService } from './services/caches/in-memory-cache.service';
import { CurrentPropertyService } from './services/current-property.service';
import { VenueContactResourceService } from './services/resources/venue-contact-resource.service';
import { VenueSettingsResourceService } from './services/resources/venue-settings-resource.service';
import { EventSpaceResourceService } from './services/resources/event-space-resource.service';
import { FunctionSpaceResourceService } from './services/resources/function-space-resource.service';
import { StorageSpaceResourceService } from './services/resources/storage-space-resource.service';
import { VendorResourceService } from './services/resources/vendor-resource.service';
import { VendorContactResourceService } from './services/resources/vendor-contact-resource.service';
import { InventoryCategoryResourceService } from './services/resources/inventory-category-resource.service';
import { ManufacturerResourceService } from './services/resources/manufacturer-resource.service';
import { InventoryResourceService } from './services/resources/inventory-resource.service';
import { StoreModule } from '@ngrx/store';
import { AppStore } from './store/store';
import { InventoryAssetStatusResourceService } from './services/resources/inventory-asset-status-resource.service';
import { InventoryMaintenanceResourceService } from './services/resources/inventory-maintenance-resource.service';
import { InventoryDocumentResourceService } from './services/resources/inventory-document-resource.service';
import { VendorsLoaderService } from './services/guards/loaders/vendors-loader.service';
import { CostCategoryResourceService } from './services/resources/cost-category-resource.service';
import { RevenueCategoryResourceService } from './services/resources/revenue-category-resource.service';
import { ServiceChargeResourceService } from './services/resources/service-charge-resource.service';
import { TaxChargeResourceService } from './services/resources/tax-charge-resource.service';
import { BudgetService } from './services/budget.service';
import { EquipmentSystemResourceService } from './services/resources/equipment-system-resource.service';
import { InventorySpacesService } from './modules/route-inventory/services/inventory-spaces.service';
import { EquipmentResourceService } from './services/resources/equipment-resource.service';
import { PackageResourceService } from './services/resources/package-resource.service';
import { PromisedStoreService } from './services/promised-store.service';
import { VenueTermsResourceService } from './services/resources/venue-terms-resource.service';
import { VenueClientResourceService } from './services/resources/venue-client-resource.service';
import { VenueClientContactResourceService } from './services/resources/venue-client-contact-resource.service';
import { VenueEventResourceService } from './services/resources/venue-event-resource.service';
import { TicketResourceService } from './services/resources/ticket-resource.service';
import { FormConstructorService } from './services/form-constructor/form-constructor.service';
import { CommandConstructorService } from './services/command-constructor/command-constructor.service';
import { ResourceConstructorService } from './services/resource-constructor/resource-constructor.service';
import { FunctionResourceService } from './services/resources/function-resource.service';
import { PurchaseOrderService } from './services/resources/purchase-order.service';
import { ContractService } from './services/resources/contract.service';
import { EventContactResourceService } from './services/resources/event-contact-resource.service';
import { ProductResourceService } from './services/resources/product-resource.service';
import { ListToSubscriberAccessMapper } from './mappers/list-to-subscriber-access.mapper';
import { UserProductResourceService } from './services/resources/user-product-resource.service';
import { FunctionRevenueCategoryCalculatorService } from './services/function-revenue-category-calculator.service';
import { RevenueCategoryToCodeConverter } from './helpers/converters/revenue-category-to-code.converter';
import { LocationService } from './services/location.service';
import { PackageOrEquipmentToFunctionEquipmentConverter } from './helpers/converters/package-or-equipment-to-function-equipment.converter';
import { EquipmentToFunctionEquipmentConverter } from './helpers/converters/equipment-to-function-equipment.converter';
import { PackageToFunctionEquipmentConverter } from './helpers/converters/package-to-function-equipment.converter';
import { FunctionContactResourceService } from './services/resources/function-contact-resource.service';
import { InMemoryStorageService } from './services/in-memory-storage.service';
import { SubscriberPaymentHistoryService } from './services/resources/subscriber-payment-history.service';
import { DecisionTreeResourceService } from './services/resources/decision-tree-resource.service';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        InvalidFormControlDirective,
        AsyncTasksDisableOnInProgressDirective,
        UserComponent,
        PanelComponent,
        VisitorComponent,
        ForgotComponent,
        ResetComponent,
        HeaderComponent,
        DashboardComponent,
        DashboardIconComponent,
        ConfirmDialogComponent,
        ModalMarkerDirective,
        AsyncTasksShowOnSuccessDirective,
        VerificationComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        PagingTableModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot(),
        StoreModule.forRoot(AppStore)
    ],
    entryComponents: [
        ConfirmDialogComponent,
        FileUploaderComponent
    ],
    providers: [
        AuthService,
        ConfigService,
        ActivityTrackerService,
        UtilsService,
        AsyncTasksService,
        GlobalsService,
        ModalService,
        RevenueCategoryResourceService,
        SubscriberResourceService,
        CountryResourceService,
        StateResourceService,
        SubscriberContactResourceService,
        UserResourceService,
        VenueResourceService,
        CurrentPropertyService,
        VenueContactResourceService,
        VenueSettingsResourceService,
        EventSpaceResourceService,
        FunctionSpaceResourceService,
        StorageSpaceResourceService,
        VendorResourceService,
        VendorContactResourceService,
        InventoryCategoryResourceService,
        ManufacturerResourceService,
        InventoryResourceService,
        InventoryAssetStatusResourceService,
        InventoryMaintenanceResourceService,
        InventoryDocumentResourceService,
        VendorsLoaderService,
        CostCategoryResourceService,
        ServiceChargeResourceService,
        TaxChargeResourceService,
        BudgetService,
        EquipmentSystemResourceService,
        InventorySpacesService,
        EquipmentResourceService,
        PackageResourceService,
        PromisedStoreService,
        VenueTermsResourceService,
        VenueClientResourceService,
        VenueClientContactResourceService,
        VenueEventResourceService,
        PurchaseOrderService,
        TicketResourceService,
        FormConstructorService,
        CommandConstructorService,
        ResourceConstructorService,
        ContractService,
        FunctionResourceService,
        EventContactResourceService,
        ProductResourceService,
        ListToSubscriberAccessMapper,
        UserProductResourceService,
        FunctionRevenueCategoryCalculatorService,
        LocationService,
        FunctionContactResourceService,
        InMemoryStorageService,
        SubscriberPaymentHistoryService,
        RevenueCategoryToCodeConverter,
        PackageOrEquipmentToFunctionEquipmentConverter,
        EquipmentToFunctionEquipmentConverter,
        PackageToFunctionEquipmentConverter,
        DecisionTreeResourceService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AddTokenInterceptorService,
            multi: true
        },
        {
            provide: AppCache,
            useClass: InMemoryCacheService
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
