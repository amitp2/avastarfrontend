import { EquipmentServiceType } from '../enums/equipment';
import { FormField } from '../services/form-constructor/form-field';
import { DefaultValue } from '../services/form-constructor/default-value';
import { Required } from '../services/form-constructor/required';

export class EquipmentFormModel {

    @Required
    @DefaultValue(EquipmentServiceType.Equipment)
    @FormField
    type: EquipmentServiceType;

    @Required
    @FormField
    priceFromDate: string;

    @Required
    @FormField
    priceToDate: string;

    @Required
    @FormField
    price: number;

    @Required
    @FormField
    categoryId: string;

    @Required
    @FormField
    subCategoryId: string;

    @Required
    @FormField
    subCategoryItemId: string;

    @Required
    @FormField
    revenueCategoryId: string;

    @Required
    @FormField
    costCategoryId: string;

    // @Required
    // @FormField
    // crossRentalType: string;

    // @Required
    // @FormField
    // setupTime: string;

    @Required
    @FormField
    clientDescription: string;

    @Required
    @FormField
    workflowDescription: string;

    @Required
    @FormField
    equipmentProductInformation: string;

    @Required
    @FormField
    @DefaultValue(true)
    rentable: boolean;

    @FormField
    @DefaultValue(false)
    excludeFromReport: boolean;

    @FormField
    @DefaultValue(false)
    thirdParty: boolean;

}
