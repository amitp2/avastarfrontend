import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { ActivityTrackerService } from './services/activity-tracker.service';
import { ModalService } from './services/modal/modal.service';
import { ConfirmDialogComponent } from './components/dialog/confirm-dialog/confirm-dialog.component';
import { GlobalsService } from './services/globals.service';
import { AuthService } from './services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { AsyncTasksService, AsyncTaskState } from './services/async-tasks.service';
import { AppError } from './enums/app-error.enum';
import { AppSuccess } from './enums/app-success.enum';
import { SpinnerComponent } from './modules/shared/components/spinner/spinner.component';

class ActivityTrackerServiceStub {
    track(): any {

    }
}

class AuthServiceStub {
    startTokenRefreshProcess() {

    }

    refreshToken(): Promise<any> {
        return Promise.resolve();
    }
}

class ToastrServiceStub {
    error(arg1: any, arg2: any, arg3: any) {

    }

    success(arg1: any, arg2: any, arg3: any) {

    }

    // this.toastr.error('iooo cool message', 'cool title', {
    //     positionClass: 'toast-bottom-right',
    //     tapToDismiss: false
    // });
}

describe('AppComponent', () => {

    let activityTrackerService: ActivityTrackerService;
    let authService: AuthService;
    let toastrService: ToastrService;
    let asyncTasksService: AsyncTasksService;

    beforeEach(async(() => {

        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule
            ],
            declarations: [
                AppComponent,
                ConfirmDialogComponent,
                SpinnerComponent
            ],
            providers: [
                {provide: ActivityTrackerService, useClass: ActivityTrackerServiceStub},
                {provide: AuthService, useClass: AuthServiceStub},
                {provide: ToastrService, useClass: ToastrServiceStub},
                AsyncTasksService,
                ModalService,
                GlobalsService
            ]
        }).compileComponents();

        activityTrackerService = TestBed.get(ActivityTrackerService);
        authService = TestBed.get(AuthService);
        toastrService = TestBed.get(ToastrService);
        asyncTasksService = TestBed.get(AsyncTasksService);
    }));

    it('should create the app', fakeAsync(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));

    it('should call track method initially after creation', async(() => {
        const spy = spyOn(activityTrackerService, 'track');
        const fixture = TestBed.createComponent(AppComponent);
        fixture.detectChanges();
        expect(spy.calls.all().length).toEqual(1);
    }));

    it('it should start refresh token process, after creation', () => {
        const spy = spyOn(authService, 'startTokenRefreshProcess');
        const fixture = TestBed.createComponent(AppComponent);
        fixture.detectChanges();
        expect(spy.calls.all().length).toEqual(1);
    });

    it('it should call refresh token immediately after initialization', () => {
        const spy = spyOn(authService, 'refreshToken').and.callThrough();
        const fixture = TestBed.createComponent(AppComponent);
        fixture.detectChanges();
        expect(spy.calls.all().length).toEqual(1);
    });

    it('on async task error, it should show message if default message exists', fakeAsync(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const spy = spyOn(toastrService, 'error').and.callThrough();
        const toastrConf = {
            positionClass: 'toast-bottom-right',
            tapToDismiss: false
        };
        fixture.detectChanges();
        asyncTasksService.taskError('SOME_TASK', AppError.USER_LOGIN_FAILED);
        asyncTasksService.taskError('SOME_TASK_3', 'nonexistent error');
        asyncTasksService.taskError('SOME_TASK_2', AppError.CURRENT_USER_FAILED);
        tick();
        fixture.detectChanges();
        expect(spy.calls.argsFor(0)).toEqual([
            'There was a problem with your email or password. Please try again',
            'Error',
            toastrConf
        ]);

        expect(spy.calls.argsFor(1)).toEqual([
            'Error while fetching current users info',
            'Error',
            toastrConf
        ]);

    }));

    it('on async task success, it should show message if default message exists', fakeAsync(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const spy = spyOn(toastrService, 'success').and.callThrough();
        const toastrConf = {
            positionClass: 'toast-bottom-right',
            tapToDismiss: false
        };
        fixture.detectChanges();
        asyncTasksService.taskSuccess('SOME_TASK', AppSuccess.USER_LOGIN);
        asyncTasksService.taskSuccess('SOME_TASK_3', 'nonexistent error');
        asyncTasksService.taskSuccess('SOME_TASK_2', AppSuccess.USER_LOGIN);
        tick();
        fixture.detectChanges();
        expect(spy.calls.argsFor(0)).toEqual([
            'Login was successful',
            'Success',
            toastrConf
        ]);

        expect(spy.calls.argsFor(1)).toEqual([
            'Login was successful',
            'Success',
            toastrConf
        ]);

    }));

    it('should count async tasks in progress properly', fakeAsync(() => {
        const fixture = TestBed.createComponent(AppComponent);
        tick();
        fixture.detectChanges();

        expect(fixture.componentInstance.asyncTasksInProgress).toEqual(0);
        asyncTasksService.taskStart('task1');
        tick();
        asyncTasksService.taskStart('task1');
        tick();
        asyncTasksService.taskSuccess('task1');
        tick();
        asyncTasksService.taskStart('task1');
        tick();
        fixture.detectChanges();
        expect(fixture.componentInstance.asyncTasksInProgress).toEqual(2);

        asyncTasksService.taskError('task2');
        tick();
        asyncTasksService.task('task2', AsyncTaskState.FINISHED);
        tick();
        fixture.detectChanges();
        expect(fixture.componentInstance.asyncTasksInProgress).toEqual(0);
    }));

    it('async tasks in progress property should not go below zero', fakeAsync(() => {
        const fixture = TestBed.createComponent(AppComponent);
        fixture.detectChanges();
        expect(fixture.componentInstance.asyncTasksInProgress).toEqual(0);
        asyncTasksService.taskSuccess('task1');
        asyncTasksService.taskSuccess('task1');
        tick();
        fixture.detectChanges();
        expect(fixture.componentInstance.asyncTasksInProgress).toEqual(0);
    }));

});
