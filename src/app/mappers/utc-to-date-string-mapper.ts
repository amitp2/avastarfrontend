import { Mapper } from '../interfaces/mapper';
import * as moment from 'moment';

export class UtcToDateStringMapper implements Mapper<any, string> {


    map(from: any): string {
        return moment.utc(from).format('YYYY-MM-DD');
    }

}
