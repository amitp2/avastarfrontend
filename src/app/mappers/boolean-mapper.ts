import { Mapper } from '../interfaces/mapper';

export class BooleanMapper implements Mapper<any, boolean> {


    map(from: any): boolean {
        return from ? true : false;
    }

}
