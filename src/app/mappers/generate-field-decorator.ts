export function generateFieldDecorator(fieldsKey: string, fieldsMappersKey: string, mapTo: any, mapper: any) {
    return function (target: any, key: string) {
        if (!target[fieldsKey]) {
            target[fieldsKey] = {};
        }
        target[fieldsKey][key] = mapTo ? mapTo : key;

        if (!target[fieldsMappersKey]) {
            target[fieldsMappersKey] = {};
        }

        if (!target[fieldsMappersKey][key]) {
            target[fieldsMappersKey][key] = new mapper();
        }
    };
}
