import { Mapper } from '../interfaces/mapper';
import * as moment from 'moment';

export function dateFormatMapper(format: string) {
    return class DateFormatMapper implements Mapper<any, any> {
        map(from: any): any {
            if (!from) {
                return null;
            }
            return moment(from).format(format);
        }
    };
}
