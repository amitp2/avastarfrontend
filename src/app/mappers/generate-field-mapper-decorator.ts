export function generateFieldMapperDecorator(fieldsMappersKey: string, mapper: any) {
    return function (target: any, key: string) {
        if (!target[fieldsMappersKey]) {
            target[fieldsMappersKey] = {};
        }
        target[fieldsMappersKey][key] = new mapper();
    };
}
