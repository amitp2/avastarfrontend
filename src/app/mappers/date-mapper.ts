import { Mapper } from '../interfaces/mapper';
import * as moment from 'moment';

export class DateMapper implements Mapper<any, number> {

    map(from: any): number {
        if (!from) {
            return null;
        }
        return +moment(from).format('x');
    }
}
