import { Mapper } from '../interfaces/mapper';
import { ProductModel } from '../models/product-model';
import { SubscriberProducts } from '../store/subscriber-product-access/subscriber-product-access';

export class ListToSubscriberAccessMapper implements Mapper<ProductModel[], SubscriberProducts> {

    map(from: ProductModel[]): SubscriberProducts {
        const products = {};

        from.forEach((product) => {
            products[product.product] = product.licensedUsersCount;
        });

        return products;
    }

}
