import { Mapper } from '../interfaces/mapper';
import * as moment from 'moment';

export class DateToUtcTimestampMapper implements Mapper<any, number> {

    map(from: any): number {
        return +moment.utc(from).format('x');
    }

}
