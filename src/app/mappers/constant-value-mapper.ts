import { Mapper } from '../interfaces/mapper';

export function constantValueMapper(value: any) {
    return class ConstantValueMapper implements Mapper<any, any> {
        map(from: any): any {
            return value;
        }
    };
}
