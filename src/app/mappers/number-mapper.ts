import { Mapper } from '../interfaces/mapper';

export class NumberMapper implements Mapper<any, number> {

    map(from: any): number {
        if (!from) {
            return 0;
        }
        return +from;
    }
}
