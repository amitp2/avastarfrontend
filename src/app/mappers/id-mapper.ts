import { Mapper } from '../interfaces/mapper';

export class IdMapper implements Mapper<any, number> {

    map(from: any): number {
        if (from && from.id) {
            return +from.id;
        }
        return null;
    }
}
