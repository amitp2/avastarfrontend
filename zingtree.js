Array.prototype.replace = function (regex, replacement) {
    if (regex instanceof RegExp) {
        return this.map(function (item) {
            if (typeof item == 'string') {
                return item.replace(regex, replacement);
            }
            return item;
        })
    }
    return this;
};

function loadSelect2() {
    var style = document.createElement('link');
    var script = document.createElement('script');

    style.rel = 'stylesheet';
    style.href = 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css';

    script.async = false;
    script.src = 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js';

    document.head.appendChild(style);
    document.head.appendChild(script);
}

loadSelect2();
initListeners();

function initListeners() {
    var observer = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
            if (!mutation.addedNodes || !mutation.addedNodes.length) {
                return;
            }
            mutation.addedNodes.forEach(function (node) {
                if (node.classList && node.classList.contains('avastar-selector')) {
                    initSelector(node);
                }
            })
        })
    });

    observer.observe(document.body, { childList: true, subtree: true });
}

function initSelector(node) {
    jQuery(node).select2();
}
